@extends('layouts.master_insurance')
@section('content')


    <div class="container">
        @if(!empty($order_id))
            <br>
            <h5>Thank you for buying a policy from InsureOnline. Your Transaction is successful.</h5>
            {{-- For any future reference please quote us with transaction id " {{$order_id}}  --}}
            <br>


            <h5>We will send invoice on your registered email id. Policy issuance is subject to approval of all the submitted documents.</h5>
            <br>
        @else
            <h5>{{$errors}}</h5>
        @endif

    </div>
    {{-- <div class="container">
        <h3>Upload Documents</h3>
        <span class="uploadstatus" style="display:none;color:green">

        </span>
        <form>
           
            Upload Documents (can add more than one) :
            <br>
            <br>
            <input type="file" id="fileupload" name="filename[]" data-url="{{url('/upload')}}" multiple="">
            <br>
            <br>
            <div id="files_list"></div>
            <p id="loading"></p>
            <input type="hidden" name="file_ids" id="file_ids" value="">
             <input type="hidden"  id ="order_id" name="order_id"  value="{{$order_id}}">
              <input type="hidden" id ="user_id" name="user_id"  value="{{$user_id}}">
            <input type="submit" value="Upload"  id="upload">
        </form>
        <br>
  </div> --}}

    <div class="container">
        <h3>Upload Documents</h3>
        <form method="POST" id="formpolicydoc" action="{{ route('uploaded-policy-documents') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden"  id ="order_id" name="order_id"  value="{{$order_id}}">
        <input type="hidden" id ="user_id" name="user_id"  value="{{$user_id}}">
        <table class="table table-striped table-bordered table-hover">
          <tr>
                {{-- <th>Sr.No.</th> --}}
                <th>Documents</th>
                <th>Upload</th>
                
          </tr>
          <tbody>
            @if($policydocs)
                @php ($i = 1) @endphp
                @foreach($policydocs as $policydoc)
                    
                  <tr>
                    {{-- <td><input type="text" name="docname[]" value="{{ $policydoc->name}}"></td> --}}
                    <td>
                        <label>{{ $policydoc->name}}</label>
                        <input type="hidden" name="docname[]" value="{{ $policydoc->name}}">
                    </td>
                    <td>
                        <input type="file" class="fileupload" name="filename[]">
                    </td>
                  </tr>
                  @php ($i++) @endphp
                @endforeach
            @endif
          </tbody>
        </table>
            <input type="submit" value="submit"  id="submit">
            <span class="my-error" style='color:#b94a48;display:none' >*</span>
        </form>
        <br>
  </div>

  @section('script')
<script type="text/javascript">

    function validForm(){

        var is_valid = true;

        var docname_arr = [];
        var filename_arr = [];

        $("input[name='docname[]']").each(function() {
            var value = $(this).val();
            if (value) {
                docname_arr.push(value);
            }
        });
        if (docname_arr.length === 0) {

            $('.my-error').html("Document Name not empty").show();
            is_valid = false;
        }

        $("input[name='filename[]']").each(function() {
            var value = $(this).val();
            if (value) {
                filename_arr.push(value);
            }
        });
        if (filename_arr.length === 0) {

            $('.my-error').html("Please upload file").show();
            is_valid = false;
        }

        return is_valid;
    }
    
</script>

<script>
    $(function () {
        $('#fileupload').fileupload({
         headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            dataType: 'json',
            add: function (e, data) {
                $('#loading').text('Uploading...');
                data.submit();
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').html(file.name + ' (' + file.size + ' KB)').appendTo($('#files_list'));
                    if ($('#file_ids').val() != '') {
                        $('#file_ids').val($('#file_ids').val() + ',');
                    }
                    $('#file_ids').val($('#file_ids').val() + file.fileID);
                });
                $('#loading').text('');
            }
        });
    });
$(document).ready(function() {
//     $(document).keydown(function(event){
//     if(event.which === 123){
//        return false;
//      }else if (event.ctrlKey && event.shiftKey && event.keyCode === 73) {
//      return false;
//   }else if (event.ctrlKey && event.shiftKey && event.keyCode === 67) {
//      return false;
//   }else if (event.ctrlKey && event.shiftKey && event.keyCode=='i'.charCodeAt(0)) {
//      return false;
//   }else if (event.ctrlKey && event.keyCode == 'U'.charCodeAt(0)) {
//      return false;
//   }else if (event.ctrlKey || event.shiftKey){
//     return false;
//   }
//   else if (event.ctrlKey && event.shiftKey){
//     return false;
//   }
// });
//     $(document).bind("contextmenu",function(e){
//   return false;
//     });

        $('body').on('click', '#upload', function(event){
        event.preventDefault();
        var order_id = $('#order_id').val();
        var user_id = $('#user_id').val();
        var file_ids = $('#file_ids').val();
        if(!(file_ids == "")){
            $.ajax({   
                    url: "{{url('/uploaddocument')}}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: { user_id:user_id, order_id:order_id,file_ids:file_ids, _method: "POST"},
                    success: function(response) {
                    if ( response.status == 'success' ){
                        $(".uploadstatus").css("display", "block");
                        $(".uploadstatus").text(response.msg)
                        setTimeout(function() {
                            var redirect ="{{url('/start')}}";
                    window.location.href = redirect;
                    }, 2000);
                    }else{
                        $(".uploadstatus").css("display", "block");
                        $(".uploadstatus").text(response.msg)
                    }
                    
                    },
                dataType: 'json'
            });
        }else{
            alert('please select file to submit')
        }
        
    });


        $("#formpolicydoc").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));


});

</script>

  @endsection
@endsection