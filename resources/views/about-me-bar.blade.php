          <div class="main-step-form-inner">
            <div class="step-form-heading">
              <h3>More About Me</h3>
            </div>
            <div class="step-form-part">
              <!--form action="step-3a.html"-->
              <form id="more_about_me" action="#" method="POST" role="form">
                <div class="form-group">
                  <label>Is the vehicle modified or non-GCC spec?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="nongcc_yes" name="nongcc" class="custom-control-input">
                    <label class="custom-control-label" for="nongcc_yes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="nongcc_no" name="nongcc" class="custom-control-input">
                    <label class="custom-control-label" for="nongcc_no">No</label>
                  </div>
                  <div class="nongcc_error error_div"></div>
                </div>
                <div class="form-group">
                  <select class="form-control" id="nationality" name="nationality">
                      <option value="">Select Nationality</option>
                      @if(isset($country) && count($country))
                          @foreach($country as $countryVal)
                              <option value="{{$countryVal->name}}" 
                                  >{{strtoupper($countryVal->name)}}
                              </option>

                          @endforeach
                      @endif
                  </select>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="nationality_error error_div"></div>
                </div>
                <div class="form-group">
                  <label>Which country issued your first driving license?</label>

                  <select class="form-control" id="country" name="country">
                      <option value="">Select Country</option>
                      @if(isset($country) && count($country))
                          @foreach($country as $countryVal)
                              <option value="{{$countryVal->name}}" 
                                  >{{strtoupper($countryVal->name)}}
                              </option>

                          @endforeach
                      @endif
                  </select>

                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="country_error error_div"></div>
                </div>
                <div class="form-group">
                  <label>How many years of international driving experience do you have?</label>
                  <input type="number" class="form-control" placeholder="" id="driving_experience" name="driving_experience">
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="driving_experience_error error_div"></div>
                </div>
                <div class="form-group">
                  <label>How long have you been driving in the UAE?</label>
                  <input type="number" class="form-control" placeholder="" id="driving_in_uae" name="driving_in_uae">
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="driving_in_uae_error error_div"></div>
                </div>
                <div class="form-group">
                  <label>Date of Birth</label>
                  <input type="text" class="form-control calendar-icon" placeholder="dd/mm/yyyy" id="datepicker_dob" readonly>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="datepicker_dob_error error_div"></div>
                </div>
                <div class="btns-part">

                  <button type="button" class="btn btn-outline-light" id="btnHideStep4">Back</button>
                  <button type="submit" class="btn btn-secondary">Next</button>
                </div>
              </form>
            </div>
          </div>

<script>

  $( function() {
        $('#datepicker_dob').datetimepicker({
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,

        });
  } );
</script>