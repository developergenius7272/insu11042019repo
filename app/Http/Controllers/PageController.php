<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Page;
// 
use Carbon\Carbon;
use Session;
use Config;
use Log;

class PageController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($slug)
    {
        $page = Page::where('slug',$slug)->active()->first();
        if( $page ) {
            $this->data['page'] = $page;
            return view('pages.page-template-1',$this->data);
        }
        abort('404');
    }
}
