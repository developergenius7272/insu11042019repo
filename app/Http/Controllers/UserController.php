<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\InsuranceProvider;

use Carbon\Carbon;
use Session;
use Config;
use Log;

class UserController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function resetPassword($token)
    {
        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;
        //$token = bin2hex(random_bytes(10));

        $reset_password_token = $token;
        if(!empty($reset_password_token)){

            $user = User::where('reset_password_token',$reset_password_token)->first();

            if($user){
                $reset_password_date = $user->reset_password_date;
                $now_date = Carbon::now();
				$totalDuration = $now_date->diffInHours($reset_password_date);
				
				if($totalDuration >= 24){
					return abort(404);
				}
				else{
					$this->data['user'] = $user;
					return view('change-password',$this->data);
				}
            }
            else{
                return abort(404);
            }
        }
        else{
            return abort(404);
        }

    }

    public function resetPasswordSave(Request $request) {
    	
        $reset_password_token = trim($request->reset_password_token);
        $newpassword = trim($request->newpassword);
        $confirmpassword = trim($request->confirmpassword);


        request()->validate([
            'reset_password_token' => 'required',
            
            //'newpassword' => 'required|min:6|max:255|regex:/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/|confirmed',
            'newpassword' => 'required|min:6|max:255|regex:/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/',
            
            'confirmpassword'=> 'sometimes|required_with:newpassword',
        ]);

        $user = User::where('reset_password_token',$reset_password_token)->first();

        if(isset($newpassword) && isset($confirmpassword)){
            if($newpassword == $confirmpassword){
                if( $user ) {
                    
                    $user->password = bcrypt($confirmpassword);
                    $user->reset_password_token = null;
                    $user->reset_password_date = null;
                    $user->save();

                    return redirect('/')->with('success', 'You account has been successfully created');
                }
                else{
					return abort(404);
                }
            }
        }
        else{
            return abort(404);
        }
    }
}
