@extends('layouts.admin.master')
{{-- Cars table view --}}
@section('content')
<div class="row">
    {{-- <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i> 
                Table 
            <span>> 
                Normal Tables
            </span>
        </h1>
    </div> --}}
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Cars</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
    
                            <form class="form-horizontal" method="POST" action="{{url('admin/cars')}}">
                                 @csrf
                                <fieldset>
                                    <legend>Create a New car</legend>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="selectCarMake">Select Car Make</label>
                                        <div class="col-md-10">
                                            <select class="form-control" id="selectCarMake" name="carMake">
                                                @if(isset($carMakes) && count($carMakes))
                                                    @foreach($carMakes as $carMake)
                                                        <option value="{{$carMake->id}}">{{strtoupper($carMake->name)}}</option>
                                                    @endforeach
                                                        <option value="new">Create New</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{--  --}}
                                    <div class="form-group hide" id="selectCarMakeNew">
                                        <label class="control-label col-md-2">Enter New Car Make Name</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control ui-autocomplete-loading" placeholder="Enter New Car Make" name="carMakeName" id="carMakeName" type="text">
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--  --}}
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Car Modal</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="carModalName" id="carModalName" placeholder="Car Modal Name" type="text">
                                        </div>
                                    </div>
                                    {{--  --}}
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Car Modal Description</label>
                                        <div class="col-md-10">
                                            <textarea class="form-control" name="carModalDescription" placeholder="Car Modal Description" rows="4"></textarea>
                                        </div>
                                    </div>
                                
                                </fieldset>

                                <fieldset class="demo-switcher-1">
                                    <legend>Car Modal Variants</legend>

                                    {{-- <section>
                                         <label class="label">Variants</label>
                                         <div class="row">
                                            @if(isset($carModelVariants) && count($carModelVariants) )
                                                @foreach($carModelVariants as $carModelVariant)
                                                    <div class="col col-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input name="carModelVariants[]" value="{{$carModelVariant->id}}" type="checkbox" class="checkbox style-0" checked="checked">
                                                                <span>{{$carModelVariant->name}}</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                         </div>
                                    </section> --}}

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Variants</label>
                                        <div class="col-md-10">

                                            @if(isset($carModelVariants) && count($carModelVariants) )
                                                @foreach($carModelVariants as $carModelVariant)
                                                    <div class="checkbox">
                                                        <label>
                                                        <input name="carModelVariants[]" value="{{$carModelVariant->id}}" type="checkbox" class="checkbox style-0" >
                                                            <span>{{$carModelVariant->name}}</span>
                                                        </label>
                                                    </div>
                                                    @endforeach
                                            @endif
                                        </div>
                                    </div>

                                </fieldset>
                                

                                {{-- <fieldset>
                                    <legend>Simple input with icons</legend>
    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Input with spinner</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control ui-autocomplete-loading" placeholder="" type="text">
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </fieldset> --}}
                                
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="admin/" class="btn btn-default">Cancel</a>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
    
                            </form>
    
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            
        </div>

    </section>
</div>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
		
    $(document).ready(function() {
        
        pageSetUp();

        $("#selectCarMake").change(function() {
            if($(this).val() == 'new') {
                $("#selectCarMakeNew").removeClass('hide');
            }
        });
    })

</script>
@endsection
