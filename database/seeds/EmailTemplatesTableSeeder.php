<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplate;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $templates = [
	            [
	                'code' => 'user_verification',
	                'description' => 'User Verification',
	                'template_code' => '67433146-ba4a-4e5c-ae04-9404706a3240',
	                'default_subject' => 'Account Verification',
	                'default_from_name' => 'Insureonline',
	                'default_from_email' => 'notifications@app.com',
	                'group_name' => 'Account',
	                'title' => 'Yeah, you are almost there',
	                'main_html' => '<p>Please complete the activation of your Insureonline account by verifying your email address. Simply click the button below -<br />or copy/paste the activation link into your browser.<br /><br />Your activation link: {{code}}</p>',
	                'button_title' => 'ACTIVATE ACCOUNT',
	                'button_link' => '{{code}}',
	                'button_color' => '',
	                'comments' => '',
	                'allow_instyle' => 0,
	                'status' => 1
	            ],
                [
                    'code' => 'user_reset_password',
                    'description' => 'Password reset',
                    'template_code' => '67433146-ba4a-4e5c-ae04-9404706a3240',
                    'default_subject' => 'Reset your Insureonline password',
                    'default_from_name' => 'Insureonline',
                    'default_from_email' => 'notifications@app.com',
                    'group_name' => 'Account',
                    'title' => 'Reset your Insureonline password',
                    'main_html' => '<p>Simply click on the link below to reset the password for your Insureonline account.</p>',
                    'button_title' => 'RESET PASSWORD',
                    'button_link' => '{{code}}',
                    'button_color' => '',
                    'comments' => 'forgot password for partner or user',
                    'allow_instyle' => 0,
                    'status' => 1
                ],
                [
                    'code' => 'user_new_signup_drip_1',
                    'description' => 'Account setup success',
                    'template_code' => '67433146-ba4a-4e5c-ae04-9404706a3240',
                    'default_subject' => 'Welcome to Insureonline?',
                    'default_from_name' => 'Insureonline',
                    'default_from_email' => 'notifications@app.com',
                    'group_name' => 'Account',
                    'title' => 'Welcome! Are you ready to grow more fans with Insureonline?',
                    'main_html' => '<p>Whoohoo! Welcome to Insureonline! It&#39;s great to have you!<br /><br /><p>&nbsp;</p><br />Do you have questions or need help? Just reply to this email to get in touch.</p>',
                    'button_title' => 'CHECK YOUR ACCOUNT',
                    'button_link' => '{{code}}',
                    'button_color' => '',
                    'comments' => '',
                    'allow_instyle' => 0,
                    'status' => 1
                ],
            ];
            foreach ($templates as $template) {
                EmailTemplate::create($template);
            }
    }
}
