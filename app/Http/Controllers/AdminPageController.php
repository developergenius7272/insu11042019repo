<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;
use Image;
use Auth;

class AdminPageController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['breadcrumbs'][] = 'Pages'; 
        $this->data['pageTitle'] = 'Pages';
        // 
        $this->data['pages'] = Page::all();
        return view('admin.pages.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new page';
        return view('admin.pages.create_edit', $this->data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();
        request()->validate([
            'slug' => 'required|unique:pages|min:2|max:100'

        ]);
        // 
        $body = Page::create($request->all());
        // 
        // upload image
        $imageName = '';
        try{
            if ($request->hasFile('image')) { 
                
                $imageName = $body->id.'.'.$request->image->getClientOriginalExtension();
                $imageName = $body->id.'_main';
                $request->image->move(public_path('uploads/pages'), $imageName);
                // // resize for logo
                // $imageLogo = $body->id.'_logo';
                // $img = Image::make(public_path('uploads/pages/' . $imageName));
                // $img->fit(75)->save(public_path('uploads/pages/' . $imageLogo));

                $body->has_banner = 1;
            }
        }catch(\Exception $e){}
        // 
        $body->created_by = $adminID;
        $body->save();
        // 
        return redirect()->route('admin.pages.index')->with('success', 'Page added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit page';
        $this->data['data'] = Page::find($id);

        if( $this->data['data'] ) {
            // 
            return view('admin.pages.create_edit',$this->data);
        } else {
            return redirect()->route('admin.pages.index')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();
        // 
        $page = Page::find($id);
        if( $page ) {
            $input = $request->all();
            $page->update($input);
            // 
            if ($request->has_logo_image == '0' ){
                $page->has_banner = 0;
            }else{
                // upload image
                try{
                    $imageName = '';
                    if ($request->hasFile('image')) { 
                        
                        $imageName = $page->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $page->id.'_main';
                        $request->image->move(public_path('uploads/pages'), $imageName);
                        // // resize for logo
                        // $imageLogo = $body->id.'_logo';
                        // $img = Image::make(public_path('uploads/pages/' . $imageName));
                        // $img->fit(75)->save(public_path('uploads/pages/' . $imageLogo));

                        $page->has_banner = 1;
                    }
                }catch(\Exception $e){}
                // 
            }
            
            $page->updated_by = $adminID;
            $page->save();
            // 
            return redirect()->route('admin.pages.index')->with('success', 'Page updated successfully');
        }else{
            return redirect()->route('admin.pages.index')->with('error', 'No data found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function suggestUniqueSlug(Request $request)
    {
        $slug         = trim($request->slug);
        $id           = trim($request->slug);
        $excludeKey   = NULL;
        $excludeValue = NULL;
        // 
        if( $id != '' ) {
            $excludeKey = 'id';
            $excludeValue = $id;
        }
        // 
        if(!empty($slug)){
            $slug       = substr(str_slug($slug),0,48);
            $uniqueSlug = \Customfunction::getUniqueSlugs($slug,'pages');
            if(!is_null($uniqueSlug)) {
                return response()->json(['action' => 1,'slug' => $uniqueSlug]);
            }else{
                return response()->json(['action' => 0,'slug' => '']);
            }
        }else{
            return response()->json(['action' => 0,'slug' => '']);
        }
    }
}
