<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BenefitMaster extends Model
{
    protected $fillable = [
        'name', 'description', 'for_each_passenger', 'status'
    ];

    public function passengerCoverAgency($amountAgency, $no_of_seats){
       
        if(!empty($this->for_each_passenger) && ($this->for_each_passenger == 1)){
            $driverCover = 1;
            $passengerCover = $no_of_seats - $driverCover;
            $amountAgency = $amountAgency * $passengerCover;
        }
        return $amountAgency;
    }
    public function passengerCoverNonAgency($amountNonAgency, $no_of_seats){
        if(!empty($this->for_each_passenger) && ($this->for_each_passenger == 1)){
            $driverCover = 1;
            $passengerCover = $no_of_seats - $driverCover;
            $amountNonAgency = $amountNonAgency * $passengerCover;
        }
        return $amountNonAgency;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
