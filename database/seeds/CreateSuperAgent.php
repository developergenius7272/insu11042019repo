<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\User;

class CreateSuperAgent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role =  Role::create([
            'name' => 'super-agent',
            'display_name' => 'super agent',
            'description' => 'Site default super agent.'
        ]);

        $permission = Permission::where('name', 'agent')->first();
        $role->attachPermission($permission);

        $user = User::create([
        	'name' => 'App Super Agent',
        	'email' => 'superagent@app.com',
        	'password' => bcrypt('123456'),
        	'email_verified_at' => \Carbon\Carbon::now(),
        	'status' => 1
        	]);

        $role = Role::where('name', 'agent')->first();
        $user->attachRole($role);

        $role = Role::where('name', 'super-agent')->first();
        $user->attachRole($role);
    }
}
