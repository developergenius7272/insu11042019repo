@component('mail::message')
Hello {{ $user->name }},

Thank you for buying car insurance from InsureOnline. Below is the Invoice details for Additional Products.
<br>
 Additional products Details:
 <br>
<table>
    <tr><th>Benefit Name</th>
    <th>price</th></tr>
        @foreach( $order->additionalCovers as $addon )
        <tr>
        <td> {{ $order->crossSellingProduct->where('id', $addon->cross_selling_product_id)->first()->name }} </td>
        <td>AED {{ $addon->premium_value }}</td>
         </tr>
        @endforeach
    <tr>
        <td>
            VAT ({{$addon->vat.'%'}}) : AED {{$addon->vat_value}}
        </td>
    </tr>
    <tr>
        <td>
           Policy Fees : AED {{$addon->fee_value}}
        </td>
    </tr>
    <tr>
        <td>
           Total Payment : AED {{$addon->vat_value+$addon->fee_value+$addon->premium_value}}
        </td>
    </tr>
</table>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
