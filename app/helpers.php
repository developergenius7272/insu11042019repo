<?php
function sqlDateDMY($date,$separator='-') {
    $dtDate = explode($separator, $date);
    $dtDate = $dtDate[2] . "-" . $dtDate[1] . "-" . $dtDate[0];
    return $dtDate;
}
// 
function sqlDateMDY($date,$separator='-') {
    $dtDate = explode($separator, $date);
    $dtDate = $dtDate[2] . "-" . $dtDate[0] . "-" . $dtDate[1];
    return $dtDate;
}
// 
function sqlDateYMD($date,$separator='-') {
    $dtDate = explode($separator, $date);
    $dtDate = $dtDate[0] . "-" . $dtDate[1] . "-" . $dtDate[2];
    return $dtDate;
}

if ( ! function_exists('snakeCaseToText') ) {
    function snakeCaseToText($str) {
        return implode(' ', preg_split('/(?=[A-Z])/', $str));
    }
}
// 
function getSQLStatement($builder) {
    $sql = $builder->toSql();
    foreach ( $builder->getBindings() as $binding ) {
        $value = is_numeric($binding) ? $binding : "'".$binding."'";
        $sql = preg_replace('/\?/', $value, $sql, 1);
    }
    return $sql;
}
// 
function switchUrl($url)
{
    if (env('FORCE_SSL',0) == 1) {
        return $url = preg_replace("/^http:/i", "https:", $url);
    }else{
        return $url = preg_replace("/^https:/i", "http:", $url);
    }
    
    return $url;
}