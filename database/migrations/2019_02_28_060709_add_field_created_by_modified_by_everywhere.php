<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCreatedByModifiedByEverywhere extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('benefit_masters', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_makes', function(Blueprint $table) {
            $table->integer('created_by')->after('modifiedby')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_models', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_model_body_types', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_model_fuels', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_model_variants', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_premiums', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_third_party_covers', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('car_values', function(Blueprint $table) {
            $table->integer('created_by')->after('effective_date')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('cross_selling_products', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('excesses', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('homepages', function(Blueprint $table) {
            $table->integer('created_by')->after('display_at_home')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('insurance_providers', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
        Schema::table('plan_benefits_partners', function(Blueprint $table) {
            $table->integer('created_by')->after('status')->nullable();
            $table->integer('modified_by')->after('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
