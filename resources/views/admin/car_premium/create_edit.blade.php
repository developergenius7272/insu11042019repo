@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>

                
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarPremium" method="POST" @if($formType == 'edit') action="{{action('AdminCarPremiumController@update', $data->id)}}" @else action="{{route('admin.premium-plans.store')}}" @endif>
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Partner</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="insuranceProviderId">Insurance provider / Partner</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="insuranceProviderId" name="insuranceProviderId">
                                            <option value="">Select Partner</option>
                                            @if(isset($partners) && count($partners))
                                                @foreach($partners as $partner)
                                                    <option value="{{$partner->id}}" 
                                                        @if(old('insuranceProviderId') == $partner->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($partner->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                            </fieldset>
                            {{-- Car related --}}
                            <fieldset>
                                <legend>Cars</legend>
                                {{-- car body --}}

                                <div class="form-group">
                                    <label class="control-label col-md-2">Select Car Model Body Type</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                            @if($formType == 'edit')
                                                <input class="form-control" readonly type="text" value="{{$data->carModelBodyType->name}}">
                                                {{-- <select class="form-control" id="carBodyTypeId" name="carBodyTypeId">
                                                    <option value="">Select Car Type</option>
                                                    @if(isset($carBodies) && count($carBodies))
                                                        @foreach($carBodies as $body)
                                                            <option value="{{$body->id}}" 
                                                                @if(old('carBodyTypeId') == $body->id) 
                                                                    {{ 'selected' }} 
                                                                @elseif(isset($data) && $data->car_model_body_type_id == $body->id)
                                                                    {{ 'selected' }} 
                                                                @endif

                                                            >{{strtoupper($body->name)}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select> --}}
                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                            @else
                                                <select class="form-control" id="carBodyTypeId" name="carBodyTypeId[]" multiple="multiple">

                                                    @if(isset($carBodies) && count($carBodies))

                                                        @foreach($carBodies as $body)
                                                            @if($errors->any())
                                                                @php ($is_record_found = 'no') @endphp

                                                                @if(old('carBodyTypeId'))
                                                                    @foreach(old('carBodyTypeId') as $val)
                                                                        @if($val == $body->id) 
                                                                            @php ($is_record_found = 'yes')
                                                                            @endphp
                                                                            @break
                                                                        @endif
                                                                    @endforeach
                                                                @endif

                                                                <option value="{{$body->id}}" 
                                                                    @if ($is_record_found == 'yes')
                                                                        {{ 'selected' }} 
                                                                    @endif
                                                                    >{{strtoupper($body->name)}}
                                                                </option>

                                                            @else
                                                                <option value="{{$body->id}}" 
                                                                    {{-- @if(isset($data) && $data->car_model_body_type_id == $body->id)     selected 
                                                                    @endif --}}
                                                                >{{strtoupper($body->name)}}
                                                            </option>
                                                            @endif
                                                        @endforeach

                                                    @endif

                                                </select>
                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- car model variants --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Model and Variants</label>
                                        <div class="col-md-10" id="model_and_variants">

                                            
                                        </div>
                                    
                                </div>
                                {{-- cylinders --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Cylinders</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Number of Cylinders" name="cylinders" id="cylinders" value="{{ $errors->any() ? old('cylinders') : $data->car_model_cylinder ?? '' }}" type="number" min="1">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- car_model_min_value --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Car Model Min Value</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Car Model Min Value" name="car_model_min_value" id="car_model_min_value" type="number" min="1" value="{{ $errors->any() ? old('car_model_min_value') : $data->car_model_min_value ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- car_model_max_value --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Car Model Max Value</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Car Model Max Value" name="car_model_max_value" id="car_model_max_value" type="number" min="1" value="{{ $errors->any() ? old('car_model_max_value') : $data->car_model_max_value ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Age Range (optional)</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Minimum Age</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Minimum Age" name="min_age" id="min_age" type="number" min="1" value="{{ $errors->any() ? old('min_age') : $data->min_age ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Maximum Age</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Maximum Age" name="max_age" id="max_age" type="number" min="1" value="{{ $errors->any() ? old('max_age') : $data->max_age ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Agency</legend>
                                {{--  --}}
                                {{-- min_premium_agency_rate --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Agency Minimum Premium Rate</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Agency Minimum premium" name="min_premium_agency_rate" id="min_premium_agency_rate" type="number" min="0" step=".01" value="{{ $errors->any() ? old('min_premium_agency_rate') : $data->min_premium_agency_rate ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- min_premium_agency --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Agency Minimum Premium Amount</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input class="form-control" placeholder="Agency Minimum premium" name="min_premium_agency" id="min_premium_agency" type="number" min="0" value="{{ $errors->any() ? old('min_premium_agency') : $data->min_premium_agency ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- available_for_years --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Available for years</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Available for years" id="available_for_year" name="available_for_year" type="number" min="1" value="{{ $errors->any() ? old('available_for_year') : (isset($data->available_for_months) ? floor($data->available_for_months / 12) : '') }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if($formType == 'add')

                                    {{-- agency available_for_years (add more)--}}
                                    <div id="agency_available_for_year_box" style="display: none">
                                        <div class="" id="agency_available_for_year_section" ></div>
                                        <div class="form-group"><label class="control-label col-md-2"><span class="addOneMore" onclick="clone_div()"><i class="fa fa-plus-square"></i>Add New</span></label></div>
                                    </div>

                                @else

                                    @if($agency_years)

                                    <div id="agency_available_for_year_box">
                                        <div class="" id="agency_available_for_year_section" >

                                            @php ($i = 1) @endphp

                                            @foreach($agency_years as $agency_year)
                                                <?php //echo $agency_year->rate; ?>
                                                {{-- @if ( $i == 1 ) --}}

                                                <div class="form-group row_agency_repiar" data-ids="{{ $i }}">    
                                                    <label class="control-label col-md-2">Year {{ $i }}</label>    
                                                    <div class="col-md-10">        
                                                        <div class="row">            
                                                            <div class="col-sm-12">


                                                            @php $available_for_year = $data->available_for_months @endphp
                                                            @if(isset($available_for_year))
                                                                @php $available_for_years = floor($data->available_for_months / 12) @endphp
                                                            @endif

                                                            @if( ($i ) > $available_for_years)
                                                            {{-- @if( ($i + 1) < 4) --}}
                                                                
                                                                <input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="{{ $agency_year->rate == 0 ? 'Free' : $agency_year->rate }}">
                                                            @else

                                                                <input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="{{ $agency_year->rate == 0 ? 'Free' : $agency_year->rate }}" readonly="">
                                                            @endif
                                                                
                                                            </div>        
                                                        </div>    
                                                    </div>
                                                </div>

                                                @php ($i++) @endphp
                                            @endforeach 

                                        </div>
                                        <div class="form-group"><label class="control-label col-md-2"><span class="addOneMore" onclick="clone_div()"><i class="fa fa-plus-square"></i>Add New</span></label></div>

                                    </div>

                                    @endif

                                @endif
                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Non-Agency</legend>

                                {{-- min_premium_nonagency_rate --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Non-Agency Minimum Premium Rate</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input class="form-control" placeholder="Non-Agency Minimum premium" name="min_premium_nonagency_rate" id="min_premium_nonagency_rate" type="number" min="0" step=".01" value="{{ $errors->any() ? old('min_premium_nonagency_rate') : $data->min_premium_nonagency_rate ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                                {{-- min_premium_nonagency --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Non-Agency Minimum Premium Amount</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input class="form-control" placeholder="Non-Agency Minimum premium" name="min_premium_nonagency" id="min_premium_nonagency" type="number" min="0" value="{{ $errors->any() ? old('min_premium_nonagency') : $data->min_premium_nonagency ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                
                            </fieldset>

                                {{--  --}}

                            <fieldset>
                                <legend>Sub types / Status / Effective Date</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Standard / Superior</label>
                                    <div class="col-md-10">
                                        <div class="radio" id="car_model_sub_type">
                                            <label>
                                                <input type="radio" name="car_model_sub_type" 
                                                    @if(old('car_model_sub_type'))
                                                        @if(old('car_model_sub_type') == 1)
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->car_model_sub_type) && $data->car_model_sub_type == 1)
                                                            {{ 'checked' }}
                                                    @elseif($formType == 'add') 
                                                        {{ 'checked' }}
                                                    @endif
                                                value="1">Standard
                                            </label>
                                            <label>
                                                <input type="radio" name="car_model_sub_type" 
                                                    @if(old('car_model_sub_type'))
                                                        @if(old('car_model_sub_type') == 2)
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->car_model_sub_type) && $data->car_model_sub_type == 2)
                                                            {{ 'checked' }}
                                                    @endif
                                                value="2">Superior
                                            </label>
                                        </div>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Effective Date</label>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                {{-- <input type="text" name="effective_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly> --}}

                                                <input type="text" dbDate="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}"
                                                name="effective_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' )) @endphp

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            {{--  --}}
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.premium-plans.index')}}" class="btn btn-default">Cancel</a>
                                        @if($can_update)
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

<div class="hide" id="selected_car_model_variants">
    @if($formType == 'edit')
        @php $selectedVariants = $data->CarPremiumVariant->pluck('car_model_variant_id'); @endphp 

        @foreach($selectedVariants as $selectedVariant)
            <span id="{{$selectedVariant}}" data-id="{{$selectedVariant}}">{{$selectedVariant}}</span>
        @endforeach
    @endif
</div>
{{-- <div id="agency_available_for_year_copy">
    <div class="form-group" id="min_premium_agency">
        <label class="control-label col-md-2">Year</label>
        <div class="col-md-10">
            <div class="row">
                <div class="col-sm-12">
                    <input class="form-control" placeholder="Available for years" name="available_for_year" type="text" min="1" value="">
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 
        $("#selectCarMake").change(function() {
            $('#selectCarModel').data('selected',0);
            getCarModels();
        });

        if( $("#selectCarMake").val() > 0 ) {
            getCarModels();
        }

        $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });

        $("#formCarPremium").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == true){
                //afterward
            }
            else{
                $(window).scrollTop(0);
                return false;
            }
        }));

        // 
        $("#available_for_year").change(function() {
            var val = parseInt($(this).val());
            //var count = val+1;
            var count = val;
            
            var html = "";
            $("#agency_available_for_year_section").empty();
            $("#agency_available_for_year_box").css('display',' none');
            var str = "";
            for (i = 0; i < count; i++) {
                $("#agency_available_for_year_box").css('display',' block');
                
                    str+= '<div class="form-group row_agency_repiar" data-ids="'+ (i+1) +'">';
                    str+= '    <label class="control-label col-md-2">Year '+ (i+1) +'</label>';
                    str+= '    <div class="col-md-2">';
                    str+= '        <div class="row">';
                    str+= '            <div class="col-sm-12">';

                    if(i < val){
                        str+= '<input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="Free" readonly>';
                    }
                    else{
                        str+= '<input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="">';
                    }
                    
                    str+= '            </div>';
                    str+= '        </div>';
                    str+= '    </div>';

                    // if(i > val){
                    //     str += '<div class="col-md-2">';
                    //     str += '<a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a>';
                    //     str += '</div>';
                    // }
                    str+= '</div>';
            }

            $("#agency_available_for_year_section").append(str);

        });
        // 
        $("#carBodyTypeId").change(function() {
           var carBodyTypeId  = $(this).val();
           $("#model_and_variants").empty();
           getVariantByBodyType(carBodyTypeId);
        });

        @if($formType == 'edit')
            var getSelectedCarModelId = [];
            getSelectedCarModelId.push('{{$data->carModelBodyType->id}}');
            getVariantByBodyType(getSelectedCarModelId,function(){
                $("#model_and_variants input:checkbox").removeAttr('checked');
                $("#selected_car_model_variants span").each(function(index,ele){
                    var variantId = $(this).data('id');
                    // console.log(variantId);
                    $("#model_and_variants input:checkbox#car_variants_"+variantId).attr('checked','checked').prop('checked',true);
                })
            })
        @endif
    });

    function getVariantByBodyType(carBodyTypeId,successCallback) {
        
        $.ajax({
            type:'POST',
            url:'{{route('admin.getVariantByBodyType')}}',
            data:{carBodyTypeId:carBodyTypeId},
            dataType: "html",
            success:function(data){
                $("#model_and_variants").empty().html(data);
                // 
                if (typeof successCallback === "function") {
                    successCallback()
                }
            }
        });
    }

    function textToDate(t){
           var ary= t.split(" ");
                var aryDate=ary[0].split("/");
                var aryTime=ary[1].split(":");
                var convertedDate = new Date(aryDate[2], 0, aryDate[0], 0, 0, 0, 0);
                //new Date(year, month, day, hours, minutes, seconds, milliseconds)
                convertedDate.setMonth(aryDate[1]>0?aryDate[1]-1:aryDate[1] ); 

                return convertedDate;
    }

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var insuranceProviderId  = $.trim($("#insuranceProviderId").val());
        var cylinders  = $.trim($("#cylinders").val());
        var car_model_min_value  = $.trim($("#car_model_min_value").val());
        var car_model_max_value  = $.trim($("#car_model_max_value").val());
        var min_age  = $.trim($("#min_age").val());
        var max_age  = $.trim($("#max_age").val());
        var min_premium_agency_rate  = $.trim($("#min_premium_agency_rate").val());
        var min_premium_agency  = $.trim($("#min_premium_agency").val());
        var available_for_year  = $.trim($("#available_for_year").val());
        var min_premium_nonagency_rate  = $.trim($("#min_premium_nonagency_rate").val());
        var min_premium_nonagency  = $.trim($("#min_premium_nonagency").val());
        //var datetimepicker  = $.trim($("#datetimepicker").val());
        var t= $.trim($("#datetimepicker").val());  

        if (insuranceProviderId == ''){

            $("#insuranceProviderId").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insuranceProviderId').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        @if($formType == 'add')
            var carBodyTypeId = $('#carBodyTypeId option:selected').index();
            if (carBodyTypeId < 0){
                
                $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
                $('#carBodyTypeId').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        @else
            // var carBodyTypeId = $('#carBodyTypeId').prop('selectedIndex');

            // if (carBodyTypeId == 0){
                
            //     $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
            //     is_valid = false;
            // }
        @endif

        if (cylinders.length > 0){

            if(!$.isNumeric( cylinders ) || cylinders < 1){
                $("#cylinders").parent().find(".my-error").html('Please fill cylinders in numeric').show();
                $('#cylinders').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (car_model_min_value.length > 0){
            
            if(!$.isNumeric( car_model_min_value ) || car_model_min_value < 1){
                $("#car_model_min_value").parent().find(".my-error").html('Please fill car model min value in numeric').show();
                $('#car_model_min_value').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (car_model_max_value.length > 0){

            if(!$.isNumeric( car_model_max_value ) || car_model_max_value < 1){
                $("#car_model_max_value").parent().find(".my-error").html('Please fill car model max value in numeric').show();
                $('#car_model_max_value').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (min_age.length > 0){

            if(!$.isNumeric( min_age ) || min_age < 1){
                $("#min_age").parent().find(".my-error").html('Please fill min age in numeric').show();
                $('#min_age').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (max_age.length > 0){

            if(!$.isNumeric( max_age ) || max_age < 1){
                $("#max_age").parent().find(".my-error").html('Please fill max age in numeric').show();
                $('#max_age').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (min_premium_agency_rate.length > 0){

            if(!$.isNumeric( min_premium_agency_rate ) || min_premium_agency_rate < 1){
                $("#min_premium_agency_rate").parent().find(".my-error").html('Please fill min premium agency rate in numeric').show();
                $('#min_premium_agency_rate').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (min_premium_agency.length > 0){

            if(!$.isNumeric( min_premium_agency ) || min_premium_agency < 1){
                $("#min_premium_agency").parent().find(".my-error").html('Please fill min premium agency in numeric').show();
                $('#min_premium_agency').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (available_for_year.length == 0){

            $("#available_for_year").parent().find(".my-error").html('Please fill available for year').show();
            $('#available_for_year').closest('.form-group').addClass('has-error');
            is_valid = false;

        }
        else{
            if(!$.isNumeric( available_for_year ) || available_for_year < 1){
                $("#available_for_year").parent().find(".my-error").html('Please fill available for year in numeric').show();
                $('#available_for_year').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (min_premium_nonagency_rate.length > 0){

            if(!$.isNumeric( min_premium_nonagency_rate ) || min_premium_nonagency_rate < 1){
                $("#min_premium_nonagency_rate").parent().find(".my-error").html('Please fill min premium nonagency rate in numeric').show();
                $('#min_premium_nonagency_rate').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (min_premium_nonagency.length > 0){

            if(!$.isNumeric( min_premium_nonagency ) || min_premium_nonagency < 1){
                $("#min_premium_nonagency").parent().find(".my-error").html('Please fill min premium nonagency in numeric').show();
                $('#min_premium_nonagency').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        //effective date checking
        if($.trim(t).length>0)
        {
            @if($formType == 'add')
 
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
                if(currentDateTime.getTime() > effectiveDate.getTime())
                { 
                    $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                    is_valid = false;
                }

            @else

                var dbDateText = $("#datetimepicker").attr('dbdate');
                var dbDate  = textToDate(dbDateText)
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
 
                if(dbDate.getTime() != effectiveDate.getTime()){
                    if(currentDateTime.getTime() > effectiveDate.getTime())
                    {
                        $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                        is_valid = false;
                    }
                }


            @endif
            
        }
        else
        {
            //alert("Please Enter valid Effective Date");
            $("#datetimepicker").parent().find(".my-error").html('Please enter effective date').show();
            $('#datetimepicker').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        var lst=$('.available_for_year_rate');
        var result = true;
        for(j =0; j<lst.length; j++)
        {
            if($.trim( $(lst[j]).val()).length == 0){

                $("#available_for_year").parent().find(".my-error").html('Please fill all Year loading percentage').show();
                $('#available_for_year').closest('.form-group').addClass('has-error');
                is_valid = false;
                break;
            }
        }

        return is_valid;
    }

    //var counter = 1;
    function clone_div()
    {

        //var i = parseInt(i) + counter;

        //var available_for_year = $('#available_for_year').val();
        var i = $("div.row_agency_repiar:last-child").attr('data-ids');
        var available_for_year_rate_val = $("div.row_agency_repiar:last-child").find(".available_for_year_rate").val();
        
        //check empty or not
        if (available_for_year_rate_val.length == 0) {
            alert('Please fill previous year first');
        }
        else{
            i = parseInt(i) + 1;

            var str = '';

            str+= '<div class="form-group row_agency_repiar" data-ids="'+ i +'">';
            //str+= '    <label class="control-label col-md-2">Year '+ i+'</label>';
            str+= '    <label class="control-label col-md-2">Year '+ i +'</label>';
            str+= '    <div class="col-md-2">';
            str+= '        <div class="row">';
            str+= '            <div class="col-sm-12">';

            str+= '<input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="">';
            
            str+= '            </div>';
            str+= '        </div>';
            str+= '    </div>';
            str+= '</div>';

            $("#agency_available_for_year_section").append(str);
        }
    }

    //var counter = 1;
    function clone_div()
    {

        var lst=$('.available_for_year_rate');
        var result = true;
        for(j =0; j<lst.length; j++)
        {
            if($.trim( $(lst[j]).val()).length == 0){
                result = false;
                break;
            }
        }

        if(result == false){
            alert('Please fill previous year first');
            return false;
        }
        else{
        
            //var available_for_year = $('#available_for_year').val();
            var i = $("div.row_agency_repiar:last-child").attr('data-ids');
            i = parseInt(i) + 1;

            var str = '';

            str+= '<div class="form-group row_agency_repiar" data-ids="'+ i +'">';
            str+= '    <label class="control-label col-md-2">Year '+ i +'</label>';
            str+= '    <div class="col-md-2">';
            str+= '        <div class="row">';
            str+= '            <div class="col-sm-12">';

            str+= '<input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="">';
            
            str+= '            </div>';
            str+= '        </div>';
            str+= '    </div>';
            str+= '</div>';

            $("#agency_available_for_year_section").append(str);
        }
    }

    function clone_divx()
    {

        //var available_for_year = $('#available_for_year').val();
        var i = $("div.row_agency_repiar:last-child").attr('data-ids');
        var available_for_year_rate_val = $("div.row_agency_repiar:last-child").find(".available_for_year_rate").val();
        
        //check empty or not
        if (available_for_year_rate_val.length == 0) {
            alert('Please fill previous year first');
        }
        else{
            i = parseInt(i) + 1;

            var str = '';

            str+= '<div class="form-group row_agency_repiar" data-ids="'+ i +'">';
            //str+= '    <label class="control-label col-md-2">Year '+ i+'</label>';
            str+= '    <label class="control-label col-md-2">Year '+ i +'</label>';
            str+= '    <div class="col-md-2">';
            str+= '        <div class="row">';
            str+= '            <div class="col-sm-12">';

            str+= '<input class="form-control available_for_year_rate" placeholder="Loading Percentage" name="available_for_year_rate[]" type="text" value="">';
            
            str+= '            </div>';
            str+= '        </div>';
            str+= '    </div>';
            str+= '</div>';

            $("#agency_available_for_year_section").append(str);
        }
    }

    function getCarModels() {
        $("#selectCarModel").empty();
        $('#selectCarModel').append($('<option>', {value: '',text: 'Select Model'}));
        // 
        if($("#selectCarMake").val() == 'new') {
            $("#selectCarMakeNew").removeClass('hide');
        } else {
            var carMakeId = $("#selectCarMake").val();
            $.ajax({
                type:'POST',
                url:'/admin/cars/make/models',
                data:{carMakeId:carMakeId},

                success:function(data){
                    if( data.action == '1' ) {
                        for( i in data.data) {
                            var row = data.data[i];
                            // console.log(row.name)
                            var selectOption = $('<option>', {value: row.id,text: row.name});
                            $('#selectCarModel').append(selectOption);
                            if( $('#selectCarModel').data('selected') > 0 ) {
                                var selected = $('#selectCarModel').data('selected');
                                $('#selectCarModel').val(selected);
                            }
                        }
                    }
                }
            });
        }
    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }


/*
    $(".formCarPremium").on('submit',(function(e) {

        var action = 0;

        var formType = "{{--$formType--}}";

        if (formType == 'add'){
            $.ajax(
            {
                type:'POST',
                //url:'/admin/retrievedatacarpremium',
                url:'{{--route('admin.getDataCarPremium')--}}',
                //data:{insuranceProviderId:insuranceProviderId,carBodyTypeIdVal:carBodyTypeIdVal},
                  data: new FormData(this),
                    processData: false,
                    contentType: false,
                  async : false,

                success: function(response){

                    response = $.parseJSON(response);
                    var status = response.status;
                    var msg = '';

                    if( status == 0) {

                        var name_insurance_provider = response.name_insurance_provider;

                        var name_car_model_body_type = response.implodeBodyType;

                        msg = "Insurance Provider : "+ name_insurance_provider + " and Car Model Body type : "+ name_car_model_body_type + " already exist, do you still want to update ?";
                        
                        if (confirm(msg)) {
                            
                            action = 1;
                            //return true;

                        } else {
                            
                            return false;
                        }

                    }

                    if( status == 1 ) {
                      
                      action = 1;

                    }

                }
            });

            if (action == 0){

                return false;
            }
        }
  }));
  */

</script>
@endsection
