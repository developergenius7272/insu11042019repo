<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

// use Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // 
    // protected function guard(){
    //     return Auth::guard('admin');
    // }

    /**
     * Show the application’s login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        //  $user = Auth::user('admin');

        // dd($user);
        return view('auth.admin-login');
    }
    
    public function logout(Request $request) {
        // dd('m here');
        if(Auth::user()->can('super-admin')){
            Auth::logout();
        }
        //Auth::guard('admin')->logout();
        return redirect()->guest(route( 'admin.login' ));
    }
}
