@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Import Car Values</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                    {{-- <form class="form-horizontal" method="POST" action="{{ route('admin.parse.import') }}" enctype="multipart/form-data">
                        {{ csrf_field() }} --}}
                    <p>{{ session('status') }}</p>

                    <form method="POST" id="formImport" action="{{ url("admin/import/car-value") }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                        <fieldset>
                            <legend>Car Values</legend>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Insurance Year</label>
                                <div class="col-md-4">
                                    
                                    <select class="form-control" id="insurance_year" name="insurance_year">
                                        @for($i=($current_year + 1 ); $i >= $current_year; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>

                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Car Manufacture Year</label>
                                <div class="col-md-4">
                                
                                    <select class="form-control" id="manufacture_year" name="manufacture_year">
                                        @for($i=($current_year + 1 ); $i >= 2000; $i--)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>

                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="control-label col-md-2">Effective Date</label>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <input type="text" name="effective_date" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                <label class="col-md-2 control-label">CSV or Excel file to import</label>
                                <div class="col-md-4">
                                    <input id="file" type="file" class="btn-default" name="file">
                                    {{-- <input id="file" type="file" class="form-control" name="file"> --}}
                                    
                                    @if ($errors->has('file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif

                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                </div>
                            </div>

                        </fieldset>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{url('/admin/import/car-value')}}" class="btn btn-default">Cancel</a>
                                    <button class="btn btn-primary" type="submit" name="submit">
                                        <i class="fa fa-save"></i>
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection


@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $("#formImport").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            scrollMonth : false,
            scrollInput : false
        });
    });

</script>
<script type="text/javascript">
    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var insurance_year  = $.trim($("#insurance_year").val());
        var manufacture_year  = $.trim($("#manufacture_year").val());
        var t  = $.trim($("#datetimepicker").val());

        if (insurance_year == ''){

            $("#insurance_year").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insurance_year').closest('.form-group').addClass('has-error');
            is_valid = false;

        }
        if (manufacture_year == ''){

            $("#manufacture_year").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#manufacture_year').closest('.form-group').addClass('has-error');
            is_valid = false;

        }

        //effective date checking
        if($.trim(t).length>0)
        {
            /*
            var ary= t.split(" "), aryDate=ary[0].split("/"),aryTime=ary[1].split(":");
            var effectiveDate = new Date(aryDate[2], 0, aryDate[0], aryTime[0], 0, 0, 0);//new Date(year, month, day, hours, minutes, seconds, milliseconds)
            effectiveDate.setMonth(aryDate[1]>0?aryDate[1]-1:aryDate[1] ); 
            var currentDateTime = new Date(); 
            currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
             
            if(effectiveDate.getTime()<currentDateTime.getTime())
            { 
                //alert("Please Enter valid Effective Date");
                $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                is_valid = false;
            }
            */
        }
        else
        {
            //alert("Please Enter valid Effective Date");
            $("#datetimepicker").parent().find(".my-error").html('Please enter effective date').show();
            $('#datetimepicker').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        var file = $("#file").val();
        var ext = file.split(".");
        ext = ext[ext.length-1].toLowerCase();

        //var arrayExtensions = ["jpg" , "jpeg", "png", "bmp", "gif"];
        if($.trim(ext).length>0){
            var arrayExtensions = ["csv" , "xlsx"];

            if (arrayExtensions.lastIndexOf(ext) == -1) {
                
                $("#file").parent().find(".my-error").html('Please insert correct file type').show();
                $('#file').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        else{
            $("#file").parent().find(".my-error").html('Please insert file type').show();
            $('#file').closest('.form-group').addClass('has-error');
            is_valid = false;            
        }

/*
        // var extension = $('#file').val().split('.').pop().toLowerCase();
        // alert(extension);
            var yourFileName = $("#file").val();

            var yourFileExtension = yourFileName.replace(/^.*\./, '');
            alert(yourFileExtension);

        //if($.inArray(extension, ['csv','xlsx','jpg','jpeg','bmp']) == -1) {
        if($.inArray(extension, ['csv','xlsx']) == -1) {
            alert('Sorry, invalid extension.');
            return false;
        }
*/

        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
