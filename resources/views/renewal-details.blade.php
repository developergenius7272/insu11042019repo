          <div class="main-step-form-inner">
            <div class="step-form-heading">
              <h3>Renewal Details</h3>
            </div>
            <div class="step-form-part">
              <!--form action="step-3a.html"-->
              <form id="renewal_details" action="#" method="POST" role="form">
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>Is the current policy Third Party Liability only?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="thirdpartyliablityYes" name="thirdpartyliablity" class="custom-control-input">
                    <label class="custom-control-label" for="thirdpartyliablityYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="thirdpartyliablityNo" name="thirdpartyliablity" class="custom-control-input">
                    <label class="custom-control-label" for="thirdpartyliablityNo">No</label>
                  </div>
                  <div class="thirdpartyliablity_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>Does your current policy include Agency Repair?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="agencyrepairYes" name="agencyrepair" class="custom-control-input">
                    <label class="custom-control-label" for="agencyrepairYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="agencyrepairNo" name="agencyrepair" class="custom-control-input">
                    <label class="custom-control-label" for="agencyrepairNo">No</label>
                  </div>
                  <div class="agencyrepair_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>Has your insurance policy already expired?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="policyexpiredYes" name="policyexpired" class="custom-control-input">
                    <label class="custom-control-label" for="policyexpiredYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="policyexpiredNo" name="policyexpired" class="custom-control-input">
                    <label class="custom-control-label" for="policyexpiredNo">No</label>
                  </div>
                  <div class="policyexpired_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>When do you want to register your car?</label>
                  <input type="text" class="form-control calendar-icon" placeholder="dd/mm/yyyy" id="datepicker_register" readonly>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="datepicker_register_error"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>Did you claim on your UAE car insurance in the last 12 months?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="claiminsuranceYes" name="claiminsurance" class="custom-control-input">
                    <label class="custom-control-label" for="claiminsuranceYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="claiminsuranceNo" name="claiminsurance" class="custom-control-input">
                    <label class="custom-control-label" for="claiminsuranceNo">No</label>
                  </div>
                  <div class="claiminsurance_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group" id="divClaimcertificate">
                  <label>Do you have a No Claims Certificate from your insurer(s)? Save up to 10%</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="claimcertificateYes" name="claimcertificate" class="custom-control-input">
                    <label class="custom-control-label" for="claimcertificateYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="claimcertificateNo" name="claimcertificate" class="custom-control-input">
                    <label class="custom-control-label" for="claimcertificateNo">No</label>
                  </div>
                  <div class="claimcertificate_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group" id="divClaimCertificateYear" style="display: none">
                  <label>Number of Years for No Claim Certificate</label>
                  <select class="form-control" id="claimCertificateYear" name="claimCertificateYear">
                    <option value="">Year</option>
                    @for($claimYear=1; $claimYear <= 10; $claimYear++)
                      <option value="{{$claimYear}}">{{$claimYear}}</option>
                    @endfor
                  </select>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 
                  <div class="claimCertificateYear_error error_div"></div>
                </div>
                <div class="btns-part">
                  <button type="button" class="btn btn-outline-light" id="btnHideStep5">Back</button>
                  <button type="submit" class="btn btn-secondary">Next</button>
                </div>
              </form>
            </div>
          </div>


<script>

  $( function() {
        $('#datepicker_register').datetimepicker({
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
  } );
</script>