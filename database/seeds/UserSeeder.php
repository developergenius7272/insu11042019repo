<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin =  User::create([
            'name' => 'agent',
            'email' => 'agent@example.com',
            'password' => bcrypt('agent1234')
        ]);

        $role = Role::first()->where('name', 'agent')->first();        
        $superAdmin->attachRole($role);
    }
}
