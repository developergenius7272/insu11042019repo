<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\PlanBenefit;
use App\Models\BenefitMaster;

class PlanBenefitMultiple extends Model
{
    //
    protected $table = 'plan_benefits_multiple';
    
    protected $fillable = [
        'plan_benefits_id', 'percentage_or_amount', 'amount', 'feature_description',
    ];

    public function planBenefits()
    {
        return $this->belongsTo(PlanBenefit::class, 'plan_benefits_id', 'id');
    }

    public function benefitMaster()
    {
        return $this->hasManyThrough(BenefitMaster::class, PlanBenefit::class, 'plan_benefits_id', 'benefit_masters_id', 'id', 'id');
    }

    public function amount($premiumValue, $no_of_seats, $benefit_id)
    {
        if ($benefit_id == 6) {
            if ($this->percentage_or_amount == 1 ) { 
                $amountcalcultaed = ($premiumValue * $this->amount) / 100;
                return  (($no_of_seats - 1) * $amountcalcultaed);
            } else {
                return (($no_of_seats - 1) * $this->amount);
            }
        } else {
            if ($this->percentage_or_amount == 1 ) { 
                $amountcalcultaed = ($premiumValue * $this->amount) / 100;
                return  $amountcalcultaed;
            } else {
                return $this->amount;
            }
        }
    }
    
}
