@extends('layouts.admin.master')
{{-- Frontend users view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START --> 
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Admin User</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formUser" method="POST" @if($formType == 'edit') action="{{action('AdminPanelUserController@update', $data->id)}}" @else action="{{route('admin.admin-users.store')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Admin User</legend>
                                {{-- user name --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- email --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Email" name="email" id="email" type="text" value="{{ $errors->any() ? old('email') : $data->email ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 {{-- email --}}
                                 @if(!isset($data))
                                <div class="form-group">
                                    <label class="control-label col-md-2">Password</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- Assign Role -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Role</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="role" class="form-control" >
                                                    <option value="sub-admin">Sub-admin</option>
                                                    <option value="super-admin">Super-admin</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Mobile --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Mobile</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Mobile" name="mobile" id="mobile" type="number" min="1" value="{{ $errors->any() ? old('mobile') : $data->mobile ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Address --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Address</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">


                                                <textarea class="form-control" placeholder="Textarea" name="address" id="address" rows="3">{{ $errors->any() ? old('address') : $data->address ?? '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Gender --}}
                                <!--legend>Gender</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Gender</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender'))
                                                        @if(old('gender') == 1)
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 1)
                                                            {{ 'checked' }}
                                                    @elseif($formType == 'add') 
                                                        {{ 'checked' }}
                                                    @endif
                                                value="1">Male
                                            </label>
                                            <label>
                                                <input type="radio" name="gender"
                                                    @if(old('gender'))
                                                        @if(old('gender') == 2) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 2)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="2">Female
                                            </label>
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender') )
                                                        @if(old('gender') == 3) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 3)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="3">Other  
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                {{-- Marital Status --}}
                                <!--legend>Marital Status</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Marital Status</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Married") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Married")
                                                        {{ 'checked' }} 
                                                    @elseif($formType == 'add') 
                                                        {{ 'checked' }}
                                                    @endif

                                                value="Married">Married
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Unmarried") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Unmarried")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Unmarried">Unmarried
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Divorced") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Divorced")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Divorced">Divorced
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Widow") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Widow")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Widow">Widow
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Single") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Single")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Single">Single
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Date Of Birth</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                @if($formType == 'edit')
                                                    {{-- <input type="text" name="dob" value="{{ isset($data->dob) ? sqlDateDMY($data->dob,'-') : '' }}" placeholder="Date of birth" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly> --}}

                                                    <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : (isset($data->dob) ? $data->dob->format('d/m/Y') : '') }}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly>

                                                @else
                                                    {{-- <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : '' }}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly> --}}

                                                    <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : ''}}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> Passport Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Passport Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Passport Number" name="passport_number" id="passport_number" type="text" value="{{ $errors->any() ? old('passport_number') : $data->passport_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="passport_expiry_date" value="{{ $errors->any() ? old('passport_expiry_date') : $data->passport_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> License Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">License Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="License Number" name="license_number" id="license_number" type="text" value="{{ $errors->any() ? old('license_number') : $data->license_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input type="text" name="license_expiry_date" value="{{ $errors->any() ? old('license_expiry_date') : $data->license_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> UAE ID Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">UAE ID</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="UAE ID" name="uae_id" id="uae_id" type="text" value="{{ $errors->any() ? old('uae_id') : $data->uae_id ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="uae_id_expiry_date" value="{{ $errors->any() ? old('uae_id_expiry_date') : $data->uae_id_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> Other Details</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-4">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgUsers" src='{{ asset('uploads/users').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.admin-users.index')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 

<script type="text/javascript">
		
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $("#formUser").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        //$('#datetimepicker').datetimepicker({
        $('.datetimepicker').datetimepicker({
            //format:'Y-m-d',
            //format:'d-m-Y',
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });



        $("#image").change(function(){
 
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgUsers").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgUsers").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }
        });
    });
</script>

<script type="text/javascript">
    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var name  = $.trim($("#name").val());
        var email  = $.trim($("#email").val());

        if (name == ''){

            $("#name").parent().find(".my-error").html('Please enter name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;

        }        
        if (email == ''){

            $("#email").parent().find(".my-error").html('Please enter email address').show();
            $('#email').closest('.form-group').addClass('has-error');
            is_valid = false;

        }
        else{
            if(!validateEmail(email)){
                $("#email").parent().find(".my-error").html('Please enter valid email address').show();
                $('#email').closest('.form-group').addClass('has-error');
                is_valid = false;                
            }
        }

        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }

    function validateEmail(email)
    {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
</script>
@endsection
