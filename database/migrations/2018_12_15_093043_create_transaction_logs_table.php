<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->decimal('transaction_amount')->nullable();
            $table->decimal('tax')->nullable();
            $table->integer('payment_type')->nullable();
            $table->dateTime('transaction_date')->nullable();
            $table->string('transaction_status',50)->nullable();
            $table->string('status_code',50)->nullable();
            $table->string('response',500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_logs');
    }
}
