<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInAdditionalcoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additionalcovers', function (Blueprint $table) {
            $table->integer('insurance_provider_id')->after('cross_selling_product_id')->default(0)->nullable();
            $table->string('name')->after('insurance_provider_id')->nullable();
            $table->double('vat')->after('name')->default(0)->nullable();
            $table->decimal('vat_value')->after('vat')->default(0)->nullable();
            $table->tinyInteger('fee_type')->after('vat')->default(0)->nullable()->comment='0 = N/A, 1 = Percentage, 2 = Amount';
            $table->decimal('fee')->after('fee_type')->default(0)->nullable();
            $table->decimal('fee_value')->after('fee')->default(0)->nullable();
            $table->text('benefits')->after('premium_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additionalcovers', function (Blueprint $table) {
            //
        });
    }
}
