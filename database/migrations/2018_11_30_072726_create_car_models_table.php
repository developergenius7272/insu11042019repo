<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\CarModel;

class CreateCarModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_models');
        Schema::create('car_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_make_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->mediumInteger('order')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        // 
        $CarMake = new CarModel();
        $CarMake->car_make_id = 1;
        $CarMake->name = '1-Series';
        $CarMake->order = 1;
        $CarMake->save();
        // 
        $CarMake = new CarModel();
        $CarMake->car_make_id = 1;
        $CarMake->name = '3 Series';
        $CarMake->order = 1;
        $CarMake->save();
        // 
        $CarMake = new CarModel();
        $CarMake->car_make_id = 2;
        $CarMake->name = 'ALTO';
        $CarMake->order = 1;
        $CarMake->save();
        // 
        $CarMake = new CarModel();
        $CarMake->car_make_id = 2;
        $CarMake->name = 'ALTO 800';
        $CarMake->order = 1;
        $CarMake->save();
        // 
        $CarMake = new CarModel();
        $CarMake->car_make_id = 2;
        $CarMake->name = 'ALTO K10';
        $CarMake->order = 1;
        $CarMake->save();
        // 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_models');
    }
}
