{{-- @extends('layouts.master') --}}
@extends('dashboard')
@section('account-panel')

          <div class="col-md-9"> 
            <!-- Account Main Panel Start -->
            <div class="account-main-panel">
              <div class="account-main-heading">
                <h2><i class="fa fa-list-ul"></i> My Commission</h2>
              </div>
              <div class="account-main-box">
                <div class="my-policy-table-part"> 
                  <!-- Policy Table Start -->
                  <div class="my-policy-table-heading">
                    <h3>Active Policies</h3>
                  </div>
                  <div class="my-policy-table">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th>Sl. No.</th>
                            <th>User</th>
                            {{-- <th>Partners</th> --}}
                            {{-- <th>Total</th>
                            <th>Premium, PAB (Passenger), PAB (Driver)</th>
                            <th>Policy Fees, VAT</th> --}}
                            {{-- <th>Benefit Options</th> --}}
                            {{-- <th>Partner commission %</th>
                            <th>Partner commission</th> --}}
                            <th>Agent commission %</th>
                            <th>Agent commission</th>
                            <th>Created Date</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            {{-- <td>{{ $order->id }}</td> --}}
                            <td>{{ $order->user ? $order->user->name : $order->name  }} </td>
                            {{-- <td>{{ $order->insuranceProvider ? $order->insuranceProvider->name : $order->name  }} </td> --}}
                            {{-- <td>{{$order->total > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->total,2) : 'N/A' }} </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        Premium : {{$order->premium_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->premium_value,2) : 'N/A' }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        PAB (Passenger): {{$order->pab_passenger > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->pab_passenger,2) : 'N/A' }}
                                    </div> 
                                </div> 
                                <div class="row">
                                    <div class="col-md-12">
                                        PAB (Driver) : {{$order->pab_driver > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->pab_driver,2) : 'N/A' }}
                                    </div> 
                                </div>
                                
                            </td> --}}
                            {{-- <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ $order->policy_fees > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->policy_fees,2) : 'N/A' }}
                                    </div> 
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        {{ $order->vat_premium > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->vat_premium,2) : 'N/A' }} 
                                    </div> 
                                </div>
                            </td> --}}

                            {{-- <td>
                                {{ $order->base_commission_percentage > 0 ? $order->base_commission_percentage. ' %' : '' }}
                            </td>

                            <td>
                                @if($order->base_commission_amount > 0)

                                    @php 
                                        $partnerCommission = $order->base_commission_amount;
                                    @endphp

                                    {{ env('CURRENCY_CODE', 'AED').' '.number_format($partnerCommission,2) }}
                                    

                                @elseif($order->base_commission_percentage > 0)
                                    
                                    @php 
                                        $partnerCommission = ((double)$order->premium_value + (double)$order->pab_passenger + (double)$order->pab_driver);

                                        $partnerCommission = (double)$partnerCommission * ((double)$order->base_commission_percentage / 100);
                                    @endphp

                                    {{ env('CURRENCY_CODE', 'AED').' '.number_format($partnerCommission,2) }}
                                @endif
                            </td>  --}}
                            <td>
                                {{ $order->commission_percentage > 0 ? $order->commission_percentage. ' %' : '' }}
                            </td>
                            <td>
                                {{-- calculate partner commssion --}}
                                @if($order->base_commission_amount > 0)

                                    @php 
                                        $partnerCommission = $order->base_commission_amount;
                                    @endphp

                                    {{-- {{ env('CURRENCY_CODE', 'AED').' '.number_format($partnerCommission,2) }} --}}

                                @elseif($order->base_commission_percentage > 0)
                                    
                                    @php 
                                        $partnerCommission = ((double)$order->premium_value + (double)$order->pab_passenger + (double)$order->pab_driver);

                                        $partnerCommission = (double)$partnerCommission * ((double)$order->base_commission_percentage / 100);
                                    @endphp

                                    {{-- {{ env('CURRENCY_CODE', 'AED').' '.number_format($partnerCommission,2) }} --}}
                                @endif

                                {{-- calculate agent commssion --}}
                                @php 
                                    $partnerCommission = (double)$partnerCommission * ((double)$order->commission_percentage / 100);
                                @endphp                                
                                {{ $partnerCommission > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($partnerCommission,2) : 'N/A' }} 
                            </td>
                            <td>{{ $order->created_at }}</td>
                            {{-- <td>{{ $order->end_date }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->premium_value }}</td> --}}
                            {{-- <td><i class="fa fa-check"></i></td> --}}
                            {{-- <td>
                              <div class="dropdown">
                                <button class="btn btn-sm btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Extend </button>
                                <div class="dropdown-menu"> <a class="dropdown-item" href="#">Re-New</a> <a class="dropdown-item" href="#">Download Policy</a> </div>
                              </div>
                            </td> --}}
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <!-- Policy Table Start --> 
                </div>
              </div>
            </div>
            <!-- Account Main Panel End --> 
          </div>
@endsection