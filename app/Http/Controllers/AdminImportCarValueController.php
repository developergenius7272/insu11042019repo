<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
//use App\Models\Contact;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
use App\Models\CarValue;
use App\Models\CarModelBodyTypeKeyword;

use Importer; 
use Carbon\Carbon;

class AdminImportCarValueController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Import'; 
        $this->data['breadcrumbs'][] = 'Car Values'; 
        $this->data['pageTitle'] = 'Import';
    }

    public function import()
    {
        $current_year = date('Y');
        $this->data['current_year'] = $current_year;

        return view('admin.import_car_value.import', $this->data);
    }

    public function parseImport(Request $request)
    {
        
        //xlsx validation not working ??????      
        // request()->validate([
        //     'file' => 'required|mimes:csv,txt,xlsx,xls'
        // ]);

        // request()->validate([

        //     //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     //'file'=>'required|max:50000|mimes:xlsx,csv'
        //     'file'=> 'required|mimes:xlsx'

        // ]);

        //dd($request['file']->getClientOriginalExtension());die;


        $fileName = $request['file']->getClientOriginalExtension();

        if ($fileName == 'csv'){

            $filename = request()->file('file')->getRealPath();

            $file_handle = fopen($filename, "r");
            while (!feof($file_handle) ) {
                $line_of_text[] = fgetcsv($file_handle, 1024);
            }
            fclose($file_handle);

            return $this->importCsvExcel($request, $line_of_text, 'CSV');
        }
        elseif ($fileName == 'xlsx'){

            $path = request()->file('file')->getRealPath();
            $excel = Importer::make('Excel');
            $excel->load($path);
            $collection = $excel->getCollection();
            $line_of_text = $collection->toArray();

            return $this->importCsvExcel($request, $line_of_text, 'Excel');

        }
        else{
            return redirect('/admin/import/car-value')->with('error', 'File has not been imported');
        }
        

    }

    public function importCsvExcel($request, $line_of_text, $fileType){

        // // // // // // Condition --- Start --- // // // // // // //
        $insurance_year = $request->insurance_year;
        $manufacture_year = $request->manufacture_year;
        $effective_date = $request->effective_date;

        request()->validate([
            'insurance_year' => 'required',
            "manufacture_year"    => "required",
            "effective_date"    => "required",

        ], [
            'insurance_year.required' => "Insurance Year is required.",
            'manufacture_year.required' => "Manufacture Year is required.",
            'effective_date.required' => "Effective Date is required.",
        ]);

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }
        // 
        if(strlen($effective_date) < 1){
            $effective_date = Carbon::now();
        }

        $checkedError = true;
        /*
        //conditions
        $carmake_arr = array_column($line_of_text,0);
        //dd($carmake_arr);
        unset($carmake_arr[0]);
        $checkedError = true;
        $row = 0;
        foreach ( $carmake_arr as $value ){
            $row++;
            if( strlen($value) < 1 ) {
                $checkedError = false;
                $rowNumber = $row;
                echo $rowNumber. " carmake error";
                exit();                            
            }
        }
        */
        if ($checkedError == true){
            $i = 0;
            foreach($line_of_text as $cav_data){
                // escape first line (assuming this a header)
                if ( $i == 0 ) {
                    $i++;
                    continue;
                }
                $i++;

                $carMakeName           = trim( $cav_data['0'] );
                $carModalName          = trim( $cav_data['1'] );
                $carModalDescription  = trim( $cav_data['2'] );
                if (empty( $carMakeName ) || empty( $carModalName ) || empty( $carModalDescription )){
                    continue;
                }
                // 
                // 
                $carModalDescriptionArray = explode(",",$carModalDescription);
                // 
                $carModelEngine = trim($carModalDescriptionArray[0]);
                $carModelBodyType = trim($carModalDescriptionArray[1]);
                $cylinders = trim($carModalDescriptionArray[2]);
                //$cylindersArray = explode(" ",$cylinders);
                // // $cylindersVal = preg_replace("/[^0-9]/", '', $cylinders); // return 123456789
                $filteredNumbers = array_filter(preg_split("/\D+/", $cylinders));
                $cylindersVal = reset($filteredNumbers);
                if( $cylindersVal == '' || $cylindersVal <= 0 ) {
                    $cylindersVal = 0;
                }
                // 
                // 
                // 
                $values  = trim($cav_data['3']);
                $min_value  = trim($cav_data['4']);
                $max_value  = trim($cav_data['5']);
                // 
                // 
                $car_make_id = 0;
                $car_model_id = 0;
                $car_model_body_type_id = 0;
                $car_model_variant_id = 0;

                $carMakeExist = CarMake::where('name', $carMakeName)->first();
                if( !$carMakeExist ) {
                    $carMakeObj = new CarMake();
                    $carMakeObj->name = $carMakeName;
                    $carMakeObj->save();
                    $car_make_id = $carMakeObj->id;
                }else{
                    $car_make_id = $carMakeExist->id;
                }

                if($car_make_id > 0) {
                    // check if model exist ??
                    $carModelExist = CarModel::where('name',$carModalName)->where('car_make_id',$car_make_id)->first();
                    if( $carModelExist ) {
                        $car_model_id = $carModelExist->id;
                    }else {
                        $carModel = new CarModel();
                        $carModel->car_make_id = $car_make_id;
                        $carModel->name = $carModalName;
                        $carModel->status = 1;
                        $carModel->save();
                        // 
                        $car_model_id = $carModel->id;
                    }
                    // 
                    if( $car_model_id > 0 ) {
                        // 
                        $carModelBodyTypeExist = CarModelBodyType::where('name',$carModelBodyType)->first();
                        if( $carModelBodyTypeExist ) {
                            $car_model_body_type_id = $carModelBodyTypeExist->id;
                        }else {
                            // check if keyword match
                            $keywordMatch = CarModelBodyTypeKeyword::where('keyword',$carModelBodyType)->first();
                            if( $keywordMatch ) {
                                $body_id = $keywordMatch->car_model_body_type_id;
                                $carModelBodyTypeExist = CarModelBodyType::find($body_id);
                                if( $carModelBodyTypeExist ) {
                                    $car_model_body_type_id = $carModelBodyTypeExist->id;
                                }
                            } else {

                                $CarModelBodyType = new CarModelBodyType();
                                $CarModelBodyType->name = $carModelBodyType;
                                $CarModelBodyType->status = 1;
                                $CarModelBodyType->save();
                                // 
                                $newKeyword = new CarModelBodyTypeKeyword();
                                $newKeyword->car_model_body_type_id = $CarModelBodyType->id;
                                $newKeyword->keyword = $carModelBodyType;
                                $newKeyword->status = 1;
                                $newKeyword->save();
                                // 
                                $car_model_body_type_id = $CarModelBodyType->id;
                            }
                        }
                        // 
                        if($car_model_body_type_id > 0) {
                            // check if variant exist??
                            $CarModelVariantExist = CarModelVariant::where('car_model_id',$car_model_id)
                                                                    ->where('car_model_body_type_id',$car_model_body_type_id)
                                                                    ->where('engine_size',$carModelEngine)
                                                                    ->where('cylinders',$cylindersVal)
                                                                    ->first();

                            if( $CarModelVariantExist ) {
                                $car_model_variant_id = $CarModelVariantExist->id;
                            }else {
                                $CarModelVariant = new CarModelVariant();
                                $CarModelVariant->name = $carModalDescription;
                                $CarModelVariant->car_model_id = $car_model_id;
                                $CarModelVariant->make_year = $manufacture_year;
                                $CarModelVariant->car_model_body_type_id = $car_model_body_type_id;
                                $CarModelVariant->engine_size = $carModelEngine;
                                $CarModelVariant->cylinders = $cylindersVal;
                                $CarModelVariant->status = 1;
                                $CarModelVariant->save();
                                // 
                                // 
                                $car_model_variant_id = $CarModelVariant->id;
                            }
                        }
                        // 
                    }
                    // 
                }
                
                if( $car_model_variant_id > 0 ) {
                    $carValueExist = CarValue::where('car_make_id',$car_make_id)
                                        ->where('car_model_id',$car_model_id)
                                        ->where('insurance_year',$insurance_year)
                                        ->where('manufacture_year',$manufacture_year)
                                        ->where('values',$values)
                                        ->where('min_value',$min_value)
                                        ->where('max_value',$max_value)
                                        ->where('effective_date',$effective_date)
                                        ->where('car_model_variant_id',$car_model_variant_id)->first();
                    if( !$carValueExist ) {
                        $carValue = new CarValue();
                        $carValue->car_make_id = $car_make_id;
                        $carValue->car_model_id = $car_model_id;
                        $carValue->car_model_variant_id = $car_model_variant_id;

                        $carValue->values = $values;
                        $carValue->min_value = $min_value;
                        $carValue->max_value = $max_value;
                        $carValue->insurance_year = $insurance_year;
                        $carValue->manufacture_year = $manufacture_year;
                        $carValue->effective_date = $effective_date;
                        $carValue->save();
                    }
                }
            }

            //echo "success";
            return redirect('/admin/import/car-value')->with('success', $fileType.' has been imported');
        }
        else
        {
            //echo "error";
            return redirect('/admin/import/car-value')->with('error', $fileType.' has not been imported');
        }

    }
    public function show()
    {

        $this->data['breadcrumbs'][] = 'Show'; 
        $this->data['tableHeading'] = 'Car Values';

        $carValues = CarValue::select('car_makes.name as make_name','car_models.name as model_name', 'car_model_variants.name as variant_name','car_values.*')
                    ->join('car_makes','car_makes.id', 'car_values.car_make_id')
                    ->join('car_models','car_models.id', 'car_values.car_model_id')
                    ->join('car_model_variants','car_model_variants.id', 'car_values.car_model_variant_id')
                    ->orderBy('car_values.id','asc')
                    //->where('car_values.id', $id)
                    ->get();

        $this->data['carValues'] = $carValues;

        return view('admin.import_car_value.show', $this->data);

    }
    
}
