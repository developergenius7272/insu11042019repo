<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAgentAddonCommission extends Model
{
    protected $table = 'user_agent_addon_commissions';
    
    protected $fillable = [
        'user_id', 'cross_selling_product_id', 'percentage_or_amount', 'amount',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function crossSellingProduct() {
        return $this->belongsTo('App\Models\CrossSellingProducts', 'cross_selling_product_id', 'id');
    }

}
