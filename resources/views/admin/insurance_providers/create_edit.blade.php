@extends('layouts.admin.master')
{{-- Partner view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Partner</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formInsuranceProvider" method="POST" @if($formType == 'edit') action="{{action('AdminInsuranceProviderController@update', $data->id)}}" @else action="{{url('/admin/partners')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Partner</legend>
                                {{-- partner name --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- email --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Email" name="email" id="email" type="text" value="{{ $errors->any() ? old('email') : $data->email ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- site url --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Website</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Website" name="web_url" id="web_url" type="text" value="{{ $errors->any() ? old('web_url') : $data->web_url ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- mobile --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Contact Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Contact Number" name="contact_number" id="contact_number" type="number" min="1" value="{{ $errors->any() ? old('contact_number') : $data->contact_number ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- image --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-10">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>
                                {{-- image Edit--}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgInsuranceProviders" src='{{ asset('uploads/admin/insurance_providers').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                {{-- Recommended --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label" style="padding-top:0px;">Is Recommended Partner?</label>
                                
                                    <div class="col-md-10">

                                      <input type="checkbox" name="recommended_partner" value="1" 
                                           @if(old('recommended_partner') == 1) 
                                                {{ 'checked' }} 
                                            @elseif(isset($data->recommended_partner) && $data->recommended_partner == 1)
                                                {{ 'checked' }} 
                                            @endif
                                      >

                                      <label class="hint" style="margin-left: 10px;font-size: 9px;">   
                                        (Recommended partners are shown top of the list to the user)
                                      </label>

                                    </div>
                                </div>

                                <!-- Site base commission -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Base Commission</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="base_commission" id="base_commission" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('base_commission') == 2) 
                                                            {{ 'selected' }} 
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }}
                                                        @elseif(isset($data->base_commission_amount)  && $data->base_commission_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('base_commission') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->base_commission_percentage)  && $data->base_commission_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">

                                                <input class="form-control" name="base_commission_amount" id="base_commission_amount" 

                                                        @if(old('base_commission_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('base_commission_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->base_commission_amount)  && $data->base_commission_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('base_commission_amount') : $data->base_commission_amount ?? '' }}">

                                                <input class="form-control" name="base_commission_percentage" id="base_commission_percentage" 
                                                        @if(old('base_commission_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->base_commission_percentage)  && $data->base_commission_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('base_commission_percentage') : $data->base_commission_percentage ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Agent Addon commission -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Addon Commission</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="addon_base_commission" id="addon_base_commission" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('addon_base_commission') == 2) 
                                                            {{ 'selected' }} 
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }}
                                                        @elseif(isset($data->addon_base_commission_amount)  && $data->addon_base_commission_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('addon_base_commission') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->addon_base_commission_percentage)  && $data->addon_base_commission_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">

                                                <input class="form-control" name="addon_base_commission_amount" id="addon_base_commission_amount" 

                                                        @if(old('addon_base_commission_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('addon_base_commission_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->addon_base_commission_amount)  && $data->addon_base_commission_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('addon_base_commission_amount') : $data->addon_base_commission_amount ?? '' }}">

                                                <input class="form-control" name="addon_base_commission_percentage" id="addon_base_commission_percentage" 
                                                        @if(old('addon_base_commission_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->addon_base_commission_percentage)  && $data->addon_base_commission_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('addon_base_commission_percentage') : $data->addon_base_commission_percentage ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Agent base commission -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Agent Base Commission</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="agent_base_commission" id="agent_base_commission" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('agent_base_commission') == 2) 
                                                            {{ 'selected' }} 
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }}
                                                        @elseif(isset($data->agent_base_commission_amount)  && $data->agent_base_commission_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('agent_base_commission') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->agent_base_commission_percentage)  && $data->agent_base_commission_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">

                                                <input class="form-control" name="agent_base_commission_amount" id="agent_base_commission_amount" 

                                                        @if(old('agent_base_commission_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('agent_base_commission_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->agent_base_commission_amount)  && $data->agent_base_commission_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('agent_base_commission_amount') : $data->agent_base_commission_amount ?? '' }}">

                                                <input class="form-control" name="agent_base_commission_percentage" id="agent_base_commission_percentage" 
                                                        @if(old('agent_base_commission_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->agent_base_commission_percentage)  && $data->agent_base_commission_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('agent_base_commission_percentage') : $data->agent_base_commission_percentage ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Agent Addon commission -->
                                <!-- Agent Addon commission -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Agent Addon Commission</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="agent_addon_base_commission" id="agent_addon_base_commission" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('agent_addon_base_commission') == 2) 
                                                            {{ 'selected' }} 
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }}
                                                        @elseif(isset($data->agent_addon_base_commission_amount)  && $data->agent_addon_base_commission_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('agent_addon_base_commission') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->agent_addon_base_commission_percentage)  && $data->agent_addon_base_commission_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">

                                                <input class="form-control" name="agent_addon_base_commission_amount" id="agent_addon_base_commission_amount" 

                                                        @if(old('agent_addon_base_commission_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('agent_addon_base_commission_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->agent_addon_base_commission_amount)  && $data->agent_addon_base_commission_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('agent_addon_base_commission_amount') : $data->agent_addon_base_commission_amount ?? '' }}">

                                                <input class="form-control" name="agent_addon_base_commission_percentage" id="agent_addon_base_commission_percentage" 
                                                        @if(old('agent_addon_base_commission_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->agent_addon_base_commission_percentage)  && $data->agent_addon_base_commission_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('agent_addon_base_commission_percentage') : $data->agent_addon_base_commission_percentage ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2">Bonus</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="bonus" id="bonus" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('bonus') == 2) 
                                                            {{ 'selected' }}
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->bonus_amount)  && $data->bonus_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('bonus') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->bonus_percentage)  && $data->bonus_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>

                                                </select>
                                            </div>

                                            <div class="col-sm-2">

                                                <input class="form-control" name="bonus_amount" id="bonus_amount" 

                                                        @if(old('bonus_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('bonus_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->bonus_amount)  && $data->bonus_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('bonus_amount') : $data->bonus_amount ?? '' }}">

                                                <input class="form-control" name="bonus_percentage" id="bonus_percentage" 
                                                        @if(old('bonus_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->bonus_percentage)  && $data->bonus_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('bonus_percentage') : $data->bonus_percentage ?? '' }}">

                                            </div>

                                            <div class="col-sm-3">
                                                <input class="form-control" name="sale_amount_limit_bonus" id="sale_amount_limit_bonus" type="number" min="1" placeholder="Sale amount limit to get bonus"  step="any" value="{{ $errors->any() ? old('sale_amount_limit_bonus') : $data->sale_amount_limit_bonus ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Policy Fees</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <select name="policy_fees" id="policy_fees" class="form-control" >

                                                    {{-- <option value="">Select</option> --}}
                                                    <option value="2" 
                                                        @if(old('policy_fees') == 2) 
                                                            {{ 'selected' }}
                                                        @elseif($formType == 'add')
                                                            {{ 'selected' }}
                                                        @elseif(isset($data->policy_fees_amount)  && $data->policy_fees_amount > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Amount
                                                    </option>

                                                    <option value="1" 
                                                        @if(old('policy_fees') === '1') 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data->policy_fees_percentage)  && $data->policy_fees_percentage > 0)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >Percentage
                                                    </option>

                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                                <input class="form-control" name="policy_fees_amount" id="policy_fees_amount" 

                                                        @if(old('policy_fees_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif($formType == 'add' && !old('policy_fees_percentage'))
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->policy_fees_amount)  && $data->policy_fees_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                 type="number" placeholder="Amount"  step="any" value="{{ $errors->any() ? old('policy_fees_amount') : $data->policy_fees_amount ?? '' }}">

                                                <input class="form-control" name="policy_fees_percentage" id="policy_fees_percentage" 
                                                        @if(old('policy_fees_percentage')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->policy_fees_percentage)  && $data->policy_fees_percentage > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                type="number" placeholder="Percentage" step="any" value="{{ $errors->any() ? old('policy_fees_percentage') : $data->policy_fees_percentage ?? '' }}">

                                            </div>

                                            <div class="col-sm-2">
                                                <input class="form-control" name="upto_max_amount" id="upto_max_amount" 

                                                        @if(old('upto_max_amount')) 
                                                            {{ 'style=display:block' }}
                                                        @elseif(isset($data->upto_max_amount)  && $data->upto_max_amount > 0)
                                                            {{ 'style=display:block' }}
                                                        @else
                                                            {{ 'style=display:none' }}                                
                                                        @endif

                                                 type="number" placeholder="Upto Max"  step="any" value="{{ $errors->any() ? old('upto_max_amount') : $data->upto_max_amount ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- mobile --}}
                                <div class="form-group" id="only_tpl_after_years">
                                    <label class="control-label col-md-2">Only TPL after years</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <input class="form-control" placeholder="Only TPL after years" name="only_tpl_after_years" id="only_tpl_after_years" type="number" min="1" value="{{ $errors->any() ? old('only_tpl_after_years') : $data->only_tpl_after_years ?? '' }}">
                                            </div>

                                          <label class="hint" style="margin-left: 10px;margin-top: 10px;font-size: 9px;">   
                                            (After defined years only TPL plans of this provider will be shown)
                                          </label>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/partners')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
		
    $(document).ready(function() {
        
        pageSetUp();

        $("#formInsuranceProvider").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $("#base_commission").change(function(){
            
            var base_commissionVal = $(this).val();
            if(base_commissionVal == 1){
                $("#base_commission_percentage").show();
                $("#base_commission_amount").hide();

                $("#base_commission_percentage").val('');
                $("#base_commission_amount").val('');
            }
            else if(base_commissionVal == 2){
                $("#base_commission_percentage").hide();
                $("#base_commission_amount").show();

                $("#base_commission_percentage").val('');
                $("#base_commission_amount").val('');
            }
            // else{
            //     $("#base_commission_percentage").hide();
            //     $("#base_commission_amount").hide();
            //     $("#base_commission_percentage").val('');
            //     $("#base_commission_amount").val('');
            // }
        });

        $("#addon_base_commission").change(function(){
            
            var addon_base_commissionVal = $(this).val();
            if(addon_base_commissionVal == 1){
                $("#addon_base_commission_percentage").show();
                $("#addon_base_commission_amount").hide();

                $("#addon_base_commission_percentage").val('');
                $("#addon_base_commission_amount").val('');
            }
            else if(addon_base_commissionVal == 2){
                $("#addon_base_commission_percentage").hide();
                $("#addon_base_commission_amount").show();

                $("#addon_base_commission_percentage").val('');
                $("#addon_base_commission_amount").val('');
            }
            // else{
            //     $("#addon_base_commission_percentage").hide();
            //     $("#addon_base_commission_amount").hide();
            //     $("#addon_base_commission_percentage").val('');
            //     $("#addon_base_commission_amount").val('');
            // }
        });

        $("#agent_base_commission").change(function(){
            
            var agent_base_commissionVal = $(this).val();
            if(agent_base_commissionVal == 1){
                $("#agent_base_commission_percentage").show();
                $("#agent_base_commission_amount").hide();

                $("#agent_base_commission_percentage").val('');
                $("#agent_base_commission_amount").val('');
            }
            else if(agent_base_commissionVal == 2){
                $("#agent_base_commission_percentage").hide();
                $("#agent_base_commission_amount").show();

                $("#agent_base_commission_percentage").val('');
                $("#agent_base_commission_amount").val('');
            }
            // else{
            //     $("#agent_base_commission_percentage").hide();
            //     $("#agent_base_commission_amount").hide();
            //     $("#agent_base_commission_percentage").val('');
            //     $("#agent_base_commission_amount").val('');
            // }
        });

        $("#agent_addon_base_commission").change(function(){
            
            var agent_addon_base_commissionVal = $(this).val();
            if(agent_addon_base_commissionVal == 1){
                $("#agent_addon_base_commission_percentage").show();
                $("#agent_addon_base_commission_amount").hide();

                $("#agent_addon_base_commission_percentage").val('');
                $("#agent_addon_base_commission_amount").val('');
            }
            else if(agent_addon_base_commissionVal == 2){
                $("#agent_addon_base_commission_percentage").hide();
                $("#agent_addon_base_commission_amount").show();

                $("#agent_addon_base_commission_percentage").val('');
                $("#agent_addon_base_commission_amount").val('');
            }
            // else{
            //     $("#agent_addon_base_commission_percentage").hide();
            //     $("#agent_addon_base_commission_amount").hide();
            //     $("#agent_addon_base_commission_percentage").val('');
            //     $("#agent_addon_base_commission_amount").val('');
            // }
        });

        $("#bonus").change(function(){
            
            var bonusVal = $(this).val();
            if(bonusVal == 1){
                $("#bonus_percentage").show();
                $("#bonus_amount").hide();

                $("#bonus_percentage").val('');
                $("#bonus_amount").val('');
                $("#sale_amount_limit_bonus").val('');
            }
            else if(bonusVal == 2){
                $("#bonus_percentage").hide();
                $("#bonus_amount").show();

                $("#bonus_percentage").val('');
                $("#bonus_amount").val('');
                $("#sale_amount_limit_bonus").val('');
            }
            // else{
            //     $("#bonus_percentage").hide();
            //     $("#bonus_amount").hide();
            //     $("#bonus_percentage").val('');
            //     $("#bonus_amount").val('');
            // }
        });

        $("#policy_fees").change(function(){
            
            var policy_feesVal = $(this).val();
            if(policy_feesVal == 1){
                $("#policy_fees_percentage").show();
                $("#policy_fees_amount").hide();
                $("#upto_max_amount").show();

                $("#policy_fees_percentage").val('');
                $("#policy_fees_amount").val('');
                $("#upto_max_amount").val('');
            }
            else if(policy_feesVal == 2){
                $("#policy_fees_percentage").hide();
                $("#policy_fees_amount").show();
                $("#upto_max_amount").hide();

                $("#policy_fees_percentage").val('');
                $("#policy_fees_amount").val('');
                $("#upto_max_amount").val('');
            }
            // else{
            //     $("#policy_fees_percentage").hide();
            //     $("#policy_fees_amount").hide();
            //     $("#upto_max_amount").hide();
            //     $("#policy_fees_percentage").val('');
            //     $("#policy_fees_amount").val('');
            //     $("#upto_max_amount").val('');
            // }
        });


        $("#image").change(function(){
             //$(this).attr('src','/new/image/src.jpg');   
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgInsuranceProviders").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    //$("#imgCarMake").hide();
                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgInsuranceProviders").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        });
    });

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var name  = $.trim($("#name").val());        

        if (name == ''){
            $("#name").parent().find(".my-error").html('Please enter Partner Name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;
        }


        return is_valid;
    }
    
    function hideErrorDiv() {

        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
