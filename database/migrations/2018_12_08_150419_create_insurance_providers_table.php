<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// 
use App\Models\InsuranceProvider;
// 
class CreateInsuranceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('web_url')->nullable();
            $table->string('contact_number')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('premium_age_factor')->default(0)->nullable();
            $table->tinyInteger('premium_car_model_body_factor')->default(0)->nullable();
            $table->tinyInteger('premium_car_model_value_factor')->default(0)->nullable();
            $table->tinyInteger('third_party_available')->default(0)->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->timestamps();
        });

        $InsuranceProvider = new InsuranceProvider();
        $InsuranceProvider->name = 'THE ORIENTAL INSURANCE COMPANY LTD';
        $InsuranceProvider->premium_car_model_body_factor = 1;
        $InsuranceProvider->premium_car_model_value_factor = 1;
        $InsuranceProvider->third_party_available = 1;
        $InsuranceProvider->save();

        $InsuranceProvider = new InsuranceProvider();
        $InsuranceProvider->name = 'Union Insurance';
        $InsuranceProvider->premium_car_model_body_factor = 1;
        $InsuranceProvider->premium_car_model_value_factor = 1;
        $InsuranceProvider->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_providers');
    }
}
