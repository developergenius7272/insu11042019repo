@extends('layouts.admin.master')
{{-- Car model variants table view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarModelVariant" method="POST" @if($formType == 'edit') action="{{action('AdminCarModelVariantController@update', $data->id)}}" @else action="{{url('/admin/cars/car-model-variants')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Create a New car</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="selectCarMake">Select Car Make</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="selectCarMake" name="carMakeId">
                                            <option value="">Select Car Make</option>
                                            @if(isset($carMakes) && count($carMakes))
                                                @foreach($carMakes as $carMake)
                                                    <option value="{{$carMake->id}}" 
                                                        @if(old('carMakeId') == $carMake->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->car_make_id == $carMake->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($carMake->name)}}
                                                    </option>

                                                @endforeach
                                                    
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Car Model</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="selectCarModel" name="carModelId" data-selected="{{$data->car_model_id ?? 0}}">
                                            <option value="">Select Model</option>
                                            @if(isset($carModels) && count($carModels))
                                                @foreach($carModels as $carModel)
                                                    {{-- <option value="{{$carModel->id}}">{{strtoupper($carModel->name)}}</option> --}}

                                                    <option value="{{$carModel->id}}" 
                                                        @if(old('carModelId') == $carModel->id) 
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($carModel->name)}}
                                                    </option>

                                                @endforeach
                                                    <option value="new">Create New</option>
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Car Variant Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {{-- ui-autocomplete-loading --}}
                                                <input class="form-control " placeholder="e.g. 1.8 L, COUPE, 4 CLYNDER" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Specifications</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Select Body Type</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="selectBodyType" name="modelBodyTypeId">
                                            <option value="">Select Body Type</option>
                                            @if(isset($carBodies) && count($carBodies))
                                                @foreach($carBodies as $carBody)

                                                    <option value="{{$carBody->id}}" 
                                                        @if(old('modelBodyTypeId') == $carBody->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->car_model_body_type_id == $carBody->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($carBody->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>

                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Select Fuel</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="selectFuelType" name="modelFuelId">
                                            <option value="">Select Fuel</option>
                                            @if(isset($carFuels) && count($carFuels))
                                                @foreach($carFuels as $carFuel)

                                                    <option value="{{$carFuel->id}}" 
                                                        @if(old('modelFuelId') == $carFuel->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->car_model_fuel_id == $carFuel->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($carFuel->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>

                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Number of Cylinders</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Number of Cylinders" name="cylinders" id="cylinders" type="number" min="1" value="{{ $errors->any() ? old('cylinders') : $data->cylinders ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Engine Size</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Engine Size" name="engine_size" id="engine_size" value="{{ $errors->any() ? old('engine_size') : $data->engine_size ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Make Year</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Make Year" name="makeYear" id="makeYear" type="number" min="1901" max="2019" value="{{ $errors->any() ? old('makeYear') : $data->make_year ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                <div class="form-group" id="no_of_seats">
                                    <label class="control-label col-md-2">Number of Seats</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Number of Seats" name="no_of_seats" id="no_of_seats" type="number" min="1" value="{{ $errors->any() ? old('no_of_seats') : $data->no_of_seats ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-10">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgCarModelVariants" src='{{ asset('uploads/admin/car_model_variants').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                {{--  --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label">Standard / Superior</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="sub_type" 
                                                        @if(isset($data->sub_type) && $data->sub_type == 1)
                                                            {{ 'checked' }} 
                                                        @elseif($formType == 'add') 
                                                            {{ 'checked' }}                                             
                                                        @endif
                                                value="1">Standard
                                            </label>
                                            <label>
                                                <input type="radio" name="sub_type" 
                                                        @if(isset($data->sub_type) && $data->sub_type == 2)
                                                            {{ 'checked' }} 
                                                        @endif
                                                value="2">Superior
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label">Car Modal Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="carModalDescription" placeholder="Car Modal Description" rows="4"></textarea>
                                    </div>
                                </div> --}}
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/cars/car-model-variants')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $("#formCarModelVariant").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $("#selectCarMake").change(function() {
            $('#selectCarModel').data('selected',0);
            getCarModels();
        });

        if( $("#selectCarMake").val() > 0 ) {
            getCarModels();
        }

        $("#image").change(function(){
             //$(this).attr('src','/new/image/src.jpg');   
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgCarModelVariants").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    //$("#imgCarMake").hide();
                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgCarModelVariants").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        });

    });

    function getCarModels() {
        $("#selectCarModel").empty();
        $('#selectCarModel').append($('<option>', {value: '',text: 'Select Model'}));
        // 
        var carMakeId = $("#selectCarMake").val();
        $.ajax({
            type:'POST',
            url:'{{route('admin.getCarMakeModels')}}',
            data:{carMakeId:carMakeId},
            success:function(data){
                if( data.action == '1' ) {
                    for( i in data.data) {
                        var row = data.data[i];
                        // console.log(row.name)
                        var selectOption = $('<option>', {value: row.id,text: row.name});
                        $('#selectCarModel').append(selectOption);
                        if( $('#selectCarModel').data('selected') > 0 ) {
                            var selected = $('#selectCarModel').data('selected');
                            $('#selectCarModel').val(selected);
                        }
                    }
                }
            }
        });
    }

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var selectCarMake = $('#selectCarMake option:selected').index();
        var selectCarModel = $('#selectCarModel option:selected').index();
        var name  = $.trim($("#name").val());
        var selectBodyType = $('#selectBodyType option:selected').index();
        var selectFuelType = $('#selectFuelType option:selected').index();
        var cylinders  = $.trim($("#cylinders").val());
        var engine_size  = $.trim($("#engine_size").val());
        var makeYear  = $.trim($("#makeYear").val());
        

        if (selectCarMake <= 0){
            $("#selectCarMake").parent().find(".my-error").html('Please select Car Make').show();
            $('#selectCarMake').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        if (selectCarModel <= 0){
            $("#selectCarModel").parent().find(".my-error").html('Please select Car Model Body Type').show();
            $('#selectCarModel').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        if (name == ''){    
            $("#name").parent().find(".my-error").html('Please enter Car Variant Name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        if (selectBodyType <= 0){
            $("#selectBodyType").parent().find(".my-error").html('Please select Body Type').show();
            $('#selectBodyType').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        if (selectFuelType <= 0){
            $("#selectFuelType").parent().find(".my-error").html('Please select Fuel Type').show();
            $('#selectFuelType').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        
        if (cylinders.length > 0){

            if(!$.isNumeric( cylinders ) || cylinders < 1){
                $("#cylinders").parent().find(".my-error").html('Please enter Cylinder in numeric').show();
                $('#cylinders').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        else{
            $("#cylinders").parent().find(".my-error").html('Please enter Cylinder').show();
            $('#cylinders').closest('.form-group').addClass('has-error');
            is_valid = false;
        }
        
        if (engine_size == ''){    
            $("#engine_size").parent().find(".my-error").html('Please enter engine size').show();
            $('#engine_size').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (makeYear == ''){    
            $("#makeYear").parent().find(".my-error").html('Please enter make year').show();
            $('#makeYear').closest('.form-group').addClass('has-error');
            is_valid = false;
        }


        return is_valid;
    }
    
    function hideErrorDiv() {

        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
