@extends('layouts.master')
@section('content')
<!-- Main Title Section Start -->

@if(isset($banners))
  <section class="banner-section" style="background-image:url('{{ asset('uploads/homepage_banners').'/'.$banners->id.'_main' }}') ">
    <div class="container h-100">
      <div class="row align-items-center h-100">
        <div class="col-12">
          <div class="banner-text">
            <h2>{{ $banners->heading }}</h2>
            <p>{{$banners->tag_line}}</p>
            <a class="btn btn-primary" href="{{$banners->button_url}}" target="_blank">{{$banners->button_text}}</a> 
          </div>
        </div>
      </div>
    </div>
  </section>
@else
  <section class="banner-section">
    <div class="container h-100">
      <div class="row align-items-center h-100">
        <div class="col-12">
          <div class="banner-text">
            <h2>Lorem Ipsum Dolor</h2>
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
            <a class="btn btn-primary" href="{{route('start')}}" target="_blank">Get Started</a> 
          </div>
        </div>
      </div>
    </div>
  </section>
@endif

<!-- Main Title Section End -->

<!-- Company Logo Slider Start -->
<section class="company-logo-section">
  <div class="owl-carousel company-slider">
    @if($partners)

      @foreach($partners as $partner)
        @if($partner->has_logo == 1) 
          <div class="company-logo"> 
            <a href="JavaScript:void(0);" >
              <img src='{{ asset('uploads/admin/insurance_providers').'/'.$partner->id.'_main' }}' alt="<?php echo $partner->name;?>" width=110 height=50 title="{{ $partner->name }}"> 
            </a> 
          </div>
        @endif
      @endforeach

    @endif
  </div>
</section>

<!-- Who We Are Section Start -->
<section class="who-we-are-section">
  <div class="container">
    <div class="col-12">
      
      <div class="who-we-are-content">
        <h2>Who We Are</h2>
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justm. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p> 
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
      </div>
      <div class="row who-box-row">
        <div class="col-md-6">
          <div class="who-box-part" style="background-image: url(images/get-quote-bg.jpg);">
            <div class="who-btn"><a href="{{route('start')}}" target="_blank" class="btn btn-primary">Get Quote</a></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="who-box-part" style="background-image: url(images/get-car-insurance-bg.jpg);">
            <div class="who-btn"><a href="{{route('start')}}" target="_blank" class="btn btn-primary">Get Car Insurance</a></div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- Who We Are Section End -->

<!-- Why Insurance Section Start -->
<section class="why-insurance-section">
  <div class="container">
    <div class="col-12">

     <div class="why-insurance-inner">
        <h2>Why Car Insurance</h2>
     </div>
      
      <div class="row">
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">01</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">02</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">03</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- Why Insurance Section End -->
@endsection