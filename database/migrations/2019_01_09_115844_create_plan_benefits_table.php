<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_providers_id')->nullable();
            $table->year('year')->nullable();
            $table->datetime('effective_date')->nullable();
            $table->tinyInteger('plan_type')->default(0)->nullable()->comment = '0 = Comprehensive Plan / 1 = TPL Plan';
            $table->integer('benefit_masters_id')->nullable();
            $table->tinyInteger('included')->default(0)->nullable()->comment = '0 = Include, 1 = Not Include';
            $table->tinyInteger('single_or_multiple')->default(0)->nullable()->comment = '0 = Single, 1 = Multiple';
            $table->tinyInteger('percentage_or_amount')->after('single_or_multiple')->default(0)->nullable()->comment = '0 = N/A, 1 = Percentage, 2 = Amount';
            $table->string('no_of_years', 20)->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('sub_type')->after('description')->default(0)->nullable()->comment = '0 = N/A, 1 = Standard, 2 = Superior';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('plan_benefits');
    }
}
