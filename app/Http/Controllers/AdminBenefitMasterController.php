<?php

namespace App\Http\Controllers;
//
use App\Models\BenefitMaster;
use App\Models\InsuranceProvider;
use App\Models\PlanBenefit;
use App\Models\PlanBenefitMultiple;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Auth;

class AdminBenefitMasterController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Benefit Master'; 
        $this->data['breadcrumbs'][] = 'Benefit'; 
        $this->data['pageTitle'] = 'Benefit Master';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Benefit Master';
        $this->data['benefitMasters'] = BenefitMaster::all();
        return view('admin.benefit_master.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new Benefit Master';
        return view('admin.benefit_master.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $description = $request->description;
        $status = $request->status;

        request()->validate([
            'name' => 'required|unique:benefit_masters|min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);


        if(!empty( $name ))
        {

            $body = BenefitMaster::where('name',$name)->first();
            if( !$body ) {
                $body = new BenefitMaster();
                $body->name = $name;
                $body->description = $description;
                $body->status = $status;
                $body->created_by = $adminID;
                $body->modified_by = $adminID;
                $body->save();
                
                return redirect('/admin/partners/benefit-masters')->with('success', 'Information has been added');
            } else {
                return redirect('/admin/partners/benefit-masters')->with('error', 'Information has not been added');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit Benefit Master';
        $this->data['data'] = BenefitMaster::find($id);
        
        if( $this->data['data'] ) {
            // 
            return view('admin.benefit_master.create_edit',$this->data);
        } else {
            return redirect('/admin/partners/benefit-masters')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $description = $request->description;
        $status = $request->status;

        request()->validate([
            'name' => "required|unique:benefit_masters,name,$id|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        if(!empty($name ))
        {
            $body = BenefitMaster::where('name',$name)->where('id','<>',$id)->first();
            if( !$body ) {
                $body = BenefitMaster::find($id);

                if( $body ) {
                    $body->name = $name;
                    $body->description = $description;
                    $body->status = $status;
                    $body->modified_by = $adminID;
                    $body->save();

                    return redirect('/admin/partners/benefit-masters')->with('success', 'Car Makes updated successfully');
                } else {
                    return redirect('/admin/partners/benefit-masters')->with('error', 'Car Makes not updated');
                }
            } else {
                return redirect('/admin/partners/benefit-masters')->with('error', 'Car Makes not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
