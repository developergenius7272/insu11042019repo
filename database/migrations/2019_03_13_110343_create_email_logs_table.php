<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('email_template_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('status_event',255)->nullable();
            $table->string('status_reason',255)->nullable();
            $table->string('code',25)->nullable();
            $table->string('template_code',255)->nullable();
            $table->string('subject',255)->nullable();
            $table->string('from_name',255)->nullable();
            $table->string('from_email',255)->nullable();
            $table->string('to_name',255)->nullable();
            $table->string('to_email',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_logs');
    }
}
