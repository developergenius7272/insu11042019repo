<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCrossSellingProductIDInUserAgentAddonCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_agent_addon_commissions', function (Blueprint $table) {
            $table->integer('cross_selling_product_id')->after('user_id')->nullable();
            $table->dropColumn('insurance_provider_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
