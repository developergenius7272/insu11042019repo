@extends('layouts.master_insurance')
@section('content')
        <!-- Main Step Form Box Start -->
        <div class="select-policy-part">
          <div class="select-policy-inner-part">
            <div class="filter-option">

              <div>
                {{$carMinValue}}
                <input type="hidden" id="car_min_value" value="{{$carMinValue}}"/>
                <input type="hidden" id="car_max_value" value="{{$carMaxValue}}"/>
                <input type="number" style="width: 79px;padding-left: 5px;" id="car_filter_value" value="{{ $carValueInput }}" />
                <input type="hidden" id="carValueSelected" value="{{ $carValueInput }}"/>
                <button type="submit" id="filtered_car_value">filter</button>
                {{$carMaxValue}}

                <i class="fa fa-filter"></i>
                <select id="filter-value">
                  <option>Filter Results</option>
                  <option value="comp">Comprehensive Plans</option>
                  <option value="tpl">Third Party Plans</option>
                </select>
              </div>
            </div>
            
            <div class="policy-table-part clearfix">
            @if(($car_premium_details->isEmpty()) && ($carThirdPartyPremium->isEmpty()))
              {{-- <div style="">{{"No Benefits available"}}</div> --}}
            @else
              <div class="policy-features-part">
                <div class="policy-feature-heading"></div>
                <div class="policy-feature-list">
                  <ul>
                  <li>Car Value</li>
                    <li>Price (AED)</li>
                    <li>Exceed (AED)</li>
                    @foreach($benefit_name as $name)
                    <li>{{$name->name}}</li>
                    @endforeach
                  </ul>
                </div>
              </div>
              @endif
              <div class="policy-listing-part">
              @php  $loop_count = 0; 
                    $cnt_benefits = 0;
               @endphp
               @if(($car_premium_details->isEmpty()) && ($carThirdPartyPremium->isEmpty()))
               {{-- <div style="width:1594px;height:auto">{{"No Plans available"}}</div> --}}
               <div style="text-align:center;height:500px;">{{"There are no plans as per information provided by you."}}</div>
               @else
                
                <div class="owl-carousel select-policy-slider">
                @foreach($car_premium_details as $car_premium_detail)
                @php  if($loop->iteration == 1) $loop_count = ($loop->iteration) -1; else $loop_count++; @endphp
                  <!-- Standard -->
                   @if(!($car_premium_detail->car_model_sub_type == 2))
                      <?php $checkStatus = "no"; $extendedRate = 0;?>
                      @if(isset($car_premium_detail->available_for_months) && ($getMonths > ($car_premium_detail->available_for_months)))
                        <?php 
                          // // $available_months = ($car_premium_detail->available_for_months/12);
                          // // $avafilable_months++;
                          
                          $available_months = ceil($getMonths/12);

                          $check            = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('year_number');
                          $loadingRate      = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('rate');
                        ?>
                        @if($check->isEmpty())
                          @php --$loop_count; @endphp
                          @php goto nonagency; @endphp
                        @else
                          <?php 
                            $checkStatus = "yes"; 
                            $extendedRate = $loadingRate[0];
                          ?>
                        @endif
                      @endif
                      @if(!empty($car_premium_detail->insuranceProvider->only_tpl_after_years))
                      @if(($year <= $car_premium_detail->insuranceProvider->only_tpl_after_years) || ($car_premium_detail->insuranceProvider->only_tpl_after_years == 0)|| ($car_premium_detail->insuranceProvider->only_tpl_after_years == NULL))
                        <div class="policy-box agency comp" id="agency-{{ $car_premium_detail->insuranceProvider->id }}" data-type="agency">
                            <div class="policy-box-header">
                            <div class="policy-logo" data-id="{{$car_premium_detail->id}}"><span><img src="uploads/admin/insurance_providers/{{$car_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_premium_detail->insuranceProvider->name }}</div>
                            <button type="submit" class="btn btn-primary buynow" data-type="agency" data-insid="{{$car_premium_detail->insuranceProvider->id}}" data-policy="{{$car_premium_detail->id}}" />Buy Now </div>
                            <div>Standard</div>
                            <div class="policy-feature-list">
                            <ul>
                                <li>{{ $carValueInput }}</li>
                                <li>
                                   <div>Agency Rate</div>
                                    <div  id="agencyvalue_{{$car_premium_detail->insuranceProvider->id}}"> {{ $agencyPremiumValue = $car_premium_detail->agencyPremiumRate($carValueInput,$checkStatus,$extendedRate) }}
                                    </div>
                                    <input type="hidden" id="actual_agencyvalue_{{$car_premium_detail->insuranceProvider->id}}" value="{{ $agencyPremiumValue }}"> 
                                     <input type="hidden" id="rate" value="{{ $car_premium_detail->min_premium_agency_rate }}">                         
                                     <input type="hidden" id="minvalue" value="{{ $car_premium_detail->min_premium_agency }}">
                                     <input type="hidden" id="base_rate" value="{{ $car_premium_detail->insuranceProvider->base_commission_percentage }}">
                                     <input type="hidden" id="base_value" value="{{ $car_premium_detail->insuranceProvider->base_commission_amount }}">
                                </li>
                                <li>
                                    @php $val = $car_premium_detail->excessValue($carValueInput,$age,$no_of_seats)->pluck('excesValue');
                                    if( ! is_null( $val ) ) {
                                      $val = $val->first();
          
                                    }
                                  @endphp
                                   @if ( !is_null($val) )
                                  <div>{{$val}}
                                  <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="{{$val}}"/>
                                   </div>
                                   @else
                                  <div>N/A
                                  <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="NA"/></div>
                                  @endif
                                </li>
                              @php $cnt_ag_benefits=0;@endphp
                                @foreach($car_premium_detail->insuranceProvider->planBenefitsPartners->where('sub_type',1) as $planBenefit)
                                  @php $size_of_ag_benefits = sizeof($car_premium_detail->insuranceProvider->planBenefitsPartners); @endphp
                                @foreach($benefit_name as $benefit_id)
                                   @php $plan = $planBenefit->planBenefits->where('benefit_masters_id',$benefit_id->id)->first();@endphp
                                   
                                  @if ( $plan )
                                    <li class="benefit-row"> 
                                    @if(isset($plan->description) && ($plan->included == 2))
                                    <div>{{ $plan->description }}
                                    <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]"  data-benefit ="{{$benefit_id->id}}" value="{{ $plan->description }}"/></div>
                                    @elseif($plan->included == 2)
                                        <div class="fea-check"><input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "yes"/></div>
                                    @elseif(isset($plan->amount) && ($plan->included == 1) && ($plan->single_or_multiple != 2))
                                
                                        <div class="custom-control custom-checkbox">
                                            @if($plan->required == 1) 

                                            <input type="radio"  name="agency_{{$loop_count}}_{{$car_premium_detail->id}}_{{$benefit_id->id}}" data-val="agencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_agency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  data-benefit ="{{$benefit_id->id}}" value="{{$agency_val = $plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" checked="checked">

                                            @else

                                            <input type="checkbox"  name="agency_1" data-val="agencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_agency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  data-benefit ="{{$benefit_id->id}}" value="{{$agency_val = $plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}">
                                            @endif

                                            <label class="custom-control-label" for="customCheck_agency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"> AED {{$plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} </label>
                                        </div>
                                     @elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1)) 
                                            
                                          <select id="sel_agency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  class ="test"  data-benefit ="{{$benefit_id->id}}" data-col = "agencyvalue_{{$car_premium_detail->insuranceProvider->id}}">
                                               <option data-col="agencyvalue_{{$car_premium_detail->insuranceProvider->id}}"   data-benefit ="{{$benefit_id->id}}" value = "0" data-value = "0">select </option>
                                            @foreach($plan->planBenefitMultiple as $planMultiple)
                                                <option id="customCheck_agency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}_{{$planMultiple->id}}" data-benefit ="{{$benefit_id->id}}" data-value="{{$planMultiple->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" value="{{$planMultiple->id}}">AED{{$planMultiple->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} - {{$planMultiple->feature_description}}</option>
                                            @endforeach  
                                          </select>
                                    @else
                                        <div class="fea-minus">
                                        <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                    @endif
                                    </li>
                  
                                    @php $cnt_ag_benefits++;@endphp
                                    @else
                                    <li>
                                    <div class="fea-minus">
                                    <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                    </li>
                               @endif
                                @endforeach                         
                           @endforeach
                          
                              @if(($cnt_ag_benefits==0))
                                @php $cnt_ag_benefits=0;@endphp
                                @php $size_of_ag_benefits=0;@endphp
                                @php $set_benefit = $benefit_count; @endphp
                                              @php $left_ag_benefits = $set_benefit -$cnt_ag_benefits;@endphp
                                              @for ($i = 0; $i < $left_ag_benefits; $i++)
                                              <li>
                                                 <div class="fea-minus">
                                                 <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]"   value = "no"/></div>
                                                  </li>
                                              @endfor
                                    @endif 
                              </ul>
                         </div>
                      </div>
                      @endif
                      @endif
                        @php nonagency: @endphp
                        @if(!empty($car_premium_detail->insuranceProvider->only_tpl_after_years))
                        @if(($year <= $car_premium_detail->insuranceProvider->only_tpl_after_years) || ($car_premium_detail->insuranceProvider->only_tpl_after_years == 0)|| ($car_premium_detail->insuranceProvider->only_tpl_after_years == NULL))
                        
                        <div class="policy-box agency comp" id="nonagency-{{ $car_premium_detail->insuranceProvider->id }}" data-type="nonagency">
                            <div class="policy-box-header">
                            <div class="policy-logo" data-id="{{$car_premium_detail->id}}"><span><img src="uploads/admin/insurance_providers/{{$car_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_premium_detail->insuranceProvider->name }}</div>
                             <button type="submit" class="btn btn-primary buynow" data-type="nonagency" data-insid="{{$car_premium_detail->insuranceProvider->id}}" data-policy="{{$car_premium_detail->id}}"/>Buy Now </div>
                            <div>Standard</div>
                            <div class="policy-feature-list">
                            <ul>
                                <li>{{ $carValueInput }}</li>
                                <li>  
                                    <div>Non Agency Rate</div>
                                        <div id ="nonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}"> {{  $nonagencyPremiumValue = $car_premium_detail->nonagencyPremiumRate($carValueInput) }}</div>
                                        <input type="hidden" id="actual_nonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}" value="{{ $nonagencyPremiumValue }}">
                                         <input type="hidden" id="rate" value="{{ $car_premium_detail->min_premium_nonagency_rate }}">                         
                                     <input type="hidden" id="minvalue" value="{{ $car_premium_detail->min_premium_nonagency }}">
                                     <input type="hidden" id="base_rate" value="{{ $car_premium_detail->insuranceProvider->base_commission_percentage }}">
                                     <input type="hidden" id="base_value" value="{{ $car_premium_detail->insuranceProvider->base_commission_amount }}">
                                </li>
                                <li>
                                  @php $val = $car_premium_detail->excessValue($carValueInput,$age,$no_of_seats)->pluck('excesValue');
                                  if( ! is_null( $val ) ) {
                                      $val = $val->first();                                    
                                    }
                                  @endphp
                                   @if ( !is_null($val) )
                                  <div>{{$val}} <input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="{{$val}}"/> </div>
                                  @else
                                  <div>N/A
                                  <input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="NA"/></div>
                                  @endif     
                                </li>
                                @php ++$loop_count; @endphp
                                @php $cnt_non_benefits=0;@endphp
                                 @foreach($car_premium_detail->insuranceProvider->planBenefitsPartners->where('sub_type',1) as $planBenefit) 
                                  @php $size_of_nonag_benefits = sizeof($car_premium_detail->insuranceProvider->planBenefitsPartners); @endphp
                                 @foreach($benefit_name as $benefit_id)
                                 @php $plan = $planBenefit->planBenefits->where('benefit_masters_id',$benefit_id->id)->first();@endphp
                                  @if ( $plan )
                                    <li class="benefit-row">
                                    @if(isset($plan->description) && ($plan->included == 2))
                                    <div>{{ $plan->description }}
                                    <input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]"  data-benefit ="{{$benefit_id->id}}" value="{{ $plan->description }}"/></div>
                                    @elseif($plan->included == 2)
                                        <div class="fea-check">
                                        <input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "yes"/></div>
                                    @elseif(isset($plan->amount) && ($plan->included == 1) &&($plan->single_or_multiple != 2))

                                      <div class="custom-control custom-checkbox">
                                       @if($plan->required == 1)
                                            <input type="radio" name="nonagency_{{$loop_count}}_{{$car_premium_detail->id}}_{{$benefit_id->id}}" data-val="nonagencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}"  data-benefit ="{{$benefit_id->id}}" class="custom-control-input benefit-price" id="customCheck_nonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" checked="checked">
                                       @else 
                                            <input type="checkbox" data-val="nonagencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}"  data-benefit ="{{$benefit_id->id}}" class="custom-control-input benefit-price" id="customCheck_nonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}">
                                        @endif
                                        
                                            <label class="custom-control-label" for="customCheck_nonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"> AED {{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}</label>
                                        </div>
                                         @elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1)) 
                                            
                                          <select id="sel_nonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  class ="test" data-benefit ="{{$benefit_id->id}}" data-col = "nonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}">
                                               <option data-col="nonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}"   data-benefit ="{{$benefit_id->id}}" value = "0" data-value = "0">select </option>
                                            @foreach($plan->planBenefitMultiple as $planMultiple)
                                                <option id="customCheck_nonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}_{{$planMultiple->id}}" data-benefit ="{{$benefit_id->id}}" data-value="{{$planMultiple->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" value="{{$planMultiple->id}}">AED {{$planMultiple->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} - {{$planMultiple->feature_description}}</option>
                                            @endforeach  
                                          </select>
                                    
                                    @else
                                        <div class="fea-minus"><input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                    @endif
                                    </li>
                                    @php $cnt_non_benefits++;@endphp
                                    @else
                                    <li>
                                    <div class="fea-minus"><input type="hidden" class="hiddendata" name="nonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                    </li>
                                    @endif
                                @endforeach
                              
                            @endforeach
                             @if($cnt_non_benefits == 0 )
                                @php $cnt_non_benefits=0;@endphp
                                @php $size_of_nonag_benefits=0;@endphp
                                 @php $set_benefit = $benefit_count; @endphp
                                    @php $left_nonag_benefits = $set_benefit -$cnt_non_benefits;@endphp
                                      @for ($i = 0; $i < $left_nonag_benefits; $i++)
                                              <li>
                                                  <div class="fea-minus">
                                                  <input type="hidden" class="hiddendata" name="agency_{{$car_premium_detail->insuranceProvider->id}}[]"  value = "no"/></div>
                                                </li>
                                       @endfor    
                                @endif
                            </ul>
                            </div>
                        </div>
                      @endif
                      @endif
                      @else
                      <?php $checkSupStatus = "no"; $extendedSupRate = 0;?>
                        @if(isset($car_premium_detail->available_for_months) && ($getMonths > ($car_premium_detail->available_for_months)))
                         <?php 
                        // // $available_months = ($car_premium_detail->available_for_months/12);
                        // // $available_months++;

                        $available_months = ceil($getMonths/12);

                        $check = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('year_number');
                        $loadingRate = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('rate');
                      ?>
                       @if($check->isEmpty())
                         @php --$loop_count; @endphp
                        @php goto supnonagency; @endphp
                      @else
                      <?php 
                        $checkSupStatus = "yes"; 
                        $extendedSupRate = $loadingRate[0];
                      ?>
                      @endif
                      @endif
                      @if(!empty($car_premium_detail->insuranceProvider->only_tpl_after_years))
                      @if(($year <= $car_premium_detail->insuranceProvider->only_tpl_after_years) || ($car_premium_detail->insuranceProvider->only_tpl_after_years == 0)|| ($car_premium_detail->insuranceProvider->only_tpl_after_years == NULL))
                        <div class="policy-box agency comp" id="supagency-{{ $car_premium_detail->insuranceProvider->id }}"  data-type="supagency">
                            <div class="policy-box-header">
                            <div class="policy-logo" data-id="{{$car_premium_detail->id}}"><span><img src="uploads/admin/insurance_providers/{{$car_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_premium_detail->insuranceProvider->name }}</div>
                             <button type="submit" class="btn btn-primary buynow" data-type="supagency" data-insid="{{$car_premium_detail->insuranceProvider->id}}" data-policy="{{$car_premium_detail->id}}"/>Buy Now </div>
                            <div>Superior</div>
                            <div class="policy-feature-list">
                                <ul>
                                    <li>{{ $carValueInput }}</li>
                                    <li>
                                       <div>Agency Rate</div>
                                         <div  id="supagencyvalue_{{$car_premium_detail->insuranceProvider->id}}"> {{ $agencyPremiumValue = $car_premium_detail->agencyPremiumRate($carValueInput,$checkSupStatus,$extendedSupRate) }}</div>   
                                         <input type="hidden" id="actual_supagencyvalue_{{$car_premium_detail->insuranceProvider->id}}" value="{{ $agencyPremiumValue }}">
                                          <input type="hidden" id="rate" value="{{ $car_premium_detail->min_premium_agency_rate }}">                         
                                         <input type="hidden" id="minvalue" value="{{ $car_premium_detail->min_premium_agency }}"> 
                                         <input type="hidden" id="base_rate" value="{{ $car_premium_detail->insuranceProvider->base_commission_percentage }}">
                                     <input type="hidden" id="base_value" value="{{ $car_premium_detail->insuranceProvider->base_commission_amount }}">               
                                    </li>
                                    <li>
                                        @php $val = $car_premium_detail->excessValue($carValueInput,$age,$no_of_seats)->pluck('excesValue');
                                  if( ! is_null( $val ) ) {
                                      $val = $val->first();
                                    
                                    }
                                  @endphp
                                   @if ( !is_null($val) )
                                  <div>{{$val}}
                                  <input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="{{$val}}"/> </div>
                                   @else
                                  <div>N/A
                                   <input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="NA"/></div>
                                  @endif
                                          </li>
                                          @php $cnt_sup_benefits=0;@endphp 
                                         @foreach($car_premium_detail->insuranceProvider->planBenefitsPartners->where('sub_type',2) as $planBenefit) 
                                         @php $size_of_sup_benefits = sizeof($car_premium_detail->insuranceProvider->planBenefitsPartners); @endphp
                                       @foreach($benefit_name as $benefit_id)
                                        @php $plan = $planBenefit->planBenefits->where('benefit_masters_id',$benefit_id->id)->first();@endphp
                                        @if ( $plan )
                                            <li class="benefit-row"> 
                                            @if(isset($plan->description) && ($plan->included == 2))
                                            <div>{{ $plan->description }}
                                            <input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]"  data-benefit ="{{$benefit_id->id}}" value="{{ $plan->description }}"/></div>
                                            @elseif($plan->included == 2)
                                                <div class="fea-check"><input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "yes"/></div>
                                            @elseif(isset($plan->amount) && ($plan->included == 1) &&($plan->single_or_multiple != 2))
                                                <div class="custom-control custom-checkbox">
                                                @if($plan->required == 1) 
                                                  <input type="radio" name="supagency_{{$loop_count}}_{{$car_premium_detail->id}}_{{$benefit_id->id}}" data-val="supagencyvalue_" data-benefit ="{{$benefit_id->id}}" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_supagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" checked="checked">
                                                @else
                                                    <input type="checkbox" data-val="supagencyvalue_" data-benefit ="{{$benefit_id->id}}" data-id="{{$car_premium_detail->insuranceProvider->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_supagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}">
                                                  @endif
                                                    <label class="custom-control-label" for="customCheck_supagency_{{$car_premium_detail->insuranceProvider->id}}_{{$planBenefit->id}}">AED {{$plan->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} </label>
                                                </div>
                                               @elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1)) 
                                            
                                                <select id="sel_supagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  class ="test" data-benefit ="{{$benefit_id->id}}" data-col = "supagencyvalue_{{$car_premium_detail->insuranceProvider->id}}">
                                                    <option data-col="supagencyvalue_{{$car_premium_detail->insuranceProvider->id}}" data-benefit ="{{$benefit_id->id}}"  value = "0" data-value = "0">select </option>
                                                  @foreach($plan->planBenefitMultiple as $planMultiple)
                                                      <option id="customCheck_supagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}_{{$planMultiple->id}}"   data-benefit ="{{$benefit_id->id}}" data-value="{{$planMultiple->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" value="{{$planMultiple->id}}">AED {{$planMultiple->amount($agencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} - {{$planMultiple->feature_description}}</option>
                                                  @endforeach  
                                                </select>
                                                
                                            @else
                                                <div class="fea-minus"><input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                            @endif
                                            </li>
                                            @php $cnt_sup_benefits++;@endphp 
                                            @else
                                          <li>
                                          <div class="fea-minus">
                                          <input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                          </li>
                                            @endif
                                        @endforeach
                                      
                            @endforeach
                             @if($cnt_sup_benefits == 0)
                                          @php $cnt_sup_benefits=0;@endphp
                                          @php $size_of_sup_benefits=0;@endphp
                                          @php $set_benefit = $benefit_count; @endphp
                                              @php $left_sup_benefits = $set_benefit -$cnt_sup_benefits;@endphp
                                              @for ($i = 0; $i < $left_sup_benefits; $i++)
                                              <li>
                                                  <div class="fea-minus">
                                                  <input type="hidden" class="hiddendata" name="supagency_{{$car_premium_detail->insuranceProvider->id}}[]"  value = "no"/></div>
                                                  </li>
                                              @endfor
                                        @endif
                                </ul>
                            </div>
                        </div>
                        @endif
                        @endif
                        @php supnonagency: @endphp
                        @if(!empty($car_premium_detail->insuranceProvider->only_tpl_after_years))
                        @if(($year <= $car_premium_detail->insuranceProvider->only_tpl_after_years) || ($car_premium_detail->insuranceProvider->only_tpl_after_years == 0)|| ($car_premium_detail->insuranceProvider->only_tpl_after_years == NULL))
                        <div class="policy-box agency comp" id="supnonagency-{{ $car_premium_detail->insuranceProvider->id }}" data-type="supnonagency">
                            <div class="policy-box-header">
                            <div class="policy-logo" data-id="{{$car_premium_detail->id}}"><span><img src="uploads/admin/insurance_providers/{{$car_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_premium_detail->insuranceProvider->name }}</div>
                             <button type="submit" class="btn btn-primary buynow" data-type="supnonagency" data-insid="{{$car_premium_detail->insuranceProvider->id}}" data-policy="{{$car_premium_detail->id}}" />Buy Now </div>
                            <div>Superior</div>
                            <div class="policy-feature-list">
                                <ul>
                                    <li>{{ $carValueInput }}</li>
                                    <li>  
                                        <div>Non Agency Rate</div>                                    
                                            <div id ="supnonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}"> {{  $nonagencyPremiumValue = $car_premium_detail->nonagencyPremiumRate($carValueInput) }}</div>
                                            <input type="hidden" id="actual_supnonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}" value="{{ $nonagencyPremiumValue }}">
                                             <input type="hidden" id="rate" value="{{ $car_premium_detail->min_premium_nonagency_rate }}">                         
                                            <input type="hidden" id="minvalue" value="{{ $car_premium_detail->min_premium_nonagency }}">
                                            <input type="hidden" id="base_rate" value="{{ $car_premium_detail->insuranceProvider->base_commission_percentage }}">
                                     <input type="hidden" id="base_value" value="{{ $car_premium_detail->insuranceProvider->base_commission_amount }}">
                                    </li>
                                    <li>
                                     @php $val = $car_premium_detail->excessValue($carValueInput,$age,$no_of_seats)->pluck('excesValue');                                  
                                  if( ! is_null( $val ) ) {
                                      $val = $val->first();
                                    
                                    }
                                  @endphp
                                   @if ( !is_null($val) )
                                  <div>{{$val}} 
                                  <input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="{{$val}}"/></div>
                                   @else
                                  <div>N/A
                                  <input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="NA"/></div>
                                  @endif
                                    </li>
                                     @php ++$loop_count; @endphp 
                                     @php $cnt_sup_non_benefits=0;@endphp
                                        @foreach($car_premium_detail->insuranceProvider->planBenefitsPartners->where('sub_type',2) as $planBenefit)
                                         @php $size_of_supnonbenefits = sizeof($car_premium_detail->insuranceProvider->planBenefitsPartners); @endphp
                                         @foreach($benefit_name as $benefit_id)
                                            @php $plan = $planBenefit->planBenefits->where('benefit_masters_id',$benefit_id->id)->first();@endphp
                                          @if ( $plan )                       
                                           <li class="benefit-row">
                                                @if(isset($plan->description) && ($plan->included == 2))
                                                <div>{{ $plan->description }}
                                                 <input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]"  data-benefit ="{{$benefit_id->id}}" value="{{ $plan->description }}"/></div>
                                                @elseif($plan->included == 2)
                                                    <div class="fea-check"><input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "yes"/></div>
                                                @elseif(isset($plan->amount) && ($plan->included == 1) &&    ($plan->single_or_multiple != 2))        
                                                    <div class="custom-control custom-checkbox">
                                                      @if($plan->required == 1)

                                                        <input type="radio" name="supnonagency_{{$loop_count}}_{{$car_premium_detail->id}}_{{$benefit_id->id}}" data-val="supnonagencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}"  data-benefit ="{{$benefit_id->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_supnonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" checked="checked">
                                                      @else
                                                        <input type="checkbox" data-val="supnonagencyvalue_" data-id="{{$car_premium_detail->insuranceProvider->id}}"  data-benefit ="{{$benefit_id->id}}" data-cnt = "{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_supnonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}">
                                                      @endif
                                                        <label class="custom-control-label" for="customCheck_supnonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}">AED {{$plan->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} </label>
                                                    </div>
                                                  @elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1)) 
                                            
                                                <select id="sel_supnonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  class ="test"  data-benefit ="{{$benefit_id->id}}" data-col = "supnonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}">
                                                    <option data-col="supnonagencyvalue_{{$car_premium_detail->insuranceProvider->id}}" data-benefit ="{{$benefit_id->id}}"  value = "0" data-value = "0">select </option>
                                                  @foreach($plan->planBenefitMultiple as $planMultiple)
                                                      <option id="customCheck_supnonagency_{{$car_premium_detail->insuranceProvider->id}}_{{$plan->id}}_{{$planMultiple->id}}"  data-benefit ="{{$benefit_id->id}}" data-value="{{$planMultiple->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" value="{{$planMultiple->id}}"> AED {{$planMultiple->amount($nonagencyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} - {{$planMultiple->feature_description}}</option>
                                                  @endforeach  
                                                </select>
                                                @else
                                                    <div class="fea-minus"><input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                                @endif
                                           </li>
                                             @php $cnt_sup_non_benefits++;@endphp
                                             @else
                                            <li>
                                            <div class="fea-minus">
                                            <input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                            </li>
                                            @endif
                                        @endforeach
                                    
                                 @endforeach
                                   @if($cnt_sup_non_benefits == 0)
                                          @php $cnt_sup_non_benefits=0;@endphp
                                          @php $size_of_supnonbenefits=0;@endphp
                                         @php $set_benefit = $benefit_count; @endphp
                                              @php $left_sup_non_benefits = $set_benefit -$cnt_sup_non_benefits;@endphp
                                              @for ($i = 0; $i < $left_sup_non_benefits; $i++)
                                              <li>
                                                  <div class="fea-minus">
                                                  <input type="hidden" class="hiddendata" name="supnonagency_{{$car_premium_detail->insuranceProvider->id}}[]"  value = "no"/></div>
                                                  </li>
                                              @endfor
                                        @endif
                                        @endif
                                 </ul>
                            </div>
                        </div>
                        @endif
                      @endif
                    @endforeach

                  @foreach($carThirdPartyPremium as $car_thirdparty_premium_detail)
                     {{--  @if(!($car_premium_detail->car_model_sub_type == 2))  --}}
                      @php $loop_count = ($loop->iteration)+$loop_count; @endphp
                      @if(!empty($car_thirdparty_premium_detail->insuranceProvider->id))
                        <div class="policy-box agency tpl" id="thirdparty-{{ $car_thirdparty_premium_detail->insuranceProvider->id }}" data-type="thirdparty">
                            <div class="policy-box-header">
                            <div class="policy-logo"><span><img src="uploads/admin/insurance_providers/{{$car_thirdparty_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_thirdparty_premium_detail->insuranceProvider->name }}</div>
                             <button type="submit" class="btn btn-primary buynow" data-type="thirdparty" data-insid="{{$car_thirdparty_premium_detail->insuranceProvider->id}}" data-policy="{{$car_thirdparty_premium_detail->id}}" />Buy Now </div>
                            <div>Standard</div>
                            <div class="policy-feature-list">
                            
                    <ul>
                      <li>{{ $carValueInput }}</li>
                      <li>
                        <div>Third Party Rate</div>
                        <div  id="thirdpartyvalue_{{$car_thirdparty_premium_detail->insuranceProvider->id}}"> {{ $thirdpartyPremiumValue = $car_thirdparty_premium_detail->thirdPartyPremiumRate($carValueInput) }}</div>  
                        <input type="hidden" id="actual_thirdpartyvalue_{{$car_thirdparty_premium_detail->insuranceProvider->id}}" value="{{ $thirdpartyPremiumValue }}"> 
                         <input type="hidden" id="rate" value="{{ $car_thirdparty_premium_detail->rate }}">
                         <input type="hidden" id="base_rate" value="{{ $car_thirdparty_premium_detail->insuranceProvider->base_commission_percentage }}">
                                     <input type="hidden" id="base_value" value="{{ $car_thirdparty_premium_detail->insuranceProvider->base_commission_amount }}">
                      </li>
                      <li>
                       @php $val = $car_thirdparty_premium_detail->excessValue($carValueInput,$age,$no_of_seats)->pluck('excesValue');
                       //var_dump($val[0]);
                               if( ! is_null( $val ) ) {
                                      $val = $val->first();
                                    
                                    }
                                  @endphp
                                   @if ( !is_null($val) )
                                  <div>{{$val}} 
                                  <input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="{{$val}}"/></div>
                                  @else
                                  <div>N/A
                                  <input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]" data-benefit="90001" value="NA"/></div>
                                  @endif
                                  
                      </li>
                      
                      @foreach($car_thirdparty_premium_detail->insuranceProvider->thirdPartyplanBenefitsPartner as $planBenefit) 
                      
                      @php $size_of_benefits = sizeof($car_thirdparty_premium_detail->insuranceProvider->thirdPartyplanBenefitsPartner); @endphp
                      @php $cnt_benefits=0;@endphp
                      @foreach($benefit_name as $benefit_id)
                       @php $plan = $planBenefit->planBenefits->where('benefit_masters_id',$benefit_id->id)->first();@endphp
                        @if ( $plan )
                        <li class="benefit-row"> 
                        @if(isset($plan->description) && ($plan->included == 2))
                        <div>{{ $plan->description }}
                        <input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]"  data-benefit ="{{$benefit_id->id}}" value="{{ $plan->description }}"/></div>
                        @elseif($plan->included == 2)
                          <div class="fea-check"><input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "yes"/></div>
                        @elseif(isset($plan->amount) && ($plan->included == 1) && ($plan->single_or_multiple != 2))
                          <div class="custom-control custom-checkbox">
                           @if($plan->required == 1)
                            <input type="radio" name="thirdparty_{{$loop_count}}_{{$car_thirdparty_premium_detail->id}}_{{$benefit_id->id}}"  data-val="thirdpartyvalue_" data-id="{{$car_thirdparty_premium_detail->insuranceProvider->id}}" data-cnt ="{{$loop_count}}"  data-benefit ="{{$benefit_id->id}}" class="custom-control-input benefit-price" id="customCheck_thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($thirdpartyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" checked="checked">
                            @else
                            <input type="checkbox"  data-val="thirdpartyvalue_" data-id="{{$car_thirdparty_premium_detail->insuranceProvider->id}}" data-cnt ="{{$loop_count}}"  data-benefit ="{{$benefit_id->id}}" class="custom-control-input benefit-price" id="customCheck_thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$plan->id}}" value="{{$plan->amount($thirdpartyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}">
                            @endif
                            <label class="custom-control-label" for="customCheck_thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$plan->id}}"> AED {{$plan->amount($thirdpartyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}} </label>
                          </div>
                          @elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1)) 
                                            
                                                <select id="sel_thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$plan->id}}"  data-benefit ="{{$benefit_id->id}}" class ="test" data-col = "thirdpartyvalue_{{$car_thirdparty_premium_detail->insuranceProvider->id}}">
                                                    <option data-col="thirdpartyvalue_{{$car_thirdparty_premium_detail->insuranceProvider->id}}" data-benefit ="{{$benefit_id->id}}"  value = "0" data-value = "0">select </option>
                                                  @foreach($plan->planBenefitMultiple as $planMultiple)
                                                      <option id="customCheck_thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$plan->id}}_{{$planMultiple->id}}"  data-benefit ="{{$benefit_id->id}}" data-value="{{$planMultiple->amount($thirdpartyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}" value="{{$planMultiple->id}}"> AED {{$planMultiple->amount($thirdpartyPremiumValue,$no_of_seats,$plan->benefit_masters_id)}}- {{$planMultiple->feature_description}}</option>
                                                  @endforeach  
                                                </select>
                        @else
                          <div class="fea-minus"><input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                        @endif
                        </li>
                        
                         @php $cnt_benefits++;@endphp
                         @else
                                    <li>
                                    <div class="fea-minus">
                                    <input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]" data-benefit ="{{$benefit_id->id}}" value = "no"/></div>
                                    </li>
                        @endif
                      
                      @endforeach
                    
                    @endforeach
                    @if($cnt_benefits == 0)
                        @php $cnt_benefits=0;@endphp
                     
                          @php $size_of_benefits=0;@endphp
                      
                      @php $set_benefit = $benefit_count; @endphp
                     
                            @php $left_benefits = $set_benefit -$cnt_benefits;@endphp
                            @for ($i = 0; $i < $left_benefits; $i++)
                            <li>
                                <div class="fea-minus">
                                <input type="hidden" class="hiddendata" name="thirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}[]"  value = "no"/></div>
                                </li>
                            @endfor
                      @endif
                    </ul>

                      </div>
                    </div>
                @endif
                {{--  @else  --}}

                {{--  @php $loop_count = ($loop->iteration)+$loop_count; @endphp
                        <div class="policy-box agency" id="thirdparty-{{ $car_thirdparty_premium_detail->insuranceProvider->id }}" data-type="supthirdparty">
                            <div class="policy-box-header">
                            <div class="policy-logo"><span><img src="uploads/admin/insurance_providers/{{$car_premium_detail->insuranceProvider->id}}_main" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{ $car_thirdparty_premium_detail->insuranceProvider->name }}</div>
                            <a href="#" class="btn btn-primary buynow">Buy Now</a> </div>
                            <div>Standard</div>
                            <div class="policy-feature-list">
                            
                    <ul>
                      <li>{{ $carValueInput }}</li>
                      <li>
                        <div>Third Party Rate</div>
                        <div  id="supthirdpartyvalue_{{$car_thirdparty_premium_detail->insuranceProvider->id}}"> {{ $thirdpartyPremiumValue = $car_thirdparty_premium_detail->thirdPartyPremiumRate($carValueInput) }}</div>                                  
                      </li>
                      <li>
                        <div>Available for months(test): {{$car_thirdparty_premium_detail->available_for_months}} -- {{ $getMonths}}</div>
                      </li>
                      @foreach($car_thirdparty_premium_detail->insuranceProvider->thirdPartyplanBenefits as $planBenefit) 
                      
                        <li class="benefit-row"> 
                        @if(isset($planBenefit->description) && ($planBenefit->included == 2))
                        <div>{{ $planBenefit->description }}</div>
                        @elseif($planBenefit->included == 2)
                          <div class="fea-check"></div>
                        @elseif(isset($planBenefit->amount) && ($planBenefit->included == 1))
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox"  data-val="supthirdpartyvalue_" data-id="{{$car_thirdparty_premium_detail->insuranceProvider->id}}" data-cnt ="{{$loop_count}}" class="custom-control-input benefit-price" id="customCheck_supthirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$planBenefit->id}}" value="{{$planBenefit->amount($thirdpartyPremiumValue)}}">
                            <label class="custom-control-label" for="customCheck_supthirdparty_{{$car_thirdparty_premium_detail->insuranceProvider->id}}_{{$planBenefit->id}}">{{$planBenefit->amount($thirdpartyPremiumValue)}} AED</label>
                          </div>  
                        @else
                          <div class="fea-minus"></div>
                        @endif
                        </li>
                        
                      @endforeach
                    </ul>
                      </div>
                    </div>  --}}



                 {{--  @endif  --}}
                
                @endforeach
                  
                </div>
              </div>
              
                @endif
            </div>
            <div class="clear"></div>
            @if(($car_premium_details->isEmpty()) && ($carThirdPartyPremium->isEmpty()))
              </div>
              {{-- <div>{{"The prices you see here are exclusive of 5% VAT"}}</div> --}}
              <div class="policy-star-terms"></div>
            @else
              <div class="policy-star-terms">*The prices you see here are exclusive of 5% VAT</div>
            @endif
            {{--  <div class="btns-part">
              <button type="submit" class="btn btn-outline-light">Back</button>
            </div>  --}}
          </div>
        </div>
        <!-- Main Step Form Box End --> 
  <!-- Step Container End --> 
<!-- Main Container End --> 
@endsection
@section('script')

<script>
$(document).ready(function(){

// $(document).bind("contextmenu",function(e){
//   return false;
// });
//     $(window).keydown(function(event){
//     if(event.which === 123){
//        return false;
//      }else if (event.ctrlKey && event.shiftKey && event.keyCode === 73) {
//      return false;
//   }else if (event.ctrlKey && event.shiftKey && event.keyCode === 67) {
//      return false;
//   }else if (event.ctrlKey && event.shiftKey && event.keyCode=='i'.charCodeAt(0)) {
//      return false;
//   }else if (event.ctrlKey && event.keyCode == 'U'.charCodeAt(0)) {
//      return false;
//   }else if (event.ctrlKey || event.shiftKey){
//     return false;
//   }
//   else if (event.ctrlKey && event.shiftKey){
//     return false;
//   }
// });
//Select Policy Slider
$('.select-policy-slider').owlCarousel({
    items:6,
    autoplay:false,
    loop:false,
    dots:false,
    nav:true,
    navText: [
        "<img src='images/left-arrow.png' alt='Prev' />",
        "<img src='images/right-arrow.png' alt='Prev' />"
    ],
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:2,
        },
        768:{
            items:3,
        },
        992:{
            items:4,
        },
        1200:{
            items:5,
        },
        1400:{
            items:6,
        },
    }
});
  
  $('.policy-feature-list').find('input[type=radio]').each(function(){
      if($(this).is(':checked')) {
        var total;
        var benefitId = $(this).attr('id');
        
        var parentBenefitRow = $(this).parents('.policy-feature-list').parent('.policy-box').parent('.owl-item');
        // 
        var agencyType       = $(parentBenefitRow).find("div[class*='agency']").attr('data-type');
        var providerId       = $(parentBenefitRow).children("div[class*='agency']").attr('id').replace(agencyType +"-", "");
        var n                = benefitId.lastIndexOf('_');
        var result           = benefitId.substring(n + 1);
        var currentBenefit   = $("#customCheck_" + agencyType + "_" + providerId + "_" + result);
        total                = parseFloat($("#" + agencyType + "value_" + providerId ).text());
        total                += parseFloat(currentBenefit.val());
        $("#" + agencyType + "value_" + providerId ).text(total); 
      }
  })

  $('.policy-feature-list').on('change', '.benefit-price', function(event){
        event.preventDefault();
        var benefitVal = $(this).val();
        var benefitId;
        var total;
        var current_check =$(this).attr("data-cnt");
        var parentBenefitRows = $(this).parents('.policy-feature-list').parent('.policy-box').parent('.owl-item').parent('.owl-stage').children();
        if ( $(this).is(':checked') ) {
            
             for ( var i = 0; i < parentBenefitRows.length; i++ ) {
                var parentBenefitRow = parentBenefitRows[i];
                var agencyType = $(parentBenefitRow).children("div[class*='agency']").attr('data-type');
                var providerId = $(parentBenefitRow).children("div[class*='agency']").attr('id').replace(agencyType +"-", "");
                benefitId = $(this).attr('id');
                var n = benefitId.lastIndexOf('_');
                var result = benefitId.substring(n + 1);
                var currentBenefit = $("#customCheck_" + agencyType + "_" + providerId + "_" + result);
                
                    if (currentBenefit.is(':checked')  && (current_check == i)) {
                        total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                        total += parseFloat(currentBenefit.val());
                        
                        $("#" + agencyType + "value_" + providerId ).text(total); 
                    } else {
                        if((current_check != i)&& (currentBenefit.val() != null) && (!currentBenefit.is(':checked'))){
                            console.log("inside is checked else:" + i);
                            currentBenefit.prop("checked", true);
                            total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                            total += parseFloat(currentBenefit.val());
                            $("#" + agencyType + "value_" + providerId ).text(total);
                        } else {
                            total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                            $("#" + agencyType + "value_" + providerId ).text(total);
                        }
                    }
            }
        } else {
            for ( var i = 0; i < parentBenefitRows.length; i++ ) { 
                var parentBenefitRow = parentBenefitRows[i];
                var agencyType = $(parentBenefitRow).children("div[class*='agency']").attr('data-type');
                var providerId = $(parentBenefitRow).children("div[class*='agency']").attr('id').replace(agencyType +"-", "");
                benefitId = $(this).attr('id');
                var n = benefitId.lastIndexOf('_');
                var result = benefitId.substring(n + 1);
                var currentBenefit = $("#customCheck_" + agencyType + "_" + providerId + "_" + result);                
                     if ( (!currentBenefit.is(':checked')) && (current_check == i)) {
                        total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                        total -= parseFloat(currentBenefit.val());
                        $("#" + agencyType + "value_" + providerId ).text(total);
                     } else {
                        if(current_check != i && (currentBenefit.val() != null) && (currentBenefit.is(':checked')) && (currentBenefit.is(':checkbox')) ){
                             currentBenefit.prop("checked", false);
                            total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                            total -= parseFloat(currentBenefit.val());
                            $("#" + agencyType + "value_" + providerId ).text(total); 
                             }    else {
                                total = parseFloat($("#" + agencyType + "value_" + providerId ).text());
                                $("#" + agencyType + "value_" + providerId ).text(total); 
                            }
                        } 
                
            }
        }
    });

  /*$(".tpl").removeClass("hidden");
  $(".comp").removeClass("hidden");*/

    $('body').on('change', "#filter-value", function(event){
      event.preventDefault();

        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            console.log(optionValue);
            if(optionValue && optionValue == "comp"){
              $(".comp").parent().removeClass("hidden");
              $(".tpl").parent().addClass("hidden");
            }
            else if(optionValue && optionValue == "tpl"){
                $(".tpl").parent().removeClass("hidden");
                $(".comp").parent().addClass("hidden");
            }
            else{
              $(".tpl").parent().removeClass("hidden");
              $(".comp").parent().removeClass("hidden");
            }
        });
    });

  $('.policy-feature-list').on('change', '.test', function(event){
		event.preventDefault();
      var jqThis = $(this);
      var value  = $(this).data("col");
      var tot    = parseFloat($("#" + value ).text());
      if ((jqThis.data("prev") !== undefined)){
        tot -=parseFloat(jqThis.data("prev"));
      }
      // jqThis.data("prev",jqThis.val());
      console.log('-------- ' + jqThis.find(':selected').data('value'));
      jqThis.data("prev",jqThis.find(':selected').data('value'));
      var current = parseFloat(jqThis.data("prev"));
      tot += current;
      $("#" + value).text(tot); 
	});


 

});

$(document).ready(function(){
    
 $(".buynow").click(function(event){ 
   event.preventDefault();
    var provider_id  = $(this).attr("data-insid");
    var agency_type = $(this).attr("data-type");
    var policy = $(this).attr("data-policy");
    var type_value;
    var type_value;
    var main_parent = agency_type+"-"+provider_id;
    var carValueSelected = $("#carValueSelected").val();
    var second_array = {};
    var premium = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#"+agency_type+"value_"+provider_id).html();
    var actual_premium = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#actual_"+agency_type+"value_"+provider_id).val();
    var select_value = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find(".test option:selected");
    var actual_rate = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#rate").val();
     var min_agency_value = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#minvalue").val();
     var base_rate = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#base_rate").val();
     var base_value = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("#base_value").val();
    $.each(select_value, function(){            
                  second_array[$(this).attr("data-benefit")]=$(this).val();
              });
    var checked_value = $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find(".benefit-price:checked");
    $.each(checked_value, function(){            
      second_array[$(this).attr("data-benefit")]=$(this).val();
    });
    var values =  $("#"+main_parent).children(".policy-feature-list").children("ul").children("li").find("input[name='"+agency_type+"_"+provider_id+"[]']");
    $.each(values, function () {
        var mm = $(this).attr("data-benefit");
        second_array[mm] = $(this).val();    
    });
      if((agency_type == "agency") || (agency_type == "supagency")){
        type_value = 0;
      }else if(agency_type == "thirdparty"){
        type_value = 2;
      }else{
        type_value = 1;
      }
      //alert(actual_rate+"---"+min_agency_value+"--"+type_value+"--"+base_rate+"--"+base_value);
          $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
              type: "post",
              url: '{{url('/paymentdata')}}',
              data: {second_array:second_array,premium:premium,provider_id:provider_id,actual_premium:actual_premium,carValueSelected:carValueSelected,actual_rate:actual_rate,min_agency_value:min_agency_value,base_rate:base_rate,base_value:base_value,type_value:type_value,policy:policy
              },
              success: function(res) {
                  window.location.href = '{{url('/paymentpage')}}';
                  }
                
              });
   });


    $('body').on('click', "#filtered_car_value", function(event){
      event.preventDefault();
      var car_min_value = parseFloat($("#car_min_value").val());
      var car_max_value = parseFloat($("#car_max_value").val());
      var carInputValue = parseFloat($("#car_filter_value").val());
        if (( carInputValue >= car_min_value) && ( carInputValue <= car_max_value )) {
            $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
              type: "post",
              url: '{{url('/filtercarvalue')}}',
              data: {carInputValue:carInputValue
              },
              success: function(res) {
                  window.location.href = '{{url('/premiumplans')}}';
                  } 
              });
        }else{
            alert("car value sholud be within range");
        }
    });
});
</script> 
@endsection
 