<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonCategory extends Model
{
	protected $fillable = ['name','status'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function crossSellingProducts() {
        return $this->hasMany('App\Models\CrossSellingProducts', 'addon_category_id', 'id');
    }
}
