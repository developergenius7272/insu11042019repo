@extends('layouts.admin.master')
{{-- Frontend users view --}}
@section('content')
<div class="row">
    
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Agent</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" method="POST" @if($formType == 'edit') action="{{action('AdminUserController@updateAgent', ['id' => $data->id])}}" @else action="{{route('admin.add.agent')}}" @endif>
                             @csrf
                             @if($formType == 'edit')
                                {{-- <input name="_method" type="hidden" value="PATCH"> --}}
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Agent</legend>
                                {{-- user name --}}
                                <div class="form-group" id="name">
                                    <label class="control-label col-md-2">Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{$data->name ?? ''}}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- email --}}
                                <div class="form-group" id="email">
                                    <label class="control-label col-md-2">Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Email" name="email" id="email" type="text" value="{{$data->email ?? ''}}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Mobile --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Mobile</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Mobile" name="mobile" id="mobile" type="text" value="{{$data->mobile ?? ''}}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Address --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Address</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">


                                                <textarea class="form-control" placeholder="Textarea" name="address" id="address" rows="3" value="{{$data->address ?? ''}}"></textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Gender --}}
                                <!--legend>Gender</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Gender</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
      <input type="radio" name="gender" <?php if (isset($data->gender) && $data->gender==1) echo "checked";?> value="1">Male
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="gender" <?php if (isset($data->gender) && $data->gender==2) echo "checked";?> value="2">Female
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="gender" <?php if (isset($data->gender) && $data->gender==3) echo "checked";?> value="3">Other  
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                {{-- Marital Status --}}
                                <!--legend>Marital Status</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Marital Status</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
      <input type="radio" name="maritalstatus" <?php if (isset($data->maritalstatus) && $data->maritalstatus=="Married") echo "checked";?> value="Married">Married
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="maritalstatus" <?php if (isset($data->maritalstatus) && $data->maritalstatus=="Unmarried") echo "checked";?> value="Unmarried">Unmarried
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="maritalstatus" <?php if (isset($data->maritalstatus) && $data->maritalstatus=="Divorced") echo "checked";?> value="Divorced">Divorced
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="maritalstatus" <?php if (isset($data->maritalstatus) && $data->maritalstatus=="Widow") echo "checked";?> value="Widow">Widow
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
      <input type="radio" name="maritalstatus" <?php if (isset($data->maritalstatus) && $data->maritalstatus=="Single") echo "checked";?> value="Single">Single
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Date Of Birth</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                    <input type="text" name="dob" value="{{$data->dob ?? ''}}" placeholder="Date of birth" class="form-control datepicker" data-dateformat="yy/mm/dd">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        <?php
                                                            $statusArry = array( 0  => 'Inactive' , 1 => 'Active' );
                                                            foreach( $statusArry as $key => $value ) {
                                                            
                                                            if( $key == $data->status ) {
                                                        ?>
                                                        <option class="form-control"  selected  value="<?php echo $key; ?>" > <?php echo $value; ?> </option>
                                                        <?php
                                                            }
                                                            else {
                                                        ?>
                                                        <option class="form-control"  value="<?php echo $key;  ?>" > <?php echo $value; ?> </option>
                                                        <?php
                                                            }
                                                            }
                                                        ?>
                                                        
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0">Inactive</option>
                                                        <option  class="form-control" value="1">Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        

                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.agents')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
		
    $(document).ready(function() {
        
        pageSetUp();
        // 
        $("#selectCarMake").change(function() {
            $('#selectCarModel').data('selected',0);
            getCarModels();
        });

        if( $("#selectCarMake").val() > 0 ) {
            getCarModels();
        }
    });

    function getCarModels() {
        $("#selectCarModel").empty();
        $('#selectCarModel').append($('<option>', {value: '',text: 'Select Model'}));
        // 
        if($("#selectCarMake").val() == 'new') {
            $("#selectCarMakeNew").removeClass('hide');
        } else {
            var carMakeId = $("#selectCarMake").val();
            $.ajax({
                type:'POST',
                url:'/admin/cars/make/models',
                data:{carMakeId:carMakeId},

                success:function(data){
                    if( data.action == '1' ) {
                        for( i in data.data) {
                            var row = data.data[i];
                            // console.log(row.name)
                            var selectOption = $('<option>', {value: row.id,text: row.name});
                            $('#selectCarModel').append(selectOption);
                            if( $('#selectCarModel').data('selected') > 0 ) {
                                var selected = $('#selectCarModel').data('selected');
                                $('#selectCarModel').val(selected);
                            }
                        }
                    }
                }
            });
        }
    }

</script>
@endsection
