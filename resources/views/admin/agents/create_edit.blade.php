@extends('layouts.admin.master')
{{-- Frontend users view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Agent</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formAgent" method="POST" @if($formType == 'edit') action="{{action('AdminUserController@updateAgent', ['id' => $data->id])}}" @else action="{{route('admin.add.agent')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                {{-- <input name="_method" type="hidden" value="PATCH"> --}}
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Agent</legend>
                                {{-- user name --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- email --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Email" name="email" id="email" type="text" value="{{ $errors->any() ? old('email') : $data->email ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Mobile --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Mobile</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Mobile" name="mobile" id="mobile" type="number" min="1" value="{{ $errors->any() ? old('mobile') : $data->mobile ?? '' }}">


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Address --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Address</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">


                                                <textarea class="form-control" placeholder="Textarea" name="address" id="address" rows="3">{{ $errors->any() ? old('address') : $data->address ?? '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Gender --}}
                                <!--legend>Gender</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Gender</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender'))
                                                        @if(old('gender') == 1)
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 1)
                                                            {{ 'checked' }}
                                                    @elseif($formType == 'add') 
                                                        {{ 'checked' }}
                                                    @endif
                                                value="1">Male
                                            </label>
                                            <label>
                                                <input type="radio" name="gender"
                                                    @if(old('gender'))
                                                        @if(old('gender') == 2) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 2)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="2">Female
                                            </label>
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender') )
                                                        @if(old('gender') == 3) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 3)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="3">Other  
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                {{-- Marital Status --}}
                                <!--legend>Marital Status</legend-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Marital Status</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Married") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Married")
                                                        {{ 'checked' }} 
                                                    @elseif($formType == 'add') 
                                                        {{ 'checked' }}
                                                    @endif

                                                value="Married">Married
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Unmarried") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Unmarried")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Unmarried">Unmarried
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Divorced") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Divorced")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Divorced">Divorced
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Widow") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Widow")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Widow">Widow
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Single") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Single")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Single">Single
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Date Of Birth</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                @if($formType == 'edit')
                                                    {{-- <input type="text" name="dob" value="{{ isset($data->dob) ? sqlDateDMY($data->dob,'-') : '' }}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly> --}}

                                                    <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : (isset($data->dob) ? $data->dob->format('d/m/Y') : '') }}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly>

                                                @else
                                                    {{-- <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : '' }}" placeholder="Date of birth" class="form-control datetimepicker"  autocomplete="off" readonly> --}}

                                                    <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : ''}}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly>

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                            </fieldset>

                    <br>
                    {{-- Base commission --}}
                    <fieldset>
                        <legend>Base Commission</legend>

                        @if($formType == 'add')
                        <div class="row myrow">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3"><label class="control-label">Insurance Provider</label></div>      
                                    <div class="col-md-2" ><label class="control-label">Type</label></div> 
                                    <div class="col-md-1" ><label class="control-label">Amount</label></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row rowsMultiple myrow">
                        @if(isset($partners) && count($partners))
                            @php $allpartnerCounter = 0; @endphp
                            @foreach($partners as $allpartner)
                                <div class="row addMoreDiv">
                                    <div class="col-md-3">
                                        <select class="form-control insuranceProviderId" name="insuranceProviderId[]">
                                            <option value="">Select Partner</option>
                                            
                                            @foreach($partners as $partner)
                                                <option value="{{$partner->id}}" 
                                                    @if(old('insuranceProviderId') == $partner->id) 
                                                        {{ 'selected' }} 
                                                    @elseif($allpartner->id == $partner->id)
                                                        {{ 'selected' }} 
                                                    @endif
                                                    >{{strtoupper($partner->name)}}
                                                </option>
                                            @endforeach
                                        
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        
                                        <select name="perAmount[]" data-ids="perAmount" class="form-control perAmount" >

                                            <option value="1" @if($allpartner->agent_base_commission_percentage > 0) selected @endif>Percent</option>

                                            <option value="2" @if($allpartner->agent_base_commission_amount > 0) selected @endif>Amount</option>
                                        </select>
                                    </div>

                                    <div class="col-md-1">
                                        <input class="form-control amount" placeholder="" type="number" step="any" min="0" name="amount[]" value="{{$allpartner->agent_base_commission_percentage > 0 ? $allpartner->agent_base_commission_percentage : $allpartner->agent_base_commission_amount}}">
                                    </div>
                                    {{-- @if($allpartnerCounter > 0)
                                        <div class="col-md-2"><a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a></div>
                                    @endif --}}

                                </div>
                            @php $allpartnerCounter++; @endphp
                            @endforeach
                        @endif
                        </div> 

                        <div class="row myrow">
                            {{-- <span class="addOneMore" onclick="clone_div()"><i class="fa fa-plus-square"></i>Add New</span>
                            <br> --}}
                            <span class="my-error" style='color:#b94a48;display:none' >*</span>
                        </div> 

                        

                        @else

                            @if($countUserAgentCommissions)

                                <div class="row myrow aas">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"><label class="control-label">Insurance Provider</label></div>      
                                            <div class="col-md-2"><label class="control-label">Type</label></div> 
                                            <div class="col-md-1"><label class="control-label">Amount</label></div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row rowsMultiple myrow">
                                
                                    @php ($i = 1) @endphp

                                    @foreach($userAgentCommissions as $userAgentCommission)

                                    <div class="row addMoreDiv">
                                        <div class="col-md-3">
                                            <select class="form-control insuranceProviderId" name="insuranceProviderId[]">
                                                <option value="">Select Partner</option>
                                                @if(isset($partners) && count($partners))
                                                    @foreach($partners as $partner)
                                                        <option value="{{$partner->id}}" 
                                                            @if(old('insuranceProviderId') == $partner->id) 
                                                                {{ 'selected' }} 
                                                            @elseif(isset($userAgentCommission) && $userAgentCommission->insurance_provider_id == $partner->id)
                                                                {{ 'selected' }} 
                                                            @endif
                                                            >{{strtoupper($partner->name)}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div class="col-md-2">

                                            <select name="perAmount[]" data-ids="perAmount" class="form-control perAmount" >

                                                <option value="1" 
                                                    @if(isset($userAgentCommission->percentage_or_amount) && $userAgentCommission->percentage_or_amount == '1')
                                                        {{ 'selected' }}                                             
                                                    @endif
                                                    >Percent
                                                </option>

                                                <option value="2" 
                                                    @if(isset($userAgentCommission->percentage_or_amount) && $userAgentCommission->percentage_or_amount == '2')
                                                        {{ 'selected' }}                                             
                                                    @endif
                                                    >Amount
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-md-1">
                                            <input class="form-control amount" placeholder="" type="number" step="any" min="0" name="amount[]" value="{{ $userAgentCommission->amount }}">
                                        </div>

                                        {{-- @if ( $i > 1 )
                                            <div class="col-md-2">
                                                <a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a>
                                            </div>
                                        @endif --}}

                                    </div>

                                    @php ($i++) @endphp
                                    @endforeach


                                
                                </div>

                                <div class="row myrow">
                                    {{-- <span class="addOneMore" onclick="clone_div()"><i class="fa fa-plus-square"></i>Add New</span>
                                    <br> --}}
                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                </div>


                            @endif

                        @endif


                    </fieldset>
                    <br>
                    <br>

                    {{-- Addon Base commission by cross selling products --}} 
                    <fieldset>
                        <legend>Addon Base Commission</legend>
                       <div class="row myrow">
                           <div class="col-md-12">
                               <div class="row">
                                   <div class="col-md-3"><label class="control-label">Insurance Provider</label></div>
                                   <div class="col-md-3"><label class="control-label">Cross Selling Product</label></div>      
                                   <div class="col-md-2" ><label class="control-label">Type</label></div> 
                                   <div class="col-md-1" ><label class="control-label">Amount</label></div>
                               </div>
                           </div>
                       </div>
                       <hr>

                       @if($formType == 'add')
                           <div class="row addon_rowsMultiple myrow">
                           @if(isset($products) && count($products))

                               @foreach($products as $product)
                                   <div class="row addMoreDiv">
                                        
                                        <div class="col-md-3">
                                            <label class="control-label">{{ isset($product->insuranceProvider->name) ? $product->insuranceProvider->name : '' }}</label>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">{{ $product->name }}</label>
                                            <input type="hidden" name="cross_selling_product_id[]" value="{{ $product->id }}">
                                        </div>

                                        <div class="col-md-2">
                                            <select class="form-control percentage_or_amount" name="percentage_or_amount[]">

                                                <option value="1" selected
                                                   
                                                    >Percent
                                                </option>

                                                <option value="2"  @if(isset($product->insuranceProvider->agent_addon_base_commission_amount) && $product->insuranceProvider->agent_addon_base_commission_amount > 0)
                                                        {{ 'selected' }}                                             
                                                    @endif >Amount</option>

                                            </select>
                                        </div>
                                        
                                        {{-- value="{{$product->insuranceProvider->agent_addon_base_commission_amount >0 ? $product->insuranceProvider->agent_addon_base_commission_amount : $product->insuranceProvider->agent_addon_base_commission_percentage }}" --}}

                                        <div class="col-md-1">
                                            <input class="form-control amount_addon" placeholder="" type="number" step="any" min="0" name="amount_addon[]" 
                                                
                                                value="{{isset($product->insuranceProvider) ? ($product->insuranceProvider->agent_addon_base_commission_amount >0 ? $product->insuranceProvider->agent_addon_base_commission_amount : $product->insuranceProvider->agent_addon_base_commission_percentage) : '' }}"
                                            >
                                        </div>

                                   </div>

                               @endforeach
                           @endif
                           </div> 


                       @else

                            @if($countUserAgentAddonCommissions)

                                <div class="row addon_rowsMultiple myrow">

                                    @foreach($userAgentAddonCommissions as $userAgentAddonCommission)
                                        <div class="row addMoreDiv">
                                            <div class="col-md-3">
                                                <label class="control-label">{{ $userAgentAddonCommission->insurance_providers_name }}</label>
                                            </div>

                                            <div class="col-md-3">
                                                <label class="control-label">{{ $userAgentAddonCommission->cross_selling_product_name }}</label>
                                                <input type="hidden" name="cross_selling_product_id[]" value="{{ $userAgentAddonCommission->cross_selling_product_id }}">
                                            </div>

                                            <div class="col-md-2">
                                                <select class="form-control percentage_or_amount" name="percentage_or_amount[]">


                                                   <option value="1" 
                                                       @if(isset($userAgentAddonCommission->percentage_or_amount) && $userAgentAddonCommission->percentage_or_amount == '1')
                                                           {{ 'selected' }}                                             
                                                       @endif
                                                       >Percent
                                                   </option>

                                                   <option value="2" 
                                                       @if(isset($userAgentAddonCommission->percentage_or_amount) && $userAgentAddonCommission->percentage_or_amount == '2')
                                                           {{ 'selected' }}                                             
                                                       @endif
                                                       >Amount
                                                   </option>

                                                </select>
                                            </div>

                                            <div class="col-md-1">
                                                <input class="form-control amount_addon" placeholder="" type="number" step="any" min="0" name="amount_addon[]" value="{{$userAgentAddonCommission->amount }}">
                                            </div>
                                        </div>
                                    @endforeach

                                </div>


                            @endif

                       @endif



                    </fieldset>
                    <br>
                    <br>

                            {{--  --}}
                            <fieldset>
                                <legend> Passport Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Passport Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Passport Number" name="passport_number" id="passport_number" type="text" value="{{ $errors->any() ? old('passport_number') : $data->passport_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="passport_expiry_date" value="{{ $errors->any() ? old('passport_expiry_date') : $data->passport_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> License Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">License Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="License Number" name="license_number" id="license_number" type="text" value="{{ $errors->any() ? old('license_number') : $data->license_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input type="text" name="license_expiry_date" value="{{ $errors->any() ? old('license_expiry_date') : $data->license_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> UAE ID Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">UAE ID</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="UAE ID" name="uae_id" id="uae_id" type="text" value="{{ $errors->any() ? old('uae_id') : $data->uae_id ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="uae_id_expiry_date" value="{{ $errors->any() ? old('uae_id_expiry_date') : $data->uae_id_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> Other Details</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-4">
                                        {{-- <input type="file" class="btn btn-default" id="image" name="image"> --}}
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgAgents" src='{{ asset('uploads/admin/agents').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.agents')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>

<style type="text/css">
    .row.myrow {
        margin-left: 97px;
        margin-top: 12px;
    }
</style>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 
        $('.datetimepicker').datetimepicker({
            //format:'Y-m-d',
            // format:'d-m-Y',
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });

        $("#base_commission").change(function(){
            
            var base_commissionVal = $(this).val();
            if(base_commissionVal == 1){
                $("#base_commission_percentage").show();
                $("#base_commission_amount").hide();

                $("#base_commission_percentage").val('');
                $("#base_commission_amount").val('');
            }
            else if(base_commissionVal == 2){
                $("#base_commission_percentage").hide();
                $("#base_commission_amount").show();

                $("#base_commission_percentage").val('');
                $("#base_commission_amount").val('');
            }
        });

        $("#image").change(function(){
 
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgAgents").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgAgents").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        });

        $("#formAgent").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $('body').on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); 
        })
    });

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid                    = true;
        // 
        var name                        = $.trim($("#name").val());
        var email                       = $.trim($("#email").val());
        // 
        var lstInsuranceProviderId      = $('.insuranceProviderId');
        var lstPerAmount                = $('.perAmount');
        var lstAmount                   = $('.amount');
        // 
        var lstAddonInsuranceProviderId = $('.addonInsuranceProviderId');
        var lstAddonAmount              = $('.addonAmount');
        var lstAddonAmount              = $('.amount_addon');

        if (name == ''){

            $("#name").parent().find(".my-error").html('Please enter name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;

        }        
        if (email == ''){

            $("#email").parent().find(".my-error").html('Please enter email address').show();
            $('#email').closest('.form-group').addClass('has-error');
            is_valid = false;

        }
        else{
            if(!validateEmail(email)){
                $("#email").parent().find(".my-error").html('Please enter valid email address').show();
                $('#email').closest('.form-group').addClass('has-error');
                is_valid = false;                
            }
        }

        var i;
        var values = [];
        for(i=0; i<lstInsuranceProviderId.length; i++)
        {
            //duplicate checking
            var select = lstInsuranceProviderId[i];
            if(values.indexOf(select.value) > -1) {
                //alert('duplicate exists'+select.value); return; //if duplicates found we are returning. without save and no need to continue.
                //alert('duplicate exists'+select.value); 
                //$(".addOneMore").parent().find(".my-error").html('duplicate exists'+select.value).show();
                $(".addOneMore").parent().find(".my-error").html('You cannot select same provider twice').show();
                is_valid = false;
                break;
            }
            else {
                values.push(select.value);
            }

            if($(lstInsuranceProviderId[i]).prop("selectedIndex")>0)
            {
                if($(lstPerAmount[i]).prop("selectedIndex")<0 ){

                    $(".addOneMore").parent().find(".my-error").html('Please select percentage or amount').show();
                    is_valid = false;
                    break;
                }
                /*
                if($.trim( $(lstAmount[i]).val()).length==0 ){

                    $(".addOneMore").parent().find(".my-error").html('Please fill amount').show();
                    is_valid = false;
                    break;
                }
                else{
                    
                    if(!$.isNumeric( $(lstAmount[i]).val() ) || $(lstAmount[i]).val() < 1){

                        $(".addOneMore").parent().find(".my-error").html('Please fill amount in numeric').show();
                        is_valid = false;
                    }
                }
                */
                // if ($.trim( $(lstAmount[i]).val()).length > 0){

                //     if(!$.isNumeric( $.trim( $(lstAmount[i]).val()) ) || $.trim( $(lstAmount[i]).val()) < 1){
                //         $(".addOneMore").parent().find(".my-error").html('Please fill amount in numeric').show();
                //         is_valid = false;
                //     }
                // }

            }

        }

        // Addon
        /*
        var addonValues = [];
        for(i=0; i<lstAddonInsuranceProviderId.length; i++)
        {
            //duplicate checking
            var select = lstAddonInsuranceProviderId[i];
            if(addonValues.indexOf(select.value) > -1) {
                //alert('duplicate exists'+select.value); return; //if duplicates found we are returning. without save and no need to continue.
                //alert('duplicate exists'+select.value); 
                //$(".addOneMore").parent().find(".my-error").html('duplicate exists'+select.value).show();
                $(".addOneMoreAddon").parent().find(".my-error").html('You cannot select same provider twice').show();
                is_valid = false;
                break;
            }
            else {
                addonValues.push(select.value);
            }

            if($(lstAddonInsuranceProviderId[i]).prop("selectedIndex")>0)
            {
                if($(lstAddonAmount[i]).prop("selectedIndex")<0 ){

                    $(".addOneMoreAddon").parent().find(".my-error").html('Please select percentage or amount').show();
                    is_valid = false;
                    break;
                }

                if($.trim( $(lstAmount[i]).val()).length==0 ){

                    $(".addOneMoreAddon").parent().find(".my-error").html('Please fill amount').show();
                    is_valid = false;
                    break;
                }
                else{
                    
                    if(!$.isNumeric( $(lstAmount[i]).val() ) || $(lstAmount[i]).val() < 1){

                        $(".addOneMoreAddon").parent().find(".my-error").html('Please fill amount in numeric').show();
                        is_valid = false;
                    }
                }
            }
        }
        */

        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
    function validateEmail(email)
    {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function clone_div(){
    var counter = 1;

        counter++;

        var apndPlace = $('.rowsMultiple');
        
        var str = '';

        str += '<div class="row addMoreDiv forremove_'+ counter +'">';

            str+= '<div class="col-md-3">';
                str += '<select class="form-control insuranceProviderId" name="insuranceProviderId[]">'
                    str += '<option value="">Select Partner</option>';
                    @if(isset($partners) && count($partners))
                        @foreach($partners as $partner)
                            str += '<option value="{{$partner->id}}"> ';
                               
                                str += '{{strtoupper($partner->name)}}';
                            str += '</option>';
                        @endforeach
                    @endif
                str += '</select>';
            str += '</div>';

            str += '<div class="col-md-2">';
                str += '<select name="perAmount[]" data-ids="perAmount" class="form-control perAmount" >';
                    str += '<option value="1">Percent</option>';
                    str += '<option value="2" >Amount</option>';
                str += '</select>';
            str += '</div>';

            str += '<div class="col-md-1">';
                str += '<input class="form-control amount" placeholder="" type="number" step="any" min="0" name="amount[]" value="">';
            str += '</div>';

            str += '<div class="col-md-2">';
                str += '<a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a>';
            str += '</div>';

        str += '</div>';

        $(apndPlace).append(str);

        $(apndPlace).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); 
        })
    }
    function clone_div_addon(){
    var counter = 1;

        counter++;

        var apndPlace = $('.addon_rowsMultiple');
        
        var str = '';

        str += '<div class="row addMoreDiv forremove_'+ counter +'">';

            str+= '<div class="col-md-3">';
                str += '<select class="form-control addonInsuranceProviderId" name="addonInsuranceProviderId[]">'
                    str += '<option value="">Select Partner</option>';
                    @if(isset($partners) && count($partners))
                        @foreach($partners as $partner)
                            str += '<option value="{{$partner->id}}"> ';
                               
                                str += '{{strtoupper($partner->name)}}';
                            str += '</option>';
                        @endforeach
                    @endif
                str += '</select>';
            str += '</div>';

            str += '<div class="col-md-2">';
                str += '<select name="addonAmount[]" data-ids="addonAmount" class="form-control addonAmount" >';
                    str += '<option value="1">Percent</option>';
                    str += '<option value="2" >Amount</option>';
                str += '</select>';
            str += '</div>';

            str += '<div class="col-md-1">';
                str += '<input class="form-control amount_addon" placeholder="" type="number" step="any" min="0" name="amount_addon[]" value="">';
            str += '</div>';

            str += '<div class="col-md-2">';
                str += '<a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a>';
            str += '</div>';

        str += '</div>';

        $(apndPlace).append(str);

        $(apndPlace).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').parent('div').remove(); 
        })
    }
</script>
@endsection
