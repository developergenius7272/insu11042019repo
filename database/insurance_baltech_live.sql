-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2020 at 07:48 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insurance_baltech_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `additionalcovers`
--

CREATE TABLE `additionalcovers` (
  `id` int(10) UNSIGNED NOT NULL,
  `cross_selling_product_id` int(11) DEFAULT NULL,
  `insurance_provider_id` int(11) DEFAULT 0,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` double DEFAULT 0,
  `fee_type` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `fee` decimal(8,2) DEFAULT 0.00,
  `fee_value` decimal(8,2) DEFAULT 0.00,
  `vat_value` decimal(8,2) DEFAULT 0.00,
  `premium_value` decimal(8,2) DEFAULT NULL,
  `benefits` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sum_insured` decimal(8,2) DEFAULT NULL,
  `site_commission_percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `site_commission_amount` double DEFAULT 0,
  `site_commission_value` double DEFAULT 0,
  `agent_id` int(11) DEFAULT 0,
  `agent_commission_percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `agent_commission_amount` double DEFAULT 0,
  `agent_commission_value` double DEFAULT 0,
  `total_premium` decimal(8,2) DEFAULT NULL,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0 COMMENT '0 = Pending, 1 = Processing, 2 = Issued'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `additionalcovers`
--

INSERT INTO `additionalcovers` (`id`, `cross_selling_product_id`, `insurance_provider_id`, `name`, `vat`, `fee_type`, `fee`, `fee_value`, `vat_value`, `premium_value`, `benefits`, `order_id`, `created_at`, `updated_at`, `sum_insured`, `site_commission_percentage_or_amount`, `site_commission_amount`, `site_commission_value`, `agent_id`, `agent_commission_percentage_or_amount`, `agent_commission_amount`, `agent_commission_value`, `total_premium`, `policy_number`, `issue_date`, `start_date`, `end_date`, `status`) VALUES
(1, 1, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '450555.00', NULL, 16, '2019-04-02 02:17:32', '2019-04-02 02:17:32', '350000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(2, 1, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '450.00', NULL, 17, '2019-04-02 02:22:40', '2019-04-02 02:22:40', '350000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(3, 1, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '999999.99', NULL, 18, '2019-04-02 03:29:57', '2019-04-02 03:29:57', '350000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(4, 2, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '340.00', NULL, 19, '2019-04-02 06:14:05', '2019-04-02 06:14:05', '300000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(5, 2, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '340.00', NULL, 38, '2019-04-03 03:49:33', '2019-04-03 03:49:33', '300000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(6, 2, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '340.00', NULL, 39, '2019-04-03 03:53:06', '2019-04-03 03:53:06', '300000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(7, 2, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '340.00', NULL, 41, '2019-04-03 04:04:20', '2019-04-03 04:04:20', '300000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(8, 2, 0, NULL, 0, 0, '0.00', '0.00', '0.00', '340.00', NULL, 63, '2019-04-03 07:24:59', '2019-04-03 07:24:59', '300000.00', 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0),
(9, 1, 15, 'apro', NULL, 1, NULL, '0.00', '0.00', '220.00', NULL, 81, '2019-04-08 04:28:16', '2019-04-08 04:28:16', '220.00', 2, 0, 0, 0, 0, 0, 0, '220.00', NULL, NULL, NULL, NULL, 0),
(11, 4, 15, 'apro3', 6, 2, '201.00', '201.00', '36.06', '601.00', NULL, 83, '2019-04-08 05:41:07', '2019-04-08 05:41:07', '501.00', 2, 150, 150, 0, 0, 0, 0, '838.06', NULL, NULL, NULL, NULL, 0),
(13, 1, 15, 'apro', NULL, 1, NULL, '0.00', '0.00', '220.00', NULL, 85, '2019-04-08 06:34:46', '2019-04-08 06:34:46', '220.00', 2, 150, 150, 0, 2, 15, 15, '220.00', NULL, NULL, NULL, NULL, 0),
(14, 4, 15, 'apro3', 6, 2, '201.00', '201.00', '36.06', '601.00', NULL, 85, '2019-04-08 06:34:46', '2019-04-08 06:34:46', '501.00', 2, 150, 150, 146, 2, 15, 15, '838.06', NULL, NULL, NULL, NULL, 0),
(15, 2, 1, 'apro2', NULL, 1, '3.00', '1.02', '0.00', '34.00', NULL, 86, '2019-04-08 07:20:32', '2019-04-08 07:20:32', NULL, 2, 150, 150, 205, 1, 441, 661.5, '35.02', NULL, NULL, NULL, NULL, 0),
(16, 4, 15, 'apro3', 6, 2, '201.00', '201.00', '36.06', '601.00', NULL, 86, '2019-04-08 07:20:32', '2019-04-08 07:20:32', '501.00', 2, 150, 150, 205, 2, 15, 15, '838.06', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `addon_categories`
--

CREATE TABLE `addon_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addon_categories`
--

INSERT INTO `addon_categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Accidental', 1, '2019-04-03 02:05:56', '2019-04-03 02:31:13'),
(2, 'Zero Depreciation Cover', 1, '2019-04-03 02:18:01', '2019-04-03 02:31:09'),
(3, 'Roadside Assistance', 1, '2019-04-03 07:31:37', '2019-04-03 07:31:37'),
(4, 'Invoice Cover', 1, '2019-04-03 07:31:37', '2019-04-03 07:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `addon_docs`
--

CREATE TABLE `addon_docs` (
  `id` int(10) UNSIGNED NOT NULL,
  `additionalcover_id` int(11) DEFAULT NULL,
  `docname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `addon_orders`
--

CREATE TABLE `addon_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_order_id` int(11) NOT NULL,
  `total_premium` decimal(8,2) DEFAULT NULL,
  `vat` decimal(8,2) DEFAULT NULL,
  `vat_amount` decimal(8,2) DEFAULT NULL,
  `policy_fees` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addon_orders`
--

INSERT INTO `addon_orders` (`id`, `user_id`, `parent_order_id`, `total_premium`, `vat`, `vat_amount`, `policy_fees`, `created_at`, `updated_at`) VALUES
(1, 125, 16, '473092.75', '5.00', '22527.75', '10.00', '2019-04-02 02:17:32', '2019-04-02 02:17:32'),
(2, 126, 17, '482.50', '5.00', '22.50', '10.00', '2019-04-02 02:22:40', '2019-04-02 02:22:40'),
(3, 127, 18, '999999.99', '5.00', '225007.00', '10.00', '2019-04-02 03:29:57', '2019-04-02 03:29:57'),
(4, 128, 19, '367.00', '5.00', '17.00', '10.00', '2019-04-02 06:14:05', '2019-04-02 06:14:05'),
(8, 179, 63, '367.00', '5.00', '17.00', '10.00', '2019-04-03 07:24:59', '2019-04-03 07:24:59'),
(9, 208, 81, '241.00', '5.00', '11.00', '10.00', '2019-04-08 04:28:16', '2019-04-08 04:28:16'),
(11, 210, 83, '641.05', '5.00', '30.05', '10.00', '2019-04-08 05:41:07', '2019-04-08 05:41:07'),
(13, 212, 85, '882.05', '5.00', '41.05', '20.00', '2019-04-08 06:34:46', '2019-04-08 06:34:46'),
(14, 213, 86, '686.75', '5.00', '31.75', '20.00', '2019-04-08 07:20:32', '2019-04-08 07:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin@app.com', '$2y$10$/Ul/9Dl1UhtY7T.K8ZjeTurFlj.0hEB3NvGF8/YK.aOWVxg9ZdXyW', 'TyyyNnw9Uj9wUGsB7BUd36R64OMW0OFwGYquuwM2xPQwjyRXg1Z2OSD0CUBS', NULL, NULL),
(2, 'abc@gmail.com', '$2y$10$QZ56AFPr4hhDcLpBdMIfEuFCXsJqulT7PDRdCXhki8U24PngYKeVS', 'aZQooeEfvU2BdhCNegigsKcbIuR3reMwarShTO0BMtClGnKSWDb8aDI2Epd5', '2018-12-15 00:42:28', '2018-12-15 00:42:28'),
(3, 'testadmin@insureonline.com', '$2y$10$c.dgYdVb7y3GcwE6dCFekupAecgS/uC.sxivKfR7UNGOSCGh9mGTS', 'DmdqB1i5LutFS1ZevimMmmdVU40nU1sQHelLHoVj1ZJJZUYm73RbaIGxpzIt', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agent_users`
--

CREATE TABLE `agent_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_current_agent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agent_users`
--

INSERT INTO `agent_users` (`id`, `agent_id`, `user_id`, `is_current_agent`, `created_at`, `updated_at`) VALUES
(1, 146, 117, 1, '2019-04-02 01:02:21', '2019-04-02 01:02:21'),
(2, 146, 118, 1, '2019-04-02 01:16:33', '2019-04-02 01:16:33'),
(3, 146, 119, 1, '2019-04-02 01:19:49', '2019-04-02 01:19:49'),
(4, 146, 120, 1, '2019-04-02 01:24:07', '2019-04-02 01:24:07'),
(5, 146, 121, 1, '2019-04-02 01:31:53', '2019-04-02 01:31:53'),
(6, 146, 122, 1, '2019-04-02 02:01:33', '2019-04-02 02:01:33'),
(7, 146, 123, 1, '2019-04-02 02:04:59', '2019-04-02 02:04:59'),
(8, 146, 124, 1, '2019-04-02 02:12:00', '2019-04-02 02:12:00'),
(9, 146, 125, 1, '2019-04-02 02:17:31', '2019-04-02 02:17:31'),
(10, 146, 126, 1, '2019-04-02 02:22:40', '2019-04-02 02:22:40'),
(11, 146, 127, 1, '2019-04-02 03:29:57', '2019-04-02 03:29:57'),
(12, 146, 128, 1, '2019-04-02 06:14:05', '2019-04-02 06:14:05'),
(13, 146, 129, 1, '2019-04-02 06:42:39', '2019-04-02 06:42:39'),
(14, 146, 131, 1, '2019-04-02 06:49:25', '2019-04-02 06:49:25'),
(15, 146, 132, 1, '2019-04-02 06:50:31', '2019-04-02 06:50:31'),
(16, 146, 133, 1, '2019-04-02 06:51:48', '2019-04-02 06:51:48'),
(17, 146, 134, 1, '2019-04-02 06:52:54', '2019-04-02 06:52:54'),
(18, 146, 135, 1, '2019-04-02 06:58:07', '2019-04-02 06:58:07'),
(19, 146, 136, 1, '2019-04-02 07:00:29', '2019-04-02 07:00:29'),
(20, 146, 137, 1, '2019-04-02 07:26:36', '2019-04-02 07:26:36'),
(21, 146, 138, 1, '2019-04-02 07:29:32', '2019-04-02 07:29:32'),
(22, 146, 139, 1, '2019-04-02 07:33:24', '2019-04-02 07:33:24'),
(23, 146, 140, 1, '2019-04-02 07:36:51', '2019-04-02 07:36:51'),
(24, 146, 142, 1, '2019-04-02 07:38:41', '2019-04-02 07:38:41'),
(25, 146, 143, 1, '2019-04-02 07:41:59', '2019-04-02 07:41:59'),
(26, 146, 144, 1, '2019-04-02 08:09:01', '2019-04-02 08:09:01'),
(27, 146, 145, 1, '2019-04-02 08:09:50', '2019-04-02 08:09:50'),
(28, 146, 146, 1, '2019-04-02 08:13:01', '2019-04-02 08:13:01'),
(29, 146, 147, 1, '2019-04-02 08:22:04', '2019-04-02 08:22:04'),
(30, 146, 148, 1, '2019-04-02 08:23:15', '2019-04-02 08:23:15'),
(31, 146, 150, 1, '2019-04-03 03:49:33', '2019-04-03 03:49:33'),
(32, 146, 151, 1, '2019-04-03 03:53:06', '2019-04-03 03:53:06'),
(33, 146, 152, 1, '2019-04-03 03:55:29', '2019-04-03 03:55:29'),
(34, 146, 153, 1, '2019-04-03 04:04:20', '2019-04-03 04:04:20'),
(35, 146, 154, 1, '2019-04-03 04:48:35', '2019-04-03 04:48:35'),
(36, 146, 155, 1, '2019-04-03 04:52:55', '2019-04-03 04:52:55'),
(37, 146, 156, 1, '2019-04-03 04:54:50', '2019-04-03 04:54:50'),
(38, 146, 157, 1, '2019-04-03 04:56:04', '2019-04-03 04:56:04'),
(39, 146, 158, 1, '2019-04-03 04:56:29', '2019-04-03 04:56:29'),
(40, 146, 159, 1, '2019-04-03 04:57:22', '2019-04-03 04:57:22'),
(41, 146, 160, 1, '2019-04-03 05:11:11', '2019-04-03 05:11:11'),
(42, 146, 161, 1, '2019-04-03 05:25:13', '2019-04-03 05:25:13'),
(43, 146, 162, 1, '2019-04-03 05:26:33', '2019-04-03 05:26:33'),
(44, 146, 163, 1, '2019-04-03 05:59:26', '2019-04-03 05:59:26'),
(45, 146, 164, 1, '2019-04-03 06:07:32', '2019-04-03 06:07:32'),
(46, 146, 165, 1, '2019-04-03 06:09:59', '2019-04-03 06:09:59'),
(47, 146, 166, 1, '2019-04-03 06:11:58', '2019-04-03 06:11:58'),
(48, 146, 167, 1, '2019-04-03 06:26:02', '2019-04-03 06:26:02'),
(49, 146, 168, 1, '2019-04-03 06:30:16', '2019-04-03 06:30:16'),
(50, 146, 170, 1, '2019-04-03 06:39:39', '2019-04-03 06:39:39'),
(51, 146, 173, 1, '2019-04-03 07:07:40', '2019-04-03 07:07:40'),
(52, 146, 174, 1, '2019-04-03 07:13:54', '2019-04-03 07:13:54'),
(53, 146, 175, 1, '2019-04-03 07:14:39', '2019-04-03 07:14:39'),
(54, 146, 176, 1, '2019-04-03 07:17:37', '2019-04-03 07:17:37'),
(55, 146, 177, 1, '2019-04-03 07:19:58', '2019-04-03 07:19:58'),
(56, 146, 178, 1, '2019-04-03 07:21:36', '2019-04-03 07:21:36'),
(57, 146, 179, 1, '2019-04-03 07:24:59', '2019-04-03 07:24:59'),
(58, 146, 180, 1, '2019-04-03 07:34:29', '2019-04-03 07:34:29'),
(59, 146, 181, 1, '2019-04-03 07:37:02', '2019-04-03 07:37:02'),
(60, 146, 182, 1, '2019-04-03 07:38:53', '2019-04-03 07:38:53'),
(61, 146, 185, 1, '2019-04-03 08:40:50', '2019-04-03 08:40:50'),
(62, 146, 186, 1, '2019-04-03 08:42:15', '2019-04-03 08:42:15'),
(63, 146, 187, 1, '2019-04-03 08:47:29', '2019-04-03 08:47:29'),
(64, 146, 188, 1, '2019-04-03 08:49:57', '2019-04-03 08:49:57'),
(65, 146, 189, 1, '2019-04-03 08:53:31', '2019-04-03 08:53:31'),
(66, 146, 191, 1, '2019-04-03 08:54:05', '2019-04-03 08:54:05'),
(67, 146, 192, 1, '2019-04-03 09:00:34', '2019-04-03 09:00:34'),
(68, 146, 193, 1, '2019-04-03 09:01:52', '2019-04-03 09:01:52'),
(69, 146, 194, 1, '2019-04-03 09:02:49', '2019-04-03 09:02:49'),
(70, 146, 197, 1, '2019-04-04 03:30:33', '2019-04-04 03:30:33'),
(71, 146, 198, 1, '2019-04-04 03:46:26', '2019-04-04 03:46:26'),
(72, 146, 206, 1, '2019-04-07 23:12:37', '2019-04-07 23:12:37'),
(73, 146, 208, 1, '2019-04-08 04:25:05', '2019-04-08 04:25:05'),
(74, 146, 209, 1, '2019-04-08 05:08:15', '2019-04-08 05:08:15'),
(75, 146, 210, 1, '2019-04-08 05:41:07', '2019-04-08 05:41:07'),
(76, 146, 211, 1, '2019-04-08 06:30:20', '2019-04-08 06:30:20'),
(77, 146, 212, 1, '2019-04-08 06:34:46', '2019-04-08 06:34:46'),
(78, 205, 213, 1, '2019-04-08 07:20:32', '2019-04-08 07:20:32'),
(79, 146, 214, 1, '2019-04-11 13:16:34', '2019-04-11 13:16:34');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_masters`
--

CREATE TABLE `benefit_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_each_passenger` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `in_order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pab_driver, pab_passenger'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_masters`
--

INSERT INTO `benefit_masters` (`id`, `name`, `description`, `for_each_passenger`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`, `in_order`) VALUES
(1, 'Loss and Damage of Vehicle', 'Loss and Damage of Vehicle', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(2, 'Third Party Bodily Injury', 'Third Party Bodily Injury', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(3, 'Third Party Property Damage', 'Third Party Property Damage', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(4, 'Ambulance Cover', 'Ambulance Cover', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(5, 'Personal Accident Benefit - Driver', 'Personal Accident Benefit - Driver', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(6, 'Personal Accident Benefit – Passengers', 'Personal Accident Benefit – Passengers', 1, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(7, 'Emergency Medical Expenses', 'Emergency Medical Expenses', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(8, 'Personal Injury', 'Personal Injury', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(9, 'Oman Cover', 'Oman Cover', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(10, 'Natural Disaster, Strike, Riots', 'Natural Disaster, Strike, Riots', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(11, 'Personal Belongings', 'Personal Belongings', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(12, 'Windscreen Damage', 'Windscreen Damage', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(13, 'Replacement of Locks', 'Replacement of Locks', NULL, 0, NULL, NULL, '2019-01-04 04:34:42', '2019-02-22 01:48:00', NULL),
(14, 'Valet Parking Theft', 'Valet Parking Theft', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(15, 'Road Side Assistance', 'Road Side Assistance', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-04 04:34:42', NULL),
(16, 'Courtesy Car Cash Benefit', 'Courtesy Car Cash Benefit', NULL, 1, NULL, NULL, '2019-01-04 04:34:42', '2019-01-15 03:56:38', NULL),
(17, 'Rent a Car', 'Rent a Car', NULL, 1, NULL, NULL, '2019-01-15 03:56:50', '2019-02-27 00:57:12', NULL),
(18, 'abc', 'test', NULL, 0, NULL, NULL, '2019-01-31 00:47:07', '2019-02-12 02:24:42', NULL),
(19, 'Comprehensive Cover', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:05:09', '2019-02-20 04:05:09', NULL),
(20, 'Geographical Scope', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:06:14', '2019-02-20 04:06:14', NULL),
(21, 'Agency Repair', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:06:38', '2019-02-20 04:06:38', NULL),
(22, 'Free Agency Repair', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:06:58', '2019-02-20 04:06:58', NULL),
(23, 'Riots, Strikes', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:07:42', '2019-02-20 04:07:42', NULL),
(24, 'Excess Waiver For Wind Screen Damage', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:08:20', '2019-02-20 04:08:20', NULL),
(25, 'Auto Gap (For Brand New Vehicle Only)', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:08:42', '2019-02-20 04:08:42', NULL),
(26, 'Off Road Cover', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:09:00', '2019-02-20 04:09:00', NULL),
(27, 'Car Registration Service', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:09:21', '2019-02-20 04:09:21', NULL),
(28, 'Off Road Service', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:09:41', '2019-02-20 04:09:41', NULL),
(29, 'Pick and Drop To Airport', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:10:00', '2019-02-20 04:10:00', NULL),
(30, 'Pick and Drop Service Center', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:10:15', '2019-02-20 04:10:15', NULL),
(31, 'Taxi Fare', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:10:34', '2019-02-20 04:10:34', NULL),
(32, 'Guarantee on Garage Repair (Body, Paint & Mechanical)', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:10:59', '2019-02-20 04:10:59', NULL),
(33, 'Personal Injury For Insured and/or Spouse (Upto)', NULL, NULL, 1, NULL, NULL, '2019-02-20 04:18:48', '2019-02-20 04:20:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_makes`
--

CREATE TABLE `car_makes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `order` mediumint(9) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `modifiedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_makes`
--

INSERT INTO `car_makes` (`id`, `name`, `has_logo`, `order`, `status`, `modifiedby`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Alfa Romeo', 0, NULL, 1, NULL, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(2, 'Aston Martin', 0, NULL, 1, NULL, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(3, 'Audi', 0, NULL, 1, NULL, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(4, 'test', 0, NULL, 1, NULL, NULL, NULL, '2019-02-26 04:42:11', '2019-02-26 07:17:04'),
(5, 'test1', 1, NULL, 1, NULL, NULL, NULL, '2019-02-26 05:39:41', '2019-02-26 07:35:29'),
(6, '22', 1, NULL, 1, NULL, NULL, 113, '2019-02-26 06:31:36', '2019-03-26 02:24:00'),
(7, '44', 0, NULL, 1, NULL, NULL, NULL, '2019-02-26 07:59:07', '2019-02-26 07:59:38'),
(8, 'make name', 0, NULL, 0, NULL, NULL, NULL, '2019-02-26 08:05:02', '2019-02-26 08:05:02'),
(9, 'ABC', 0, NULL, 1, NULL, NULL, NULL, '2019-02-27 23:54:21', '2019-02-27 23:56:50'),
(10, 'XYZ', 0, NULL, 1, NULL, NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(11, 'My Make', 0, NULL, 1, NULL, NULL, NULL, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(12, 'My Make1', 0, NULL, 1, NULL, NULL, NULL, '2019-03-04 04:14:59', '2019-03-04 04:14:59'),
(13, 'My Make2', 0, NULL, 1, NULL, NULL, NULL, '2019-03-04 05:06:00', '2019-03-04 05:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_models`
--

CREATE TABLE `car_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_make_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` mediumint(9) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_models`
--

INSERT INTO `car_models` (`id`, `car_make_id`, `name`, `description`, `order`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 1, '166', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(2, 1, '4c', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(3, 1, 'Giuletta', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(4, 1, 'Giuletta QV', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(5, 1, 'Mito', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(6, 2, 'DB9', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(7, 2, 'DB9 Volante', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(8, 2, 'Rapide', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(9, 2, 'Rapide S', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(10, 2, 'V12 Vantage', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(11, 2, 'V12 Vantage Roadster', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(12, 2, 'V8 Vantage', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(13, 2, 'V8 Vantage Roadster', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(14, 2, 'Vanquish', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(15, 2, 'Vantage', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(16, 3, 'A3', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(17, 3, 'A4', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(18, 3, 'A5', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(19, 3, 'A6', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(20, 3, 'A7', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(21, 3, 'A8', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(22, 3, 'Q3', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(23, 3, 'Q5', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(24, 3, 'Q7', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(25, 3, 'R8', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(26, 3, 'RS3', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(27, 3, 'RS4', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(28, 3, 'RS5', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(29, 3, 'RS6', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(30, 3, 'RS7', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(31, 3, 'S3', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(32, 3, 'S5', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(33, 3, 'S6', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(34, 3, 'S7', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(35, 3, 'TT', NULL, NULL, 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(36, 4, 'test model name', NULL, NULL, 1, NULL, NULL, '2019-02-26 04:44:29', '2019-02-26 04:46:42'),
(37, 5, 'test1', NULL, NULL, 1, NULL, NULL, '2019-02-26 05:40:29', '2019-02-26 05:40:29'),
(38, 5, 'test2', NULL, NULL, 1, NULL, NULL, '2019-02-26 05:54:33', '2019-02-26 05:54:33'),
(39, 1, 'test3', NULL, NULL, 1, NULL, NULL, '2019-02-26 05:54:58', '2019-02-26 05:54:58'),
(40, 5, 'test4', NULL, NULL, 1, NULL, NULL, '2019-02-26 05:55:16', '2019-02-26 05:55:16'),
(41, 1, 'test5', NULL, NULL, 0, NULL, NULL, '2019-02-26 05:56:08', '2019-02-26 05:56:08'),
(42, 5, 'test12', NULL, NULL, 0, NULL, NULL, '2019-02-26 05:57:47', '2019-02-26 05:57:47'),
(43, 5, 'test123', NULL, NULL, 0, NULL, NULL, '2019-02-26 05:58:40', '2019-02-26 06:53:15'),
(44, 4, 'test', NULL, NULL, 1, NULL, NULL, '2019-02-26 07:17:25', '2019-02-26 07:17:25'),
(45, 7, 'hfh', NULL, NULL, 1, NULL, NULL, '2019-02-26 08:03:02', '2019-02-26 08:03:02'),
(46, 8, '11', NULL, NULL, 0, NULL, NULL, '2019-02-26 08:05:02', '2019-02-26 08:05:02'),
(48, 9, 'ABC series', NULL, NULL, 1, NULL, NULL, '2019-02-27 23:58:34', '2019-02-27 23:58:34'),
(49, 10, 'XYZ series', NULL, NULL, 1, NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(50, 11, 'My modal', NULL, NULL, 1, NULL, NULL, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(51, 12, 'My modal1', NULL, NULL, 1, NULL, NULL, '2019-03-04 04:14:59', '2019-03-04 04:14:59'),
(52, 13, 'My modal2', NULL, NULL, 1, NULL, NULL, '2019-03-04 05:06:00', '2019-03-04 05:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_model_body_types`
--

CREATE TABLE `car_model_body_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_model_body_types`
--

INSERT INTO `car_model_body_types` (`id`, `name`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Sedan', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(2, 'Coupe', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(3, 'Hatchback', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(4, 'Convertable', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(5, 'AJ37 V8', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(6, 'SUV', 1, NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(7, 'Saloon', 1, NULL, NULL, '2019-01-16 23:30:21', '2019-01-16 23:30:21'),
(9, 'testdev2', 0, NULL, NULL, '2019-01-18 01:14:54', '2019-02-27 07:48:51'),
(12, 'test model body', 0, NULL, NULL, '2019-02-26 05:32:51', '2019-02-27 07:48:43'),
(15, 'ABC model', 1, NULL, NULL, '2019-02-28 00:51:20', '2019-02-28 00:51:20'),
(16, 'myhatchback', 1, NULL, NULL, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(17, 'myhatchback1', 1, NULL, NULL, '2019-03-04 04:14:59', '2019-03-04 04:14:59');

-- --------------------------------------------------------

--
-- Table structure for table `car_model_body_type_keywords`
--

CREATE TABLE `car_model_body_type_keywords` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_model_body_type_id` int(11) DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_model_body_type_keywords`
--

INSERT INTO `car_model_body_type_keywords` (`id`, `car_model_body_type_id`, `keyword`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sedan', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(2, 2, 'Coupe', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(3, 3, 'Hatchback', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(4, 4, 'Convertable', 1, '2019-01-03 07:55:46', '2019-01-24 05:21:44'),
(5, 5, 'AJ37 V8', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(6, 6, 'SUV', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(7, 2, 'Coupes', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(8, 3, 'Hatch Back', 1, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(9, 9, 'testdev2', 1, '2019-01-18 01:14:54', '2019-02-27 07:48:51'),
(10, 4, 'Convertible', 1, '2019-01-24 05:21:44', '2019-01-24 05:21:44'),
(11, 10, 'test model', 1, '2019-02-26 04:57:36', '2019-02-26 04:57:36'),
(12, 11, 'er', 1, '2019-02-26 05:09:29', '2019-02-26 05:09:29'),
(13, 12, 'test key1', 1, '2019-02-26 05:32:51', '2019-02-27 07:48:43'),
(14, 12, 'test key2', 1, '2019-02-26 05:32:51', '2019-02-27 07:48:43'),
(15, 12, 'test model name', 1, '2019-02-26 05:32:51', '2019-02-27 07:48:43'),
(16, 12, 'test model body', 1, '2019-02-26 05:33:52', '2019-02-27 07:48:43'),
(17, 13, 'test1', 1, '2019-02-26 05:46:43', '2019-02-26 06:57:54'),
(18, 13, 'Test123', 1, '2019-02-26 05:51:22', '2019-02-26 06:57:54'),
(19, 14, '22', 1, '2019-02-26 08:22:00', '2019-02-26 08:22:00'),
(20, 15, 'ABC', 1, '2019-02-28 00:51:20', '2019-02-28 00:51:20'),
(21, 15, 'ABC model', 1, '2019-02-28 00:51:20', '2019-02-28 00:51:20'),
(22, 16, 'myhatchback', 1, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(23, 17, 'myhatchback1', 1, '2019-03-04 04:14:59', '2019-03-04 04:14:59');

-- --------------------------------------------------------

--
-- Table structure for table `car_model_engine_sizes`
--

CREATE TABLE `car_model_engine_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `car_model_fuels`
--

CREATE TABLE `car_model_fuels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_model_fuels`
--

INSERT INTO `car_model_fuels` (`id`, `name`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'petrol', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38'),
(2, 'diesel', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38'),
(3, 'CNG', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38'),
(4, 'LPG', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38'),
(5, 'CNG Kit', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38'),
(6, 'External CNG Kit', 1, NULL, NULL, '2018-12-10 00:04:38', '2018-12-10 00:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `car_model_variants`
--

CREATE TABLE `car_model_variants` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_model_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `make_year` year(4) DEFAULT NULL,
  `car_model_body_type_id` int(11) DEFAULT NULL,
  `cylinders` tinyint(4) DEFAULT NULL,
  `engine_size` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_seats` tinyint(4) DEFAULT 0,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `car_model_fuel_id` int(11) DEFAULT NULL,
  `sub_type` tinyint(4) DEFAULT 0,
  `engine_size_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_model_variants`
--

INSERT INTO `car_model_variants` (`id`, `car_model_id`, `name`, `has_logo`, `make_year`, `car_model_body_type_id`, `cylinders`, `engine_size`, `no_of_seats`, `status`, `created_by`, `modified_by`, `car_model_fuel_id`, `sub_type`, `engine_size_id`, `created_at`, `updated_at`) VALUES
(1, 1, '3.0 L, Sedan, 6Clynder', 0, 2019, 1, 6, '3.0 L', 5, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-02-04 07:53:28'),
(2, 2, '1.8 L, Coupe, 4 Clynder', 0, 2019, 2, 4, '1.8 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:54:22'),
(3, 3, '1.8 L, Hatchback, 4 Clynder', 0, 2019, 3, 4, '1.8 L', 5, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-02-28 03:29:52'),
(4, 4, '1.4 L, Hatchback, 4 Clynders', 0, 2019, 3, 4, '1.4 L', 5, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-02-28 03:30:01'),
(5, 5, '1.4 L, Hatchback, 4 Clynderss', 0, 2019, 3, 4, '1.4 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:55:27'),
(6, 6, '6.0 L, Coupe, 12 Clynder', 0, 2019, 2, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:55:47'),
(7, 7, '6.0 L, Coupe, 12 Clynders', 0, 2019, 2, 12, '6.0 L', 5, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-02-28 07:02:53'),
(8, 8, '6.0 L, Coupe, 12 Clyndersx', 0, 2019, 2, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:56:25'),
(9, 9, '6.0 L, Coupe, 12 Clynders2', 0, 2019, 2, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:56:47'),
(10, 10, '6.0 L, Coupe, 12 Clynders3', 0, 2019, 2, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:57:00'),
(11, 11, '6.0 L, Convertable, 12 Clynder', 0, 2019, 4, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:57:43'),
(12, 12, '4.7 L, Coupe, 8 Clynders', 0, 2019, 2, 8, '4.7 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:58:08'),
(13, 13, '4.7 L, Convertable, 8 Clynders', 0, 2019, 4, 8, '4.7 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:58:25'),
(14, 14, '6.0 L, Convertable, 12 Clynders', 0, 2019, 4, 12, '6.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:58:38'),
(15, 15, '4.3 L, AJ37 V8, 12 Clynder', 0, 2019, 5, 12, '4.3 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:58:53'),
(16, 16, '1.4 L, Sedan, 5 Clynder', 0, 2019, 1, 5, '1.4 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:59:06'),
(17, 17, '1.4 L, Sedan, 4 Clynders', 0, 2019, 1, 4, '1.4 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:59:22'),
(18, 18, '2.0L, Coupe, 4 Clynders', 0, 2019, 2, 4, '2.0L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 07:59:43'),
(19, 19, '2.0L, Sedan, 4 Clynders', 0, 2019, 1, 4, '2.0L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:00:08'),
(20, 20, '1.8L, Sedan, 4 Clynder', 0, 2019, 1, 4, '1.8L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:00:24'),
(21, 21, '4.0 L, Sedan, 8 Clynder', 0, 2019, 1, 8, '4.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:00:46'),
(22, 22, '1.4L, SUV, 4 Cylinder', 0, 2019, 6, 4, '1.4L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:01:02'),
(23, 23, '2.0L, SUV, 4 Cylinders', 0, 2019, 6, 4, '2.0L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:01:15'),
(24, 24, '3.0L, SUV, 6 Cylinders', 0, 2019, 6, 6, '3.0L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:01:28'),
(25, 25, '5.2 L, Coupe, 8 Clynders', 0, 2019, 2, 8, '5.2 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:01:40'),
(26, 26, '2.5 L, Hatchback, 5 Clynders', 0, 2019, 3, 5, '2.5 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:01:57'),
(27, 27, '2.5 L, Hatchback, 5 Clynders1', 0, 2019, 3, 5, '2.5 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:02:19'),
(28, 28, '2.5 L, Hatchback, 5 Clynders2', 0, 2019, 3, 5, '2.5 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:02:46'),
(29, 29, '2.5 L, Hatchback, 5 Clynder3', 0, 2019, 3, 5, '2.5 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:03:03'),
(30, 30, '4.0 L, Hatchback, 8 Clynder', 0, 2019, 3, 8, '4.0 L', 0, 1, NULL, NULL, 2, 1, NULL, '2019-01-03 07:55:46', '2019-01-24 08:03:17'),
(31, 31, '2.0L, Sedan, 4 Clynder ', 0, 2019, 1, 4, '2.0L', 0, 1, NULL, NULL, NULL, 1, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(32, 32, '3.0L, Coupe, 6 Clynder ', 0, 2019, 2, 6, '3.0L', 0, 1, NULL, NULL, NULL, 1, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(33, 33, '4.0 L, Sedan, 8 Clynders', 0, 2019, 1, 8, '4.0 L', 0, 1, NULL, NULL, NULL, 1, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(34, 34, '4.0 L, Sedan, 8 Clynders', 0, 2019, 1, 8, '4.0 L', 0, 1, NULL, NULL, NULL, 1, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(35, 35, '2.0L, Coupe, 4 Clynder ', 0, 2019, 2, 4, '2.0L', 0, 1, NULL, NULL, NULL, 1, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(36, 36, 'test car variant name', 0, 1906, 5, 6, 'hghghghghggggggggg', 5, 1, NULL, NULL, 3, 0, NULL, '2019-02-26 04:52:44', '2019-02-26 05:09:11'),
(37, 43, 'test1', 0, 2008, 7, 1, 'tttttttttttttttttttttttttt', 1, 1, NULL, NULL, 1, 0, NULL, '2019-02-26 05:45:31', '2019-02-26 06:52:16'),
(38, 45, 'gg', 0, 1901, 9, 1, '999999', 1, 0, NULL, NULL, 2, 0, NULL, '2019-02-26 06:18:39', '2019-02-26 08:16:56'),
(39, 43, 'First & Last Name', 0, 1901, 2, 1, 'hghghghghggggggggg', NULL, 1, NULL, NULL, 2, 0, NULL, '2019-02-26 06:51:47', '2019-02-26 06:51:47'),
(40, 2, '2L, 5 cylinder', 0, 2019, 5, 5, '2', 5, 1, NULL, NULL, 6, 0, NULL, '2019-02-27 23:40:03', '2019-02-27 23:40:03'),
(41, 48, '3 L, 3 Cylinder', 0, 2003, 3, 3, '3', 2, 1, NULL, NULL, 2, 0, NULL, '2019-02-28 00:12:58', '2019-02-28 00:12:58'),
(42, 48, '3.0 L, ABC MODEL, 4 Cylinder', 0, 2018, 15, 4, '3.0 L', 5, 1, NULL, 1, 1, 0, NULL, '2019-03-01 00:33:36', '2019-03-01 01:49:24'),
(43, 48, '3.0 L, HATCHBACK, 3 Clynder', 0, 2019, 3, 3, '3.0 L', 1, 1, NULL, 113, 2, 0, NULL, '2019-03-04 01:06:22', '2019-04-02 05:53:50'),
(44, 49, '4.0 L, Sedan, 4 Cylender', 0, 2019, 1, 4, '4.0 L', 5, 1, NULL, 1, 4, 0, NULL, '2019-03-04 01:06:22', '2019-03-04 01:11:03'),
(45, 16, 'car variant name', 0, 2018, 15, 4, 'engine size', 5, 1, 1, 1, 1, 0, NULL, '2019-03-04 03:38:43', '2019-03-04 03:38:43'),
(46, 50, '5.0 L, myhatchback, 4 mycyllinder', 0, 2018, 16, 4, '5.0 L', 0, 1, NULL, NULL, NULL, 0, NULL, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(47, 51, '6.0 L, myhatchback1, 4 mycyllinder1', 0, 2019, 17, 41, '6.0 L', 0, 1, NULL, NULL, NULL, 0, NULL, '2019-03-04 04:14:59', '2019-03-04 04:14:59'),
(48, 52, '6.0 L, myhatchback1, 4 mycyllinder1', 0, 2019, 17, 41, '6.0 L', 0, 1, NULL, NULL, NULL, 0, NULL, '2019-03-04 05:06:00', '2019-03-04 05:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_premiums`
--

CREATE TABLE `car_premiums` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `car_model_body_type_id` int(11) DEFAULT 0,
  `car_model_min_value` int(11) DEFAULT 0,
  `car_model_max_value` int(11) DEFAULT 0,
  `min_age` tinyint(3) UNSIGNED DEFAULT 0,
  `max_age` tinyint(3) UNSIGNED DEFAULT 0,
  `car_model_cylinder` tinyint(4) DEFAULT 0,
  `min_premium_agency` double DEFAULT 0,
  `min_premium_agency_rate` double DEFAULT 0,
  `min_premium_nonagency` double DEFAULT 0,
  `min_premium_nonagency_rate` double DEFAULT 0,
  `available_for_months` tinyint(4) DEFAULT 0,
  `car_model_sub_type` tinyint(4) DEFAULT 0,
  `effective_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `policy_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_premiums`
--

INSERT INTO `car_premiums` (`id`, `insurance_provider_id`, `car_model_body_type_id`, `car_model_min_value`, `car_model_max_value`, `min_age`, `max_age`, `car_model_cylinder`, `min_premium_agency`, `min_premium_agency_rate`, `min_premium_nonagency`, `min_premium_nonagency_rate`, `available_for_months`, `car_model_sub_type`, `effective_date`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`, `policy_type_id`) VALUES
(1, 2, 7, 1, 70000, NULL, NULL, NULL, 1500, 4, 1300, 3.05, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 03:57:23', '2019-02-20 03:57:23', NULL),
(2, 2, 7, 70001, 150000, NULL, NULL, NULL, NULL, 3.4, NULL, 2.7, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 03:58:16', '2019-02-20 03:58:16', NULL),
(3, 2, 7, 150001, 500000, NULL, NULL, NULL, NULL, 2.9, NULL, 2.35, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 03:59:01', '2019-02-20 03:59:01', NULL),
(4, 2, 7, 1, 70000, NULL, NULL, NULL, 1600, 4.25, 1400, 3.24, 36, 2, '2019-02-20 00:00:00', 0, NULL, NULL, '2019-02-20 04:00:27', '2019-02-20 04:00:27', NULL),
(5, 2, 7, 70001, 150000, NULL, NULL, NULL, NULL, 3.5, NULL, 2.84, 36, 2, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 04:01:25', '2019-02-20 04:01:25', NULL),
(6, 2, 7, 150001, 500000, NULL, NULL, NULL, NULL, 3, NULL, 2.46, 36, 2, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 04:02:22', '2019-02-20 04:02:22', NULL),
(7, 1, 3, NULL, NULL, NULL, NULL, 4, 2000, 3.2, 1300, 3, 36, 2, '2019-03-20 00:00:00', 1, NULL, 113, '2019-02-20 23:58:21', '2019-04-04 08:11:10', NULL),
(8, 1, 7, NULL, NULL, NULL, NULL, NULL, 2000, 3.2, 1300, 3, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 23:58:21', '2019-02-20 23:58:21', NULL),
(9, 4, 7, NULL, NULL, 25, 100, 6, 1900, 3.5, 1300, 3, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 04:46:21', '2019-02-21 04:46:21', NULL),
(10, 4, 7, NULL, NULL, 1, 24, 6, 1900, 4.75, 1300, 4.25, 36, 1, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 05:10:35', '2019-02-21 05:10:35', NULL),
(11, 1, 2, 15, 250000, 18, 30, 4, 500, 4.3, 300, 3.7, 24, 1, '2019-02-21 12:22:00', 1, NULL, 113, '2019-02-26 01:22:17', '2019-04-01 23:54:58', NULL),
(12, 1, 7, 150000, 250000, 18, 30, 4, 500, 4.3, 300, 3.7, 24, 1, '2019-02-21 12:22:00', 1, NULL, 113, '2019-02-26 01:22:17', '2019-03-26 06:09:50', NULL),
(13, 5, 5, NULL, NULL, 25, 100, NULL, 1200, 2, 1600, 3, 24, 1, '2019-02-14 18:00:00', 1, NULL, NULL, '2019-02-26 05:52:17', '2019-02-26 05:52:17', NULL),
(14, 6, 15, 1500, 4000, 20, 30, 4, 1000, 50, 500, 50, 24, 1, '2019-03-01 13:00:00', 1, NULL, 1, '2019-02-28 01:01:04', '2019-03-01 03:40:47', NULL),
(15, 4, 15, 2500, 20000, 20, 30, 4, 2000, 5, 2500, 10, 24, 1, '2019-03-02 18:00:00', 1, 1, 1, '2019-03-01 05:36:55', '2019-03-01 06:44:55', NULL),
(16, 3, 1, 3000, 8000, 25, 35, 4, 1500, 5, 2000, 10, 24, 2, '2019-03-09 14:00:00', 1, 1, 1, '2019-03-04 01:19:58', '2019-03-04 01:19:58', NULL),
(17, 3, 17, 90000, 300000, 20, 30, 3, 10, 3.5, 80, 2.4, 12, 2, '2019-03-04 13:13:00', 1, 1, 1, '2019-03-04 02:24:49', '2019-03-04 06:44:52', NULL),
(18, 1, 3, 1, 500000, NULL, NULL, 4, 1300, 3.3, 1500, 4, 24, 1, '2019-02-04 19:08:00', 1, 113, 113, '2019-04-04 08:08:18', '2019-04-04 08:08:18', NULL),
(19, 15, 3, NULL, NULL, NULL, NULL, 4, 2500, 4.2, 2100, 4, 24, 1, '2019-03-26 16:04:00', 1, 113, 113, '2019-04-08 05:04:50', '2019-04-08 05:04:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_premium_agency_available_for_years`
--

CREATE TABLE `car_premium_agency_available_for_years` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_premiums_id` int(11) DEFAULT NULL,
  `year_number` tinyint(4) DEFAULT 0,
  `rate` double DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_premium_agency_available_for_years`
--

INSERT INTO `car_premium_agency_available_for_years` (`id`, `car_premiums_id`, `year_number`, `rate`, `created_at`, `updated_at`) VALUES
(18, 17, 2, 30, '2019-03-04 06:44:52', '2019-03-04 06:44:52'),
(17, 17, 1, 20, '2019-03-04 06:44:52', '2019-03-04 06:44:52'),
(22, 12, 2, 0, '2019-03-26 06:09:54', '2019-03-26 06:09:54'),
(21, 12, 1, 0, '2019-03-26 06:09:54', '2019-03-26 06:09:54'),
(23, 11, 1, 0, '2019-04-01 23:54:58', '2019-04-01 23:54:58'),
(24, 11, 2, 0, '2019-04-01 23:54:58', '2019-04-01 23:54:58'),
(25, 18, 1, 0, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(26, 18, 2, 0, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(27, 7, 1, 0, '2019-04-04 08:11:10', '2019-04-04 08:11:10'),
(28, 7, 2, 0, '2019-04-04 08:11:10', '2019-04-04 08:11:10'),
(29, 7, 3, 0, '2019-04-04 08:11:10', '2019-04-04 08:11:10'),
(30, 19, 1, 0, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(31, 19, 2, 0, '2019-04-08 05:04:50', '2019-04-08 05:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `car_premium_variants`
--

CREATE TABLE `car_premium_variants` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_model_variant_id` int(11) NOT NULL,
  `car_premium_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_premium_variants`
--

INSERT INTO `car_premium_variants` (`id`, `car_model_variant_id`, `car_premium_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 41, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(2, 43, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(3, 3, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(4, 4, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(5, 5, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(6, 26, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(7, 27, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(8, 28, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(9, 29, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(10, 30, 18, 1, '2019-04-04 08:08:18', '2019-04-04 08:08:18'),
(11, 41, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(12, 43, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(13, 3, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(14, 4, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(15, 5, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(16, 26, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(17, 27, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(18, 28, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(19, 29, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50'),
(20, 30, 19, 1, '2019-04-08 05:04:50', '2019-04-08 05:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `car_premium_variant_logs`
--

CREATE TABLE `car_premium_variant_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_premium_variant_id` int(11) NOT NULL,
  `car_model_variant_id` int(11) NOT NULL,
  `car_premium_id` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `car_third_party_covers`
--

CREATE TABLE `car_third_party_covers` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `car_model_body_type_id` int(11) DEFAULT 0,
  `car_model_min_value` int(11) DEFAULT 0,
  `car_model_max_value` int(11) DEFAULT 0,
  `min_age` tinyint(3) UNSIGNED DEFAULT 0,
  `max_age` tinyint(3) UNSIGNED DEFAULT 0,
  `car_model_cylinder` tinyint(4) DEFAULT 0,
  `driving_license_min_age` tinyint(4) DEFAULT 0,
  `driving_license_max_age` tinyint(4) DEFAULT 0,
  `min_premium_agency` double DEFAULT 0,
  `min_premium_agency_rate` double DEFAULT 0,
  `min_premium_nonagency` double DEFAULT 0,
  `min_premium_nonagency_rate` double DEFAULT 0,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rate` double NOT NULL,
  `effective_date` datetime DEFAULT NULL,
  `above_cylinder` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_third_party_covers`
--

INSERT INTO `car_third_party_covers` (`id`, `insurance_provider_id`, `car_model_body_type_id`, `car_model_min_value`, `car_model_max_value`, `min_age`, `max_age`, `car_model_cylinder`, `driving_license_min_age`, `driving_license_max_age`, `min_premium_agency`, `min_premium_agency_rate`, `min_premium_nonagency`, `min_premium_nonagency_rate`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`, `rate`, `effective_date`, `above_cylinder`) VALUES
(1, 5, 4, 0, 0, 25, NULL, 2, 25, NULL, 0, 0, 0, 0, 1, NULL, NULL, '2019-02-20 03:41:46', '2019-02-20 03:41:46', 2, '1999-02-01 00:00:00', 0),
(2, 5, 7, 0, 0, 25, NULL, 4, 1, NULL, 0, 0, 0, 0, 1, NULL, NULL, '2019-02-20 03:43:59', '2019-02-20 03:44:24', 900, '1980-02-20 00:00:00', 0),
(3, 3, 15, 0, 0, NULL, NULL, 42, 3, NULL, 0, 0, 0, 0, 1, 1, 1, '2019-02-28 07:39:37', '2019-03-04 08:01:00', 430, '2019-02-28 19:00:00', 1),
(4, 2, 4, 0, 0, NULL, NULL, 3, NULL, NULL, 0, 0, 0, 0, 0, 2, 2, '2019-02-28 07:40:17', '2019-02-28 07:40:17', 3, '2019-03-02 18:40:00', 0),
(5, 1, 3, 0, 0, NULL, NULL, 3, 1, NULL, 0, 0, 0, 0, 1, 1, 113, '2019-02-28 08:19:00', '2019-04-02 06:04:31', 390, '2019-02-28 23:00:00', 1),
(6, 6, 15, 0, 0, 20, 30, 4, 3, NULL, 0, 0, 0, 0, 1, 1, 1, '2019-03-01 01:21:24', '2019-03-01 04:54:54', 30, '2019-03-01 13:00:00', 0),
(7, 3, 17, 0, 0, 20, 100, 41, NULL, NULL, 0, 0, 0, 0, 1, 1, 1, '2019-03-04 07:46:39', '2019-03-04 08:07:24', 2000, '2019-03-04 18:46:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_values`
--

CREATE TABLE `car_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `car_make_id` int(11) NOT NULL,
  `car_model_id` int(11) DEFAULT NULL,
  `car_model_variant_id` int(11) DEFAULT NULL,
  `values` double DEFAULT 0,
  `min_value` double DEFAULT 0,
  `max_value` double DEFAULT 0,
  `insurance_year` year(4) DEFAULT NULL,
  `manufacture_year` year(4) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_values`
--

INSERT INTO `car_values` (`id`, `car_make_id`, `car_model_id`, `car_model_variant_id`, `values`, `min_value`, `max_value`, `insurance_year`, `manufacture_year`, `effective_date`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 39500, 35550, 51350, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(2, 1, 2, 2, 160000, 144000, 208000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(3, 1, 3, 3, 87000, 78300, 113100, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(4, 1, 4, 4, 175000, 157500, 227500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(5, 1, 5, 5, 39500, 35550, 51350, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(6, 2, 6, 6, 52900, 47610, 68770, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(7, 2, 7, 7, 135000, 121500, 175500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(8, 2, 8, 8, 46900, 42210, 60970, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(9, 2, 9, 9, 39900, 35910, 51870, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(10, 2, 10, 10, 99000, 89100, 128700, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(11, 2, 11, 11, 500000, 450000, 650000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(12, 2, 12, 12, 220000, 198000, 286000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(13, 2, 13, 13, 440000, 396000, 572000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(14, 2, 14, 14, 395000, 355500, 513500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(15, 2, 15, 15, 240600, 216540, 312780, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(16, 3, 16, 16, 418500, 376650, 544050, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(17, 3, 17, 17, 99900, 126000, 129870, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(18, 3, 18, 18, 480000, 432000, 624000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(19, 3, 19, 19, 261000, 234900, 339300, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(20, 3, 20, 20, 158700, 142830, 206310, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(21, 3, 21, 21, 405000, 364500, 526500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(22, 3, 22, 22, 297000, 267300, 386100, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(23, 3, 23, 23, 660000, 594000, 858000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(24, 3, 24, 24, 1320000, 1188000, 1716000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(25, 3, 25, 25, 1185000, 1066500, 1540500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(26, 3, 26, 26, 721800, 649620, 938340, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(27, 3, 27, 27, 1255500, 1129950, 1632150, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(28, 3, 28, 28, 299700, 269730, 389610, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(29, 3, 29, 29, 135000, 121500, 175500, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(30, 3, 30, 30, 357000, 321300, 464100, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(31, 3, 31, 31, 237000, 213300, 308100, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(32, 3, 32, 32, 960000, 864000, 1248000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(33, 3, 33, 33, 522000, 469800, 678600, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(34, 3, 34, 34, 539400, 485460, 701220, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(35, 3, 35, 35, 1050000, 945000, 1365000, 2019, 2019, '2019-01-01 00:00:00', NULL, NULL, '2019-01-03 07:55:46', '2019-01-03 07:55:46'),
(36, 1, 1, 1, 39500, 35550, 51350, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(37, 1, 2, 2, 160000, 144000, 208000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(38, 1, 3, 3, 87000, 78300, 113100, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(39, 1, 4, 4, 175000, 157500, 227500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(40, 1, 5, 5, 39500, 35550, 51350, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(41, 2, 6, 6, 52900, 47610, 68770, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(42, 2, 7, 7, 135000, 121500, 175500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(43, 2, 8, 8, 46900, 42210, 60970, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(44, 2, 9, 9, 39900, 35910, 51870, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(45, 2, 10, 10, 99000, 89100, 128700, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(46, 2, 11, 11, 500000, 450000, 650000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(47, 2, 12, 12, 220000, 198000, 286000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(48, 2, 13, 13, 440000, 396000, 572000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(49, 2, 14, 14, 395000, 355500, 513500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(50, 2, 15, 15, 240600, 216540, 312780, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(51, 3, 16, 16, 418500, 376650, 544050, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(52, 3, 17, 17, 99900, 126000, 129870, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(53, 3, 18, 18, 480000, 432000, 624000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(54, 3, 19, 19, 261000, 234900, 339300, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(55, 3, 20, 20, 158700, 142830, 206310, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(56, 3, 21, 21, 405000, 364500, 526500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(57, 3, 22, 22, 297000, 267300, 386100, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(58, 3, 23, 23, 660000, 594000, 858000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(59, 3, 24, 24, 1320000, 1188000, 1716000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(60, 3, 25, 25, 1185000, 1066500, 1540500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(61, 3, 26, 26, 721800, 649620, 938340, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(62, 3, 27, 27, 1255500, 1129950, 1632150, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(63, 3, 28, 28, 299700, 269730, 389610, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(64, 3, 29, 29, 135000, 121500, 175500, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(65, 3, 30, 30, 357000, 321300, 464100, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(66, 3, 31, 31, 237000, 213300, 308100, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(67, 3, 32, 32, 960000, 864000, 1248000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(68, 3, 33, 33, 522000, 469800, 678600, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(69, 3, 34, 34, 539400, 485460, 701220, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(70, 3, 35, 35, 1050000, 945000, 1365000, 2019, 2018, '2018-01-01 00:00:00', NULL, NULL, '2019-01-03 07:58:50', '2019-01-03 07:58:50'),
(71, 1, 1, 1, 39500, 35550, 51350, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(72, 1, 2, 2, 160000, 144000, 208000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(73, 1, 3, 3, 87000, 78300, 113100, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(74, 1, 4, 4, 175000, 157500, 227500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(75, 1, 5, 5, 39500, 35550, 51350, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(76, 2, 6, 6, 52900, 47610, 68770, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(77, 2, 7, 7, 135000, 121500, 175500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(78, 2, 8, 8, 46900, 42210, 60970, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(79, 2, 9, 9, 39900, 35910, 51870, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(80, 2, 10, 10, 99000, 89100, 128700, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(81, 2, 11, 11, 500000, 450000, 650000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(82, 2, 12, 12, 220000, 198000, 286000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(83, 2, 13, 13, 440000, 396000, 572000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(84, 2, 14, 14, 395000, 355500, 513500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(85, 2, 15, 15, 240600, 216540, 312780, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(86, 3, 16, 16, 418500, 376650, 544050, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(87, 3, 17, 17, 99900, 126000, 129870, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(88, 3, 18, 18, 480000, 432000, 624000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(89, 3, 19, 19, 261000, 234900, 339300, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(90, 3, 20, 20, 158700, 142830, 206310, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(91, 3, 21, 21, 405000, 364500, 526500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(92, 3, 22, 22, 297000, 267300, 386100, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(93, 3, 23, 23, 660000, 594000, 858000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(94, 3, 24, 24, 1320000, 1188000, 1716000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(95, 3, 25, 25, 1185000, 1066500, 1540500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(96, 3, 26, 26, 721800, 649620, 938340, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(97, 3, 27, 27, 1255500, 1129950, 1632150, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(98, 3, 28, 28, 299700, 269730, 389610, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(99, 3, 29, 29, 135000, 121500, 175500, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(100, 3, 30, 30, 357000, 321300, 464100, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(101, 3, 31, 31, 237000, 213300, 308100, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(102, 3, 32, 32, 960000, 864000, 1248000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(103, 3, 33, 33, 522000, 469800, 678600, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(104, 3, 34, 34, 539400, 485460, 701220, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(105, 3, 35, 35, 1050000, 945000, 1365000, 2019, 2017, '2017-01-01 00:00:00', NULL, NULL, '2019-01-03 07:59:32', '2019-01-03 07:59:32'),
(106, 1, 1, 1, 39500, 35550, 51350, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(107, 1, 2, 2, 160000, 144000, 208000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(108, 1, 3, 3, 87000, 78300, 113100, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(109, 1, 4, 4, 175000, 157500, 227500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(110, 1, 5, 5, 39500, 35550, 51350, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(111, 2, 6, 6, 52900, 47610, 68770, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(112, 2, 7, 7, 135000, 121500, 175500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(113, 2, 8, 8, 46900, 42210, 60970, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(114, 2, 9, 9, 39900, 35910, 51870, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(115, 2, 10, 10, 99000, 89100, 128700, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(116, 2, 11, 11, 500000, 450000, 650000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(117, 2, 12, 12, 220000, 198000, 286000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(118, 2, 13, 13, 440000, 396000, 572000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(119, 2, 14, 14, 395000, 355500, 513500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(120, 2, 15, 15, 240600, 216540, 312780, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(121, 3, 16, 16, 418500, 376650, 544050, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(122, 3, 17, 17, 99900, 126000, 129870, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(123, 3, 18, 18, 480000, 432000, 624000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(124, 3, 19, 19, 261000, 234900, 339300, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(125, 3, 20, 20, 158700, 142830, 206310, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(126, 3, 21, 21, 405000, 364500, 526500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(127, 3, 22, 22, 297000, 267300, 386100, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(128, 3, 23, 23, 660000, 594000, 858000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(129, 3, 24, 24, 1320000, 1188000, 1716000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(130, 3, 25, 25, 1185000, 1066500, 1540500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(131, 3, 26, 26, 721800, 649620, 938340, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(132, 3, 27, 27, 1255500, 1129950, 1632150, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(133, 3, 28, 28, 299700, 269730, 389610, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(134, 3, 29, 29, 135000, 121500, 175500, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(135, 3, 30, 30, 357000, 321300, 464100, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(136, 3, 31, 31, 237000, 213300, 308100, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(137, 3, 32, 32, 960000, 864000, 1248000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(138, 3, 33, 33, 522000, 469800, 678600, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(139, 3, 34, 34, 539400, 485460, 701220, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(140, 3, 35, 35, 1050000, 945000, 1365000, 2019, 2016, '2016-01-01 00:00:00', NULL, NULL, '2019-01-03 08:03:32', '2019-01-03 08:03:32'),
(141, 1, 1, 1, 39500, 35550, 51350, 2019, 2018, '2019-03-02 12:00:00', NULL, NULL, '2019-03-01 00:33:36', '2019-03-01 00:33:36'),
(142, 1, 2, 2, 160000, 144000, 208000, 2019, 2018, '2019-03-02 12:00:00', NULL, NULL, '2019-03-01 00:33:36', '2019-03-01 00:33:36'),
(143, 9, 48, 42, 3000, 2000, 50000, 2019, 2018, '2019-03-02 12:00:00', NULL, NULL, '2019-03-01 00:33:36', '2019-03-01 00:33:36'),
(144, 1, 1, 1, 39500, 35550, 51350, 2019, 2019, '2019-03-05 13:00:00', NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(145, 1, 2, 2, 160000, 144000, 208000, 2019, 2019, '2019-03-05 13:00:00', NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(146, 9, 48, 43, 3000, 2000, 50000, 2019, 2019, '2019-03-05 13:00:00', NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(147, 10, 49, 44, 5000, 3000, 8000, 2019, 2019, '2019-03-05 13:00:00', NULL, NULL, '2019-03-04 01:06:22', '2019-03-04 01:06:22'),
(148, 11, 50, 46, 100000, 90000, 149999, 2019, 2018, '2019-03-04 14:59:00', NULL, NULL, '2019-03-04 04:00:05', '2019-03-04 04:00:05'),
(149, 12, 51, 47, 100000, 90000, 149999, 2019, 2019, '2019-03-04 15:14:00', NULL, NULL, '2019-03-04 04:14:59', '2019-03-04 04:14:59'),
(150, 13, 52, 48, 150010, 150001, 250000, 2019, 2019, '2019-03-04 16:05:00', NULL, NULL, '2019-03-04 05:06:00', '2019-03-04 05:06:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'UAE', 1, '2019-01-03 13:41:42', '2019-01-03 13:41:42');

-- --------------------------------------------------------

--
-- Table structure for table `cross_selling_products`
--

CREATE TABLE `cross_selling_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` double DEFAULT 0,
  `fee_type` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `fee` double DEFAULT 0,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `addon_category_id` int(11) DEFAULT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `premium` double DEFAULT 0,
  `benefits` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sum_insured` double DEFAULT 0,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cross_selling_products`
--

INSERT INTO `cross_selling_products` (`id`, `name`, `vat`, `fee_type`, `fee`, `has_logo`, `insurance_provider_id`, `addon_category_id`, `provider_name`, `premium`, `benefits`, `sum_insured`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'apro', NULL, 1, NULL, 0, 15, 4, NULL, 220, NULL, 220, 1, 113, 113, '2019-04-07 23:23:42', '2019-04-07 23:37:35'),
(2, 'apro2', NULL, 1, 3, 0, 1, 4, NULL, 34, NULL, NULL, 1, 113, 113, '2019-04-07 23:25:46', '2019-04-08 07:20:08'),
(3, 'asdasdasd', NULL, 1, NULL, 0, 1, 2, NULL, 43, NULL, NULL, 0, 113, 113, '2019-04-07 23:56:41', '2019-04-07 23:56:41'),
(4, 'apro3', 6, 2, 201, 0, 15, 4, NULL, 601, NULL, 501, 1, 113, 113, '2019-04-08 05:38:17', '2019-04-08 05:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `email_logs`
--

CREATE TABLE `email_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_template_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `status_event` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_to_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_color` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_instyle` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_template_bases`
--

CREATE TABLE `email_template_bases` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `excesses`
--

CREATE TABLE `excesses` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `car_model_body_type_id` int(11) DEFAULT 0,
  `car_model_min_value` int(11) DEFAULT 0,
  `car_model_max_value` int(11) DEFAULT 0,
  `min_seat` tinyint(3) UNSIGNED DEFAULT 0,
  `max_seat` tinyint(3) UNSIGNED DEFAULT 0,
  `min_age` tinyint(3) UNSIGNED DEFAULT 0,
  `max_age` tinyint(3) UNSIGNED DEFAULT 0,
  `excess_value` int(11) DEFAULT NULL,
  `excess_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excesses`
--

INSERT INTO `excesses` (`id`, `insurance_provider_id`, `car_model_body_type_id`, `car_model_min_value`, `car_model_max_value`, `min_seat`, `max_seat`, `min_age`, `max_age`, `excess_value`, `excess_description`, `effective_date`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 50000, NULL, NULL, NULL, NULL, 350, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:25:48', '2019-02-20 02:25:48'),
(2, 2, 7, 1, 50000, NULL, NULL, NULL, NULL, 350, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:25:48', '2019-02-20 02:25:48'),
(3, 2, 2, 50001, 100000, NULL, NULL, NULL, NULL, 500, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:26:37', '2019-02-20 02:26:37'),
(4, 2, 7, 50001, 100000, NULL, NULL, NULL, NULL, 500, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:26:37', '2019-02-20 02:26:37'),
(5, 2, 2, 100001, 250000, NULL, NULL, NULL, NULL, 750, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:27:39', '2019-02-20 02:27:39'),
(6, 2, 7, 100001, 250000, NULL, NULL, NULL, NULL, 750, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:27:39', '2019-02-20 02:27:39'),
(7, 2, 2, 250001, 500000, NULL, NULL, NULL, NULL, 1000, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:28:16', '2019-02-20 02:28:16'),
(8, 2, 7, 250001, 500000, NULL, NULL, NULL, NULL, 1000, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-20 02:28:16', '2019-02-20 02:28:16'),
(9, 1, 3, 1, 49999, NULL, NULL, NULL, NULL, 250, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:50:23', '2019-02-21 00:50:23'),
(10, 1, 7, 1, 49999, NULL, NULL, NULL, NULL, 250, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:50:23', '2019-02-21 00:50:23'),
(11, 1, 3, 50000, 99999, NULL, NULL, NULL, NULL, 350, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:53:13', '2019-02-21 00:53:13'),
(12, 1, 7, 50000, 99999, NULL, NULL, NULL, NULL, 350, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:53:13', '2019-02-21 00:53:13'),
(13, 1, 3, 100000, 249999, NULL, NULL, NULL, NULL, 500, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:53:44', '2019-02-21 00:53:44'),
(14, 1, 7, 100000, 249999, NULL, NULL, NULL, NULL, 500, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:53:44', '2019-02-21 00:53:44'),
(15, 1, 3, 250000, 499999, NULL, NULL, NULL, NULL, 1000, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:54:16', '2019-02-21 00:56:02'),
(16, 1, 7, 250000, 499999, NULL, NULL, NULL, NULL, 10000, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:54:16', '2019-02-21 00:54:16'),
(17, 1, 3, 500000, 749999, NULL, NULL, NULL, NULL, 1400, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:54:43', '2019-02-21 00:54:43'),
(18, 1, 7, 500000, 749999, NULL, NULL, NULL, NULL, 1400, NULL, '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:54:43', '2019-02-21 00:54:43'),
(19, 1, 3, 750000, NULL, NULL, NULL, NULL, NULL, NULL, 'Case by Case Basis', '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:55:24', '2019-02-21 00:55:24'),
(20, 1, 7, 750000, NULL, NULL, NULL, NULL, NULL, NULL, 'Case by Case Basis', '2019-02-20 00:00:00', 1, NULL, NULL, '2019-02-21 00:55:24', '2019-02-21 00:55:24'),
(21, 6, 15, 2000, 50000, 5, 8, 20, 30, 50, NULL, '2019-03-02 14:00:00', 1, NULL, 1, '2019-02-28 01:14:24', '2019-03-01 01:23:03'),
(22, 1, 3, NULL, NULL, NULL, NULL, 1, 24, NULL, '10% of Compensation', '2019-03-01 10:00:00', 1, 1, 1, '2019-02-28 23:16:53', '2019-02-28 23:17:53'),
(23, 3, 17, 160000, 200000, NULL, NULL, NULL, NULL, 350, '15 % extra', '2019-03-05 11:26:00', 1, 1, 1, '2019-03-05 00:26:41', '2019-03-05 01:11:26');

-- --------------------------------------------------------

--
-- Table structure for table `homepages`
--

CREATE TABLE `homepages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `heading` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_line` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_at_home` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepages`
--

INSERT INTO `homepages` (`id`, `name`, `has_logo`, `heading`, `tag_line`, `button_text`, `button_url`, `display_at_home`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'Insurance for less', 0, NULL, 'need a car insurance? compare and buy', 'get started', 'http://insureonline.testsite4me.com/laravel/public/start', 0, NULL, NULL, '2018-12-28 00:15:56', '2019-01-23 06:20:42'),
(2, 'Buy insurance at competitive price', 1, 'Buy insurance at competitive price', 'Need a Car Insurance? Compare and Buy', 'Get Started', 'http://insureonline.testsite4me.com/laravel/public/start', 1, NULL, 1, '2018-12-28 00:28:44', '2019-03-06 00:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_providers`
--

CREATE TABLE `insurance_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended_partner` tinyint(4) DEFAULT 0,
  `premium_age_factor` tinyint(4) DEFAULT 0,
  `premium_car_model_body_factor` tinyint(4) DEFAULT 0,
  `premium_car_model_value_factor` tinyint(4) DEFAULT 0,
  `third_party_available` tinyint(4) DEFAULT 0,
  `base_commission_percentage` double DEFAULT 0,
  `base_commission_amount` double DEFAULT 0,
  `addon_base_commission_percentage` double DEFAULT 0,
  `addon_base_commission_amount` double DEFAULT 0,
  `agent_base_commission_percentage` double DEFAULT 0,
  `agent_base_commission_amount` double DEFAULT 0,
  `agent_addon_base_commission_percentage` double DEFAULT 0,
  `agent_addon_base_commission_amount` double DEFAULT 0,
  `bonus_percentage` double DEFAULT 0,
  `bonus_amount` double DEFAULT 0,
  `sale_amount_limit_bonus` double DEFAULT 0,
  `policy_fees_percentage` double DEFAULT 0,
  `upto_max_amount` double DEFAULT 0,
  `policy_fees_amount` double DEFAULT 0,
  `only_tpl_after_years` tinyint(4) DEFAULT 0,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` mediumint(9) DEFAULT NULL,
  `display_at_home` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `insurance_providers`
--

INSERT INTO `insurance_providers` (`id`, `name`, `has_logo`, `email`, `web_url`, `contact_number`, `description`, `recommended_partner`, `premium_age_factor`, `premium_car_model_body_factor`, `premium_car_model_value_factor`, `third_party_available`, `base_commission_percentage`, `base_commission_amount`, `addon_base_commission_percentage`, `addon_base_commission_amount`, `agent_base_commission_percentage`, `agent_base_commission_amount`, `agent_addon_base_commission_percentage`, `agent_addon_base_commission_amount`, `bonus_percentage`, `bonus_amount`, `sale_amount_limit_bonus`, `policy_fees_percentage`, `upto_max_amount`, `policy_fees_amount`, `only_tpl_after_years`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`, `order`, `display_at_home`) VALUES
(1, 'The Oriental Insurance Company Ltd', 1, '', '', '', NULL, NULL, 0, 1, 1, 1, 0, 200, NULL, 150, 0, 20, NULL, 15, 2, 0, 1, 2, 100, 0, 3, 1, NULL, 113, '2018-12-10 00:04:39', '2019-04-08 05:35:31', NULL, 1),
(2, 'Union Insurance', 1, '', '', '', NULL, NULL, 0, 1, 1, 0, 8, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 20, 0, 1, NULL, NULL, '2018-12-10 00:04:39', '2019-02-01 07:34:22', NULL, 1),
(3, 'Insurance House', 1, '', '', '', '', NULL, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 3, 0, 0, 2, 0, 0, 0, 1, NULL, NULL, '2019-01-07 04:53:27', '2019-02-01 07:34:39', NULL, 1),
(4, 'Aman', 1, '', '', '', '', NULL, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 50, 0, 3, 1, NULL, 1, '2019-01-07 04:55:50', '2019-03-08 01:49:24', NULL, 1),
(5, 'New India Assurance', 0, 'policy@newindia.com', 'newindiaassurance.com', '0987654321', '', 1, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 10, 0, 1, NULL, NULL, '2019-02-01 07:01:50', '2019-02-01 07:33:28', NULL, 1),
(6, 'test abc pvt', 0, 'testing001web@gmail.com', 'www.testabc.com', '4', '', 0, 0, 0, 0, 0, 0, 2500, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 500, 2, 1, NULL, NULL, '2019-02-28 00:56:15', '2019-02-28 00:56:15', NULL, 1),
(12, 'partnernew', 0, 'admin12@app.com', '', '', '', 0, 0, 0, 0, 0, 0, 11, 1, 0, 0, 22, 49, 0, 0, 33, 330, 3, 331, 0, 1, 1, 113, 113, '2019-04-04 00:57:01', '2019-04-04 00:57:01', NULL, 1),
(14, 'partnernew2', 0, 'aaa@gmail.com', '', '', '', NULL, 0, 0, 0, 0, 1, 0, 0, 222, 3, 0, NULL, 444, 0, 0, 1, 0, 0, 0, 1, 1, 113, 113, '2019-04-05 02:21:04', '2019-04-08 07:26:43', NULL, 1),
(15, 'partnernew3', 0, 'aaa3@gmail.com', '', '', '', NULL, 0, 0, 0, 0, 0, 200, NULL, 150, 0, 20, NULL, 15, 0, 0, 1, 0, 0, 0, 1, 1, 113, 113, '2019-04-07 23:17:35', '2019-04-08 05:36:44', NULL, 1),
(24, 'zzaa', 0, 'murtazagenius7272@gmail.com', '', '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, NULL, 0, 113, 113, '2019-04-15 14:19:21', '2019-04-15 14:22:01', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_provider_noclaim_bonuses`
--

CREATE TABLE `insurance_provider_noclaim_bonuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_provider_id` int(11) NOT NULL,
  `bonus_percent_amount` tinyint(4) DEFAULT NULL,
  `bonus_value` double DEFAULT NULL,
  `year_number` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `insurance_provider_noclaim_bonuses`
--

INSERT INTO `insurance_provider_noclaim_bonuses` (`id`, `insurance_provider_id`, `bonus_percent_amount`, `bonus_value`, `year_number`, `created_at`, `updated_at`) VALUES
(15, 24, 1, 1, 1, '2019-04-15 14:22:01', '2019-04-15 14:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `log_addons`
--

CREATE TABLE `log_addons` (
  `id` int(10) UNSIGNED NOT NULL,
  `additionalcover_id` int(11) DEFAULT NULL,
  `cross_selling_product_id` int(11) DEFAULT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` double DEFAULT 0,
  `fee_type` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `fee` double DEFAULT 0,
  `fee_value` double DEFAULT 0,
  `vat_value` double DEFAULT 0,
  `premium_value` double DEFAULT 0,
  `benefits` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `sum_insured` double DEFAULT 0,
  `site_commission_percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `site_commission_amount` double DEFAULT 0,
  `site_commission_value` double DEFAULT 0,
  `agent_id` int(11) DEFAULT NULL,
  `agent_commission_percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `agent_commission_amount` double DEFAULT 0,
  `agent_commission_value` double DEFAULT 0,
  `total_premium` double DEFAULT 0,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0 COMMENT '0 = Pending, 1 = Processing, 2 = Issued',
  `created_at_old` datetime DEFAULT NULL,
  `updated_at_old` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_29_065220_create_admins_table', 1),
(4, '2018_11_30_072548_create_car_makes_table', 1),
(5, '2018_11_30_072726_create_car_models_table', 1),
(6, '2018_11_30_102222_create_car_model_variants_table', 1),
(7, '2018_12_01_123011_create_car_model_body_types_table', 1),
(8, '2018_12_01_123047_create_car_model_fuels_table', 1),
(9, '2018_12_01_124605_create_car_model_engine_sizes_table', 1),
(10, '2018_12_08_150419_create_insurance_providers_table', 1),
(11, '2018_12_08_152531_create_car_premium_table', 1),
(12, '2018_12_07_083909_add_admin_detail', 2),
(13, '2018_12_12_122146_alter_has_image', 2),
(14, '2018_12_13_130056_alter_insurance_providers', 2),
(15, '2018_12_14_092747_create_homepages_table', 2),
(19, '2018_12_15_062307_add_user_role_agent', 3),
(20, '2018_12_15_093006_create_policies_table', 4),
(21, '2018_12_15_093012_create_policy_details_table', 4),
(22, '2018_12_15_093015_create_policy_user_details_table', 4),
(23, '2018_12_15_093033_create_policy_documents_table', 5),
(24, '2018_12_15_093043_create_transaction_logs_table', 6),
(25, '2018_12_15_094443_create_policy_car_details_table', 7),
(26, '2018_12_15_094812_create_policy_payment_details_table', 7),
(27, '2018_12_15_094932_create_policy_renewal_details_table', 7),
(28, '2018_12_15_121609_create_car_third_party_covers_table', 8),
(29, '2018_12_17_102423_create_contacts_table', 9),
(30, '2018_12_17_144552_rename_variant_table_column_type', 9),
(31, '2018_12_17_145837_add_engine_name_into_car_model_variant', 10),
(32, '2018_12_21_122445_add_status_to_variants', 11),
(33, '2018_12_27_071145_alter_homepages', 12),
(34, '2018_12_28_053255_add_standard_superior', 13),
(35, '2018_12_28_114637_create_car_values_table', 13),
(36, '2018_12_29_094828_create_car_model_body_type_keywords_table', 14),
(37, '2018_12_31_081331_create_policy_leads_table', 15),
(38, '2019_01_02_112338_add_effective_date_in_premiumplan', 16),
(39, '2019_01_03_064738_create_countries_table', 17),
(40, '2019_01_03_073226_add_field_policy_leads', 17),
(42, '2019_01_04_060455_create_benefit_masters_table', 18),
(43, '2019_01_04_100844_add_field_recommended_partner_into_insurance_provider', 19),
(44, '2019_01_07_110511_add_image_on_user', 20),
(45, '2019_01_09_115844_create_plan_benefits_table', 20),
(46, '2019_01_09_121141_create_plan_benefits_multiple_table', 20),
(47, '2019_01_21_074727_add_fields_to_plan_benefit', 21),
(48, '2019_01_23_050356_add_status_to_plan_benefits', 22),
(49, '2019_02_01_072613_add_field_policy_fees_in_partners', 23),
(50, '2019_02_04_053944_add_field_base_commission_agents', 24),
(51, '2019_02_04_130222_add_no_of_seats_in_variant', 24),
(52, '2019_02_07_132917_create_excesses_table', 25),
(53, '2019_02_08_071233_add_filed_to_excess', 26),
(54, '2019_02_08_101750_add_field_onlytplafteryears_partners', 27),
(55, '2019_02_08_113044_create_cross_selling_products_table', 28),
(56, '2019_02_06_110519_alter_car_third_party_covers_add_effective_date', 29),
(57, '2019_02_19_145754_add_excess_cylinder_to_car_third_party_covers', 30),
(58, '2019_02_21_061342_change_column_tonullable_effective_date_in_car_third_party_covers', 31),
(60, '2019_02_25_075212_create_plan_benefits_partners_table', 33),
(61, '2019_02_25_093507_remove_some_column_from_plan_benefits', 33),
(62, '2019_02_26_045559_add_reset_password_token_field_into_users', 34),
(63, '2019_02_26_105830_add_reset_password_date_in_user', 34),
(64, '2019_02_27_095541_add_fields_in_user', 35),
(65, '2019_02_28_060709_add_field_created_by_modified_by_everywhere', 36),
(66, '2019_02_28_101629_add_sale_amount_limit_bonus_in_providers', 37),
(67, '2019_02_28_121538_change_column_in_car_third_party_covers', 38),
(68, '2019_03_01_091502_create_car_premium_agency_available_for_years_table', 39),
(69, '2019_03_05_051238_create_user_agent_commissions_table', 40),
(70, '2019_03_05_063311_add_created_by_modified_by_in_user', 40),
(71, '2019_03_05_123944_add_fields_cross_selling_product', 40),
(72, '2019_03_06_060737_add_field_heading_into__homepages_table', 41),
(73, '2019_03_06_060537_delete_column_available_for_months_tpl', 42),
(74, '2019_03_07_075119_add_column_in_policy_leads_table', 43),
(75, '2019_03_07_102320_create_orders_table', 43),
(76, '2019_03_07_105349_add_data_in_policy_leads_table', 43),
(77, '2019_03_07_075130_create_car_premium_variants_table', 44),
(78, '2019_03_08_105027_create_car_premium_variant_logs_table', 45),
(79, '2019_03_11_014901_add_column_pab_in_orders_table', 46),
(80, '2019_03_11_055909_changes_commission_in_orders_table', 46),
(81, '2019_03_12_093919_entrust_setup_tables', 47),
(82, '2019_03_13_061220_add_partners_column_in_orders_table', 47),
(83, '2019_03_13_110343_create_email_logs_table', 48),
(84, '2019_03_13_113629_create_email_templates_table', 48),
(85, '2019_03_14_052817_create_email_template_bases_table', 49),
(86, '2019_03_14_121702_add_excess_in_orders_table', 49),
(87, '2019_03_14_124322_create_pages_table', 49),
(93, '2019_03_18_060808_create_prohibited_routes_table', 51),
(94, '2019_03_18_090100_create_uploads_table', 51),
(95, '2019_03_19_071122_add_status_column_in_orders_table', 52),
(96, '2019_03_19_084711_create_additionalcovers_table', 52),
(97, '2019_03_16_083139_add_policy_values_in_orders_table', 53),
(98, '2019_03_25_093103_add_actual_premium_in_orders_table', 54),
(99, '2019_03_26_084135_add_column_carvalue_in_orders_table', 55),
(100, '2019_03_26_084439_add_column_suminsured_in_additionalcovers_table', 55),
(101, '2019_03_27_065837_create_policy_docs_table', 56),
(102, '2019_03_28_101448_add_column_agency_type_in_orders_table', 57),
(103, '2019_03_28_114208_create_agent_users_table', 57),
(104, '2019_03_29_112430_rename_status_in_agent_users_table', 58),
(105, '2019_03_29_124354_create_addon_orders_table', 58),
(106, '2019_04_01_062140_add_partner_base_commision_in_insurance_providers_table', 58),
(107, '2019_04_02_113900_create_policy_document_masters_table', 59),
(108, '2019_04_03_063407_add_field__insurance_provider_in_cross_selling', 60),
(109, '2019_04_02_063644_add_column_agent_id_in_orders_table', 61),
(110, '2019_04_03_071712_create_addon_categories_table', 62),
(111, '2019_04_03_075526_add_field_addoncaegoryid_in_cross_selling_products', 63),
(112, '2019_04_03_111934_add_field_policy_document_name_in_uploadsx', 64),
(113, '2019_04_03_111934_add_field_policy_document_name_in_uploads', 65),
(114, '2019_04_03_075652_add_field_partner_addon_base_commision_in_insurance_providers_table', 66),
(115, '2019_04_03_091411_create_user_agent_addon_commissions_table', 66),
(116, '2019_04_03_123633_add_policy_fee_and_vat_in_cross_selling_products_table', 66),
(117, '2019_04_04_061539_add_field_addon_base_commision_per_amount_in_insurance_providers', 67),
(118, '2019_04_04_113803_add_field_type_amount_in_cross_selling_products', 68),
(119, '2019_04_05_060843_add_field_cross_selling_product_i_d_in_user_agent_addon_commissionsx', 69),
(120, '2019_04_05_060843_add_field_cross_selling_product_i_d_in_user_agent_addon_commissions', 70),
(121, '2019_04_05_132307_remove_field_in_cross_selling_products', 71),
(122, '2019_04_05_125703_add_fields_in_additionalcovers_table', 72),
(123, '2019_04_08_064803_add_commission_fields_in_additionalcovers_table', 72),
(124, '2019_04_08_095017_add_total_premium_field_in_additionalcovers_table', 72),
(125, '2019_04_08_131545_add_commissions_fields_in_orders', 73),
(126, '2019_04_10_050844_add_field_basicpremium_in_orders', 74),
(127, '2019_04_10_090223_add_required_in_plan_benefits', 74),
(128, '2019_04_12_050925_add_vat_and_currency_code_field_in_orders_table', 74),
(129, '2019_04_12_092117_create_policy_types_table', 74),
(130, '2019_04_12_104341_add_type_field_in_car_premiums_table', 74),
(131, '2019_04_12_130533_create_insurance_provider_noclaim_bonuses_table', 74),
(132, '2019_04_15_073142_add_no_claim_discount_fields_in_orders_table', 75),
(133, '2019_04_16_091045_create_table_ordersnew', 76),
(134, '2019_04_17_075035_add_inorder_field_in_benefit_masters_table', 76),
(135, '2019_04_19_061048_add_no_claim_for_years_in_orders_table', 76),
(136, '2019_04_22_093549_alter_claim_certificate_year_in_policy_leads', 76),
(137, '2019_04_22_103658_create_order_update_histories_table', 77),
(138, '2019_04_22_122445_add_field_policy_id_in_orders_table', 77),
(139, '2019_04_23_045157_add_fields_loading_in_orders', 77),
(140, '2019_04_24_053052_add_status_field_in_policy_docs_table', 77),
(141, '2019_04_24_102106_add_field_status_in_uploads', 77),
(142, '2019_04_29_051319_add_field_status_in_plan_benefits', 78),
(143, '2019_04_29_100234_add_fields_in_additionalcovers', 78),
(144, '2019_04_29_130355_create_addon_docs_table', 78),
(145, '2019_04_29_133134_create_log_addons_table', 78),
(146, '2019_04_30_044737_update_policy_number_in_orders', 78);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_providers_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `car_premiums_id` int(11) DEFAULT 0,
  `benefit_options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_premium` double DEFAULT 0,
  `total` decimal(8,2) NOT NULL,
  `premium_value` decimal(8,2) NOT NULL,
  `vat_premium` decimal(8,2) NOT NULL,
  `policy_fees` decimal(8,2) NOT NULL,
  `pab_passenger` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pab_driver` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_commission_amount` decimal(8,2) DEFAULT NULL,
  `base_commission_percentage` decimal(8,2) DEFAULT NULL,
  `app_commission` double DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `partner_policy_percentage` decimal(8,2) DEFAULT NULL,
  `partner_max_amount` decimal(8,2) DEFAULT NULL,
  `partner_policy_amount` decimal(8,2) DEFAULT NULL,
  `excess` decimal(8,2) DEFAULT NULL,
  `grand_total` decimal(8,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `additional_fees` decimal(8,2) DEFAULT NULL,
  `additional_product_selected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_premium_value` decimal(8,2) DEFAULT NULL,
  `car_value` decimal(8,2) DEFAULT NULL,
  `agency_type` tinyint(4) DEFAULT NULL,
  `precentage_rate` double DEFAULT NULL,
  `minimum_value` double DEFAULT NULL,
  `commission_percentage` double DEFAULT NULL,
  `commission_value` double DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `agent_commission_percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `agent_commission_amount` double DEFAULT 0,
  `agent_commission_value` double DEFAULT 0,
  `agent_commission` double DEFAULT 0,
  `vat` double DEFAULT 0,
  `currency_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_claim_bonus_percent_or_amount` tinyint(4) DEFAULT NULL,
  `no_claim_bonus_value` tinyint(4) DEFAULT NULL,
  `no_claim_bonus_amount` double DEFAULT NULL,
  `no_claim_for_years` tinyint(4) DEFAULT 0,
  `loading_rate` double DEFAULT 0,
  `loading_year_number` tinyint(4) DEFAULT 0,
  `loading_amount` double DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `insurance_providers_id`, `users_id`, `policy_id`, `car_premiums_id`, `benefit_options`, `basic_premium`, `total`, `premium_value`, `vat_premium`, `policy_fees`, `pab_passenger`, `pab_driver`, `base_commission_amount`, `base_commission_percentage`, `app_commission`, `created_at`, `updated_at`, `partner_policy_percentage`, `partner_max_amount`, `partner_policy_amount`, `excess`, `grand_total`, `created_by`, `status`, `policy_number`, `issue_date`, `start_date`, `end_date`, `additional_fees`, `additional_product_selected`, `actual_premium_value`, `car_value`, `agency_type`, `precentage_rate`, `minimum_value`, `commission_percentage`, `commission_value`, `agent_id`, `agent_commission_percentage_or_amount`, `agent_commission_amount`, `agent_commission_value`, `agent_commission`, `vat`, `currency_code`, `no_claim_bonus_percent_or_amount`, `no_claim_bonus_value`, `no_claim_bonus_amount`, `no_claim_for_years`, `loading_rate`, `loading_year_number`, `loading_amount`) VALUES
(1, 1, 112, NULL, 0, '{\"1\": \"yes\", \"2\": \"yes\", \"3\": \"yes\", \"4\": \"Upto AED 200\", \"7\": \"yes\", \"8\": \"yes\", \"9\": \"no\", \"10\": \"yes\", \"11\": \"yes\", \"12\": \"Upto AED 1000\", \"14\": \"yes\", \"15\": \"yes\", \"16\": \"yes\", \"17\": \"0\", \"19\": \"As per policy\", \"20\": \"UAE & Oman\", \"21\": \"90% of Compensation\", \"22\": \"yes\", \"23\": \"no\", \"24\": \"yes\", \"25\": \"no\", \"26\": \"yes\", \"27\": \"yes\", \"28\": \"yes\", \"29\": \"no\", \"30\": \"Within 3 Km range\", \"31\": \"no\", \"32\": \"no\", \"33\": \"no\"}', 0, '2784.00', '2784.00', '0.00', '0.00', '1', '1', '44.00', '0.00', 0, '2019-03-08 00:15:54', '2019-03-26 02:09:27', NULL, NULL, NULL, NULL, NULL, NULL, 0, '111', '2019-03-29 13:09:00', '2019-03-08 13:09:00', '2019-03-23 13:09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(2, 1, 112, NULL, 0, '{\"1\": \"no\", \"2\": \"yes\", \"3\": \"yes\", \"4\": \"Upto AED 200\", \"5\": \"100\", \"6\": \"120\", \"7\": \"yes\", \"8\": \"yes\", \"9\": \"no\", \"10\": \"yes\", \"11\": \"yes\", \"12\": \"Upto AED 1000\", \"14\": \"yes\", \"15\": \"yes\", \"16\": \"yes\", \"17\": \"100\", \"19\": \"As per policy\", \"20\": \"UAE & Oman\", \"21\": \"90% of Compensation\", \"22\": \"yes\", \"23\": \"no\", \"24\": \"yes\", \"25\": \"no\", \"26\": \"yes\", \"27\": \"yes\", \"28\": \"yes\", \"29\": \"no\", \"30\": \"Within 3 Km range\", \"31\": \"no\", \"32\": \"no\", \"33\": \"no\"}', 0, '3104.00', '3104.00', '4.00', '3.00', '2', '2', '0.00', '1.00', 0, '2019-03-08 02:50:26', '2019-03-27 02:06:09', '4.00', '454.00', NULL, NULL, NULL, 101, 0, '11123', '2019-03-15 13:04:00', '2019-03-02 13:04:00', '2019-03-28 18:57:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 97, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(3, 1, 114, NULL, 0, '{\"1\": \"yes\", \"2\": \"yes\", \"3\": \"yes\", \"4\": \"Upto AED 200\", \"5\": \"100\", \"6\": \"120\", \"7\": \"yes\", \"8\": \"yes\", \"9\": \"no\", \"10\": \"yes\", \"11\": \"yes\", \"12\": \"Upto AED 1000\", \"14\": \"yes\", \"15\": \"yes\", \"16\": \"yes\", \"17\": \"120\", \"19\": \"As per policy\", \"20\": \"UAE & Oman\", \"21\": \"90% of Compensation\", \"22\": \"yes\", \"23\": \"no\", \"24\": \"yes\", \"25\": \"no\", \"26\": \"yes\", \"27\": \"yes\", \"28\": \"yes\", \"29\": \"no\", \"30\": \"Within 3 Km range\", \"31\": \"no\", \"32\": \"no\", \"33\": \"no\", \"90001\": \"350\"}', 0, '2950.00', '2950.00', '0.00', '0.00', '3', '3', '45.00', '0.00', 0, '2019-03-08 03:06:17', '2019-03-25 06:02:03', NULL, NULL, NULL, NULL, '444.00', NULL, 2, '534534', '2019-03-03 16:20:00', '2019-03-01 16:20:00', '2019-03-02 16:20:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(4, 1, 106, NULL, 0, '{\"1\": \"yes\", \"2\": \"yes\", \"3\": \"yes\", \"4\": \"Upto AED 200\", \"5\": \"100\", \"7\": \"yes\", \"8\": \"yes\", \"9\": \"no\", \"10\": \"yes\", \"11\": \"yes\", \"12\": \"Upto AED 1000\", \"14\": \"yes\", \"15\": \"yes\", \"16\": \"yes\", \"17\": \"120\", \"19\": \"As per policy\", \"20\": \"UAE & Oman\", \"21\": \"90% of Compensation\", \"22\": \"yes\", \"23\": \"no\", \"24\": \"yes\", \"25\": \"no\", \"26\": \"yes\", \"27\": \"yes\", \"28\": \"yes\", \"29\": \"no\", \"30\": \"Within 3 Km range\", \"31\": \"no\", \"32\": \"no\", \"33\": \"no\", \"90001\": \"350\"}', 0, '3004.00', '3004.00', '0.00', '0.00', '4', '4', '0.00', '2.00', 0, '2019-03-08 04:08:57', '2019-03-08 04:08:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(5, 1, 107, NULL, 0, '{\"1\": \"yes\", \"2\": \"yes\", \"3\": \"yes\", \"4\": \"Upto AED 200\", \"5\": \"100\", \"6\": \"120\", \"7\": \"yes\", \"8\": \"yes\", \"9\": \"no\", \"10\": \"yes\", \"11\": \"yes\", \"12\": \"Upto AED 1000\", \"14\": \"yes\", \"15\": \"yes\", \"16\": \"yes\", \"17\": \"150\", \"19\": \"As per policy\", \"20\": \"UAE & Oman\", \"21\": \"90% of Compensation\", \"22\": \"yes\", \"23\": \"no\", \"24\": \"yes\", \"25\": \"no\", \"26\": \"yes\", \"27\": \"yes\", \"28\": \"yes\", \"29\": \"no\", \"30\": \"Within 3 Km range\", \"31\": \"no\", \"32\": \"no\", \"33\": \"no\", \"90001\": \"350\"}', 0, '3154.00', '3154.00', '0.00', '0.00', '5', '5', '46.00', '0.00', 0, '2019-03-08 05:50:11', '2019-03-08 05:50:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 98, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_new`
--

CREATE TABLE `orders_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `agency_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insurance_providers_id` int(11) DEFAULT NULL,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car_value` double DEFAULT 0,
  `policy_rate` double DEFAULT 0 COMMENT 'Agency / Non Agency Rate',
  `minimum_policy_amount` double DEFAULT 0,
  `loading_rate` double DEFAULT 0,
  `loading_year_number` tinyint(4) DEFAULT 0,
  `loading_amount` double DEFAULT 0,
  `no_claim_bonus_value_type` tinyint(4) DEFAULT 0 COMMENT '1 = Percentage, 2 = Amount',
  `no_claim_bonus_value` double DEFAULT 0,
  `no_claim_bonus_year_number` tinyint(4) DEFAULT 0,
  `no_claim_bonus_amount` double DEFAULT 0,
  `basic_premium` double DEFAULT 0 COMMENT '(cv * rate%) * loading% - no_claim_bonus_amount',
  `pab_driver` double DEFAULT 0,
  `pab_passenger` double DEFAULT 0,
  `premium_amount` double DEFAULT 0 COMMENT 'basic_premium + pab_d + pab_p',
  `policy_options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_benefit_amount` double DEFAULT 0,
  `vat` double DEFAULT 0,
  `var_amount` double DEFAULT 0,
  `policy_fee_type` tinyint(4) DEFAULT 0 COMMENT '1 = Percentage, 2 = Amount',
  `policy_fee_value` double DEFAULT 0,
  `policy_fee_max_amount` double DEFAULT 0,
  `policy_fee_amount` double DEFAULT 0,
  `total_premium_amount` double DEFAULT 0 COMMENT 'basic_premium + total_benefit_amount + vat_amout + policy_fee_amount',
  `total_payment` double DEFAULT 0 COMMENT 'basic_premium + total_benefit_amount + vat_amout + policy_fee_amount + cross_selling_products_total',
  `excess` double DEFAULT 0,
  `app_commission_type` tinyint(4) DEFAULT 0 COMMENT '1 = Percentage, 2 = Amount',
  `app_commission_value` double DEFAULT 0,
  `app_commission` double DEFAULT 0,
  `agent_id` int(11) DEFAULT NULL,
  `agent_commission_type` tinyint(4) DEFAULT 0 COMMENT '1 = Percentage, 2 = Amount',
  `agent_commission_value` double DEFAULT 0,
  `agent_commission` double DEFAULT 0,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_update_histories`
--

CREATE TABLE `order_update_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `car_premium_id` int(11) DEFAULT NULL,
  `benefit_options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_premium` double DEFAULT 0,
  `total` double DEFAULT 0,
  `premium_value` double DEFAULT 0,
  `vat_premium` double DEFAULT 0,
  `policy_fees` double DEFAULT 0,
  `pab_passenger` double DEFAULT 0,
  `pab_driver` double DEFAULT 0,
  `base_commission_amount` double DEFAULT 0,
  `base_commission_percentage` double DEFAULT 0,
  `app_commission` double DEFAULT 0,
  `partner_policy_percentage` double DEFAULT 0,
  `partner_max_amount` double DEFAULT 0,
  `partner_policy_amount` double DEFAULT 0,
  `excess` double DEFAULT 0,
  `grand_total` double DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `policy_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `additional_fees` double DEFAULT 0,
  `additional_product_selected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_premium_value` double DEFAULT 0,
  `car_value` double DEFAULT 0,
  `agency_type` tinyint(4) DEFAULT 0,
  `precentage_rate` double DEFAULT 0,
  `minimum_value` double DEFAULT 0,
  `commission_percentage` double DEFAULT 0,
  `commission_value` double DEFAULT 0,
  `agent_id` int(11) DEFAULT NULL,
  `agent_commission_percentage_or_amount` double DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `agent_commission_amount` double DEFAULT 0,
  `agent_commission_value` double DEFAULT 0,
  `agent_commission` double DEFAULT 0,
  `vat` double DEFAULT 0,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_claim_bonus_percent_or_amount` tinyint(4) DEFAULT 0,
  `no_claim_bonus_value` double DEFAULT 0,
  `no_claim_bonus_amount` double DEFAULT 0,
  `no_claim_for_years` tinyint(4) DEFAULT 0,
  `updated_by` int(11) DEFAULT NULL,
  `created_at_old` datetime DEFAULT NULL,
  `updated_at_old` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` tinyint(4) DEFAULT 0,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('deepak@baltech.in', '$2y$10$mKksSAqxQhelpt3n/4PXVO7HqUtw5DJrZTO/RSsMVQa8PE40JGpPy', '2019-03-11 04:21:14'),
('user1@gmail.com', '$2y$10$hHJT6tHOx3jm.43RyvoOi.hWbAE43EnV3sFXuMkTKkMnhSDCZOLPe', '2019-03-25 04:06:51'),
('alimurtaza@gmail.com', '$2y$10$jAyhpZjHIGGGiq7uMy9MeO9cTFFxxqu0g5qsPo29CvCZQ4rOpqDQ.', '2019-03-26 23:19:05'),
('yuiyi@hh.dd', '$2y$10$pr.OqEx1R0FoSggZlJoB..2UfehXMrV7UoUuoCsJHc1umrWIqIWJa', '2019-04-02 00:56:25'),
('fsdfsdsd@hh.pp', '$2y$10$jupkZEGnv0Vj0ElDEIiWNe/dll4Stg2U9rlD9kY0fBAQCbamn0B5S', '2019-04-02 01:02:13'),
('hffghfg@kk.pp', '$2y$10$fHpn5vfRW9dtBClBqiH0ze/6aCctjF8Ga74NGqspupsR48vDd/uey', '2019-04-02 01:16:25'),
('yrtyrtyrt@tt.ll', '$2y$10$/KNJ8wbGLITtRs6y3jm0feiQZD85cwzRUG/a0SjjMVm1LXX5AYhvm', '2019-04-02 01:19:41'),
('dfsdfsdf@gg.nn', '$2y$10$OIXXoodRvB/dbUWMSMbqhe7h2uJ1gNG38Dq7lNG25otj.YjrSzb1u', '2019-04-02 01:23:54'),
('utyutyu@kk.pp', '$2y$10$mIWrkVb0vFQcygxVsMZDVuCTrZ1pvkxS13hEmeUHfUpoRLyrtGhtC', '2019-04-02 01:31:40'),
('dfsdfsder@kk.mm', '$2y$10$mgDOyd9HgiWALAsZvwzTDOvVa8pj3bsRvvd90ew6Vm8IEZAe3MClG', '2019-04-02 02:01:18'),
('dfgdfgd@rr.pp', '$2y$10$oYTvZ9LG2RtJg/LNLpWDPuoS87IAlHmlnid1to0JljnCcksNASphK', '2019-04-02 02:04:51'),
('fgfgrtyr@tty.oi', '$2y$10$N.gH1sdJGRi57lR7WT5lgustL4bnbDilfpIDqSnrx5MbDAiKppvGG', '2019-04-02 02:11:47'),
('dfsdsd@tt.ok', '$2y$10$Ns55AlXNeRVZt8Jey0NzEuIppm8HlpA6TgBR/lCdF3kH99KCAgS9W', '2019-04-02 02:17:19'),
('asqw@ew.kj', '$2y$10$dvoTs2xUUXz0i49z1.03U.L3HtRd.lIW1jPN3uXeEVBC/V6KnXzZ2', '2019-04-02 02:22:27'),
('dsfsdfsd@rr.ds', '$2y$10$PvOCjsNRCotFnFBBZqF7S.Ja117iut9r7/QZeldzVxrpHLMgSTwxO', '2019-04-02 03:29:43'),
('dfgdfdfre@hh.pp', '$2y$10$YHWuvDX4GvvYdtWduCvF/.aNc6HVohZa7.lwrHNTRFav7zrYe.oi6', '2019-04-02 08:23:07'),
('murtaza333@baltech.in', '$2y$10$3tOKw.UtBan5.e7X501E3.PuMexAb9EJBNijIZeXZ7xJqT7Dtx./e', '2019-04-03 01:11:43'),
('ewre@hh.mn', '$2y$10$ObBcsqeGI0A3LoWw4dbhqeWUqolMnFWArx7EYEvt0ZDrARaPSHpn2', '2019-04-03 04:48:22'),
('xxa@kk.pl', '$2y$10$za0S.5HzzV7KLveoaD8pIeY02Jt4p0IgTRoqrbUqVXjT2VaY7ZbiK', '2019-04-03 06:11:50'),
('sdfsdfsd@gg.dd', '$2y$10$3ReMycc..ZhViurkIvfD.uI0dpU71akg.LDvvEmO/MksGMe56654m', '2019-04-03 06:25:48'),
('dfdfgdf@rr.pp', '$2y$10$Z9NuYia/qM/Ep2m/YkVeBuNDHRuY8zGR2L9Oal1ty.zm1UavuoeEW', '2019-04-03 07:14:31'),
('sdfdfwerwe@gg.ol', '$2y$10$/30MlTPcZ1SU7WbF.P/eb.P2nLBJHSKejI/oj61wHXZ0nFDWId0R6', '2019-04-03 07:17:29'),
('sdfwerwer@gt.pl', '$2y$10$QejJjOP1XJ20mO/gIwqi3Ok.eyp/HjmnbFfeT2LTkMHzyt9tBooia', '2019-04-03 07:38:45'),
('ssname3@domain.com', '$2y$10$oIM/BlEoJaQCqeKF5VbLHeby7eeNOYUZELsKPM.9itTRIUPvbWIbG', '2019-04-03 08:20:23'),
('abcqq2@gmail.com', '$2y$10$H448xcp0iY/5C/JozgoEJe4GFRrdrDJsdEqnD5fYd9sHEYnN7zBn2', '2019-04-03 08:22:20'),
('dsfsfsdf@ee.oo', '$2y$10$Sp952VgxnlyrDGXnLZ1Yhe7.HWaDC6LuvTS7PSPNzvBXUkuk5qJ9e', '2019-04-03 08:40:36'),
('dssdewrw@hh.hh', '$2y$10$TyF4mXScBKhmfps7Mcg2.O4IfaVzc/UvmYtNXnK5N.viWNV5V/y/G', '2019-04-03 08:42:07'),
('sdfsdfewrwe@re.pl', '$2y$10$lRz5oH80sBGJ0Q.7VKRRVu2cMAfMjedLBJSBcMhx4mrpgx54NrJ0e', '2019-04-03 09:01:45'),
('dfsdfwet@gg.pl', '$2y$10$3fe5mAmFSXxuYpKO196v7uK5WkG99gCWlxT7FJCIrKM4rJ4NokG/q', '2019-04-03 09:02:41'),
('ab444c@gmail.com', '$2y$10$vt7yKKRMHrxQ6HHNQjR3BOB3xxlatVDpqgG3MQVz0rOuKwXLlQ7da', '2019-04-04 01:16:44'),
('name123@domain.com', '$2y$10$YYSzFyEYCKAgIwPhgnfPU.NuFNiVTnCpVz4gSGF9baIrUCMODURCW', '2019-04-04 01:54:27'),
('dfgdfg@ff.ll', '$2y$10$Z0z9TYs/5asBec538SnB6uJyRowiFJL.Nct9X8P6rnIJ5iUMttsGy', '2019-04-04 03:46:13'),
('abc333@gmail.com', '$2y$10$aQyJ7iNixIVeCg5ImW8u0OuuIENpoe/xMHyhV6AkHR0Stv5jY.knS', '2019-04-04 04:35:31'),
('aaa2@gmail.com', '$2y$10$vvomJYKa0dqWtxi08334VuVi9H0KNTcwLl8AypyLQnYQGDX5ovPM.', '2019-04-05 03:59:52'),
('aaa@gmail.com', '$2y$10$3RlTn1n9xIn5EdVTlwcLqOO05jACTHNFRE/ZCoIV9oONDhVBVpscm', '2019-04-05 04:44:04'),
('sdfsdf@ff.oo', '$2y$10$Fp4aJBrLKvuCd8T9BF8xZuazzp3nOk8iInmS7xwHh5NM/Fs0LV9hq', '2019-04-07 23:12:26'),
('aaa4@gmail.com', '$2y$10$HTYMX6b79G7wO5tVG1zxu.3YClKE5vZ641.IsjJjIMTle1ItEGyse', '2019-04-07 23:27:32'),
('dfsdfsd@rf.ll', '$2y$10$9SW.28WAU7YSLfbKpmT1ZON39p2yYWurgL1jfNv.Xc9mES1p8swfO', '2019-04-08 04:24:54'),
('aaaab@aa.ab', '$2y$10$PpSjmTWqz2bFdoA/gOeWJO./esUmWO9Phv9Ju/vIOxe1iNCPYH6FW', '2019-04-08 05:08:01'),
('aabb@aa.bb', '$2y$10$k3ibUp.KXwJjG9VZW0oFjuvbREQcW6cXrIv0vkAEgrWqSPpv5fw5S', '2019-04-08 05:40:49'),
('dfsfsdfs@ff.ll', '$2y$10$gmI8jG9uza1I63T0H/1JXO1SO7ZidIXJ9wOZDkzHLO/2FJ1k9P/16', '2019-04-08 06:30:07'),
('qqq@qq.qq', '$2y$10$AX8mh6mZaNu7qcH8b8UdWOaYb7/wHbgIHM6niZqL0xlrnKO.6T6nW', '2019-04-08 06:34:33'),
('zzz@gmail.com', '$2y$10$TKMhdh.yzpZV5m8k3/nreOyFbXzhVbPb1e6BE/CINPnw5X4y0hrL2', '2019-04-08 07:20:19'),
('dfgdf@ff.ll', '$2y$10$3ESQiVvgpSQYLKr3YDvvJOkmERArOS9K5taDKIY9XZOl07YL9LYDa', '2019-04-11 13:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'agent', 'Agent', 'Buys policy behalf of customer.', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(2, 'marketing', 'Marketing user', 'They can sell the products', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(3, 'role-create', 'Create Role', 'Create New Role', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(4, 'role-list', 'Display Role Listing', 'List All Roles', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(5, 'role-update', 'Update Role', 'Update Role Information', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(6, 'user', 'User', 'User', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(7, 'user-create', 'Create User', 'Create New User', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(8, 'user-list', 'Display User Listing', 'List All Users', '2019-03-25 01:38:06', '2019-03-25 01:38:06'),
(9, 'user-update', 'Update User', 'Update User Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(10, 'agent-create', 'Create Agent', 'Create New Agent', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(11, 'agent-list', 'Display Agent Listing', 'List All Agents', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(12, 'agent-update', 'Update Agent', 'Update Agent Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(13, 'partner-create', 'Create Partner', 'Create New Partner', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(14, 'partner-list', 'Display Partner Listing', 'List All Partners', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(15, 'partner-update', 'Update Partner', 'Update Partner Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(16, 'page-create', 'Create Page', 'Create New Page', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(17, 'page-list', 'Display Page Listing', 'List All Pages', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(18, 'page-update', 'Update Page', 'Update Page Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(19, 'car-value-create', 'Create Car Value', 'Create New Car Value', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(20, 'car-value-list', 'Display Car Value Listing', 'List All Car Values', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(21, 'car-value-update', 'Update Car Value', 'Update Car Value Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(22, 'order-create', 'Create Order', 'Create New Order', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(23, 'order-list', 'Display Order Listing', 'List All Orders', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(24, 'order-update', 'Update Order', 'Update Order Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(25, 'lead-create', 'Create Lead', 'Create New Lead', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(26, 'lead-list', 'Display Lead Listing', 'List All Leads', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(27, 'lead-update', 'Update Lead', 'Update Lead Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(28, 'cross-selling-product-create', 'Create Cross Selling Product', 'Create New Cross Selling Product', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(29, 'cross-selling-product-list', 'Display Cross Selling Product Listing', 'List All Cross Selling Products', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(30, 'cross-selling-product-update', 'Update Cross Selling Product', 'Update Cross Selling Product Information', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(31, 'excess-create', 'Create Excess', 'Create New Excess', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(32, 'excess-list', 'Display Excess Listing', 'List All Excess', '2019-03-25 01:38:07', '2019-03-25 01:38:07'),
(33, 'excess-update', 'Update Excess', 'Update Excess Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(34, 'premium-plan-create', 'Create Premium Plan', 'Create New Premium Plan', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(35, 'premium-plan-list', 'Display Premium Plan Listing', 'List All Premium Plan', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(36, 'premium-plan-update', 'Update Premium Plan', 'Update Premium Plan Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(37, 'third-party-cover-create', 'Create TPL Plan', 'Create New TPL Plan', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(38, 'third-party-cover-list', 'Display TPL Plan Listing', 'List All TPL Plan', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(39, 'third-party-cover-update', 'Update TPL Plan', 'Update TPL Plan Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(40, 'enefit-master-create', 'Create Benefit Master', 'Create New Benefit Master', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(41, 'enefit-master-list', 'Display Benefit Master Listing', 'List All Benefit Masters', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(42, 'enefit-master-update', 'Update Benefit Master', 'Update Benefit Master Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(43, 'planbenefit-create', 'Create Plan Benefit', 'Create New Plan Benefit', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(44, 'planbenefit-list', 'Display Plan Benefit Listing', 'List All Plan Benefits', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(45, 'planbenefit-update', 'Update Plan Benefit', 'Update Plan Benefit Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(46, 'homepage-banner-create', 'Create Homepage Banner', 'Create New Homepage Banner', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(47, 'homepage-banner-list', 'Display Homepage Banner Listing', 'List All Homepage Banners', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(48, 'homepage-banner-update', 'Update Homepage Banner', 'Update Homepage Banner Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(49, 'car-make-create', 'Create Car Maker', 'Create New Car Maker', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(50, 'car-make-list', 'Display Car Maker Listing', 'List All Car Makers', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(51, 'car-make-update', 'Update Car Maker', 'Update Car Maker Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(52, 'car-model-create', 'Create Car Model', 'Create New Car Model', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(53, 'car-model-list', 'Display Car Model Listing', 'List All Car Models', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(54, 'car-model-update', 'Update Car Model', 'Update Car Model Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(55, 'car-model-variant-create', 'Create Car Model Variant', 'Create New Car Model Variant', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(56, 'car-model-variant-list', 'Display Car Model Variant Listing', 'List All Car Model Variants', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(57, 'car-model-variant-update', 'Update Car Model Variant', 'Update Car Model Variant Information', '2019-03-25 01:38:08', '2019-03-25 01:38:08'),
(58, 'car-model-body-create', 'Create Car Model Body', 'Create New Car Model Body', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(59, 'car-model-body-list', 'Display Car Model Body Listing', 'List All Car Model Body', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(60, 'car-model-body-update', 'Update Car Model Body', 'Update Car Model Body Information', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(61, 'car-model-fuel-create', 'Create Car Model Fuel', 'Create New Car Model Fuel', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(62, 'car-model-fuel-list', 'Display Car Model Fuel Listing', 'List All Car Model Fuel', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(63, 'car-model-fuel-update', 'Update Car Model Fuel', 'Update Car Model Fuel Information', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(64, 'super-admin', 'Super Admin', 'This will be one permission, that can not be assigned or created.', '2019-03-25 01:38:09', '2019-03-25 01:38:09'),
(65, 'admin', 'Admin', 'This can create other users ,agent and assign permissions to them.', '2019-03-25 01:38:09', '2019-03-25 01:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(64, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits`
--

CREATE TABLE `plan_benefits` (
  `id` int(10) UNSIGNED NOT NULL,
  `benefit_masters_id` int(11) DEFAULT NULL,
  `included` tinyint(4) DEFAULT 0,
  `single_or_multiple` tinyint(4) DEFAULT 0 COMMENT '0 = Comprehensive Plan / 1 = TPL Plan',
  `percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount ',
  `no_of_years` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` tinyint(4) DEFAULT 0 COMMENT '0 = Not Required, 1 = Required',
  `status` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `plan_benefits_partners_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_benefits`
--

INSERT INTO `plan_benefits` (`id`, `benefit_masters_id`, `included`, `single_or_multiple`, `percentage_or_amount`, `no_of_years`, `amount`, `description`, `required`, `status`, `created_at`, `updated_at`, `plan_benefits_partners_id`) VALUES
(1, 1, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(2, 4, 2, 0, 0, NULL, 0, 'Upto AED 200', 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(3, 7, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(4, 10, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(5, 16, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(6, 19, 2, 0, 0, NULL, 0, 'As per policy', 0, 0, '2019-02-28 01:47:08', '2019-02-28 06:47:12', 1),
(7, 20, 2, 0, 0, NULL, 0, 'UAE & Oman', 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(8, 21, 2, 0, 0, NULL, 0, '90% of Compensation', 0, 0, '2019-02-28 01:47:08', '2019-02-28 06:47:12', 1),
(9, 22, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(10, 24, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(11, 27, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:47:08', '2019-02-28 01:49:03', 1),
(12, 2, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(13, 3, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(14, 5, 1, 1, 2, NULL, 100, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 01:49:03', 1),
(15, 6, 1, 1, 2, NULL, 30, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 01:49:03', 1),
(16, 8, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(17, 11, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(18, 12, 2, 0, 0, NULL, 0, 'Upto AED 1000', 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(19, 13, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(20, 14, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(21, 15, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(22, 17, 1, 2, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(23, 26, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(24, 28, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(25, 30, 2, 0, 0, NULL, 0, 'Within 3 Km range', 0, 0, '2019-02-28 01:49:03', '2019-02-28 02:21:01', 1),
(26, 1, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:52:34', 2),
(27, 4, 2, 0, 0, NULL, 0, 'test', 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(28, 8, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(29, 14, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(30, 17, 1, 2, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(31, 19, 1, 2, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(32, 20, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:52:34', 2),
(33, 21, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(34, 22, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(35, 25, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:52:34', 2),
(36, 27, 1, 1, 2, NULL, 20, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:56:31', 2),
(37, 29, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 03:52:34', '2019-03-01 03:52:34', 2),
(38, 1, 2, 0, 0, NULL, 0, 'damage', 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(39, 2, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:08:58', 3),
(40, 3, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:08:58', 3),
(41, 4, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(42, 6, 1, 1, 2, NULL, 100, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(43, 7, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(44, 8, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(45, 16, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(46, 17, 1, 2, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(47, 19, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(48, 21, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(49, 22, 1, 1, 2, NULL, 50, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:08:58', 3),
(50, 24, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(51, 25, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(52, 27, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(53, 33, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-01 04:08:58', '2019-03-01 04:12:16', 3),
(54, 4, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(55, 5, 1, 1, 2, NULL, 200, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:34:14', 4),
(56, 6, 1, 1, 2, NULL, 300, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:34:14', 4),
(57, 8, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(58, 11, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(59, 16, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:34:14', 4),
(60, 19, 2, 0, 0, NULL, 0, 'testing', 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(61, 21, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(62, 25, 2, 0, 0, NULL, 0, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(63, 27, 2, 0, 0, NULL, 0, 'test', 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:39:48', 4),
(64, 33, 1, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-04 00:34:14', '2019-03-04 00:34:14', 4),
(65, 18, 2, 0, 0, NULL, NULL, NULL, 0, 0, '2019-03-04 00:39:48', '2019-03-04 00:39:48', 4);

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits_copy`
--

CREATE TABLE `plan_benefits_copy` (
  `id` int(10) UNSIGNED NOT NULL,
  `benefit_masters_id` int(11) DEFAULT NULL,
  `included` tinyint(4) DEFAULT 0,
  `single_or_multiple` tinyint(4) DEFAULT 0 COMMENT '0 = Comprehensive Plan / 1 = TPL Plan',
  `percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount ',
  `no_of_years` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `plan_benefits_partners_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `plan_benefits_copy`
--

INSERT INTO `plan_benefits_copy` (`id`, `benefit_masters_id`, `included`, `single_or_multiple`, `percentage_or_amount`, `no_of_years`, `amount`, `description`, `created_at`, `updated_at`, `plan_benefits_partners_id`) VALUES
(1, 1, 2, 0, 0, NULL, 0, 'Covered', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(2, 2, 2, 0, 0, NULL, 0, 'Unlimited', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(3, 3, 2, 0, 0, NULL, 0, '3.5 Million', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(4, 4, 2, 0, 0, NULL, 0, 'Free', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(5, 6, 2, 0, 0, NULL, 0, 'Unlimited', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(6, 7, 2, 0, 0, NULL, 0, 'Upto AED 3,000', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(7, 10, 2, 0, 0, NULL, 0, 'Free', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(8, 13, 2, 0, 0, NULL, 0, 'Upto AED 1,000', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(9, 14, 2, 0, 0, NULL, 0, 'Free', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(10, 15, 2, 0, 0, NULL, 0, 'Free Silver', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(11, 19, 2, 0, 0, NULL, 0, 'Upto 10 Years', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(12, 20, 2, 0, 0, NULL, 0, 'UAE & Oman', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(13, 21, 2, 0, 0, NULL, 0, 'Upto 5 Years', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(14, 22, 2, 0, 0, NULL, 0, 'For first 2 years (for the vehicle value above AED 70,000)', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(15, 23, 2, 0, 0, NULL, 0, 'Free', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(16, 24, 2, 0, 0, NULL, 0, 'Upto AED 1,000', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(17, 25, 2, 0, 0, NULL, 0, 'Under 6 Months', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(18, 31, 1, 1, 2, NULL, 100, NULL, '2019-02-20 04:18:20', '2019-02-20 04:18:20', 0),
(19, 32, 2, 0, 0, NULL, 0, 'Upto Policy period', '2019-02-20 04:18:20', '2019-02-20 04:19:15', 0),
(20, 33, 2, 0, 0, NULL, NULL, 'Upto AED 10,000', '2019-02-20 04:19:15', '2019-02-20 04:19:15', 0),
(21, 1, 2, 0, 0, NULL, NULL, 'Covered', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(22, 2, 2, 0, 0, NULL, NULL, 'Unlimited', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(23, 3, 2, 0, 0, NULL, NULL, '5 Million', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(24, 6, 2, 0, 0, NULL, NULL, 'Unlimited', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(25, 7, 2, 0, 0, NULL, NULL, 'Upto AED 6,000', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(26, 10, 2, 0, 0, NULL, NULL, 'Free', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(27, 11, 2, 0, 0, NULL, NULL, 'Upto AED 5,000', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(28, 13, 2, 0, 0, NULL, NULL, 'Upto AED 5,000', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(29, 14, 2, 0, 0, NULL, NULL, 'Free', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(30, 19, 2, 0, 0, NULL, NULL, 'Upto 12 Years (case to case)', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(31, 20, 2, 0, 0, NULL, NULL, 'UAE-OMAN', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(32, 21, 2, 0, 0, NULL, NULL, 'Upto 5 Years', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(33, 22, 2, 0, 0, NULL, NULL, 'For first 2 years (for the vehicle value above AED 70,000)', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(34, 23, 2, 0, 0, NULL, NULL, 'Free', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(35, 24, 2, 0, 0, NULL, NULL, NULL, '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(36, 33, 2, 0, 0, NULL, NULL, 'Upto AED 20,000', '2019-02-20 04:33:15', '2019-02-20 04:33:15', 0),
(37, 1, 2, 0, 0, NULL, 0, 'Covered', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(38, 2, 2, 0, 0, NULL, 0, 'Unlimited', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(39, 3, 2, 0, 0, NULL, 0, '2.5 Million', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(40, 4, 2, 0, 0, NULL, 0, 'Free', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(41, 5, 1, 1, 2, NULL, 100, NULL, '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(42, 6, 2, 0, 0, NULL, 0, 'Unlimited', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(43, 7, 2, 0, 0, NULL, 0, 'Upto AED 3,500', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(44, 9, 2, 0, 0, NULL, 0, NULL, '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(45, 10, 2, 0, 0, NULL, 0, 'Free', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(46, 13, 2, 0, 0, NULL, 0, 'Upto AED 1,100', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(47, 14, 2, 0, 0, NULL, 0, 'Free', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(48, 15, 2, 0, 0, NULL, 0, 'Free Silver', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(49, 16, 2, 0, 0, NULL, 0, NULL, '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(50, 19, 2, 0, 0, NULL, 0, 'Upto 10 Years', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(52, 21, 2, 0, 0, NULL, 0, 'Upto 3 Years', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(54, 23, 2, 0, 0, NULL, 0, 'Free', '2019-02-21 02:11:08', '2019-02-22 02:26:43', 0),
(60, 21, 2, 0, 0, NULL, 0, 'gtrfyth', '2019-02-26 07:32:49', '2019-02-27 01:07:49', 1),
(61, 21, 2, 0, 0, NULL, 0, 'hgjh', '2019-02-26 07:45:27', '2019-02-26 07:55:46', 2),
(62, 1, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(63, 4, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(64, 5, 1, 1, 2, NULL, 100, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(65, 6, 1, 1, 2, NULL, 30, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(66, 7, 2, 0, 0, NULL, NULL, 'Unlimited', '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(67, 8, 2, 1, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(68, 9, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(69, 10, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(70, 11, 2, 1, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(71, 16, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(72, 19, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(73, 20, 2, 0, 0, NULL, NULL, 'OMAN', '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(74, 22, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(75, 24, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(76, 25, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:46', '2019-02-26 07:55:46', 2),
(77, 26, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:47', '2019-02-26 07:55:47', 2),
(78, 27, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:47', '2019-02-26 07:55:47', 2),
(79, 28, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:47', '2019-02-26 07:55:47', 2),
(80, 32, 2, 0, 0, NULL, NULL, NULL, '2019-02-26 07:55:47', '2019-02-26 07:55:47', 2),
(81, 21, 1, 2, 0, NULL, NULL, NULL, '2019-02-27 00:47:42', '2019-02-27 00:47:42', 0),
(82, 21, 1, 2, 0, NULL, NULL, NULL, '2019-02-27 00:48:21', '2019-02-27 00:48:21', 0),
(83, 1, 2, 0, 0, NULL, NULL, 'Unlimited', '2019-02-27 00:55:58', '2019-02-27 00:55:58', 3),
(84, 4, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 4),
(85, 5, 1, 1, 2, NULL, 100, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 5),
(86, 6, 1, 1, 2, NULL, 30, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 6),
(87, 7, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 7),
(88, 8, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 8),
(89, 10, 2, 0, 0, NULL, NULL, 'Upto 3 Lakh', '2019-02-27 00:55:58', '2019-02-27 00:55:58', 9),
(90, 11, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 10),
(91, 16, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 11),
(92, 19, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 12),
(93, 20, 2, 0, 0, NULL, NULL, 'Oman', '2019-02-27 00:55:58', '2019-02-27 00:55:58', 13),
(94, 21, 1, 1, 1, NULL, 100, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 14),
(95, 22, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 15),
(96, 24, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 16),
(97, 25, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 17),
(98, 26, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 18),
(99, 27, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 19),
(100, 28, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 20),
(101, 32, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 00:55:58', '2019-02-27 00:55:58', 21),
(102, 17, 1, 2, 0, NULL, NULL, NULL, '2019-02-27 00:57:57', '2019-02-27 00:57:57', 0),
(103, 22, 1, 2, 0, NULL, 0, NULL, '2019-02-27 01:07:49', '2019-02-27 01:08:16', 1),
(104, 4, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 07:30:03', '2019-02-27 07:30:03', 22),
(105, 21, 2, 0, 0, NULL, NULL, NULL, '2019-02-27 07:30:03', '2019-02-27 07:30:03', 23),
(106, 4, 2, 0, 0, NULL, 0, 'Covered', '2019-02-27 07:47:37', '2019-02-28 00:41:08', 24),
(107, 21, 2, 1, 0, NULL, NULL, 'kjkbj', '2019-02-27 07:47:37', '2019-02-27 07:47:37', 25),
(108, 17, 1, 2, 0, NULL, 0, NULL, '2019-02-28 00:41:08', '2019-02-28 01:03:00', 24),
(109, 21, 2, 0, 0, NULL, 0, 'kjkbj', '2019-02-28 00:41:08', '2019-02-28 01:03:00', 24),
(110, 1, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(111, 2, 2, 0, 0, NULL, 0, 'Unlimited', '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(112, 3, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(113, 5, 1, 1, 1, NULL, 100, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:00', 24),
(114, 6, 1, 1, 1, NULL, 30, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:00', 24),
(115, 7, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(116, 10, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(117, 15, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(118, 16, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(119, 19, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(120, 20, 2, 0, 0, NULL, 0, 'Oman', '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(121, 22, 2, 0, 0, NULL, 0, '1 Year', '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(122, 26, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(123, 27, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24),
(124, 28, 2, 0, 0, NULL, 0, NULL, '2019-02-28 01:03:00', '2019-02-28 01:03:20', 24);

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits_multiple`
--

CREATE TABLE `plan_benefits_multiple` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_benefits_id` int(11) DEFAULT NULL,
  `percentage_or_amount` tinyint(4) DEFAULT 0,
  `amount` double DEFAULT 0,
  `feature_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_benefits_multiple`
--

INSERT INTO `plan_benefits_multiple` (`id`, `plan_benefits_id`, `percentage_or_amount`, `amount`, `feature_description`, `created_at`, `updated_at`) VALUES
(25, 22, 2, 150, '15 days', '2019-03-26 06:09:36', '2019-03-26 06:09:36'),
(24, 22, 2, 120, '10 days', '2019-03-26 06:09:36', '2019-03-26 06:09:36'),
(23, 22, 2, 100, '7 days', '2019-03-26 06:09:36', '2019-03-26 06:09:36'),
(15, 30, 2, 20, 'for 2 day', '2019-03-01 03:56:31', '2019-03-01 03:56:31'),
(14, 30, 2, 10, 'for 1 day', '2019-03-01 03:56:31', '2019-03-01 03:56:31'),
(17, 31, 2, 50, 'for 2', '2019-03-01 03:56:31', '2019-03-01 03:56:31'),
(16, 31, 2, 25, 'for 1', '2019-03-01 03:56:31', '2019-03-01 03:56:31'),
(22, 46, 2, 40, 'for 2 day', '2019-03-01 04:12:16', '2019-03-01 04:12:16'),
(21, 46, 2, 20, 'for 1 day', '2019-03-01 04:12:16', '2019-03-01 04:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits_multiple_copy`
--

CREATE TABLE `plan_benefits_multiple_copy` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_benefits_id` int(11) DEFAULT NULL,
  `percentage_or_amount` tinyint(4) DEFAULT 0,
  `amount` double DEFAULT 0,
  `feature_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `plan_benefits_multiple_copy`
--

INSERT INTO `plan_benefits_multiple_copy` (`id`, `plan_benefits_id`, `percentage_or_amount`, `amount`, `feature_description`, `created_at`, `updated_at`) VALUES
(1, 81, 1, 22, NULL, '2019-02-27 00:47:42', '2019-02-27 00:47:42'),
(2, 81, 1, 33, NULL, '2019-02-27 00:47:42', '2019-02-27 00:47:42'),
(3, 82, 1, 5, NULL, '2019-02-27 00:48:21', '2019-02-27 00:48:21'),
(4, 82, 1, 66, NULL, '2019-02-27 00:48:21', '2019-02-27 00:48:21'),
(5, 102, 2, 100, '7 Days', '2019-02-27 00:57:57', '2019-02-27 00:57:57'),
(6, 102, 1, 120, '10 Days', '2019-02-27 00:57:57', '2019-02-27 00:57:57'),
(7, 102, 1, 150, '15 Days', '2019-02-27 00:57:57', '2019-02-27 00:57:57'),
(9, 103, 1, 20, 'jhkjhkj', '2019-02-27 01:08:16', '2019-02-27 01:08:16'),
(10, 103, 2, 234, 'vgvhg', '2019-02-27 01:08:16', '2019-02-27 01:08:16'),
(24, 108, 1, 150, '15 days', '2019-02-28 01:06:18', '2019-02-28 01:06:18'),
(23, 108, 2, 100, '7 days', '2019-02-28 01:06:18', '2019-02-28 01:06:18'),
(22, 108, 1, 120, '10 days', '2019-02-28 01:06:18', '2019-02-28 01:06:18');

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits_partners`
--

CREATE TABLE `plan_benefits_partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_providers_id` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `plan_type` tinyint(1) DEFAULT 0 COMMENT '0 = Comprehensive Plan / 1 = TPL Plan',
  `sub_type` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Standard, 2 = Superior',
  `status` tinyint(1) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_benefits_partners`
--

INSERT INTO `plan_benefits_partners` (`id`, `insurance_providers_id`, `year`, `effective_date`, `plan_type`, `sub_type`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 1, 2019, '2019-02-27 00:00:00', 0, 1, 1, NULL, NULL, '2019-02-28 01:47:08', '2019-02-28 01:47:08'),
(2, 6, 2019, '2019-03-02 15:00:00', 0, 1, 1, 1, 1, '2019-03-01 03:52:34', '2019-03-01 03:52:34'),
(3, 6, 2019, '2019-03-02 17:00:00', 1, 1, 1, 1, 1, '2019-03-01 04:08:58', '2019-03-01 04:08:58'),
(4, 6, 2019, '2019-03-06 13:00:00', 0, 1, 1, 1, 1, '2019-03-04 00:34:14', '2019-03-04 00:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `plan_benefits_partners_copy`
--

CREATE TABLE `plan_benefits_partners_copy` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_providers_id` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `plan_type` tinyint(1) DEFAULT 0 COMMENT '0 = Comprehensive Plan / 1 = TPL Plan',
  `sub_type` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Standard, 2 = Superior',
  `status` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=FIXED;

--
-- Dumping data for table `plan_benefits_partners_copy`
--

INSERT INTO `plan_benefits_partners_copy` (`id`, `insurance_providers_id`, `year`, `effective_date`, `plan_type`, `sub_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 2019, '2019-02-27 18:38:00', 0, 1, 1, '2019-02-26 07:32:49', '2019-02-26 07:32:49'),
(2, 1, 2019, '2019-02-14 18:51:00', 0, 1, 1, '2019-02-26 07:45:27', '2019-02-26 07:45:27'),
(3, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(4, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(5, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(6, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(7, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(8, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(9, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(10, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(11, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(12, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(13, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(14, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(15, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(16, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(17, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(18, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(19, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(20, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(21, 4, 2019, '2019-02-28 00:00:00', 0, 1, 1, '2019-02-27 00:55:58', '2019-02-27 00:55:58'),
(22, 3, 2019, '2019-02-07 18:35:00', 1, 1, 1, '2019-02-27 07:30:03', '2019-02-27 07:30:03'),
(23, 3, 2019, '2019-02-07 18:35:00', 1, 1, 1, '2019-02-27 07:30:03', '2019-02-27 07:30:03'),
(24, 1, 2019, '2019-02-20 18:53:00', 0, 1, 1, '2019-02-27 07:47:37', '2019-02-27 07:47:37'),
(25, 1, 2019, '2019-02-20 18:53:00', 0, 1, 1, '2019-02-27 07:47:37', '2019-02-27 07:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` int(10) UNSIGNED NOT NULL,
  `insurance_provider_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `policynumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issuedate` datetime DEFAULT NULL,
  `effectivedate` datetime DEFAULT NULL,
  `expirydate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_car_details`
--

CREATE TABLE `policy_car_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `car_make_id` int(11) DEFAULT NULL,
  `car_model_id` int(11) DEFAULT NULL,
  `car_model_variant_id` int(11) DEFAULT NULL,
  `vehicle_year` year(4) DEFAULT NULL,
  `vehicle_value` int(11) DEFAULT NULL,
  `vehicle_registration_date` date DEFAULT NULL,
  `city_of_registration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_car_new` tinyint(4) DEFAULT NULL,
  `is_vehicle_modified` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_details`
--

CREATE TABLE `policy_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `policy_amount` decimal(8,2) DEFAULT NULL,
  `exceed_value` decimal(8,2) DEFAULT NULL,
  `personal_accidental_driver` decimal(8,2) DEFAULT NULL,
  `is_agency_repair` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_docs`
--

CREATE TABLE `policy_docs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `docname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `policy_docs`
--

INSERT INTO `policy_docs` (`id`, `user_id`, `order_id`, `docname`, `status`, `created_at`, `updated_at`) VALUES
(1, 112, 2, '11123-UIC Motor Tariff-Direct .pdf', 0, '2019-03-27 02:29:14', '2019-03-27 02:29:14');

-- --------------------------------------------------------

--
-- Table structure for table `policy_documents`
--

CREATE TABLE `policy_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `document_name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_document_masters`
--

CREATE TABLE `policy_document_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `policy_document_masters`
--

INSERT INTO `policy_document_masters` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Identity document', 1, '2019-04-02 06:36:17', '2019-04-02 06:36:17'),
(2, 'Aadharx43423', 1, NULL, '2019-04-03 01:23:50'),
(6, 'yyyy', 0, '2019-04-03 01:28:36', '2019-04-03 01:28:36'),
(7, 'zzz', 0, '2019-04-15 12:53:35', '2019-04-15 12:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `policy_leads`
--

CREATE TABLE `policy_leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `car_model_year` int(11) DEFAULT NULL,
  `car_make_id` int(11) DEFAULT NULL,
  `car_model_id` int(11) DEFAULT NULL,
  `car_model_variant_id` int(11) DEFAULT NULL,
  `car_value` int(11) DEFAULT NULL,
  `is_car_new` tinyint(4) DEFAULT NULL,
  `car_registration_date` date DEFAULT NULL,
  `is_nongcc` tinyint(4) DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_experience` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_in_uae` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `is_thirdpartyliablity` tinyint(4) DEFAULT NULL,
  `is_agencyrepair` tinyint(4) DEFAULT NULL,
  `is_policyexpired` tinyint(4) DEFAULT NULL,
  `register` datetime DEFAULT NULL,
  `is_claiminsurance` tinyint(4) DEFAULT NULL,
  `is_claimcertificate` tinyint(4) DEFAULT NULL,
  `city_of_registration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `claimCertificateYear` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `policy_leads`
--

INSERT INTO `policy_leads` (`id`, `user_id`, `car_model_year`, `car_make_id`, `car_model_id`, `car_model_variant_id`, `car_value`, `is_car_new`, `car_registration_date`, `is_nongcc`, `nationality`, `country`, `driving_experience`, `driving_in_uae`, `dob`, `is_thirdpartyliablity`, `is_agencyrepair`, `is_policyexpired`, `register`, `is_claiminsurance`, `is_claimcertificate`, `city_of_registration`, `claimCertificateYear`, `created_at`, `updated_at`, `mobile`, `email`, `name`, `mobile_otp`, `email_otp`, `order_id`) VALUES
(7, 210, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1979-04-14', NULL, NULL, NULL, NULL, NULL, NULL, 'Ajman', NULL, '2019-04-08 05:40:04', '2019-04-08 05:41:10', '1122112211', 'aabb@aa.bb', 'aabb', 'insure', 'insure', 83),
(6, 209, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1984-04-21', NULL, NULL, NULL, NULL, NULL, NULL, 'Fujairah', NULL, '2019-04-08 05:02:34', '2019-04-08 05:08:18', '1111111111', 'aaaab@aa.ab', 'aaaab', 'insure', 'insure', 82),
(5, 208, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1980-04-19', NULL, NULL, NULL, NULL, NULL, NULL, 'Ajman', NULL, '2019-04-08 04:21:56', '2019-04-08 04:28:24', '4353453434', 'dfsdfsd@rf.ll', 'dsfsdf', 'insure', 'insure', 81),
(8, 211, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1985-04-13', NULL, NULL, NULL, NULL, NULL, NULL, 'Ajman', NULL, '2019-04-08 06:29:07', '2019-04-08 06:30:23', '3434534535', 'dfsfsdfs@ff.ll', 'dsfsdfs', 'insure', 'insure', 84),
(9, 212, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1978-04-15', NULL, NULL, NULL, NULL, NULL, NULL, 'Ajman', NULL, '2019-04-08 06:33:53', '2019-04-08 06:34:49', '1122223344', 'qqq@qq.qq', 'qqq', 'insure', 'insure', 85),
(10, 213, 2019, 1, 3, 3, 87000, 1, '2019-04-08', 0, 'UAE', 'UAE', '2', '2', '1987-04-18', NULL, NULL, NULL, NULL, NULL, NULL, 'Ajman', NULL, '2019-04-08 07:19:01', '2019-04-08 07:20:35', '4433556677', 'zzz@gmail.com', 'zzz', 'insure', 'insure', 86),
(11, 214, 2019, 1, 3, 3, 87000, 1, '2019-04-12', 0, 'UAE', 'UAE', '2', '2', '1980-04-19', NULL, NULL, NULL, NULL, NULL, NULL, 'Dubai', NULL, '2019-04-11 13:15:51', '2019-04-11 14:18:59', '3453434534', 'dfgdf@ff.ll', 'dfgd', 'insure', 'insure', 97);

-- --------------------------------------------------------

--
-- Table structure for table `policy_payment_details`
--

CREATE TABLE `policy_payment_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `transaction_amount` decimal(8,2) UNSIGNED DEFAULT NULL,
  `tax` decimal(8,2) UNSIGNED DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_renewal_details`
--

CREATE TABLE `policy_renewal_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `is_thirdparty_only` tinyint(4) DEFAULT NULL,
  `is_agency_repair` tinyint(4) DEFAULT NULL,
  `is_policy_expired` tinyint(4) DEFAULT NULL,
  `registeration_date` date DEFAULT NULL,
  `is_claim` tinyint(4) DEFAULT NULL,
  `no_claim_cert` tinyint(4) DEFAULT NULL,
  `no_of_claim_years` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_types`
--

CREATE TABLE `policy_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `policy_user_details`
--

CREATE TABLE `policy_user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` int(11) DEFAULT NULL,
  `license_country` int(11) DEFAULT NULL,
  `intl_driving_exp_year` year(4) DEFAULT NULL,
  `uae_driving_years` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prohibited_routes`
--

CREATE TABLE `prohibited_routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'agent', 'agent', 'This will be one permission, that can not be assigned or modified.', '2019-03-25 01:41:16', '2019-03-25 01:41:16'),
(2, 'super-admin', 'superadmin', 'This can create other users ,agent and assign permissions to them.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(112, 1),
(113, 2),
(114, 1),
(146, 1),
(205, 1),
(207, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_logs`
--

CREATE TABLE `transaction_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `transaction_amount` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `transaction_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_document_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_exact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `policy_document_name`, `filename`, `file_exact_name`, `order_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Identity document', 'Ih4l_1554698579_agent2.png', 'agent2.png', 78, 206, 0, '2019-04-07 23:12:59', '2019-04-07 23:12:59'),
(2, 'Aadharx43423', 'uil0_1554717521_err2.png', 'err2.png', 81, 208, 0, '2019-04-08 04:28:41', '2019-04-08 04:28:41'),
(3, 'Identity document', 'rWe3_1554719910_check.png', 'check.png', 82, 209, 0, '2019-04-08 05:08:30', '2019-04-08 05:08:30'),
(4, 'Aadharx43423', '9teV_1554721880_policyStanSupe.png', 'policyStanSupe.png', 83, 210, 0, '2019-04-08 05:41:20', '2019-04-08 05:41:20'),
(5, 'Aadharx43423', '0grO_1554725010_agent2.png', 'agent2.png', 84, 211, 0, '2019-04-08 06:33:30', '2019-04-08 06:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_password_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_date` timestamp NULL DEFAULT NULL,
  `user_role` tinyint(4) DEFAULT 0,
  `mobile_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_mobile_verified` tinyint(1) DEFAULT NULL,
  `is_email_verified` tinyint(1) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `mobile_verified_at` timestamp NULL DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `maritalstatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_logo` tinyint(4) NOT NULL DEFAULT 0,
  `base_commission_percentage` double DEFAULT 0,
  `base_commission_amount` double DEFAULT 0,
  `passport_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_expiry_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_expiry_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uae_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uae_id_expiry_date` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modifiedby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile`, `email`, `password`, `reset_password_token`, `reset_password_date`, `user_role`, `mobile_otp`, `email_otp`, `is_mobile_verified`, `is_email_verified`, `email_verified_at`, `mobile_verified_at`, `address`, `dob`, `gender`, `maritalstatus`, `has_logo`, `base_commission_percentage`, `base_commission_amount`, `passport_number`, `passport_expiry_date`, `license_number`, `license_expiry_date`, `uae_id`, `uae_id_expiry_date`, `status`, `created_by`, `modified_by`, `modifiedby`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'saloon', '1029384756', 'saloon@yahoo.com', '$2y$10$NTWqf3SQUOCfFJZH3WI.G.TO3sTArLiHjck0Ut0YUZZX6Hja3A09W', NULL, NULL, 0, 'insure', 'insure', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'WKTo0zEMNegF7xZ7DkNEMvSm94TrvonSUuPFLjxtesUxNROiR21IajAgvF2D', '2019-02-20 23:40:50', '2019-02-20 23:40:50'),
(2, 'aaaaa2', '32342', 'murtazagenius7272@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, 'insure', 'insure', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'owpxEMNKN5B1nOwW0o35nzPY0ynpyovQemtF8PV6r0LoFsmF3LvE8pi2tvvx', '2019-02-21 00:03:02', '2019-03-20 01:36:38'),
(102, 'Agent 1', '9201837465', 'alinoble20172@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1976-03-08', 1, 'Married', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, '2019-03-08 01:11:09', '2019-03-08 01:11:09'),
(111, 'agnet23', '8888', 'alinoble20171@gmail.com', '$2y$10$MWknOIGt5JQ5m1nIGKWIAO.NFFWprf3mgMJoR8YtoHyuRFtSbQa2C', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'address', NULL, 1, 'Married', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, 2, NULL, 'hJhgpJq6EeV5XeOlUh0emN9ueqUs4BcFaijBqgapTMMOG68ZAepbMbWho7hR', '2019-03-20 04:09:43', '2019-03-20 04:12:50'),
(112, 'agent', NULL, 'alinoble2017@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, NULL, NULL, NULL, NULL, '2019-03-26 07:15:58', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'v6j3AzgllFXSvjK0eO8eYW5hBYEzxNdPZxTA07UzraVAmlXjGf4NJeJtRclT', '2019-03-25 01:42:58', '2019-03-26 07:15:58'),
(113, 'super-admin', NULL, 'abc@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '0thI0alvbrX1sXIh1em5mMfiWo0Qr8WMc9EYpCkETPQzXkCEfpc15nfZVbDM', '2019-03-25 02:08:47', '2019-03-25 02:08:47'),
(114, 'user1', '9988776655', 'user1@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, NULL, NULL, NULL, NULL, '2019-03-26 07:15:58', NULL, 'address', '2001-03-02', 3, 'Widow', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 113, 113, NULL, 'NJbyDjkgU2V1edDcNk2aDe75k1lOyr6NWzPGfaQ15uZYTymDJdcyGZo0XPiZ', '2019-03-25 04:06:51', '2019-03-25 04:06:51'),
(115, 'wwweee', '3333', 'alimurtaza@gmail.com', '$2y$10$U.b2iVWjTZ6JtiomR2BmRuW6fpi9Btl6gzb/j632B8PVb9esVKvme', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'addr', '2019-03-15', 3, 'Divorced', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 113, 113, NULL, NULL, '2019-03-26 23:19:05', '2019-03-26 23:19:05'),
(146, 'agentenv', NULL, 'agentenv@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-08 05:40:49', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(205, 'agentnew', NULL, 'aaa@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 1, NULL, NULL, NULL, 1, '2019-04-07 23:12:26', NULL, NULL, NULL, 1, 'Married', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 113, 113, NULL, NULL, '2019-04-05 04:44:04', '2019-04-07 23:29:51'),
(206, 'aass', '3453543434', 'sdfsdf@ff.oo', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-07 23:12:26', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-07 23:12:26', '2019-04-07 23:12:26'),
(207, 'agentnew3', NULL, 'aaa4@gmail.com', '$2y$10$EowkqVz2VJw6ImTmkzC6wugEBNr1pFXlG4Ds5rRHYO68QtB3S9Sbi', NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 'Married', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 113, 113, NULL, 'VKrMlXL6d6sQsfExPnzz4iDShzgNJT5CIfP38FU3l1mSfTxb1JBoj8MUOQqw', '2019-04-07 23:27:32', '2019-04-07 23:38:35'),
(208, 'dsfsdf', '4353453434', 'dfsdfsd@rf.ll', '$2y$10$jMAmQZVO3b2cDXyZ8xT3jOKF4qRRuWvjIFzK/AVK8McAv2VSlU12S', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-08 04:24:54', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-08 04:24:54', '2019-04-08 04:24:54'),
(210, 'aabb', '1122112211', 'aabb@aa.bb', '$2y$10$aYpDT8mPWva3/BFH68syPuolLTA69gYiBuhNZuo5fLAlajvJm9eRS', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-08 05:40:49', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-08 05:40:49', '2019-04-08 05:40:49'),
(212, 'qqq', '1122223344', 'qqq@qq.qq', '$2y$10$u7ic81DDBO4GDoK3o1UItuzhPRBrFawlql3FlHa28ZAld5CFs/glK', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-08 06:34:33', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-08 06:34:33', '2019-04-08 06:34:33'),
(213, 'zzz', '4433556677', 'zzz@gmail.com', '$2y$10$y/X2aI9cXSFSK/BAof6UU.ZaXjP3xJqE5juoULdXNi4hVc9fjY/XG', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-08 07:20:19', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-08 07:20:19', '2019-04-08 07:20:19'),
(214, 'dfgd', '3453434534', 'dfgdf@ff.ll', '$2y$10$HF8UChfG4arDpfGr550ffe9/j27NtMnEg6Rs/7VdsrOf6z1Lizd4u', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-11 13:16:26', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-04-11 13:16:26', '2019-04-11 13:16:26'),
(215, 'hd', '99', 'murtaza_52f52@yahoo.com', '$2y$10$LutxL2k9HLsDX00JMhPg6O/T8gYrv3LO8myMBH/WFm/7qaJyq8BDK', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-05-05 13:36:58', '2019-05-05 13:36:58'),
(216, 'hd', '99', 'murtaza_52gf52@yahoo.com', '$2y$10$7MY5bc0pon7gicH8TLReTez7HPsOsmafDmPQKW6SMQnQPK0fk3Hci', NULL, NULL, 0, NULL, NULL, NULL, 1, '2019-04-11 13:16:26', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'ma3SG6aipwfqeo7HFN2N4vCz90UYzlCUBtbAxyV5xcDsUasNM6HNnIVNDxHR', '2019-05-05 13:37:32', '2019-05-05 13:37:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_agent_addon_commissions`
--

CREATE TABLE `user_agent_addon_commissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cross_selling_product_id` int(11) DEFAULT NULL,
  `percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `amount` double DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_agent_addon_commissions`
--

INSERT INTO `user_agent_addon_commissions` (`id`, `user_id`, `cross_selling_product_id`, `percentage_or_amount`, `amount`, `created_at`, `updated_at`) VALUES
(1, 112, 1, 2, 520, '2019-04-07 23:23:42', '2019-04-07 23:23:42'),
(2, 114, 1, 2, 520, '2019-04-07 23:23:42', '2019-04-07 23:23:42'),
(4, 112, 2, 1, 44, '2019-04-07 23:25:46', '2019-04-07 23:25:46'),
(5, 114, 2, 1, 44, '2019-04-07 23:25:46', '2019-04-07 23:25:46'),
(9, 205, 1, 2, 521, '2019-04-07 23:29:51', '2019-04-07 23:29:51'),
(10, 205, 2, 1, 441, '2019-04-07 23:29:51', '2019-04-07 23:29:51'),
(15, 207, 1, 2, 50, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(16, 207, 2, 1, 4, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(17, 112, 3, 1, 44, '2019-04-07 23:56:41', '2019-04-07 23:56:41'),
(18, 114, 3, 1, 44, '2019-04-07 23:56:41', '2019-04-07 23:56:41'),
(19, 205, 3, 1, 44, '2019-04-07 23:56:41', '2019-04-07 23:56:41'),
(20, 207, 3, 1, 44, '2019-04-07 23:56:41', '2019-04-07 23:56:41'),
(21, 112, 4, 2, 15, '2019-04-08 05:38:17', '2019-04-08 05:38:17'),
(22, 114, 4, 2, 15, '2019-04-08 05:38:17', '2019-04-08 05:38:17'),
(23, 205, 4, 2, 15, '2019-04-08 05:38:17', '2019-04-08 05:38:17'),
(24, 207, 4, 2, 15, '2019-04-08 05:38:17', '2019-04-08 05:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_agent_commissions`
--

CREATE TABLE `user_agent_commissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `insurance_provider_id` int(11) DEFAULT NULL,
  `percentage_or_amount` tinyint(4) DEFAULT 0 COMMENT '0 = N/A, 1 = Percentage, 2 = Amount',
  `amount` double DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_agent_commissions`
--

INSERT INTO `user_agent_commissions` (`id`, `user_id`, `insurance_provider_id`, `percentage_or_amount`, `amount`, `created_at`, `updated_at`) VALUES
(1, 112, 15, 1, 52, '2019-04-07 23:17:35', '2019-04-07 23:17:35'),
(2, 114, 15, 1, 52, '2019-04-07 23:17:35', '2019-04-07 23:17:35'),
(13, 205, 15, 1, 52, '2019-04-07 23:29:51', '2019-04-07 23:29:51'),
(40, 207, 15, 1, 52, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(39, 207, 14, 1, 3, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(38, 207, 12, 2, 22, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(37, 207, 6, 1, 0, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(36, 207, 5, 1, 0, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(35, 207, 4, 1, 0, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(34, 207, 3, 1, 0, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(33, 207, 2, 1, 11, '2019-04-07 23:38:35', '2019-04-07 23:38:35'),
(32, 207, 1, 1, 11, '2019-04-07 23:38:35', '2019-04-07 23:38:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additionalcovers`
--
ALTER TABLE `additionalcovers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_categories`
--
ALTER TABLE `addon_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_docs`
--
ALTER TABLE `addon_docs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_orders`
--
ALTER TABLE `addon_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `agent_users`
--
ALTER TABLE `agent_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_masters`
--
ALTER TABLE `benefit_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_makes`
--
ALTER TABLE `car_makes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_models`
--
ALTER TABLE `car_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model_body_types`
--
ALTER TABLE `car_model_body_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model_body_type_keywords`
--
ALTER TABLE `car_model_body_type_keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model_engine_sizes`
--
ALTER TABLE `car_model_engine_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model_fuels`
--
ALTER TABLE `car_model_fuels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_model_variants`
--
ALTER TABLE `car_model_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_premiums`
--
ALTER TABLE `car_premiums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_premium_agency_available_for_years`
--
ALTER TABLE `car_premium_agency_available_for_years`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_premium_variants`
--
ALTER TABLE `car_premium_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_premium_variant_logs`
--
ALTER TABLE `car_premium_variant_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_third_party_covers`
--
ALTER TABLE `car_third_party_covers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_values`
--
ALTER TABLE `car_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cross_selling_products`
--
ALTER TABLE `cross_selling_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_logs`
--
ALTER TABLE `email_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_template_bases`
--
ALTER TABLE `email_template_bases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excesses`
--
ALTER TABLE `excesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepages`
--
ALTER TABLE `homepages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_providers`
--
ALTER TABLE `insurance_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_provider_noclaim_bonuses`
--
ALTER TABLE `insurance_provider_noclaim_bonuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_addons`
--
ALTER TABLE `log_addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_new`
--
ALTER TABLE `orders_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_update_histories`
--
ALTER TABLE `order_update_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`);

--
-- Indexes for table `plan_benefits`
--
ALTER TABLE `plan_benefits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_benefits_plan_benefits_partners_id_foreign` (`plan_benefits_partners_id`);

--
-- Indexes for table `plan_benefits_copy`
--
ALTER TABLE `plan_benefits_copy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_benefits_plan_benefits_partners_id_foreign` (`plan_benefits_partners_id`);

--
-- Indexes for table `plan_benefits_multiple`
--
ALTER TABLE `plan_benefits_multiple`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_benefits_multiple_copy`
--
ALTER TABLE `plan_benefits_multiple_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_benefits_partners`
--
ALTER TABLE `plan_benefits_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_benefits_partners_copy`
--
ALTER TABLE `plan_benefits_partners_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_car_details`
--
ALTER TABLE `policy_car_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_details`
--
ALTER TABLE `policy_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_docs`
--
ALTER TABLE `policy_docs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_documents`
--
ALTER TABLE `policy_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_document_masters`
--
ALTER TABLE `policy_document_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_leads`
--
ALTER TABLE `policy_leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_payment_details`
--
ALTER TABLE `policy_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_renewal_details`
--
ALTER TABLE `policy_renewal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_types`
--
ALTER TABLE `policy_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy_user_details`
--
ALTER TABLE `policy_user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prohibited_routes`
--
ALTER TABLE `prohibited_routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `transaction_logs`
--
ALTER TABLE `transaction_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_agent_addon_commissions`
--
ALTER TABLE `user_agent_addon_commissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_agent_commissions`
--
ALTER TABLE `user_agent_commissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additionalcovers`
--
ALTER TABLE `additionalcovers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `addon_categories`
--
ALTER TABLE `addon_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `addon_docs`
--
ALTER TABLE `addon_docs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `addon_orders`
--
ALTER TABLE `addon_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `agent_users`
--
ALTER TABLE `agent_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `benefit_masters`
--
ALTER TABLE `benefit_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `car_makes`
--
ALTER TABLE `car_makes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_models`
--
ALTER TABLE `car_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `car_model_body_types`
--
ALTER TABLE `car_model_body_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `car_model_body_type_keywords`
--
ALTER TABLE `car_model_body_type_keywords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `car_model_engine_sizes`
--
ALTER TABLE `car_model_engine_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_model_fuels`
--
ALTER TABLE `car_model_fuels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `car_model_variants`
--
ALTER TABLE `car_model_variants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `car_premiums`
--
ALTER TABLE `car_premiums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `car_premium_agency_available_for_years`
--
ALTER TABLE `car_premium_agency_available_for_years`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `car_premium_variants`
--
ALTER TABLE `car_premium_variants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `car_premium_variant_logs`
--
ALTER TABLE `car_premium_variant_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_third_party_covers`
--
ALTER TABLE `car_third_party_covers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `car_values`
--
ALTER TABLE `car_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cross_selling_products`
--
ALTER TABLE `cross_selling_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `email_logs`
--
ALTER TABLE `email_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_template_bases`
--
ALTER TABLE `email_template_bases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `excesses`
--
ALTER TABLE `excesses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `homepages`
--
ALTER TABLE `homepages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `insurance_providers`
--
ALTER TABLE `insurance_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `insurance_provider_noclaim_bonuses`
--
ALTER TABLE `insurance_provider_noclaim_bonuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `log_addons`
--
ALTER TABLE `log_addons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `orders_new`
--
ALTER TABLE `orders_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_update_histories`
--
ALTER TABLE `order_update_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `plan_benefits`
--
ALTER TABLE `plan_benefits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `plan_benefits_copy`
--
ALTER TABLE `plan_benefits_copy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `plan_benefits_multiple`
--
ALTER TABLE `plan_benefits_multiple`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `plan_benefits_multiple_copy`
--
ALTER TABLE `plan_benefits_multiple_copy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `plan_benefits_partners`
--
ALTER TABLE `plan_benefits_partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `plan_benefits_partners_copy`
--
ALTER TABLE `plan_benefits_partners_copy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_car_details`
--
ALTER TABLE `policy_car_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_details`
--
ALTER TABLE `policy_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_docs`
--
ALTER TABLE `policy_docs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `policy_documents`
--
ALTER TABLE `policy_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_document_masters`
--
ALTER TABLE `policy_document_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `policy_leads`
--
ALTER TABLE `policy_leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `policy_payment_details`
--
ALTER TABLE `policy_payment_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_renewal_details`
--
ALTER TABLE `policy_renewal_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_types`
--
ALTER TABLE `policy_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `policy_user_details`
--
ALTER TABLE `policy_user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prohibited_routes`
--
ALTER TABLE `prohibited_routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaction_logs`
--
ALTER TABLE `transaction_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `user_agent_addon_commissions`
--
ALTER TABLE `user_agent_addon_commissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_agent_commissions`
--
ALTER TABLE `user_agent_commissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
