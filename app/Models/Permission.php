<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;
use App\models\Role;
use Auth;


class Permission extends EntrustPermission
{
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Display a listing of the permissions based on permissions associated with current user
     *
     * @return \Illuminate\Http\Response
     */
    public static function getPermissions()
    {
        if ( Auth::user()->can(['admin', 'super-admin']) ){
            $permissions = SELF::where('name', '!=', 'super-admin')->get();
        } else {
            $permissions = SELF::whereNotIn('name', ['super-admin', 'admin'])->get();
        }
        return $permissions;
    }

}
