<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\InsuranceProvider as InsuranceProviderResource;
use App\Http\Resources\CarModelBodyType as CarModelBodyTypeResouce;
use App\Http\Resources\Collections\PlanBenefitCollection;
use App\Http\Resources\Collections\CarPremiumCollection;
use App\Models\CarPremium as carPremiumModel;
use Carbon\Carbon;

class CarPremium extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
    
        $now = Carbon::now();
        $carRegDate = Carbon::createFromFormat('d/m/Y', $request->carRegistrationDate);
        $getMonths = $carRegDate->diffInMonths($now);
        return [
            'id' => $this->id,
            // 'insurance_provider_id' => $this->insurance_provider_id,
            // 'car_model_body_type_id' => $this->car_model_body_type_id,
            // 'car_model_min_value' => $this->car_model_min_value,
            // 'car_model_max_value' => $this->car_model_max_value,
            // 'min_age' => $this->min_age,
            // 'max_age' => $this->max_age,
            'insuranceProvider' => new InsuranceProviderResource($this->insuranceProvider),
            'carModelBodyType' => new CarModelBodyTypeResouce($this->carModelBodyType),
            'planBenefits' => new PlanBenefitCollection($this->insuranceProvider->planBenefits),
            'premiumRates'=> $this->premiumRate($request->carValueInput, $getMonths)
        ];
    }
}
