<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'SiteController@index');
// Route::get('/contact-us', 'SiteController@contactUs')->name('contactUs');
// Route::get('/terms', 'SiteController@terms')->name('terms');
// Route::get('/privacy-policy', 'SiteController@privacyPolicy')->name('privacyPolicy');
// Route::get('/payment-refund-policy', 'SiteController@paymentRefundPolicy')->name('paymentRefundPolicy');
// Route::get('/disclaimer', 'SiteController@disclaimer')->name('disclaimer');

Auth::routes(['verify' => true]);

//Route::middleware(['verified', 'auth'])->get('/home', 'HomeController@index')->name('home');
Route::get('/start', 'HomeController@start')->name('start');
Route::any('/ajax-register', 'HomeController@createAccountAjax');
Route::post('/ajax-register-otp', 'HomeController@createAccountAjaxOTP');
Route::post('/ajax-car-makes', 'HomeController@getCarMakes')->name('ajaxGetCarMakes');
Route::post('/ajax-car-models', 'HomeController@getCarModels')->name('ajaxGetCarModels');
Route::post('/ajax-car-variants', 'HomeController@getCarVariants')->name('ajaxGetCarVariants');
Route::post('/ajax-car-values', 'HomeController@getCarValues')->name('ajaxGetCarValues');
Route::post('/ajax-save-car-leads', 'HomeController@saveCarLeads')->name('ajaxSaveCarLeads');
Route::post('/ajax-save-more-about-me', 'HomeController@saveMoreAboutMe')->name('ajaxSaveMoreAboutMe');
Route::post('/ajax-save-renewal-details', 'HomeController@saveRenewalDetails')->name('ajaxSaveRenewalDetails');
Route::post('/ajax-filter-detail', 'HomeController@getFilterResult')->name('ajaxGetFilterResult');
Route::get('/reset-password/{token}', 'UserController@resetPassword')->name('resetPassword');
Route::post('/reset-password-save','UserController@resetPasswordSave')->name('resetPasswordSave');

//premium policy page route
Route::get('/premiumplans', 'HomeController@getPremium')->name('premium');

//Route::post('ajaxGetData/{$page}','AdminCarMakeController@ajaxGetDatas');


Route::prefix('admin')->name('admin.')->group(function() {
    // Admin Auth
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('logout');
    // 
    // 
    Route::middleware(['role:super-admin|sub-admin'])->group(function () {
        //
        Route::get('/home', 'AdminController@index')->name('home');

        // Cars
        Route::post('/cars/make/models','AdminCarController@getCarMakeModels')->name('getCarMakeModels');
        Route::post('/cars/body_type/variants','AdminCarController@getVariantByBodyType')->name('getVariantByBodyType');
        // car section 
        Route::resource('/car','AdminCarController');
        Route::resource('/cars/car-makes','AdminCarMakeController');
        Route::resource('/cars/car-models','AdminCarModelController');
        Route::resource('/cars/car-model-variants','AdminCarModelVariantController');
        Route::resource('/cars/car-model-body','AdminCarModelBodyController');
        Route::resource('/cars/car-model-fuel','AdminCarModelFuelController');
        // 
        Route::post('/cars/car-body-keyword-exist','AdminCarModelBodyController@keywordExist')->name('carModelBodyKeywordExist');
        //partners 
        Route::resource('/partners/premium-plans','AdminCarPremiumController');
        Route::resource('/partners/third-party-covers','AdminThirdPartyCoverController');
        Route::resource('/partners/benefit-masters','AdminBenefitMasterController');
        Route::resource('/partners','AdminInsuranceProviderController');
        //
        Route::resource('/planbenefits','AdminPlanBenefitController');
        // 
        Route::resource('/excess','AdminExcessController');
        Route::resource('/cross-selling-products','AdminCrossSellingProductsController');
        //
        Route::resource('/homepages','HomepageController');
        // site users
        Route::resource('/users','AdminUserController');
        Route::get('/users/show/{id}','AdminUserController@show')->name('users.view');
        Route::get('/user/{user_id}/orders','AdminOrderController@index')->name('user.orders');
        Route::get('/user/order/show/{id}/{user_id}','AdminOrderController@show')->name('user.order.show');
        // site users agents
        Route::get('/agents','AdminUserController@agentsList')->name('agents');
        Route::get('/agents/create','AdminUserController@createAgent')->name('create.agent');
        Route::get('/agents/{id}/edit','AdminUserController@editAgent')->name('edit.agent');
        Route::post('/agent/add','AdminUserController@addAgent')->name('add.agent');
        Route::post('/agent/update/{id}','AdminUserController@updateAgent')->name('update.agent');
        Route::post('password-sent','AdminUserController@passwordSent')->name('passwordSent');
        Route::get('/agents/showAgentsUser/{id}','AdminUserController@showAgentsUser')->name('agents.showAgentsUser');//xxxxxxxxxxxxxxxxxxx
        // Leads
        Route::get('/leads','AdminPolicyLeadController@index')->name('leads');
        Route::get('/leads/show/{id}','AdminPolicyLeadController@show')->name('leads.view');
        // Order
        Route::resource('/orders','AdminOrderController');
        //Route::get('/orders/{id}/edit','AdminUserController@editAgent')->name('edit.agent');
        //Route::get('/orders/show/{id}','AdminOrderController@show')->name('orders.show');
        // Import data
        Route::get('/import/car-value',  'AdminImportCarValueController@import');
        Route::post('/import/car-value', 'AdminImportCarValueController@parseImport');
        Route::get('/import/car-value/show','AdminImportCarValueController@show')->name('carValue.view');
        // Report
        Route::get('/reports/commission','AdminReportController@commission')->name('reports.commission');
        Route::get('/reports/commission/agent','AdminReportController@agentcommission')->name('reports.commission.agent');
        
        // Email template data
        Route::resource('/email-templates','AdminEmailTemplateController');
        Route::post('/email-templates/preview','AdminEmailTemplateController@preview')->name('email-templates.preview');;
        // Page/CMS
        Route::resource('/pages','AdminPageController');
        Route::any('/pages/suggest_slug','AdminPageController@suggestUniqueSlug')->name('pages.suggestSlug');

        // NEED TO CHECK
        Route::post('changehomepage','HomepageController@editHomePage')->name('editHomePage');
        //Route::resource('sites','SiteController');
        //Route::post('retrievedatathirdparty','AdminThirdPartyCoverController@getDataThirdParty');
        Route::post('retrievedatathirdparty','AdminThirdPartyCoverController@getDataThirdParty')->name('getDataThirdParty');
        Route::post('retrievedatacarpremium','AdminCarPremiumController@getDataCarPremium')->name('getDataCarPremium');
        Route::post('addplanbenefit','AdminPlanBenefitController@addFormPlanBenefit')->name('addFormPlanBenefit');
        Route::post('editplanbenefit','AdminPlanBenefitController@editFormPlanBenefit')->name('editFormPlanBenefit');

        //roles
        Route::resource('/roles', 'Auth\RoleController');
        // 
        Route::resource('/admin-users','AdminPanelUserController');

        //document master
        Route::resource('/policydocumentmaster','AdminPolicyDocumentMasterController');
        //Addon Category
        Route::resource('/addoncategory','AdminAddonCategory');
    });
});

Route::post('/paymentdata','HomeController@paymentdata');
Route::get('/paymentpage','HomeController@payment');

Route::post('/orderdata','HomeController@orderDetails')->name('order-submit');

Route::prefix('agent')->group(function() {
    Route::middleware('auth','permission:agent')->get('dashboard', 'HomeController@dashboard')->name('agent-dashboard');
    Route::middleware('auth','permission:agent')->get('dashboard/my-policy', 'HomeController@mypolicy')->name('agent-mypolicy');
    Route::middleware('auth','permission:agent')->get('dashboard/my-myprofile', 'HomeController@myprofile')->name('agent-myprofile');
    Route::middleware('auth','permission:agent')->post('/dashboard/my-profile/update','HomeController@updateprofile')->name('agent-update-profile');
    Route::middleware('auth','permission:agent')->get('dashboard/agent-commission-report', 'HomeController@agentcommissionreport')->name('agent-commission-report');
});

Route::prefix('user')->group(function() {
    Route::middleware('auth','permission:user')->get('dashboard', 'HomeController@dashboard')->name('user-dashboard');
    Route::middleware('auth','permission:user')->get('dashboard/my-policy', 'HomeController@mypolicy')->name('user-mypolicy');
    Route::middleware('auth','permission:user')->get('dashboard/my-profile', 'HomeController@myprofile')->name('user-myprofile');

    Route::middleware('auth','permission:user')->post('/dashboard/my-profile/update','HomeController@updateprofile')->name('user-update-profile');
});

Route::post('/orderdata','HomeController@orderDetails')->name('order-submit');
// Route::get('/orderdata','HomeController@orderSave');

//Route::get('/dashboard', 'UserController@dashboard');

Route::get('please-verify-email', function(){
    return view('auth.verify');
});

//buynow for policy
Route::get('/buynow','HomeController@upload');

//upload of file
Route::post('/upload', 'HomeController@uploadFile');
//Route::post('/uploaddocument', 'HomeController@uploadedDocuments');
Route::post('/uploaddocuments', 'HomeController@uploadedPolicyDocuments')->name('uploaded-policy-documents');


//filter data for car value in policy detail page
Route::post('/filtercarvalue', 'HomeController@filterCarValue');

//store payment details of paymnet page 
Route::post('/storepaydetails','HomeController@storePayment');
Route::post('/update-paymentpage','HomeController@updatePayment');

// always at the bottom of the page
Route::get('{slug}', 'PageController@view');

if (env('APP_ENV') <> "production") {
    Route::get('/test/testing', 'TestController@testing');
}