<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\CarPremium;
use App\Models\PlanBenefit;
use App\Models\PlanBenefitsPartner;
use App\Models\BenefitMaster;
use App\Models\CarThirdPartyCover;
use App\Models\Excess;

class InsuranceProvider extends Model
{
    public function scopeRecomended($query)
    {
        return $query->where('recommended_partner', 1);
    }
    public function scopeNonRecomended($query)
    {
        return $query->where('recommended_partner', '!=', 1)->orWhereNull('recommended_partner');
    }
    public function carPremium()
    {
        return $this->hasMany(CarPremium::class, 'insurance_provider_id', 'id');
    }

    public function carThirdPartyCover()
    {
        return $this->hasMany(carThirdPartyCover::class, 'insurance_provider_id', 'id');
    }
  
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    
    public function planBenefitsPartners()
    {
      return $this->hasMany(PlanBenefitsPartner::class, 'insurance_providers_id', 'id')->active()->comprehensive()->effectiveDate();
    }

    public function thirdPartyplanBenefitsPartner()
    {
      return $this->hasMany(PlanBenefitsPartner::class, 'insurance_providers_id', 'id')->active()->planType()->effectiveDate();
    }

    public function benefitMasters()
    {
        return $this->hasManyThrough(BenefitMaster::class, PlanBenefit::class, 'insurance_providers_id', 'id', 'id', 'benefit_masters_id' );
    }

    public function excess()
    {
        return $this->hasMany( Excess::class, 'insurance_provider_id', 'id');
    }

    public function order() {
        return $this->hasMany('App\Models\Order', 'insurance_providers_id', 'id');
    }

    public function crossSellingProducts() {
        return $this->hasMany('App\Models\CrossSellingProducts', 'insurance_provider_id', 'id');
    }

    public function userAgentCommission() {
        return $this->hasMany('App\Models\UserAgentCommission', 'insurance_provider_id', 'id');
    }
}
