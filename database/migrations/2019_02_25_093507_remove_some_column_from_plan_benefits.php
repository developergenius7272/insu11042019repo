<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSomeColumnFromPlanBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_benefits', function (Blueprint $table) {
            $table->dropColumn(['insurance_providers_id','year','effective_date','plan_type','sub_type','status']);
            $table->unsignedInteger('plan_benefits_partners_id');
            $table->foreign('plan_benefits_partners_id')->references('id')->on('plan_benefits_partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_benefits', function (Blueprint $table) {
            //
        });
    }
}
