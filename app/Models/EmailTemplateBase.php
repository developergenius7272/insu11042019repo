<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateBase extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_template_bases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
