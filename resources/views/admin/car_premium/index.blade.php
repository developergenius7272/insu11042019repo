@extends('layouts.admin.master')
{{-- car_premium plans table view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>{{$tableHeading ?? 'Cars'}}</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{-- <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p> --}}
                        
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Partner</th>
                                    <th>Body Type</th>
                                    {{-- <th>Agency/Non Agency Basic Rate</th>
                                    <th>Agency/Non Agency Min Premium</th> --}}
                                    <th>Basic Rate</th>
                                    <th>Min Premium</th>
                                    <th>Value Range</th>
                                    <th>Age Range</th>
                                    <th>Sub Type</th>
                                    <th>Effective Date</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($plans)
                                @foreach($plans as $plan)
                                <tr>
                                    <td>{{ $plan->partner_name }}</td>
                                    <td>{{ $plan->car_body_name }}</td>
                                    <td>{{ $plan->min_premium_agency_rate }}% / {{ $plan->min_premium_nonagency_rate }}%</td>
                                    <td>{{ number_format($plan->min_premium_agency) }} / {{ number_format($plan->min_premium_nonagency) }}</td>
                                    <td>{{ number_format($plan->car_model_min_value) }} - {{ number_format($plan->car_model_max_value) }}</td>
                                    <td>{{ $plan->min_age }} - {{ $plan->max_age }}</td>
                                    {{-- <td>
                                        {{ $plan->car_model_sub_type == 0 ? 'Standard' : 'Superior' }}
                                    </td>       --}}                                  
                                    <td>
                                        {{  $plan->car_model_sub_type == 0 ? 'Not Applicable' : ($plan->car_model_sub_type == 1 ? 'Standard' : 'Superior') }}
                                    </td>

                                    <td>{{ isset($plan->effective_date) ? $plan->effective_date->format('d/m/Y H:i') : '' }}</td>
                                    

                                    <td>
                                        {{ $plan->status == 0 ? 'Inactive' : 'Active' }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{action('AdminCarPremiumController@edit', $plan->id)}}">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{action('AdminCarPremiumController@destroy', $plan->id)}}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
		
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
    $(document).ready(function() {
        
        pageSetUp();
        
        /* // DOM Position key index //
    
        l - Length changing (dropdown)
        f - Filtering input (search)
        t - The Table! (datatable)
        i - Information (records)
        p - Pagination (paging)
        r - pRocessing 
        < and > - div elements
        <"#id" and > - div with an id
        <"class" and > - div with a class
        <"#id.class" and > - div with an id and class
        
        Also see: http://legacy.datatables.net/usage/features
        */	

        /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "order": [[ 7, 'desc' ]],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

        /* END BASIC */
        
        // /* COLUMN FILTER  */
        // var otable = $('#datatable_fixed_column').DataTable({
        //     //"bFilter": false,
        //     //"bInfo": false,
        //     //"bLengthChange": false
        //     //"bAutoWidth": false,
        //     //"bPaginate": false,
        //     //"bStateSave": true // saves sort state using localStorage
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_fixed_column) {
        //             responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_fixed_column.respond();
        //     }		
        
        // });
        
        // // custom toolbar
        // $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
                
        // // Apply the filter
        // $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
            
        //     otable
        //         .column( $(this).parent().index()+':visible' )
        //         .search( this.value )
        //         .draw();
                
        // } );
        // /* END COLUMN FILTER */   
    
        // /* COLUMN SHOW - HIDE */
        // $('#datatable_col_reorder').dataTable({
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_col_reorder) {
        //             responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_col_reorder.respond();
        //     }			
        // });
        
        // /* END COLUMN SHOW - HIDE */

        // /* TABLETOOLS */
        // $('#datatable_tabletools').dataTable({
            
        //     // Tabletools options: 
        //     //   https://datatables.net/extensions/tabletools/button_options
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },		
        //     "oTableTools": {
        //             "aButtons": [
        //             "copy",
        //             "csv",
        //             "xls",
        //             {
        //                 "sExtends": "pdf",
        //                 "sTitle": "SmartAdmin_PDF",
        //                 "sPdfMessage": "SmartAdmin PDF Export",
        //                 "sPdfSize": "letter"
        //             },
        //             {
        //                 "sExtends": "print",
        //                 "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
        //             }
        //             ],
        //         "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        //     },
        //     "autoWidth" : true,
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_tabletools) {
        //             responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_tabletools.respond();
        //     }
        // });
        
        // /* END TABLETOOLS */
    
    })

</script>
@endsection
