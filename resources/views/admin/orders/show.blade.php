@extends('layouts.admin.master')
{{-- Car model variants table view --}}
@section('content')
<div class="row">
    <div>
       @if ($errors->any())
           <div class="alert alert-danger">
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </div>
       @endif
   </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Leads View</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" method="POST">
                            {{--  --}}
                            <fieldset>
                                <legend>Order Detail</legend>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Purchase Date</label>
                                    <div class="col-md-6">
                                        <label class="col-md-6 lbl">{{ isset($data->created_at) ? $data->created_at->format('d/m/Y H:i') : '' }}</label>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Partner</label>
                                    <div class="col-md-6">
                                        <label class="col-md-6 lbl">{{ $data->insuranceProvider ? $data->insuranceProvider->name : $data->name  }}</label>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">User</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->user ? $data->user->name : $data->name }}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Agent</label>
                                    <div class="col-md-6">
                                        {{-- <label class="col-md-4 lbl">InsureOnline (Admin)</label> --}}
                                        <label class="col-md-4 lbl">{{ $data->agent ?  $data->agent->name : 'InsureOnline (Admin)' }}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Premium Value</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->premium_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->premium_value,2) : 'N/A' }}</label>
                                    
                                    </div>
                                </div>                                

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Passenger Cover</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->pab_passenger > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->pab_passenger,2) : 'N/A' }}</label>
                                    
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Driver Cover</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->pab_driver > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->pab_driver,2) : 'N/A' }}</label>
                                    
                                    </div>
                                </div>                                

                                <div class="form-group">
                                    <label class="col-md-3 control-label">VAT {{ env('VAT', '5').'%' }}</label>
                                    <div class="col-md-6">
                                        {{-- <label class="col-md-4 lbl">{{ $data->vat_premium }}</label> --}}
                                        <label class="col-md-4 lbl">{{ $data->vat_premium > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->vat_premium,2) : 'N/A' }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Policy Fees</label>
                                    <div class="col-md-6">
                                        {{-- <label class="col-md-4 lbl">{{ $data->policy_fees }}</label> --}}
                                        <label class="col-md-4 lbl">{{ $data->policy_fees > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->policy_fees,2) : 'N/A' }}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->total > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->total,2) : 'N/A' }}</label>
                                    </div>
                                </div>


                                {{--  --}}
                            </fieldset>

                            {{-- Policy Info --}}
                            <fieldset>
                                <legend>Policy's Info</legend>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Policy No</label>
                                    <div class="col-md-6">
                                        <label class="col-md-6 lbl">
                                            {{ $data->policy_number > 0 ? $data->policy_number : 'N/A' }}
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Start Date</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">
                                            {{ isset($data->start_date) ? $data->start_date->format('d/m/Y H:i') : 'N/A' }}
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Expiry Date</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">
                                            {{ isset($data->end_date) ? $data->end_date->format('d/m/Y H:i') : 'N/A' }}
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Issue Date</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">
                                            {{ isset($data->issue_date) ? $data->issue_date->format('d/m/Y H:i') : 'N/A' }}
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">
                                            {{ $data->status == 0 ? 'Pending' : ($data->status == 1 ? 'Processing' : 'Processed') }}
                                        </label>

                                    </div>
                                </div>


                                {{--  --}}
                            </fieldset>

                            {{-- Partner Info --}}
                            <fieldset>
                                <legend>Partner's Info</legend>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Base Commission</label>
                                    <div class="col-md-6">
                                        <label class="col-md-6 lbl">
                                            @if($data->base_commission_amount > 0)
                                                {{ env('CURRENCY_CODE', 'AED').' '.number_format($data->base_commission_amount,2) }}
                                            @elseif($data->base_commission_percentage > 0)
                                                {{ $data->base_commission_percentage. ' %' }}
                                            @endif
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Policy Fees</label>
                                    <div class="col-md-6">
                                        <label class="col-md-6 lbl">
                                            @if($data->partner_policy_amount > 0)
                                                {{ env('CURRENCY_CODE', 'AED').' '.number_format($data->partner_policy_amount,2) }}
                                            @elseif($data->partner_policy_percentage > 0)

                                                @if($data->partner_max_amount)
                                                    {{ $data->partner_policy_percentage. ' % max upto '. env('CURRENCY_CODE', 'AED').' '.number_format($data->partner_max_amount,2) }}
                                                @else
                                                    {{ $data->partner_policy_percentage. ' %' }}
                                                @endif

                                            @endif
                                        </label>

                                    </div>
                                </div>
                                {{--  --}}
                            </fieldset>

                            </fieldset>

                            {{-- Partner Info --}}
                            <fieldset>
                                <legend>Excess</legend>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Excess</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">{{ $data->excess > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->excess,2) : 'N/A' }}</label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Add On</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">{{ $data->addon_amount > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->addon_amount,2) : 'N/A' }}</label>

                                    </div>
                                </div>
                                {{--  --}}
                            </fieldset>

                            {{-- Partner Info --}}
                            <fieldset>
                                <legend>Cross Selling Product and Agent Commission</legend>

                                @php $agent_commission_value  = 0;  @endphp
                                @php $covers = $data->additionalCovers; @endphp
                                {{-- @if($covers) --}}
                                @if(isset($covers) && count($covers))
                                    <table class="table table-striped table-bordered table-hover">
                                      <tr>
                                            {{-- <th>Sr.No.</th> --}}
                                            <th>Name</th>
                                            <th>Premium</th>
                                            <th>Addon Commission</th>
                                            
                                      </tr>
                                      <tbody>
                                        
                                            @foreach($covers as $cover)
                                                
                                              <tr>
                                                <td>{{ $cover->crossSellingProduct->name }}</td>
                                                <td>{{ $cover['premium_value'] }}</td>
                                                <td>{{ $cover['agent_commission_value'] }}</td>
                                              </tr>
                                              @php $agent_commission_value += $cover['agent_commission_value']; @endphp
                                            @endforeach
                                        
                                      </tbody>
                                    </table>
                                @else
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Cross Selling Porduct</label>
                                        <div class="col-md-6">
                                            <label class="col-md-6 lbl">Nothing</label>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Agent Commission</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">{{ $data->agent_commission_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($data->agent_commission_value,2) : 'N/A' }} </label>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Agent Addon Commission</label>
                                    <div class="col-md-6">

                                        <label class="col-md-6 lbl">{{ $agent_commission_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($agent_commission_value,2) : 'N/A' }} </label>

                                    </div>
                                </div>
                                
                            </fieldset>
                            <br>
                            <br>
                            {{-- {{ Policy Document upload }} --}}
                            <fieldset>
                                <legend>Policy Document upload</legend>

                                @php $uploads = $data->upload; $i = 1; @endphp

                                @if(isset($uploads) && count($uploads))
                                    <table class="table table-striped table-bordered table-hover">
                                      <tr>
                                            <th>Sr.No.</th>
                                            <th>Policy Document Name</th>
                                            <th>File</th>
                                            
                                      </tr>
                                      <tbody>
                                        
                                            @foreach($uploads as $upload)
                                                
                                              <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $upload->policy_document_name }}</td>
                                                <td>
                                                    
                                                    <a href="{{ asset('uploads/admin/userdoc').'/'.$upload->filename }}" target="_blank"><img id="imgUsers" src='{{ asset('uploads/admin/userdoc').'/'.$upload->filename }}' width=50 height=50></a>
                                                </td>

                                              </tr>
                                              @php $i++; @endphp
                                            @endforeach
                                        
                                      </tbody>
                                    </table>
                                @else
                                    No Documents uploaded
                                @endif

                            </fieldset>

                            {{-- Benefit Options --}}
                            <fieldset>
                                <legend>Benefit Options</legend>

                                @if($arrs)
                                    @foreach($arrs as $arr)

                                        @if(!empty($arr['name']))
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">{{ $arr['name'] }}</label>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <label class="col-md-4 lbl">{{ $arr['value'] }}</label>
                                                    </div>
                                                </div>
                                            </div> 
                                        @endif

                                    @endforeach
                                @endif

                            </fieldset>




                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection
<style>
    .lbl {padding-top:7px;}
    /*.row.myrow {
        margin-left: 321px;
        margin-top: 12px;
    }    */
</style>
@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

    });


</script>
@endsection