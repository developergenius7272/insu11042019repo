@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formThirdParty" method="POST" @if($formType == 'edit') action="{{action('AdminThirdPartyCoverController@update', $data->id)}}" @else action="{{url('/admin/partners/third-party-covers')}}" @endif>
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}
                            <fieldset>
                                <legend>Partner</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="insuranceProviderId">Insurance Provider / Partner</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="insuranceProviderId" name="insuranceProviderId">
                                            <option value="">Select Partner</option>
                                            @if(isset($partners) && count($partners))
                                                @foreach($partners as $partner)
                                                    <option value="{{$partner->id}}" 
                                                        @if(old('insuranceProviderId') == $partner->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($partner->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                            </fieldset>
                            {{-- Car related --}}
                            <fieldset>
                                <legend>Cars</legend>
                                {{-- car body --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Select Car Model Body Type</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                @if($formType == 'edit')
                                                    <select class="form-control" id="carBodyTypeId" name="carBodyTypeId">
                                                        <option value="">Select Car Type</option>
                                                        @if(isset($carBodies) && count($carBodies))
                                                            @foreach($carBodies as $body)
                                                                <option value="{{$body->id}}" @if(isset($data) && $data->car_model_body_type_id == $body->id) selected @endif>{{strtoupper($body->name)}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                @else
                                                  <select class="form-control" id="carBodyTypeId" name="carBodyTypeId[]" multiple="multiple">
                                                        <!--option value="">Select Car Type</option-->
                                                        {{-- @if(isset($carBodies) && count($carBodies))
                                                            @foreach($carBodies as $body)
                                                                <option value="{{$body->id}}" @if(isset($data) && $data->car_model_body_type_id == $body->id) selected @endif>{{strtoupper($body->name)}}</option>
                                                            @endforeach
                                                        @endif --}}

                                                        @if(isset($carBodies) && count($carBodies))

                                                        @foreach($carBodies as $body)
                                                            @if($errors->any())
                                                                @php ($is_record_found = 'no')

                                                                @if(old('carBodyTypeId'))
                                                                    @foreach(old('carBodyTypeId') as $val)
                                                                        @if($val == $body->id) 
                                                                            @php ($is_record_found = 'yes')
                                                                            @break
                                                                        @endif
                                                                    @endforeach
                                                                @endif

                                                                <option value="{{$body->id}}" 
                                                                    @if ($is_record_found == 'yes')
                                                                        {{ 'selected' }} 
                                                                    @endif
                                                                    >{{strtoupper($body->name)}}
                                                                </option>

                                                            @else
                                                                <option value="{{$body->id}}" 
                                                                    {{-- @if(isset($data) && $data->car_model_body_type_id == $body->id)     selected 
                                                                    @endif --}}
                                                                >{{strtoupper($body->name)}}
                                                            </option>
                                                            @endif
                                                        @endforeach

                                                    @endif
                                                    </select>
                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {{-- cylinders --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Cylinders</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Number of Cylinders" name="cylinders" id="cylinders" value="{{ $errors->any() ? old('cylinders') : $data->car_model_cylinder ?? '' }}" type="number" min="1">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--excess cylinders--}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Above Cylinders</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                               <input type="checkbox" name="above_cylinder" value="1"
                                                    @if(old('above_cylinder') == 1)
                                                         {{ 'checked' }}
                                                    @elseif(isset($data->above_cylinder) &&                           $data->above_cylinder == 1)
                                                            {{ 'checked' }}
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- rate--}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Rate</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="rate" name="rate" id="rates" value="{{ $errors->any() ? old('rate') : $data->rate ?? '' }}" type="number" min="0" step=".01" >

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- effective date--}}
                                {{--  <div class="form-group">
                                    <label class="control-label col-md-2">Effective Date</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="date" name="effective_date" id="eff_dates" value="{{$data->effective_date ?? ''}}" type="date" >

                                            </div>
                                        </div>
                                    </div>
                                </div>  --}}
                                    <div class="form-group">
                                    <label class="control-label col-md-2">Effective Date</label>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" dbDate="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}"
                                                name="effective_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Age Range (optional)</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Minimum Age</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Minimum Age" name="min_age" id="min_age" type="number" min="1" value="{{ $errors->any() ? old('min_age') : $data->min_age ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Maximum Age</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Maximum Age" name="max_age" id="max_age" type="number" min="1" value="{{ $errors->any() ? old('max_age') : $data->max_age ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend>Driving License Experience (optional)</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Min License Experience</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Min License Experience" name="driving_license_min_age" id="min_age" type="number" min="1" value="{{ $errors->any() ? old('driving_license_min_age') : $data->driving_license_min_age ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group" style="display:none">
                                    <label class="control-label col-md-2">Max License Experience</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Max License Experience" name="driving_license_max_age" id="max_age" type="number" min="1" value="{{ $errors->any() ? old('driving_license_max_age') : $data->driving_license_max_age ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            {{--  --}}

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/partners/third-party-covers')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
     <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

         $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });

        var availableTags = [];
        var newElement = '';
        @if( count($cylindersGroup) )
            @foreach($cylindersGroup as $cylinder)
                newElement = "{{$cylinder->cylinders}}"
                availableTags.push(newElement);
            @endforeach
        @endif
        $( "#cylinders" ).autocomplete({
          source: availableTags
        });

        $("#formThirdParty").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }
        }));

    });

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var insuranceProviderId  = $.trim($("#insuranceProviderId").val());
        var t= $.trim($("#datetimepicker").val());
        var rates  = $.trim($("#rates").val());

        if (insuranceProviderId == ''){

            $("#insuranceProviderId").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insuranceProviderId').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        @if($formType == 'add')
            var carBodyTypeId = $('#carBodyTypeId option:selected').index();
            if (carBodyTypeId < 0){
                
                $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
                $('#carBodyTypeId').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        @else
            // var carBodyTypeId = $('#carBodyTypeId').prop('selectedIndex');

            // if (carBodyTypeId == 0){
                
            //     $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
            //     is_valid = false;
            // }
        @endif

        //effective date checking
        if($.trim(t).length>0)
        {
            @if($formType == 'add')
 
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
                if(currentDateTime.getTime() > effectiveDate.getTime())
                { 
                    $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                    is_valid = false;
                }

            @else

                var dbDateText = $("#datetimepicker").attr('dbdate');
                var dbDate  = textToDate(dbDateText)
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
 
                if(dbDate.getTime() != effectiveDate.getTime()){
                    if(currentDateTime.getTime() > effectiveDate.getTime())
                    {
                        $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                        is_valid = false;
                    }
                }


            @endif
            
        }
        else
        {
            //alert("Please Enter valid Effective Date");
            $("#datetimepicker").parent().find(".my-error").html('Please enter effective date').show();
            $('#datetimepicker').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (rates.length > 0){

            if(!$.isNumeric( rates ) || rates < 0){
                $("#rates").parent().find(".my-error").html('Please enter rate in numeric').show();
                $('#rates').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        else{
            $("#rates").parent().find(".my-error").html('Please enter rate').show();
            $('#rates').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        return is_valid;
    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }

    function textToDate(t){
           var ary= t.split(" ");
                var aryDate=ary[0].split("/");
                var aryTime=ary[1].split(":");
                var convertedDate = new Date(aryDate[2], 0, aryDate[0], 0, 0, 0, 0);
                //new Date(year, month, day, hours, minutes, seconds, milliseconds)
                convertedDate.setMonth(aryDate[1]>0?aryDate[1]-1:aryDate[1] ); 

                return convertedDate;
    }

/*
    $("#formThirdParty").on('submit',(function(e) {

        var action = 0;

        var formType = "{{$formType}}";

        if (formType == 'add'){
            $.ajax(
            {

                type:'POST',
                //url:'/admin/retrievedatathirdparty',
                url:'{{route('admin.getDataThirdParty')}}',
                //data:{insuranceProviderId:insuranceProviderId,carBodyTypeIdVal:carBodyTypeIdVal},
                  data: new FormData(this),
                    processData: false,
                    contentType: false,
                  async : false,

                success: function(response){

                    response = $.parseJSON(response);
                    var status = response.status;
                    var msg = '';

                    if( status == 0) {

                        var name_insurance_provider = response.name_insurance_provider;

                        var name_car_model_body_type = response.implodeBodyType;

                        msg = "Insurance Provider : "+ name_insurance_provider + " and Car Model Body type : "+ name_car_model_body_type + " already exist, do you still want to update ?";
                        
                        if (confirm(msg)) {
                            
                            action = 1;
                            //return true;

                        } else {
                            //alert('false');
                            return false;
                        }

                    }

                    if( status == 1 ) {
                      //alert('ok');
                      action = 1;

                    }

                }
            });

            if (action == 0){

                return false;
            }
        }

  }));
*/
</script>
@endsection
