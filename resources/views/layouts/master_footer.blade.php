<!-- Footer Start -->
<footer>
  <section class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h6>Get in Touch</h6>
          <ul class="footer-location-menu">
            <li><i class="fa fa-map-marker"></i>abc, Dubai</li>
            <li><a href="#"><i class="fa fa-phone"></i>+ 971 4433 3455</a></li>
            <li><a href="#"><i class="fa fa-fax"></i>+ 971 4433 3455</a></li>
          </ul>
        </div>
        <div class="col-md-4 text-center">
          <h6>Follow Us</h6>
          <ul class="footer-social-menu">
            <li><a target="blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
            <li><a target="blank" href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
            <li><a target="blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        <div class="col-md-4 text-right">
          <h6>Secure Payments</h6>
          <ul class="footer-payment-menu">
            <li><i class="fa fa-cc-visa"></i></li>
            <li><i class="fa fa-cc-mastercard"></i></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="footer-menu">
            <ul>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Terms of Service</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Payment & Refund Policies</a></li>
              <li><a href="#">Disclaimer</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="copyright">{!! env('COPYRIGHT') !!}</div>
      </div>
    </div>
  </section>
</footer>
<!-- Footer End --> 

<!-- JAVASCRIPT --> 
<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script> 

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- For Banner Slider --> 
<script src="{{ URL::asset('js/owlcarousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/scripts.js')}}?v={{env('APP_VERSION',1)}}"></script> 
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script>
<script src="{{ URL::asset('assets/upload/js/jquery.ui.widget.js') }}"></script>
<script src="{{ URL::asset('assets/upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ URL::asset('assets/upload/js/jquery.fileupload.js') }}"></script>
<script>
$(document).ready(function(){
//Company Logo Slider
$('.company-slider').owlCarousel({
  items:7,
  autoplay:true,
  loop:true,
  dots:false,
  nav:true,
  navText: [
    "<img src='{{ URL::asset('images/left-arrow.png') }}' alt='Prev' />",
    "<img src='{{ URL::asset('images/right-arrow.png') }}' alt='Prev' />",
  ],
  responsive:{
    0:{
      items:3,
    },
    600:{
      items:4,
    },
    768:{
      items:5,
    },
    992:{
      items:6,
    },
    1200:{
      items:7,
    },
  }
});

//Testimonnial Slider
$('.testimonial-slider').owlCarousel({
  items:1,
  autoplay:true,
  autoplayTimeout:10000,
  loop:true,
  dots:true,
  nav:true,
  navText: [
    "<img src='images/arrow-prev.png' alt='Prev' />",
    "<img src='images/arrow-next.png' alt='Next' />"
  ],
});

//Select Policy Slider
// $('.select-policy-slider').owlCarousel({
//   items:5,
//   autoplay:false,
//   loop:false,
//   dots:false,
//   nav:true,
//   navText: [
//     "<img src='images/left-arrow.png' alt='Prev' />",
//     "<img src='images/right-arrow.png' alt='Prev' />"
//   ],
//   responsive:{
//     0:{
//       items:1,
//     },
//     600:{
//       items:2,
//     },
//     768:{
//       items:3,
//     },
//     992:{
//       items:4,
//     },
//     1200:{
//       items:5,
//     },
//   }
// });

});
</script> 