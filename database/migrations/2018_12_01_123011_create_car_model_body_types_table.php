<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// 
use App\Models\CarModelBodyType;

class CreateCarModelBodyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_model_body_types');
        Schema::create('car_model_body_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });

         // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Saloon';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Hatch Back';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Station Wagon';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Four Wheel Drive 4WD';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'SUV';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Coupes';
        $CarModelBodyType->save();
        // 
        $CarModelBodyType = new CarModelBodyType();
        $CarModelBodyType->name = 'Sports';
        $CarModelBodyType->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_model_body_types');
    }
}
