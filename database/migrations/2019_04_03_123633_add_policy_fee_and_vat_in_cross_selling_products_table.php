<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolicyFeeAndVatInCrossSellingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cross_selling_products', function (Blueprint $table) {
            $table->double('vat')->after('name')->default(0)->nullable();
            $table->tinyInteger('fee_type')->after('vat')->default(0)->nullable()->comment = '0 = N/A, 1 = Percentage, 2 = Amount';
            $table->double('fee')->after('fee_type')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cross_selling_products', function (Blueprint $table) {
            //
        });
    }
}
