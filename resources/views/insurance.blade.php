@extends('layouts.master_insurance')
@section('script')
<script>

  var APP_URL = {!! json_encode(url('/')) !!}

  $( function() {
        $('#datepicker,#datepicker_dob,#datepicker_register').datetimepicker({
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false
        });
  } );
  $(document).ready(function() {
    //$('.customCheck1').change(function() {
    $('body').on('change','.btnAmount',function(e){

        var insurance_id = $(this).attr('data-insurance_id');
        var amtAgNonAg = $(this).attr('data-amtAgNonAg');

        var totalPriceAED = 0;
        var amountPlanBenefit = 0;

        if(amtAgNonAg == 'amtAg'){
          var priceVal = $("#clsPriceAg"+insurance_id).attr('data-priceaed');
        }
        else{
          var priceVal = $("#clsPriceNonAg"+insurance_id).attr('data-priceaed');  
        }

        if($(this).is(":checked")) {

          amountPlanBenefit = $(this).val();
          totalPriceAEDAg = (parseFloat(amountPlanBenefit)+parseFloat(priceVal));
          //totalPriceAEDNonAg = (parseFloat(amountPlanBenefit)+parseFloat(priceValNonAg));

          //$(this).removeClass('amountAdd').addClass('amountRemove').text('- Remove').removeClass('badge-success').addClass('badge-danger');

        }
        else{

          amountPlanBenefit = $(this).val();
          totalPriceAEDAg = parseFloat(priceVal) - parseFloat(amountPlanBenefit);
          //totalPriceAEDNonAg = parseFloat(priceValNonAg) - parseFloat(amountPlanBenefit);

          //$(this).removeClass('amountRemove').addClass('amountAdd').text('+ Add').removeClass('badge-danger').addClass('badge-success');
        }

        if(amtAgNonAg == 'amtAg'){
          $("#clsPriceAg"+insurance_id).html(format_number(totalPriceAEDAg));
          $("#clsPriceAg"+insurance_id).attr('data-priceaed', totalPriceAEDAg);
        }
        else{
          $("#clsPriceNonAg"+insurance_id).html(format_number(totalPriceAEDAg));
          $("#clsPriceNonAg"+insurance_id).attr('data-priceaed', totalPriceAEDAg);
        }

        //$('#textbox1').val($(this).is(':checked'));        
    });

    // step 2
    $('body').on('submit','#main_signup_form_step2',function(e){
      e.preventDefault();
      // 
      hideErrorDiv();
      // 
      var formData = $(this).serialize();
      var otp = $.trim($('#otp').val());
      // 
      if (otp == '') {
          $("#otp").addClass('is-invalid');
          $(".otp_error").addClass('invalid-tooltip').html('Please enter your registered OTP.').show();
          return false;

      } else {
          $(".otp_error").hide();
          $.ajax({
              type: "POST",
              url: 'ajax-register-otp',
              dataType: "json",
              data: formData,
              beforeSend :function(){
                  jQuery("#loader").hide();
              },
              success: function(res) {
                  if(res.action == 1){

                    switchSteps('Step-3');
                    $('#Step-3').html(res.html);

                  }else{
                      $("#otp").addClass('is-invalid');
                      $(".otp_error").addClass('invalid-tooltip').html('Invalid OTP').show();
                      return false;
                  }
              },
              error: function(res) {
                  switchSteps('Step-1');
                  return false;    
              }
          });
      }
    });

    // get car makes on year change
    $('body').on('change','#carModelYear',function(e){
        // 
        // $("ol.cd-multi-steps li").removeClass("current");
        // $("#my-car-detail-bar").addClass("current")
        var year = $(this).val();
        // reset other
        $('#carMakes').empty()
          .append($('<option>', {value: '',text: 'Car Make'}));
        // 
        $('#carModels').empty()
          .append($('<option>', {value: '',text: 'Car Model'}));
        // 
        $('#carVariants').empty()
          .append($('<option>', {value: '',text: 'Model Details'}));
        //
        $("#carValueHeading").text('').hide(); 
        // 
        $("#newCarYes,#newCarNo").attr("disabled", 'disabled');
        // 
        if(year > 0) {
            $.ajax({
              type: "POST",
              url: '{{route('ajaxGetCarMakes')}}',
              dataType: "json",
              data: {year:year},
              beforeSend :function(){
                  jQuery("#loader").hide();
              },
              success: function(res) {
                if(res.action == 1) {
                  var makes = res.makes;
                  for( i in makes ) {
                    $('#carMakes').append($('<option>', {
                                      value: makes[i].car_make_id,
                                      text: makes[i].name
                                  }));
                  }
                }
              },
              error: function(res) {}
          });
        }
    })

    // get car models on make change
    $('body').on('change','#carMakes',function(e){
        // 
        // $("ol.cd-multi-steps li").removeClass("current");
        // $("#my-car-detail-bar").addClass("current");
        // 
        var make = $(this).val();
        var year = $("#carModelYear").val();
        // 
        $('#carModels').empty()
          .append($('<option>', {value: '',text: 'Car Model'}));
        // 
        $("#carValueHeading").text('').hide();
        //
        $('#carVariants').empty()
          .append($('<option>', {value: '',text: 'Model Details'})); 
        // 
        $("#newCarYes,#newCarNo").attr("disabled", 'disabled');
        // console.log(make)
        if(make > 0) {
            $.ajax({
              type: "POST",
              url: '{{route('ajaxGetCarModels')}}',
              dataType: "json",
              data: {make:make, year:year},
              beforeSend :function(){
                  jQuery("#loader").hide();
              },
              success: function(res) {
                if(res.action == 1) {
                  var models = res.models;
                  for( i in models ) {
                    $('#carModels').append($('<option>', {
                                      value: models[i].car_model_id,
                                      text: models[i].name
                                  }));
                  }
                }
              },
              error: function(res) {}
          });
        }
    }); 

    // get car variants on model change
    $('body').on('change','#carModels',function(e){
        // 
        // $("ol.cd-multi-steps li").removeClass("current");
        // $("#my-car-detail-bar").addClass("current");
        // 
        var year = $("#carModelYear").val();
        var make = $("#carMakes").val();
        var model = $(this).val();
        // 
        $('#carVariants').empty()
          .append($('<option>', {value: '',text: 'Model Details'}));
        // 
        $("#carValueHeading").text('').hide();
        // 
        $("#newCarYes,#newCarNo").attr("disabled", 'disabled');
        // 
        if(year > 0 && make > 0 && model > 0) {
          $.ajax({
              type: "POST",
              url: '{{route('ajaxGetCarVariants')}}',
              dataType: "json",
              data: {year:year, make:make, model:model},
              beforeSend :function(){
                  jQuery("#loader").hide();
              },
              success: function(res) {
                if(res.action == 1) {
                  var variants = res.variants;
                  for( i in variants ) {
                    $('#carVariants').append($('<option>', {
                                      value: variants[i].car_model_variant_id,
                                      text: variants[i].name
                                  }));
                  }
                }
              },
              error: function(res) {}
          });
        }
    });

    // get car values
    $('body').on('change','#carVariants',function(e){
        // 
        // $("ol.cd-multi-steps li").removeClass("current");
        // $("#my-car-detail-bar").addClass("current");
        // 
        var year = $("#carModelYear").val();
        var make = $("#carMakes").val();
        var model = $("#carModels").val();
        var variant = $(this).val();
        $("#carValueInput").val('');
        $("#carValueHeading").text('').hide();
        $("#newCarYes,#newCarNo").attr("disabled", 'disabled');
        // 
        if(year > 0 && make > 0 && model > 0) {
            $.ajax({
              type: "POST",
              url: '{{route('ajaxGetCarValues')}}',
              dataType: "json",
              data: {year:year, make:make, model:model, variant:variant},
              beforeSend :function(){
                  jQuery("#loader").hide();
              },
              success: function(res) {
                if(res.action == 1) {
                  var string = 'Please select a value between <span>AED '+res.values.min_value+'</span> and <span>AED '+res.values.max_value+'</span>';
                  $("#carValueHeading").html(string).show();
                  $("#carValueInput").attr('min',res.values.min_value).attr('max',res.values.max_value);
                  $("#carValueInput").val(res.values.values);
                  // 
                  $("#newCarYes,#newCarNo").removeAttr("disabled").prop("disabled", false);
                }
              },
              error: function(res) {}
          });
        }
    }); 

    //new car yes/no
    $('body').on('change','#newCarYes',function(e){

       $("#datepicker").prop('disabled', true);
       //$("#datepicker").datetimepicker({ dateFormat: "dd/mm/yyyy"}).datetimepicker("setDate", new Date());
       $("#datepicker").datepicker({ dateFormat: "dd/mm/yy"}).datepicker("setDate", new Date());

    });
    
    $('body').on('change','#newCarNo',function(e){

      $("#datepicker").prop('disabled', false);
      $("#datepicker").datepicker("destroy");
      
    });

    // save car details
    $('body').on('click','#saveCarDetailLead',function(e){
      // 
      hideErrorDiv();

      var year = $("#carModelYear").val();
      var make = $("#carMakes").val();
      var model = $("#carModels").val();
      var variant = $("#carVariants").val();
      var carValue = $("#carValueInput").val();
      var isNewCar = $("#newCarYes").is(":checked") ? 1 : 0;
      var carRegistrationDate = $("#datepicker").val();
      var city_of_registration = $("#city_of_registration").val();
      // 
      //conditions checking
      var minVal = $("#carValueInput").attr('min');
      var maxVal = $("#carValueInput").attr('max');
      var brandnewcar = $("input:radio[name='brandnewcar']:checked");
      // 
      minVal = parseFloat(minVal);
      maxVal = parseFloat(maxVal);
      // 
      if ($("#carModelYear").val() == '')
      {
          $("#carModelYear").addClass('is-invalid');
          $(".carModelYear_error").addClass('invalid-tooltip').html('Please Select Car Model Year').show();
          return false;
      }
      else if ($("#carMakes").val() == '')
      {
          $("#carMakes").addClass('is-invalid');
          $(".carMakes_error").addClass('invalid-tooltip').html('Please Select Car Make').show();
          return false;
      }
      else if ($("#carModels").val() == '')
      {
          $("#carModels").addClass('is-invalid');
          $(".carModels_error").addClass('invalid-tooltip').html('Please Select Car Model').show();
          return false;
      }      
      else if ($("#carVariants").val() == '')
      {
          $("#carVariants").addClass('is-invalid');
          $(".carVariants_error").addClass('invalid-tooltip').html('Please Select Model Details').show();
          return false;
      }      
      else if ($("#carValueInput").val() == '')
      {
          $("#carValueInput").addClass('is-invalid');
          $(".carValueInput_error").addClass('invalid-tooltip').html('Please Fill Car Value').show();
          return false;
      }
      else if (carValue < minVal || carValue > maxVal ) 
      {
          $("#carValueInput").addClass('is-invalid');
          $(".carValueInput_error").addClass('invalid-tooltip').html(carValue + ' is not between ' + minVal + ' and '  + maxVal).show();
          return false;
      }
      else if (brandnewcar.length === 0)
      {
          $("#brandnewcar_error").addClass('is-invalid');
          $(".brandnewcar_error").addClass('invalid-tooltip').html('Is your car brand new?').show();
          return false;
      }
      else if ($("#datepicker").val() == '')
      {
          $("#datepicker").addClass('is-invalid');
          $(".datepicker_error").addClass('invalid-tooltip').html('Please Fill (When was your car first registered?)').show();
          return false;
      }
      else if ($("#city_of_registration").val() == '')
      {
          $("#city_of_registration").addClass('is-invalid');
          $(".city_of_registration_error").addClass('invalid-tooltip').html('Please Select City.').show();
          return false;
      }
      else{
        $.ajax({
            type: "POST",
            url: '{{route('ajaxSaveCarLeads')}}',
            dataType: "json",
            data: {year:year, make:make, model:model, 
              variant:variant, carValue:carValue, isNewCar:isNewCar, 
              carRegistrationDate:carRegistrationDate, city_of_registration:city_of_registration
            },
            beforeSend :function(){
                jQuery("#loader").hide();
            },
            success: function(res) {
              if(res.action == 1) {
                var variants = res.variants;
                for( i in variants ) {
                  $('#carVariants').append($('<option>', {
                                    value: variants[i].car_model_variant_id,
                                    text: variants[i].name
                                }));
                }
                      switchSteps('Step-4');
                      $('#Step-4').html(res.html);

                      if(isNewCar == 1){
                        $("ol.cd-multi-steps li#renewal-details").hide();
                      }
                      else{
                        $("ol.cd-multi-steps li#renewal-details").show(); 
                      }
                      $(window).scrollTop(0);
              }
              else{
                  if(res.error == 1){

                    $("#datepicker").addClass('is-invalid');
                    $(".datepicker_error").addClass('invalid-tooltip').html('Car registration year cannot be less than Model Year').show();
                    return false;
                  }
                  else{
                    switchSteps('Step-1');
                  }

              }
            },
            error: function(res) {}
        });
      }

    })

    //more_about_me
    $('body').on('submit','#more_about_me',function(e){
      e.preventDefault();
      hideErrorDiv();
      var formData = $(this).serialize();

      var isNonGCC = $("#nongcc_yes").is(":checked") ? 1 : 0;
      var nationality = $("#nationality").val();
      var country = $("#country").val();
      var driving_experience = $("#driving_experience").val();
      var driving_in_uae = $("#driving_in_uae").val();
      var dob = $("#datepicker_dob").val();
       var carVariantID = $("#carVariants").val();
       var carValueInput = $('#carValueInput').val();
        var carRegistrationDate = $('#datepicker').val();
       
      
      //Condition Checking 
      //if isNewCar = No then form saved, no display renewal details page
      var isNewCar = $("#newCarYes").is(":checked") ? 1 : 0;

      var nongcc = $("input:radio[name='nongcc']:checked");
      if (nongcc.length === 0){        
          $(".nongcc_error").addClass('invalid-tooltip').html('Please tick (Is the vehicle modified or non-GCC spec?)').show();
          return false;
      }
      else if ($("#nationality").val() == '')
      {
          $("#nationality").addClass('is-invalid');
          $(".nationality_error").addClass('invalid-tooltip').html('Please Select Nationality.').show();
          return false;
      }
      else if ($("#country").val() == '')
      {
          $("#country").addClass('is-invalid');
          $(".country_error").addClass('invalid-tooltip').html('Please Select Country.').show();
          $("#loader").hide();
          return false;
      }
      else if ($("#driving_experience").val() == '')
      {
          $("#driving_experience").addClass('is-invalid');
          $(".driving_experience_error").addClass('invalid-tooltip').html('Please Fill Driving Experience.').show();
          return false;
      }
      else if ($("#driving_in_uae").val() == '')
      {
          $("#driving_in_uae").addClass('is-invalid');
          $(".driving_in_uae_error").addClass('invalid-tooltip').html('Please Fill your experience of driving in UAE.').show();
          $("#loader").hide();
          return false;
      }
      else if ($("#datepicker_dob").val() == '')
      {
          $("#datepicker_dob").addClass('is-invalid');
          $(".datepicker_dob_error").addClass('invalid-tooltip').html('Please Fill Date of Birth').show();
          return false;
      }
      else{
        $.ajax({
            type: "POST",
            url: '{{route('ajaxSaveMoreAboutMe')}}',
            dataType: "json",
            data: {nationality:nationality, isNonGCC:isNonGCC, country:country, 
              driving_experience:driving_experience, driving_in_uae:driving_in_uae, dob:dob, carVariantID:carVariantID, carValueInput:carValueInput,carRegistrationDate:carRegistrationDate
            },
            beforeSend :function(){
                $("#loader").hide();
            },
            success: function(res) {

              if(res.action == 1){
                if (isNewCar == 0){
                  switchSteps('Step-5');
                  $('#Step-5').html(res.html);
                }
                else{

                  //filterResult();
                  //alert('{{route('premium')}}');
                  window.location.href = '{{route('premium')}}';
                }

              }
              else{
                  if(res.error == 1){

                    $("#datepicker_dob").addClass('is-invalid');
                    $(".datepicker_dob_error").addClass('invalid-tooltip').html('You are less than 18 years').show();
                    return false;
                  }
                  else{
                    switchSteps('Step-1');
                  }
              }
            },
            error: function(res) {}
        });
      }
      
    });

    // renewal_details
    $('body').on('submit','#renewal_details',function(e){
      e.preventDefault();
      hideErrorDiv();
      // 
      var isThirdpartyliablityYes = $("#thirdpartyliablityYes").is(":checked") ? 1 : 0;
      var isAgencyrepairYes = $("#agencyrepairYes").is(":checked") ? 1 : 0;
      var isPolicyexpiredYes = $("#policyexpiredYes").is(":checked") ? 1 : 0;
      var register = $("#datepicker_register").val();
      var isClaiminsuranceYes = $("#claiminsuranceYes").is(":checked") ? 1 : 0;
      var isClaimcertificateYes = $("#claimcertificateYes").is(":checked") ? 1 : 0;
      var claimCertificateYear = $("#claimCertificateYear").val();

      //Condition checking
      //alert($("#policyexpiredYes").val());
      
      var thirdpartyliablity = $("input:radio[name='thirdpartyliablity']:checked");
      var agencyrepair = $("input:radio[name='agencyrepair']:checked");
      var policyexpired = $("input:radio[name='policyexpired']:checked");
      var claiminsurance = $("input:radio[name='claiminsurance']:checked");
      var claimcertificate = $("input:radio[name='claimcertificate']:checked");
      if (claiminsurance.length === 1){
        if (isClaiminsuranceYes == 1){ 
          $("input:radio[name='claimcertificate']").prop('checked', false);
          $("#claimCertificateYear").val('');
        }
        else{
          if (claimcertificate.length === 1){
            if (isClaimcertificateYes == 1){ 
              if ($("#claimCertificateYear").val() == ''){
                    $(".claimCertificateYear_error").addClass('invalid-tooltip').html('Please Select No. of Years').show();
                    return false;
                }
            }
          }
          if (claimcertificate.length === 0){        
              $(".claimcertificate_error").addClass('invalid-tooltip').html('Please Select Yes or No').show();
              return false;
          } 
        }
      }     

      if (thirdpartyliablity.length === 0){        
          $(".thirdpartyliablity_error").addClass('invalid-tooltip').html('Please Select Yes or No').show();
          return false;
      }
      else if (agencyrepair.length === 0){        

          $(".agencyrepair_error").addClass('invalid-tooltip').html('Please Select Yes or No').show();
          return false;
      }
      else if (policyexpired.length === 0){        
          
          $(".policyexpired_error").addClass('invalid-tooltip').html('Please Select Yes or No').show();
          $("#loader").hide();
          return false;
      }
      else if ($("#datepicker_register").val() == ''){
          $("#datepicker_register").addClass('is-invalid');
          $(".datepicker_register_error").addClass('invalid-tooltip').html('Please Fill Date').show();
          return false;
      }
      else if (claiminsurance.length === 0){        
          $(".claiminsurance_error").addClass('invalid-tooltip').html('Please Select Yes or No').show();
          $("#loader").hide();
          return false;
      }
      if(isPolicyexpiredYes === 0){
        $.ajax({
            type: "POST",
            url: '{{route('ajaxSaveRenewalDetails')}}',
            dataType: "json",
            data: {register:register, isThirdpartyliablityYes:isThirdpartyliablityYes, isAgencyrepairYes:isAgencyrepairYes, isPolicyexpiredYes:isPolicyexpiredYes, 
              isClaiminsuranceYes:isClaiminsuranceYes, isClaimcertificateYes:isClaimcertificateYes, claimCertificateYear:claimCertificateYear
            },
            beforeSend :function(){
                $("#loader").hide();
            },
            success: function(res) {

              if(res.action == 1){

                //filterResult();
                //alert('{{route('premium')}}');
                  window.location.href = '{{route('premium')}}';

              }else{
                  if(res.error == 1){

                    $("#datepicker_register").addClass('is-invalid');
                    $(".datepicker_register_error").addClass('invalid-tooltip').html('Registration date should be greater than current date').show();
                    return false;
                  }
                  else{
                    switchSteps('Step-1');
                  }
              }
            },
            error: function(res) {}
        });

      }else {

        alert('We do not renew policy if previous policy is expired. Please contact InsureOnline Administrator.');
        return false;
      }
    });

    //hide show
    $('body').on('click','#btnHideStep2',function(e){
      switchSteps("Step-1");
    });
    $('body').on('click','#btnHideStep3',function(e){
      switchSteps("Step-1");
      //clearAll();
    });
    $('body').on('click','#btnHideStep4',function(e){
      switchSteps("Step-3");
    });
    $('body').on('click','#btnHideStep5',function(e){
      switchSteps("Step-4");
    });
    $('body').on('click','#btnHideStep6',function(e){
      switchSteps("Step-5");
    });
///////////////////////////////////////////
    // step 1
    $("#main_signup_form").on('submit', function(event) {
      event.preventDefault();
      // 
      hideErrorDiv();
      // 
      var formData = $(this).serialize();
      var name     = $('#name').val();
      var mobile   = $('#mobile').val();
      var email    = $('#email').val();
      
      if ($("#name").val() == '')
      {
          $("#name").addClass('is-invalid');
          $(".name_error").addClass('invalid-tooltip').html('Please enter your name.').show();
          return false;

      }else if($.trim($("#mobile").val()) == '') {
          $("#mobile").addClass('is-invalid');
          $(".mobile_error").addClass('invalid-tooltip').html('Please enter your mobile number.').show();
          return false;
      }else if(mobile.length !=10) {
          $("#mobile").addClass('is-invalid');
          $(".mobile_error").addClass('invalid-tooltip').html('Please enter 10 digit mobile number.').show();
          return false;
          
      }else if(!validateMobile(mobile)) {
          $("#mobile").addClass('is-invalid');
          $(".mobile_error").addClass('invalid-tooltip').html('Please enter mobile number in numeric.').show();
          return false;
          
      }else if ($.trim($("#email").val()) == '')
      {
          $("#email").addClass('is-invalid');
          $(".email_error").addClass('invalid-tooltip').html('Please enter your email address.').show();
          return false;
      }else if(!validateEmail($("#email").val())) 
      {
          $("#email").addClass('is-invalid');
          $(".email_error").addClass('invalid-tooltip').html('Please enter a valid email address.').show();
          return false;    
     
      }else if($("#email").val() != '') 
      {
          $(".name_error,.mobile_error,.email_error").hide();
          var sEmail = $("#email").val().trim();
          if (validateEmail(sEmail)) {
                 event.preventDefault();
                $.ajax({
                  type: "POST",
                  url: 'ajax-register',
                  dataType: "json",
                  data: formData,
                  beforeSend :function(){
                      $("#loader").hide();
                  },
                  success: function(res) {
                    console.log(res);
                      if(res.action == true){
                        // switchSteps('Step-2');
                        // $('#otp').focus();
                        //step2('Step-2');
                        if(res.status == 1){
                          //$("#email").addClass('is-invalid');
                          $(".email_error").addClass('invalid-tooltip').html(res.msg).show();
                        }else if(res.id != ""){
                           switchSteps('Step-3');
                         $('#Step-3').html(res.html);
                        }else if(res.status == 2) { 
                          $(".email_error").addClass('invalid-tooltip').html(res.msg).show();
                        }else { 
                          step2('Step-2');
                        }
                      }else{
                          $("#email").addClass('is-invalid');
                          $(".name_error_signup_email").addClass('invalid-tooltip').html(res.error_message).show();
                          return false;
                      }
                  },
                  error: function(res) {

                      var errors = res.responseJSON;
                      hideErrorDiv();
                      $("#email").addClass('is-invalid');
                      $(".email_error").addClass('invalid-tooltip').html('Oops! This email address is already in use. Please enter a unique email address.').show();
                      $("#loader").hide();
                      
                  }
              });
          }else{
              e.preventDefault();
              $(".name_error").hide();
              $(".mobile_error").hide();
              $(".email_error").show();
              $(".email_error").html('Please enter a valid email address.').parent().addClass('has-error');
              $("#loader").hide();
              return false;
          }

      }
    });



    $('body').on('change','#claiminsuranceYes',function(e){

      if($(this).is(':checked')){
        $('#divClaimcertificate').hide();
        $('#divClaimCertificateYear').hide();
        //$("input:radio[name='claimcertificate']:checked").val('');
        $("#claimcertificateYes").prop("checked", true);
        $("#claimCertificateYear").val('');
        $('#claiminsuranceYes').parent('div').parent('div').next().hide(); 
      }

    });

    $('body').on('change','#claiminsuranceNo',function(e){

      if($(this).is(':checked')){
        $('#divClaimcertificate').show();
        $('#divClaimCertificateYear').show();
        //$("input:radio[name='claimcertificate']:checked").val('');
        $("#claimcertificateYes").prop("checked", true);
        $("#claimCertificateYear").val('');
        $('#claiminsuranceNo').parent('div').parent('div').next().show(); 
      }

    });
    // 
    $('body').on('change','#claimcertificateYes',function(e){

      if($(this).is(':checked')){
        $('#divClaimCertificateYear').show();
        $("#claimCertificateYear").val('');
      }

    });
    // 
    $('body').on('change','#claimcertificateNo',function(e){

      if($(this).is(':checked')){
        $('#divClaimCertificateYear').hide();
        $("#claimCertificateYear").val('');
      }
      
    });


  });
    // 
    function nextTab(elem) {
      //alert(elem);
        //$('.step-breadcrumb-part .cd-multi-steps li.current').removeClass('active');
        //$(elem).next().find('a[data-toggle="tab"]').click();
        //$(elem).next().addClass('current');
         $("ol.cd-multi-steps li").removeClass("current");
         //$("#my-detail-bar").addClass("current");
         $(elem).next().addClass('current');
    }
    //
    function switchSteps(step)
    {
        $("#Step-1,#Step-2,#Step-3,#Step-4,#Step-5,#Step-6").hide();
        $("#"+step).show();

        var activeBar = $("#"+step).data('bar');
        //$('ol.cd-multi-steps li').removeClass('current');
        $("#"+activeBar).addClass('current');
        $("#"+activeBar).next().removeClass('current');

         // $("ol.cd-multi-steps li").removeClass("current");
         // $("#my-detail-bar").addClass("current");
         $(window).scrollTop(0);
    }
    // 

  function clearAll(){
    $('#carModelYear').val('');    
  }
  function validateEmail(email)
  {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }
  function validateNumber(otp)
  {
      var re = /^[0-9]{4}$/;
      return re.test(otp);
  }
  function validateMobile(mobile)
  {
      var re = /^[0-9]*$/;
      return re.test(mobile);
  }

  function hideErrorDiv() {
    $(".form-control").removeClass('is-invalid');
    $(".error_div").hide();
  }

  // var number = 5000000.245;
  // document.write(format_number(number));

  function format_number(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  // switchSteps('Step-6');
  function filterResult(){

        var carVariantID = $("#carVariants").val();
        // var carVariantID = 1;

        var carValueInput = $('#carValueInput').val();
        //var carValueInput = 130000;
        // var carValueInput = 60000;

        var dob = $('#datepicker_dob').val();
        // var dob = '02/11/1992';
		
		var drivingInUae = $('#driving_in_uae').val();

        var carRegistrationDate = $('#datepicker').val();
        //var carRegistrationDate = '02/12/2018';
        $.ajax({
            type: "POST",
            url: "{{route('ajaxGetFilterResult')}}",
            dataType: "json",
            data: {carVariantID:carVariantID, carValueInput:carValueInput, dob:dob, carRegistrationDate:carRegistrationDate, drivingInUae: drivingInUae
            },
            beforeSend :function(){
                $("#loader").hide();
            },
            success: function(res) {
              $("body").html(res.html);
              var new_url   = APP_URL + "/premiumplans";
              window.history.pushState("data","Title",new_url);
//               //var premiums = res.premiums;
//               // var benefitMasters = res.benefitMasters;
//               var benefitMasters = $("li.benefitName");
//               var matrixData = res.matrixData;
// //console.log(matrixData);
//                 for (j in matrixData) {
//                   var matrix = matrixData[j];
                  
//                   for (k in matrix) {
//                     var pb_sub_type = matrix[k];
//                     var agencyData = pb_sub_type.agency;
//                   if(agencyData.priceAEDAgencyRaw != 0){
//                     var insurance_id = agencyData.insurance_id;
//                     var partnerName = agencyData.partner_name;
//                     var logo = APP_URL + '/uploads/admin/insurance_providers/'+agencyData.insurance_id+'_main';
//                     var priceAEDAgencyRaw = agencyData.priceAEDAgencyRaw;
//                     var priceAEDNonAgencyRaw = agencyData.priceAEDNonAgencyRaw;

//                     var includes = [];
//                     var partnerBenefits = agencyData.benefits;
//                     // console.log('partnerBenefits');
//                     // console.log(partnerBenefits);

//                     $("li.benefitName").each(function(){

//                       var benefitMasterID = $(this).data('id');

//                       if(typeof partnerBenefits[benefitMasterID] == 'undefined') {
//                         includes.push({'include': 0, 'amtAgNonAg': '', 'amount': 0, 'amountRaw': 0, 'description': ''});
//                       }else if( partnerBenefits[benefitMasterID].included == 0){
//                         includes.push({'include': 0, 'amtAgNonAg': 'amtAg', 'amount': partnerBenefits[benefitMasterID].amount, 'amountRaw': partnerBenefits[benefitMasterID].amountRaw, 'description': partnerBenefits[benefitMasterID].description});
//                       }else if( partnerBenefits[benefitMasterID].included == 1){
//                         includes.push({'include': 1, 'amtAgNonAg': 'amtAg', 'amount': partnerBenefits[benefitMasterID].amount, 'amountRaw': partnerBenefits[benefitMasterID].amountRaw, 'description': partnerBenefits[benefitMasterID].description});
//                       }
//                     });
                    
//                     addProvider(insurance_id, partnerName,logo, carValueInput, priceAEDAgencyRaw, priceAEDNonAgencyRaw, includes);
//                   }

//                    var nonAgencyData = pb_sub_type.nonagency;

//                     var insurance_id = nonAgencyData.insurance_id;
//                     var partnerName = nonAgencyData.partner_name;
//                     var logo = APP_URL + '/uploads/admin/insurance_providers/'+nonAgencyData.insurance_id+'_main';
//                     var priceAEDAgencyRaw = nonAgencyData.priceAEDAgencyRaw;
//                     var priceAEDNonAgencyRaw = nonAgencyData.priceAEDNonAgencyRaw;

//                     var includes = [];
//                     var partnerBenefits = nonAgencyData.benefits;
//                     // console.log('partnerBenefits');
//                     // console.log(partnerBenefits);

//                     $("li.benefitName").each(function(){

//                       var benefitMasterID = $(this).data('id');

//                       if(typeof partnerBenefits[benefitMasterID] == 'undefined') {
//                         includes.push({'include': 0, 'amtAgNonAg': 'amtNonAg', 'amount': 0, 'amountRaw': 0, 'description': ''});
//                       }else if( partnerBenefits[benefitMasterID].included == 0){
//                         includes.push({'include': 0, 'amtAgNonAg': 'amtNonAg', 'amount': partnerBenefits[benefitMasterID].amount, 'amountRaw': partnerBenefits[benefitMasterID].amountRaw, 'description': partnerBenefits[benefitMasterID].description});
//                       }else if( partnerBenefits[benefitMasterID].included == 1){
//                         includes.push({'include': 1, 'amtAgNonAg': 'amtNonAg', 'amount': partnerBenefits[benefitMasterID].amount, 'amountRaw': partnerBenefits[benefitMasterID].amountRaw, 'description': partnerBenefits[benefitMasterID].description});
//                       }
//                     });
//                   }
//                     addProvider(insurance_id, partnerName,logo, carValueInput, priceAEDAgencyRaw, priceAEDNonAgencyRaw, includes);
//                    }

//              }


            },
            error: function(res) {}
        });
    }

    function addProvider(insurance_id, partnerName,logo, premium_sub_type, carValueInput, priceAEDAgencyRaw, priceAEDNonAgencyRaw, includes) {
    //function addProvider(insurance_id, partnerName,logo, carValueInput, priceAEDAgencyRaw, priceAEDNonAgencyRaw) {
      var checkboxCounter = $('.custom-checkbox input[type="checkbox"]').length;

      var str = '';
      str += '<div class="policy-box">';
        str += '<div class="policy-box-header">';
          str += '<div class="policy-logo">';
            str += '<span><img src="'+logo+'" alt="'+partnerName+'" title="'+partnerName+'" /></span>';

          str += '</div>';          
           //str += '<span>'+partnerName+'</span>';
           str += '<div class="policy-provider-name">'+ (partnerName) +'</div>';
          str += '<a href="#" class="btn btn-primary">Buy Now</a>';
           str += '<div>'+ premium_sub_type +'</div>';
        str += '</div>';
        str += '<div class="policy-feature-list">';
          str += '<ul>';

            //str += '<li>'+parseFloat(carValueInput).toFixed(2) +'</li>';
            str += '<li>'+ format_number(parseFloat(carValueInput)) +'</li>';
            // str += '<li id="clsPrice'+ insurance_id+'" data-priceaed="'+ priceAEDAgency4Calc +'">'+priceAEDAgency+'</li>';

            str += '<li id="clsPrice'+ insurance_id+'">';
              /*
              if(priceAEDAgencyRaw > 0){
                str += '<div id="clsPriceAg'+ insurance_id+'" data-priceaed="'+ priceAEDAgencyRaw +'">'+priceAEDAgencyRaw.toFixed(2) +'</div>';
              }
              str += '<div id="clsPriceNonAg'+ insurance_id+'" data-priceaed="'+ priceAEDNonAgencyRaw +'">Non Ag.: '+priceAEDNonAgencyRaw +'</div>';
            */
              /*
              if(priceAEDAgencyRaw > 0){

                str += '<div><button class="badge badge-success" id="clsPriceAg'+ insurance_id+'" data-priceaed="'+ priceAEDAgencyRaw +'" type="button">Agency : '+priceAEDAgencyRaw +'</button></div>';

                str += '<div><button class="badge" id="clsPriceNonAg'+ insurance_id+'" data-priceaed="'+ priceAEDNonAgencyRaw +'" type="button">Non Ag.: '+priceAEDNonAgencyRaw +'</button></div>';

              }
              else if(priceAEDAgencyRaw == 0){

                str += '<div><button class="badge badge-success" id="clsPriceNonAg'+ insurance_id+'" data-priceaed="'+ priceAEDNonAgencyRaw +'" type="button">Non Ag.: '+priceAEDNonAgencyRaw +'</button></div>';
              }
              */

              if(priceAEDAgencyRaw > 0){
                str += '<div>Agency</div>';

                str += '<div id="clsPriceAg'+ insurance_id+'_'+premium_sub_type+'" data-priceaed="'+ priceAEDAgencyRaw +'">'+ format_number(parseFloat(priceAEDAgencyRaw)) +'</div>';
              }
              else if(priceAEDNonAgencyRaw > 0){
                str += '<div>Non-Agency</div>';

                str += '<div id="clsPriceNonAg'+ insurance_id+'_'+premium_sub_type+'" data-priceaed="'+ priceAEDNonAgencyRaw +'">'+ format_number(parseFloat(priceAEDNonAgencyRaw)) +'</div>';
              }
            str += '</li>';

            str += '<li>560.00</li>';

            // var counter = 1;

            for (i in includes) {
              var include =  includes[i].include;
              var amtAgNonAg =  includes[i].amtAgNonAg;
              var amount =  includes[i].amount;
              var amountRaw =  includes[i].amountRaw;
              var description =  includes[i].description;
              var includeIcon =  'YES';
              var setamount = 0;
              description = $.trim(description);

              str += '<li>';
              
              if( include == 0 && amount <= 0 ) {
                str += '<div class="fea-minus"></div>';
              }
              else if( include == 1 && amount <= 0 ) {
                str += '<div class="fea-minus"></div>';
              }
              else if(include == 2) {
                if(description == null || description == ''){
                  str += '<div class="fea-check"></div>';
                }
                else{
                  str += '<div>'+ description +'</div>';
                }
              } 
              else if( include == 1 && amount > 0 && amount !== undefined) {
                
                str += '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input btnAmount amountAdd" id="customCheck'+ (checkboxCounter+i) +'" data-insurance_id="'+ insurance_id +'_'+premium_sub_type+'" data-amtAgNonAg="'+ amtAgNonAg +'" data-amountRaw="'+ amountRaw +'"  value="'+amountRaw+'"><label class="custom-control-label" for="customCheck'+ (checkboxCounter+i) +'">'+amount+' AED</label></div>';

                //str += '<div><button class="badge badge-success btnAmount amountAdd" data-insurance_id="'+ insurance_id +'" data-amtAgNonAg="'+ amtAgNonAg +'" data-amountRaw="'+ amountRaw +'"  value="'+amountRaw+'" type="button">Add</button></div>'+amount+' AED</li>';
                
              }
                
              str += '</li>';
            } 

            // str += '<li>';
            //   str += '<div class="fea-minusx">'+ include +'</div>';
            // str += '</li>';
            // str += '<li>';
            //   str += '<div class="badge badge-success">+ ADD</div>+110 AED</li>';
            // str += '<li>';
            //   str += '<div class="badge badge-success">+ ADD</div>+110 AED</li>';
            // str += '<li>';
            //   str += '<div class="fea-check"></div>';
            // str += '</li>';



          str += '</ul>';
        str += '</div>';
        // str += '<div class="policy-box-footer"> <a href="#" class="btn btn-primary">'+priceAED+'</a> </div>';
             // str += '<div class="policy-box-footer"> <a href="#" class="btn btn-primary" id="clsPriceFooter'+ insurance_id+'">'+priceAEDAgency4Raw+'</a> </div>';
      str += '</div>';
      
      $("#policy_listing").append(str);


    }

    function step2(step){
      
        $("#Step-1,#Step-2,#Step-3,#Step-4,#Step-5,#Step-6").hide();
        $("#"+step).show();

      $("#Step-2").empty();

      var apndPlace = $('#Step-2');

      var str = '';

      str += '<div class="main-step-form-inner">';
      str += '  <div class="step-form-heading">';
      str += '    <h3>Confirm Your Email</h3>';
      str += '    <p>We have sent an OTP on your email. Please enter the OTP below to verify.</p>';
      str += '  </div>';
      str += '  <div class="step-form-part">';
      str += '    <form id="main_signup_form_step2" action="#" method="POST" role="form"> ';
      str += '      <div class="form-group">';
      str += '        <input type="text" class="form-control" id="otp" name="otp" placeholder="Enter OTP" autofocus>';
      str += '        <div class="otp_error error_div"></div>';
      str += '      </div>';
      str += '      <div class="btns-part">';
      str += '        <button type="button" class="btn btn-outline-light" id="btnHideStep2">Back</button>';
      str += '        <button type="submit" class="btn btn-secondary">Next</button>';
      str += '      </div>';
      str += '    </form>';
      str += '  </div>';
      str += '</div>';

      $(apndPlace).append(str);
    }

</script>


@endsection
@section('content')
<div class="rowx">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
    </div>
</div>
<!-- Main Container Start -->
<section class="main-container-section"> 
  <!-- Step 1 Container Start -->
  <div class="step-main-container">
    <!--div class="container is_select_policy"-->
    <div class="container">
      <div class="step-inner-container"> 
        
        <!-- Step Breadcrumbs Bar Start -->
        <div class="step-breadcrumb-part">
          <ol class="cd-multi-steps text-bottom count">
            <li id="my-detail-bar" class="current"><em>My Details</em></li>
            <li id="my-car-detail-bar"><em>My Car Details</em></li>
            <li id="about-me-bar"><em>More About Me</em></li>
            {{-- <li id="select-policy-bar"><em>Select Policy</em></li> --}}
            <li id="renewal-details" style="display:none;"><em>Renewal Details</em></li>
            <li id="select-policy"><em>Select Policy</em></li>
            <li id="payment-bar"><em>Payment</em></li>
            <li id="upload-document-bar"><em>Upload Documents</em></li>
          </ol>
        </div> 
        <!-- Step Breadcrumbs Bar End --> 
        
        <!-- Main Step-1 Form Box Start -->
        <div class="main-step-form-box" style="display: block" id="Step-1" data-bar="my-detail-bar">
          <div class="rowx">
              <!-- error/message -->
              <div>
                  @if (Session::has('error'))
                      <div class="alert alert-danger">
                          <a class="close" data-dismiss="alert" href="#">×</a>
                          <p>{!! Session::get('error') !!}</p>
                      </div>
                  @endif
                  @if (Session::has('success'))
                       <div class="alert alert-success">
                          <a class="close" data-dismiss="alert" href="#">×</a>
                          <p>{!! Session::get('success') !!}</p>
                       </div>
                  @endif
              </div>
          </div>
          <div class="main-step-form-inner">
            <div class="step-form-heading">
              <h3>Tell us About Yourself</h3>
            </div>
            <div class="step-form-part">
              <form id="main_signup_form" action="#" method="POST" role="form" class="needs-validation" autocomplete="off">
                @if(($user=Auth::user()) && !(Auth::user()->can(['super-admin','agent','sub-admin'])))
                <div class="form-group">
                  <input autocomplete="off" type="text" class="form-control" id="name" name="name" placeholder="Your Name" value="{{$user->name}}" />
                  <input  type="hidden"  id="id" name="id" value="{{$user->id}}" />
                  <div class="name_error error_div"></div>
                </div>
                <div class="form-group">
                  <input autocomplete="off" type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="{{$user->mobile}}"/>
                  <div class="mobile_error error_div"></div>
                </div>
                <div class="form-group">
                  <input autocomplete="off" type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{$user->email}}"/>
                  <div style="font-size: 13px;">(We will send OTP to this email for verification)</div>
                  <div class="email_error error_div"></div>
                </div>
                @else
                 <div class="form-group">
                  <input autocomplete="off" type="text" class="form-control" id="name" name="name" placeholder="Your Name"/>
                  <input  type="hidden"  id="id" name="id" value="" />
                  <div class="name_error error_div"></div>
                </div>
                <div class="form-group">
                  <input autocomplete="off" type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number"/>
                  <div class="mobile_error error_div"></div>
                </div>
                <div class="form-group">
                  <input autocomplete="off" type="text" class="form-control" id="email" name="email" placeholder="Email" />
                  <div style="font-size: 13px;">(We will send OTP to this email for verification)</div>
                  <div class="email_error error_div"></div>
                </div>
                @endif
                <div class="btns-part">
                  <button type="submit" class="btn btn-secondary">Next</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Main Step-1 Form Box End --> 
        
        <!-- Main Step-2 Form Box Start -->
        <div class="main-step-form-box" style="display: none" id="Step-2" data-bar="my-detail-bar">
          {{-- <div class="main-step-form-inner">
            <div class="step-form-heading">
              <h3>Confirm Your Email</h3>
              <p>We have sent an OTP on your email. Please enter the OTP below to verify.</p>
            </div>
            <div class="step-form-part">
              <form id="main_signup_form_step2" action="#" method="POST" role="form"> 
                <div class="form-group">
                  <input type="text" class="form-control" id="otp" name="otp" placeholder="Enter OTP">
                  <div class="otp_error error_div"></div>
                </div>
                <div class="btns-part">
                  <button type="button" class="btn btn-outline-light" id="btnHideStep2">Back</button>
                  <button type="submit" class="btn btn-secondary">Next</button>
                </div>
              </form>
            </div>
          </div> --}}
        </div>
        <!-- Main Step-2 Form Box End -->

        <!-- Main Step-3 Form Box Start -->
        <div class="main-step-form-box" style="display: none" id="Step-3" data-bar="my-car-detail-bar">
        </div>
        <!-- Main Step-3 Form Box End --> 

        <!-- Main Step Form Box Start -->
        <div class="main-step-form-box" style="display: none" id="Step-4" data-bar="about-me-bar">
        </div>
        <!-- Main Step Form Box End --> 

        <!-- Main Step Form Box Start -->
        <div class="main-step-form-box" style="display: none" id="Step-5" data-bar="renewal-details">
        </div>
        <!-- Main Step Form Box End --> 

        <!-- Main Step Form Box Start -->
        <!--div class="select-policy-part"-->
        <div class="select-policy-part" style="display: none" id="Step-6" data-bar="select-policy">
          <div class="select-policy-inner-part">
            <div class="filter-option"><a href="#"><i class="fa fa-filter"></i> Filter Results</a></div>
            <div class="policy-table-part clearfix">
              <div class="policy-features-part">
                <div class="policy-feature-heading"></div>
                <div class="policy-feature-list">
                  <!--ul>
                    <li>Price AED</li>
                    <li>Exceed AED</li>
                    <li>Agency Repair</li>
                    <li>Personal <br>
                      Accident Driver</li>
                    <li>Personal Accident <br>
                      Passenger</li>
                    <li>Coverage <br>
                      Outside UAE</li>
                  </ul-->

                      @if(isset($benefits) && count($benefits))
                        <ul id="benefitsList">
                        <li>Car Value</li>
                        <li>Price (AED)</li>
                        <li>Exceed (AED)</li>                          
                          @foreach($benefits as $benefit)
                              <li class="benefitName" data-id="{{ $benefit->id }}">{{ $benefit->name }}</li>
                          @endforeach
                        </ul>
                      @endif
                </div>
              </div>
              {{--  --}}
              <div class="policy-listing-part">
                <div class="owl-carousel select-policy-slider" id="policy_listing">
                  {{-- <div class="policy-box">
                    <div class="policy-box-header">
                      <div class="policy-logo">
                        <span>
                          <img src="images/company-logo-2.png" alt="company-logo" />
                        </span>
                      </div>
                      <a href="#" class="btn btn-primary">Buy Now</a> 
                    </div>
                    <div class="policy-feature-list">
                      <ul>
                        <li>1500</li>
                        <li>560</li>
                        <li>
                          <div class="fea-minus"></div>
                        </li>
                        <li>
                          <div class="badge badge-success">+ ADD</div>
                          +110 AED</li>
                        <li>
                          <div class="badge badge-success">+ ADD</div>
                          +110 AED</li>
                        <li>
                          <div class="fea-check"></div>
                        </li>
                      </ul>
                    </div>
                    <div class="policy-box-footer"> <a href="#" class="btn btn-primary">Buy Now</a> </div>
                  </div> --}}
                  {{--  --}}


                </div>
              </div>
              {{--  --}}
            </div>
            <div class="policy-star-terms">*The prices you see here are exclusive of 5% VAT</div>
            <div class="btns-part">
              <button type="submit" class="btn btn-outline-light ss" id="btnHideStep6">Back</button>
            </div>
          </div>
        </div>
        <!-- Main Step Form Box End --> 


      </div>
    </div>
  </div>
  <!-- Step Container End --> 
</section>
<!-- Main Container End --> 
@endsection
