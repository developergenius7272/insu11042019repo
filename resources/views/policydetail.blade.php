@extends('layouts.master_insurance')

@section('content')


       
     
        <!-- Step Breadcrumbs Bar Start -->
       <!--  <div class="step-breadcrumb-part">
          <ol class="cd-multi-steps text-bottom count">
            <li class="visited current"><em>My Details</em></li>
            <li class="visited current"><em>My Car Details</em></li>
            <li class="visited current"><em>More About Me</em></li>
            <li class="current"><em>Select Policy</em></li>
            <li><em>Payment</em></li>
            <li><em>Upload Documents</em></li>
          </ol>
        </div> -->
        <!-- Step Breadcrumbs Bar End --> 
        
        <!-- Main Step Form Box Start -->
        <div class="select-policy-part">
          <div class="select-policy-inner-part">
            <div class="filter-option"><a href="#"><i class="fa fa-filter"></i> Filter Results</a></div>
            <div class="policy-table-part clearfix">
              <div class="policy-features-part">
                <div class="policy-feature-heading"></div>
                <div class="policy-feature-list">
                  <ul>
                    <li>Car Value</li>
                    <li>Price (AED)</li>
                    <li>Exceed (AED)</li>
                    <li>Loss and Damage of Vehicle</li>
                    <li>Third Party Bodily Injury</li>
                    <li>Third Party Property Damage</li>
                    <li>Ambulance Cover</li>
                    <li>Personal Accident Benefit - Driver</li>
                    <li>Personal Accident Benefit – Passengers</li>
                    <li>Emergency Medical Expenses</li>
                    <li>Personal Injury</li>
                    <li>Oman Cover</li>
                    <li>Natural Disaster, Strike, Riots</li>
                    <li>Personal Belongings</li>
                    <li>Windscreen Damage</li>
                    <li>Replacement of Locks</li>
                    <li>Valet Parking Theft</li>
                    <li>Road Side Assistance</li>
                    <li>Courtesy Car Cash Benefit</li>
                  </ul>
                </div>
              </div>
              <div class="policy-listing-part">
              
                <div class="owl-carousel select-policy-slider">
                @foreach($policyDetail['matrixData'] as $val)
                    @foreach($val as $details)
                        <div class="policy-box">
                            <div class="policy-box-header">
                            <div class="policy-logo"><span><img src="images/company-logo-2.png" alt="company-logo" /></span></div>
                            <div class="policy-provider-name">{{$details['nonagency']['partner_name']}}</div>
                            <a href="#" class="btn btn-primary">Buy Now</a> </div>
                            <div class="policy-feature-list">
                            <ul>
                                <li>{{ $policyDetail['input_value'] }}</li>
                                <li>
                                    <div>Non-Agency</div>
                                    <div>{{$details['nonagency']['priceAEDNonAgencyRaw']}}</div>
                                </li>
                                @foreach($details['nonagency']['benefits'] as $benefit_details)
                                    <li> 
                                    @if($benefit_details['included'] == 1)
                                        <div class="fea-check"></div>
                                    @elseif(isset($benefit_details['amount']))
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck19" value="50">
                                            <label class="custom-control-label" for="customCheck19">{{$benefit_details['amount']}} AED</label>
                                        </div>
                                    @else
                                        <div class="fea-minus"></div>
                                    @endif
                                    </li>
                                @endforeach
                            </ul>
                            </div>
                        </div>
                    @endforeach
                 @endforeach
                </div>
               
              </div>
            </div>
            <div class="policy-star-terms">*The prices you see here are exclusive of 5% VAT</div>
            <div class="btns-part">
              <button type="submit" class="btn btn-outline-light">Back</button>
            </div>
          </div>
        </div>
        <!-- Main Step Form Box End --> 
        
     
  <!-- Step Container End --> 

<!-- Main Container End --> 



@endsection
@section('script')
<script>
$(document).ready(function(){

//Select Policy Slider
$('.select-policy-slider').owlCarousel({
	items:6,
	autoplay:false,
	loop:false,
	dots:false,
	nav:true,
	navText: [
		"<img src='images/left-arrow.png' alt='Prev' />",
		"<img src='images/right-arrow.png' alt='Prev' />"
	],
	responsive:{
		0:{
			items:1,
		},
		600:{
			items:2,
		},
		768:{
			items:3,
		},
		992:{
			items:4,
		},
		1200:{
			items:5,
		},
		1400:{
			items:6,
		},
	}
});

});
</script> 
@endsection
