<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanBenefitsPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_benefits_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_providers_id')->nullable();
            $table->year('year')->nullable();
            $table->datetime('effective_date')->nullable();
            $table->boolean('plan_type')->default(0)->nullable()->comment = '0 = Comprehensive Plan / 1 = TPL Plan';
            $table->tinyInteger('sub_type')->default(0)->nullable()->comment = '0 = N/A, 1 = Standard, 2 = Superior';
            $table->boolean('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_benefits_partners');
    }
}
