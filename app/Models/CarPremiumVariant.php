<?php

namespace App\Models;
use App\Models\CarPremium;
use App\Models\CarModelVariant;

use Illuminate\Database\Eloquent\Model;

class CarPremiumVariant extends Model
{
    protected $table = 'car_premium_variants';
    protected $fillable = [
        'car_model_variant_id', 'car_premium_id'
    ];
    //
    public function CarModelVariant() {
        return $this->belongsTo(CarModelVariant::class, 'car_model_variant_id', 'id');
    }
    // 
    public function CarPremium() {
        return $this->belongsTo(CarPremium::class, 'car_model_variant_id', 'id');
    }
}
