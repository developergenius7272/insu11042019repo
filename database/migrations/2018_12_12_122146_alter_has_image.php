<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHasImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_makes', function ($table) {
            $table->tinyInteger('has_logo')->default(0)->after('name'); // add this field in table
        });

        Schema::table('car_model_variants', function ($table) {
            $table->tinyInteger('has_logo')->default(0)->after('name'); // add this field in table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
