<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\CarMake;

class CreateCarMakesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_makes');
        Schema::create('car_makes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->mediumInteger('order')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('modifiedby')->nullable();
            $table->timestamps();
        });

        // 
        $CarMake = new CarMake();
        $CarMake->name = 'BMW';
        $CarMake->order = 1;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'maruti';
        $CarMake->order = 2;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'honda';
        $CarMake->order = 3;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'chevrolet';
        $CarMake->order = 4;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'fiat';
        $CarMake->order = 5;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'ford';
        $CarMake->order = 6;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'mahindra';
        $CarMake->order = 7;
        $CarMake->save();
        // 

        $CarMake = new CarMake();
        $CarMake->name = 'renault';
        $CarMake->order = 8;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'skoda';
        $CarMake->order = 9;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'tata';
        $CarMake->order = 10;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'toyota';
        $CarMake->order = 11;
        $CarMake->save();
        // 
        $CarMake = new CarMake();
        $CarMake->name = 'volkswagen';
        $CarMake->order = 12;
        $CarMake->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_makes');
    }
}
