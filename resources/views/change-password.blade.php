@extends('layouts.master_insurance')
          

@section('content')
                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
<!-- Main Container Start -->
<section class="main-container-section"> 
  <!-- Step 1 Container Start -->
  <div class="step-main-container">
    <!--div class="container is_select_policy"-->
    <div class="container">
      <div class="step-inner-container"> 

              <div class="main-step-form-box" style="display: block" id="" data-bar="">
                <div class="main-step-form-inner">
                  <div class="step-form-heading">
                    <h3>Reset Password</h3>
                  </div>
                  <div class="step-form-part">
                    <form id="confirm_password_form" action="{{route('resetPasswordSave')}}" method="POST">
                      @csrf
                      <input name="reset_password_token" type="hidden" value="{{$user->reset_password_token}}">
                      <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" id="newpassword" name="newpassword" placeholder="New Password" />
                        <div class="newpassword_error error_div"></div>
                      </div>
                      <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" />
                        <div class="confirmpassword_error error_div"></div>
                        <div class="password_error error_div"></div>
                      </div>
                      <div class="btns-part">
                        <button type="submit" class="btn btn-secondary">Next</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
      </div>
    </div>
  </div>

  <!-- Step Container End --> 
</section>
<!-- Main Container End --> 
@endsection

@section('script')
<script>

$(document).ready(function() {
    
  $('body').on('submit','#confirm_password_form',function(e){
    //e.preventDefault();
    hideErrorDiv();
    
    var newpassword = $("#newpassword").val();
    var confirmpassword = $("#confirmpassword").val();

    if($.trim($("#newpassword").val()) == '') {
      $("#newpassword").addClass('is-invalid');
      $(".newpassword_error").addClass('invalid-tooltip').html('Please enter new password.').show();
      return false;
    }
    else if ($.trim($("#confirmpassword").val()) == ''){

      $("#confirmpassword").addClass('is-invalid');
      $(".confirmpassword_error").addClass('invalid-tooltip').html('Please enter confirm password.').show();
      return false;
    }

    else if (newpassword != confirmpassword) {

          $("#newpassword").addClass('is-invalid');
          $("#confirmpassword").addClass('is-invalid');
          $(".password_error").addClass('invalid-tooltip').html('Passwords do not match.').show();
          return false;
    }
    else{
      return true;
    }

  });

});

  function hideErrorDiv() {
    $(".form-control").removeClass('is-invalid');
    $(".error_div").hide();
  }
</script>
@endsection
