<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Email</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
@media only screen and (max-width: 600px) {
#template_container {
  width: auto!important;
}
}
</style>
</head>
<body leftmargin="0" topmargin="0" offset="0" marginheight="0" marginwidth="0" bgcolor="#ededed">
<div style="width: 100%; -webkit-text-size-adjust: none !important; margin: 0; padding: 10px 0;">
  <table height="100%" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600">
            <tbody>
              <tr>
                <td align="center" valign="top"><!-- Body -->
                  
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="background:#ffffff;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td height="145" align="center" valign="middle" style="background:#213367;"><img src="{{ URL::asset('images/logo.png') }}" width="348" height="52" /></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top" style="padding:40px; font-family:Arial, Helvetica, sans-serif;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td align="left" valign="top">
                                      <!-- body -->
                                      @yield('content')
                                      <!-- body -->
                                    </td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top" style="background:#213367; padding:25px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="55" align="center" valign="top"><a href="#"><img src="{{ URL::asset('images/facebook-icon.png') }}" alt="facebook" style="margin:0 3px;" /></a> <a href="#"><img src="{{ URL::asset('images/twitter-icon.png') }}" alt="twitter" style="margin:0 3px;" /></a> <a href="#"><img src="{{ URL::asset('images/linkedin-icon.png') }}" alt="linkedin" style="margin:0 3px;" /></a></td>
                                  </tr>
                                  <tr>
                                    <td height="25" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#d4d4d4;">{!! env('COPYRIGHT') !!}</td>
                                  </tr>
                                  <tr>
                                    <td align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#d4d4d4;">This email was sent to: xxxxx@mail.com | <a href="#" style="color:#d4d4d4; text-decoration:none;">Privacy Policy</a></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </tbody>
                  </table>
                  
                  <!-- End Body --></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>