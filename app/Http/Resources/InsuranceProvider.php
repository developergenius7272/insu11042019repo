<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InsuranceProvider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'has_logo' => $this->hash_logo,
            'recommended_partner' => $this->recommended_partner,
            'premium_age_factor' => $this->premium_age_factor,
            'premium_car_model_body_factor' => $this->premium_car_model_body_factor,
            'premium_car_model_value_factor' => $this->premium_car_model_value_factor,
            'third_party_available' => $this->third_party_available,
            'base_commission_percentage' => $this->base_commission_percentage,
            'base_commission_amount' => $this->base_commission_amount,
            'bonus_percentage' => $this->bonus_percentage,
            'bonus_amount' => $this->bonus_amount,
            'policy_fees_percentage' => $this->policy_fees_percentage,
            'upto_max_amount' => $this->upto_max_amount,
            'policy_fees_amount' => $this->policy_fees_amount
        ];
    }
}
