<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAgentCommission extends Model
{
    protected $table = 'user_agent_commissions';
    
    protected $fillable = [
        'user_id', 'insurance_provider_id', 'percentage_or_amount', 'amount',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function insuranceProvider() {
        return $this->belongsTo('App\Models\InsuranceProvider', 'insurance_provider_id', 'id');
    }
}
