<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarModelFuel extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
    
    // 
    public function scopeActive($query)
    {
        return $query->where('car_model_fuels.status',1);    
    }
}
