<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\InsuranceProvider;
use App\Models\CarModelBodyType;
use Carbon\Carbon;
use App\Models\CarPremiumAgencyAvailableForYear;

class CarPremium extends Model
{
    protected $table = 'car_premiums';
    // protected $table = 'car_premiums as car_premiums';


    protected $dates = ['effective_date'];
    
    public function insuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id')->active();
    }

    public function recomendedInsuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id')->recomended();
    }
    public function nonRecomendedInsuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id')->NonRecomended();
    }

    
    public function carModelBodyType()
    {
        return $this->belongsTo(CarModelBodyType::class, 'car_model_body_type_id', 'id')->where('status', '=', 1);    
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeEffectiveDate($query)
    {
        $current_date = Carbon::now()->toDateTimeString();
        // return $query->where('effective_date','<=',$current_date);
        // return $query->where('effective_date',$query->orderBy('effective_date', 'DESC')->pluck('effective_date')->first());
        return $query->where('effective_date',function($query) use($current_date) {
            $query->select('sub_car_premium.effective_date')
                ->from(with(new CarPremium)->getTable().' as sub_car_premium')
                ->whereColumn('sub_car_premium.insurance_provider_id','car_premiums.insurance_provider_id')
                ->whereColumn('sub_car_premium.car_model_sub_type','car_premiums.car_model_sub_type')
                ->whereColumn('sub_car_premium.car_model_body_type_id','car_premiums.car_model_body_type_id')
                ->where('sub_car_premium.effective_date','<=',$current_date)
                ->orderBy('sub_car_premium.effective_date', 'DESC')->limit(1);
        });
    }

    public function scopeAge($query, $age)
    {
        return $query->where('max_age','=',0)->orWhereNull('max_age')
            ->orWhere(function($q) use($age){
                    return $q->where('min_age', '<=', $age)->where('max_age', '>=', $age);
                });
    }
    public function agencyPremiumRate($carValueInput,$checkStatus,$extendedRate)
    {
        
        if($checkStatus == "no"){
            // calculate agency rate
            $priceAEDAmount = ($carValueInput * $this->min_premium_agency_rate) / 100;
            return ( $priceAEDAmount > $this->min_premium_agency ) ? $priceAEDAmount : $this->min_premium_agency;
        }else{
            // calculate loading on agency rate
            $priceAEDAmount = ($carValueInput * $this->min_premium_agency_rate) / 100;
            if( $priceAEDAmount > $this->min_premium_agency ) {
                $agencyLoadingRate = ($priceAEDAmount * $extendedRate) / 100;
                $totalAgencyValue = ($priceAEDAmount + $agencyLoadingRate);
            }else{
                $agencyLoadingRate = ($this->min_premium_agency * $extendedRate) / 100;
                $totalAgencyValue = ($this->min_premium_agency + $agencyLoadingRate);
            }
            return  $totalAgencyValue;
        }
    }
    public function nonagencyPremiumRate($carValueInput)
    {
        $priceNonAEDAmount = ($carValueInput * $this->min_premium_nonagency_rate) / 100;
        return ( $priceNonAEDAmount > $this->min_premium_nonagency ) ? $priceNonAEDAmount : $this->min_premium_nonagency;
    }
    public function checkavailability($getMonths){
        return ( isset($this->available_for_months) && ($getMonths <= $this->available_for_months) ) ? false : true; 
    }

    public function premiumRate($carValueInput, $getMonths)
    {
        if ( $getMonths >= $this->available_for_months ) {
            return ['nonagencyPremiumRate' => $this->nonagencyPremiumRate($carValueInput)];
        } else {
            return ['agencyPremiumRate' => $this->agencyPremiumRate($carValueInput)];
        }
    }

    // public function scopeCylinderRange($query, $no_of_cylinders)
    // {
    //     return $query->where('car_model_cylinder', '=', $no_of_cylinders)->orWhere(function ($q) use ($no_of_cylinders) {
    //         return $q->where('above_cylinder','=',1)->where('car_model_cylinder', '<=', $no_of_cylinders);
    //     });
    // }

    public function excessValue($carValue,$age,$no_of_seats)
    {
        $test = $this->hasManyThrough(Excess::class, CarModelBodyType::class, 'id', 'car_model_body_type_id', 'car_model_body_type_id','id');

          $test->where('insurance_provider_id', '=', $this->insurance_provider_id);
          return $test->where(function($q) use($carValue){
                        $q->where('car_model_min_value', '<', $carValue)
                        ->where('car_model_max_value', '>', $carValue)->orWhereNull('car_model_max_value');})
                        ->where(function($q) use($age){$q->where('min_age','<=',$age)
                        ->where('max_age','>=',$age)->orWhereNull('max_age');})
                        ->where(function($q) use($no_of_seats){
                    $q->where('min_seat','<=',$no_of_seats)
                    ->where('max_seat','>=',$no_of_seats)
                    ->orWhereNull('max_seat');
                    })->get();
        
        // $test->where('insurance_provider_id', '=', $this->insurance_provider_id);
        // return $test->where('car_model_min_value', '<=', $this->car_model_max_value) 
        //             ->where('car_model_max_value', '>=', $this->car_model_max_value) //300000//60000
        //             ->where('min_age','<=',$this->max_age)
        //             ->where('max_age','>=',$this->max_age)->get();
    }

    public function CarPremiumVariant() {
        return $this->hasMany('App\Models\CarPremiumVariant', 'car_premium_id', 'id');
    } 

    public function carPremiumAgencyAvailableForYear()
    {
        return $this->hasMany(carPremiumAgencyAvailableForYear::class, 'car_premiums_id', 'id');
    }
}
