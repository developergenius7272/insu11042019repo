<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CarPremium;

class CarPremiumAgencyAvailableForYear extends Model
{
    protected $table = 'car_premium_agency_available_for_years';
    
    protected $fillable = [
        'car_premiums_id', 'year_number', 'rate',
    ];

    public function carPremium()
    {
        return $this->belongsTo(CarPremium::class,'car_premiums_id','id');
    }
}
