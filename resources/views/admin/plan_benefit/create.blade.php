@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection
@section('content')
<div class="row">
    {{-- <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i> 
                Table 
            <span>> 
                Normal Tables
            </span>
        </h1>
    </div> --}}
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Benefit Plan</h2>

                    </header>
                    <div class="formError" style="display:none;">

                        <div class="alert alert-danger mytbodyError">
                            <ul>
                                {{-- @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach --}}
                            </ul>
                        </div>

                    </div>

                    {{-- <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div> --}}
                    {{-- <div style="text-align: center;">
                        @if (\Session::has('success'))
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div><br />
                         @endif
                     </div> --}}
                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
    
                            <form class="form-horizontal" id="formPlanBenefit" method="POST" action="{{route('admin.planbenefits.store')}}" >
                                 @csrf
                                {{--  --}}
                                <fieldset>
                                    <legend>Benefit Plan</legend>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Select Partners</label>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <select class="form-control" id="insurance_providers_id" name="insurance_providers_id">
                                                        <option value="">Select Partner</option>
                                                        @if( isset($partners) && $partners->count() )
                                                            @foreach($partners as $partner)
                                                                <option value="{{$partner->id}}" 
                                                                    @if(old('insurance_providers_id') == $partner->id) 
                                                                        {{ 'selected' }} 
                                                                    @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                                        {{ 'selected' }} 
                                                                    @endif
                                                                    >{{strtoupper($partner->name)}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-2">Years</label>
                                          <div class="col-md-4">
                                                <select class="form-control" id="year" name="year">
                                                    @for($i=($current_year + 1 ); $i >= 2000; $i--)

                                                        <option value="{{$i}}"
                                                            @if(old('year') == $i) 
                                                                {{ 'selected' }} 
                                                            @elseif($i == $current_year) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                            >{{$i}}</option>
                                                    @endfor
                                                </select>
                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                          </div> 
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-md-2">Effective Date</label>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <input type="text" name="effective_date" value="{{ $errors->any() ? old('effective_date') : '' }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Comprehensive plan / TPL plan</label>
                                        <div class="col-md-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="plan_type" value="0" checked >Comprehensive plan
                                                </label>
                                                <label>
                                                    <input type="radio" name="plan_type" 
                                                    @if(old('plan_type') == 1)
                                                        {{ 'checked' }}
                                                    @endif
                                                    value="1">TPL plan
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Standard / Superior</label>
                                        <div class="col-md-10">
                                            <div class="radio">
                                                {{-- <label>
                                                    <input type="radio" name="sub_type" 
                                                            @if(old('sub_type') == 0) 
                                                                {{ 'checked' }} 
                                                            @elseif(isset($data->sub_type) && $data->sub_type == 0)
                                                                {{ 'checked' }} 
                                                            @elseif($formType == 'add') 
                                                                {{ 'checked' }}
                                                            @endif
                                                    value="0">Not Applicable
                                                </label> --}}
                                                <label>
                                                    <input type="radio" name="sub_type" checked value="1">Standard
                                                </label>
                                                <label>
                                                    <input type="radio" name="sub_type" 
                                                            @if(old('sub_type') == 2) 
                                                                {{ 'checked' }} 
                                                            @endif
                                                    value="2">Superior
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Status</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" 
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                            value="0">Inactive
                                                        </option>
                                                        <option  class="form-control" 
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                            value="1">Active
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                {{--  --}}

                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-md-3"><label class="control-label">Benefit Name</label></div>      
                                        <div class="col-md-1" style="width: 116px;"><label class="control-label">Include in plan</label></div> 
                                        <div class="col-md-1" style="width: 132px;"><label class="control-label">Single or Multiple</label></div>
                                        {{-- <div class="col-md-1"><label class="control-label">Percentage or Amount</label></div>
                                        <div class="col-md-1"><label class="control-label">Amount</label></div>  
                                        <div class="col-md-2"><label class="control-label">Description</label></div>   --}}
                                    </div>

                                <hr>
                                    @if($benefitMasters)
                                        @foreach($benefitMasters as $benefitMaster)

                                    <div class="benefit-row row row_{{ $benefitMaster->id }}">
                                    <div class="row addMoreDiv addMoreDiv_{{ $benefitMaster->id }}">

                                        <input type="hidden" value="{{ $errors->any() ? old('benefit_id_'.$benefitMaster->id) : '' }}" name="benefit_id_{{ $benefitMaster->id }}" class="benefit_id_{{ $benefitMaster->id }}">

                                        <div class="col-md-3">
                                            {{ $benefitMaster->name }}
                                        </div>      

                                        <div class="col-sm-1">
                                            <select name="included_{{ $benefitMaster->id }}" class="form-control included" >

                                                <option value="0">N/A</option>
                                                <option value="1" 
                                                    @if(old("included_".$benefitMaster->id) === '1')
                                                        {{ 'selected' }}
                                                    @endif

                                                    >Not Included
                                                </option>

                                                <option value="2" 
                                                    @if(old("included_".$benefitMaster->id) === '2')
                                                        {{ 'selected' }}
                                                    @endif

                                                    {{-- @if($formType == 'add')
                                                        {{ 'selected' }}
                                                    @elseif(old("included_".$benefitMaster->id) === '1')
                                                        {{ 'selected' }}
                                                    @endif --}}

                                                    >Included
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-md-1 divSingleMul_{{ $benefitMaster->id }}" 
                                            @if(old('included_'.$benefitMaster->id) === '1')
                                                {{ 'style=display:block' }}
                                            @else
                                                {{ 'style=display:none' }}
                                            @endif
                                        >
                                            <select name="singleMul_{{ $benefitMaster->id }}" class="form-control singleMul singleMul_{{ $benefitMaster->id }}" >

                                                <option value="0">N/A</option>
                                                <option value="1" 
                                                    @if(old("singleMul_".$benefitMaster->id) === '1')
                                                        {{ 'selected' }}
                                                    @endif
                                                    >Single
                                                </option>

                                                <option value="2" 
                                                    @if(old("singleMul_".$benefitMaster->id) === '2')
                                                        {{ 'selected' }}
                                                    @endif
                                                    >Multiple
                                                </option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-1 form-group-other divPerAmount_{{ $benefitMaster->id }}" 
                                            @if(old('included_'.$benefitMaster->id) === '1')
                                                {{ 'style=display:block' }}
                                            @else
                                                {{ 'style=display:none' }}
                                            @endif
                                        >
                                        
                                            <select name="perAmount_{{ $benefitMaster->id }}[]" data-ids="perAmount_{{ $benefitMaster->id }}" class="form-control perAmount perAmount_{{ $benefitMaster->id }}" >

                                                {{-- <option value="0">N/A</option> --}}
                                                <option value="1" 
                                                    {{-- @if(old("perAmount_".$benefitMaster->id) === '1')
                                                        {{ 'selected' }}
                                                    @endif --}}
                                                    >Percent
                                                </option>

                                                <option value="2" 
                                                    {{-- @if(old("perAmount_".$benefitMaster->id) === '2')
                                                        {{ 'selected' }}
                                                    @endif --}}
                                                    >Amount
                                                </option>
                                            </select>
                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                        </div> 

                                        {{-- Amount --}}
                                        <div class="col-md-1 form-group-other divAmount_{{ $benefitMaster->id }}" 
                                            @if(old('perAmount_'.$benefitMaster->id) === '1')
                                                {{ 'style=display:block' }}
                                            @elseif(old('perAmount_'.$benefitMaster->id) === '2')
                                                {{ 'style=display:block' }}
                                            @else
                                                {{ 'style=display:none' }}
                                            @endif
                                        >
                                            <input class="form-control amount amount_{{ $benefitMaster->id }}" placeholder="" type="number" step="any" min="1" name="amount_{{ $benefitMaster->id }}[]" value="{{-- {{old("amount_".$benefitMaster->id,'')}} --}}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                        </div>

                                        {{-- feature_description --}}
                                        <div class="col-md-2 divFeature_description_{{ $benefitMaster->id }}" style="display:none;">

                                            <input class="form-control feature_description_{{ $benefitMaster->id }}" placeholder="Description" type="text" name="feature_description_{{ $benefitMaster->id }}[]" value="">

                                        </div>

                                        {{-- Description --}}
                                        <div class="col-md-2 divDescription_{{ $benefitMaster->id }}" 
                                            @if(old('included_'.$benefitMaster->id) === '2')
                                                {{-- style="display:block;margin-left:325px;"  --}}
                                                style="display:block;" 
                                            @else
                                                style="display:none;" 
                                            @endif
                                        >
                                            <input class="form-control description_{{ $benefitMaster->id }}" placeholder="Description" type="text" name="description_{{ $benefitMaster->id }}" value="{{old("description_".$benefitMaster->id,'')}}">
                                        </div> 

                                        <div class="col-md-2 divRequired_{{ $benefitMaster->id }}" style="display:none;">
                                            <input type="checkbox" class="required_{{ $benefitMaster->id }}" name="required_{{ $benefitMaster->id }}" value="1"
                                            >
                                        </div>
                                        
                                        {{-- <br> --}}
                                        <span class="addOneMore addOneMore_{{ $benefitMaster->id }}" onclick="clone_div({{ $benefitMaster->id }})"><i class="fa fa-plus-square"></i>Add New</span>
                                    </div>
                                    </div>

                                    <hr>

                                      @endforeach
                                    @endif

                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{url('/admin/planbenefits')}}" class="btn btn-default">Cancel</a>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
    
                            </form>
    
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            
        </div>

    </section>
</div>
@endsection

<style>
.addOneMore{
  display: none;
}
.addMoreDiv{
    margin-left: 0px!important;
    margin-right: 0px!important;
}

</style>

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();

        // 
        $("#formPlanBenefit").on('submit',(function(e) {
            
            var action = 0;
            
            $('.formError').hide();

            var is_valid =  validForm();
            
            if(is_valid == true){

                $.ajax(
                {
                    type:'POST',

                    url:'{{route('admin.addFormPlanBenefit')}}',

                      data: new FormData(this),
                        processData: false,
                        contentType: false,
                        //ContentType:"application/json",
                      async : false,

                    success: function(response){

                        response = $.parseJSON(response);

                        var status = response.status;
                        var msgs = response.msgs ;

                        if(status == 1){

                            action = 1;

                        }
                        else{

                          var tbodyTest = "";

                          $.each(msgs, function(i, item) {

                            tbodyTest += "<li>";
                            tbodyTest += item.msg;
                            tbodyTest += "</li>";
                            
                            $(".mytbodyError").empty();
                            $(".mytbodyError").append(tbodyTest );

                            $('.formError').show();
                           
                           });
                        }
                        $(window).scrollTop(0);
                        //flag=false;
                        return false;
                    }
                });
            }
            else{
                $(window).scrollTop(0);
                return false;
            }
            //alert(flag);
            if (action == 0){

                return false;
            }

        }));        
        // 

        $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
        // Include in plan
        $('body').on('change','.included',function(e){

            var includedName = $(this).attr('name');

            var arr = includedName.split('_');
            var benefitId = arr[1];
            $('.benefit_id_'+benefitId).val(benefitId); // hidden field

            var includedVal = $(this).val();
            //if not include
            if(includedVal == 1){

                $('.divSingleMul_'+benefitId).show();

                $('.singleMul_'+benefitId).val(1);

                $('.divPerAmount_'+benefitId).show();
                $('.perAmount_'+benefitId).val(1);
                //$('.perAmount').val('');

                $('.amount_'+benefitId).val('');
                $('.divAmount_'+benefitId).show();

                $('.divRequired_'+benefitId).show();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it
                
                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).hide();

                $('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).hide();
                
                $('.addOneMore_'+benefitId).hide();
            }
            //if include
            else if(includedVal == 2){

                $('.divSingleMul_'+benefitId).hide();

                $('.perAmount_'+benefitId).val('');
                $('.divPerAmount_'+benefitId).hide();

                $('.amount_'+benefitId).val('');
                $('.divAmount_'+benefitId).hide();

                $('.divRequired_'+benefitId).hide();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it

                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).hide();

                //$('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).show();
                $('.addOneMore_'+benefitId).hide();
                //remove extra rows
                $('.forremove_'+benefitId).remove();

                //$('.divDescription_'+benefitId).css( { marginLeft : "325px" } );
            }
            //else
            else if(includedVal == 0){

                $('.divSingleMul_'+benefitId).hide();

                $('.perAmount_'+benefitId).val('');
                $('.divPerAmount_'+benefitId).hide();

                $('.amount_'+benefitId).val('');
                $('.divAmount_'+benefitId).hide();

                $('.divRequired_'+benefitId).hide();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it

                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).hide();

                $('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).hide();

                $('.addOneMore_'+benefitId).hide();
                //remove extra rows
                $('.forremove_'+benefitId).remove();

                $('.benefit_id_'+benefitId).val('');
            }
        });

        $('body').on('change','.singleMul',function(e){
            var singleMulName = $(this).attr('name');

            var arr = singleMulName.split('_');
            var benefitId = arr[1];
            $('.benefit_id_'+benefitId).val(benefitId);

            var singleMulVal = $(this).val();
            //if single
            if(singleMulVal == 1){
                //remove extra rows
                $('.forremove_'+benefitId).remove();

                $('.addOneMore_'+benefitId).hide();

                $('.divPerAmount_'+benefitId).show();
                $('.perAmount_'+benefitId).val(1);

                $('.divAmount_'+benefitId).show();
                $('.amount_'+benefitId).val('');
                $('.divPerAmount_'+benefitId).show();
                //$('.divAmount_'+benefitId).show();

                $('.divRequired_'+benefitId).show();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it
                
                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).hide();

                $('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).hide();
            }
            //if multiple
            else if(singleMulVal == 2){

                $('.addOneMore_'+benefitId).show();
                
                $('.divPerAmount_'+benefitId).show();
                $('.perAmount_'+benefitId).val(1);

                //$('.divPerAmount_'+benefitId).hide();
                $('.divPerAmount_'+benefitId).show();

                $('.amount_'+benefitId).val('');
                //$('.divAmount_'+benefitId).hide();
                $('.divAmount_'+benefitId).show();

                $('.divRequired_'+benefitId).hide();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it

                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).show();

                $('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).hide();
            }
            //if N/A
            else if(singleMulVal == 0){
                $('.addOneMore_'+benefitId).hide();

                $('.perAmount_'+benefitId).val('');

                $('.amount_'+benefitId).val('');
                $('.divAmount_'+benefitId).hide();
                $('.divPerAmount_'+benefitId).hide();

                $('.divRequired_'+benefitId).hide();
                $('.required_'+benefitId).prop('checked', false); // Unchecks it

                $('.feature_description_'+benefitId).val('');
                $('.divFeature_description_'+benefitId).hide();
                
                $('.description_'+benefitId).val('');
                $('.divDescription_'+benefitId).hide();

                //remove extra rows
                $('.forremove_'+benefitId).remove();
            }
        });

        $('body').on('change','.perAmount',function(e){

            //var perAmountName = $(this).attr('name');
            var perAmountName = $(this).attr('data-ids');

            var arr = perAmountName.split('_');
            var benefitId = arr[1];
            var counterId = arr[2];
            
            if (counterId == null){
                counterId = '';
            }
            else{
                counterId = '_'+counterId;
            }

            var perAmountVal = $(this).val();

            if(perAmountVal == 1){
                
                $('.divAmount_'+benefitId+counterId).show();
                $('.amount_'+benefitId+counterId).val('');
                //$('.amount_'+benefitId+counterId).attr('placeholder','Percentage');

            }
            else if(perAmountVal == 2){
                
                $('.divAmount_'+benefitId+counterId).show();
                $('.amount_'+benefitId+counterId).val('');
                //$('.amount_'+benefitId+counterId).attr('placeholder','Amount');

            }
            else if(perAmountVal == 0){
                $('.divAmount_'+benefitId+counterId).hide();
                $('.amount_'+benefitId+counterId).val('');
            }
        });
        
    });
</script>
<script type="text/javascript">

var counter = 1;
function clone_div(benefitId){
    counter++;
    var apndPlace = $('.row_'+benefitId);

    //var text = '<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>';  

    var str = '';
    str += '<div class="row addMoreDiv forremove_'+ benefitId +' addMoreDiv_'+ benefitId +'_'+ counter +'">';

        str += '<div class="col-md-3">';
        str += '</div>';

        str += '<div class="col-md-1">';
        str += '</div>';

        str += '<div class="col-md-1">';
        str += '</div>';

        str += '<div class="col-md-1">';
        str += '</div>';

        // str += '<div class="col-md-1 divPerAmount_'+ benefitId +'_'+ counter +'">';
        //     str += '<select name="perAmount_'+ benefitId +'[]" data-ids="perAmount_'+ benefitId +'_'+ counter +'" class="form-control perAmount perAmount_'+ benefitId+'">';
        //     //str += '<option value="0">N/A</option>';
        //     str += '<option selected value="1">Percentage</option>';
        //     str += '<option value="2">Amount</option>';
        //     str += '</select>';
        // str += '</div>';

        str += '<div class="col-md-1 form-group-other divAmount_1'+ benefitId +'_'+ counter +'">';

            str += '<input class="form-control amount amount_'+ benefitId +'_'+ counter +'" placeholder="" type="number" step="any" min="1" name="amount_'+ benefitId +'[]" value="">';
            str += '<span class="my-error" style="color:#b94a48;display:none" >*</span>';

        str += '</div>';

        str += '<div class="col-md-2 divFeature_description_1'+ benefitId +'_'+ counter +'">';

            str += '<input class="form-control feature_description_'+ benefitId +'_'+ counter +'" placeholder="Description" type="text" name="feature_description_'+ benefitId +'[]" value="">';

        str += '</div>';

        str += '<div class="col-md-2">';
        str += '<a href="javascript:void(0);" class="remove_field" title="Add field">Remove</a>';
        str += '</div>';

    str += '</div>';
    //str += '<br>';

    var text2 = '<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>';

    //$(apndPlace).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
    $(apndPlace).append(str);


    $(apndPlace).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); 
    })
}

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var insuranceProviderId  = $.trim($("#insurance_providers_id").val());
        var year = $('#year').prop('selectedIndex');
        //var datetimepicker  = $.trim($("#datetimepicker").val());
        var t= $.trim($("#datetimepicker").val());  

        if (insuranceProviderId == ''){

            $("#insurance_providers_id").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insurance_providers_id').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (year == 0){
            
            $("#year").parent().find(".my-error").html('Please select year').show();
            $('#year').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        //effective date checking
        if($.trim(t).length>0)
        {
            
            var ary= t.split(" "), aryDate=ary[0].split("/"),aryTime=ary[1].split(":");
            var effectiveDate = new Date(aryDate[2], 0, aryDate[0], aryTime[0], 0, 0, 0);//new Date(year, month, day, hours, minutes, seconds, milliseconds)
            effectiveDate.setMonth(aryDate[1]>0?aryDate[1]-1:aryDate[1] ); 
            var currentDateTime = new Date(); 
            currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
             
            if(effectiveDate.getTime()<currentDateTime.getTime())
            { 
                //alert("Please Enter valid Effective Date");
                $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                $('#datetimepicker').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
            
        }
        else
        {
            //alert("Please Enter valid Effective Date");
            $("#datetimepicker").parent().find(".my-error").html('Please enter effective date').show();
            $('#datetimepicker').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        $('.benefit-row').each(function(){
            //var lstAmount = $(this).find("[type='number']:visible");
            var lstAmount = $(this).find(".amount:visible");
            $(lstAmount).each(function(){
                if($.trim( $(this).val()).length==0){

                    //$(this).parent().find(".my-error").show();
                    $(this).parent().find(".my-error").html('Please fill amount').show();
                    //$(this).parent().addClass('has-error');
                    $(this).closest('.form-group-other').addClass('has-error');
                    is_valid = false;

                }
            });

            var lstperAmount = $(this).find(".perAmount:visible");
            $(lstperAmount).each(function(){
                if($(this).prop("selectedIndex")<0){

                    //$(this).parent().find(".my-error").show();
                    $(this).parent().find(".my-error").html('Please select percent or amount').show();
                    $(this).closest('.form-group-other').addClass('has-error');
                    is_valid = false;

                }
            });
        });


        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        //$(".form-control").removeClass('has-error');
        $(".form-group").removeClass('has-error');
        $(".form-group-other").removeClass('has-error');
    }

</script>
@endsection
