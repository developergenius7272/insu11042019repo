<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnersColumnInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('partner_policy_percentage',8,2)->nullable();
            $table->decimal('partner_max_amount',8,2)->nullable();
            $table->decimal('partner_policy_amount',8,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('partner_policy_percentage');
            $table->dropColumn('partner_max_amount');
            $table->dropColumn('partner_policy_amount');
        });
    }
}
