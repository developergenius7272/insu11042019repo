<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolicyValuesInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('policy_number')->nullable();
            $table->datetime('issue_date')->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->decimal('additional_fees', 8, 2)->nullable();
            $table->json('additional_product_selected')->nullable();
            $table->integer('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('policy_number');
            $table->dropColumn('issue_date');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('additional_fees');
            $table->dropColumn('additional_product_selected');
            $table->dropColumn('created_by');
        });
    }
}
