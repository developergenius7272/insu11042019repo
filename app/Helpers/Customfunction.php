<?php 

namespace App\Helpers;


use App\User;

use DB;
use Carbon\Carbon;
use Njasm\Soundcloud\SoundcloudFacade;
use Illuminate\Support\Facades\URL;
use Config;
use Session;
use Log;
use Storage;
use Redirect;
use Exception;



class Customfunction {


    // method to get notiifcation
    public static function randomString($length = 6) {
        $token = "";
        // $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        if(empty($token)) {
            for ($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[Customfunction::crypto_rand_secure(0, $max-1)];
            }
        }
        
        while(DB::table('fan_gate')->where('uid', $token)->count()) {
            $token = "";
            for ($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[Customfunction::crypto_rand_secure(0, $max-1)];
            }
        }
        return $token; 
    } 

    public static function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    } 

    //
    //*** Email Sender function for all emails ***
    //
    //  info array will contain email code to use with params
    //
    public static function emailSend($info)
    {
        $status = false;
        if (is_array($info)) {
            if (isset($info["code"])) {
                $emailtemplate = EmailTemplate::where('code',$info["code"])->first();
                if($emailtemplate) {
                    if ($emailtemplate->status == 1) {
                        if ($emailtemplate->template_code <> '') {
                            // from name
                            $mail_test_active_send = 1;
                            if (isset($info["mail_test_active_send"])) {
                                $mail_test_active_send = $info['mail_test_active_send'];
                            } else {
                                $mail_test_active_send = 1;
                            }
                            // get user id iinternal
                            $user_id = null;
                            if (isset($info["user_id"])) {
                                $user_id = $info["user_id"];
                            }
                            // from name
                            $from_name = '';
                            if (isset($info["from_name"])) {
                                $from_name = $info['from_name'];
                            } else {
                                $from_name = $emailtemplate->default_from_name;
                            }
                            // from email
                            $from_email = '';
                            if (isset($info["from_email"])) {
                                $from_email = $info['from_email'];
                            } else {
                                $from_email = $emailtemplate->default_from_email;
                            }
                            // first name
                            $first_name = '';
                            if (isset($info["first_name"])) {
                                $first_name = $info['first_name'];
                            }
                            if ($first_name == '') {
                                $first_name = "buddy";
                            }
                            // to name
                            $to_name = '';
                            if (isset($info["to_name"])) {
                                $to_name = $info['to_name'];
                            } else {
                                $to_name = $emailtemplate->default_to_name;
                            }
                            // to email
                            $to_email = '';
                            if (isset($info["to_email"])) {
                                $to_email = $info['to_email'];
                            } else {
                                $to_email = $emailtemplate->default_to_email;
                            }
                            // get subject
                            $subject = '';
                            if (isset($info["subject"])) {
                                $subject = $info['subject'];
                            } else {
                                $subject = $emailtemplate->default_subject;
                            }

                            if ($from_name == '') {
                                $from_name = env('MAIL_FROM_NAME', 'InsureOnline');
                            }
                            if ($from_email == '') {
                                $from_email = env('MAIL_FROM_EMAIL', 'info@baltech.in');
                            }
                            if ($to_email == '') {
                                $to_email = env("MAIL_ADMIN_EMAIL", "info@baltech.in");
                            }
                            if ($to_name == '') {
                                $to_name = null;
                            }

                            // create and send
                            $from = new \SendGrid\Email($from_name, $from_email);
                            $to = new \SendGrid\Email($to_name, $to_email);

                            $content = new \SendGrid\Content("text/html", " ");
                            $mail = new \SendGrid\Mail($from, $subject, $to, $content);

                            // reply to code
                            if (isset($info["reply_to"])) {
                                $reply_to = new \SendGrid\ReplyTo($info["reply_to"]);
                                $mail->setReplyTo($reply_to);
                            } else {
                                if ($from_email <> '') {
                                    $reply_to = new \SendGrid\ReplyTo($from_email);
                                    $mail->setReplyTo($reply_to);
                                }
                            }

                            // ************* using newer templating scheme if base template ************************
                            if ($emailtemplate->main_html <> '') {

                                $title = $emailtemplate->title;
                                $main_html = $emailtemplate->main_html;
                                $button_title = $emailtemplate->button_title;
                                $button_link = $emailtemplate->button_link;

                                $replace_array = array();
                                if (isset($info["replace_values"])) {
                                    $replace_array = $info["replace_values"];
                                }
                                $replace_array["{{first_name}}"] = $first_name;

                                if (count($replace_array)>0) {
                                    foreach ($replace_array as $key => $value) {
                                        if (($value == null) or ($value == '')) {
                                            $value = ' ';
                                        }
                                        $value = (string)$value;

                                        $main_html = str_replace($key, $value, $main_html);
                                        $subject = str_replace($key, $value, $subject);
                                        $title = str_replace($key, $value, $title);
                                        $button_title = str_replace($key, $value, $button_title);
                                        $button_link = str_replace($key, $value, $button_link);
                                    }
                                }

                                // special case for no clicking tracking in Send Grid
                                $main_html = str_replace('class="noclicktracking"', 'clicktracking=off', $main_html);

                                $mail->addSection("{{body_section}}", $main_html);
                                $mail->personalization[0]->addSubstitution("{{subject}}", $subject);
                                $mail->personalization[0]->addSubstitution("{{title}}", $title);
                                $mail->personalization[0]->addSubstitution("{{button_title}}", $button_title);
                                $mail->personalization[0]->addSubstitution("{{button_link}}", $button_link);
                                $mail->personalization[0]->addSubstitution("{{body}}", "{{body_section}}");

                            } else {
                                // **************** current way!!!!!
                                //
                                if (isset($info["replace_values"])) {
                                    $replace_array = $info["replace_values"];
                                    foreach ($replace_array as $key => $value) {
                                        if (($value == null) or ($value == '')) {
                                            $value = ' ';
                                        }
                                        $value = (string)$value;
                                        ///\Log::info("  Email Send Keys: code : ".$info['code']." ".$emailtemplate->template_code." : key : ".$key." value : ".$value);

                                        $mail->personalization[0]->addSubstitution($key, $value);
                                    }
                                }
                                if (isset($info["section_values"])) {
                                    $section_array = $info["section_values"];
                                    foreach ($section_array as $key => $value) {
                                        if (($value == null) or ($value == '')) {
                                            $value = ' ';
                                        }
                                        $value = (string)$value;
                                        $mail->addSection($key, $value);
                                    }
                                }
                            }


                            $mail->setTemplateId($emailtemplate->template_code);
                            $apiKey = env('SENDGRID_APIKEY', false);
                            $sg = new \SendGrid($apiKey);
                            try {
                                //
                                // log this email was sent
                                // $email_log = new EmailLog();
                                // $email_log->user_id = $user_id;
                                // $email_log->code = $info["code"];
                                // $email_log->email_template_id = $emailtemplate->id;
                                // $email_log->template_code = $emailtemplate->template_code;
                                // $email_log->subject = $subject;
                                // $email_log->to_name = $to_name;
                                // $email_log->to_email = $to_email;
                                // $email_log->from_name = $from_name;
                                // $email_log->from_email = $from_email;
                                // if ($mail_test_active_send == 0) {
                                //     $email_log->subject = "[Did Not Send] " . $subject;
                                // }
                                // $email_log->save();
                                //
                                $email_log_id = $email_log->id;
                                $mail->addCustomArg("environment", env("APP_ENV"));
                                $mail->addCustomArg("logid", (string)$email_log_id);
                                //

                                try {
                                    if ($mail_test_active_send == 0) {
                                        // do not send this email via sendgrid but act like it did
                                        //
                                    } else {
                                        $response = $sg->client->mail()->send()->post($mail);
                                        ////\Log::info("Email Send Response: email log id : ".$email_log_id." : code : ".$response->statusCode()." ".$response->body());
                                        if ($response->statusCode() == 400) {
                                            \Log::info("    Response: email log id : " . $email_log_id . " : code : " . print_r($response, true));
                                        }
                                    }
                                } catch (Exception $e) {
                                    \Log::info("Email Send Error: " . $e->getMessage());
                                }


                                $status = true;
                                //
                                //
                            } catch (Exception $e) {
                                $status = false;
                                \Log::info("Email Create/Send Error: " . $e->getMessage());
                            }
                        }
                    } else {
                        \Log::info("Email Template INACTIVE: " . $emailtemplate->code);
                    }
                }
            }
        }
        return $status;
    }
    //
    //*** Audit Log - send code and user info to log users progress in app ***
    //
    //  info array will contain email code to use with params
    //
    public static function audit($info)
    {
        if (is_array($info)) {
            if (isset($info["code"])) {
                // create log entry
                $audit_log = new AuditLog();
                $audit_log->code = $info["code"];
                if (isset($info["user_id"])) {
                    $audit_log->user_id = $info["user_id"];
                }
                if (isset($info["admin_id"])) {
                    $audit_log->admin_id = $info["admin_id"];
                }
                
                if (isset($info["comments"])) {
                    $audit_log->comments = $info["comments"];
                }
                // set location
                $ip_address = '';
                try {
                    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        //$ip_address = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
                        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    }
                    if (($ip_address == '') and (isset($_SERVER['REMOTE_ADDR']))) {
                        $ip_address = $_SERVER['REMOTE_ADDR'];
                    }
                } catch (\Exception $e) {
                    //
                }
                if ($ip_address <> '') {
                    $audit_log->ip_address = $ip_address;
                }
                //
                // set user agent
                $user_agent = '';
                try {
                    if (isset($_SERVER['HTTP_USER_AGENT'])) {
                        $user_agent = $_SERVER['HTTP_USER_AGENT'];
                    }
                } catch (\Exception $e) {
                }
                if ($user_agent <> '') {
                    $audit_log->user_agent = $user_agent;
                }
                //
                $audit_log->save();
                //
            }
        }
    }
   
    

    public static function redact($str) {
        
        if(trim($str)){
            $len = strlen($str);
            if($len>1){
                return substr($str, 0, 1).str_repeat('*', $len-2).substr($str, $len - 1, 1);    
            }else{
                return $str;
            }
        }
        
    }

    public static function randomInteger($length = 5) {
        $token = "";
        // $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet = "0123456789";
        $max = strlen($codeAlphabet); // edited

        if(empty($token)) {
            for ($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[Customfunction::crypto_rand_secure(0, $max-1)];
            }
        }
        return $token; 
    }



    public static function displayHoursMinutes($reviewed_diff_in_mins) {
        //
        $diff_hours = floor($reviewed_diff_in_mins / 60);
        $diff_minutes = $reviewed_diff_in_mins - ($diff_hours * 60);
        if ($diff_hours == 0) { $hours_text = ""; }
        elseif ($diff_hours == 1) { $hours_text = $diff_hours." Hour"; }
        else { $hours_text = $diff_hours." Hours"; }
        if ($diff_minutes == 0) { $minutes_text = ""; }
        elseif ($diff_minutes == 1) { $minutes_text = $diff_minutes." Minute"; }
        else { $minutes_text = $diff_minutes." Minutes"; }
        $reviewed_diff_in_mins_formatted = $hours_text;
        if ($reviewed_diff_in_mins_formatted <> '') { $reviewed_diff_in_mins_formatted .= " ".$minutes_text; }
        else {  $reviewed_diff_in_mins_formatted = $minutes_text; }
        //
        return trim($reviewed_diff_in_mins_formatted);
    }




    public static function displayFeedTimeInterval($feed_datetime) {
        //
        $total_seconds = $feed_datetime->diffInSeconds();
        $return_display = '';
        $diff_days = floor($total_seconds / (60 * 60 * 24));
        if ($diff_days==0) {
            $diff_hours = floor($total_seconds / (60 * 60));
            if ($diff_hours == 0) {
                $diff_minutes = floor($total_seconds / 60);
                if ($diff_minutes == 0) {
                    $return_display = $total_seconds." Seconds Ago";
                } else {
                    if ($diff_minutes == 1) {
                        $return_display = $diff_minutes." Minute Ago";
                    } else {
                        $return_display = $diff_minutes." Minutes Ago";
                    }
                }
            } else {
                if ($diff_hours == 1) {
                    $return_display = $diff_hours." Hour Ago";
                } else {
                    $return_display = $diff_hours." Hours Ago";
                }
            }
        } else {
            if ($diff_days == 1) {
                $return_display = $diff_days." Day Ago";
            } else {
                $return_display = $diff_days." Days Ago";
            }
        }
        //
        return trim($return_display);
    }


    /**
     * @name        : uniqueStringByTableField
     * @Description : Generate unique String for given table's field name
     * @param       : table_name in db, field name where have to check if already available in table or not, string length
     * @return      : random alphanumeric code
     *
     */ 
    public static function uniqueStringByTableField($table='users',$field='reset_password_token',$length=6) {
        $token = "";
        if(!empty($table) && !empty($field)){
            // $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet.= "0123456789";
            $max = strlen($codeAlphabet); // edited

            $ok_token = false;

            while($ok_token == false) {
                $token = "";
                for ($i=0; $i < $length; $i++) {
                    $token .= $codeAlphabet[Customfunction::crypto_rand_secure(0, $max-1)];
                }

                if (($table == "fan_gate") or ($table == "shares")) {
                    $token_match_gates  = DB::table("fan_gate")->where("uid", $token)->count();
                    $token_match_shares = DB::table("shares")->where("code", $token)->count();
                    $token_match_routes = DB::table("hype_routes")->where("route_name", $token)->count();
                    if (($token_match_gates==0) and ($token_match_shares==0) and ($token_match_routes==0)) {
                        $ok_token = true;
                    }
                } else {
                    $token_match = DB::table($table)->where($field, $token)->count();
                    if ($token_match==0) {
                        $ok_token = true;
                    }
                }
            }
        }
        return $token;
    }
    
    /*
    *  getUniqueSlugs
    */
    public static function getUniqueSlugs($slug, $table='pages',$field='slug',$excludeKey = NULL, $excludeValue = NULL)
    {
        if(!is_null($table) && !empty($table) && !is_null($field) && !empty($field) ){
            $slug  = str_slug($slug);
            $match = true;
            $max_iteration = 100;
            $iteration = 0;
            $i     = 0;
            while ($match){
                $matchQuery = DB::table($table);
                if(!empty($field)){
                    $matchQuery = $matchQuery->where($field,$slug);
                }
                if(!empty($excludeKey)){
                    $matchQuery = $matchQuery->where($excludeKey,'!=',$excludeValue);
                }

                $matchQueryCount = $matchQuery->count();
                $token_match_routes = DB::table("prohibited_routes")->where("route_name", $slug)->count();
                if($matchQueryCount == 0 && $token_match_routes == 0){
                    $match = false;
                }else{
                    if (!preg_match('/-{1}[0-9]+$/', $slug))
                        $slug .= '-' . ++$i;
                    else
                        $slug = preg_replace('/[0-9]+$/', ++$i, $slug);    
                }
                $iteration++;
                if( !($iteration < $max_iteration) ) {
                    $match = false;
                }
            }
            return $slug;
        }else{
            return null;
        }
    }

    /**
     * Prep URL
     *
     * Simply adds the http:// part if no scheme is included
     *
     * @param   string  the URL
     * @return  string
     */
    public static function prep_url($str = '')
    {
        if ($str === 'http://' OR $str === '')
        {
            return '';
        }
        $url = parse_url($str);
        if ( ! $url OR ! isset($url['scheme']))
        {
            return 'http://'.$str;
        }
        return $str;
    }

    
    /*
     * encodeChatEmailAddress
     */
    public static function encodeChat($chat_code,$second_code) {

        $token = '';
        $codeAlphabet = "abcdefghjkmnpqrstuvwxy";
        $max          = strlen($codeAlphabet); // edited

        for ($i=0; $i < strlen($chat_code); $i++) {
            $token = $chat_code[$i].$token;
            if ($i == 6) {
                $second_hashed = '';
                $second_code_str = (string) $second_code;
                for ($j=0; $j < strlen($second_code_str); $j++) {
                    $second_hashed = $codeAlphabet[Customfunction::crypto_rand_secure(0, $max-1)].$second_code_str[$j].$second_hashed;;
                }
                $token = "z".$second_hashed."z".$token;
            }
        }
        //
        return $token;
    }
    /*
     * decodeChatEmailAddress
     */
    public static function decodeChat($token) {
        //
        $chat_code = ''; $second_code = '';
        //
        try {
            $token_array = explode("z", $token);
            if (count($token_array) == 3) {
                $chat_code_reversed = $token_array[0] . $token_array[2];
                for ($i = 0; $i < strlen($chat_code_reversed); $i++) {
                    $chat_code = $chat_code_reversed[$i] . $chat_code;
                }
                $second_code_reversed = $token_array[1];
                $second_next_char = 0;
                for ($i = 0; $i < strlen($second_code_reversed); $i++) {
                    if ($second_next_char == 1) {
                        $second_code = $second_code_reversed[$i] . $second_code;
                        $second_next_char = 0;
                    } else {
                        $second_next_char = 1;
                    }
                }
            }
        } catch(\Exception $ecode) {
        }
        return $chat_code."-".$second_code;
    }
    

    /*
     * makeClickableLinks - return with hrefs - also string make displayable non-html text
     */
    public static function makeChatMessageConvert($string) {
        $string = htmlspecialchars($string);
        $string = nl2br($string);
        //return preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.%-=#]*(\?\S+)?)?)?)@', '<a href="$1">$1</a>', $s);
        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
        $string = preg_replace($url, '<a href="$0" target="_blank" rel=noopener title="$0">$0</a>', $string);
        return  $string;
    }
    /**
    * Format bytes to kb, mb, gb, tb
    *
    * @param  integer $size
    * @param  integer $precision
    * @return integer
    */
    public static function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size     = (int) $size;
            $base     = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }
    // 
    public static function setLoginRedirect($static_path=null) {
        // $request = new Request;
        // dd($request->url());
        if(is_null($static_path) || empty($static_path)){
            $path = url::full();
            Session::put("login_redirect",$path);
        }elseif(!empty($static_path)){
            Session::put("login_redirect",$static_path);
        }
        return Redirect::to('/login');
    }
}