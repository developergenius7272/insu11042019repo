@extends('layouts.master_insurance')
@section('css')
<style>
#loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #000;
   z-index: 99;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 100;
}
</style>
@endsection
@section('content')
<div id="loading" style="display: none">
  <img id="loading-image" src="{{URL::asset('images/loading.svg')}}" alt="Loading..." />
</div>
 <!-- Main Step Form Box Start -->
        <div class="payment-policy-part">
          <div class="payment-policy-inner-part">
            <div class="payment-tabs-info">
              
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                @if(!$additional_Products->isEmpty())
                  <li class="nav-item"> <a class="nav-link active" id="infotab_1-tab" data-toggle="tab" href="#infotab_1" role="tab" aria-controls="infotab_1" aria-selected="true">Additional Products</a> </li>
                @endif
                  <li class="nav-item"> <a @if(!$additional_Products->isEmpty()) class="nav-link" @else class="nav-link active" @endif id="infotab_2-tab" data-toggle="tab" href="#infotab_2" role="tab" aria-controls="infotab_2" aria-selected="false">What’s Included</a> </li>
              </ul>

              <div class="tab-content" id="myTabContent">
              <!-- additional Products section -->
              @if(!$additional_Products->isEmpty())
                <div class="tab-pane fade show active" id="infotab_1" role="tabpanel" aria-labelledby="infotab_1-tab">
                  <div class="payment-tab-content">
                    <table class="table table-bordered table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Select</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Provider Name</th>
                            <th scope="col">Sum Insured</th>
                            <th scope="col">Premium</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach( $additional_Products as $products )
                          <tr>
                            <td><input type="checkbox" class="product_select" data-id="{{ $products->id }}"  data-val = "{{ $products->premium }}" data-name="{{ $products->name }}" data-vat="{{ $products->vat }}" data-fee="{{ $products->fee }}" data-feetype="{{ $products->fee_type }}" /></td>
                            <td>{{ $products->name }}</td>
                            <td>{{ isset($products->insuranceProvider) ? $products->insuranceProvider->name : '' }}</td>
                            <td>{{ $products->sum_insured }}</td>
                            <td>{{ $products->premium }}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                  </div>
                </div>
              @endif

                <div id="infotab_2" role="tabpanel" aria-labelledby="infotab_2-tab" @if(!$additional_Products->isEmpty()) class="tab-pane fade" @else class="tab-pane fade show active" @endif>
                  <div class="payment-tab-content">
                    <h4>What’s Included</h4>
                    <ul class="include-option clearfix">
                    @foreach($payment_data as $key => $value)
                        @if( $value === "no" )
                          <li class="untick">{{ $key }}</li>
                        @elseif($value === "yes" )
                          <li class="tick">{{ $key }}</li>
                        @else
                         <li class="tick"> <input type="checkbox" class="custom-control-input" id="pap" value= {{$value}} />{{ $key }}({{$value}})</li>
                        @endif
                    @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="policy-payment-detail-part">
              <div class="row">
                <div class="col-lg-7">
                  <div class="payment-form-part">
                  <form method="POST" action="{{ route('order-submit') }}">
                    @csrf
                    <div class="form-row">
                      <div class="col-md-12">
                        <label>Credit Card</label>
                      </div>
                      <div class="form-group col-md-6">
                        <input type="text" class="form-control" placeholder="Card Number">
                      </div>
                      <div class="form-group col-md-6">
                        <input type="text" class="form-control" placeholder="Cardholder’s Name">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12">
                        <label>Card Expiry Date</label>
                      </div>
                      <div class="form-group col-md-8">
                        <div class="form-inline">
                          <input type="text" class="form-control" placeholder="MM" style="width:46%">
                          <span style="width:8%;" class="cc-divider"> / </span>
                          <input type="text" class="form-control" placeholder="YYYY" style="width:46%">
                        </div>
                      </div>
                      <div class="form-group col-md-4">
                        <input type="text" class="form-control" placeholder="CVV">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-12">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="termsAgree" required>
                          <label class="custom-control-label" for="termsAgree">I agree to the website terms and conditions and the insurer terms and conditions</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-btn-part">
                      <button type="submit"  id ="buynow" class="btn btn-secondary">Buy Insurance Now</button>
                    </div>
                  </form>
                  </div>
                </div>
                <div class="col-lg-5">
                  <div class="policy-cost-box">
                    <div class="policy-secure-by">
                      <h5>Payment Made Secure By</h5>
                      <ul>
                        <li><i class="fa fa-cc-visa"></i></li>
                        <li><i class="fa fa-cc-mastercard"></i></li>
                        <li><i class="fa fa-cc-paypal"></i></li>
                        <li><i class="fa fa-cc-discover"></i></li>
                      </ul>
                    </div>
                    <div class="polist-cost-part">
                      <h5>Insurance details</h5>
                      <table class="table">
                        <tbody>
                          <tr>
                            <th>Base Price</th>
                            <td>{{ env('CURRENCY_CODE', 'USD') }} {{$premiumvalue}}</td>
                          </tr>
                          {{--  <tr>
                            <th>Additional Covers</th>
                            <td id="addValue">{{ env('CURRENCY_CODE', 'USD') }} 0.00</td>
                          </tr>  --}}
                          <tr>
                            <th>VAT</th>
                            <td>{{ env('CURRENCY_CODE', 'USD') }} {{ $vat_premium }}</td>
                          </tr>
                           <tr>
                            <th>Policy Fees</th>
                            <td >{{ env('CURRENCY_CODE', 'USD') }} {{ $policy_fees }}</td>
                          </tr>
                          <tr class="total-tr">
                            <th>Sub Total</th>
                            <td >{{ env('CURRENCY_CODE', 'USD') }} {{ $total }}</td>
                          </tr>
                          {{--  <tr class="total-tr">
                            <th>Total</th>
                            <td id="totalPrice">{{ env('CURRENCY_CODE', 'USD') }} {{ $total }}</td>
                          </tr>  --}}
                        </tbody>
                      </table>
                       <h5>Additional Product details</h5>
                      <table class="table addonproduct">
                        <tbody>
                          <tr>
                            <th>Product Name</th>
                            <th>Price</th>
                            
                          </tr>
                          {{--  <tr id="product-name">  --}}
                          {{--  <td id="product-price"></td>  --}}
                          {{--  <tr>  --}}
                          {{--  <tr>
                            <th>Additional Covers</th>
                            <td id="addValue">{{ env('CURRENCY_CODE', 'USD') }} 0.00</td>
                          </tr>  --}}
                          <tr class="default_demo">
                            <th>VAT</th>
                            <td id="vatValue">{{ env('CURRENCY_CODE', 'USD') }} 0.0</td>
                          </tr>
                           <tr class="default_demo">
                            <th>Policy Fees</th>
                            <td id="policyFees" >{{ env('CURRENCY_CODE', 'USD') }} 0.0</td>
                          </tr>
                          <tr class='total-tr default_demo'><th colspan='3'>&nbsp;</th></tr>
                          <tr class="">
                            <th>Sub Total</th>
                            <td id="addValue">{{ env('CURRENCY_CODE', 'USD') }} 0.00</td>
                          </tr>
                          <tr class="total-tr">
                            <th>Total</th>
                            <td id="totalPrice">{{ env('CURRENCY_CODE', 'USD') }} {{ $total }}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="btns-part">
              <button type="submit" class="btn btn-outline-light">Back</button>
            </div>
          </div>
        </div>
        <!-- Main Step Form Box End --> 
    @endsection
@section('script')

<script>
$(document).ready(function(){

  $('body').on('change', '.product_select', function(event){
		event.preventDefault();
    updatePaymentpage($(this));
    return false;
   // //  $("#loading").show();
   // //  var push_view    = '';
   // //  var second_array = {};
   // //  // 
   // //  var value        = $(this).data("val");
   // //  var vat          = $(this).data("vat");
   // //  var addonFees    = $(this).data("fee");
   // //  var addonFeeType = $(this).data("feetype");
   // //  // 
   // //  var totalprice   = $("#totalPrice" ).text();
   // //  var total        = $("#addValue" ).text();  // sub total
   // //  totalprice       = parseFloat(totalprice.replace('{{ env('CURRENCY_CODE', 'USD') }}', ''));
   // //  total            = parseFloat(total.replace('{{ env('CURRENCY_CODE', 'USD') }}', ''));
   // //  // 
   // //  var vatprice     = $("#vatValue" ).text();
   // //  var policyFees   = $("#policyFees" ).text();
   // //  vatprice         = parseFloat(vatprice.replace('{{ env('CURRENCY_CODE', 'USD') }}', ''));
   // //  policyFees       = parseFloat(policyFees.replace('{{ env('CURRENCY_CODE', 'USD') }}', ''));
   // //  // 
   // //  var check_status = $(this). prop("checked");

	  // // if ( check_status == true ){
   // //     // addonvat
   // //     var vat_cal = ((value*vat)/100);
   // //     vatprice    += vat_cal;
       
   // //     // addon free
   // //     if(addonFeeType == 1) {
   // //      var fee_cal = ((value*addonFees)/100);
   // //      addonFees   = fee_cal
   // //     }

   // //     totalprice  += (value+addonFees+vat_cal);
   // //     total       += (value+addonFees+vat_cal);
   // //     // 
   // //     policyFees  += addonFees;
   // //     // 
   // //     // 
   // //     push_view   +="<tr data-id ="+$(this).data("id")+"><td>"+$(this).data("name")+"</td>";
   // //     push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+$(this).data("val")+"</td>";
   // //     push_view   +="</tr>";

   // //     push_view   +="<tr data-id ="+$(this).data("id")+"><td>VAT</td>";
   // //     push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+vat_cal+"</td>";
   // //     push_view   +="</tr>";

   // //     push_view   +="<tr data-id ="+$(this).data("id")+"><td>Policy Fees</td>";
   // //     push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+addonFees+"</td>";
   // //     push_view   +="</tr>";

   // //     push_view   +="<tr data-id ="+$(this).data("id")+" class='total-tr'><th colspan='3'>&nbsp;</th></tr>";

   // //     $(".default_demo").hide();
	  // // }else{
   // //    var vat_cal = ((value*vat)/100);
   // //    vatprice    -= vat_cal;

   // //    // addon free
   // //    if(addonFeeType == 1) {
   // //      var fee_cal = ((value*addonFees)/100);
   // //      addonFees   = fee_cal
   // //    }
      
   // //    totalprice  -= (value+addonFees+vat_cal); 
   // //    total       -= (value+addonFees+vat_cal);

   // //    policyFees  -= addonFees;
   // //    // 
   // //    $(".addonproduct").find('tr[data-id="' + $(this).data("id") + '"]').slideUp("fast", function() {
   // //          $(this).remove();
   // //      });
   // //  }
   // //  var str2          = "{{ env('CURRENCY_CODE', 'USD') }} ";
   // //  var res           = str2.concat(total);
   // //  var checked_value = $.each($(".product_select:checked"), function(){            
   // //                second_array[$(this).data("id")]=$(this).data("val");
                  
   // //            });
   // //  $(".addonproduct > tbody > tr:eq(1)").before(push_view);
   // //  //console.log(second_array);
   // //  var vat_value  = str2.concat(vatprice);
   // //  var totalValue = str2.concat(totalprice);
   // //  $("#addValue").text(res); 
   // //  $("#totalPrice").text(totalValue); 

    
   // //  var total_premium_value = totalprice;
   // //  var add_fees            = total;
   // //  var add_select          = JSON.stringify(second_array);
   // //  $("#vatValue").text(vat_value);
   // //  var vat_total       = vatprice;
   // //  var addon_policyfee = policyFees;
   // //  $("#policyFees").text(str2.concat(policyFees))
   // //    $.ajax({   
   // //         url: "{{url('/storepaydetails')}}",
   // //           headers: {
   // //              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   // //            },
   // //          dataType: 'json',
   // //          type: 'POST',
   // //          data: { add_select:add_select, add_fees:add_fees,vat_total:vat_total,total_premium_value:total_premium_value,addon_policyfee:addon_policyfee, _method: "POST"},
   // //          success: function(response) {
   // //                  // alert('success')
   // //                  $("#loading").hide();
   // //          },
   // //          complete: function() {
   // //            $("#loading").hide();
   // //          },
   // //   });

	}); 
});
function updatePaymentpage(obj){
  var second_array = {};
  var checked_value = $.each($(".product_select:checked"), function(){            
            second_array[$(this).data("id")]=$(this).data("val");
  });
  var add_select = JSON.stringify(second_array);
  $.ajax({   
      url: "{{url('/update-paymentpage')}}",
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: 'json',
      type: 'POST',
      data: { add_select:add_select, _method: "POST"},
      success: function(response) {
          if(response.action) {
            var totalprice   = response.policy_total;
            var subtotalprice   = 0;
            var addon = response.addOnProducts;
            var push_view    = '';
            $(".result_row").remove();
            if(addon.length){
              $(".default_demo").hide();

              for(i in addon){
                var value        = addon[i].premium;
                var vat          = addon[i].vat;
                var addonFees    = addon[i].fee;
                var addonFeeType = addon[i].fee_type;
                var id           = addon[i].id;
                var name         = addon[i].name;
                // 
                var vat_cal = ((value*vat)/100);
                if(addonFeeType == 1) {
                 var fee_cal = ((value*addonFees)/100);
                 addonFees   = fee_cal
                }
                // 
                totalprice += (value+addonFees+vat_cal);
                subtotalprice += (value+addonFees+vat_cal);

                push_view   +='<tr class="result_row" data-id ="'+id+'"><td>'+name+'</td>';
                push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+value+"</td>";
                push_view   +="</tr>";

                push_view   +="<tr class='result_row' data-id ="+id+"><td>VAT</td>";
                push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+vat_cal+"</td>";
                push_view   +="</tr>";

                push_view   +="<tr class='result_row' data-id ="+id+"><td>Policy Fees</td>";
                push_view   +="<td> {{ env('CURRENCY_CODE', 'USD') }} "+addonFees+"</td>";
                push_view   +="</tr>";

                push_view   +="<tr data-id ="+id+" class='total-tr result_row'><th colspan='3'>&nbsp;</th></tr>";
                $(".addonproduct > tbody > tr:eq(1)").before(push_view);
                push_view = "";

              }
            }else{
              $(".default_demo").show();
            }
              // console.log('totalprice ' + totalprice);
              // console.log('subtotalprice ' + subtotalprice);
            var currency = "{{ env('CURRENCY_CODE', 'USD') }} ";
            subtotalprice = currency.concat(subtotalprice);
            totalprice = currency.concat(totalprice);
            $("#addValue").text(subtotalprice)
            $("#totalPrice").text(totalprice)
            
          }
        },
      complete: function() {
        $("#loading").hide();
        },
      });
} 
</script>
@endsection