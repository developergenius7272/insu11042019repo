<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->decimal('policy_amount')->nullable();
            $table->decimal('exceed_value')->nullable();
            $table->decimal('personal_accidental_driver')->nullable();
            $table->tinyInteger('is_agency_repair')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_details');
    }
}
