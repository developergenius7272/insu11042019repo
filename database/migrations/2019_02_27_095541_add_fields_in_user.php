<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('passport_number',100)->after('base_commission_amount')->nullable();
            $table->string('passport_expiry_date',30)->after('passport_number')->nullable();
            $table->string('license_number',100)->after('passport_expiry_date')->nullable();
            $table->string('license_expiry_date',30)->after('license_number')->nullable();
            $table->string('uae_id',100)->after('license_expiry_date')->nullable();
            $table->string('uae_id_expiry_date',30)->after('uae_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
