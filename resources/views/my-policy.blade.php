{{-- @extends('layouts.master') --}}
@extends('dashboard')
@section('account-panel')

          <div class="col-md-9"> 
            <!-- Account Main Panel Start -->
            <div class="account-main-panel">
              <div class="account-main-heading">
                <h2><i class="fa fa-list-ul"></i> My Policies</h2>
              </div>
              <div class="account-main-box">
                <div class="my-policy-table-part"> 
                  <!-- Policy Table Start -->
                  <div class="my-policy-table-heading">
                    <h3>Active Policies</h3>
                  </div>
                  <div class="my-policy-table">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Sl. No.</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Policy No.</th>
                            <th scope="col">Date Issued</th>
                            <th scope="col">Expiry Date</th>
                            <th scope="col">Total</th>
                            <th scope="col">Premium Value</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($ordersactive->count())
                        @foreach($ordersactive as $order)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $order->insuranceProvider->name }}</td>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td>{{ $order->end_date }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->premium_value }}</td>
                            <td><i class="fa fa-check"></i></td>
                            <td><div class="dropdown">
                                <button class="btn btn-sm btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Extend </button>
                                <div class="dropdown-menu"> <a class="dropdown-item" href="#">Re-New</a> <a class="dropdown-item" href="#">Download Policy</a> </div>
                              </div></td>
                          </tr>
                          @endforeach
                          @else
                            <tr>
                              <td colspan="10">There are no active policies</td>
                            </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="my-policy-table-heading">
                    <h3>Expired Policies</h3>
                  </div>
                  <div class="my-policy-table">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">Sl. No.</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Policy No.</th>
                            <th scope="col">Date Issued</th>
                            <th scope="col">Expiry Date</th>
                            <th scope="col">Total</th>
                            <th scope="col">Premium Value</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($ordersexpired->count())
                        @foreach($ordersexpired as $order)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $order->insuranceProvider->name }}</td>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td>{{ $order->end_date }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->premium_value }}</td>
                            <td><i class="fa fa-check"></i></td>
                            <td><div class="dropdown">
                                <button class="btn btn-sm btn-link dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Extend </button>
                                <div class="dropdown-menu"> <a class="dropdown-item" href="#">Re-New</a> <a class="dropdown-item" href="#">Download Policy</a> </div>
                              </div></td>
                          </tr>
                          @endforeach
                          @else
                            <tr>
                              <td colspan="10">There are no expired policies</td>
                            </tr>
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- Policy Table Start --> 
                </div>
              </div>
            </div>
            <!-- Account Main Panel End --> 
          </div>
@endsection