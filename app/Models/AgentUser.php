<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentUser extends Model
{
    protected $table = 'agent_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_id', 'user_id', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_current_agent',1);
    }

    public function agent() {
        return $this->belongsTo('App\User', 'agent_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
