<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTypeAmountInCrossSellingProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cross_selling_products', function (Blueprint $table) {
            $table->tinyInteger('addon_commission_type')->after('addon_category_id')->default(0)->nullable()->comment = '0 = N/A, 1 = Percentage, 2 = Amount';
            $table->double('addon_commission_value')->after('addon_commission_type')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
