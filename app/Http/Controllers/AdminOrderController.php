<?php

namespace App\Http\Controllers;
// 
use App\Models\Order;
use App\User;
use App\Models\BenefitMaster;
use App\Models\PolicyDoc;

// 
use Illuminate\Http\Request;
use Image;
use Auth;
use Carbon\Carbon;

class AdminOrderController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Partners'; 
        $this->data['pageTitle'] = 'Partners';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $this->data['tableHeading'] = 'Orders';
    //     $orders = Order::all();
    //     // dd($orders);
    //     $this->data['orders'] = $orders;
    //     return view('admin.orders.index', $this->data);
    // }

    public function index(Request $request,$user_id=null)
    {
        
        $this->data['tableHeading'] = 'Orders';

        if(is_null($user_id)) {
            $user_id = $request->user_id;
        }

        if(isset($user_id) && $user_id > 0 ){
            $user = User::find($user_id);
            if( $user ) {
                 $orders = Order::where('users_id', $user->id)->get();
            }
        }else{
            $orders = Order::all();
        }

        $benefitMaster = BenefitMaster::all();
        $arrayBenefitMasterId = $benefitMaster->pluck('id')->toArray();
        $arrayBenefitMasterName = $benefitMaster->pluck('name')->toArray();

        $this->data['arrayBenefitMasterId'] = $arrayBenefitMasterId;
        $this->data['arrayBenefitMasterName'] = $arrayBenefitMasterName;
        $this->data['orders'] = $orders;
        return view('admin.orders.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$user_id=null)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Update partner';
        $this->data['tableHeading'] = 'Leads';
        // 
        $orders = Order::find($id);
        if( $orders ) {
            if(isset($user_id) && $user_id > 0 ){
                $user = User::find($user_id);
                if( $user ) {
                    if( $orders->users_id !== $user->id) {
                        return redirect()->route('admin.users.index')->with('error', 'No data found'); 
                    }
                }else{
                    return redirect()->route('admin.users.index')->with('error', 'No data found'); 
                }
            }
                // 
            $obj = json_decode($orders->benefit_options,true);
            $arrs =array();

            if($obj){
                $benefitMaster = BenefitMaster::all();

                $arrayBenefitMasterId = $benefitMaster->pluck('id')->toArray();
                $arrayBenefitMasterName = $benefitMaster->pluck('name')->toArray();

                foreach ($obj as $key => $value) {
                    // $arrs['key'][] = $key;
                    // $arrs['value'][] = $value;
                    // $benefitMasters = BenefitMaster::where('id', $key)->first();
                    // 
                    $master_id = array_search($key, $arrayBenefitMasterId);
                    if( $master_id !== false ) {
                        $arrs[$key]['name'] = $arrayBenefitMasterName[$master_id];
                    }
                    // 
                    if($value == 'yes'){
                        $value = 'Include';
                    }
                    elseif($value == 'no'){
                        $value = 'Not Include';   
                    }
                    // 
                    $arrs[$key]['value'] = $value;
                }
            }
            // 
            $this->data['arrs'] = $arrs;
            $this->data['data'] = $orders;
            return view('admin.orders.show', $this->data);
        }else{
            return redirect('/admin/orders')->with('error', 'No data found');
        }

    }

    public function edit($id)
    {
        $order = Order::find($id);
        if( $order ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update order';
            
            // 
            $this->data['data'] = $order;

            // 
            return view('admin.orders.update_edit', $this->data);
        } else {
            //return redirect('/admin/partners/third-party-covers')->with('error', 'No data found');
            return "error";
        }
    }

    public function update(Request $request, $id)
    {
        $adminID = Auth::id();
        $policyNo = $request->policy_no;
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $issueDate = $request->issue_date;
        $status = $request->status;
        if( !empty($startDate) ) {
            $startDate = Carbon::createFromFormat('d/m/Y H:i', $startDate);
        }
        if( !empty($endDate) ) {
            $endDate = Carbon::createFromFormat('d/m/Y H:i', $endDate);
        }
        if( !empty($issueDate) ) {
            $issueDate = Carbon::createFromFormat('d/m/Y H:i', $issueDate);
        }

        request()->validate([
            'policy_no' => 'required',
            "start_date"    => "required|date_format:d/m/Y H:i",
            "end_date"    => "required|date_format:d/m/Y H:i",
            "issue_date"    => "required|date_format:d/m/Y H:i",
        ]);
        $order = Order::find($id);

        if($order){

            $userid = $order->user ? $order->user->id : '';
            $username = $order->user ? $order->user->name : '';

            $order->policy_number = $policyNo;
            $order->start_date = $startDate;
            $order->end_date = $endDate;
            $order->issue_date = $issueDate;
            $order->status = $status;
            $order->save();

            // upload image
            $imageName = '';
            try{
                if ($request->hasFile('image')) { 

                    $file = request()->image->getClientOriginalName();
                    //$filename = pathinfo($file, PATHINFO_FILENAME);
                    $filename = $order->policy_number.'-'.$file;
                    //$filename = $order->policy_number .'.'. request()->image->getClientOriginalExtension();
                    // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();
                    
                    //$request->image->move(public_path('uploads/admin/policydoc'), $imageName);

                    $path = public_path('uploads/admin/policydoc');
                    $userDir  = $path.'/' . $username . "/";

                    if(is_dir($userDir)==false){ 
                        $old = umask(0); 
                        mkdir("$userDir", 0755); 
                        umask($old);
                    }

                    if(is_dir("$userDir/".$filename)==false){
                        //move_uploaded_file($file_tmp, "$workerDirByWork/".$file_name);
                        //$request->image->move(public_path('uploads/admin/policydoc'), $filename);
                        $request->image->move($userDir, $filename);
                    }

                    if (strlen($filename)>1)
                    {
                        //$imgurl1 =  URL . $workerDirByWork . $file_name;

                        PolicyDoc::where('order_id',$order->id)->delete();

                        $policy_doc = new PolicyDoc();
                        $policy_doc->user_id = $userid;
                        $policy_doc->order_id = $order->id;
                        $policy_doc->docname = $filename;
                        $policy_doc->save();

                    }

                }
            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }

            return redirect('/admin/orders')->with('success', 'Policy Details updated successfully');
        }else{

            return redirect('/admin/orders')->with('error', 'Policy Details not updated');
        }

    }
}
