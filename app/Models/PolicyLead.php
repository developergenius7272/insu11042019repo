<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PolicyLead extends Model
{
    protected $table = 'policy_leads';
    // protected $dates = ['register','dob','car_registration_date'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    // 
    public function carMake() {
        return $this->belongsTo('App\Models\CarMake', 'car_make_id', 'id');
    }
    // 
    public function carModel() {
        return $this->belongsTo('App\Models\CarModel', 'car_model_id', 'id');
    }
    // 
    public function CarModelVariant() {
        return $this->belongsTo('App\Models\CarModelVariant', 'car_model_variant_id', 'id');
    }
    // 
    public function order() {
        return $this->hasOne('App\Models\Order', 'order_id', 'id');
    }
}
