<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\BenefitMaster;
use App\Models\PlanBenefitMultiple;
use Illuminate\Support\Facades\DB;

class PlanBenefit extends Model
{
    protected $fillable = [
        'insurance_providers_id', 'year', 'effective_date', 'plan_type', 'benefit_masters_id', 'included', 'single_or_multiple', 'no_of_years', 'description',
    ];

    protected $dates = ['effective_date'];

    public function benefitMaster()
    {
        return $this->belongsTo(BenefitMaster::class, 'benefit_masters_id', 'id');
    }

    public function planBenefitMultiple()
    {
        return $this->hasMany(PlanBenefitMultiple::class, 'plan_benefits_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePlanType($query)
    {
        return $query->where('plan_type', 1);
    }

    public function scopeComprehensive($query)
    {
        return $query->where('plan_type', 0);
    }

    public function scopeIncluded($query)
    {
        return $query->where('included', 1);
    }
    public function scopeEffectiveDate($query)
    {   
        return $query->where('effective_date', $query->orderBy('effective_date', 'DESC')->pluck('effective_date')->first())->orWhere('effective_date', '=', null);
    }
    

    public function planAgencyBenefit($priceAEDAmount){
        $amountAgency = ($priceAEDAmount *(float) $this->amount) / 100;
        return ( $this->percentage_or_amount == 1 ) ?  $amountAgency : $this->amount;
    }

    public function planNonAgencyBenefit($priceNonAEDAmount){
        $amountNonAgency = ($priceNonAEDAmount *(float) $this->amount) / 100;
        return ( $this->percentage_or_amount == 1 ) ?  $amountNonAgency : $this->amount;
    }
    
    public function amount($premiumValue, $no_of_seats, $benefit_id)
    {
        if ($benefit_id == 6) {
            if ($this->percentage_or_amount == 1 ) { 
                $amountcalcultaed = ($premiumValue * $this->amount) / 100;
                if ($no_of_seats == 0 ) {
                    $amountcalcultaed = 0;
                    return $amountcalcultaed;
                }else{
                    return  (($no_of_seats - 1) * $amountcalcultaed);
                }
            } else {
                if ($no_of_seats == 0 ) {
                    $amountcalcultaed = 0;
                    return $amountcalcultaed;
                }else{
                    return (($no_of_seats - 1) * $this->amount);
                }   
            }
        } else {
            if ($this->percentage_or_amount == 1 ) { 
                $amountcalcultaed = ($premiumValue * $this->amount) / 100;
                return  $amountcalcultaed;
            } else {
                return $this->amount;
            }
        }
    }
}
