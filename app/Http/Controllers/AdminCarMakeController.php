<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminCarMakeController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('role:super-admin|sub-admin');
        $this->data['breadcrumbs'][] = 'Car'; 
        $this->data['breadcrumbs'][] = 'Make';
        $this->data['pageTitle'] = 'Car Makes';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Car Makers';
        //$this->data['carMakes'] = CarMake::with('carmodels')->get();
        $this->data['carMakes'] = CarMake::with('carmodels')->orderBy('name', 'asc')->get();
        return view('admin.car_make.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new car make';
        return view('admin.car_make.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = trim($request->name);
        $status = $request->status;

        request()->validate([
            'name' => 'required|unique:car_makes|min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);


        if(!empty( $name ))
        {

            $body = CarMake::where('name',$name)->first();
            if( !$body ) {
                $body = new CarMake();
                $body->name = $name;
                $body->status = $status;
                $body->created_by = $adminID;
                $body->modified_by = $adminID;
                $body->save();

                // upload image
                $imageName = '';
                try{
                    if ($request->hasFile('image')) { 
                        
                        // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();
                        
                        $imageName = $body->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $body->id.'_main';
                        $request->image->move(public_path('uploads/admin/car_makes'), $imageName);
                        // resize for logo
                        $imageLogo = $body->id.'_logo';
                        $img = Image::make(public_path('uploads/admin/car_makes/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/admin/car_makes/' . $imageLogo));

                        $body->has_logo = 1;
                        $body->save();

                    }
                }
                catch(\Exception $e){
                    // do task when error
                    // echo $e->getMessage();
                }
                
                return redirect('/admin/cars/car-makes')->with('success', 'Car Makes added successfully');
            } else {
                return redirect('/admin/cars/car-makes')->with('error', 'Car Makes has not added');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit car make';
        $this->data['data'] = CarMake::find($id);

        if( $this->data['data'] ) {
            // 
            return view('admin.car_make.create_edit',$this->data);
        } else {
            return redirect('/admin/cars/car-makes')->with('error', 'No data found');
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();
        
        $name = trim($request->name);
        $status = $request->status;

        request()->validate([
            'name' => "required|unique:car_makes,name,$id|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        if(!empty($name ))
        {
            $body = CarMake::where('name',$name)->where('id','<>',$id)->first();
            if( !$body ) {
                $body = CarMake::find($id);

                if( $body ) {
                    $body->name = $name;
                    $body->status = $status;
                    $body->modified_by = $adminID;
                    // $body->has_logo = $request->has_logo_image;
                    // $body->save();
                    if ($request->has_logo_image == '0' ){
                        //$body->has_logo = $request->has_logo_image;
                        $body->has_logo = 0;
                    }


                    $imageName = '';
                    try{
                        if ($request->hasFile('image')) { 
                            // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                            $imageName = $body->id.'_main';
                            $request->image->move(public_path('uploads/admin/car_makes'), $imageName);
                            // resize for logo
                            $imageLogo = $body->id.'_logo';
                            $img = Image::make(public_path('uploads/admin/car_makes/' . $imageName));
                            $img->fit(75)->save(public_path('uploads/admin/car_makes/' . $imageLogo));

                            $body->has_logo = 1;
                        }

                    }
                    catch(\Exception $e){
                        // do task when error
                        // echo $e->getMessage();
                    }

                    $body->save();

                    return redirect('/admin/cars/car-makes')->with('success', 'Car Makes updated successfully');
                } else {
                    return redirect('/admin/cars/car-makes')->with('error', 'Car Makes not updated');
                }
            } else {
                return redirect('/admin/cars/car-makes')->with('error', 'Car Makes not updated');
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carMake = CarMake::find($id);
        if( $carMake ) {
            // find child data if any
            $carModelsCount = \App\Models\CarModel::where('car_make_id', $carMake->id)->count();
            if( $carModelsCount ) {
                return redirect('/admin/cars/car-makes')->with('success','Information has not been deleted');
            } else {
                $carMake->delete();
                return redirect('/admin/cars/car-makes')->with('success','Information has been deleted');
            }
        } else {
            return redirect('/admin/cars/car-makes')->with('success','No information found');
        }
    }




}
