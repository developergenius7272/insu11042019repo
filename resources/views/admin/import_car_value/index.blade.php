@extends('layouts.admin.master')
{{-- car_model_variants table view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>{{$tableHeading ?? 'Cars'}}</h2>

                    </header>
                    
                     
                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            {{-- <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p> --}}
                            
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Car Maked</th>
                                        <th>Model Name</th>
                                        <th>Variant Name</th>
                                        <th>Values</th>
                                        <th>Min Value</th>
                                        <th>Max Value</th>
                                        <th>Insurance Year</th>
                                        <th>Manufacture Year</th>
                                        <th>Effective Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($carValues)
                                    @foreach($carValues as $carValue)
                                    <tr>
                                        <td>{{ $carValue->make_name }}</td>
                                        <td>{{ $carValue->model_name }}</td>
                                        <td>{{ $carValue->variant_name }}</td>
                                        <td>{{ $carValue->values }}</td>
                                        <td>{{ $carValue->min_value }}</td>
                                        <td>{{ $carValue->max_value }}</td>
                                        <td>{{ $carValue->insurance_year }}</td>
                                        <td>{{ $carValue->manufacture_year }}</td>
                                        <td>{{ $carValue->effective_date }}</td>

                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            
        </div>

    </section>
</div>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
		
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
    $(document).ready(function() {
        
        pageSetUp();
        
        /* // DOM Position key index //
    
        l - Length changing (dropdown)
        f - Filtering input (search)
        t - The Table! (datatable)
        i - Information (records)
        p - Pagination (paging)
        r - pRocessing 
        < and > - div elements
        <"#id" and > - div with an id
        <"class" and > - div with a class
        <"#id.class" and > - div with an id and class
        
        Also see: http://legacy.datatables.net/usage/features
        */	

        /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

    })

</script>
@endsection
