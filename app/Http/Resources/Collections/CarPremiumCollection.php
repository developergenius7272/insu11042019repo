<?php

namespace App\Http\Resources\Collections;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Http\Resources\CarPremium as CarPremiumResource;
use App\Models\CarPremium;

class CarPremiumCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (CarPremium $carPremium) use($request){
            return (new CarPremiumResource($carPremium));
        });
        return parent::toArray($request);
    }
}
