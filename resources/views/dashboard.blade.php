@extends('layouts.master')
@section('content')

<div class="rowx">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
        @endif
    </div>
</div>
<div class="rowx">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- Main Container Start -->
<section class="main-container-section"> 
  <!-- My Account Container Start -->
  
  <div class="account-main-container">
    <div class="container is_myaccount">
      <div class="account-main-section">

        <div class="row no-gutters">
          <div class="col-md-3"> 
            <!-- Account Sidebar Start -->
            <div class="account-sidebar">
              <nav class="sidebar">
                <div class="sidebar-header">
                  <div class="avatar-part">
                    <div class="avatar-pic"><img src="images/avatar-icon.jpg" alt="Jack Martin"></div>
                    <div class="avatar-name">WELCOME <span>@if(Auth::check())
                    {{ Auth::user()->name }}
                    @else {{"Jack Martin"}}
                    @endif!</span></div>
                  </div>
                </div>
                <ul class="list-unstyled components">
                  <p>Dashboard</p>
                  @role('agent')
                    <li class="actived"> <a href="#"><i class="fa fa-list-ul"></i> My Customers</a></li>
                    <li class="actived"> <a href="{{ route('agent-mypolicy')}}" @if(\Request::route()->getName()=='agent-mypolicy') class="active" @endif><i class="fa fa-list-ul"></i> My Policies</a></li>
                  @endrole
                  
                  @role('user')
                    <li class="actived"> <a href="{{ route('user-mypolicy')}}" @if(\Request::route()->getName()=='user-mypolicy') class="active" @endif><i class="fa fa-list-ul"></i> My Policies</a></li>
                  @endrole

                  <li class="actived"> <a href="#"><i class="fa fa-reply"></i> My Transactions</a></li>
                  
                 <!--  <li class="actived"> <a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-list-ul"></i> My Policies</a>
                    <ul class="list-unstyled collapse" id="submenu1">
                      <li> <a href="#">Policy 1</a> </li>
                      <li> <a href="#">Policy 2</a> </li>
                      <li> <a href="#">Policy 3</a> </li>
                    </ul>
                  </li> -->
                  
                 
                  
                  @role('agent')
                    <li> <a href="{{ route('agent-myprofile')}}" @if(\Request::route()->getName()=='agent-myprofile') class="active" @endif><i class="fa fa-user"></i> My Profile</a> </li>
                    <li class="actived"> <a href="{{ route('agent-commission-report')}}"><i class="fa fa-list-ul"></i> My Commission</a></li>
                  @endrole
                  
                  
                  
                  @role('user')
                    <li> <a href="{{ route('user-myprofile')}}" @if(\Request::route()->getName()=='user-myprofile') class="active" @endif><i class="fa fa-user"></i> My Profile</a> </li>
                    <li> <a href="#"><i class="fa fa-upload"></i> Upload Documents</a> </li>
                  @endrole
                  
                  <li> <a href="#"><i class="fa fa-repeat"></i> Change Password</a> </li>
                  
                  @role(['agent','user','super-admin'])
                    <li><a href="{{url('/start')}}">Buy products</a></li>
                  @endrole
            
                  <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a> </li>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </ul>
              </nav>
            </div>
            <!-- Account Sidebar End --> 
          </div>



          @yield('account-panel')

        </div>
      </div>
    </div>
  </div>
  
  <!-- My Account Container End --> 
</section>
<!-- Main Container End --> 
@endsection