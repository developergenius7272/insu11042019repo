          <div class="main-step-form-inner">
            <div class="step-form-heading">
              <h3>My Car Details</h3>
            </div>
            <div class="step-form-part">
              <form action="step-2.html">
                <div class="form-group">
                  <select class="form-control" id="carModelYear" name="carModelYear">
                    <option value="">Car Model Year</option>
                    @for($i=$current_year; $i>= 2000; $i--)
                      <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i>
                  <div class="carModelYear_error error_div"></div>
                </div>
                <div class="form-group">
                  <select class="form-control" id="carMakes" name="carMakes">
                    <option value="">Car Make</option>
                    {{-- <option>Car Make 2</option> --}}
                    {{-- <option>2000</option>
                    <option>2001</option>
                    <option>2002</option>
                    <option>2003</option>
                    <option>2004</option>
                    <option>2005</option> --}}
                  </select>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i>
                  <div class="carMakes_error error_div"></div>
                </div>
                <div class="form-group">
                  <select class="form-control" id="carModels">
                    <option>Car Model</option>
                    {{-- <option>Maruti Suzuki</option>
                    <option>Huandai</option>
                    <option>Honda</option>
                    <option>Tata</option>
                    <option>BMW</option>
                    <option>Datsun</option> --}}
                  </select>
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i>
                  <div class="carModels_error error_div"></div>
                </div>
                <div class="form-group">
                  <select class="form-control" id="carVariants">
                    <option>Model Details</option>
                  </select>
                  <input type="hidden" id="carVariantBodyType">
                  <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i>
                  <div class="carVariants_error error_div"></div>
                </div>
                <div class="form-group">
                  <small class="form-text" id="carValueHeading">Please select a value between <span>{{ env('CURRENCY_CODE', 'AED') }} 785, 250</span> and <span>{{ env('CURRENCY_CODE', 'AED') }} 1,308,750</span></small>
                  <input type="number" class="form-control" placeholder="Car Value (AED)" id="carValueInput">
                  <div class="carValueInput_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                <div class="form-group">
                  <label>Is your car brand new?</label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="newCarYes" name="brandnewcar" class="custom-control-input">
                    <label class="custom-control-label" for="newCarYes">Yes</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="newCarNo" name="brandnewcar" class="custom-control-input">
                    <label class="custom-control-label" for="newCarNo">No</label>
                  </div>
                  <div class="brandnewcar_error error_div"></div>
                </div>
                <div class="io_divider"></div>
                {{-- <div id="newCarSection" style="display: none"> --}}
                <div id="newCarSection">
                  <div class="form-group">
                    <label>When was your car first registered?</label>
                    <small class="form-text">(Find this on your car registration card)</small>
                      <input type="text" class="form-control calendar-icon" placeholder="dd/mm/yyyy" id="datepicker">
                    <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i>
                    <div class="datepicker_error error_div"></div>
                  </div>

                  <div class="form-group">
                    <label>City of Registration?</label>
                    {{-- <input type="text" class="form-control" placeholder="" id="city_of_registration" name="city_of_registration"> --}}
                    <i class="info-btn" data-toggle="tooltip" data-placement="right" title="Tooltip content here."></i> 

                    <select class="form-control" id="city_of_registration" name="city_of_registration">
                        <option value="">Select City</option>
                        <option value="Abu Dhabi">Abu Dhabi</option>
                        <option value="Dubai">Dubai</option>
                        <option value="Sharjah">Sharjah</option>
                        <option value="Ajman">Ajman</option>
                        <option value="Fujairah">Fujairah</option>
                        <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                        <option value="Umm Al Quwain">Umm Al Quwain</option>
                    </select>
                    <div class="city_of_registration_error error_div"></div>
                  </div>
                </div>
                <div class="btns-part">
                  <button type="button" class="btn btn-outline-light" id="btnHideStep3">Back</button>
                  <button type="button" class="btn btn-secondary" id="saveCarDetailLead">Next</button>
                </div>
              </form>
            </div>
          </div>

<script>

  $( function() {
        $('#datepicker').datetimepicker({
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
  } );
</script>