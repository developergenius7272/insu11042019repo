@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('css')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection
@section('content')
<div class="row">
    {{-- <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i> 
                Table 
            <span>> 
                Normal Tables
            </span>
        </h1>
    </div> --}}
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Benefit Plan</h2>

                    </header>

                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div style="text-align: center;">
                        @if (\Session::has('success'))
                          <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                          </div><br />
                         @endif
                     </div>
                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
    
                            <form class="form-horizontal" method="POST" @if($formType == 'edit') action="" @else action="{{url('/admin/planbenefits')}}" @endif>
                                 @csrf
                                 @if($formType == 'edit')
                                    <input name="_method" type="hidden" value="PATCH">
                                @endif
                                {{--  --}}
                                <fieldset>
                                    <legend>Benefit Plan</legend>

                                    <div class="form-group" id="carBodyTypeId">
                                        <label class="control-label col-md-2">Select Partners</label>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <select class="form-control" id="insurance_providers_id" name="insurance_providers_id">
                                                        <option value="">Select Partner</option>
                                                        @if(isset($partners) && count($partners))
                                                            {{-- @foreach($partners as $partner)
                                                                <option value="{{$partner->id}}" @if(isset($data) && $data->insurance_provider_id == $partner->id) selected @endif>{{strtoupper($partner->name)}}</option>
                                                            @endforeach --}}

                                                            @foreach($partners as $partner)
                                                                <option value="{{$partner->id}}" 
                                                                    @if(old('insurance_providers_id') == $partner->id) 
                                                                        {{ 'selected' }} 
                                                                    @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                                        {{ 'selected' }} 
                                                                    @endif
                                                                    >{{strtoupper($partner->name)}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-2">Years</label>
                                          <div class="col-md-4">
                                                <select class="form-control" id="year" name="year">
                                                    @for($i=($current_year + 1 ); $i >= 2000; $i--)

                                                        {{-- <option 
                                                            @if($i == $current_year) 
                                                                selected 
                                                            @else
                                                                {{$i}}
                                                            @endif
                                                            >{{$i}}
                                                        </option> --}}

                                                        <option 
                                                            @if(old('year') == $i) 
                                                                {{ 'selected' }} 
                                                            @elseif($i == $current_year) 
                                                                {{ 'selected' }} 
                                                            @else
                                                                {{$i}}
                                                            @endif
                                                            >{{$i}}
                                                        </option>
                                                    @endfor
                                                </select>
                                          </div> 
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Effective Date</label>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <input type="text" name="effective_date" value="{{ $errors->any() ? old('effective_date') : '' }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Comprehensive plan / TPL plan</label>
                                        <div class="col-md-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="plan_type" 
                                                    @if(old('plan_type') == 0)
                                                        {{ 'checked' }}
                                                    @endif
                                                    value="0">Comprehensive plan
                                                </label>
                                                <label>
                                                    <input type="radio" name="plan_type" 
                                                    @if(old('plan_type') == 1)
                                                        {{ 'checked' }}
                                                    @endif
                                                    value="1">TPL plan
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Standard / Superior / Not Applicable</label>
                                        <div class="col-md-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sub_type" 
                                                            @if(old('sub_type') == 0) 
                                                                {{ 'checked' }} 
                                                            @elseif(isset($data->sub_type) && $data->sub_type == 0)
                                                                {{ 'checked' }} 
                                                            @elseif($formType == 'add') 
                                                                {{ 'checked' }}
                                                            @endif
                                                    value="0">Not Applicable
                                                </label>
                                                <label>
                                                    <input type="radio" name="sub_type" 
                                                            @if(old('sub_type') == 1) 
                                                                {{ 'checked' }} 
                                                            @elseif(isset($data->sub_type) && $data->sub_type == 1)
                                                                {{ 'checked' }}                               
                                                            @endif
                                                    value="1">Standard
                                                </label>
                                                <label>
                                                    <input type="radio" name="sub_type" 
                                                            @if(old('sub_type') == 2) 
                                                                {{ 'checked' }} 
                                                            @elseif(isset($data->sub_type) && $data->sub_type == 2)
                                                                {{ 'checked' }} 
                                                            @endif
                                                    value="2">Superior
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Status</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" 
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                            value="0">Inactive
                                                        </option>
                                                        <option  class="form-control" 
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                            value="1">Active
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                {{--  --}}

                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-md-3"><label class="control-label">Benefit Name</label></div>      
                                        <div class="col-md-2"><label class="control-label">Include in plan</label></div> 
                                        <div class="col-md-3 divAmountHeading"><label class="control-label">Amount</label></div>  
                                    </div>

                                <hr>
                                    @if($benefitMasters)
                                        @foreach($benefitMasters as $benefitMaster)

                                    <div class="row row_{{ $benefitMaster->id }}">
                                        <input type="hidden" value="" name="benefit_id_{{ $benefitMaster->id }}" class="benefit_id_{{ $benefitMaster->id }}">

                                        <div class="col-md-3">
                                            {{strtoupper($benefitMaster->name)}}
                                        </div>      
                                        <div class="col-md-2">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="included_{{ $benefitMaster->id }}" class="included" 
                                                        @if(old("included_".$benefitMaster->id) === '0')
                                                            {{ 'checked' }}
                                                        @endif
                                                    value="0">Not Included
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="included_{{ $benefitMaster->id }}" class="included" 
                                                        @if(old("included_".$benefitMaster->id) === '1')
                                                            {{ 'checked' }}
                                                        @endif
                                                    value="1">Included
                                                </label>
                                            </div>
                                        </div> 

                                        {{-- <div class="col-md-2 divPerAmount_{{ $benefitMaster->id }}" style="display:none;"> --}}
                                        <div class="col-md-2 divPerAmount_{{ $benefitMaster->id }}" 
                                            @if(old('included_'.$benefitMaster->id) === '0')
                                                {{ 'style=display:block' }}
                                            @else
                                                {{ 'style=display:none' }}
                                            @endif
                                        >
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="perAmount_{{ $benefitMaster->id }}" class="perAmount" 
                                                        @if(old("perAmount_".$benefitMaster->id) === '1')
                                                            {{ 'checked' }}
                                                        @endif
                                                    value="1">Percentage
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="perAmount_{{ $benefitMaster->id }}" class="perAmount" 
                                                        @if(old("perAmount_".$benefitMaster->id) === '2')
                                                            {{ 'checked' }}
                                                        @endif
                                                    value="2">Amount
                                                </label>
                                            </div>
                                        </div> 

                                        <div class="col-md-2 divAmount_{{ $benefitMaster->id }}" 
                                            @if(old('included_'.$benefitMaster->id) === '0')
                                                {{ 'style=display:block' }}
                                            @else
                                                {{ 'style=display:none' }}
                                            @endif
                                        >
                                            <input class="form-control amount_{{ $benefitMaster->id }}" placeholder="amount" type="number" step="any" name="amount_{{ $benefitMaster->id }}" value="{{old("amount_".$benefitMaster->id,'')}}">
                                        </div> 
                                    </div>
                                    <hr>

                                      @endforeach
                                    @endif

                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{url('/admin/partners/benefit-master')}}" class="btn btn-default">Cancel</a>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
    
                            </form>
    
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            
        </div>

    </section>
</div>
@endsection



@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $('#datetimepicker').datetimepicker({
            //dateFormat: 'dd/MM/yyyy hh:mm:ss',
            format:'d/m/Y H:i',
            //format:'d/m/Y',
        });

        //$(".included").change(function(){
        $('body').on('change','.included',function(e){

            var includedName = $(this).attr('name');

            var arr = includedName.split('_');
            var benefitId = arr[1];
            $('.benefit_id_'+benefitId).val(benefitId);

            var includedVal = $(this).val();
            if(includedVal == 0){
                
                $('.amount_'+benefitId).val('');
                $('.divAmountHeading').show();
                $('.divAmount_'+benefitId).show();
                $('.divPerAmount_'+benefitId).show();
            }
            else{
                $('.perAmount_'+benefitId).prop("checked",false); //not working
                $('.divPerAmount_'+benefitId).hide();

                $('.amount_'+benefitId).val('');
                $('.divAmount_'+benefitId).hide();   
            }
        });
        
    });


</script>
@endsection
