<?php

namespace App\Http\Controllers;
use App\Models\InsuranceProvider;
use App\Models\UserAgentCommission;
use App\Models\UserAgentAddonCommission;
use App\Models\Role;
use App\Models\AgentUser;
use App\Models\CrossSellingProducts;
use App\User;

// 
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Auth;
use Log;
use Mail;

class AdminUserController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:super-admin|agent-create',['only' => ['createAgent']]);
        $this->middleware('permission:super-admin|agent-update',['only' => ['updateAgent']]);
        $this->data['breadcrumbs'][] = 'Users'; 
        $this->data['pageTitle'] = 'Users';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Users';
        $this->data['users'] = User::withRole('user')->get();
        return view('admin.users.index', $this->data);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType']  = 'add';
        $this->data['formTitle'] = 'Add a new user';
        $this->data['agents']    = User::active()->orderBy('name')->withRole('agent')->get();
        // 
        return view('admin.users.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();
        // 
        $name    = $request->name;
        $email   = $request->email;
        $status  = $request->status;
        $dob     = $request->dob;
        $agentId = $request->agentId;

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        $request->merge(["password" => bcrypt(uniqid())]);
        // 
        request()->validate([
            'name' => 'required|min:2|max:100',
            'email' => 'required|unique:users|min:2|max:100',
            "agentId" => "required"
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique' => "The name $email has already been taken.",
            ]
        );
        // 
        if( $user = User::create($request->all()) ) {
            $user->status      = $status;
            $user->dob         = $dob;
            $user->created_by  = $adminID;
            $user->modified_by = $adminID;
            $user->save();
            // 
            $role = Role::where('name', 'user')->first();
            $user->attachRole($role);
            // User::sendWelcomeEmail($user);
            // upload image
            $imageName = '';
            try{
                if ($request->hasFile('image')) { 
                    
                    $imageName = $user->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $user->id.'_main';
                    $request->image->move(public_path('uploads/users'), $imageName);
                    // resize for logo
                    $imageLogo = $user->id.'_logo';
                    $img = Image::make(public_path('uploads/users/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/users/' . $imageLogo));

                    $user->has_logo = 1;
                    $user->save();
                }
            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }
            try{
                // 
                
                $agentuser                   = new AgentUser();
                $agentuser->agent_id         = $agentId;
                $agentuser->user_id          = $user->id;
                $agentuser->is_current_agent = 1;
                $agentuser->save();
            }catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }

            return redirect('/admin/users')->with('success', 'User added successfully');
        } else {
            return redirect('/admin/users')->with('success', 'User not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit user';

        $user   = User::where('id',$id)->roleUser()->first();
        $agents = User::active()->orderBy('name')->withRole('agent')->get();
        //$agentusers = AgentUser::where('user_id',$id)->orderBy('id','DESC')->first();
        //$agentusers = AgentUser::where('user_id',$id)->where('is_current_agent',1)->first();
        $agentusers = AgentUser::where('user_id',$id)->active()->first();

        if( $user ) {
            $this->data['data']       = $user;
            $this->data['agents']     = $agents;
            $this->data['agentusers'] = $agentusers;
            return view('admin.users.create_edit',$this->data);
        } else {
            return redirect('/admin/users')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name                 = $request->name;
        $email                = $request->email;
        $mobile               = $request->mobile;
        $gender               = $request->gender;
        $address              = $request->address;
        $maritalstatus        = $request->maritalstatus;
        $dob                  = $request->dob;
        $passport_number      = $request->passport_number;
        $passport_expiry_date = $request->passport_expiry_date;
        $license_number       = $request->license_number;
        $license_expiry_date  = $request->license_expiry_date;
        $uae_id               = $request->uae_id;
        $uae_id_expiry_date   = $request->uae_id_expiry_date;
        $status               = $request->status;
        $agentId              = $request->agentId;
        $oldAgentId           = $request->oldAgentId;

        // if( !empty($dob) ) {
        //     // date format should be in YMD format
        //     $dob = sqlDateDMY($dob,'-');
        // }

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        request()->validate([
            "name" => "required|min:2|max:100",
            "email" => "required|unique:users,email,$id",
            "agentId" => "required"
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique' => "The name $email has already been taken.",
            ]
        );

        $body = User::find($id);
        // 
        if( $body ) {

            $body->name                 = $name;
            $body->email                = $email;
            $body->mobile               = $mobile;
            $body->gender               = $gender;
            $body->address              = $address;
            $body->maritalstatus        = $maritalstatus;
            $body->dob                  = $dob;
            $body->passport_number      = $passport_number;
            $body->passport_expiry_date = $passport_expiry_date;
            $body->license_number       = $license_number;
            $body->license_expiry_date  = $license_expiry_date;
            $body->uae_id               = $uae_id;
            $body->uae_id_expiry_date   = $uae_id_expiry_date;
            $body->status               = $status;
            $body->modified_by          = $adminID;

            if ($request->has_logo_image == '0' ){
                $body->has_logo = 0;
            }
            // 
            $imageName = '';
            try{
                if ($request->hasFile('image')) { 
                    // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $body->id.'_main';
                    $request->image->move(public_path('uploads/users'), $imageName);
                    // resize for logo
                    $imageLogo = $body->id.'_logo';
                    $img = Image::make(public_path('uploads/users/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/users/' . $imageLogo));

                    $body->has_logo = 1;
                }

            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }

            $body->save();

            //agent user attachement
            if($agentId !== $oldAgentId){
                //1. all agent should be updated 0
                $agentusers = AgentUser::where('user_id',$id)
                            ->where('is_current_agent' , 1)
                            ->update(['is_current_agent' => 0]);

                //2. new agent user combination
                $agentuser                   = new AgentUser();
                $agentuser->agent_id         = $agentId;
                $agentuser->user_id          = $id;
                $agentuser->is_current_agent = 1;
                $agentuser->save();
            }

            return redirect('/admin/users')->with('success', 'User updated successfully');
        } else {
            return redirect('/admin/users')->with('error', 'User not updated!');
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if( $user ) {
            // find child data if any
            // $carModelsCount = \App\Models\CarModel::where('car_make_id', $carMake->id)->count();
            // if( $carModelsCount ) {
            //     return redirect('/admin/cars/car-makes')->with('success','Information has not been deleted');
            // } else {
            //     $carMake->delete();
            //     return redirect('/admin/cars/car-makes')->with('success','Information has been deleted');
            // }
        } else {
            return redirect('/admin/users')->with('success','No data found');
        }
    }

     /* 
        Agents section
    */
    public function agentsList() {
        $this->data['breadcrumbs'][] = 'Agents'; 
        $this->data['pageTitle'] = 'Agents';
        // 
        $this->data['tableHeading'] = 'Agents';
        $this->data['users'] = User::withRole('agent')->get();
        return view('admin.agents.index', $this->data);        
    }
    
    public function createAgent() {
        $this->data['breadcrumbs'][] = 'Agents'; 
        $this->data['pageTitle'] = 'Agents';
        // 
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a new agent';

        $partners = InsuranceProvider::all();
        $products = CrossSellingProducts::all();



        $this->data['partners'] = $partners;
        $this->data['products'] = $products;

        return view('admin.agents.create_edit', $this->data);
    }

    public function addAgent(Request $request) {
        $adminID = Auth::id();

        $name   = $request->name;
        $email  = $request->email;
        $status = $request->status;
        $dob    = $request->dob;
        // $base_commission_percentage = $request->base_commission_percentage;
        // $base_commission_amount = $request->base_commission_amount;

        $insuranceProviderId = $request->insuranceProviderId;
        $perAmount           = $request->perAmount;
        $amount              = $request->amount;

        // $addonInsuranceProviderId = $request->addonInsuranceProviderId;
        // $addonAmount           = $request->addonAmount;
        // $amount_addon              = $request->amount_addon;

        //$addonInsuranceProviderId = $request->addonInsuranceProviderId;
        $cross_selling_product_id = $request->cross_selling_product_id;
        $percentage_or_amount = $request->percentage_or_amount;
        $amount_addon = $request->amount_addon;

        //echo "<pre>";print_r($amount_addon);die;


        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        $request->merge(["password" => bcrypt(uniqid())]);
        // 
        request()->validate([
            'name' => 'required|min:2|max:100',
            'email' => 'required|unique:users|min:2|max:100'
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique' => "The name $email has already been taken.",
            ]
        );

        //4. Base Commission checking
        $co = count($insuranceProviderId);

        //duplicate checking
        if(count(array_unique($insuranceProviderId))<$co)
        {
            // Array has duplicates
            $this->validate($request, [
                'insuranceProviderId_duplicate' => 'required',
            ],[
                'insuranceProviderId_duplicate.required' => "You cannot select same provider twice",
            ]);
        }

        for ($k=0; $k < $co; $k++) {
            
            if($insuranceProviderId[$k] > 0){

                if(!empty($amount[$k])){
                    
                    //if(empty($perAmount[$k]) || $perAmount[$k] == 0){
                    if(empty($perAmount[$k])){

                        $this->validate($request, [
                            'perAmount_not_blank' => 'required',
                        ],[
                            'perAmount_not_blank.required' => "Please select Percentage or Amount on ".($k +1),
                        ]);

                    }
                    
                }
                //5.Condition to check -> if perAmount fill and amount blank
                // if(empty($amount[$k])){

                //     if($perAmount[$k] >= 0){

                //         $this->validate($request, [
                //             'amount_not_blank' => 'required',
                //         ],[
                //             'amount_not_blank.required' => "Please fill amount if Percentage or Amount selected on ".($k +1),
                //         ]);

                //     }
                // }
            }
        }

        /*
        //5. Addon Base Commission checking
        $co = count($addonInsuranceProviderId);

        //duplicate checking
        if(count(array_unique($addonInsuranceProviderId))<$co)
        {
            // Array has duplicates
            $this->validate($request, [
                'addonInsuranceProviderId_duplicate' => 'required',
            ],[
                'addonInsuranceProviderId_duplicate.required' => "You cannot select same provider twice on Addon",
            ]);
        }

        for ($k=0; $k < $co; $k++) {
            
            if($addonInsuranceProviderId[$k] > 0){

                if(!empty($amount_addon[$k])){
                    
                    //if(empty($addonAmount[$k]) || $addonAmount[$k] == 0){
                    if(empty($addonAmount[$k])){

                        $this->validate($request, [
                            'addonAmount_not_blank' => 'required',
                        ],[
                            'addonAmount_not_blank.required' => "Please select Addon Percentage or Amount on ".($k +1),
                        ]);

                    }
                    
                }
                //5.Condition to check -> if addonAmount fill and amount blank
                // if(empty($amount_addon[$k])){

                //     if($addonAmount[$k] >= 0){

                //         $this->validate($request, [
                //             'amount_addon_not_blank' => 'required',
                //         ],[
                //             'amount_addon_not_blank.required' => "Please fill Addon amount if Percentage or Amount selected on ".($k +1),
                //         ]);

                //     }
                // }
            }
        }
        */

        //Insertion
        //Insertion
        //Insertion
        if( $agent = User::create($request->all())) {
            // $agent->fill(['user_role', 1]);
            
            $agent->user_role = 1;
            $agent->status = $status;
            $agent->dob = $dob;
            // $agent->base_commission_percentage = $base_commission_percentage;
            // $agent->base_commission_amount = $base_commission_amount;
            $agent->created_by = $adminID;
            $agent->modified_by = $adminID;

            $agent->save();
            $agent_id = $agent->id;
            $role = Role::where('name', 'agent')->first();
            $agent->attachRole($role);
            User::sendWelcomeEmail($agent);

            $co = count($insuranceProviderId);
            for ($k=0; $k < $co; $k++) {
                
                if($insuranceProviderId[$k] > 0){

                    $userAgentCommissionSave = new UserAgentCommission();
                    $userAgentCommissionSave->user_id = $agent_id;
                    $userAgentCommissionSave->insurance_provider_id = $insuranceProviderId[$k];
                    $userAgentCommissionSave->percentage_or_amount = $perAmount[$k];
                    $userAgentCommissionSave->amount = $amount[$k];
                    $userAgentCommissionSave->save();
                }

            }

            $co = 0;
            $co = count($cross_selling_product_id);
            for ($k=0; $k < $co; $k++) {
                
                if($cross_selling_product_id[$k] > 0){

                    $userAgentAddonCommissionSave = new UserAgentAddonCommission();
                    $userAgentAddonCommissionSave->user_id = $agent_id;
                    $userAgentAddonCommissionSave->cross_selling_product_id = $cross_selling_product_id[$k];
                    //$userAgentAddonCommissionSave->insurance_provider_id = $addonInsuranceProviderId[$k];
                    $userAgentAddonCommissionSave->percentage_or_amount = $percentage_or_amount[$k];
                    $userAgentAddonCommissionSave->amount = $amount_addon[$k];
                    $userAgentAddonCommissionSave->save();
                }

            }
                // upload image
                $imageName = '';
                try{
                    if ($request->hasFile('image')) { 
                        
                        // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();
                        
                        $imageName = $agent->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $agent->id.'_main';
                        $request->image->move(public_path('uploads/agents'), $imageName);
                        // resize for logo
                        $imageLogo = $agent->id.'_logo';
                        $img = Image::make(public_path('uploads/agents/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/agents/' . $imageLogo));

                        $agent->has_logo = 1;
                        $agent->save();

                    }
                }
                catch(\Exception $e){
                    // do task when error
                    // echo $e->getMessage();
                }

            return redirect()->route('admin.agents')->with('success', 'Agent added successfully');
        } else {
            return redirect()->route('admin.agents')->with('error', 'Agent not added');
        }
    }

    function editAgent($id)
    {
        $this->data['breadcrumbs'][] = 'Agents'; 
        $this->data['pageTitle'] = 'Agents';
        // 
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit agent';

        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;

        $user = User::where('id',$id)->roleAgent()->first();
        if( $user ) {

            $userAgentCommissions = UserAgentCommission::select('user_agent_commissions.*')
            ->leftJoin('users','users.id', 'user_agent_commissions.user_id')
            ->where('user_agent_commissions.user_id',$user->id)
            ->orderBy('user_agent_commissions.id', 'ASC')
            ->get();

            $this->data['userAgentCommissions'] = $userAgentCommissions;
            $this->data['countUserAgentCommissions'] = count($userAgentCommissions);
            // 

            $userAgentAddonCommissions = UserAgentAddonCommission::select('user_agent_addon_commissions.*','cross_selling_products.name as cross_selling_product_name', 'insurance_providers.name as insurance_providers_name')
            ->leftJoin('users','users.id', 'user_agent_addon_commissions.user_id')
            ->leftJoin('cross_selling_products','cross_selling_products.id', 'user_agent_addon_commissions.cross_selling_product_id')
            ->leftJoin('insurance_providers','insurance_providers.id', 'cross_selling_products.insurance_provider_id')
            ->where('user_agent_addon_commissions.user_id',$user->id)
            ->orderBy('user_agent_addon_commissions.id', 'ASC')
            ->get();
            // dd($userAgentAddonCommissions);
            $this->data['userAgentAddonCommissions'] = $userAgentAddonCommissions;
            $this->data['countUserAgentAddonCommissions'] = count($userAgentAddonCommissions);


            $this->data['data'] = $user;
            return view('admin.agents.create_edit',$this->data);
        } else {
            return redirect('/admin/agents')->with('error', 'No data found');
        }
    }

    public function updateAgent(Request $request, $id)
    {
        $adminID = Auth::id();

        $name                          = $request->name;
        $email                         = $request->email;
        $mobile                        = $request->mobile;
        $gender                        = $request->gender;
        $address                       = $request->address;
        $maritalstatus                 = $request->maritalstatus;
        $dob                           = $request->dob;
        // $base_commission_percentage = $request->base_commission_percentage;
        // $base_commission_amount     = $request->base_commission_amount;
        
        $passport_number               = $request->passport_number;
        $passport_expiry_date          = $request->passport_expiry_date;
        $license_number                = $request->license_number;
        $license_expiry_date           = $request->license_expiry_date;
        $uae_id                        = $request->uae_id;
        $uae_id_expiry_date            = $request->uae_id_expiry_date;
        
        $status                        = $request->status;
        
        $insuranceProviderId           = $request->insuranceProviderId;
        $perAmount                     = $request->perAmount;
        $amount                        = $request->amount;

        // $addonInsuranceProviderId      = $request->addonInsuranceProviderId;
        // $addonAmount                   = $request->addonAmount;
        // $amount_addon                  = $request->amount_addon;

        //$addonInsuranceProviderId = $request->addonInsuranceProviderId;
        $cross_selling_product_id = $request->cross_selling_product_id;
        $percentage_or_amount = $request->percentage_or_amount;
        $amount_addon = $request->amount_addon;

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }
        
        request()->validate([
            "name" => "required|min:2|max:100",
            "email" => "required|unique:users,email,$id"
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique' => "The name $email has already been taken.",
            ]
        );

        /*
        // Other Conditions
        //2.
        if(empty($base_commission_percentage)){
            $base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($base_commission_percentage)){
                $this->validate($request, [
                    'base_commission_percentage_numeric' => 'required',
                ],[
                    'base_commission_percentage_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
            else if($base_commission_percentage > 100){

                $this->validate($request, [
                    'base_commission_percentage_greater' => 'required',
                ],[
                    'base_commission_percentage_greater.required' => "Please enter Base Commission's percentage not more than 100",
                ]);
            }
        }

        //3.
        if(empty($base_commission_amount)){
            $base_commission_amount = 0;
        }
        else{
            if(!is_numeric($base_commission_amount)){
                $this->validate($request, [
                    'base_commission_amount_numeric' => 'required',
                ],[
                    'base_commission_amount_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
        }
        */
        
        //4. Base Commission checking
        $co = count($insuranceProviderId);

        //duplicate checking
        if(count(array_unique($insuranceProviderId))<$co)
        {
            // Array has duplicates
            $this->validate($request, [
                'insuranceProviderId_duplicate' => 'required',
            ],[
                'insuranceProviderId_duplicate.required' => "You cannot select same provider twice",
            ]);
        }
        
        for ($k=0; $k < $co; $k++) {
            
            if($insuranceProviderId[$k] > 0){

                if(!empty($amount[$k])){
                    
                    //if(empty($perAmount[$k]) || $perAmount[$k] == 0){
                    if(empty($perAmount[$k])){

                        $this->validate($request, [
                            'perAmount_not_blank' => 'required',
                        ],[
                            'perAmount_not_blank.required' => "Please select Percentage or Amount on ".($k +1),
                        ]);

                    }
                    
                }
                //5.Condition to check -> if perAmount fill and amount blank
                // if(empty($amount[$k])){

                //     if($perAmount[$k] > 0 || $perAmount[$k] == 0){

                //         $this->validate($request, [
                //             'amount_not_blank' => 'required',
                //         ],[
                //             'amount_not_blank.required' => "Please fill amount if Percentage or Amount selected on ".($k +1),
                //         ]);

                //     }
                // }
            }
        }

        /*
        //5. Addon Base Commission checking
        $co = count($addonInsuranceProviderId);

        //duplicate checking
        if(count(array_unique($addonInsuranceProviderId))<$co)
        {
            // Array has duplicates
            $this->validate($request, [
                'addonInsuranceProviderId_duplicate' => 'required',
            ],[
                'addonInsuranceProviderId_duplicate.required' => "You cannot select same provider twice on Addon",
            ]);
        }
        
        for ($k=0; $k < $co; $k++) {
            
            if($addonInsuranceProviderId[$k] > 0){

                if(!empty($amount_addon[$k])){
                    
                    //if(empty($addonAmount[$k]) || $addonAmount[$k] == 0){
                    if(empty($addonAmount[$k])){

                        $this->validate($request, [
                            'addonAmount_not_blank' => 'required',
                        ],[
                            'addonAmount_not_blank.required' => "Please select Addon Percentage or Amount on ".($k +1),
                        ]);

                    }
                    
                }
                //5.Condition to check -> if addonAmount fill and amount blank
                // if(empty($amount_addon[$k])){

                //     if($addonAmount[$k] >= 0){

                //         $this->validate($request, [
                //             'amount_addon_not_blank' => 'required',
                //         ],[
                //             'amount_addon_not_blank.required' => "Please fill Addon amount if Percentage or Amount selected on ".($k +1),
                //         ]);

                //     }
                // }
            }
        }
        */

        
        // 
        $body = User::find($id);
        // 
        if( $body ) {
            $body->name = $name;
            $body->email = $email;
            $body->mobile = $mobile;
            $body->gender = $gender;
            $body->address = $address;
            $body->maritalstatus = $maritalstatus;
            $body->dob = $dob;
            // $body->base_commission_percentage = $base_commission_percentage;
            // $body->base_commission_amount = $base_commission_amount;
            $body->passport_number = $passport_number;
            $body->passport_expiry_date = $passport_expiry_date;
            $body->license_number = $license_number;
            $body->license_expiry_date = $license_expiry_date;
            $body->uae_id = $uae_id;
            $body->uae_id_expiry_date = $uae_id_expiry_date;
            $body->status = $status;
            $body->modified_by = $adminID;

            if ($request->has_logo_image == '0' ){

                $body->has_logo = 0;
            }


            $imageName = '';
            try{
                if ($request->hasFile('image')) { 
                    // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $body->id.'_main';
                    $request->image->move(public_path('uploads/agents'), $imageName);
                    // resize for logo
                    $imageLogo = $body->id.'_logo';
                    $img = Image::make(public_path('uploads/agents/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/agents/' . $imageLogo));

                    $body->has_logo = 1;
                }

            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }

            $body->save();
            $agent_id = $body->id;

            UserAgentCommission::where('user_id',$agent_id)->delete();
            $co = count($insuranceProviderId);
            for ($k=0; $k < $co; $k++) {

                if($insuranceProviderId[$k] > 0){
                
                    $userAgentCommissionSave = new UserAgentCommission();
                    $userAgentCommissionSave->user_id = $agent_id;
                    $userAgentCommissionSave->insurance_provider_id = $insuranceProviderId[$k];
                    $userAgentCommissionSave->percentage_or_amount = $perAmount[$k];
                    $userAgentCommissionSave->amount = $amount[$k];
                    $userAgentCommissionSave->save();

                }

            }

            UserAgentAddonCommission::where('user_id',$agent_id)->delete();
            $co = 0;
            $co = count($cross_selling_product_id);
            for ($k=0; $k < $co; $k++) {
                
                if($cross_selling_product_id[$k] > 0){

                    $userAgentAddonCommissionSave = new UserAgentAddonCommission();
                    $userAgentAddonCommissionSave->user_id = $agent_id;
                    $userAgentAddonCommissionSave->cross_selling_product_id = $cross_selling_product_id[$k];
                    $userAgentAddonCommissionSave->percentage_or_amount = $percentage_or_amount[$k];
                    $userAgentAddonCommissionSave->amount = $amount_addon[$k];
                    $userAgentAddonCommissionSave->save();
                }

            }

            return redirect('/admin/agents')->with('success', 'Agent updated successfully');
        } else {
            return redirect('/admin/agents')->with('error', 'Agent not updated');
        }
    }

    public function showAgentsUser($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Update partner';
        $this->data['tableHeading'] = 'Agents User';
        // 
        $agent = User::where('id',$id)->withRole('agent')->first();
        // 
        if( $agent ) {
            $this->data['agent'] = $agent;
            return view('admin.agents.show',$this->data);
        } else {
            return redirect()->route('admin.agents')->with('error', 'No data found');
        }
    }

    public function passwordSent(Request $request) {

        $response = array();
        
        $user_id = $request->user_id;

        $user = User::find($user_id);

        if(isset($user_id) || !empty($user_id)){
            if( $user ) {
                //reset_password_token generate - start
                $token = \Customfunction::uniqueStringByTableField();
                $reset_password_date = Carbon::now();
                $user->reset_password_token = $token;
                $user->reset_password_date = $reset_password_date;
                $user->save();
                //reset_password_token generate - end 

                /*
                // send email - user verification
                $info = array(
                    "code"=>"user_verification",
                    "user_id"=>$user_id,
                    "to_name" => "",
                    "to_email" => $user->email,
                    "replace_values" => array(
                        "-textPara-"=>"Let's confirm your email address.",
                        //"-code-"=>$confirmation_code,
                        "-code-"=>$token,
                        "-codeType-"=>"CONFIRM EMAIL ADDRESS")
                );
                $this->emailSend($info);
                */
                
                
                // send email - user verification
                $data = array(
                    'emailheading' => 'Reset Password',
                    'email' => $user->email,
                    'name' => $user->name,
                    //'authcode' => $random_authcode,
                );

                Mail::send('emails.users.reset_password', $data, function ($m) use ($user) {
                 $m->to($user->email, $user->name)->subject('Reset Password');
                });

                $response['status'] = 1;

            }
            else{
                $response['status'] = 0;
            }
        }
        else{
            $response['status'] = 0;
        }

        echo json_encode($response) ;

    }

    //
    //*** Email Sender function for all emails ***
    //
    //  info array will contain email code to use with params
    //
    private function emailSend($info)
    {
        $status = false;
        if (is_array($info)) {
            if (isset($info["code"])) {
                        
                    // from name
                    $mail_test_active_send = 1;
                    if (isset($info["mail_test_active_send"])) {
                        $mail_test_active_send = $info['mail_test_active_send'];
                    } else {
                        $mail_test_active_send = 1;
                    }
                    // get user id iinternal
                    $user_id = null;
                    if (isset($info["user_id"])) {
                        $user_id = $info["user_id"];
                    }
                    // from name
                    $from_name = '';
                    if (isset($info["from_name"])) {
                        $from_name = $info['from_name'];
                    } else {
                        $from_name = 'InsureOnline';
                    }
                    // from email
                    $from_email = '';
                    if (isset($info["from_email"])) {
                        $from_email = $info['from_email'];
                    } else {
                        $from_email = 'info@baltech.in';
                    }
                    // to name
                    $to_name = '';
                    if (isset($info["to_name"])) {
                        $to_name = $info['to_name'];
                    } else {
                        $to_name = 'InsureOnline';
                    }
                    // to email
                    $to_email = '';
                    if (isset($info["to_email"])) {
                        $to_email = $info['to_email'];
                    } else {
                        $to_email = 'info@baltech.in';
                    }
                    // get subject
                    $subject = '';
                    if (isset($info["subject"])) {
                        $subject = $info['subject'];
                    } else {
                        $subject = 'Lets confirm your email address.';
                    }

                    if ($from_name == '') {
                        $from_name = env('MAIL_FROM_NAME', 'InsureOnline');
                    }
                    if ($from_email == '') {
                        $from_email = env('MAIL_FROM_EMAIL', 'info@baltech.in');
                    }
                    if ($to_email == '') {
                        $to_email = env("MAIL_ADMIN_EMAIL", "info@baltech.in");
                    }
                    if ($to_name == '') {
                        $to_name = null;
                    }

                    // create and send
                    $from = new \SendGrid\Email($from_name, $from_email);
                    $to = new \SendGrid\Email($to_name, $to_email);

                    $content = new \SendGrid\Content("text/html", " ");
                    $mail = new \SendGrid\Mail($from, $subject, $to, $content);

                    // reply to code
                    if (isset($info["reply_to"])) {
                        $reply_to = new \SendGrid\ReplyTo($info["reply_to"]);
                        $mail->setReplyTo($reply_to);
                    } else {
                        if ($from_email <> '') {
                            $reply_to = new \SendGrid\ReplyTo($from_email);
                            $mail->setReplyTo($reply_to);
                        }
                    }

                    // **************** current way!!!!!
                    //
                    if (isset($info["replace_values"])) {
                        $replace_array = $info["replace_values"];
                        foreach ($replace_array as $key => $value) {
                            if (($value == null) or ($value == '')) {
                                $value = ' ';
                            }
                            $value = (string)$value;
                            Log::info("  Email Send Keys: code : ".$info['code']." ".env('TEMPLATE_CODE', 'd-1b6f0be2202947a3b33e5f38d8d0b944')." : key : ".$key." value : ".$value);

                            $mail->personalization[0]->addSubstitution($key, $value);
                        }
                    }
                    if (isset($info["section_values"])) {
                        $section_array = $info["section_values"];
                        foreach ($section_array as $key => $value) {
                            if (($value == null) or ($value == '')) {
                                $value = ' ';
                            }
                            $value = (string)$value;
                            $mail->addSection($key, $value);
                        }
                    }
                    
                    $mail->setTemplateId(env('TEMPLATE_CODE', 'd-1b6f0be2202947a3b33e5f38d8d0b944'));
                    $apiKey = env('SENDGRID_APIKEY', false);
                    $sg = new \SendGrid($apiKey);
                    
                    try {
                        if ($mail_test_active_send == 0) {
                            // do not send this email via sendgrid but act like it did
                            //
                        } else {
                            $response = $sg->client->mail()->send()->post($mail);
                            //\Log::info("Email Send Response: email log id : ".$email_log_id." : code : ".$response->statusCode());
                            if ($response->statusCode() == 400) {
                                \Log::info("    Response: email log id : code : " . print_r($response, true));
                            }
                        }
                    } catch (Exception $e) {
                        \Log::info("Email Send Error: " . $e->getMessage());
                    }
                
            }
        }
        return $status;
    }

}
