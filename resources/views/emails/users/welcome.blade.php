@extends('layouts.master_email')
@section('content')
<h2 style="font-size:32px; color:#25aae1; margin:0 0 25px; padding:0; font-weight:bold;">Welcome to insureonline.</h2>
<h4 style="font-size:16px; color:#494949; margin:0 0 15px; padding:0; font-weight:bold;">Hello {{ $user->name }},</h4>
<p style="font-size:15px; color:#494949; line-height:24px;">Welcome to insureonline.</p>
<br />
<br />Thanks,
<br /> <strong>{{ config('app.name') }}</strong> 
@endsection('content')

{{-- 

@component('mail::message')
Hello {{ $user->name }},

Welcome to insureonline.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
--}}
