<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PolicyDoc extends Model
{
    protected $table = 'policy_docs';
    
    protected $fillable = [
        'user_id', 'order_id', 'docname',
    ];

    public function order() {
        return $this->hasOne('App\Models\Order', 'order_id', 'id');
    }
    
}
