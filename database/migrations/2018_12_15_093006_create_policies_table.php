<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_provider_id');
            $table->integer('agent_id');
            $table->integer('user_id');
            $table->string('policynumber',255)->nullable();
            $table->dateTime('issuedate')->nullable();
            $table->dateTime('effectivedate')->nullable();
            $table->dateTime('expirydate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');
    }
}
