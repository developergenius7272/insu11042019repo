<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPartnerAddonBaseCommisionInInsuranceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_providers', function (Blueprint $table) {
            $table->double('agent_addon_base_commission_percentage')->after('agent_base_commission_amount')->default(0)->nullable();
            $table->double('agent_addon_base_commission_amount')->after('agent_addon_base_commission_percentage')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_providers', function (Blueprint $table) {
            //
        });
    }
}
