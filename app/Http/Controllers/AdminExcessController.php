<?php

namespace App\Http\Controllers;

use App\Models\Excess;
use App\Models\InsuranceProvider;
use App\Models\CarModelBodyType;
// 
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class AdminExcessController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Excess'; 
        $this->data['pageTitle'] = 'Excess';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = "Excess";

        $excesses = Excess::select('excesses.*')
                ->addSelect('insurance_providers.name as partner_name')
                ->addSelect('car_model_body_types.name as car_body_name')
                ->join('insurance_providers','insurance_providers.id', 'excesses.insurance_provider_id')
                ->leftJoin('car_model_body_types','car_model_body_types.id', 'excesses.car_model_body_type_id')
                ->orderBy('insurance_providers.name')
                ->get();

        $this->data['excesses'] = $excesses;
        return view('admin.excess.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a premium plan';
        $partners = InsuranceProvider::all();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        // 
        $this->data['partners'] = $partners;
        $this->data['carBodies'] = $carBodies;
        //
        return view('admin.excess.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $insuranceProviderId = $request->insuranceProviderId;
        $carBodyTypeId = $request->carBodyTypeId;

        $car_model_min_value = $request->car_model_min_value;
        $car_model_max_value = $request->car_model_max_value;
        
        $min_seat = $request->min_seat;
        $max_seat = $request->max_seat;

        $min_age = $request->min_age;
        $max_age = $request->max_age;

        $excess_value = $request->excess_value;
        $excess_description = $request->excess_description;
        
        $status = $request->status;

        $effective_date = $request->effective_date;

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        //Conditions
        // 1. General Conditions
        request()->validate([
            
            'insuranceProviderId' => 'required|numeric',
            "carBodyTypeId"    => "required|array|min:1",
            "effective_date"    => "required",

            //nullable, numeric, greater than zero condition
            'car_model_min_value' => 'nullable|numeric|min:1',
            'car_model_max_value' => 'nullable|numeric|min:1',
            'min_seat' => 'nullable|numeric|min:1',
            'max_seat' => 'nullable|numeric|min:1',
            'min_age' => 'nullable|numeric|min:1',
            'max_age' => 'nullable|numeric|min:1',

        ], [
            'insuranceProviderId.required' => "Insurance Provider is required.",
            'carBodyTypeId.required' => "Car Type is required.",
            'effective_date.required' => "Please enter Effective Date.",
        ]);

        /*
        // 2. Car Model Max value should be greater than min value
        if($car_model_max_value > 0){
            if($car_model_min_value > $car_model_max_value){
                $this->validate($request, [
                    'car_model_max_value_greater' => 'required',
                ],[
                    'car_model_max_value_greater.required' => "Car Model Max value should be greater than min value",
                ]);
            }
            if($car_model_min_value < $car_model_max_value){

                //if(!is_numeric($excess_value) || $excess_value <= 0){
                if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description)){
                    $this->validate($request, [
                        'excess_value_required' => 'required',
                    ],[
                        //'excess_value_required.required' => "Please enter excess value",
                        'excess_value_required.required' => "Please enter Excess Value or Excess Description",
                    ]);
                }
                // else{
                //     $excess_description = null;
                // }
            }
        }

        if($car_model_max_value == 0){
            if($car_model_min_value > 0){
                // if(empty($excess_description)){
                //     $this->validate($request, [
                //         'excess_description_required' => 'required',
                //     ],[
                //         'excess_description_required.required' => "Please enter excess description",
                //     ]);
                // }
                // else{
                //     $excess_value = null;
                // }
                if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description)){
                    $this->validate($request, [
                        'excess_value_or_excess_description' => 'required',
                    ],[
                        'excess_value_or_excess_description.required' => "Please enter Excess Value or Excess Description",
                    ]);
                }
            }
            else{
                $this->validate($request, [
                    'car_model_min_value_required' => 'required',
                ],[
                    'car_model_min_value_required.required' => "Please enter car Model Min Value",
                ]);
            }
        }
        */

        if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description))
        {
            $this->validate($request, [
                'excess_value_or_excess_description' => 'required',
            ],[
                'excess_value_or_excess_description.required' => "Please enter Excess Value or Excess Description",
            ]);
        }

        // 2. Car Model Max Value should be greater than min value
        if($car_model_min_value > $car_model_max_value){
            $this->validate($request, [
                'car_model_max_value_greater' => 'required',
            ],[
                'car_model_max_value_greater.required' => "Car Model Max value should be greater than min value",
            ]);
        }

        // 3. Seat Range Max Seat should be greater than min Seat
        if($min_seat > $max_seat){
            $this->validate($request, [
                'max_seat_greater' => 'required',
            ],[
                'max_seat_greater.required' => "Seat Range Maximum seat should be greater than min seat",
            ]);
        }

        // 4. Age Range Max value should be greater than min value
        if($min_age > $max_age){
            $this->validate($request, [
                'max_age_greater' => 'required',
            ],[
                'max_age_greater.required' => "Age Range Maximum Age should be greater than min value",
            ]);
        }

        //4.It should be one value (either $excess_value OR $excess_description)
        //if(empty($excess_value) && empty($excess_description))
        if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description))
        {
            $this->validate($request, [
                'excess_value_or_excess_description' => 'required',
            ],[
                'excess_value_or_excess_description.required' => "Please enter Excess Value or Excess Description",
            ]);
        }

        //5.Effective date should be greater than current date
        $currentDate = Carbon::now();
        if($currentDate > $effective_date){
            $this->validate($request, [
                'effective_date_greater' => 'required',
            ],[
                'effective_date_greater.required' => "Effective date should be greater than current date",
            ]);
        }

        if(!empty( $insuranceProviderId ) && ( $carBodyTypeId ))
        {
            foreach ($carBodyTypeId as $val){

                    $excess = new Excess();
                    $excess->insurance_provider_id = $insuranceProviderId;
                    $excess->car_model_body_type_id = $val;
                    $excess->car_model_min_value = $car_model_min_value;
                    $excess->car_model_max_value = $car_model_max_value;
                    $excess->min_seat = $min_seat;
                    $excess->max_seat = $max_seat;
                    $excess->min_age = $min_age;
                    $excess->max_age = $max_age;
                    $excess->excess_value = $excess_value;
                    $excess->excess_description = $excess_description;
                    $excess->status = $status;
                    $excess->created_by = $adminID;
                    $excess->modified_by = $adminID;
                    $excess->effective_date = $effective_date;
                    $excess->save();
                

            }
            return redirect('/admin/excess')->with('success', 'Excess added successfully');

        }else{
            return redirect('/admin/excess')->with('error', 'Excess not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $excess = Excess::find($id);
        if( $excess ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update Excess';
            // 
            $partners = InsuranceProvider::all();
            $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
            // 
            $this->data['partners'] = $partners;
            $this->data['carBodies'] = $carBodies;
            // 
            $this->data['data'] = $excess;
            // 
            return view('admin.excess.create_edit', $this->data);
        } else {
            return redirect('/admin/excess')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $insuranceProviderId = $request->insuranceProviderId;
        $carBodyTypeId = $request->carBodyTypeId;

        $car_model_min_value = $request->car_model_min_value;
        $car_model_max_value = $request->car_model_max_value;

        $min_seat = $request->min_seat;
        $max_seat = $request->max_seat;

        $min_age = $request->min_age;
        $max_age = $request->max_age;

        $excess_value = $request->excess_value;
        $excess_description = $request->excess_description;

        $status = $request->status;

        $effective_date = $request->effective_date;

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        //Conditions
        // 1. General Conditions
        request()->validate([
            'insuranceProviderId' => 'required|numeric',
            'carBodyTypeId' => 'required|numeric',
            'effective_date' => 'required',

            //nullable, numeric, greater than zero condition
            'car_model_min_value' => 'nullable|numeric|min:1',
            'car_model_max_value' => 'nullable|numeric|min:1',
            'min_seat' => 'nullable|numeric|min:1',
            'max_seat' => 'nullable|numeric|min:1',
            'min_age' => 'nullable|numeric|min:1',
            'max_age' => 'nullable|numeric|min:1',

        ], [
            'insuranceProviderId.required' => "Insurance Provider is required.",
            'carBodyTypeId.required' => "Car Type is required.",
            'effective_date.required' => "Effective Date is required.",
        ]);

        /*
        // 2. Car Model Max value should be greater than min value
        if($car_model_max_value > 0){
            if($car_model_min_value > $car_model_max_value){
                $this->validate($request, [
                    'car_model_max_value_greater' => 'required',
                ],[
                    'car_model_max_value_greater.required' => "Car Model Max value should be greater than min value",
                ]);
            }
            if($car_model_min_value < $car_model_max_value){

                //if(!is_numeric($excess_value) || $excess_value <= 0){
                if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description)){
                    $this->validate($request, [
                        'excess_value_required' => 'required',
                    ],[
                        //'excess_value_required.required' => "Please enter excess value",
                        'excess_value_required.required' => "Please enter Excess Value or Excess Description",
                    ]);
                }
                // else{
                //     $excess_description = null;
                // }
            }
        }

        if($car_model_max_value == 0){
            if($car_model_min_value > 0){
                // if(empty($excess_description)){
                //     $this->validate($request, [
                //         'excess_description_required' => 'required',
                //     ],[
                //         'excess_description_required.required' => "Please enter excess description",
                //     ]);
                // }
                // else{
                //     $excess_value = null;
                // }
                if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description)){
                    $this->validate($request, [
                        'excess_value_or_excess_description' => 'required',
                    ],[
                        'excess_value_or_excess_description.required' => "Please enter Excess Value or Excess Description",
                    ]);
                }
            }
            else{
                $this->validate($request, [
                    'car_model_min_value_required' => 'required',
                ],[
                    'car_model_min_value_required.required' => "Please enter car Model Min Value",
                ]);
            }
        }
        */

        // 2. Car Model Max Value should be greater than min value
        if($car_model_min_value > $car_model_max_value){
            $this->validate($request, [
                'car_model_max_value_greater' => 'required',
            ],[
                'car_model_max_value_greater.required' => "Car Model Max value should be greater than min value",
            ]);
        }

        // 3. Seat Range Max Seat should be greater than min Seat
        if($min_seat > $max_seat){
            $this->validate($request, [
                'max_seat_greater' => 'required',
            ],[
                'max_seat_greater.required' => "Seat Range Maximum seat should be greater than min seat",
            ]);
        }

        // 4. Age Range Max value should be greater than min value
        if($min_age > $max_age){
            $this->validate($request, [
                'max_age_greater' => 'required',
            ],[
                'max_age_greater.required' => "Age Range Maximum Age should be greater than min value",
            ]);
        }

        //4.It should be one value (either $excess_value OR $excess_description)
        if((!is_numeric($excess_value) || $excess_value <= 0) && empty($excess_description))
        {
            $this->validate($request, [
                'excess_value_or_excess_description' => 'required',
            ],[
                'excess_value_or_excess_description.required' => "Please enter Excess Value or Excess Description",
            ]);
        }
        
        //5.Effective date should be greater than current date
        $excess = Excess::find($id);
        $old_effective_date = $excess->effective_date;

        $currentDate = Carbon::now();
        if($old_effective_date != $effective_date){

            $currentDate = Carbon::now();
            if($currentDate > $effective_date){
                $this->validate($request, [
                    'effective_date_greater' => 'required',
                ],[
                    'effective_date_greater.required' => "Effective date should be greater than current date",
                ]);
            }

        }

        
        if(!empty( $insuranceProviderId ) || !empty( $carBodyTypeId ))
        {
            //$excess = Excess::find($id);
            if( $excess ) {

                $excess->insurance_provider_id = $insuranceProviderId;
                $excess->car_model_body_type_id = $carBodyTypeId;
                $excess->car_model_min_value = $car_model_min_value;
                $excess->car_model_max_value = $car_model_max_value;
                $excess->min_seat = $min_seat;
                $excess->max_seat = $max_seat;
                $excess->min_age = $min_age;
                $excess->max_age = $max_age;
                $excess->excess_value = $excess_value;
                $excess->excess_description = $excess_description;

                $excess->status = $status;
                $excess->effective_date = $effective_date;
                $excess->modified_by = $adminID;
                $excess->save();

                return redirect('/admin/excess')->with('success', 'Excess updated successfully');
            }else{
                return redirect('/admin/excess')->with('error', 'Excess not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
