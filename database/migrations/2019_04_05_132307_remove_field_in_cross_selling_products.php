<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldInCrossSellingProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cross_selling_products', function (Blueprint $table) {
            $table->dropColumn('addon_commission_type');
            $table->dropColumn('addon_commission_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cross_selling_products', function (Blueprint $table) {
            //
        });
    }
}
