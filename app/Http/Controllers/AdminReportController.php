<?php

namespace App\Http\Controllers;
// 
use App\Models\Order;
use App\User;
use App\Models\BenefitMaster;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminReportController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Partners'; 
        $this->data['pageTitle'] = 'Partners';
    }

    public function commission()
    {
        $this->data['tableHeading'] = 'Partner Commission Report';
        $orders = Order::all();
        $this->data['orders'] = $orders;
        //return view('admin.orders.index', $this->data);
        return view('admin.reports.commission', $this->data);
    }
    
    public function agentcommission()
    {
        $this->data['tableHeading'] = 'Agent Commission Report';
        $orders = Order::all();

        //echo "<pre>";print_r($arrOrders);die;
        $this->data['orders'] = $orders;
        //return view('admin.orders.index', $this->data);
        return view('admin.reports.agent-commission', $this->data);
    }
    
    /*
    public function agentcommission()
    {
        $this->data['tableHeading'] = 'Agent Commission Report';
        $orders = Order::all();
//dd($orders);
        $arrOrders  = array();
        foreach ($orders as $key => $value) {

            $agent_id = $value['agent_id'];
            $base_commission_amount = $value['base_commission_amount'];
            $base_commission_percentage = $value['base_commission_percentage'];
            $premium_value = $value['premium_value'];
            $pab_passenger = $value['pab_passenger'];
            $pab_driver = $value['pab_driver'];
            
            $partnerCommission = 0;
            if($base_commission_amount > 0){
                $partnerCommission = $base_commission_amount;
            }
            else if($base_commission_percentage > 0){

                $partnerCommission = ((double)$premium_value + (double)$pab_passenger + (double)$pab_driver);

                $partnerCommission = (double)$partnerCommission * ((double)$base_commission_percentage / 100);
            }

            $agent_commission_value = $value['agent_commission_value'];
            $agent_commission_value = $value->additionalCovers;

            $arrOrders[$agent_id]['agent_id'] = $agent_id;  
            //$arrOrders[$agent_id]['partnerCommission'] = $partnerCommission; 
            $arrOrders[$agent_id]['agent_commission_value'] =  $agent_commission_value; 
        }

        echo "<pre>";print_r($arrOrders);die;
        $this->data['orders'] = $orders;
        //return view('admin.orders.index', $this->data);
        return view('admin.reports.agent-commission', $this->data);
    }
    */
}
