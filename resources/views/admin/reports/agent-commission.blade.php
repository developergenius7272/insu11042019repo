@extends('layouts.admin.master')
{{-- prontend users table view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>{{$tableHeading ?? 'Agents'}}</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{-- <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p> --}}
                        
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Agent</th>
                                    <th>User</th>
                                    <th>Partners</th>
                                    <th>Total</th>


                                    <th>Agent commission %</th>
                                    <th>Agent commission</th>
                                    <th>Agent Addon commission %</th>
                                    <th>Agent AddOn Comm.</th>
                                    <th>Total Comm.</th>
                                    <th>Created Date</th>
                                    {{-- <th>Benefit Options</th> --}}
                                    <th>View</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if($orders)
                                @foreach($orders as $order)
                                <tr>

                                    <td>{{ $order->agent ? $order->agent->name : $order->name  }} </td>
                                    <td>{{ $order->user ? $order->user->name : $order->name  }} </td>
                                    <td>{{ $order->insuranceProvider ? $order->insuranceProvider->name : $order->name  }} </td>
                                    <td>{{$order->total > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->total,2) : 'N/A' }} </td>

                                    {{-- Agent Commssion % --}}
                                    <td>
                                        {{-- {{ $order->commission_percentage > 0 ? $order->commission_percentage. ' %' : '' }} --}}
                                        {{ $order->agent_commission_percentage_or_amount == 1 ? $order->agent_commission_amount. ' %' : '' }}
                                    </td>
                                    {{-- Agent Commssion --}}
                                    <td>
                                            {{ $order->agent_commission > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($order->agent_commission,2) : 'N/A' }} 

                                    </td>

                                    {{-- Agent Addon commission % --}}
                                    <td>
                                        @php 
                                            $agent_commission_value  = 0;
                                            $additionalArr = $order->additionalCovers; 
                                        @endphp 

                                        @foreach($additionalArr as $key => $value)
                                            @if($value->agent_commission_percentage_or_amount == 1)
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        {{ $value->agent_commission_amount.' %' }}
                                                    </div>
                                                </div>

                                            @elseif($value->agent_commission_percentage_or_amount == 2)
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        {{ $value->agent_commission_amount > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($value->agent_commission_amount,2) : 'N/A' }}
                                                    </div>
                                                </div>
                                            @endif

                                            {{-- {{ $value->agent_commission_percentage_or_amount }}, --}}
                                        @endforeach
                                    </td>

                                    {{-- Agent AddOn Comm. --}}
                                    <td>
                                        @php 
                                            $agent_commission_value  = 0;
                                            $additionalArr = $order->additionalCovers; 
                                        @endphp 

                                        @foreach($additionalArr as $key => $value)

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        
                                                        {{ $value->agent_commission_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($value->agent_commission_value,2) : 'N/A' }}
                                                    </div>
                                                </div>

                                            {{-- {{ $value->agent_commission_percentage_or_amount }}, --}}
                                        @endforeach
                                    </td>


                                    {{-- Total Comm. --}}
                                    <td>
                                        @php 
                                            $agent_commission_value  = 0;
                                            $additionalArr = $order->additionalCovers; 
                                        @endphp 
                                            {{-- {{$additionalArr}} --}}
                                        
                                        @foreach($additionalArr as $key => $value)
                                            
                                            @php 
                                                $agent_commission_value += $value->agent_commission_value;
                                                // $agent_commission_value = $agent_commission_value + $value->agent_commission_value;
                                                
                                            @endphp
                                        @endforeach
                                        @php $agent_commission_value = $agent_commission_value + $order->agent_commission_value @endphp
                                        {{ $agent_commission_value > 0 ? env('CURRENCY_CODE', 'AED').' '.number_format($agent_commission_value,2) : 'N/A' }} 
                                    </td>




                                    <td>{{ isset($order->created_at) ? $order->created_at->format('d/m/Y H:i') : '' }}</td>

                                    {{-- <td>{{ $order->benefit_options  }} </td> --}}
                                    {{-- <td>
                                        @php ($obj = json_decode($order->benefit_options,true)) @endphp 
                                        @php ($arrs =array()) @endphp 
                                        @if($obj)
                                            @foreach($obj as $key => $value)
                                            
                                                @php ($master_id = array_search($key, $arrayBenefitMasterId)) @endphp
                                                @if($master_id !== false)

                                                    @php ($arrs[$key]['name'] = $arrayBenefitMasterName[$master_id]) @endphp

                                                @else

                                                    @php ($arrs[$key]['name'] = $key) @endphp
                                                @endif

                                                @if($value == 'yes')

                                                    @php ($value = 'Include') @endphp

                                                @elseif($value == 'no')
                                                    @php ($value = 'Not Include') @endphp                                                
                                                @endif

                                                @php ($arrs[$key]['value'] = $value) @endphp
                                            @endforeach


                                            @if($arrs)
                                                @php ($i = 1) @endphp
                                                @foreach($arrs as $arr)
                                                
                                                    @if($i <= 3)
                                                        <div class="row myrow">
                                                            <div class="col-md-7">
                                                                {{ $arr['name'] }}
                                                            </div>
                                                                
                                                            <div class="col-md-4">
                                                                {{ $arr['value'] }}
                                                            </div> 
                                                        </div>
                                                    @else

                                                        <div class="row myrow morerows content content_{{ $order->id }}">
                                                            <div class="col-md-7">
                                                                {{ $arr['name'] }}
                                                            </div>
                                                                
                                                            <div class="col-md-4">
                                                                {{ $arr['value'] }}
                                                            </div> 
                                                        </div>

                                                    @endif

                                                    @php ($i++) @endphp
                                                @endforeach

                                                <a href="javascript:void(0)" class="show_hide show_hide_{{ $order->id }}" val="show_hide_{{ $order->id }}" data-content="toggle-text">Read More</a>

                                            @endif
                                        @endif
                                    </td> --}}
                                    <td>
                                        <a class="btn btn-primary" href="{{route('admin.orders.show',[$order->id,$order->users_id])}}">View Order</a>
                                    </td>

                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
		
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
    $(document).ready(function() {
        
        pageSetUp();
        
        /* // DOM Position key index //
    
        l - Length changing (dropdown)
        f - Filtering input (search)
        t - The Table! (datatable)
        i - Information (records)
        p - Pagination (paging)
        r - pRocessing 
        < and > - div elements
        <"#id" and > - div with an id
        <"class" and > - div with a class
        <"#id.class" and > - div with an id and class
        
        Also see: http://legacy.datatables.net/usage/features
        */	

        /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
   "order": [[ 9, 'desc' ]],
  //  "columnDefs": [
  //     //{ "targets": [0], "orderable": false }
  //     { targets: 'no-sort', orderable: true }
  // ],
    // "columnDefs": [ {
    //   "targets"  : 'no-sort',
    //   "orderable": false,
    //   "order": []
    // }],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

        /* END BASIC */
        
        // /* COLUMN FILTER  */
        // var otable = $('#datatable_fixed_column').DataTable({
        //     //"bFilter": false,
        //     //"bInfo": false,
        //     //"bLengthChange": false
        //     //"bAutoWidth": false,
        //     //"bPaginate": false,
        //     //"bStateSave": true // saves sort state using localStorage
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_fixed_column) {
        //             responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_fixed_column.respond();
        //     }		
        
        // });
        
        // // custom toolbar
        // $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
                
        // // Apply the filter
        // $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
            
        //     otable
        //         .column( $(this).parent().index()+':visible' )
        //         .search( this.value )
        //         .draw();
                
        // } );
        // /* END COLUMN FILTER */   
    
        // /* COLUMN SHOW - HIDE */
        // $('#datatable_col_reorder').dataTable({
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_col_reorder) {
        //             responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_col_reorder.respond();
        //     }			
        // });
        
        // /* END COLUMN SHOW - HIDE */

        // /* TABLETOOLS */
        // $('#datatable_tabletools').dataTable({
            
        //     // Tabletools options: 
        //     //   https://datatables.net/extensions/tabletools/button_options
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },		
        //     "oTableTools": {
        //             "aButtons": [
        //             "copy",
        //             "csv",
        //             "xls",
        //             {
        //                 "sExtends": "pdf",
        //                 "sTitle": "SmartAdmin_PDF",
        //                 "sPdfMessage": "SmartAdmin PDF Export",
        //                 "sPdfSize": "letter"
        //             },
        //             {
        //                 "sExtends": "print",
        //                 "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
        //             }
        //             ],
        //         "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        //     },
        //     "autoWidth" : true,
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_tabletools) {
        //             responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_tabletools.respond();
        //     }
        // });
        
        // /* END TABLETOOLS */
    
    })

</script>

<script type="text/javascript">
$(document).ready(function () {
    $(".content").hide();
    $(".show_hide").on("click", function () {

        var show_hide = $(this).attr('val');
        
        var arr = show_hide.split('_');
        var orderId = arr[2];

        // var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
        // $(".show_hide").text(txt);
        // $(this).next('.content').slideToggle(200);

        var txt = $(".content_"+orderId).is(':visible') ? 'Read More' : 'Read Less';
        //console.log(txt);
        $(".show_hide_"+orderId).text(txt);
        //$(this).next('.content_'+orderId).slideToggle(200);
        $('.content_'+orderId).slideToggle(500);
    });
});
</script>
@endsection
