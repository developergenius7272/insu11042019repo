@component('mail::message')
Hello {{ $user->name }},

Thank you for buying a policy from InsureOnline. Your Transaction is successful. For any future reference please quote us with transaction id : {{ $orders->id }}


Thanks,<br>
{{ config('app.name') }}
@endcomponent
