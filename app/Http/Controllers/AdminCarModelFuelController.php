<?php

namespace App\Http\Controllers;

// 
use App\Models\CarModelFuel;
//
use Illuminate\Http\Request;
use Auth;

class AdminCarModelFuelController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Car'; 
        $this->data['breadcrumbs'][] = 'Model';
        $this->data['breadcrumbs'][] = 'Fuel';
        $this->data['pageTitle'] = 'Car Fuels';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Car Modal Fuel';
        $this->data['carFuels'] = CarModelFuel::all();
        return view('admin.car_fuel.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new car model fuel';
        return view('admin.car_fuel.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;

        request()->validate([
            'name' => 'required|unique:car_model_fuels|min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        if(!empty( $name ))
        {
            $body = CarModelFuel::where('name',$name)->first();
            if( !$body ) {
                $body = new CarModelFuel();
                $body->name = $name;
                $body->status = $status;
                $body->created_by = $adminID;
                $body->modified_by = $adminID;
                $body->save();
                return redirect('/admin/cars/car-model-fuel')->with('success', 'Car Model Fuel added successfully');
            } else {
                return redirect('/admin/cars/car-model-fuel')->with('error', 'Car Model Fuel not added');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit car model fuel';
        $this->data['data'] = CarModelFuel::find($id);

        if( $this->data['data'] ) {
            // 
            return view('admin.car_fuel.create_edit',$this->data);
        } else {
            return redirect('/admin/cars/car-model-fuel')->with('error', 'No data found');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;

        request()->validate([
            'name' => "required|unique:car_model_fuels,name,$id|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        if(!empty($name ))
        {
            $body = CarModelFuel::where('name',$name)->where('id','<>',$id)->first();
            if( !$body ) {
                $body = CarModelFuel::find($id);
                if( $body ) {
                    $body->name = $name;
                    $body->status = $status;
                    $body->modified_by = $adminID;
                    $body->save();
                    return redirect('/admin/cars/car-model-fuel')->with('success', 'Car Model Fuel updated successfully');
                } else {
                    return redirect('/admin/cars/car-model-fuel')->with('error', 'Car Model Fuel not updated');
                }
            } else {
                return redirect('/admin/cars/car-model-fuel')->with('error', 'Car Model Fuel not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carModelFuel = CarModelFuel::find($id);
        if( $carModelFuel ) {
            // find child data if any
            $carModelsVariantCount = \App\Models\CarModelVariant::where('car_model_fuel_id', $carModelFuel->id)->count();
            if( $carModelsVariantCount ) {
                return redirect('/admin/cars/car-model-fuel')->with('success','Information has not been deleted');
            } else {
                $carModelFuel->delete();
                return redirect('/admin/cars/car-model-fuel')->with('success','Information has been deleted');
            }
        } else {
            return redirect('/admin/cars/car-model-fuel')->with('success','No information found');
        }
    }
}
