@extends('layouts.admin.master')
{{-- Car model add/edit table view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarModel" method="POST" @if($formType == 'edit') action="{{action('AdminCarModelController@update', $data->id)}}" @else action="{{url('/admin/cars/car-models')}}" @endif>
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <legend>{{$formTitle ?? ' '}}</legend>
                                @if( $formType == 'edit')
                                <div class="form-group" id="carMakeName">
                                    <label class="control-label col-md-2">Car make name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {{-- ui-autocomplete-loading --}}
                                                <!--input class="form-control" id="carMakeName" type="text" value="{{--$data->car_make_name ?? ''--}}" disabled-->

                                                <input class="form-control" id="carMakeName" type="text" value="{{$data->carmake['name'] ?? ''}}" disabled>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="selectCarMake">Select Car Make</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="selectCarMake" name="carMakeId">
                                            @if(isset($carMakes) && count($carMakes))
                                                @foreach($carMakes as $carMake)
                                                    <option value="{{$carMake->id}}">{{strtoupper($carMake->name)}}</option>
                                                @endforeach
                                                    {{-- <option value="new">Create New</option> --}}
                                            @endif
                                        </select>

                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--  --}}
                                <div class="form-group hide" id="carMakeNewName">
                                    <label class="control-label col-md-2">Enter New Car Make Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control ui-autocomplete-loading" placeholder="Enter New Car Make" name="carMakeNewName" type="text">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                @endif
                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ $formType == 'edit' ? 'Update' : 'Enter'}} Car model name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {{-- ui-autocomplete-loading --}}
                                            <input class="form-control " placeholder="{{ $formType == 'edit' ? 'Update' : 'Enter'}} Car model Name : 1 Series" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--  --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label">Car Model Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="carModelDescription" placeholder="Car Model Description" rows="4"></textarea>
                                    </div>
                                </div> --}}
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/cars/car-models')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
		
    $(document).ready(function() {
        
        pageSetUp();

        $("#selectCarMake").change(function() {
            if($(this).val() == 'new') {
                //$("#carMakeNewName").removeClass('hide');
            }
        });

        $("#formCarModel").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));
        
    })

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var selectCarMake = $('#selectCarMake option:selected').index();
        var name  = $.trim($("#name").val());

        if (selectCarMake <= 0){
            $("#selectCarMake").parent().find(".my-error").html('Please select Car Make').show();
            $('#selectCarMake').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (name == ''){    
            $("#name").parent().find(".my-error").html('Please enter Car Model Name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        // if($('#carMakeNewName').is(":visible")){
        //     var carMakeNewName  = $.trim($("#carMakeNewName").val());
        //     alert('3');
        //     if(carMakeNewName == ''){
        //         $("#carMakeNewName").parent().find(".my-error").html('Please enter Car Make Name').show();
        //         $('#carMakeNewName').closest('.form-group').addClass('has-error');
        //         is_valid = false;
        //     }
        // }

        return is_valid;
    }
    
    function hideErrorDiv() {

        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
