<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// 
use App\Models\CarModelFuel;

class CreateCarModelFuelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_model_fuels');
        Schema::create('car_model_fuels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'petrol';
        $CarModelFuel->save();
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'diesel';
        $CarModelFuel->save();
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'CNG';
        $CarModelFuel->save();
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'LPG';
        $CarModelFuel->save();
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'CNG Kit';
        $CarModelFuel->save();
        // 
        $CarModelFuel = new CarModelFuel();
        $CarModelFuel->name = 'External CNG Kit';
        $CarModelFuel->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_model_fuels');
    }
}
