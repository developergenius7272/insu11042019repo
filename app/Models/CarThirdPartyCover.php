<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Excess;
use Carbon\Carbon;

class CarThirdPartyCover extends Model
{
    protected $table = 'car_third_party_covers';
    protected $dates = ['effective_date'];
    
    public function insuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id')->active();
    }

    public function recomendedInsuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id')->recomended();
    }

    public function carModelBodyType()
    {
        return $this->belongsTo(CarModelBodyType::class, 'car_model_body_type_id', 'id')->where('status', '=', 1);    
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeEffectiveDate($query)
    {
        $current_date = Carbon::now()->toDateTimeString();
        return $query->where('effective_date','<=',$current_date);
    }

    public function scopeAge($query, $age)
    {
        return $query->where('max_age','=',0)->orWhereNull('max_age')
            ->orWhere(function($q) use($age){
                    return $q->where('min_age', '<=', $age)->where('max_age', '>=', $age);
                });
    }

    public function scopeDrivingExperience($query,$driving_exp){
        return $query->where('driving_license_min_age','=',0)->orWhereNull('driving_license_min_age')
        ->orWhere(function($q) use($driving_exp){
                return $q->where('driving_license_min_age', '<=', $driving_exp);
            });
    }

    public function scopeCylinderRange($query, $no_of_cylinders)
    {
        return $query->where('car_model_cylinder', '=', $no_of_cylinders)->orWhere(function ($q) use ($no_of_cylinders) {
            return $q->where('above_cylinder','=',1)->where('car_model_cylinder', '<=', $no_of_cylinders);
        });
    }
    
    public function thirdPartyPremiumRate($carValueInput)
    {
        $priceAEDAmount =  $this->rate;
        return $priceAEDAmount;
        //return ( $priceAEDAmount > $this->min_premium_agency ) ? $priceAEDAmount : $this->min_premium_agency;
    }
    
    public function checkavailability($getMonths){
        return ( isset($this->available_for_months) && ($getMonths <= $this->available_for_months) ) ? false : true; 
    }

    // public function excessValue($carValue,$body_type,$insurance_id,$age){

    //     $excess_value = Excess::query();
    //     $excess_value = $excess_value->where('max_age','=',0)
    //                                  ->orWhereNull('max_age')
    //                                  ->orWhere(function($q) use($age){
    //                                         $q->where('min_age', '<=', $age)->where('max_age', '>=', $age);
    //                                     })->where('car_model_body_type_id','=',$body_type)
    //                                  ->where('insurance_provider_id','=',$insurance_id)
    //                                  ->where('car_model_min_value', '<', $carValue)
    //                                  ->where('car_model_max_value', '>', $carValue)->get();
                                     
    //        return $excess_value;
    // }

    public function excessValue($carValue,$age,$no_of_seats)
    {
        $current_date = Carbon::now()->toDateTimeString();
        $test = $this->hasManyThrough(Excess::class, CarModelBodyType::class, 'id', 'car_model_body_type_id', 'car_model_body_type_id','id');

          $test->where('insurance_provider_id', '=', $this->insurance_provider_id);
          return $test->where(function($q) use($carValue){
                        $q->where('car_model_min_value', '<', $carValue)
                    ->where('car_model_max_value', '>', $carValue)->orWhereNull('car_model_max_value');})
                    ->where(function($q) use($age){$q->where('min_age','<=',$age)
                    ->where('max_age','>=',$age)->orWhereNull('max_age');})
                    ->where(function($q) use($no_of_seats){
                        $q->where('min_seat','<=',$no_of_seats)
                           ->where('max_seat','>=',$no_of_seats)
                           ->orWhereNull('max_seat');})
                    ->where('effective_date','<=',$current_date)->get();
                    // ->where('min_seat','<=',$no_of_seats)
                    // ->where('max_seat','>=',$no_of_seats)
                    // ->orWhereNull('max_seat')
                    
        
        // $test->where('insurance_provider_id', '=', $this->insurance_provider_id);
        // return $test->where('car_model_min_value', '<=', $this->car_model_max_value) 
        //             ->where('car_model_max_value', '>=', $this->car_model_max_value) //300000//60000
        //             ->where('min_age','<=',$this->max_age)
        //             ->where('max_age','>=',$this->max_age)->get();
    }

}
