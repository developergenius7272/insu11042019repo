<?php

use Illuminate\Database\Seeder;
use App\Models\PolicyDocumentMaster;

class PolicyDocumentMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $docs = [
        	[
        		'name' => 'Identity document',
        		'status' => 1
        	]
        ];
        foreach ($docs as $doc) {
            PolicyDocumentMaster::create($doc);
        }
    }
}
