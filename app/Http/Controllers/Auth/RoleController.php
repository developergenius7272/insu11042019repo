<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;

use Session;

use APP\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Permission;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::getRoles();
        $role = Role::where('name','!=','super-admin')->get();
        $roles = $roles->merge($role);
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissionGroups =  Permission::getPermissions();

        return view('admin.roles.create_edit', [
            'permissionGroups' => $permissionGroups,
            'formType' => 'add',
            'formTitle' => 'Add a Role'
            ]);
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $validatedData = $request->validate([
            'display_name' => 'required|unique:roles|max:255',
            'permissions.*' => 'required|exists:permissions,name'
        ]);

        if ( empty( $request->input('permissions') ) ) {
            Session::flash('warning', 'Please select some permission to the role!');
            return back();
        }

        //Store
        $createRole = new Role();
        $createRole->name = preg_replace('#[ -]+#', '-', strtolower($validatedData['display_name']));

        if ( Role::where('name', '=', $createRole->name)->get()->isEmpty() ) {
            
            $createRole->display_name = $request->input('display_name');
            $createRole->description = $request->input('description');
            $createRole->save();
            //Assign permissions to role here
            if ( ! empty( $validatedData['permissions'] ) ){
                foreach ( $validatedData['permissions'] as $permission ){
                    $permission = Permission::first()->where('name', $permission )->first();
                    $createRole->attachPermission($permission);
                }
            }
        } else {
            //toastr('Role is already exists!', 'warning');
            Session::flash('warning', 'Role is already exists!');
        }
        //Success message
        Session::flash('success', 'New Role is created.');
        //toastr('New Role is created.');

        //redirect
        return back();
    }

    /**
     * Display the specified role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return view('roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissionGroups = Permission::getPermissions();
        return view('admin.roles.create_edit', [
            'role' => Role::find($id),
            'permissionGroups' => $permissionGroups,
            'formType' => 'edit',
            'formTitle' => 'Update Role'
            ]);
    }

    /**
     * Update the specified role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $flag = false;

        $validatedData = $request->validate([
            'name' => 'string|max:255|unique:roles,name' . $role->id,
            'permissions.*' => 'required|exists:permissions,name'
        ]);

        $name = preg_replace('#[ -]+#', '-', strtolower($request->input('display_name')));

        if ( $role->name != $name && ! Role::where('name', '=',  $role->name)->get()->isEmpty() ) {
            //toastr('Please use a differnt name, the same name is getting used for another role.', 'warning');
            Session::flash('warning','Please use a differnt name, the same name is getting used for another role.');
            return back();
        }

        if ( empty( $request->input('permissions') ) ) {
            //toastr('Please assign some permission to the role!', 'warning');
            Session::flash('warning','Please assign some permission to the role!');
            return back();
        }

        //Update
        $role->name = $name;
        $role->display_name = $request->input('display_name');
        $role->description = $request->input('description');
        
        if($role->save()) {
            //toastr('Role is updated.');
            Session::flash('success','Role is updated.');
        } else {
            //toastr('Unable to update role, please try again later.', 'warning');
            Session::flash('warning','Unable to update role, please try again later.');

        }

        //Assign a permission to role here
        if ( ! empty( $request->input('permissions') ) ){
            $role->perms()->sync([]);
            foreach ( $request->input('permissions') as $permission ){
                $permission = Permission::first()->where('name', $permission )->first();
                $role->attachPermission($permission);
            }
        }
        //Redirect
        return redirect()->route('admin.roles.index');
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $users = RoleUser::where('role_id', '=', $id)->get();

        if ( $users->isEmpty() ) {
            $role->delete();
            toastr('Role is deleted.');
        } else {
            toastr('Role is associated to some user, please do not delete it!', 'warning');
        }

        // try {
        //     $role->delete();
        //     toastr('Role is deleted.');
        // } catch ( QueryException $e) {
    
        //     if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
        //         // return error to user here
        //         toastr('Role can not be deleted.');
        //     }
        // }
        return redirect()->route('roles.index');
    }
}