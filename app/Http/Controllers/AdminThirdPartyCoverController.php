<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
use App\Models\CarPremium;
use App\Models\InsuranceProvider;
use App\Models\CarThirdPartyCover;
//
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class AdminThirdPartyCoverController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Premiums'; 
        $this->data['breadcrumbs'][] = 'TPL Cover'; 
        $this->data['pageTitle'] = 'Premiums';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = "Partner's TPL covers";

        $plans = CarThirdPartyCover::select('car_third_party_covers.*')
                ->addSelect('insurance_providers.name as partner_name')
                ->addSelect('car_model_body_types.name as car_body_name')
                ->join('insurance_providers','insurance_providers.id', 'car_third_party_covers.insurance_provider_id')
                ->leftJoin('car_model_body_types','car_model_body_types.id', 'car_third_party_covers.car_model_body_type_id')
                ->orderBy('insurance_providers.name')
                ->get();

        $this->data['plans'] = $plans;

        //return view('admin.car_premium.index', $this->data);
        return view('admin.car_third_party_covers.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a premium plan';
        $partners = InsuranceProvider::all();
        $carMakes = CarMake::active()->get();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        $carFuels = CarModelFuel::active()->orderBy('name','asc')->get();

        $cylindersGroup = CarModelVariant::select('cylinders')
                ->groupBy('cylinders')
                ->orderBy('cylinders')
                ->get();

        $this->data['partners'] = $partners;
        $this->data['carMakes'] = $carMakes;
        $this->data['carBodies'] = $carBodies;
        $this->data['carFuels'] = $carFuels;
        $this->data['cylindersGroup'] = $cylindersGroup;
        

        //
        return view('admin.car_third_party_covers.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $insuranceProviderId = $request->insuranceProviderId;
        // 
        $carBodyTypeId = $request->carBodyTypeId;
        $cylinders = $request->cylinders;
        // 
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        // 
        $driving_license_min_age = $request->driving_license_min_age;
        $driving_license_max_age = $request->driving_license_max_age;
        $status = $request->status;
         $rate = $request->rate;
         $effective_date = $request->effective_date;

      if( empty($request->above_cylinder) ){
         $above_cylinder =  0;
      }else{
            $above_cylinder = $request->above_cylinder;
         }
         
         if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        request()->validate([
            'insuranceProviderId' => 'required|numeric',
            //'carBodyTypeId' => 'required|numeric',
            "carBodyTypeId"    => "required|array|min:1",
            "effective_date"    => "required|date_format:d/m/Y H:i",

            "rate" => "required|numeric|min:0",
            //'cylinders' => 'required',

            //nullable, numeric, greater than zero condition
            'cylinders' => 'nullable|numeric|min:1',
            'min_age' => 'nullable|numeric|min:1',
            'max_age' => 'nullable|numeric|min:1',
            'driving_license_min_age' => 'nullable|numeric|min:1',

        ], [
            'insuranceProviderId.required' => "Insurance Provider is required.",
            'carBodyTypeId.required' => "Car Type is required.",
            //'cylinders.required' => "Cylinders is required.",
        ]);

        // 3. Age Range Max value should be greater than min value
        if($min_age > $max_age){
            $this->validate($request, [
                'max_age_greater' => 'required',
            ],[
                'max_age_greater.required' => "Age Range Maximum Age should be greater than min value",
            ]);
        }

        //5.Effective date should be greater than current date
        $currentDate = Carbon::now();
        if($currentDate > $effective_date){
            $this->validate($request, [
                'effective_date_greater' => 'required',
            ],[
                'effective_date_greater.required' => "Effective date should be greater than current date",
            ]);
        }


        //if(!empty( $insuranceProviderId ) || !empty( $carBodyTypeId ))
        if(!empty( $insuranceProviderId ) && ( $carBodyTypeId ))
        {

            foreach ($carBodyTypeId as $val){

                $planExist = CarThirdPartyCover::where('insurance_provider_id',$insuranceProviderId)
                                                        ->where('car_model_body_type_id',$val)->first();
                //echo "<pre>";print_r($planExist);die;

                if($planExist){

                    $id = $planExist->id;

                    $plan = CarThirdPartyCover::find($id);
                    if( $plan ) {

                        $plan->insurance_provider_id = $insuranceProviderId;
                        $plan->car_model_body_type_id = $val;
                        $plan->car_model_cylinder = $cylinders;
                        $plan->min_age = $min_age;
                        $plan->max_age = $max_age;
                        $plan->driving_license_min_age = $driving_license_min_age;
                        $plan->driving_license_max_age = $driving_license_max_age;
                        $plan->status = $status;
                        $plan->rate = $rate;
                        $plan->effective_date = $effective_date;
                        $plan->above_cylinder = $above_cylinder;
                        $plan->modified_by = $adminID;
                        $plan->save();

                        //return redirect('/admin/partners/third-party-covers')->with('success', 'Information has been added');
                    }
                }
                else{

                    $plan = new CarThirdPartyCover();
                    $plan->insurance_provider_id = $insuranceProviderId;
                    $plan->car_model_body_type_id = $val;
                    $plan->car_model_cylinder = $cylinders;
                    $plan->min_age = $min_age;
                    $plan->max_age = $max_age;        
                    $plan->driving_license_min_age = $driving_license_min_age;
                    $plan->driving_license_max_age = $driving_license_max_age;
                    $plan->status = $status;
                    $plan->rate = $rate;
                    $plan->effective_date = $effective_date;
                    $plan->above_cylinder = $above_cylinder;
                    $plan->created_by = $adminID;
                    $plan->modified_by = $adminID;                    
                    $plan->save();
                }

            }
            return redirect('/admin/partners/third-party-covers')->with('success', 'Third Party Cover added successfully');

        }else{
            return redirect('/admin/partners/third-party-covers')->with('error', 'Third Party Cover not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = CarThirdPartyCover::find($id);
        if( $plan ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update partner';
            // 
            $partners = InsuranceProvider::all();
            $carMakes = CarMake::active()->get();
            $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
            $carFuels = CarModelFuel::active()->orderBy('name','asc')->get();
            // 
            $this->data['partners'] = $partners;
            $this->data['carMakes'] = $carMakes;
            $this->data['carBodies'] = $carBodies;
            $this->data['carFuels'] = $carFuels;
            
            // 
            $this->data['data'] = $plan;

            $cylindersGroup = CarModelVariant::select('cylinders')
                    ->groupBy('cylinders')
                    ->orderBy('cylinders')
                    ->get();

            $this->data['cylindersGroup'] = $cylindersGroup;
            // 
            return view('admin.car_third_party_covers.create_edit', $this->data);
        } else {
            return redirect('/admin/partners/third-party-covers')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $insuranceProviderId = $request->insuranceProviderId;
        // 
        $carBodyTypeId = $request->carBodyTypeId;
        $cylinders = $request->cylinders;
        // 
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        // 
        $driving_license_min_age = $request->driving_license_min_age;
        $driving_license_max_age = $request->driving_license_max_age;
        $status = $request->status;
        $rate = $request->rate;
        $effective_date = $request->effective_date;
        $above_cylinder =  $request->above_cylinder;
      
        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }
        

        request()->validate([
            'insuranceProviderId' => 'required|numeric',
            'carBodyTypeId' => 'required|numeric',
            //'cylinders' => 'required',
            "effective_date"    => "required|date_format:d/m/Y H:i",
            "rate" => "required|numeric|min:0",

            //nullable, numeric, greater than zero condition
            'cylinders' => 'nullable|numeric|min:1',
            'min_age' => 'nullable|numeric|min:1',
            'max_age' => 'nullable|numeric|min:1',
            'driving_license_min_age' => 'nullable|numeric|min:1',

        ], [
            'insuranceProviderId.required' => "Insurance Provider is required.",
            'carBodyTypeId.required' => "Car Type is required.",
            //'cylinders.required' => "Cylinders is required.",
        ]);

        //5.Effective date should be greater than current date
        $plan = CarThirdPartyCover::find($id);
        $old_effective_date = $plan->effective_date;

        $currentDate = Carbon::now();
        if($old_effective_date != $effective_date){

            $currentDate = Carbon::now();
            if($currentDate > $effective_date){
                $this->validate($request, [
                    'effective_date_greater' => 'required',
                ],[
                    'effective_date_greater.required' => "Effective date should be greater than current date",
                ]);
            }

        }

        if(!empty( $insuranceProviderId ) || !empty( $carBodyTypeId ))
        {
            //$plan = CarThirdPartyCover::find($id);
            if( $plan ) {

                $plan->insurance_provider_id = $insuranceProviderId;
                $plan->car_model_body_type_id = $carBodyTypeId;
                $plan->car_model_cylinder = $cylinders;
                $plan->min_age = $min_age;
                $plan->max_age = $max_age;
                $plan->driving_license_min_age = $driving_license_min_age;
                $plan->driving_license_max_age = $driving_license_max_age;
                $plan->status = $status;
                $plan->rate = $rate;
                $plan->effective_date = $effective_date;
                $plan->above_cylinder = $above_cylinder;
                $plan->modified_by = $adminID;
                $plan->save();

                return redirect('/admin/partners/third-party-covers')->with('success', 'Third Party Cover updated successfully');
            }else{
                return redirect('/admin/partners/third-party-covers')->with('error', 'Third Party Cover not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataThirdParty(Request $request) {

        $response = array();

        $insuranceProviderId = $request->insuranceProviderId;
        //$carBodyTypeIdVal = $request->carBodyTypeIdVal;
        $carBodyTypeIdVal = $request->carBodyTypeId;

        $hasEntry = 0;
        $body_type = array();

        if (empty($insuranceProviderId)) {
            $response['status'] =  1;
            echo json_encode($response);
            exit();   
        }

        if ($carBodyTypeIdVal ) {

            foreach ($carBodyTypeIdVal as $val) {

                $planExist = CarThirdPartyCover::where('insurance_provider_id',$insuranceProviderId)
                                                        ->where('car_model_body_type_id',$val)->first();
                if($planExist){
                    $hasEntry = 1;
                    $insurance_provider = InsuranceProvider::where('id',$planExist->insurance_provider_id)->first();
                    $car_model_body_type = CarModelBodyType::where('id',$planExist->car_model_body_type_id)->first();

                    $body_type[] = $car_model_body_type->name;

                    $response['status'] =  0;
                    $response['name_insurance_provider'] =  $insurance_provider->name;
                }
            }

            if ($hasEntry == 1){
                $implodeBodyType = implode(",", $body_type);
                $response['implodeBodyType'] =  $implodeBodyType;                
            } else {
                $response['status'] =  1;
            }
        } else {
            $response['status'] =  1;//Please Select Car Model Body Type
        }
        echo json_encode( $response );
        exit();      
    }
}
