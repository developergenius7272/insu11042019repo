<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',50);
            $table->string('description',255)->nullable();
            $table->string('template_code',255)->nullable();
            $table->string('default_subject',255)->nullable();
            $table->string('default_from_name',255)->nullable();
            $table->string('default_from_email',255)->nullable();
            $table->string('default_to_name',255)->nullable();
            $table->string('default_to_email',255)->nullable();
            $table->string('group_name',100)->nullable();
            $table->string('subscription',100)->nullable();
            $table->string('title',500)->nullable();
            $table->longText('main_html')->nullable();
            $table->string('button_title',255)->nullable();
            $table->string('button_link',255)->nullable();
            $table->string('button_color',25)->nullable();
            $table->longText('comments')->nullable();
            $table->tinyInteger('allow_instyle')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates');
    }
}
