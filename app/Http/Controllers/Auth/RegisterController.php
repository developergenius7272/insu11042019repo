<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\EmailNotification;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use App\Traits\InsureUsersTrait;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use InsureUsersTrait;
    

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/please-verify-email';
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'numeric', 'min:10'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    { 
        //dd($data);
        //if ( User::where('email', $data['email'])->get()->isEmpty() ) {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'mobile' => $data['mobile'],
            ]);
    
            $role = Role::where('name', 'user')->first();
            $user->attachRole($role);
            // Attach user with site agent
            $this->assignDefaultAgent($user);
            // 
            User::welcomeNotification($user);
        return $user;
        // } else {
        //     return response()->json(['status'=>'success','msg'=>"This Email id is already registered"]);
        // }
    }
   
    // protected function redirectTo()
    // {
    //     if ( auth()->user()->can(['agent']) ) {
    //         return 'agent/dashboard';
    //     } else {
    //         return 'user/dashboard';
    //     }
    // }
        public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        //return $this->registered($request, $user)
                        //?: redirect($this->redirectPath());
        return $this->registered($request, $user)
                        ?:response()->json(['status'=>'success','url'=>$this->redirectPath()]) ;

    }
    
}
