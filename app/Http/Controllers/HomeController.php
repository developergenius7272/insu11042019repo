<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

use App\Models\CarModel;
use App\Models\CarValue;
use App\Models\PolicyLead;
use App\Models\Country;
use App\Models\InsuranceProvider;
use App\Models\Role;
use App\Models\Upload;

use App\Models\PlanBenefit;
use App\Models\PlanBenefitMultiple;
use App\Models\BenefitMaster;
use App\Models\CarPremium;
use App\Models\CarThirdPartyCover;
use App\Models\CarModelBodyType;
use App\Models\CarModelVariant;
use App\Models\Order;
use App\Models\CrossSellingProducts;
use App\Models\AdditionalCover;
use App\Models\AgentUser;
use App\Models\AddonOrder;
use App\Models\PolicyDocumentMaster;
use App\Models\UserAgentAddonCommission;

use Illuminate\Support\Facades\DB;

// 
use Carbon\Carbon;
use Session;
use Config;
use Log;
use Image;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\CarPremium as CarPremiumResource;
use App\Http\Resources\Collections\CarPremiumCollection;
use App\Http\Resources\Collections\PlanBenefitCollection;
use Illuminate\Support\Facades\Storage;
use App\Traits\InsureUsersTrait;

class HomeController extends Controller
{
    use InsureUsersTrait;
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {

        Session::forget('policy_leads_id');
        Session::forget('user_id');

        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;

        $this->data['benefits'] = BenefitMaster::active()->get();
        
        //$this->data['current_year'] = date('Y');
        $this->data['current_year'] = Carbon::now()->year;

        $this->data['country'] = Country::all();
        return view('insurance',$this->data);
    }

    public function resetPassword($token)
    {
        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;
        //$token = bin2hex(random_bytes(10));

        $reset_password_token = $token;
        if(!empty($reset_password_token)){

            $this->data['user'] = User::where('reset_password_token',$reset_password_token)->first();

            if($this->data['user']){
                return view('change-password',$this->data);
            }
            else{
                return abort(404);
            }
        }
        else{
            return abort(404);
        }

    }

    public function resetPasswordSave(Request $request) {

        $reset_password_token = trim($request->reset_password_token);
        $newpassword = trim($request->newpassword);
        $confirmpassword = trim($request->confirmpassword);

        $user = User::where('reset_password_token',$reset_password_token)->first();

        if(isset($newpassword) && isset($confirmpassword)){
            if($newpassword == $confirmpassword){
                if( $user ) {
                    
                    $user->password = bcrypt($confirmpassword);
                    $user->reset_password_token = '';
                    $user->save();

                    return redirect('/')->with('success', 'You account has been successfully created');
                }
            }
        }
        else{
            return abort(404);
        }
    }

    /**
     * @name createAccountAjax
     * @Description use signup via ajax 
     * @return View
     *
     */
    public function createAccountAjax(Request $request){
        //dd(Auth::user());
        $userId       = 0;
        $leadId       = 0;
        $email        = null;
        $name         = null;
        $mobile       = null;
        $msg          = "";
        $status       = "";
        $returnId     = "";
        $view         = "";
        $current_year = Carbon::now()->year;
        // to skip the OTP step
        if( Auth::user() && Auth::user()->can('agent') ) {
            $returnId = "agent";
            $view = view("my-car-detail-bar",compact('current_year'))->render();
        }
        // 
        $aUserRequestData = $request->all();
        // 
        $email  = urldecode(trim($aUserRequestData["email"]));
        $name   = trim($aUserRequestData["name"]);
        $mobile = trim($aUserRequestData["mobile"]);
        // 
        $nowdate           = Carbon::now();
        $confirmation_code = str_random(4);
        if(env('OTP_TEST_ACTIVE',0) == 1){
            $confirmation_code = env('OTP_TEST','insure');
        }
        // check if email already exist
        $userExist = User::where('email',$email)->first();
    if(($user = Auth::user()) && !(Auth::user()->can(['super-admin','agent','sub-admin']))){
        $userId                                = $user->id;
        $user->mobile                          = $mobile;
        $user->name                            = $name;
        $user->email                           = $email;
        
        Session::put('policy_leads_id', $leadId);
        Session::put('user_id', $userId);
        Session::put('user_email', $email);
        Session::put('user_name', $name);
        Session::put('user_mobile', $mobile);
        $view = view("my-car-detail-bar",compact('current_year'))->render();
        return response()->json(array('action' => 1, 'html'=>$view,'id'=>$user->id));

    }else{

        if( $userExist ) {
            
            if(empty(Auth::user())){
                $userId                = $userExist->id;
                $userExist->mobile     = $mobile;
                $userExist->email      = $email;
                $userExist->mobile_otp = $confirmation_code;
                $userExist->email_otp  = $confirmation_code;
                $userExist->save();
                $msg                   ="This email id is already registered with us. Please login";
                $status = 1;
            }elseif( (!empty(Auth::user())) && (Auth::user()->can(['super-admin','agent','sub-admin'])) ){
                //dd("guru");
                $checkAgentStatus = AgentUser::where('user_id',$userExist->id)->active()->latest()->first();
                if($checkAgentStatus && $checkAgentStatus->agent_id == Auth::user()->id ){
                    $lead             = new PolicyLead();
                    $lead->email      = $email;
                    $lead->mobile     = $mobile;
                    $lead->mobile_otp = $confirmation_code;
                    $lead->email_otp  = $confirmation_code;
                    $lead->user_id    = $checkAgentStatus->user_id;
                    $lead->save();
                    $leadId           = $lead->id;
                    Session::put('agentuser_userid', $checkAgentStatus->user_id);
                    // to skip the OTP step
                    $returnId = $userExist->id;

                }else{
                    $msg ="Customer already exist in our system under other sources. Please get authorization letter from customer to change the source.";
                    $status = 2;
                }
                

            }
        } else {
            // $oUser = new User();
            // $oUser->name = $name;
            // $oUser->mobile = $mobile;
            // $oUser->email = $email;
            // $oUser->password = bcrypt($confirmation_code);
            // $oUser->mobile_otp = $confirmation_code;
            // $oUser->email_otp = $confirmation_code;
            // $oUser->save();
            // // 
            // $userId = $oUser->id;
            //dd(Session::get('policy_leads_id'));
            if( Session::has('policy_leads_id') && Session::get('policy_leads_id') > 0 ) {
                $lead = PolicyLead::find(Session::get('policy_leads_id'));
    
                if( $lead ) {
                    if(Session::has('user_id') && Session::get('user_id') > 0)
                    $leadId           = $lead->id;
                    $lead->user_id    = Session::get('user_id');
                    $lead->email      = $email;
                    $lead->mobile     = $mobile;
                    $lead->mobile_otp = $confirmation_code;
                    $lead->email_otp  = $confirmation_code;
                    $lead->save();
                }
            }else{
                    $lead             = new PolicyLead();
                    $lead->name       = $name;
                    $lead->email      = $email;
                    $lead->mobile     = $mobile;
                    $lead->mobile_otp = $confirmation_code;
                    $lead->email_otp  = $confirmation_code;
                    $lead->save();
                    $leadId           = $lead->id;

                }
            
        }
    }
        Session::put('policy_leads_id', $leadId);
        Session::put('user_id', $userId);
        Session::put('user_email', $email);
        Session::put('user_name', $name);
        Session::put('user_mobile', $mobile);

        // Send email to the user with email verification link
        $sTo = $email;
        // 
        Log::info('User Created : '.$userId." - ".$email);
        if(env('OTP_TEST_ACTIVE',0) == 1){

        }else{
            // send email - user verification
            $info = array(
                "code"=>"user_verification",
                "user_id"=>$userId,
                "to_name" => "",
                "to_email" => $sTo,
                "replace_values" => array(
                    "-textPara-"=>"Let's confirm your email address.",
                    "-code-"=>$confirmation_code,
                    "-codeType-"=>"CONFIRM EMAIL ADDRESS")
            );
            $this->emailSend($info);
        }
        //    
        return response()->json(array('action' => true,'msg'=>$msg,'status'=>$status,'id'=>$returnId,'html'=>$view));
    }

    //
    //*** Email Sender function for all emails ***
    //
    //  info array will contain email code to use with params
    //
    private function emailSend($info)
    {
        $status = false;
        if (is_array($info)) {
            if (isset($info["code"])) {
                        
                    // from name
                    $mail_test_active_send = 1;
                    if (isset($info["mail_test_active_send"])) {
                        $mail_test_active_send = $info['mail_test_active_send'];
                    } else {
                        $mail_test_active_send = 1;
                    }
                    // get user id iinternal
                    $user_id = null;
                    if (isset($info["user_id"])) {
                        $user_id = $info["user_id"];
                    }
                    // from name
                    $from_name = '';
                    if (isset($info["from_name"])) {
                        $from_name = $info['from_name'];
                    } else {
                        $from_name = 'InsureOnline';
                    }
                    // from email
                    $from_email = '';
                    if (isset($info["from_email"])) {
                        $from_email = $info['from_email'];
                    } else {
                        $from_email = 'info@baltech.in';
                    }
                    // to name
                    $to_name = '';
                    if (isset($info["to_name"])) {
                        $to_name = $info['to_name'];
                    } else {
                        $to_name = 'InsureOnline';
                    }
                    // to email
                    $to_email = '';
                    if (isset($info["to_email"])) {
                        $to_email = $info['to_email'];
                    } else {
                        $to_email = 'info@baltech.in';
                    }
                    // get subject
                    $subject = '';
                    if (isset($info["subject"])) {
                        $subject = $info['subject'];
                    } else {
                        $subject = 'Lets confirm your email address.';
                    }

                    if ($from_name == '') {
                        $from_name = env('MAIL_FROM_NAME', 'InsureOnline');
                    }
                    if ($from_email == '') {
                        $from_email = env('MAIL_FROM_EMAIL', 'info@baltech.in');
                    }
                    if ($to_email == '') {
                        $to_email = env("MAIL_ADMIN_EMAIL", "info@baltech.in");
                    }
                    if ($to_name == '') {
                        $to_name = null;
                    }

                    // create and send
                    $from = new \SendGrid\Email($from_name, $from_email);
                    $to = new \SendGrid\Email($to_name, $to_email);

                    $content = new \SendGrid\Content("text/html", " ");
                    $mail = new \SendGrid\Mail($from, $subject, $to, $content);

                    // reply to code
                    if (isset($info["reply_to"])) {
                        $reply_to = new \SendGrid\ReplyTo($info["reply_to"]);
                        $mail->setReplyTo($reply_to);
                    } else {
                        if ($from_email <> '') {
                            $reply_to = new \SendGrid\ReplyTo($from_email);
                            $mail->setReplyTo($reply_to);
                        }
                    }

                    // **************** current way!!!!!
                    //
                    if (isset($info["replace_values"])) {
                        $replace_array = $info["replace_values"];
                        foreach ($replace_array as $key => $value) {
                            if (($value == null) or ($value == '')) {
                                $value = ' ';
                            }
                            $value = (string)$value;
                            Log::info("  Email Send Keys: code : ".$info['code']." ".env('TEMPLATE_CODE', 'd-1b6f0be2202947a3b33e5f38d8d0b944')." : key : ".$key." value : ".$value);

                            $mail->personalization[0]->addSubstitution($key, $value);
                        }
                    }
                    if (isset($info["section_values"])) {
                        $section_array = $info["section_values"];
                        foreach ($section_array as $key => $value) {
                            if (($value == null) or ($value == '')) {
                                $value = ' ';
                            }
                            $value = (string)$value;
                            $mail->addSection($key, $value);
                        }
                    }
                    
                    $mail->setTemplateId(env('TEMPLATE_CODE', 'd-1b6f0be2202947a3b33e5f38d8d0b944'));
                    $apiKey = env('SENDGRID_APIKEY', false);
                    $sg = new \SendGrid($apiKey);
                    
                    try {
                        if ($mail_test_active_send == 0) {
                            // do not send this email via sendgrid but act like it did
                            //
                        } else {
                            $response = $sg->client->mail()->send()->post($mail);
                            //\Log::info("Email Send Response: email log id : ".$email_log_id." : code : ".$response->statusCode());
                            if ($response->statusCode() == 400) {
                                \Log::info("    Response: email log id : code : " . print_r($response, true));
                            }
                        }
                    } catch (Exception $e) {
                        \Log::info("Email Send Error: " . $e->getMessage());
                    }
                
            }
        }
        return $status;
    }
    // step 2
    function createAccountAjaxOTP(Request $request) {
        
        $otp = trim($request->otp);
        if( Session::has('user_id') && Session::get('user_id') > 0 ) {
                $userId = Session::get('user_id');
                // validate OTP and 
                $user = User::find($userId);
               
            if( $user ) {
                // check if OTP correct
                    if( $user->mobile_otp == $otp || $user->email_otp == $otp )
                    {
                        //reset_password_token generate - start
                        $token = bin2hex(random_bytes(10));
                        $reset_password_date = Carbon::now();
                        $user->reset_password_token = $token;
                        $user->reset_password_date = $reset_password_date;
                        $user->save();
                        //reset_password_token generate - end 


                        $current_year = Carbon::now()->year;
                        $view = view("my-car-detail-bar",compact('current_year'))->render();
                        //return response()->json(['html'=>$view]);

                        return response()->json(array('action' => 1, 'html'=>$view));
                    }else{
                        return response()->json(array('action' => 0, 'error' => 'Incorrect OTP'));
                    }
            }else {
                return response()->json(array('action' => 0, 'error' => 'No user found'));
            }
        }elseif(Session::has('policy_leads_id') && Session::get('policy_leads_id') > 0 ){
            $lead = PolicyLead::find(Session::get('policy_leads_id'));
            if($lead){
                if( $lead->mobile_otp == $otp || $lead->email_otp == $otp )
                    {
                        //reset_password_token generate - start
                        // $token = bin2hex(random_bytes(10));
                        // $reset_password_date = Carbon::now();
                        // $user->reset_password_token = $token;
                        // $user->reset_password_date = $reset_password_date;
                        // $user->save();
                        //reset_password_token generate - end 


                        $current_year = Carbon::now()->year;
                        $view = view("my-car-detail-bar",compact('current_year'))->render();
                        //return response()->json(['html'=>$view]);

                        return response()->json(array('action' => 1, 'html'=>$view));
                    }else{
                        return response()->json(array('action' => 0, 'error' => 'Incorrect OTP'));
                    }
            }

        }else{
            return response()->json(array('action' => 0, 'error' => 'Session expired'));
        }
    }

    function getCarDetailsView() {

    }
    // step 3
    function getCarMakes(Request $request) {
        $modelYear = $request->year;
        $carMakes = CarValue::select('car_makes.name','car_make_id')
                    ->join('car_makes','car_makes.id','car_values.car_make_id')
                    ->where('manufacture_year',$modelYear)
                    ->groupBy('car_make_id','car_makes.name')
                    ->get();
        return response()->json(array('action' => 1, 'makes' => $carMakes));
    }
    // 
    public function getCarModels(Request $request) {
        $modelYear = $request->year;
        $makeId = $request->make;
        $carModels = CarValue::select('car_models.name','car_model_id')
                        ->join('car_models','car_models.id','car_values.car_model_id')
                        ->where('manufacture_year',$modelYear)
                        ->where('car_values.car_make_id',$makeId)
                        ->groupBy('car_model_id','car_models.name')
                        ->get();
        return response()->json(array('action' => 1, 'models' => $carModels));
    }
    // 
    public function getCarVariants(Request $request) {
        $year = $request->year;
        $make = $request->make;
        $model = $request->model;
        // 
        $carVariants = CarValue::select('car_model_variants.name','car_model_variant_id')
                    ->join('car_model_variants','car_model_variants.id','car_values.car_model_variant_id')
                    ->where('manufacture_year',$year)
                    ->where('car_values.car_make_id',$make)
                    ->where('car_values.car_model_id',$model)
                    ->groupBy('car_model_variant_id','car_model_variants.name')
                    ->get();
        return response()->json(array('action' => 1, 'variants' => $carVariants));
    }

    public function getCarValues(Request $request) {
        $year = $request->year;
        $make = $request->make;
        $model = $request->model;
        $variant = $request->variant;
        // 
        $carValues = CarValue::select('car_values.values','car_values.min_value','car_values.max_value')
                    ->join('car_model_variants','car_model_variants.id','car_values.car_model_variant_id')
                    ->where('manufacture_year',$year)
                    ->where('car_values.car_make_id',$make)
                    ->where('car_values.car_model_id',$model)
                    ->where('car_values.car_model_variant_id',$variant)
                    // ->groupBy('car_model_variant_id','car_model_variants.name')
                    ->first();
                    Session::put('car_values_deatils', $carValues);
        return response()->json(array('action' => 1, 'values' => $carValues));
    }

    public function saveCarLeads(Request $request) {
        if( !Session::has('user_id') ){
            return response()->json(['action' => 0]);
        }
           // dd(Session::get('user_id'));
        $country = Country::all();
        Session::get('policy_leads_id');
        // 
        if(Session::has('agentuser_userid') && Session::get('agentuser_userid') > 0 ){
            $user_id = Session::get('agentuser_userid');
        }else{
            $user_id = Session::get('user_id');
        }      
        $year = $request->year;
        $make = $request->make;
        $model = $request->model;
        $variant = $request->variant;
        $carValue = $request->carValue;
        $isNewCar = $request->isNewCar;
        $carRegistrationDate = $request->carRegistrationDate;
        $city_of_registration = $request->city_of_registration;
        
        //check Car Registration date not less than year (if old car)
        if($isNewCar == 0){
            $carRegDate = Carbon::createFromFormat('d/m/Y', $carRegistrationDate);
            $carRegYear = Carbon::parse($carRegDate)->year;

            if($carRegYear < $year ){
                return response()->json(['action' => 0, 'error' => 1, 'error_code' => 'carRegYear']);
            }
        }

        if( !empty($carRegistrationDate) ) {
            // date format should be in DMY format
            $carRegistrationDate = sqlDateDMY($carRegistrationDate,'/');
        }
        // 

        // 
        if( Session::has('policy_leads_id') && Session::get('policy_leads_id') > 0 ) {
            
            $lead = PolicyLead::find(Session::get('policy_leads_id'));

            if( $lead ) {

                $lead->user_id = $user_id;
                $lead->car_model_year = $year;
                $lead->car_make_id = $make;
                $lead->car_model_id = $model;
                $lead->car_model_variant_id = $variant;
                $lead->car_value = $carValue;
                $lead->is_car_new = $isNewCar;
                $lead->car_registration_date = $carRegistrationDate;
                $lead->city_of_registration = $city_of_registration;
                $lead->save();

                $view = view("about-me-bar",compact('country'))->render();
                //return response()->json(['html'=>$view]);

                return response()->json(array('action' => 1, 'lead' => $lead->id, 'html'=>$view));
            }
        }
        else{
            
            $lead = new PolicyLead();
            $lead->user_id = $user_id;
            $lead->car_model_year = $year;
            $lead->car_make_id = $make;
            $lead->car_model_id = $model;
            $lead->car_model_variant_id = $variant;
            $lead->car_value = $carValue;
            $lead->is_car_new = $isNewCar;
            $lead->car_registration_date = $carRegistrationDate;
            $lead->city_of_registration = $city_of_registration;
            // 
            $lead->save();
            // 
            Session::put('policy_leads_id', $lead->id);

            $view = view("about-me-bar",compact('country'))->render();
            //return response()->json(['html'=>$view]);

            return response()->json(array('action' => 1, 'lead' => $lead->id, 'html'=>$view));
        }


    }

    public function saveMoreAboutMe(Request $request) {
        if( !Session::has('user_id') ){
            return response()->json(['action' => 0]);
        }
        // 
        $user_id = Session::get('user_id');
        $policy_leads_id = Session::get('policy_leads_id');

        $isNonGCC = $request->isNonGCC;
        //$isNonGCC = 1;
        $nationality = $request->nationality;
        $country = $request->country;
        $driving_experience = $request->driving_experience;
        $driving_in_uae = $request->driving_in_uae;
        $dob = $request->dob;
        
        $carVariantID = $request->carVariantID;
        $carValueInput = $request->carValueInput;
        $carRegistrationDate = $request->carRegistrationDate;
        session(['carVariantID' => $request->carVariantID,'carValueInput' => $request->carValueInput,'carRegistrationDate' => $request->carRegistrationDate,'dob' => $request->dob,'driving_exp' => $request->driving_in_uae]);

        //check dob less than 18 years
        $now = Carbon::now();
        $dobCheck = Carbon::createFromFormat('d/m/Y', $dob);
        //echo $age = Carbon::parse($dob)->age;die;
        $getAge = $dobCheck->diffInYears($now);

        if($getAge < 18){
            return response()->json(['action' => 0, 'error' => 1, 'error_code' => 'AGE']);
        }


        if( !empty($dob) ) {
            // date format should be in DMY format
            $dob = sqlDateDMY($dob,'/');
        }
        
        // 
        $lead = PolicyLead::find($policy_leads_id);

        if( $lead ) {

            $lead->is_nongcc = $isNonGCC;
            $lead->nationality = $nationality;
            $lead->country = $country;
            $lead->driving_experience = $driving_experience;
            $lead->driving_in_uae = $driving_in_uae;
            $lead->dob = $dob;
            $lead->save();

            //$current_year = Carbon::now()->year;
            //$view = view("renewal-details",compact('current_year'))->render();
            $view = view("renewal-details")->render();

            return response()->json(array('action' => 1, 'lead' => $lead->id, 'html'=>$view));
        }
    }

    public function saveRenewalDetails(Request $request) {
        if( !Session::has('user_id') ){
            return response()->json(['action' => 0]);
        }
        // 
        $user_id = Session::get('user_id');
        $policy_leads_id = Session::get('policy_leads_id');

        $is_thirdpartyliablity = $request->isThirdpartyliablityYes;
        //$is_thirdpartyliablity = 1;
        $isAgencyrepairYes = $request->isAgencyrepairYes;
        $isPolicyexpiredYes = $request->isPolicyexpiredYes;
        $register = $request->register;
        $isClaiminsuranceYes = $request->isClaiminsuranceYes;
        $isClaimcertificateYes = $request->isClaimcertificateYes;
        $claimCertificateYear = $request->claimCertificateYear;

        if( !empty($register) && !is_null($register)) {
            $register = Carbon::createFromFormat('d/m/Y', $register);
            $register = $register->toDateString();
            $nowdate = Carbon::now()->toDateString();
            //echo $nowdate->diffInDays($register);die;

            if($nowdate > $register){
                return response()->json(['action' => 0, 'error' => 1, 'error_code' => 'register']);
            }

            // date format should be in DMY format
            //echo $register = sqlDateDMY($register,'/');

        }

        // 
        $lead = PolicyLead::find($policy_leads_id);

        if( $lead ) {

            $lead->is_thirdpartyliablity = $is_thirdpartyliablity;
            $lead->is_agencyrepair = $isAgencyrepairYes;
            $lead->is_policyexpired = $isPolicyexpiredYes;
            $lead->register = $register;
            $lead->is_claiminsurance = $isClaiminsuranceYes;
            $lead->is_claimcertificate = $isClaimcertificateYes;
            $lead->claimCertificateYear = $claimCertificateYear;
            $lead->save();

            return response()->json(array('action' => 1, 'lead' => $lead->id));
        }
    }

    public function getFilterResult1(Request $request) {
           
        if( !Session::has('user_id') ){
            return response()->json(['action' => 0]);
        }

        if ( $request->ajax() ) {
        $benefitMasters = null;

        $carVariantID = $request->carVariantID;
        $carValueInput = $request->carValueInput;
        $dob = $request->dob;
        $carRegistrationDate = $request->carRegistrationDate;
        $dob = Carbon::createFromFormat('d/m/Y', $dob);
        $age = Carbon::parse($dob)->age;

        //Get Registration date
        $now = Carbon::now();
        $carRegDate = Carbon::createFromFormat('d/m/Y', $carRegistrationDate);
        $getMonths = $carRegDate->diffInMonths($now);

        $carVariant = CarModelVariant::where('id',$carVariantID)->first();
        $sub_type = $carVariant->sub_type;
        $no_of_seats = $carVariant->no_of_seats;

            $planBenefits = BenefitMaster::select('benefit_masters.id as benefit_id', 'benefit_masters.name as benefit_name', 'plan_benefits.plan_type', 'plan_benefits.sub_type as plan_benefits_sub_type', 'benefit_masters.for_each_passenger', 'plan_benefits.id as plan_benefits_id', 'insurance_providers.id as insurance_id', 'insurance_providers.name as partner_name', 'car_premiums.id AS car_premium_id', 'plan_benefits.included', 'plan_benefits.percentage_or_amount', 'plan_benefits.amount', 'plan_benefits.single_or_multiple', 'plan_benefits.effective_date', 'plan_benefits.description', 'car_premiums.car_model_cylinder', 'car_premiums.available_for_months',

                    DB::raw("case plan_benefits.included
                        when 1 then 'yes'
                        when 0 then 'no'
                        else 'no' end as 'is_include',
                        plan_benefits.single_or_multiple,
                    CASE plan_benefits.single_or_multiple
                        when 1 then 
                        (select group_concat(plan_benefits_multiple.amount,'|#|',plan_benefits_multiple.feature_description) as tect from plan_benefits_multiple 
                            where plan_benefits_multiple.plan_benefits_id =  plan_benefits.id)
                        else '' end as multiple_amount_description")) 

                    ->addSelect('car_premiums.*')
                    ->addSelect('insurance_providers.has_logo')
                    ->addSelect('insurance_providers.name as partner_name')
                    ->addSelect('car_model_body_types.name as car_body_name')

                    ->leftJoin('plan_benefits','plan_benefits.benefit_masters_id', 'benefit_masters.id')
                    ->leftJoin('insurance_providers','insurance_providers.id', 'plan_benefits.insurance_providers_id')
                    ->leftJoin('car_premiums','car_premiums.insurance_provider_id', 'insurance_providers.id')
                    ->leftJoin('car_model_body_types','car_model_body_types.id', 'car_premiums.car_model_body_type_id')

                    ->where('plan_benefits.effective_date',DB::raw("(select max(pb.effective_date) from plan_benefits pb
                                                            where pb.insurance_providers_id = plan_benefits.insurance_providers_id AND pb.sub_type = plan_benefits.sub_type)"))
                    
                    //Vehicle Body Type checking (car_model_variants and car_premiums)
                    ->where('car_premiums.car_model_body_type_id',$carVariant->car_model_body_type_id)

                    //Car Value range checking (CarValueInput from car_premium)
                    ->whereRaw(DB::raw("CASE 
                                    WHEN 
                                    car_premiums.car_model_max_value > 0 
                                        THEN $carValueInput BETWEEN car_premiums.car_model_min_value AND car_premiums.car_model_max_value
                                         ELSE TRUE END"))

                    //Car Value range checking (CarValueInput from car_premium)
                    ->whereRaw(DB::raw("CASE 
                                    WHEN 
                                    car_premiums.max_age > 0 
                                        THEN $age BETWEEN car_premiums.min_age AND car_premiums.max_age
                                         ELSE TRUE END"))

                    ->where('car_premiums.status',1)
                    ->where('insurance_providers.status',1)
                    //->orderBy('insurance_providers.recommended_partner','DESC')
                    ->get();

        $matrixData  = array();

        if(!empty($planBenefits)){

            foreach ($planBenefits as $key => $valplanBenefit) {

                $insurance_id = $valplanBenefit['insurance_id'];
                $pb_sub_type = $valplanBenefit['plan_benefits_sub_type'];

                $min_premium_agency_rate = (float)$valplanBenefit['min_premium_agency_rate'];
                $min_premium_agency = (float)$valplanBenefit['min_premium_agency'];
                $min_premium_nonagency_rate = (float)$valplanBenefit['min_premium_nonagency_rate'];
                $min_premium_nonagency = (float)$valplanBenefit['min_premium_nonagency'];
                $available_for_months = $valplanBenefit['available_for_months'];
                $percentage_or_amount = $valplanBenefit['percentage_or_amount'];

                $priceAED = 0;
                $getMonths = $getMonths; //Allowing 2 more months
                //$getMonths = 12;

                // 1. Year Month calculation of agency/non-agency rate (to be taken)
                $agencyRateAvailableMonths = $available_for_months;
                $paddingMonths = 0;
                // 

                
                $matrixData[$insurance_id][$pb_sub_type]['agency']['priceAEDAgencyRaw'] = 0;
                
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['priceAEDNonAgencyRaw'] = 0;


                //Agency Array
                //Agency Array
                //Agency Array
                
                if( ($agencyRateAvailableMonths > 0) && ($agencyRateAvailableMonths + $paddingMonths) >= $getMonths ) {

                    $matrixData[$insurance_id][$pb_sub_type]['agency']['partner_name'] = $valplanBenefit['partner_name'];
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['insurance_id'] = $valplanBenefit['insurance_id'];
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['has_logo'] = $valplanBenefit['has_logo'];
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['pb_sub_type'] = $pb_sub_type;
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['min_premium_agency_rate'] = $min_premium_agency_rate;
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['min_premium_agency'] = $min_premium_agency;
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['min_premium_nonagency_rate'] = $min_premium_nonagency_rate;
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['min_premium_nonagency'] = $min_premium_nonagency;
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['available_for_months'] = $available_for_months;

                    //2. Price AED calculation bcos percentage_or_amount calculation is based on it.
                    //Calculation of Agency rate
                    $priceAEDAmount = $carValueInput * $min_premium_agency_rate / 100;
                    
                    if($priceAEDAmount > $min_premium_agency){
                        $priceAED = $priceAEDAmount;
                    }
                    else{
                        $priceAED = $min_premium_agency;
                    }
                    
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['priceAEDAgencyRaw'] = $priceAED;


                    $amountAgency = 0;
                    //3. if percentage (in percentage_or_amount) in plan benefits
                    if ($percentage_or_amount == 1){

                        //$valplanBenefit['amount'] = $priceAED * (float)$valplanBenefit['amount'] / 100;
                        $amountAgency = $priceAED * (float)$valplanBenefit['amount'] / 100;
                    }
                    else{
                        $amountAgency = $valplanBenefit['amount'];
                    }   

                    //4. match (PERSONAL ACCIDENT BENEFIT – PASSENGERS)
                    //$no_of_seats = 5;
                    if(!empty($valplanBenefit['for_each_passenger']) && $valplanBenefit['for_each_passenger'] == 1){
                        $driverCover = 1;
                        $passengerCover = $no_of_seats - $driverCover;
                        //$valplanBenefit['amount'] = $valplanBenefit['amount'] * $passengerCover;
                        $amountAgency = $amountAgency * $passengerCover;
                    }

                    // partner benefits
                    $matrixData[$insurance_id][$pb_sub_type]['agency']['benefits'][$valplanBenefit['benefit_id']] = [
                        'benefit_name' => $valplanBenefit['benefit_name'],
                        'benefit_id' => $valplanBenefit['benefit_id'],
                        'plan_benefits_id' => $valplanBenefit['plan_benefits_id'],
                        'pb_sub_type' => $pb_sub_type,
                        'included' => $valplanBenefit['included'],
                        'percentage_or_amount' => $valplanBenefit['percentage_or_amount'],
                        //'amount' => number_format($amountAgency,2),
                        'amount' => $amountAgency,
                        'amountRaw' => (float)$amountAgency,
                        'description' => $valplanBenefit['description'],
                        'for_each_passenger' => $valplanBenefit['for_each_passenger']

                    ];
                 }
                
                //Non Agency Array
                //Non Agency Array
                //Non Agency Array

                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['partner_name'] = $valplanBenefit['partner_name'];
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['insurance_id'] = $valplanBenefit['insurance_id'];
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['has_logo'] = $valplanBenefit['has_logo'];
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['pb_sub_type'] = $pb_sub_type;
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['min_premium_agency_rate'] = $min_premium_agency_rate;
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['min_premium_agency'] = $min_premium_agency;
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['min_premium_nonagency_rate'] = $min_premium_nonagency_rate;
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['min_premium_nonagency'] = $min_premium_nonagency;
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['available_for_months'] = $available_for_months;


                $priceAED = 0;
                $priceAEDAmount = 0;
                //2. Price AED calculation bcos percentage_or_amount calculation is based on it.
                //Calculation of None Agency rate
                $priceAEDAmount = $carValueInput * $min_premium_nonagency_rate / 100;
                
                if($priceAEDAmount > $min_premium_nonagency){
                    $priceAED = $priceAEDAmount;
                }
                else{
                    $priceAED = $min_premium_nonagency;
                }
                
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['priceAEDNonAgencyRaw'] = $priceAED;

                
                $amountNonAgency = 0;
                //3. if percentage (in percentage_or_amount) in plan benefits
                if ($percentage_or_amount == 1){

                    $amountNonAgency = $priceAED * (float)$valplanBenefit['amount'] / 100;
                }
                else{
                    $amountNonAgency = $valplanBenefit['amount'];
                }
                
                //4. match (PERSONAL ACCIDENT BENEFIT – PASSENGERS)
                //$no_of_seats = 5;
                if(!empty($valplanBenefit['for_each_passenger']) && $valplanBenefit['for_each_passenger'] == 1){
                    $driverCover = 1;
                    $passengerCover = $no_of_seats - $driverCover;
                    //$valplanBenefit['amount'] = $valplanBenefit['amount'] * $passengerCover;
                    $amountNonAgency = $amountNonAgency * $passengerCover;
                }

                // partner benefits
                $matrixData[$insurance_id][$pb_sub_type]['nonagency']['benefits'][$valplanBenefit['benefit_id']] = [
                    'benefit_name' => $valplanBenefit['benefit_name'],
                    'benefit_id' => $valplanBenefit['benefit_id'],
                    'plan_benefits_id' => $valplanBenefit['plan_benefits_id'],
                    'pb_sub_type' => $pb_sub_type,
                    'included' => $valplanBenefit['included'],
                    'percentage_or_amount' => $valplanBenefit['percentage_or_amount'],
                    //'amount' => number_format($amountNonAgency,2),
                    'amount' => $amountNonAgency,
                    'amountRaw' => (float)$amountNonAgency,
                    'description' => $valplanBenefit['description'],
                    'for_each_passenger' => $valplanBenefit['for_each_passenger']
                ];
                
            }
        }
        
        //$mm = ['action' => 1,  'planBenefits' => $planBenefits, 'matrixData' => $matrixData, 'benefitMasters' => $benefitMasters,'input_value' => $carValueInput];
        $policyDetail = $this->policyDetails($mm);
        return json_encode(['html' => $policyDetail ]);
    }
    }
    public function policyDetails($policyDetail){
       $partners = InsuranceProvider::all();
       return view('policydetail',compact('policyDetail', 'partners'))->render();
    }

    public function getFilterResult(Request $request) {
        $carVariantID = $request->carVariantID;
        $carValueInput = $request->carValueInput;
        $dob = $request->dob;
        $carRegistrationDate = $request->carRegistrationDate;
        $dob = Carbon::createFromFormat('d/m/Y', $dob);
        $age = Carbon::parse($dob)->age;
           
        //Get Registration date
        $now = Carbon::now();
        $carRegDate = Carbon::createFromFormat('d/m/Y', $carRegistrationDate);
        $getMonths = $carRegDate->diffInMonths($now);
        //$getMonths = Carbon::parse($carRegDate)->age;
        $year = Carbon::parse($carRegDate)->age;
        
        //Get Driver experience In UAE
        $driving_exp = $request->drivingInUae;

        $benefit_name = BenefitMaster::active()->orderBy('name')->get();
        $list_benefit = $benefit_name->pluck('id')->toArray();
       
        $benefit_count = $benefit_name->count();
        $carVariant = CarModelVariant::where('id',$carVariantID)->first();
       
        $sub_type = $carVariant->sub_type;
        $no_of_seats = $carVariant->no_of_seats;
        $no_of_cylinders = $carVariant->cylinders;
        $car_model_body_type_id = $carVariant->car_model_body_type_id;
        
        $carPremiumsRecommended = CarPremium::query();
        $carPremiumsNonRecommended = CarPremium::query();
        $carThirdPartyPremiums = CarThirdPartyCover::query();

        //mendatory filters here for premium
        $carPremiumsRecommended = $carPremiumsRecommended->where('car_model_body_type_id', '=', $car_model_body_type_id)->where(function($q) use($carValueInput){
            $q->where('car_model_min_value', '<', $carValueInput)
                    ->where('car_model_max_value', '>', $carValueInput)->orWhereNull('car_model_max_value');});
        $carPremiumsRecommended = $carPremiumsRecommended->active()->age($age)->whereHas('recomendedInsuranceProvider')->effectiveDate()->get();

        $carPremiumsNonRecommended = $carPremiumsNonRecommended->where('car_model_body_type_id', '=', $car_model_body_type_id)->where(function($q) use($carValueInput){
            $q->where('car_model_min_value', '<', $carValueInput)
                    ->where('car_model_max_value', '>', $carValueInput)->orWhereNull('car_model_max_value');});
        $carPremiumsNonRecommended = $carPremiumsNonRecommended->active()->age($age)->whereHas('NonRecomendedInsuranceProvider')->effectiveDate()->get();

        $carPremium = $carPremiumsRecommended->merge($carPremiumsNonRecommended);
        
        //third party premium
        $carThirdPartyPremium = $carThirdPartyPremiums->where('car_model_body_type_id', '=', $car_model_body_type_id);                                              // ->where('car_model_min_value', '<', $carValueInput)
                                                        //->where('car_model_max_value', '>', $carValueInput);
        $carThirdPartyPremium = $carThirdPartyPremiums->active()->age($age)->drivingExperience($driving_exp)->cylinderRange($no_of_cylinders)->effectiveDate()->get();
        //excessValue($carValue,$body_type,$insurance_id,$age)
        //$val=$carThirdPartyPremium[0]->excessValue(39500,25)->pluck('excesValue');
        //return $val;
        //return sizeof($carPremium[3]->insuranceProvider->planBenefits);
        //$car_premium_details = $carPremium;
        $car_premium_details = $carPremium;
        //return $carPremium;
        //$test = $carPremium[0]->insuranceProvider->planBenefitsPartners[0];//->planBenefits;
         //return $test->effectiveDate();
        //$partners = InsuranceProvider::all();
        //return Redirect::route('premiumplans')->with(compact('car_premium_details', 'partners', 'carValueInput', 'getMonths','carThirdPartyPremium','age','no_of_seats','benefit_name','benefit_count','year'));
       //return View::make('testpolicy', compact('car_premium_details', 'partners', 'carValueInput', 'getMonths','carThirdPartyPremium','age','no_of_seats','benefit_name','benefit_count','year'));
        $car = $this->policyDetails1($car_premium_details, $carValueInput, $getMonths,$carThirdPartyPremium,$age,$no_of_seats,$benefit_name,$benefit_count,$year);
        return json_encode(['html' =>  $car ]);
    }

    public function policyDetails1($car_premium_details, $carValueInput, $getMonths,$carThirdPartyPremium,$age,$no_of_seats,$benefit_name,$benefit_count,$year){
       
        $partners = InsuranceProvider::all();
      // return Redirect::route('premiumplans')->with(compact('car_premium_details', 'partners', 'carValueInput', 'getMonths','carThirdPartyPremium','age','no_of_seats','benefit_name','benefit_count','year'));
        return view('testpolicy',compact('car_premium_details', 'partners', 'carValueInput', 'getMonths','carThirdPartyPremium','age','no_of_seats','benefit_name','benefit_count','year'))->render();
    }


    public function getPremium(Request $request)
    {
        $data                = $request->session()->all();
        $carMinValue         = $data['car_values_deatils']['min_value'];
        $carMaxValue         = $data['car_values_deatils']['max_value'];
        $carVariantID        = $data['carVariantID'];
        $carValueInput       = $data['carValueInput'];
        $dob                 = $data['dob'];
        $carRegistrationDate = $data['carRegistrationDate'];
        $dob                 = Carbon::createFromFormat('d/m/Y', $dob);
        $age                 = Carbon::parse($dob)->age;

        //Get Registration date
        $now        = Carbon::now();
        $carRegDate = Carbon::createFromFormat('d/m/Y', $carRegistrationDate);
        $getMonths  = $carRegDate->diffInMonths($now);
        //$getMonths = Carbon::parse($carRegDate)->age;
        $year = Carbon::parse($carRegDate)->age;

        //Get Driver experience In UAE
        $driving_exp = $data['driving_exp'];

        $benefit_name = BenefitMaster::active()->orderBy('name')->get();
        $list_benefit = $benefit_name->pluck('id')->toArray();

        $benefit_count = $benefit_name->count();
        // 
        $carVariant    = CarModelVariant::where('id', $carVariantID)->first();

        $sub_type               = $carVariant->sub_type;
        $no_of_seats            = $carVariant->no_of_seats;
        $no_of_cylinders        = $carVariant->cylinders;
        $car_model_body_type_id = $carVariant->car_model_body_type_id;

        $carPremiumsRecommended    = CarPremium::query();
        $carPremiumsNonRecommended = CarPremium::query();
        $carThirdPartyPremiums     = CarThirdPartyCover::query();

        //mendatory filters here for premium
        $carPremiumsRecommended = $carPremiumsRecommended->where('car_model_body_type_id', '=', $car_model_body_type_id)->where(function ($q) use ($carValueInput) {
            $q->where('car_model_min_value', '<', $carValueInput)
                ->where('car_model_max_value', '>', $carValueInput)->orWhereNull('car_model_max_value');});
        $carPremiumsRecommended = $carPremiumsRecommended->active()->age($age)->whereHas('recomendedInsuranceProvider')->effectiveDate()->get();

        $carPremiumsNonRecommended = $carPremiumsNonRecommended->where('car_model_body_type_id', '=', $car_model_body_type_id)->where(function ($q) use ($carValueInput) {
            $q->where('car_model_min_value', '<', $carValueInput)
                ->where('car_model_max_value', '>', $carValueInput)->orWhereNull('car_model_max_value');});
        $carPremiumsNonRecommended = $carPremiumsNonRecommended->active()->age($age)->whereHas('NonRecomendedInsuranceProvider')->effectiveDate()->get();

        $carPremium = $carPremiumsRecommended->merge($carPremiumsNonRecommended);

        //third party premium
        $carThirdPartyPremium = $carThirdPartyPremiums->where('car_model_body_type_id', '=', $car_model_body_type_id); // ->where('car_model_min_value', '<', $carValueInput)
        //->where('car_model_max_value', '>', $carValueInput);
        $carThirdPartyPremium = $carThirdPartyPremiums->active()->age($age)->drivingExperience($driving_exp)->cylinderRange($no_of_cylinders)->effectiveDate()->get();

        $car_premium_details = $carPremium;
        $partners            = InsuranceProvider::all();
        return view('testpolicy', compact('car_premium_details', 'partners', 'carValueInput', 'getMonths', 'carThirdPartyPremium', 'age', 'no_of_seats', 'benefit_name', 'benefit_count', 'year', 'carMinValue', 'carMaxValue'));
    }

    // post route
    public function paymentdata(Request $request){
        $basic_premium_cal = 0;
        $total_premium_cal = 0;
        $excessValue       = 0;
        $partnerBenefit    = [];
        $excess           = [];
        // 
        $get_premium       = $request->premium;
        $policy_id         = $request->policy;
        $policy_type       = $request->type_value;
        $selected_benefits = $request->second_array;
        $selected_benefits_details = [];
        // 
        $carVariantID        = $request->session()->get('carVariantID');
        $carVariant          = CarModelVariant::where('id',$carVariantID)->first();
        $carValueInput       = $request->session()->get('carValueInput');
        $carRegistrationDate = $request->session()->get('carRegistrationDate');
        $dob = $request->session()->get('dob');
        $dob = Carbon::createFromFormat('d/m/Y', $dob);
        $age = Carbon::parse($dob)->age;
        // 
        $now                 = Carbon::now();
        $carRegDate          = Carbon::createFromFormat('d/m/Y', $carRegistrationDate);
        $getMonths           = $carRegDate->diffInMonths($now);
        // 
        // agency and non-agency
        if($policy_type == 1 || $policy_type == 0) {
            $car_premium_detail = CarPremium::find($policy_id);
            if(!$car_premium_detail){
                // error
            }else{
                $partnerBenefit = $car_premium_detail->insuranceProvider->planBenefitsPartners->where('sub_type',$car_premium_detail->car_model_sub_type);
                $excess = $car_premium_detail->excessValue($carValueInput,$age,$carVariant->no_of_seats)->pluck('excesValue');
            }
        }else{
            // thirdparty
            $car_thirdparty_premium_detail = CarThirdPartyCover::find($policy_id);
            if(!$car_thirdparty_premium_detail) {
                // error
            }else{
                $partnerBenefit = $car_thirdparty_premium_detail->insuranceProvider->thirdPartyplanBenefitsPartner;
                $excess = $car_thirdparty_premium_detail->excessValue($carValueInput,$age,$carVariant->no_of_seats)->pluck('excesValue');

            }
        }
        // re-calculate premium
        
        $checkStatus  = "no"; 
        $extendedRate = 0;
        // 
        if($policy_type == 0) {
            // 
            if(isset($car_premium_detail->available_for_months) && ($getMonths > ($car_premium_detail->available_for_months))){
                
                $available_months = ceil($getMonths/12);
                $check            = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('year_number');
                $loadingRate      = $car_premium_detail->carPremiumAgencyAvailableForYear->where('year_number',$available_months)->pluck('rate');
                if(!$check->isEmpty()) {
                    $checkStatus = "yes"; 
                    $extendedRate = $loadingRate[0];
                }
            }
            $basic_premium_cal = $car_premium_detail->agencyPremiumRate($carValueInput,$checkStatus,$extendedRate);
        }
        // 
        if($policy_type == 1) {
            $basic_premium_cal = $car_premium_detail->nonagencyPremiumRate($carValueInput);
        }
        // 
        if($policy_type == 2) {
            $basic_premium_cal = $car_thirdparty_premium_detail->thirdPartyPremiumRate($carValueInput);
        }

        // if(!($basic_premium_cal == $get_premium)) {
        //     Log::info('$get_premium = ' . $get_premium . ' but $premium_cal = '. $basic_premium_cal);
        //     // error
        // }
        //  $excess

        if(!$excess->isEmpty()) {
             $excessValue = $excess->first();
        }
        // 
        $total_premium_cal += $basic_premium_cal;
        // 
        if(count($selected_benefits) && !$partnerBenefit->isEmpty()) {

            $benefits            = BenefitMaster::active()->get();
            $planPartnerBenefits = $partnerBenefit->first()->planBenefits->all();
            // 
            foreach($planPartnerBenefits as $plan){

                if($benefitDetail = $benefits->where('id',$plan->benefit_masters_id)->first()) {
                    // // if(array_key_exists($plan->benefit_masters_id, $selected_benefits)) {

                        if(isset($plan->description) && ($plan->included == 2)){
                            $selected_benefits[$plan->benefit_masters_id] = $plan->description;
                            // 
                            $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => $plan->description, 'selected_value_type' => 'text','comment' => 'include with description'];

                        }elseif($plan->included == 2){
                            $selected_benefits[$plan->benefit_masters_id] = "yes";
                            // 
                            $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => "yes", 'selected_value_type' => 'text','comment' => 'include'];
                            

                        }elseif(isset($plan->amount) && ($plan->included == 1) && ($plan->single_or_multiple != 2)) {
                            $benefitAmount = $plan->amount($basic_premium_cal,$carVariant->no_of_seats,$plan->benefit_masters_id);
                            if($plan->required == 1) {
                                $total_premium_cal += $benefitAmount;
                                $selected_benefits[$plan->benefit_masters_id] = $benefitAmount;
                                // 
                                $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => $benefitAmount, 'selected_value_type' => 'amount','comment' => 'required'];

                            }elseif(array_key_exists($plan->benefit_masters_id, $selected_benefits)) {
                                $total_premium_cal += $benefitAmount;
                                $selected_benefits[$plan->benefit_masters_id] = $benefitAmount;
                                // 
                                $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => $benefitAmount, 'selected_value_type' => 'amount','comment' => 'checked'];
                            }

                        }elseif( ($plan->single_or_multiple == 2) && ($plan->included  == 1) ){
                            
                            if( array_key_exists($plan->benefit_masters_id, $selected_benefits) ) {
                                $selectedValueFromMultiple =  $selected_benefits[$plan->benefit_masters_id];

                                if($selectedValueFromMultiple > 0) {

                                    $selected_benefits[$plan->benefit_masters_id] = "NOT SELECTED";
                                    // 
                                    $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => "NOT SELECTED", 'selected_value_type' => 'text','comment' => 'multiple not found'];

                                    foreach($plan->planBenefitMultiple as $planMultiple){

                                        $benefitAmount = $planMultiple->amount($basic_premium_cal,$carVariant->no_of_seats,$plan->benefit_masters_id);
                                        
                                        // if($benefitAmount == $selectedValueFromMultiple){
                                        if($selectedValueFromMultiple == $planMultiple->id){
                                            $total_premium_cal += $benefitAmount;
                                            $selected_benefits[$plan->benefit_masters_id] = $benefitAmount;
                                            // 
                                            // $detail = $benefitDetail->with(['plan_benefit_multiple' => function($q) use ($planMultiple) {
                                            //     $q->where('id',$planMultiple->id);
                                            // }])->get();
                                            $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => $benefitAmount . ' - '.$planMultiple->feature_description, 'selected_value_type' => 'amount','comment' => 'multiple of row plan_benefit_multiple.id='.$planMultiple->id];

                                        }
                                    }
                                }else{
                                    $selected_benefits[$plan->benefit_masters_id] = "NOT SELECTED";
                                    // 
                                    $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => "NOT SELECTED", 'selected_value_type' => 'text','comment' => 'multiple not selected'];
                                }
                            }else{
                                $selected_benefits[$plan->benefit_masters_id] = "NOT SELECTED";
                                // 
                                $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => "NOT SELECTED", 'selected_value_type' => 'text','comment' => 'multiple removed'];
                            }

                        }else{
                            $selected_benefits[$plan->benefit_masters_id] = "no";
                            $selected_benefits_details[$plan->benefit_masters_id] = ['benefit' => $benefitDetail->toArray(),'plan_benefit' => $plan->toArray(), 'selected_value' => "NO", 'selected_value_type' => 'text','comment' => 'not include'];
                        }

                        if(isset($selected_benefits_details[$plan->benefit_masters_id])){
                            $selected_benefits_details[$plan->benefit_masters_id]['CURRENCY_CODE'] = env('CURRENCY_CODE','USD');
                        }
                    // // }
                }
            }
        }
        // // // // Log::info(print_r($request->second_array,true));
        // // // // Log::info('----aftercheck------');
        // // // // Log::info(print_r($selected_benefits,true));
        // // // // Log::info('----aftercheck detail------');
        // // // // Log::info(print_r($selected_benefits_details,true));
        // 
        session([
                    "pay_data"          => $request->second_array,
                    "pay_data_checked"  => $selected_benefits,
                    "pay_data_detailed" => $selected_benefits_details,
                    "premium"           => $request->premium,
                    'basic_premium'     => $basic_premium_cal,
                    'actual_premium'    => $request->actual_premium,
                    'carValueSelected'  => $request->carValueSelected,
                    'actual_rate'       => $request->actual_rate,
                    'min_agency_value'  => $request->min_agency_value,
                    'base_rate'         => $request->base_rate,
                    'base_value'        => $request->base_value,
                    'type_value'        => $request->type_value,
                    'insuranceId'       => $request->provider_id,
                    'policy'            => $request->policy,
                    'pay_excess'        => $excessValue,
                ]);
    }


    public function payment(Request $request)
    {
        $policy_fees            = 0;
        $pay_excess             = 0;
        // $payment_details        = $request->session()->get('pay_data');
        $payment_details        = $request->session()->get('pay_data_detailed');
        $payment_data           = [];
        $premiumvalue           = $request->session()->get('premium');
        $insuranceId            = $request->session()->get('insuranceId');
        $excess                 = 0;
        $pab_driver             = 'NA';
        $pab_passenger          = 'NA';
        $partners               = InsuranceProvider::active()->get();
        $this->data['partners'] = $partners;
        //$this->data['payment_details'] = $payment_details;
        // // // // foreach ($payment_details as $key => $value) {
        // // // //     //$benefitMaster = BenefitMaster::find($key);
        // // // //     if (!empty($key)) {
        // // // //         $benefitMaster = BenefitMaster::find($key);
        // // // //         if (!is_null($benefitMaster)) {
        // // // //             $payment_data[$benefitMaster->name] = $value;
        // // // //             if ($key == 5) {
        // // // //                 $pab_driver = $value;
        // // // //             } elseif ($key == 6) {
        // // // //                 $pab_passenger = $value;
        // // // //             }
        // // // //         } elseif ($key == 90001) {
        // // // //             $payment_data['excess'] = $value;
        // // // //             $pay_excess             = $value;
        // // // //         }
        // // // //     }
        // // // //  }
        foreach ($payment_details as $key => $data) {
            $description = $data['selected_value'];
            if($data['selected_value_type'] == 'amount' ) {
                $description = $data['CURRENCY_CODE'] . ' ' .$description;
            }
            $payment_data[$data['benefit']['name']] = $description;
        }
        ksort($payment_data);
        // dd($payment_data);
        // dd('test');
        $additionalProducts = CrossSellingProducts::active()->get();
        //env value for vat
        $vat          = env('VAT', 0);
        $vat_premium  = ($premiumvalue * ($vat / 100));
        $policy_value = InsuranceProvider::find($insuranceId);
        //
        if ($policy_value->policy_fees_percentage || $policy_value->upto_max_amount || $policy_value->policy_fees_amount) {
            if (!empty($policy_value->policy_fees_percentage)) {
                $policy_fees = ($premiumvalue * ($policy_value->policy_fees_percentage / 100));
                if ($policy_fees > $policy_value->upto_max_amount) {
                    $policy_fees = $policy_value->upto_max_amount;
                }
            } elseif (!empty($policy_value->upto_max_amount)) {
                $policy_fees = $policy_value->upto_max_amount;
            } elseif (!empty($policy_value->policy_fees_amount)) {
                $policy_fees = $policy_value->policy_fees_amount;
            } else {
                $policy_fees = 0;
            }

        }
        session([
            'vat_premium'               => $vat_premium,
            'pab_passenger'             => $pab_passenger,
            'pab_driver'                => $pab_driver,
            'partner_policy_percentage' => $policy_value->policy_fees_percentage,
            'partner_max_amount'        => $policy_value->upto_max_amount,
            'partner_policy_amount'     => $policy_value->policy_fees_amount,
            // 'pay_excess'                => $pay_excess,
        ]);
        //dd($vat_premium."---".$premiumvalue);
        $add_fees            = 0;
        $add_select          = '';
        $vat_total           = 0;
        $this->data['total'] = ($premiumvalue + $vat_premium + $policy_fees);
        //
        session([
            'total'                      => $this->data['total'],
            'base_commission_percentage' => $policy_value->base_commission_percentage,
            'base_commission_amount'     => $policy_value->base_commission_amount,
            'policy_fees'                => $policy_fees,
            'total_premium_value'        => $this->data['total'],
            'add_fees'                   => $add_fees,
            'add_select'                 => $add_select,
            'addon_vat'                  => $vat_total,
            'addon_policyfees'           => 0,
        ]);
        //
        $this->data['vat_premium']         = $vat_premium;
        $this->data['payment_data']        = $payment_data;
        $this->data['policy_fees']         = $policy_fees;
        $this->data['premiumvalue']        = $premiumvalue;
        $this->data['additional_Products'] = $additionalProducts;
        $this->data['excess']              = $excess;
        //return $payment_data;
        return view('paymentpage', $this->data);
    }


    public function orderDetails(Request $request)
    {
        $pab_passenger_value = 'NA';
        $pab_driver_value    = 'NA';
        if (!empty(Session::get('pab_passenger'))) {
            if (Session::get('pab_passenger') == "yes") {
                $pab_passenger_value = 0;
            } else {
                $pab_passenger_value = Session::get('pab_passenger');
            }
        }

        if (!empty(Session::get('pab_driver'))) {
            if (Session::get('pab_driver') == "yes") {
                $pab_driver_value = 0;
            } else {
                $pab_driver_value = Session::get('pab_driver');
            }
        }
        $total_add_on_product = 0;
        $total_vat_calc       = 0;
        $add_on_policy_fees   = 0;
        $add_on_total         = 0;
        // Cross selling / addon products
        $additionalSelectedProduct = json_decode($request->session()->get('add_select'), true);
        
        // // if (!empty($additionalSelectedProduct)) {
        // //     foreach ($additionalSelectedProduct as $key => $value) {
        // //         $prod_select = CrossSellingProducts::where('id', $key)->get();
        // //         if (!$prod_select->isEmpty()) {
        // //             $total_add_on_product += $prod_select->first()->premium;
        // //             $vat_val  = (float) env('ADD_ON_VAT');
        // //             $vat_calc = (($prod_select->first()->premium) * ($vat_val)) / 100;
        // //             $total_vat_calc += $vat_calc;
        // //             $add_on_policy_fees += (float) env('ADD_ON_POLICY_FEES');

        // //         } else {
        // //             return "Additional product you have selected is invalid";
        // //         }
        // //     }
        // //     $add_on_total = ($total_add_on_product + $total_vat_calc + $add_on_policy_fees);
        // // }

        // // // // $benefit_array = $request->session()->get('pay_data');
        $benefit_array = $request->session()->get('pay_data_detailed');
        unset($benefit_array['90001']);
        $benefitOption             = json_encode($benefit_array);
        $premiumvalue              = $request->session()->get('premium');
        $insuranceId               = $request->session()->get('insuranceId');
        $vatPremium                = $request->session()->get('vat_premium');
        $total                     = $request->session()->get('total');
        $policyFees                = $request->session()->get('policy_fees');
        $partner_policy_percentage = $request->session()->get('partner_policy_percentage');
        $partner_max_amount        = $request->session()->get('partner_max_amount');
        $partner_policy_amount     = $request->session()->get('partner_policy_amount');
        $additionalProductValue    = $add_on_total;
        $actualPremiumValue        = $request->session()->get('actual_premium');
        $carValueSelected          = $request->session()->get('carValueSelected');
        $agencyType                = $request->session()->get('type_value');
        $commissionPercentage      = $request->session()->get('base_rate');
        $commissionAmount          = $request->session()->get('base_value');
        $minimumRate               = $request->session()->get('actual_rate');
        $minimumValue              = $request->session()->get('min_agency_value');
        $addOnVat                  = $total_vat_calc;
        $addOnPolicyFee            = $add_on_policy_fees;
        $grandTotal                = $total + $add_on_total;
        /* Baltech code*/
        $user_id = 0;
        if ((Session::has('user_id') && Session::get('user_id') > 0) || (Session::has('agentuser_userid') && Session::get('agentuser_userid') > 0)) {
            
            if ((Session::has('user_id') && Session::get('user_id') > 0)) {
                $user_id = Session::get('user_id');
            } else {
                $user_id = Session::get('agentuser_userid');
            }
            $user = User::find($user_id);
        }else {
            $email = Session::get('user_email');
            $user  = User::where('email',$email)->first();
            if($user) {
                $user_id = $user->id;
            }else{
                $user                    = new User();
                $user->name              = Session::get('user_name');
                $user->mobile            = Session::get('user_mobile');
                $user->email             = Session::get('user_email');
                $user->password          = bcrypt('secret');
                $user->is_email_verified = 1;
                $user->email_verified_at = Carbon::now();
                $user->save();
                // 
                $role    = Role::where('name', 'user')->first();
                $user->attachRole($role);
                $user_id = $user->id;
                // User::sendWelcomeEmail($user);
                if ((!empty(Auth::user())) && (Auth::user()->can(['agent']))) {
                    $agentUser                   = new AgentUser();
                    $agentUser->agent_id         = Auth::user()->id;
                    $agentUser->user_id          = $user_id;
                    $agentUser->is_current_agent = 1;
                    $agentUser->save();
                } else {
                    $this->assignDefaultAgent($user);
                    // // $agentUser                   = new AgentUser();
                    // // $agentUser->agent_id         = env('SUPERADMINID');
                    // // $agentUser->user_id          = $user_id;
                    // // $agentUser->is_current_agent = 1;
                    // // $agentUser->save();
                }
            }
        }

        if($user_id) {
            $order                             = new Order();
            $order->insurance_providers_id     = $insuranceId;
            $order->premium_value              = $premiumvalue;
            $order->vat_premium                = $vatPremium;
            $order->policy_fees                = $policyFees;
            $order->benefit_options            = $benefitOption;
            $order->basic_premium              = Session::get('basic_premium');
            $order->total                      = $total;
            $order->base_commission_percentage = Session::get('base_commission_percentage');
            $order->base_commission_amount     = Session::get('base_commission_amount');
            $order->pab_passenger              = $pab_passenger_value;
            $order->pab_driver                 = $pab_driver_value;
            $order->users_id                   = $user_id;
            $order->partner_policy_percentage  = $partner_policy_percentage;
            $order->partner_max_amount         = $partner_max_amount;
            $order->partner_policy_amount      = $partner_policy_amount;
            $order->excess                     = Session::get('pay_excess');
            $order->grand_total                = $grandTotal;
            //$order->additional_product_selected = $additionalSelectedProduct;
            // if ((!empty(Auth::user())) && (Auth::user()->can(['agent', 'super-admin', 'sub-admin']))) {
            if ((!empty(Auth::user()))) {
                $order->created_by = Auth::user()->id;
            } else {
                $order->created_by = $user_id;
            }
            $order->status                = 0;
            $order->actual_premium_value  = $actualPremiumValue;
            $order->car_value             = $carValueSelected;
            $order->agency_type           = $agencyType;
            $order->precentage_rate       = $minimumRate;
            $order->minimum_value         = $minimumValue;
            $order->commission_percentage = $commissionPercentage;
            $order->commission_value      = $commissionAmount;
            if ((!empty(Auth::user())) && (Auth::user()->can(['agent']))) {
                $order->agent_id = Auth::user()->id;
            } else {
                // // $order->agent_id = env('SUPERADMINID');
                $super_agent = User::active()->orderBy('id','asc')->withRole('super-agent')->first();
                $order->agent_id = $super_agent->id;
            }
            /////////////////////////
            // commission on order //
            /////////////////////////
            try{
                $app_commission = 0;
                $basic_premium       = Session::get('basic_premium');
                $partner             = $order->insuranceProvider;
                if($partner) {
                    ////////////////////
                    // App commission //
                    ////////////////////
                    $app_base_commission_percentage = $partner->base_commission_percentage;
                    $app_base_commission_amount     = $partner->base_commission_amount;
                    // 
                    if($app_base_commission_percentage > 0) {
                        $_total         = $basic_premium + (float)$pab_passenger_value + (float)$pab_driver_value;
                        $app_commission = ($_total * $app_base_commission_percentage)/100;
                    }else {
                        $app_commission = $app_base_commission_amount;
                    }
                    // 
                    $order->app_commission = $app_commission;
                    //////////////////////
                    // Agent Commission //
                    //////////////////////
                    try{
                        $agent_commission = $partner->userAgentCommission->where('user_id',$order->agent_id)->first();
                        if($agent_commission){
                            $order->agent_commission_percentage_or_amount = $agent_commission->percentage_or_amount;
                            $order->agent_commission_amount               = $agent_commission->amount;
                            if($agent_commission->agent_base_commission_percentage > 0) {
                                $order->agent_commission = (($app_commission * $agent_commission->agent_base_commission_percentage ) / 100);
                            }else{
                                $order->agent_commission = $agent_commission->amount;
                            }
                        }

                    }catch(\Exception $e){
                        // 
                    }
                }

            }catch(\Exception $e){
                // 
            }
            ////////////////
            // order save //
            ////////////////
            $order->save();
            $orderId = $order->id;
            
            ////////////////////////////////////
            // Cross Selling / Addon Prodcuts //
            ////////////////////////////////////
            if (!empty($additionalSelectedProduct)) {
                // // // foreach ($additionalSelectedProduct as $key => $value) {
                // // //     $additional_cover                           = new AdditionalCover();
                // // //     $additional_cover->cross_selling_product_id = $key;
                // // //     $add_value_select                           = CrossSellingProducts::where('id', $key)->get();
                // // //     $additional_cover->premium_value            = $add_value_select->first()->premium;
                // // //     $additional_cover->order_id                 = $orderId;
                // // //     $sumInsured                                 = CrossSellingProducts::where('id', $key)->pluck('sum_insured');
                // // //     $additional_cover->sum_insured              = $sumInsured['0'];
                // // //     $additional_cover->save();
                // // // }

                foreach ($additionalSelectedProduct as $key => $value) {
                    // 
                    $crossSellingProducts = CrossSellingProducts::where('id', $key)->first();
                    // 
                    $premiumValue = $crossSellingProducts->premium;
                    $total_add_on_product += $premiumValue;
                    
                    /*$addOnagentCommission = UserAgentAddonCommission::where('user_id',$order->agent_id)->where('cross_selling_product_id',$key)->first();
                    $partner = InsuranceProvider::find($crossSellingProducts->insurance_provider_id);
                    */
                    $addOnagentCommission = $crossSellingProducts->userAgentAddonCommission->where('user_id',$order->agent_id)->first();
                    $partner              = $crossSellingProducts->insuranceProvider;
                    // 
                    $additional_cover                           = new AdditionalCover();
                    $additional_cover->order_id                 = $orderId;
                    $additional_cover->cross_selling_product_id = $key;
                    $additional_cover->insurance_provider_id    = $crossSellingProducts->insurance_provider_id;
                    $additional_cover->name                     = $crossSellingProducts->name;
                    $additional_cover->premium_value            = $premiumValue;
                    $additional_cover->benefits                 = $crossSellingProducts->benefits;
                    $additional_cover->sum_insured              = $crossSellingProducts->sum_insured;
                    $additional_cover->vat                      = $crossSellingProducts->vat;
                    $additional_cover->fee_type                 = $crossSellingProducts->fee_type;
                    $additional_cover->fee                      = $crossSellingProducts->fee;
                    
                    if($crossSellingProducts->vat) {
                        $vat_calc = ($premiumValue * $crossSellingProducts->vat)/100;
                        $additional_cover->vat_value = $vat_calc;
                        $total_vat_calc += $vat_calc;
                    }

                    if($crossSellingProducts->fee_type == 1) {
                        $fee_calc = ($premiumValue * $crossSellingProducts->fee)/100;
                        $additional_cover->fee_value = $fee_calc;
                        $add_on_policy_fees += $fee_calc;
                    }
                    if($crossSellingProducts->fee_type == 2) {
                        $additional_cover->fee_value = $crossSellingProducts->fee;
                        $add_on_policy_fees += $crossSellingProducts->fee;
                    }
                     
                    // commission
                    if($partner) {
                        // 
                        $siteCommissionValue  = 0;
                        $agentCommissionValue = 0;
                        // Site commission
                        if($partner->addon_base_commission_percentage) {
                            $siteCommissionValue = $premiumValue * ($partner->addon_base_commission_percentage/100);
                        }
                        // 
                        if($partner->addon_base_commission_amount) {
                            $siteCommissionValue = $partner->addon_base_commission_amount;
                        }

                        $additional_cover->site_commission_percentage_or_amount = $partner->addon_base_commission_percentage ? 1 : 2;
                        $additional_cover->site_commission_amount               = $partner->addon_base_commission_percentage ? $partner->addon_base_commission_percentage : $partner->addon_base_commission_amount;
                        $additional_cover->site_commission_value                = $siteCommissionValue;

                        // Agent commission
                        if($addOnagentCommission) { 
                            $additional_cover->agent_id                              = $addOnagentCommission->user_id;
                            $additional_cover->agent_commission_percentage_or_amount = $addOnagentCommission->percentage_or_amount;
                            $additional_cover->agent_commission_amount               = $addOnagentCommission->amount;
                            
                            if($addOnagentCommission->percentage_or_amount == 2) {
                                $agentCommissionValue = $addOnagentCommission->amount;
                            
                            }elseif($addOnagentCommission->percentage_or_amount == 1) {
                                if($addOnagentCommission->amount > 0){
                                    $agentCommissionValue = $siteCommissionValue * ($addOnagentCommission->amount/100);
                                }
                            }
                            
                            $additional_cover->agent_commission_value         = $agentCommissionValue;
                        }
                    }
                    
                    $additional_cover->total_premium = $additional_cover->vat_value+$additional_cover->fee_value+$additional_cover->premium_value;
                    $additional_cover->save();
                }
                $add_on_total       = ($total_add_on_product + $total_vat_calc + $add_on_policy_fees);
                $order->grand_total += $add_on_total;
                $order->save();

                //add on details saving in addon_order table
                $addon_order                  = new AddonOrder();
                $addon_order->user_id         = $user_id;
                $addon_order->parent_order_id = $orderId;
                $addon_order->total_premium   = $additionalProductValue;
                $addon_order->vat             = env('ADD_ON_VAT');
                $addon_order->vat_amount      = $addOnVat;
                $addon_order->policy_fees     = $addOnPolicyFee;
                $addon_order->save();
                
                Order::sendAddOnInvoiceNotification($user, $order);
            }
            // 
            if ($orderId) {
                $lead           = PolicyLead::find(Session::get('policy_leads_id'));
                $lead->order_id = $orderId;
                $lead->user_id  = $user_id;
                if ($lead->save()) {
                    $request->session()->forget(['pay_data', 'premium', 'insuranceId', 'vat_premium', 'pab_passenger', 'pab_driver']);
                    $partners               = InsuranceProvider::all();
                    $this->data['partners'] = $partners;
                    $this->data['order_id'] = $orderId;
                    $this->data['user_id']  = $user_id;
                    // Order::sendOrderCreatedNotification($user, $order);
                    // Order::sendInvoiceNotification($user, $order);
                    $this->data['policydocs'] = PolicyDocumentMaster::active()->get();
                    return view('buynowsuccess', $this->data);
                } else {
                    $this->data['errors'] = "unable to place the order";
                    return view('buynowsuccess', $this->data);
                }
            }

        }else{
            $this->data['errors'] = "unable to place the order";
            return view('buynowsuccess', $this->data);
        }
    }

    public function updatePayment(Request $request) {
        $addOnProducts = [];
        session(['add_select'      => $request->add_select]);
        $additionalSelectedProduct = json_decode($request->get('add_select'), true);
        
        foreach ($additionalSelectedProduct as $key => $value) {
            $select = ['id','name','premium','sum_insured','vat','fee_type','fee'];
            $prod_select = CrossSellingProducts::select($select)->where('id', $key)->first();
            if ($prod_select) {
                $addOnProducts[] = $prod_select;
            } else {
                return response()->json(array('action' => 0));
            }
        }
        return response()->json(array('action' => 1, 'addOnProducts'=>$addOnProducts,'policy_total'=> $request->session()->get('total')));
        // return view('additional_product_details', compact('addOnProducts'));
    }

    public function dashboard() {
        $partners = InsuranceProvider::all();
        $user = Auth::user();
        if($user && !$user->hasVerifiedEmail()) {
            return redirect('/please-verify-email');
        }
        // 
        // $ordersactive = Order::query();
        // $ordersexpired = Order::query();

        // if($user->hasRole('agent')){
        //     $ordersactive = $ordersactive->where('created_by',$user->id)->expiredDateGreaterThan()->get();
        //     $ordersexpired = $ordersexpired->where('created_by',$user->id)->expiredDateLessThan()->get();
        // }
        // else{
        //     $ordersactive = $ordersactive->where('users_id',$user->id)->expiredDateGreaterThan()->get();
        //     $ordersexpired = $ordersexpired->where('users_id',$user->id)->expiredDateLessThan()->get();
        // }

        // $this->data['partners'] = $partners;
        // $this->data['ordersactive'] = $ordersactive;
        // $this->data['ordersexpired'] = $ordersexpired;
        // return view('dashboard',$this->data);
        return view('dashboard');
    }


    public function mypolicy()
    {
        $user = Auth::user();
        if(!$user->hasVerifiedEmail()) {
            return redirect('/please-verify-email');
        }
        // 
        $ordersactive = Order::query();
        $ordersexpired = Order::query();

        if($user->hasRole('agent')){
            $ordersactive = $ordersactive->where('created_by',$user->id)->expiredDateGreaterThan()->get();
            $ordersexpired = $ordersexpired->where('created_by',$user->id)->expiredDateLessThan()->get();
        }
        else{
            $ordersactive = $ordersactive->where('users_id',$user->id)->expiredDateGreaterThan()->get();
            $ordersexpired = $ordersexpired->where('users_id',$user->id)->expiredDateLessThan()->get();
        }

        //$this->data['partners'] = $partners;
        $this->data['ordersactive'] = $ordersactive;
        $this->data['ordersexpired'] = $ordersexpired;
        return view('my-policy',$this->data);

    }

    public function myprofile()
    {
        $user = Auth::user();
        if(!$user->hasVerifiedEmail()) {
            return redirect('/please-verify-email');
        }

        if( $user ) {

            $this->data['data'] = $user;
            return view('my-profile',$this->data);
        } else {
            //return redirect('/admin/users')->with('error', 'No data found');
        }
    }

    //public function updateprofile(Request $request, $id)
    public function updateprofile(Request $request)
    {
        $user = Auth::user();

        if(!$user->hasVerifiedEmail()) {
            return redirect('/please-verify-email');
        }

        //$adminID = Auth::id();

        $userid = $user->id;

        $name = $request->name;
        //$email = $request->email;
        $mobile = $request->mobile;
        $gender = $request->gender;
        $address = $request->address;
        $maritalstatus = $request->maritalstatus;
        $dob = $request->dob;
        $passport_number = $request->passport_number;
        $passport_expiry_date = $request->passport_expiry_date;
        $license_number = $request->license_number;
        $license_expiry_date = $request->license_expiry_date;
        $uae_id = $request->uae_id;
        $uae_id_expiry_date = $request->uae_id_expiry_date;
        //$status = $request->status;

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        request()->validate([
            "name" => "required|min:2|max:100",
            //"email" => "required|unique:users,email,$id"
        ], 
            [
                'name.required' => "The name field is required.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                //'email.required' => "The email field is required.",
                //'email.unique' => "The name $email has already been taken.",
            ]
        );

        $user->name                 = $name;
        //$user->email                = $email;
        $user->mobile               = $mobile;
        $user->gender               = $gender;
        $user->address              = $address;
        $user->maritalstatus        = $maritalstatus;
        $user->dob                  = $dob;
        $user->passport_number      = $passport_number;
        $user->passport_expiry_date = $passport_expiry_date;
        $user->license_number       = $license_number;
        $user->license_expiry_date  = $license_expiry_date;
        $user->uae_id               = $uae_id;
        $user->uae_id_expiry_date   = $uae_id_expiry_date;
        //$user->status               = $status;
        $user->modified_by          = $userid;

        if ($request->has_logo_image == '0' ){
            $user->has_logo = 0;
        }
        // 
        $imageName = '';
        try{
            if ($request->hasFile('image')) { 
                // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                $imageName = $user->id.'_main';
                $request->image->move(public_path('uploads/users'), $imageName);
                // resize for logo
                $imageLogo = $user->id.'_logo';
                $img = Image::make(public_path('uploads/users/' . $imageName));
                $img->fit(75)->save(public_path('uploads/users/' . $imageLogo));

                $user->has_logo = 1;
            }

        }
        catch(\Exception $e){
            // do task when error
            // echo $e->getMessage();
        }

        $user->save();
        
        if($user->hasRole('agent')){
            return redirect('/agent/dashboard')->with('success', 'Agent updated successfully');
        }
        else{
            return redirect('/user/dashboard')->with('success', 'User updated successfully');
        }
        
    }

    public function agentcommissionreport()
    {
        $user = Auth::user();
        if(!$user->hasVerifiedEmail()) {
            return redirect('/please-verify-email');
        }
        // 
        //echo "<pre>";print_r($user);die;
        $orders = Order::query();
        //$ordersactive = $ordersactive->where('created_by',$user->id)->expiredDateGreaterThan()->get();
        $orders = $orders->where('created_by',$user->id)->get();

        $this->data['orders'] = $orders;
        return view('agent-commission-report',$this->data);

    }

    public function upload() {
        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;
        return view('uploadfile',$this->data);
    }
    // public function orderSave() {
        
    //     return view('',$this->data);
    // }

    public function uploadFile(Request $request)
    {
        $files = [];

        foreach ($request->file('filename') as $file) {
            $filename    = $file->store('files');
            $upload_data = Upload::create([
                'filename' => $filename,
            ]);

            $upload_object         = new \stdClass();
            $upload_object->name   = str_replace('files/', '', $file->getClientOriginalName());
            $upload_object->size   = round(Storage::size($filename) / 2048, 2);
            $upload_object->fileID = $upload_data->id;
            $files[]               = $upload_object;
        }
        return response()->json(array('files' => $files), 200);
    }


    public function uploadedDocuments(Request $request)
    {
        Upload::whereIn('id', explode(",", $request->file_ids))
            ->update(['order_id' => $request->order_id, 'user_id' => $request->user_id]);
        return response()->json(['status' => 'success', 'msg' => 'File uploaded sucessfully']);
    }

    public function uploadedPolicyDocuments(Request $request)
    {

        // $this->validate($request, [
        //     'docname' => 'required',
        //     'filename' => 'required',
        // ]);


        //echo "<pre>";print_r($request->filename);die;
        // request()->validate([
        //     //'file' => 'required|mimes:csv,txt,xlsx,xls'
        //     'docname' => 'required',
        //     'filename' => 'required',
        // ]);

        $docname = $request->docname;
        $order_id = $request->order_id;
        $user_id = $request->user_id;

        if($request->hasFile('filename')){
            $files = $request->file('filename');
            //echo "<pre>";print_r($files);die;

            $images=array();
            foreach($files as $key=>$file){
                $imageName = $file->getClientOriginalName();
                //$extension = $file->getClientOriginalExtension();
                $random_str = str_random(4);
                $filename = $random_str.'_'.time().'_'.$imageName;

                $file->move(public_path('uploads/admin/userdoc'), $filename);
                //$images[] = $imageName;

                $body = new Upload();
                $body->policy_document_name = $docname[$key];
                $body->filename = $filename;
                $body->file_exact_name = $imageName;
                $body->order_id = $order_id;
                $body->user_id = $user_id;
                $body->save();
            }
        }
        // else{
        //     //return redirect()->route('start')->with('error', 'Please upload documents');
        // }

        /*
        foreach($docname as $key=>$val){

            $body = new Upload();
            $body->policy_document_name = $val;
            $body->filename = $images[$key];
            $body->order_id = $order_id;
            $body->user_id = $user_id;
            $body->save();

        }
        */
        return redirect()->route('start')->with('success', 'You have successfully uploaded documents');

    }

    public function filterCarValue(Request $request)
    {
        $car_values_deatils = $request->session()->get('car_values_deatils');
        $car_min_value      = $car_values_deatils['min_value'];
        $car_max_value      = $car_values_deatils['max_value'];
        $car_value          = $car_values_deatils['values'];
        // 
        $carInputValue = $request->carInputValue;
        $carInputValue = (float) $carInputValue;
        if ( ( $carInputValue >= $car_min_value) && ( $carInputValue <= $car_max_value ) ) {
            session::put('carValueInput', $carInputValue);
            return response()->json(['action' => 1]);
        }else{
            return response()->json(['action' => 0]);
        }
    }

    //store payment details
    public function storePayment(Request $request)
    {
        session([
            'add_fees'            => $request->add_fees,
            'add_select'          => $request->add_select,
            'addon_vat'           => $request->vat_total,
            'total_premium_value' => $request->total_premium_value,
            'addon_policyfees'    => $request->addon_policyfee,
        ]);
    }

}
