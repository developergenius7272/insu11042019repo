<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminCarModelVariantController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Car'; 
        $this->data['breadcrumbs'][] = 'Models';
        $this->data['breadcrumbs'][] = 'Variants';
        $this->data['pageTitle'] = 'Car Variants';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Car Model Variants';
        $carModelVariants = CarModelVariant::select('car_model_variants.*')
                    ->addSelect('car_makes.name as car_make_name')
                    ->addSelect('car_models.name as car_model_name')
                    ->addSelect('car_model_body_types.name as car_model_body_type_name')
                    ->leftJoin('car_models','car_models.id', 'car_model_variants.car_model_id')
                    ->leftJoin('car_makes','car_makes.id', 'car_models.car_make_id')
                    ->leftJoin('car_model_body_types','car_model_variants.car_model_body_type_id', 'car_model_body_types.id')
                    ->get();
        $this->data['carModelVariants'] = $carModelVariants;
        
        return view('admin.car_model_variants.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create car model variant';
        // 
        $carMakes = CarMake::active()->get();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        $carFuels = CarModelFuel::active()->orderBy('name','asc')->get();
        // 
        $this->data['carMakes'] = $carMakes;
        $this->data['carBodies'] = $carBodies;
        $this->data['carFuels'] = $carFuels;
        // 
        return view('admin.car_model_variants.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $carMakeId = $request->carMakeId;
        $carModelId = $request->carModelId;
        // 
        $carMakeNameNew = $request->carMakeNameNew;
        $carModelNameNew = $request->carModelNameNew;
        // 
        $name = $request->name; // car variant name
        $cylinders = $request->cylinders;
        $engine_size = $request->engine_size;
        // 
        $modelBodyTypeId = $request->modelBodyTypeId;
        $modelFuelId = $request->modelFuelId;
        $makeYear =  $request->makeYear;
        $no_of_seats =  $request->no_of_seats;
        $status =  $request->status;
        //$sub_type =  $request->sub_type;

        $name = $request->name;

        request()->validate([
            'carMakeId' => 'required',
            'carModelId' => 'required',
            'name' => "required|unique:car_model_variants,name,NULL,id,car_model_id,$carModelId|min:2|max:100",
            'modelBodyTypeId' => 'required',            
            'modelFuelId' => 'required',            
            'cylinders' => 'required',            
            'engine_size' => 'required',            
            'makeYear' => 'required',

        ], [
            'carMakeId.required' => "Please Select Car Make.",
            'carModelId.required' => "Please Select Car Model.",

            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
            'modelBodyTypeId.required' => "Please Select Car Model Body Type.",
            'modelFuelId.required' => "Please Select Fuel.",
            'cylinders.required' => "Please Fill Cylinder.",
            'engine_size.required' => "Please Fill Engine Size.",
            'makeYear.required' => "Please Fill Make Year.",
        ]);
        // 
        $carModelExist = CarModel::where('id',$carModelId)
            ->where('car_make_id',$carMakeId)
            ->active()->first();
        if( $carModelExist ) {
            
            $car = new CarModelVariant();
            $car->name = $name;
            $car->car_model_id = $carModelId;
            $car->make_year = $makeYear;
            $car->car_model_body_type_id = $modelBodyTypeId;
            $car->car_model_fuel_id = $modelFuelId;
            $car->cylinders = $cylinders;
            $car->engine_size = $engine_size;
            $car->no_of_seats = $no_of_seats;
            $car->status = $status;
            $car->created_by = $adminID;
            $car->modified_by = $adminID;
            //$car->sub_type = $sub_type;
            $car->save();
            // upload image
            try{
                if ($request->hasFile('image')) { 
                    // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();
                    $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $car->id.'_main';
                    $request->image->move(public_path('uploads/admin/car_model_variants'), $imageName);
                    // resize for logo
                    $imageLogo = $car->id.'_logo';
                    $img = Image::make(public_path('uploads/admin/car_model_variants/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/admin/car_model_variants/' . $imageLogo));

                    $car->has_logo = 1;
                    $car->save();
                }
            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }
            // 
            return redirect('/admin/cars/car-model-variants')->with('success', 'Car variant details added successfully');
        } else {
            return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit car variant';
        // 
        $carMakes = CarMake::active()->get();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        $carFuels = CarModelFuel::active()->orderBy('name','asc')->get();
        // 
        $this->data['tableHeading'] = 'Car Model Variants';
        $carModelVariant = CarModelVariant::select('car_model_variants.*')
                    ->addSelect('car_makes.name as car_make_name')
                    ->addSelect('car_makes.id as car_make_id')
                    ->addSelect('car_models.name as car_model_name')
                    ->leftJoin('car_models','car_models.id', 'car_model_variants.car_model_id')
                    ->leftJoin('car_makes','car_makes.id', 'car_models.car_make_id')
                    ->where('car_model_variants.id',$id)
                    ->first();
        $this->data['data'] = $carModelVariant;
        // 
        $this->data['carMakes'] = $carMakes;
        $this->data['carBodies'] = $carBodies;
        $this->data['carFuels'] = $carFuels;

        if( $this->data['data'] ) {
            return view('admin.car_model_variants.create_edit', $this->data);
        }
        else{
            return redirect('/admin/cars/car-model-variants')->with('error', 'No data found');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $carMakeId = $request->carMakeId;
        $carModelId = $request->carModelId;
        // 
        $carMakeNameNew = $request->carMakeNameNew;
        $carModelNameNew = $request->carModelNameNew;
        // 
        $name = $request->name; // car variant name
        $cylinders = $request->cylinders;
        $engine_size = $request->engine_size;
        // 
        $modelBodyTypeId = $request->modelBodyTypeId;
        $modelFuelId = $request->modelFuelId;
        $makeYear =  $request->makeYear;
        $no_of_seats =  $request->no_of_seats;
        $status =  $request->status;
        //$sub_type =  $request->sub_type;

        $name = $request->name;

        request()->validate([
            'name' => "required|unique:car_model_variants,name,$id,id,car_model_id,$carModelId|min:2|max:100",

            'carMakeId' => 'required',
            'carModelId' => 'required',
            'modelBodyTypeId' => 'required',            
            'modelFuelId' => 'required',            
            'cylinders' => 'required',            
            'engine_size' => 'required',            
            'makeYear' => 'required',

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",

            'carMakeId.required' => "Please Select Car Make.",
            'carModelId.required' => "Please Select Car Model.",
            'modelBodyTypeId.required' => "Please Select Car Model Body Type.",
            'modelFuelId.required' => "Please Select Fuel.",
            'cylinders.required' => "Please Fill Cylinder.",
            'engine_size.required' => "Please Fill Engine Size.",
            'makeYear.required' => "Please Fill Make Year.",
        ]);

        if(!empty( $name ))
        {
            
            $carMakeExist = CarMake::where('id',$carMakeId)->first();
            if( $carMakeExist ) {
                $carModelExist = CarModel::where('id',$carModelId)
                    ->where('car_make_id',$carMakeId)
                    ->active()->first();
                if( $carModelExist ) {
                    $carVariantExist = CarModelVariant::where('name',$name)
                        ->where('car_model_id',$carModelId)
                        ->where('id', '<>', $id)
                        ->first();
                    if( !$carVariantExist ) {
                        $car = CarModelVariant::find($id);
                        if( $car ) {

                            $car->name = $name;
                            $car->car_model_id = $carModelId;
                            $car->make_year = $makeYear;
                            $car->car_model_body_type_id = $modelBodyTypeId;
                            $car->car_model_fuel_id = $modelFuelId;
                            $car->cylinders = $cylinders;
                            $car->engine_size = $engine_size;
                            $car->no_of_seats = $no_of_seats;
                            $car->status = $status;
                            $car->modified_by = $adminID;
                            //$car->sub_type = $sub_type;
                            $car->save();

                            if ($request->has_logo_image == '0' ){
                                //$car->has_logo = $request->has_logo_image;
                                $car->has_logo = 0;
                            }

                            try{
                                if ($request->hasFile('image')) { 
                                    // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                                    $imageName = $car->id.'_main';
                                    $request->image->move(public_path('uploads/admin/car_model_variants'), $imageName);
                                    // resize for logo
                                    $imageLogo = $car->id.'_logo';
                                    $img = Image::make(public_path('uploads/admin/car_model_variants/' . $imageName));
                                    $img->fit(75)->save(public_path('uploads/admin/car_model_variants/' . $imageLogo));

                                    $car->has_logo = 1;
                                }                                
                            }
                            catch(\Exception $e){
                                // do task when error
                                // echo $e->getMessage();
                            }

                            $car->save();

                            return redirect('/admin/cars/car-model-variants')->with('success', 'Car variant details updated successfully');
                        } else {
                            return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not updated');
                        }
                    } else {
                        // echo "carVariantExist";
                        return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not updated');
                    }
                } else {
                    // echo "carModelExist";
                    return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not updated');
                }
            } else {
                // echo 'carMakeExist';
                return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not updated');
            }
        } else {
            return redirect('/admin/cars/car-model-variants')->with('error', 'Car variant details not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
