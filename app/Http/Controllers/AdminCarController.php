<?php

namespace App\Http\Controllers;

use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelSpecification;
use App\Models\CarModelVariant;
// 
use Illuminate\Http\Request;
use Auth;

class AdminCarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $cars = CarMake::select('car_makes.*')
                ->addSelect('car_models.name as modal_name')
                ->addSelect('car_models.description as modal_name_description')
                ->active()
                ->leftJoin('car_models','car_models.car_make_id','car_makes.id')
                ->get();
        $data['cars'] = $cars;
        return view('admin.cars.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['carMakes'] = CarMake::active()->get();
        $data['carModelVariants'] = CarModelVariant::all();

        return view('admin.cars.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carMakeId = $request->carMake;
        $carMakeName = $request->carMakeName;
        $carModalName = $request->carModalName;
        $carModalDescription = $request->carModalDescription;
        $carModelVariants = $request->carModelVariants;
        // 
        if( $carMakeId == 'new' && !empty($carMakeName) ) {
            $carMake = new CarMake();
            $carMake->name = $carMakeName;
            $carMake->save();
            // 
            $carMake->order = $carMake->id;
            $carMakeId = $carMake->id;
        }

        if( $carMakeId > 0 ) {
            $carMake = CarMake::where('id',$carMakeId)->first();
            if( $carMake ) {
                // 
                $carModel = CarModel::where('car_make_id',$carMakeId)->where('name',$carModalName)->first();
                if( !$carModel ) {
                    $car = new CarModel();
                    $car->car_make_id = $carMakeId;
                    $car->name = $carModalName;
                    $car->description = $carModalDescription;
                    $car->save();
                    return redirect('admin/cars')->with('success', 'Information has been added');
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // 
    /**
     * Return car_models for car_make_id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $car_makes.id
     * @return json
     */
    public function getCarMakeModels(Request $request) {
        $makeId = $request->carMakeId;
        if( $makeId > 0) {
            $models = CarModel::where('car_make_id',$makeId)->where('status',1)->get();
            if( $models ) {
                return response()->json(['action' => 1, 'data' => $models]);
            } else {
                return response()->json(['action' => 0]);
            }
        } else {
            return response()->json(['action' => 0]);
        }
    }

    public function getVariantByBodyType(Request $request) {
        $carBodyTypeId = $request->carBodyTypeId;
        $carBodyTypeIdArray = [];
        $carVariants = [];
        $variants = [];
        if( !is_array($carBodyTypeId) ) {
            $carBodyTypeIdArray[] = $carBodyTypeId;
        }else{
            $carBodyTypeIdArray = $carBodyTypeId;
        }
        // $carBodyTypeIdArray = [7];
        foreach ($carBodyTypeIdArray as $bodyId) {
            $variants[] = CarModelVariant::where('car_model_body_type_id',$bodyId)->active()->with(['carModel'])->active()->get();
        }
        // 
        $variantCounter = 1; 
        $variantDataArray = [];
        $variantMakeArray = [];
        $variantModelArray = [];
        $variantBodyTypeArray = [];
        // 
        foreach($variants as $variant) {
            foreach( $variant as $car ) {
                if($car->carModel && $car->carModel->carMake) {
                    $make = $car->carModel->carMake->name;
                    $model = $car->carModel->name;
                    $bodyType = $car->carModelBodyType->name;
                    // 
                    if( !in_array($make,$variantMakeArray) ) {
                        $variantMakeArray[] = $make;
                    }
                    if( !in_array($model,$variantModelArray) ) {
                        $variantModelArray[] = $model;
                    }
                    if( !in_array($bodyType,$variantBodyTypeArray) ) {
                        $variantBodyTypeArray[] = $bodyType;
                    }
                    // 
                    $variantDataArray[$make][$bodyType][] = $car;
                }
            }
        }
        // 
        sort($variantMakeArray);
        sort($variantBodyTypeArray);
        // 
        // foreach( $variantMakeArray as $make ) {
        //     foreach( $variantBodyTypeArray as $bodyType ) { 
        //         if( isset($variantDataArray[$make][$bodyType])) {
        //             foreach($variantDataArray[$make][$bodyType] as $car ) {
                                    // echo "make - $make | body - $bodyType | variant : $car->name";
        //             }
        //         }
        //     }
        // }

        return view('admin.variant-by-body-type',compact('variantMakeArray','variantBodyTypeArray','variantDataArray'))->render();
    }
}
