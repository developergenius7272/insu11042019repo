<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceProvider;
use App\Models\Homepage;
use Illuminate\Routing\UrlGenerator;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Homepage::where('display_at_home',1)->first();
        $this->data['banners'] = $banners;

        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;

        return view('welcome', $this->data);
    }

    public function contactUs()
    {

        return view('static-views.contact-us');
    }

    public function terms()
    {

        return view('static-views.terms');
    }    

    public function privacyPolicy()
    {

        return view('static-views.privacy-policy');
    }    

    public function paymentRefundPolicy()
    {

        return view('static-views.payment-refund-policy');
    }

    public function disclaimer()
    {

        return view('static-views.disclaimer');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
