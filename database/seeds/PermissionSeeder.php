<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'agent',
                'display_name' => 'Agent',
                'description' => 'Buys policy behalf of customer.'
            ],
            [
                'name' => 'marketing',
                'display_name' => 'Marketing user',
                'description' => 'They can sell the products'
            ],
            [
                'name' => 'role-create',
                'display_name' => 'Create Role',
                'description' => 'Create New Role'
            ],
            [
                'name' => 'role-list',
                'display_name' => 'Display Role Listing',
                'description' => 'List All Roles'
            ],
            [
                'name' => 'role-update',
                'display_name' => 'Update Role',
                'description' => 'Update Role Information'
            ],
            [
                'name' => 'user',
                'display_name' => 'User',
                'description' => 'User'
            ],
            [
                'name' => 'user-create',
                'display_name' => 'Create User',
                'description' => 'Create New User'
            ],
            [
                'name' => 'user-list',
                'display_name' => 'Display User Listing',
                'description' => 'List All Users'
            ],
            [
                'name' => 'user-update',
                'display_name' => 'Update User',
                'description' => 'Update User Information'
            ],
            [
                'name' => 'agent-create',
                'display_name' => 'Create Agent',
                'description' => 'Create New Agent'
            ],
            [
                'name' => 'agent-list',
                'display_name' => 'Display Agent Listing',
                'description' => 'List All Agents'
            ],
            [
                'name' => 'agent-update',
                'display_name' => 'Update Agent',
                'description' => 'Update Agent Information'
            ],
            [
                'name' => 'partner-create',
                'display_name' => 'Create Partner',
                'description' => 'Create New Partner'
            ],
            [
                'name' => 'partner-list',
                'display_name' => 'Display Partner Listing',
                'description' => 'List All Partners'
            ],
            [
                'name' => 'partner-update',
                'display_name' => 'Update Partner',
                'description' => 'Update Partner Information'
            ],
            [
                'name' => 'page-create',
                'display_name' => 'Create Page',
                'description' => 'Create New Page'
            ],
            [
                'name' => 'page-list',
                'display_name' => 'Display Page Listing',
                'description' => 'List All Pages'
            ],
            [
                'name' => 'page-update',
                'display_name' => 'Update Page',
                'description' => 'Update Page Information'
            ],
            [
                'name' => 'car-value-create',
                'display_name' => 'Create Car Value',
                'description' => 'Create New Car Value'
            ],
            [
                'name' => 'car-value-list',
                'display_name' => 'Display Car Value Listing',
                'description' => 'List All Car Values'
            ],
            [
                'name' => 'car-value-update',
                'display_name' => 'Update Car Value',
                'description' => 'Update Car Value Information'
            ],
            [
                'name' => 'order-create',
                'display_name' => 'Create Order',
                'description' => 'Create New Order'
            ],
            [
                'name' => 'order-list',
                'display_name' => 'Display Order Listing',
                'description' => 'List All Orders'
            ],
            [
                'name' => 'order-update',
                'display_name' => 'Update Order',
                'description' => 'Update Order Information'
            ],
            [
                'name' => 'lead-create',
                'display_name' => 'Create Lead',
                'description' => 'Create New Lead'
            ],
            [
                'name' => 'lead-list',
                'display_name' => 'Display Lead Listing',
                'description' => 'List All Leads'
            ],
            [
                'name' => 'lead-update',
                'display_name' => 'Update Lead',
                'description' => 'Update Lead Information'
            ],
            [
                'name' => 'cross-selling-product-create',
                'display_name' => 'Create Cross Selling Product',
                'description' => 'Create New Cross Selling Product'
            ],
            [
                'name' => 'cross-selling-product-list',
                'display_name' => 'Display Cross Selling Product Listing',
                'description' => 'List All Cross Selling Products'
            ],
            [
                'name' => 'cross-selling-product-update',
                'display_name' => 'Update Cross Selling Product',
                'description' => 'Update Cross Selling Product Information'
            ],
            [
                'name' => 'excess-create',
                'display_name' => 'Create Excess',
                'description' => 'Create New Excess'
            ],
            [
                'name' => 'excess-list',
                'display_name' => 'Display Excess Listing',
                'description' => 'List All Excess'
            ],
            [
                'name' => 'excess-update',
                'display_name' => 'Update Excess',
                'description' => 'Update Excess Information'
            ],
            [
                'name' => 'premium-plan-create',
                'display_name' => 'Create Premium Plan',
                'description' => 'Create New Premium Plan'
            ],
            [
                'name' => 'premium-plan-list',
                'display_name' => 'Display Premium Plan Listing',
                'description' => 'List All Premium Plan'
            ],
            [
                'name' => 'premium-plan-update',
                'display_name' => 'Update Premium Plan',
                'description' => 'Update Premium Plan Information'
            ],
            [
                'name' => 'third-party-cover-create',
                'display_name' => 'Create TPL Plan',
                'description' => 'Create New TPL Plan'
            ],
            [
                'name' => 'third-party-cover-list',
                'display_name' => 'Display TPL Plan Listing',
                'description' => 'List All TPL Plan'
            ],
            [
                'name' => 'third-party-cover-update',
                'display_name' => 'Update TPL Plan',
                'description' => 'Update TPL Plan Information'
            ],
            [
                'name' => 'enefit-master-create',
                'display_name' => 'Create Benefit Master',
                'description' => 'Create New Benefit Master'
            ],
            [
                'name' => 'enefit-master-list',
                'display_name' => 'Display Benefit Master Listing',
                'description' => 'List All Benefit Masters'
            ],
            [
                'name' => 'enefit-master-update',
                'display_name' => 'Update Benefit Master',
                'description' => 'Update Benefit Master Information'
            ],
            [
                'name' => 'planbenefit-create',
                'display_name' => 'Create Plan Benefit',
                'description' => 'Create New Plan Benefit'
            ],
            [
                'name' => 'planbenefit-list',
                'display_name' => 'Display Plan Benefit Listing',
                'description' => 'List All Plan Benefits'
            ],
            [
                'name' => 'planbenefit-update',
                'display_name' => 'Update Plan Benefit',
                'description' => 'Update Plan Benefit Information'
            ],
            [
                'name' => 'homepage-banner-create',
                'display_name' => 'Create Homepage Banner',
                'description' => 'Create New Homepage Banner'
            ],
            [
                'name' => 'homepage-banner-list',
                'display_name' => 'Display Homepage Banner Listing',
                'description' => 'List All Homepage Banners'
            ],
            [
                'name' => 'homepage-banner-update',
                'display_name' => 'Update Homepage Banner',
                'description' => 'Update Homepage Banner Information'
            ],
            [
                'name' => 'car-make-create',
                'display_name' => 'Create Car Maker',
                'description' => 'Create New Car Maker'
            ],
            [
                'name' => 'car-make-list',
                'display_name' => 'Display Car Maker Listing',
                'description' => 'List All Car Makers'
            ],
            [
                'name' => 'car-make-update',
                'display_name' => 'Update Car Maker',
                'description' => 'Update Car Maker Information'
            ],
            [
                'name' => 'car-model-create',
                'display_name' => 'Create Car Model',
                'description' => 'Create New Car Model'
            ],
            [
                'name' => 'car-model-list',
                'display_name' => 'Display Car Model Listing',
                'description' => 'List All Car Models'
            ],
            [
                'name' => 'car-model-update',
                'display_name' => 'Update Car Model',
                'description' => 'Update Car Model Information'
            ],
            [
                'name' => 'car-model-variant-create',
                'display_name' => 'Create Car Model Variant',
                'description' => 'Create New Car Model Variant'
            ],
            [
                'name' => 'car-model-variant-list',
                'display_name' => 'Display Car Model Variant Listing',
                'description' => 'List All Car Model Variants'
            ],
            [
                'name' => 'car-model-variant-update',
                'display_name' => 'Update Car Model Variant',
                'description' => 'Update Car Model Variant Information'
            ],
            [
                'name' => 'car-model-body-create',
                'display_name' => 'Create Car Model Body',
                'description' => 'Create New Car Model Body'
            ],
            [
                'name' => 'car-model-body-list',
                'display_name' => 'Display Car Model Body Listing',
                'description' => 'List All Car Model Body'
            ],
            [
                'name' => 'car-model-body-update',
                'display_name' => 'Update Car Model Body',
                'description' => 'Update Car Model Body Information'
            ],
            [
                'name' => 'car-model-fuel-create',
                'display_name' => 'Create Car Model Fuel',
                'description' => 'Create New Car Model Fuel'
            ],
            [
                'name' => 'car-model-fuel-list',
                'display_name' => 'Display Car Model Fuel Listing',
                'description' => 'List All Car Model Fuel'
            ],
            [
                'name' => 'car-model-fuel-update',
                'display_name' => 'Update Car Model Fuel',
                'description' => 'Update Car Model Fuel Information'
            ],

            [
                'name' => 'super-admin',
                'display_name' => 'Super Admin',
                'description' => 'This will be one permission, that can not be assigned or created.'
            ],
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => 'This can create other users ,agent and assign permissions to them.'
            ],


        ];
        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
