<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Admin; 

class AddAdminDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = new Admin();
        $admin->email = 'abc@gmail.com';
        $admin->password = Hash::make('123456');
        $admin->remember_token = '123456';
        $admin->created_at = date('Y-m-d H:m:s');
        $admin->updated_at = date('Y-m-d H:m:s');
        $admin->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
