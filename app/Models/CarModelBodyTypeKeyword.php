<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarModelBodyTypeKeyword extends Model
{
    protected $fillable = ['car_model_body_type_id', 'keyword', 'status'];
    
    /**
     * Get the post that owns the comment.
     */
    public function bodytype()
    {
        return $this->belongsTo('App\Models\CarModelBodyType', 'car_model_body_type_id', 'id');
    }
}
