@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cross Selling Products</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarPremium" method="POST" @if($formType == 'edit') action="{{action('AdminCrossSellingProductsController@update', $data->id)}}" @else action="{{url('/admin/cross-selling-products')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            {{--  --}}

                            {{-- Car related --}}
                            <fieldset>
                                <legend>Cross Selling Product</legend>
                                
                                {{-- name --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Product Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Product Name" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="insuranceProviderId">Insurance provider / Partner</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="insuranceProviderId" name="insuranceProviderId">
                                            <option value="">Select Partner</option>
                                            @if(isset($partners) && count($partners))
                                                @foreach($partners as $partner)
                                                    <option value="{{$partner->id}}" 
                                                        @if(old('insuranceProviderId') == $partner->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($partner->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="addon_category_id">Addon Category</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="addon_category_id" name="addon_category_id">
                                            <option value="">Select Addon Categories</option>
                                            @if(isset($addoncategories) && count($addoncategories))
                                                @foreach($addoncategories as $addoncategory)
                                                    <option value="{{$addoncategory->id}}" 
                                                        @if(old('addon_category_id') == $addoncategory->id) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->addon_category_id == $addoncategory->id)
                                                            {{ 'selected' }} 
                                                        @endif
                                                        >{{strtoupper($addoncategory->name)}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>

                                {{-- premium --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Premium</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Premium" name="premium" id="premium" type="number" min="1" value="{{ $errors->any() ? old('premium') : $data->premium ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- benefits --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Benefits</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <textarea class="form-control" placeholder="Benefits" name="benefits" id="benefits" rows="5">{{ $errors->any() ? old('benefits') : $data->benefits ?? '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Sum Insured --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Sum Insured</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Sum Insured" name="sum_insured" id="sum_insured" type="number" min="1" value="{{ $errors->any() ? old('sum_insured') : $data->sum_insured ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- VAT -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">VAT</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="VAT" name="vat" id="sum_insured" type="number" min="1" value="{{ $errors->any() ? old('vat') : $data->vat ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fee -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Policy Fee</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                
                                                <select name="fee_type" class="form-control" >
                                                    <option value="1"
                                                    @if(old('fee_type') == 1) 
                                                        {{ 'selected' }} 
                                                    @elseif(isset($data) && $data->fee_type == '')
                                                        {{ 'selected' }} 
                                                    @endif
                                                    >Percent</option>
                                                    <option value="2">Amount
                                                        @if(old('fee_type') == 1) 
                                                            {{ 'selected' }} 
                                                        @elseif(isset($data) && $data->fee_type == '')
                                                            {{ 'selected' }} 
                                                        @endif
                                                    </option>
                                                </select>

                                            </div>
                                            <div class="col-sm-6">
                                                
                                                <input class="form-control" placeholder="Policy Fee" name="fee" id="fee" type="number" min="1" value="{{ $errors->any() ? old('fee') : $data->fee ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-4">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgCrossSellingProduct" src='{{ asset('uploads/admin/cross_selling_product').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 

                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            {{--  --}}
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/cross-selling-products')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $("#formCarPremium").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $("#image").change(function(){
             //$(this).attr('src','/new/image/src.jpg');   
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgCrossSellingProduct").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    //$("#imgCrossSellingProduct").hide();
                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgCrossSellingProduct").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        });
    });
</script>

<script type="text/javascript">
    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var name  = $.trim($("#name").val());
        var insuranceProviderId  = $.trim($("#insuranceProviderId").val());
        var addon_category_id  = $.trim($("#addon_category_id").val());
        var premium  = $.trim($("#premium").val());

        if (name == ''){

            $("#name").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;

        }

        if (addon_category_id == ''){

            $("#addon_category_id").parent().find(".my-error").html('Please select addon category').show();
            $('#addon_category_id').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (insuranceProviderId == ''){

            $("#insuranceProviderId").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insuranceProviderId').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (premium.length > 0){

            if(!$.isNumeric( premium ) || premium < 1){
                $("#premium").parent().find(".my-error").html('Please fill premium in numeric').show();
                $('#premium').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        else{
            $("#premium").parent().find(".my-error").html('Please fill premium').show();
            $('#premium').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
