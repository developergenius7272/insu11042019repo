@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Policy Details</h2>

                </header>

                
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formUpdateOrder" method="POST" @if($formType == 'edit') action="{{action('AdminOrderController@update', $data->id)}}" @else action="/admin/orders/" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Policy No</label>
                                    <div class="col-md-10">   
                                        <input class="form-control"  id ="policy_no" name="policy_no" type="number" value="{{ $errors->any() ? old('policy_no') : (isset($data->policy_number) ? $data->policy_number : '') }}" required>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Start Date</label>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="start_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->start_date) ? $data->start_date->format('d/m/Y H:i') : '') }}" placeholder="Start Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Expiry Date</label>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="end_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->end_date) ? $data->end_date->format('d/m/Y H:i') : '') }}" placeholder="End Date" id="datetimepicker1" class="form-control" autocomplete="off" readonly>

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Issue Date</label>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="issue_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->issue_date) ? $data->issue_date->format('d/m/Y H:i') : '') }}" placeholder="Issue Date" id="datetimepicker2" class="form-control" autocomplete="off" readonly>

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Upload</label>
                                    <div class="col-md-4">
                                        <input type="file" class="btn-default" id="image" name="image">

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
 

                                        @if($data->PolicyDoc)
                                            
                                            <a href="{{ asset('uploads/admin/policydoc'.'/'.$data->user->name.'/'.$data->PolicyDoc->docname)}}" id="imgFile" target="_blank">Download File</a>

                                        @endif

                                    </div>
                                </div>

                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Pending' , 1 => 'Processing' , 2 => 'Processed' )) @endphp

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Pending</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Processing</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            {{--  --}}
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.orders.index')}}" class="btn btn-default">Cancel</a>
                                        
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                            </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

{{-- <div id="agency_available_for_year_copy">
    <div class="form-group" id="min_premium_agency">
        <label class="control-label col-md-2">Year</label>
        <div class="col-md-10">
            <div class="row">
                <div class="col-sm-12">
                    <input class="form-control" placeholder="Available for years" name="available_for_year" type="text" min="1" value="">
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

        $("#formUpdateOrder").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }
        }));

        $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,

            onChangeDateTime:function(dp,$input){

              var t = $input.val();

                var ary= t.split(" ");
                var aryDate=ary[0].split("/");
                var aryTime=ary[1].split(":");

                var year = parseInt(aryDate[2]);
                var month = aryDate[1];
                var day = aryDate[0];

                var hour = aryTime[0];
                var minute = aryTime[1];

                if ((year & 3) == 0 && ((year % 25) != 0 || (year & 15) == 0))
                {
                    /* leap year */
                    if(day == 29){
                        day = day - 1;
                    }
                }

                year = parseInt(aryDate[2]) + parseInt(1);

                var convertedDate = new Date(year, month, day, hour, minute, 0, 0);
                //new Date(year, month, day, hours, minutes, seconds, milliseconds)

                // month = aryDate[1]>0?aryDate[1]-1:aryDate[1];
                // convertedDate.setMonth(month); 
                console.log(convertedDate);

                $("#datetimepicker1").val( day + "/" + month + "/" + year + " " + hour + ":" + minute );


            }

        });

        $('#datetimepicker1').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
        $('#datetimepicker2').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
        //
    });

    function getVariantByBodyType(carBodyTypeId,successCallback) {
        
        $.ajax({
            type:'POST',
            url:'{{route('admin.getVariantByBodyType')}}',
            data:{carBodyTypeId:carBodyTypeId},
            dataType: "html",
            success:function(data){
                $("#model_and_variants").empty().html(data);
                // 
                if (typeof successCallback === "function") {
                    successCallback()
                }
            }
        });
    }

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var policyNo  = $.trim($("#policy_no").val());
        var startDate  =$.trim($("#datetimepicker").val());
        var endDate  = $.trim($("#datetimepicker1").val());
        var issueDate  = $.trim($("#datetimepicker1").val()); 

        if (policyNo == ''){

            $("#policy_no").parent().find(".my-error").html('Please enter policy No').show();
            $('#policy_no').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

       
        if (min_premium_nonagency.length > 0){

            if(!$.isNumeric( min_premium_nonagency ) || min_premium_nonagency < 1){
                $("#min_premium_nonagency").parent().find(".my-error").html('Please fill min premium nonagency in numeric').show();
                $('#min_premium_nonagency').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

    }

       

    

    

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }


/*
    $(".formCarPremium").on('submit',(function(e) {

        var action = 0;

        var formType = "{{--$formType--}}";

        if (formType == 'add'){
            $.ajax(
            {
                type:'POST',
                //url:'/admin/retrievedatacarpremium',
                url:'{{--route('admin.getDataCarPremium')--}}',
                //data:{insuranceProviderId:insuranceProviderId,carBodyTypeIdVal:carBodyTypeIdVal},
                  data: new FormData(this),
                    processData: false,
                    contentType: false,
                  async : false,

                success: function(response){

                    response = $.parseJSON(response);
                    var status = response.status;
                    var msg = '';

                    if( status == 0) {

                        var name_insurance_provider = response.name_insurance_provider;

                        var name_car_model_body_type = response.implodeBodyType;

                        msg = "Insurance Provider : "+ name_insurance_provider + " and Car Model Body type : "+ name_car_model_body_type + " already exist, do you still want to update ?";
                        
                        if (confirm(msg)) {
                            
                            action = 1;
                            //return true;

                        } else {
                            
                            return false;
                        }

                    }

                    if( status == 1 ) {
                      
                      action = 1;

                    }

                }
            });

            if (action == 0){

                return false;
            }
        }
  }));
  */

</script>
@endsection
