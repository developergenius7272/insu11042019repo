@extends('layouts.admin.master')
{{-- Car model variants table view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Leads View</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" method="POST">
                            {{--  --}}
                            <fieldset>
                                <legend>Personal Information</legend>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Name</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->user ? strtoupper($data->user->name) : strtoupper($data->name) }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->user ? $data->user->email : $data->email }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Mobile</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->user ? $data->user->mobile : $data->mobile }}</label>
                                    </div>
                                </div>
                                {{--  --}}
                            </fieldset>
                            {{--  --}}
                            <fieldset>
                                <legend>Car Details</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Model Year</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->car_model_year }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Car Make</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->carMake ? $data->carMake->name : '' }}</label>
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Car Model</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->carModel ? strtoupper($data->carModel->name) : ''}}</label>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Car Model Details</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->carModelVariant ? strtoupper($data->carModelVariant->name) : ''}}</label>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Car Value</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->car_value ? env('CURRENCY_CODE', 'AED').' '.number_format($data->car_value,2) : '' }}</label>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Is New CarNo?</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->is_car_new === 1 ? 'Yes' : ($data->is_car_new === 0 ? 'No' : '')}}</label>
                                    </div>
                                </div>
                                @if( $data->is_car_new == 1)
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Reg. Date</label>
                                        <div class="col-md-6">

                                            <label class="col-md-4 lbl">{{ isset($data->car_registration_date) ? sqlDateDMY($data->car_registration_date,'-') : ''}}</label>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Reg. City</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{ $data->city_of_registration }}</label>
                                        </div>
                                    </div>
                                @endif
                            
                            </fieldset>

                            <fieldset>
                                <legend>About <b>{{ $data->user ? $data->user->name : $data->name }}</b></legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Is the vehicle modified or non-GCC spec?</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{$data->is_nongcc === 1 ? 'Yes' : ($data->is_nongcc === 0 ? 'No' : '')}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nationality</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->nationality }}</label>
                                    </div>
                                </div>                                       
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Country</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->country }}</label>
                                    </div>
                                </div>                                        
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Driving Experience (in years)</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->driving_experience }}</label>
                                    </div>
                                </div>                                          
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Driving in UAE (in years)</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ $data->driving_in_uae }}</label>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Date of Birth</label>
                                    <div class="col-md-6">
                                        <label class="col-md-4 lbl">{{ isset($data->dob) ? sqlDateDMY($data->dob,'-') : ''}}</label>
                                    </div>
                                </div>
                                
                            
                            </fieldset>

                            @if( $data->is_car_new == 0)
                                <fieldset>
                                    <legend>Renewal Details</legend>
                                    {{--  --}}
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Is the current policy Third Party Liability only?</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{$data->is_thirdpartyliablity === 1 ? 'Yes' : ($data->is_thirdpartyliablity === 0 ? 'No' : '')}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Does your current policy include Agency Repair?</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{$data->is_agencyrepair === 1 ? 'Yes' : ($data->is_agencyrepair === 0 ? 'No' : '')}}</label>
                                        </div>
                                    </div>                                       
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Has your insurance policy already expired?</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{$data->is_policyexpired === 1 ? 'Yes' : ($data->is_policyexpired === 0 ? 'No' : '')}}</label>
                                        </div>
                                    </div>                                       
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">When do you want to register your car?</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{ isset($data->register) ? date('d-m-Y h:m:s', strtotime($data->register)) : ''}}</label>
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Did you claim on your UAE car insurance in the last 12 months?</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{$data->is_claiminsurance === 1 ? 'Yes' : ($data->is_claiminsurance === 0 ? 'No' : '')}}</label>
                                        </div>
                                    </div>                                      
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Do you have a No Claims Certificate from your insurer(s)? Save up to 10%</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{$data->is_claimcertificate === 1 ? 'Yes' : ($data->is_claimcertificate === 0 ? 'No' : '')}}</label>
                                        </div>
                                    </div>                                     
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Number of Years for No Claim Certificate</label>
                                        <div class="col-md-6">
                                            <label class="col-md-4 lbl">{{ isset($data->claimCertificateYear) ? $data->claimCertificateYear : '' }}</label>
                                        </div>
                                    </div>  

                                </fieldset>
                            @endif

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection
<style>
    .lbl {padding-top:7px;}
</style>
@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 

    });


</script>
@endsection
