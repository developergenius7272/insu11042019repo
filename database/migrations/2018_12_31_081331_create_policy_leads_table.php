<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_leads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('car_model_year')->nullable();
            $table->integer('car_make_id')->nullable();
            $table->integer('car_model_id')->nullable();
            $table->integer('car_model_variant_id')->nullable();
            $table->integer('car_value')->nullable();
            $table->tinyInteger('is_car_new')->nullable();
            $table->date('car_registration_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_leads');
    }
}
