<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
use App\Models\CarPremium;
use App\Models\CarPremiumVariant;
use App\Models\CarPremiumVariantLog;
use App\Models\InsuranceProvider;
use App\Models\CarPremiumAgencyAvailableForYear;
// 
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class AdminCarPremiumController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Premiums'; 
        $this->data['pageTitle'] = 'Premiums';
        $this->data['can_update'] = true;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = "Partner's Plans";
        $plans = CarPremium::select('car_premiums.*')
                ->addSelect('insurance_providers.name as partner_name')
                ->addSelect('car_model_body_types.name as car_body_name')
                ->join('insurance_providers','insurance_providers.id', 'car_premiums.insurance_provider_id')
                ->leftJoin('car_model_body_types','car_model_body_types.id', 'car_premiums.car_model_body_type_id')
                ->orderBy('insurance_providers.name')
                ->get();
        $this->data['plans'] = $plans;
        return view('admin.car_premium.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a premium plan';
        $partners = InsuranceProvider::all();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        // 
        $this->data['partners'] = $partners;
        $this->data['carBodies'] = $carBodies;
        //
        return view('admin.car_premium.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $available_for_months = 0;

        $insuranceProviderId = $request->insuranceProviderId;
        $carBodyTypeId = $request->carBodyTypeId;
        //echo "<pre>";print_r($carBodyTypeId);die;
        $cylinders = $request->cylinders;
        $car_model_min_value = $request->car_model_min_value;
        $car_model_max_value = $request->car_model_max_value;
        // 
        $min_age = $request->min_age;
        $max_age = $request->max_age;
        // 
        $min_premium_agency = $request->min_premium_agency;
        $min_premium_nonagency = $request->min_premium_nonagency;
        // 
        $min_premium_agency_rate = $request->min_premium_agency_rate;
        $min_premium_nonagency_rate = $request->min_premium_nonagency_rate;
        $available_for_year = $request->available_for_year;
        $status = $request->status;
        $car_model_sub_type = $request->car_model_sub_type;
        $effective_date = $request->effective_date;//die;

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
            //echo $date = Carbon::parse($effective_date)->format('Y-m-d');die;
        }

        //Conditions
        // 1. General Conditions
        request()->validate([
            'insuranceProviderId' => 'required|numeric',
            "carBodyTypeId"    => "required|array|min:1",
            "effective_date"    => "required",
            "available_for_year"    => "required",

            //nullable, numeric, greater than zero condition
            'cylinders' => 'nullable|numeric|min:1',
            'car_model_min_value' => 'nullable|numeric|min:1',
            'car_model_max_value' => 'nullable|numeric|min:1',
            'min_age' => 'nullable|numeric|min:1',
            'max_age' => 'nullable|numeric|min:1',
            'min_premium_agency_rate' => 'nullable|numeric|min:0',
            'min_premium_agency' => 'nullable|numeric|min:1',
            'available_for_year' => 'required|numeric|min:1',
            'min_premium_nonagency_rate' => 'nullable|numeric|min:0',
            'min_premium_nonagency' => 'nullable|numeric|min:1',

        ], [
            'insuranceProviderId.required' => "Insurance Provider is required.",
            'carBodyTypeId.required' => "Car Type is required.",
            'effective_date.required' => "Effective Date is required.",
            'available_for_year.required' => "Please enter Available for years."
        ]);
        
        //convert year into months
        if (!empty($available_for_year)){
            $available_for_months = $available_for_year * 12;
            // $available_for_months += 2;

            //check available_for_year_rate not empty
            $available_for_year_rate = $request->available_for_year_rate;
            for($availableYearCounter=0;$availableYearCounter<$available_for_year;$availableYearCounter++){
                $available_for_year_rate[$availableYearCounter]='Free';
            }

            $co = count($available_for_year_rate);
            for ($k=0; $k < $co; $k++) {

                //if(empty($available_for_year_rate[$k]) || $available_for_year_rate[$k] <= 0){
                if(empty($available_for_year_rate[$k]) ){

                    $this->validate($request, [ 
                        'available_for_year_rate_not_blank' => 'required',
                    ],[
                        'available_for_year_rate_not_blank.required' => "Please fill Available for year ". ($k +1),
                    ]);

                }

                if( ($k + 1) > $available_for_year){
                    if(!is_numeric($available_for_year_rate[$k]) || $available_for_year_rate[$k] <= 0){

                        $this->validate($request, [ 
                            'available_for_year_rate_numeric' => 'required|numeric|min:1',
                        ],[
                            'available_for_year_rate_numeric.required' => "Please fill Available for year in numeric on ". ($k +1),
                        ]);

                    }
                }

            }
            
        }

        // 3. Car Model Max value should be greater than min value
        if($car_model_min_value > $car_model_max_value){
            $this->validate($request, [
                'car_model_max_value_greater' => 'required',
            ],[
                'car_model_max_value_greater.required' => "Car Model Max value should be greater than min value",
            ]);
        }

        // 3. Age Range Max value should be greater than min value
        if($min_age > $max_age){
            $this->validate($request, [
                'max_age_greater' => 'required',
            ],[
                'max_age_greater.required' => "Age Range Maximum Age should be greater than min value",
            ]);
        }

        //5.Effective date should be greater than current date
        $currentDate = Carbon::now();
        if($currentDate > $effective_date){
            $this->validate($request, [
                'effective_date_greater' => 'required',
            ],[
                'effective_date_greater.required' => "Effective date should be greater than current date",
            ]);
        }

        if(!empty( $insuranceProviderId ) && ( $carBodyTypeId ))
        {
            /*
			foreach ($carBodyTypeId as $val){

                $planExist = CarPremium::where('insurance_provider_id',$insuranceProviderId)
                                                        ->where('car_model_body_type_id',$val)->first();

                if($planExist){

                    $id = $planExist->id;

                    $plan = CarPremium::find($id);
                    if( $plan ) {

                        $plan->insurance_provider_id = $insuranceProviderId;
                        $plan->car_model_body_type_id = $val;
                        $plan->car_model_cylinder = $cylinders;
                        $plan->car_model_min_value = $car_model_min_value;
                        $plan->car_model_max_value = $car_model_max_value;
                        $plan->min_age = $min_age;
                        $plan->max_age = $max_age;
                        $plan->min_premium_agency = $min_premium_agency;
                        $plan->min_premium_nonagency = $min_premium_nonagency;
                        $plan->min_premium_agency_rate = $min_premium_agency_rate;
                        $plan->min_premium_nonagency_rate = $min_premium_nonagency_rate;
                        $plan->available_for_months = $available_for_months;
                        $plan->status = $status;
                        $plan->car_model_sub_type = $car_model_sub_type;
                        $plan->effective_date = $effective_date;
                        $plan->save();

                        //return redirect('/admin/partners/third-party-covers')->with('success', 'Information has been added');
                    }
                }
                else{

                    $plan = new CarPremium();
                    $plan->insurance_provider_id = $insuranceProviderId;
                    $plan->car_model_body_type_id = $val;
                    $plan->car_model_cylinder = $cylinders;
                    $plan->car_model_min_value = $car_model_min_value;
                    $plan->car_model_max_value = $car_model_max_value;
                    $plan->min_age = $min_age;
                    $plan->max_age = $max_age;
                    $plan->min_premium_agency = $min_premium_agency;
                    $plan->min_premium_nonagency = $min_premium_nonagency;
                    $plan->min_premium_agency_rate = $min_premium_agency_rate;
                    $plan->min_premium_nonagency_rate = $min_premium_nonagency_rate;
                    $plan->available_for_months = $available_for_months;
                    $plan->status = $status;
                    $plan->car_model_sub_type = $car_model_sub_type;
                    $plan->effective_date = $effective_date;
                    $plan->save();
                }

			}
            */
            foreach ($carBodyTypeId as $val){

                    $plan = new CarPremium();
                    $plan->insurance_provider_id = $insuranceProviderId;
                    $plan->car_model_body_type_id = $val;
                    $plan->car_model_cylinder = $cylinders;
                    $plan->car_model_min_value = $car_model_min_value;
                    $plan->car_model_max_value = $car_model_max_value;
                    $plan->min_age = $min_age;
                    $plan->max_age = $max_age;
                    $plan->min_premium_agency = $min_premium_agency;
                    $plan->min_premium_nonagency = $min_premium_nonagency;
                    $plan->min_premium_agency_rate = $min_premium_agency_rate;
                    $plan->min_premium_nonagency_rate = $min_premium_nonagency_rate;
                    $plan->available_for_months = $available_for_months;
                    $plan->status = $status;
                    $plan->car_model_sub_type = $car_model_sub_type;
                    $plan->effective_date = $effective_date;
                    $plan->created_by = $adminID;
                    $plan->modified_by = $adminID;

                    $plan->save();
                    $plan_id = $plan->id;

                    $co = count($available_for_year_rate);
                    for ($k=0; $k < $co; $k++) {
                        
                        $agencyYear = new CarPremiumAgencyAvailableForYear();
                        $agencyYear->car_premiums_id = $plan_id;
                        $agencyYear->year_number = $k+1;
                        $agencyYear->rate = $available_for_year_rate[$k];
                        $agencyYear->save();

                    }
                    
                    // plan variatns
                    $car_variants = $request->car_variants;
                    if( $car_variants <> null && $car_variants != '' ) {
                        if( is_array($car_variants) ) {
                            foreach( $car_variants as $car_variant ) {
                                $car = new CarPremiumVariant();
                                $car->car_model_variant_id = $car_variant;
                                $car->car_premium_id = $plan_id;
                                $car->save();
                            }
                        }
                    }
            }
            return redirect('/admin/partners/premium-plans')->with('success', 'Premium Plan added successfully');

        }else{
            return redirect('/admin/partners/premium-plans')->with('error', 'Premium Plan not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = CarPremium::find($id);
        $this->data['can_update'] = true;
        if( $plan->effective_date->lt(Carbon::now()) ) {
            // $this->data['can_update'] = false;
        }

        if( $plan ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update partner';
            // 
            $partners = InsuranceProvider::all();
            // 
            $this->data['partners'] = $partners;
            // 
            $this->data['data'] = $plan;

            if( $plan ){
                $agency_years = CarPremiumAgencyAvailableForYear::select('car_premium_agency_available_for_years.*')
                ->leftJoin('car_premiums','car_premiums.id', 'car_premium_agency_available_for_years.car_premiums_id')
                ->where('car_premium_agency_available_for_years.car_premiums_id',$plan->id)
                ->orderBy('year_number', 'asc')
                ->get();
                $this->data['agency_years'] = $agency_years;
            }
            // 
            return view('admin.car_premium.create_edit', $this->data);
        } else {
            return redirect('/admin/partners/premium-plans')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $plan = CarPremium::find($id);
        if( $plan ) {
            // can not udpate passed date plan
            // 
            if( $plan->effective_date->lt(Carbon::now()) ) {
                // return redirect('/admin/partners/premium-plans')->with('error', 'Can not udpate passed date plan');
            }

            $available_for_months = 0;

            $insuranceProviderId = $request->insuranceProviderId;
            // 
            // $carBodyTypeId = $request->carBodyTypeId;
            $cylinders = $request->cylinders;
            $car_model_min_value = $request->car_model_min_value;
            $car_model_max_value = $request->car_model_max_value;
            // 
            $min_age = $request->min_age;
            $max_age = $request->max_age;
            // 
            $min_premium_agency = $request->min_premium_agency;
            $min_premium_nonagency = $request->min_premium_nonagency;
            // 
            $min_premium_agency_rate = $request->min_premium_agency_rate;
            $min_premium_nonagency_rate = $request->min_premium_nonagency_rate;
            $available_for_year = $request->available_for_year;
            $status = $request->status;
            $car_model_sub_type = $request->car_model_sub_type;
            $effective_date = $request->effective_date;

            if( !empty($effective_date) ) {
                $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
            }

            request()->validate([
                'insuranceProviderId' => 'required|numeric',
                // 'carBodyTypeId' => 'required|numeric',
                'effective_date' => 'required',
                'available_for_year' => 'required',

                //nullable, numeric, greater than zero condition
                'cylinders' => 'nullable|numeric|min:1',
                'car_model_min_value' => 'nullable|numeric|min:1',
                'car_model_max_value' => 'nullable|numeric|min:1',
                'min_age' => 'nullable|numeric|min:1',
                'max_age' => 'nullable|numeric|min:1',
                'min_premium_agency_rate' => 'nullable|numeric|min:0',
                'min_premium_agency' => 'nullable|numeric|min:1',
                'available_for_year' => 'required|numeric|min:1',
                'min_premium_nonagency_rate' => 'nullable|numeric|min:0',
                'min_premium_nonagency' => 'nullable|numeric|min:1',

            ], [
                'insuranceProviderId.required' => "Insurance Provider is required.",
                // 'carBodyTypeId.required' => "Car Type is required.",
                'effective_date.required' => "Effective Date is required.",
                'available_for_year.required' => "Please enter Available for years.",
            ]);

            //convert year into months
            if (!empty($available_for_year)){
                $available_for_months = $available_for_year * 12;
                // $available_for_months += 2;

                //check available_for_year_rate not empty
                $available_for_year_rate = $request->available_for_year_rate;
            
                for($availableYearCounter=0;$availableYearCounter<$available_for_year;$availableYearCounter++){
                    $available_for_year_rate[$availableYearCounter]='Free';
                }

                $co = count($available_for_year_rate);
                for ($k=0; $k < $co; $k++) {

                    //if(empty($available_for_year_rate[$k]) || $available_for_year_rate[$k] <= 0){
                    if(empty($available_for_year_rate[$k]) ){

                        $this->validate($request, [ 
                            'available_for_year_rate_not_blank' => 'required',
                        ],[
                            'available_for_year_rate_not_blank.required' => "Please fill Available for year ". ($k +1),
                        ]);

                    }

                    if( ($k + 1) > $available_for_year){
                        if(!is_numeric($available_for_year_rate[$k]) || $available_for_year_rate[$k] <= 0){

                            $this->validate($request, [ 
                                'available_for_year_rate_numeric' => 'required|numeric|min:1',
                            ],[
                                'available_for_year_rate_numeric.required' => "Please fill Available for year in numeric on ". ($k +1),
                            ]);

                        }
                    }

                }
            }
            
            //5.Effective date should be greater than current date
            
            $old_effective_date = $plan->effective_date;

            $currentDate = Carbon::now();
            if($old_effective_date != $effective_date){

                $currentDate = Carbon::now();
                if($currentDate > $effective_date){
                    $this->validate($request, [
                        'effective_date_greater' => 'required',
                    ],[
                        'effective_date_greater.required' => "Effective date should be greater than current date",
                    ]);
                }

            }

            if(!empty( $insuranceProviderId ))
            {
                $plan->insurance_provider_id = $insuranceProviderId;
                // $plan->car_model_body_type_id = $carBodyTypeId;
                $plan->car_model_cylinder = $cylinders;
                $plan->car_model_min_value = $car_model_min_value;
                $plan->car_model_max_value = $car_model_max_value;
                $plan->min_age = $min_age;
                $plan->max_age = $max_age;
                $plan->min_premium_agency = $min_premium_agency;
                $plan->min_premium_nonagency = $min_premium_nonagency;
                $plan->min_premium_agency_rate = $min_premium_agency_rate;
                $plan->min_premium_nonagency_rate = $min_premium_nonagency_rate;
                $plan->available_for_months = $available_for_months;
                $plan->status = $status;
                $plan->car_model_sub_type = $car_model_sub_type;
                $plan->effective_date = $effective_date;
                $plan->modified_by = $adminID;
                $plan->save();
                $plan_id = $plan->id;

                CarPremiumAgencyAvailableForYear::where('car_premiums_id',$plan_id)->delete();
                $co = count($available_for_year_rate);
                for ($k=0; $k < $co; $k++) {
                    
                    $agencyYear = new CarPremiumAgencyAvailableForYear();
                    $agencyYear->car_premiums_id = $plan_id;
                    $agencyYear->year_number = $k+1;
                    $agencyYear->rate = $available_for_year_rate[$k];
                    $agencyYear->save();

                }
                // plan variatns
                $car_variants = $request->car_variants;
                if( $car_variants <> null && $car_variants != '' ) {
                    if( is_array($car_variants) ) {
                        // 
                        CarPremiumVariant::where('car_premium_id',$plan_id)->update(['status' => 99]);
                        // 
                        foreach( $car_variants as $car_variant ) {
                            $vatiantCheck = CarPremiumVariant::firstOrNew(['car_model_variant_id'=>$car_variant,'car_premium_id'=>$plan_id]);
                                $vatiantCheck->status = 1;
                                $vatiantCheck->save();
                        }
                        

                        $deleteRows = CarPremiumVariant::where('car_premium_id',$plan_id)->where('status',99)->get();
                        if( $deleteRows->count() ) {
                            $deleteData = [];
                            $now = Carbon::now()->toDateTimeString();
                            foreach( $deleteRows as $deleteRow ) {

                                $deleteData[] = [ 
                                    'car_premium_variant_id' => $deleteRow->id,
                                    'car_model_variant_id' => $deleteRow->car_model_variant_id,
                                    'car_premium_id' => $deleteRow->car_premium_id,
                                    'deleted_by' => $adminID,
                                    'created_at' => $now,
                                    'updated_at' => $now
                                ];
                            }
                            if( count($deleteData) ) {
                                
                                CarPremiumVariantLog::insert($deleteData);
                            }
                            CarPremiumVariant::where('car_premium_id',$plan_id)->where('status',99)->delete();
                        }
                    }
                }

                return redirect('/admin/partners/premium-plans')->with('success', 'Premium Plan updated successfully');
            
            }
        }else{
            return redirect('/admin/partners/premium-plans')->with('error', 'Premium Plan not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataCarPremium(Request $request) {
        
        $response = array();

        $insuranceProviderId = $request->insuranceProviderId;
        //$carBodyTypeIdVal = $request->carBodyTypeIdVal;
        $carBodyTypeIdVal = $request->carBodyTypeId;

        $hasEntry = 0;
        $body_type = array();

        if(empty($insuranceProviderId)){
            //echo "yes";
            $response['status'] =  1;
            echo json_encode( $response );
            exit();   
        }

        if($carBodyTypeIdVal )
        {

            //echo "yes";die;
            foreach ($carBodyTypeIdVal as $val){

                $planExist = CarPremium::where('insurance_provider_id',$insuranceProviderId)
                                                        ->where('car_model_body_type_id',$val)->first();
                
                //echo "<pre>";print_r($planExist);die;
                if($planExist){
                    //echo "Yes";die;
                    $hasEntry = 1;
                    $insurance_provider = InsuranceProvider::where('id',$planExist->insurance_provider_id)->first();
                    $car_model_body_type = CarModelBodyType::where('id',$planExist->car_model_body_type_id)->first();

                    $body_type[] = $car_model_body_type->name;

                    $response['status'] =  0;
                    $response['name_insurance_provider'] =  $insurance_provider->name;
                    //$response['name_car_model_body_type'] =  $body_type;
                    
                    // echo json_encode( $response );
                    // exit();      

                }

            }

            if($hasEntry == 1){
                $implodeBodyType = implode(",", $body_type);
                $response['implodeBodyType'] =  $implodeBodyType;                
            }
            else{
                //echo "Noaa";die;
                $response['status'] =  1;
            }


        }
        else{
            //echo "ok";die;
            $response['status'] =  1;//Please Select Car Model Body Type
        }

        echo json_encode( $response );
        exit();      
    }
    
}
