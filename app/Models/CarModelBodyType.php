<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CarPremium;
use App\Models\CarThirdPartyCover;
use App\Models\Excess;

class CarModelBodyType extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * accounts for user
     */
    public function keyword()
    {
        return $this->hasMany('App\Models\CarModelBodyTypeKeyword', 'car_model_body_type_id', 'id');
    }

    /**
     * Scope a query to only include active makes.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('car_model_body_types.status',1);
    }

    public function carPremium()
    {
        return $this->hasMany(CarPremium::class, 'car_model_body_type_id', 'id');
    }

    public function carThirdPartyCover()
    {
        return $this->hasMany(CarThirdPartyCover::class, 'car_model_body_type_id', 'id');
    }

    public function excess()
    {
        return $this->hasMany( Excess::class, 'car_model_body_type_id', 'id');
    }

    public function carModelVariant()
    {
        return $this->hasMany('App\Models\CarModelVariant');
    }
}
