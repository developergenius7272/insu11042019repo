<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPolicyLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('policy_leads', function(Blueprint $table) {
            $table->string('city_of_registration')->after('car_registration_date')->nullable();
            
            $table->tinyInteger('is_nongcc')->after('car_registration_date')->nullable();
            $table->string('nationality')->after('is_nongcc')->nullable();
            $table->string('country')->after('nationality')->nullable();
            $table->string('driving_experience')->after('country')->nullable();
            $table->string('driving_in_uae')->after('driving_experience')->nullable();
            $table->date('dob')->after('driving_in_uae')->nullable();

            $table->tinyInteger('is_thirdpartyliablity')->after('dob')->nullable();
            $table->tinyInteger('is_agencyrepair')->after('is_thirdpartyliablity')->nullable();
            $table->tinyInteger('is_policyexpired')->after('is_agencyrepair')->nullable();
            $table->datetime('register')->after('is_policyexpired')->nullable();
            $table->tinyInteger('is_claiminsurance')->after('register')->nullable();
            $table->tinyInteger('is_claimcertificate')->after('is_claiminsurance')->nullable();
            $table->year('claimCertificateYear')->after('is_claimcertificate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
