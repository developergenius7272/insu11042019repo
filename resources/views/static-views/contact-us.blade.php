@extends('layouts.master')
@section('content')


<!-- Who We Are Section Start -->
<section class="who-we-are-section">
  <div class="container">
    <div class="col-12">
      
      <div class="who-we-are-content">
        <h2>Contact Us</h2>
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justm. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p> 
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
      </div>


    </div>
  </div>
</section>
<!-- Who We Are Section End -->

<!-- Why Insurance Section Start -->
<section class="why-insurance-section">
  <div class="container">
    <div class="col-12">

     <div class="why-insurance-inner">
        <h2>Why Car Insurance</h2>
     </div>
      
      <div class="row">
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">01</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">02</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="why-insurance-point">
            <div class="why-point-count">03</div>
            <div class="why-point-content">
              <h4>Lorem ipsum dolor</h4>
              <p>Lorem ipsum dolor sit amet, selitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Vero eos et accusam et justo.</p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- Why Insurance Section End -->
@endsection