<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStandardSuperior extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_model_variants', function (Blueprint $table) {
            $table->tinyInteger('sub_type')->after('car_model_fuel_id')->default(1)->nullable()->comment = '1 = Standard, 2 = Superior';
        });
        Schema::table('car_premiums', function (Blueprint $table) {
            $table->tinyInteger('car_model_sub_type')->after('min_premium_nonagency_rate')->default(0)->nullable()->comment = '0 = N/A, 1 = Standard, 2 = Superior';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
