<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BenefitMaster;
use App\Http\Resources\PlanBenefitMultiple;

class PlanBenefit extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
          //  'insurance_providers_id' => $this->insurance_providers_id,
            'year' => $this->year,
            'effective_date' => $this->effective_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'benifitMaster' => new BenefitMaster($this->benefitMaster),
            'planBenefitMultiple' => new PlanBenefitMultiple(new $this->planBenefitMultiple),
        ];
    }
}
