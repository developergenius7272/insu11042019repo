@component('mail::message')
Hello {{ $user->name }},

Thank you for buying car insurance from InsureOnline. Below is the Invoice details.
<table>
    <tr>
        <td>
            Premium: AED {{ $order->premium_value }}
        </td>
    </tr>
    <tr>
        <td>
            Personal Accidental Benefit (Passenger): AED {{ $order->pab_passenger }}
        </td>
    </tr>
    <tr>
        <td>
            Personal Accidental Benefit (Driver): AED {{ $order->pab_driver }}
        </td>
    </tr>
     {{--  <tr>
        <td>
            Total Addon: AED @if(!empty($order->addon_amount))
                                {{ $order->addon_amount }}
                            @else
                                {{ "0.0" }}
                            @endif
        </td>
    </tr>  --}}
    <tr>
        <td>
            VAT (5%) : AED {{$order->vat_premium}}
        </td>
    </tr>
    <tr>
        <td>
            Policy Fees: AED {{ $order->policy_fees }}
        </td>
    </tr>
    <tr>
        <td>
            Total Payment: AED {{ $order->total }}
        </td>
    </tr>
    {{--  Additional products Details:
    <tr><th>Benefit Name</th>
    <th>price</th></tr>
        @foreach( $order->additionalCovers as $addon )
        <tr>
        <td> {{ $order->crossSellingProduct->where('id', $addon->cross_selling_product_id)->first()->name }} </td>
        <td>AED {{ $addon->premium_value }}</td>
         </tr>
        @endforeach  --}}
</table>
<br>


Thanks,<br>
{{ config('app.name') }}
@endcomponent