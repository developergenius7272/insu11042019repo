<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalCover extends Model
{
    protected $table = 'additionalcovers';

    public function orders(){
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    public function crossSellingProduct(){
        return $this->belongsTo('App\Models\CrossSellingProducts', 'cross_selling_product_id', 'id');
    }

}
