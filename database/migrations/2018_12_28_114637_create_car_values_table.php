<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_make_id');
            $table->integer('car_model_id')->nullable();
            $table->integer('car_model_variant_id')->nullable();
            $table->double('values')->default(0)->nullable();
            $table->double('min_value')->default(0)->nullable();
            $table->double('max_value')->default(0)->nullable();
            $table->year('insurance_year')->nullable();
            $table->year('manufacture_year')->nullable();
            $table->datetime('effective_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('car_values');
    }
}
