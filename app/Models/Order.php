<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrdersCreated;
use App\Mail\InvoiceMail;
use App\Mail\AdditionalProductInvoice;
use Carbon\Carbon;


class Order extends Model
{
    protected $fillable = [
        'insurance_providers_id',  'benefit_options', 'users_id', 'total', 'vat_premium', 'policy_fees',
    ];

    protected $dates = ['start_date','end_date','issue_date'];

    public function insuranceProvider()
    {
        return $this->belongsTo('App\Models\InsuranceProvider', 'insurance_providers_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function policyLead() {
        return $this->hasOne('App\Models\PolicyLead', 'order_id', 'id');
    }

    public static function sendOrderCreatedNotification($user, $orders)
    {
        Mail::to($user->email)->send(new OrdersCreated($user, $orders));
    }

    public static function sendInvoiceNotification($user, $order)
    {
        Mail::to($user->email)->send(new InvoiceMail($user, $order));
    }
    public function agent() {
        return $this->belongsTo('App\User', 'created_by', 'id');

    }
    public function additionalCovers(){
        return $this->hasMany('App\Models\AdditionalCover','order_id','id');
    }
    public function crossSellingProduct()
    {
        return $this->hasManyThrough('App\Models\CrossSellingProducts','App\Models\AdditionalCover','order_id','id','id','cross_selling_product_id');
    }

    public function scopeExpiredDateGreaterThan($query)
    {
        $current_date = Carbon::now()->toDateTimeString();
        return $query->where('end_date','>=',$current_date);
    }
    public function scopeExpiredDateLessThan($query)
    {
        $current_date = Carbon::now()->toDateTimeString();
        return $query->where('end_date','<',$current_date);
    }

    public function PolicyDoc() {
        return $this->hasOne('App\Models\PolicyDoc', 'order_id', 'id');
    }

    public function addOnOrder(){
        return $this->hasOne('App\Models\AddonOrder', 'parent_order_id', 'id');
    }

    //mail sending function for additional products
    public static function sendAddOnInvoiceNotification($user, $order)
    {
        Mail::to($user->email)->send(new AdditionalProductInvoice($user, $order));
    }
    public function upload(){
        return $this->hasMany('App\Models\Upload','order_id','id');
    }
}