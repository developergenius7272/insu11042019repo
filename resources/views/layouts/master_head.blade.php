<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Title -->
<title>Insure Online</title>
<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#223367">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#223367">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#223367">
<!-- Favicon -->
<link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
<!-- CSS -->
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/template.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}">
<link rel="stylesheet" href="{{ URL::asset('js/owlcarousel/owl.carousel.css') }}">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>