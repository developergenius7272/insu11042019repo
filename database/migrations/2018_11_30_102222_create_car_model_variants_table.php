<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// 
use App\Models\CarModelVariant;

class CreateCarModelVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_model_variants');
        Schema::create('car_model_variants', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('car_make_id');
            $table->integer('car_model_id')->nullable();
            $table->string('name')->nullable();
            $table->year('make_year')->nullable();
            $table->integer('car_model_body_type_id')->nullable();
            // $table->integer('car_model_cylinder_id')->nullable();
            $table->tinyInteger('cylinders')->nullable();
            $table->integer('car_model_fuel_id')->nullable();
            $table->integer('car_model_engine_size_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_model_variants');
    }
}
