<?php

namespace App\Http\Controllers;
// 
use App\User;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
use App\Models\InsuranceProvider;
use App\Models\UserAgentCommission;
use App\Models\UserAgentAddonCommission;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminInsuranceProviderController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Partners'; 
        $this->data['pageTitle'] = 'Partners';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Partners';
        //$partners = InsuranceProvider::all();
        $partners = InsuranceProvider::orderBy('name', 'asc')->get();

        $this->data['partners'] = $partners;
        return view('admin.insurance_providers.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a partner';
        // 
        $carMakes = CarMake::active()->get();
        $carBodies = CarModelBodyType::active()->orderBy('name','asc')->get();
        $carFuels = CarModelFuel::active()->orderBy('name','asc')->get();
        // 
        $this->data['carMakes'] = $carMakes;
        $this->data['carBodies'] = $carBodies;
        $this->data['carFuels'] = $carFuels;
        //
        return view('admin.insurance_providers.create_edit', $this->data); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name                             = trim($request->name);
        $email                            = trim($request->email); // unique
        $webUrl                           = trim($request->web_url);
        $contactNumber                    = trim($request->contact_number);
        $description                      = trim($request->description);
        $recommended_partner              = $request->recommended_partner;
        // 
        $base_commission_percentage       = $request->base_commission_percentage;
        $base_commission_amount           = $request->base_commission_amount;
        // 
        $addon_base_commission_percentage = $request->addon_base_commission_percentage;
        $addon_base_commission_amount     = $request->addon_base_commission_amount;
        // 
        $agent_base_commission_percentage = $request->agent_base_commission_percentage;
        $agent_base_commission_amount     = $request->agent_base_commission_amount;
        // 
        $agent_addon_base_commission_percentage = $request->agent_addon_base_commission_percentage;
        $agent_addon_base_commission_amount     = $request->agent_addon_base_commission_amount;
        // 
        $bonus_percentage                 = $request->bonus_percentage;
        $bonus_amount                     = $request->bonus_amount;
        $sale_amount_limit_bonus          = $request->sale_amount_limit_bonus;
        $policy_fees_percentage           = $request->policy_fees_percentage;
        $policy_fees_amount               = $request->policy_fees_amount;
        $upto_max_amount                  = $request->upto_max_amount;
        $only_tpl_after_years             = $request->only_tpl_after_years;
        $status                           = $request->status;

        //General Condition

        request()->validate([
            'name' => 'required|unique:insurance_providers|min:2|max:100',
            //'base_commission_percentage' => 'numeric',

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
            //'base_commission_percentage.numeric' => "Base Commission's percentage should be numeric.",
        ]);

        // Other Conditions
        //1.
        if(empty($recommended_partner)){
            $recommended_partner = 0;
        }

        //2.
        if(empty($base_commission_percentage)){
            $base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($base_commission_percentage)){
                $this->validate($request, [
                    'base_commission_percentage_numeric' => 'required',
                ],[
                    'base_commission_percentage_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
            else if($base_commission_percentage > 100){

                $this->validate($request, [
                    'base_commission_percentage_greater' => 'required',
                ],[
                    'base_commission_percentage_greater.required' => "Please enter Base Commission's percentage not more than 100",
                ]);
            }
        }

        //3.
        if(empty($base_commission_amount)){
            $base_commission_amount = 0;
        }
        else{
            if(!is_numeric($base_commission_amount)){
                $this->validate($request, [
                    'base_commission_amount_numeric' => 'required',
                ],[
                    'base_commission_amount_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
        }
        // 4. Addon base commission percentage checking
        if(empty($addon_base_commission_percentage)){
            $addon_base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($addon_base_commission_percentage)){
                $this->validate($request, [
                    'addon_base_commission_percentage_numeric' => 'required',
                ],[
                    'addon_base_commission_percentage_numeric.required' => "Addon Base Commission's percentage should be numeric",
                ]);
            }
            else if($addon_base_commission_percentage > 100){

                $this->validate($request, [
                    'addon_base_commission_percentage_greater' => 'required',
                ],[
                    'addon_base_commission_percentage_greater.required' => "Please enter AddonBase Commission's percentage not more than 100",
                ]);
            }
        }
        // 5. Addon base commission amount checking
        if(empty($addon_base_commission_amount)){
            $addon_base_commission_amount = 0;
        }
        else{
            if(!is_numeric($addon_base_commission_amount)){
                $this->validate($request, [
                    'addon_base_commission_amount_numeric' => 'required',
                ],[
                    'addon_base_commission_amount_numeric.required' => "Addon Base Commission's percentage should be numeric",
                ]);
            }
        }
        // 
        // 
        if(empty($agent_base_commission_percentage)){
            $agent_base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($agent_base_commission_percentage)){
                $this->validate($request, [
                    'agent_base_commission_percentage_numeric' => 'required',
                ],[
                    'agent_base_commission_percentage_numeric.required' => "Agent Base Commission's percentage should be numeric",
                ]);
            }
            else if($agent_base_commission_percentage > 100){

                $this->validate($request, [
                    'agent_base_commission_percentage_greater' => 'required',
                ],[
                    'agent_base_commission_percentage_greater.required' => "Please enter Agent Base Commission's percentage not more than 100",
                ]);
            }
        }

        if(empty($agent_base_commission_amount)){
            $agent_base_commission_amount = 0;
        }
        else{
            if(!is_numeric($agent_base_commission_amount)){
                $this->validate($request, [
                    'agent_base_commission_amount_numeric' => 'required',
                ],[
                    'agent_base_commission_amount_numeric.required' => "Agent Base Commission's percentage should be numeric",
                ]);
            }
        }

        if(empty($agent_addon_base_commission_amount)){
            $agent_addon_base_commission_amount = 0;
        }
        else{
            if(!is_numeric($agent_addon_base_commission_amount)){
                $this->validate($request, [
                    'agent_addon_base_commission_amount_numeric' => 'required',
                ],[
                    'agent_addon_base_commission_amount_numeric.required' => "Agent Addon Commission's percentage should be numeric",
                ]);
            }
        }

        // 
        // 
        // 

        //4.
        if(empty($bonus_percentage)){
            $bonus_percentage = 0;
        }
        else{
            if(!is_numeric($bonus_percentage)){
                $this->validate($request, [
                    'bonus_percentage_numeric' => 'required',
                ],[
                    'bonus_percentage_numeric.required' => "Bonus's percentage should be numeric",
                ]);
            }
            else if($bonus_percentage > 100){

                $this->validate($request, [
                    'bonus_percentage_greater' => 'required',
                ],[
                    'bonus_percentage_greater.required' => "Please enter Bonus's percentage not more than 100",
                ]);
            }
        }

        //5.
        if(empty($bonus_amount)){
            $bonus_amount = 0;
        }
        else{
            if(!is_numeric($bonus_amount)){
                $this->validate($request, [
                    'bonus_amount_numeric' => 'required',
                ],[
                    'bonus_amount_numeric.required' => "Bonus's percentage should be numeric",
                ]);
            }
        }

        //5.a.
        if(empty($sale_amount_limit_bonus)){
            $sale_amount_limit_bonus = 0;
        }
        else{
            if(!is_numeric($sale_amount_limit_bonus)){
                $this->validate($request, [
                    'sale_amount_limit_bonus_numeric' => 'required',
                ],[
                    'sale_amount_limit_bonus_numeric.required' => "Sale amount limit bonus to get bonus field should be numeric",
                ]);
            }
        }

        //6.
        if(empty($policy_fees_percentage)){
            $policy_fees_percentage = 0;
        }
        else{
            if(!is_numeric($policy_fees_percentage)){
                $this->validate($request, [
                    'policy_fees_percentage_numeric' => 'required',
                ],[
                    'policy_fees_percentage_numeric.required' => "Policy Fees's percentage should be numeric",
                ]);
            }
            else if($policy_fees_percentage > 100){

                $this->validate($request, [
                    'policy_fees_percentage_greater' => 'required',
                ],[
                    'policy_fees_percentage_greater.required' => "Please enter Policy Fees's percentage not more than 100",
                ]);
            }
        }

        //7.
        if(empty($policy_fees_amount)){
            $policy_fees_amount = 0;
        }
        else{
            if(!is_numeric($policy_fees_amount)){
                $this->validate($request, [
                    'policy_fees_amount_numeric' => 'required',
                ],[
                    'policy_fees_amount_numeric.required' => "Policy Fees's percentage should be numeric",
                ]);
            }
        }

        //8.
        if(empty($upto_max_amount)){
            $upto_max_amount = 0;
        }
        else{
            if(!is_numeric($upto_max_amount)){
                $this->validate($request, [
                    'upto_max_amount_numeric' => 'required',
                ],[
                    'upto_max_amount_numeric.required' => "Upto max amount should be numeric",
                ]);
            }
        }

        if(!empty( $name ))
        {
            //$partnerExist = InsuranceProvider::where('email', $email)->first();
            $partnerExist = InsuranceProvider::where('name', $name)->first();
            if( !$partnerExist ) {
                $partner                                   = new InsuranceProvider();
                $partner->email                            = $email;
                $partner->name                             = $name;
                $partner->web_url                          = $webUrl;
                $partner->contact_number                   = $contactNumber;
                $partner->description                      = $description;
                $partner->recommended_partner              = $recommended_partner;
                // 
                $partner->base_commission_percentage       = $base_commission_percentage;
                $partner->base_commission_amount           = $base_commission_amount;
                // 
                $partner->addon_base_commission_percentage       = $addon_base_commission_percentage;
                $partner->addon_base_commission_amount           = $addon_base_commission_amount;
                // 
                $partner->agent_base_commission_percentage = $agent_base_commission_percentage;
                $partner->agent_base_commission_amount     = $agent_base_commission_amount;
                // 
                $partner->agent_addon_base_commission_percentage = $agent_addon_base_commission_percentage;
                $partner->agent_addon_base_commission_amount     = $agent_addon_base_commission_amount;
                // 
                $partner->bonus_percentage                 = $bonus_percentage;
                $partner->bonus_amount                     = $bonus_amount;
                $partner->sale_amount_limit_bonus          = $sale_amount_limit_bonus;
                $partner->policy_fees_percentage           = $policy_fees_percentage;
                $partner->policy_fees_amount               = $policy_fees_amount;
                $partner->upto_max_amount                  = $upto_max_amount;
                $partner->only_tpl_after_years             = $only_tpl_after_years;
                $partner->status                           = $status;
                $partner->created_by                       = $adminID;
                $partner->modified_by                      = $adminID;
                // $partner->save();

                // upload image
                $imageName = '';
                try{
                    if ($request->hasFile('image')) { 
                        $imageName         = $partner->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName         = $partner->id.'_main';
                        $request->image->move(public_path('uploads/admin/insurance_providers'), $imageName);
                        // resize for logo
                        $imageLogo         = $partner->id.'_logo';
                        $img               = Image::make(public_path('uploads/admin/insurance_providers/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/admin/insurance_providers/' . $imageLogo));
                        $partner->has_logo = 1;
                    }
                }
                catch(\Exception $e){
                    // do task when error
                    // echo $e->getMessage();
                }
                // 
                $partner->save();
                // set all agents base commision for this partner
                if( $partner->agent_base_commission_percentage > 0 || $partner->agent_base_commission_amount > 0 ) {
                    $agents = User::withRole('agent')->get();
                    if($agents->count()) {
                        foreach($agents as $agent) {
                            $commision                        = new UserAgentCommission();
                            $commision->user_id               = $agent->id;
                            $commision->insurance_provider_id = $partner->id;
                            $commision->percentage_or_amount  = $partner->agent_base_commission_percentage > 0 ? 1 : 2;
                            $commision->amount                = $partner->agent_base_commission_percentage > 0 ? $partner->agent_base_commission_percentage : $partner->agent_base_commission_amount ;
                            $commision->save();

                           
                        }
                    }
                }
                // 

                return redirect()->route('admin.partners.index')->with('success', 'Partners added successfully');
            } else {
                return redirect()->route('admin.partners.index')->with('error', 'Partners not added');
            }
        } else {
            return redirect()->route('admin.partners.index')->with('error', 'Partners not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Update partner';
        // 
        $partnerExist = InsuranceProvider::where('id', $id)->first();
        $this->data['data'] = $partnerExist;

        if( $this->data['data'] ) {
            // 
            return view('admin.insurance_providers.create_edit', $this->data);
        } else {
            return redirect('/admin/partners')->with('error', 'No data found');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name                             = trim($request->name);
        $email                            = trim($request->email); // unique
        $webUrl                           = trim($request->web_url);
        $contactNumber                    = trim($request->contact_number);
        $description                      = trim($request->description);
        $recommended_partner              = $request->recommended_partner;

        $base_commission_percentage       = $request->base_commission_percentage;
        $base_commission_amount           = $request->base_commission_amount;
        // 
        $addon_base_commission_percentage = $request->addon_base_commission_percentage;
        $addon_base_commission_amount     = $request->addon_base_commission_amount;
        // 
        $agent_base_commission_percentage = $request->agent_base_commission_percentage;
        $agent_base_commission_amount     = $request->agent_base_commission_amount;
        // 
        // 
        $agent_addon_base_commission_percentage = $request->agent_addon_base_commission_percentage;
        $agent_addon_base_commission_amount     = $request->agent_addon_base_commission_amount;
        // 
        $bonus_percentage                 = $request->bonus_percentage;
        $bonus_amount                     = $request->bonus_amount;
        $sale_amount_limit_bonus          = $request->sale_amount_limit_bonus;
        $policy_fees_percentage           = $request->policy_fees_percentage;
        $policy_fees_amount               = $request->policy_fees_amount;
        $upto_max_amount                  = $request->upto_max_amount;
        $only_tpl_after_years             = $request->only_tpl_after_years;
        $status                           = $request->status;
        // 

        //1.General Conditions
        request()->validate([
            'name' => "required|unique:insurance_providers,name,$id|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        // Other Conditions
        //2.
        if(empty($base_commission_percentage)){
            $base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($base_commission_percentage)){
                $this->validate($request, [
                    'base_commission_percentage_numeric' => 'required',
                ],[
                    'base_commission_percentage_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
            else if($base_commission_percentage > 100){

                $this->validate($request, [
                    'base_commission_percentage_greater' => 'required',
                ],[
                    'base_commission_percentage_greater.required' => "Please enter Base Commission's percentage not more than 100",
                ]);
            }
        }

        //3.
        if(empty($base_commission_amount)){
            $base_commission_amount = 0;
        }
        else{
            if(!is_numeric($base_commission_amount)){
                $this->validate($request, [
                    'base_commission_amount_numeric' => 'required',
                ],[
                    'base_commission_amount_numeric.required' => "Base Commission's percentage should be numeric",
                ]);
            }
        }
        // 
        // 
        // 
        if(empty($agent_base_commission_percentage)){
            $agent_base_commission_percentage = 0;
        }
        else{
            if(!is_numeric($agent_base_commission_percentage)){
                $this->validate($request, [
                    'agent_base_commission_percentage_numeric' => 'required',
                ],[
                    'agent_base_commission_percentage_numeric.required' => "Agent Base Commission's percentage should be numeric",
                ]);
            }
            else if($agent_base_commission_percentage > 100){

                $this->validate($request, [
                    'agent_base_commission_percentage_greater' => 'required',
                ],[
                    'agent_base_commission_percentage_greater.required' => "Please enter Agent Base Commission's percentage not more than 100",
                ]);
            }
        }

        if(empty($agent_base_commission_amount)){
            $agent_base_commission_amount = 0;
        }
        else{
            if(!is_numeric($agent_base_commission_amount)){
                $this->validate($request, [
                    'agent_base_commission_amount_numeric' => 'required',
                ],[
                    'agent_base_commission_amount_numeric.required' => "Agent Base Commission's percentage should be numeric",
                ]);
            }
        }

        //4.
        if(empty($bonus_percentage)){
            $bonus_percentage = 0;
        }
        else{
            if(!is_numeric($bonus_percentage)){
                $this->validate($request, [
                    'bonus_percentage_numeric' => 'required',
                ],[
                    'bonus_percentage_numeric.required' => "Bonus's percentage should be numeric",
                ]);
            }
            else if($bonus_percentage > 100){

                $this->validate($request, [
                    'bonus_percentage_greater' => 'required',
                ],[
                    'bonus_percentage_greater.required' => "Please enter Bonus's percentage not more than 100",
                ]);
            }
        }

        //5.
        if(empty($bonus_amount)){
            $bonus_amount = 0;
        }
        else{
            if(!is_numeric($bonus_amount)){
                $this->validate($request, [
                    'bonus_amount_numeric' => 'required',
                ],[
                    'bonus_amount_numeric.required' => "Bonus's percentage should be numeric",
                ]);
            }
        }

        //5.a.
        if(empty($sale_amount_limit_bonus)){
            $sale_amount_limit_bonus = 0;
        }
        else{
            if(!is_numeric($sale_amount_limit_bonus)){
                $this->validate($request, [
                    'sale_amount_limit_bonus_numeric' => 'required',
                ],[
                    'sale_amount_limit_bonus_numeric.required' => "Sale amount limit bonus to get bonus field should be numeric",
                ]);
            }
        }

        //6.
        if(empty($policy_fees_percentage)){
            $policy_fees_percentage = 0;
        }
        else{
            if(!is_numeric($policy_fees_percentage)){
                $this->validate($request, [
                    'policy_fees_percentage_numeric' => 'required',
                ],[
                    'policy_fees_percentage_numeric.required' => "Policy Fees's percentage should be numeric",
                ]);
            }
            else if($policy_fees_percentage > 100){

                $this->validate($request, [
                    'policy_fees_percentage_greater' => 'required',
                ],[
                    'policy_fees_percentage_greater.required' => "Please enter Policy Fees's percentage not more than 100",
                ]);
            }
        }

        //7.
        if(empty($policy_fees_amount)){
            $policy_fees_amount = 0;
        }
        else{
            if(!is_numeric($policy_fees_amount)){
                $this->validate($request, [
                    'policy_fees_amount_numeric' => 'required',
                ],[
                    'policy_fees_amount_numeric.required' => "Policy Fees's percentage should be numeric",
                ]);
            }
        }

        //8.
        if(empty($upto_max_amount)){
            $upto_max_amount = 0;
        }
        else{
            if(!is_numeric($upto_max_amount)){
                $this->validate($request, [
                    'upto_max_amount_numeric' => 'required',
                ],[
                    'upto_max_amount_numeric.required' => "Upto max amount should be numeric",
                ]);
            }
        }



        //updation
        //updation
        //updation
        $partnerExist = InsuranceProvider::where('id', $id)->first();
        if( $partnerExist ) {
                $partnerExist->email                            = $email;
                $partnerExist->name                             = $name;
                $partnerExist->web_url                          = $webUrl;
                $partnerExist->contact_number                   = $contactNumber;
                $partnerExist->recommended_partner              = $recommended_partner;
                // 
                $partnerExist->base_commission_percentage       = $base_commission_percentage;
                $partnerExist->base_commission_amount           = $base_commission_amount;
                // 
                $partnerExist->addon_base_commission_percentage       = $addon_base_commission_percentage;
                $partnerExist->addon_base_commission_amount           = $addon_base_commission_amount;
                // 
                $partnerExist->agent_base_commission_percentage = $agent_base_commission_percentage;
                $partnerExist->agent_base_commission_amount     = $agent_base_commission_amount;
                // 
                $partnerExist->agent_addon_base_commission_percentage = $agent_addon_base_commission_percentage;
                $partnerExist->agent_addon_base_commission_amount     = $agent_addon_base_commission_amount;
                // 
                $partnerExist->bonus_percentage                 = $bonus_percentage;
                $partnerExist->bonus_amount                     = $bonus_amount;
                $partnerExist->sale_amount_limit_bonus          = $sale_amount_limit_bonus;
                // 
                $partnerExist->policy_fees_percentage           = $policy_fees_percentage;
                $partnerExist->policy_fees_amount               = $policy_fees_amount;
                $partnerExist->upto_max_amount                  = $upto_max_amount;
                $partnerExist->only_tpl_after_years             = $only_tpl_after_years;
                // 
                $partnerExist->status                           = $status;
                $partnerExist->modified_by                      = $adminID;
                //$partnerExist->save();


                if ($request->has_logo_image == '0' ){
                    //$body->has_logo = $request->has_logo_image;
                    $partnerExist->has_logo = 0;
                }


                $imageName = '';
                try{
                    if ($request->hasFile('image')) { 
                        // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $partnerExist->id.'_main';
                        $request->image->move(public_path('uploads/admin/insurance_providers'), $imageName);
                        // resize for logo
                        $imageLogo = $partnerExist->id.'_logo';
                        $img = Image::make(public_path('uploads/admin/insurance_providers/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/admin/insurance_providers/' . $imageLogo));

                        $partnerExist->has_logo = 1;
                    }

                }
                catch(\Exception $e){
                    // do task when error
                    // echo $e->getMessage();
                }

                $partnerExist->save();
                
                return redirect('/admin/partners')->with('success', 'Partners updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
