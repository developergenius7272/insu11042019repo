<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAgencyTypeInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->tinyInteger('agency_type')->nullable();
            $table->double('precentage_rate')->nullable();
            $table->double('minimum_value')->nullable();
            $table->double('commission_percentage')->nullable();
            $table->double('commission_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('agency_type');
            $table->dropColumn('precentage_rate');
            $table->dropColumn('minimum_value');
            $table->dropColumn('commission_percentage');
            $table->dropColumn('commission_value');
        });
    }
}
