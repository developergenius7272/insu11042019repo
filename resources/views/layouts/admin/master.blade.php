<!DOCTYPE html>
<html lang="en-us" @if(!(\Auth::user()->hasRole(['super-admin','sub-admin'])) )id="extr-page" @endif>
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>{{ isset($pageTitle) ? $pageTitle.'::'.env('APP_NAME', 'Insure Online') : env('APP_NAME', 'Insure Online')}}</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/font-awesome.min.css') }}">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/smartadmin-production-plugins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/smartadmin-production.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/smartadmin-skins.min.css') }}">

		<!-- SmartAdmin RTL Support  -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/smartadmin-rtl.min.css') }}">

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		{{-- <link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/your_style.css') }}">  --}}
		-->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		{{-- <link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/admin/css/demo.min.css') }}"> --}}

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="{{ URL::asset('assets/admin/img/favicon/favicon.png') }}" type="image/x-icon">
		<link rel="icon" href="{{ URL::asset('assets/admin/img/favicon/favicon.png') }}" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="{{ URL::asset('assets/admin/img/splash/sptouch-icon-iphone.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/admin/img/splash/touch-icon-ipad.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/admin/img/splash/touch-icon-iphone-retina.png') }}') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/admin/img/splash/touch-icon-ipad-retina.png') }}">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/admin/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/admin/img/splash/ipad-portrait.png') }}') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/admin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">

		@yield('css')
	</head>
	
	<!--

	TABLE OF CONTENTS.
	
	Use search to find needed section.
	
	===================================================================
	
	|  01. #CSS Links                |  all CSS links and file paths  |
	|  02. #FAVICONS                 |  Favicon links and file paths  |
	|  03. #GOOGLE FONT              |  Google font link              |
	|  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
	|  05. #BODY                     |  body tag                      |
	|  06. #HEADER                   |  header tag                    |
	|  07. #PROJECTS                 |  project lists                 |
	|  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
	|  09. #MOBILE                   |  mobile view dropdown          |
	|  10. #SEARCH                   |  search field                  |
	|  11. #NAVIGATION               |  left panel & navigation       |
	|  12. #RIGHT PANEL              |  right panel userlist          |
	|  13. #MAIN PANEL               |  main panel                    |
	|  14. #MAIN CONTENT             |  content holder                |
	|  15. #PAGE FOOTER              |  page footer                   |
	|  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
	|  17. #PLUGINS                  |  all scripts and plugins       |
	
	===================================================================
	
	-->
	
	<!-- #BODY -->
	<!-- Possible Classes

		* 'smart-style-{SKIN#}'
		* 'smart-rtl'         - Switch theme mode to RTL
		* 'menu-on-top'       - Switch to top navigation (no DOM change required)
		* 'no-menu'			  - Hides the menu completely
		* 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
		* 'fixed-header'      - Fixes the header
		* 'fixed-navigation'  - Fixes the main menu
		* 'fixed-ribbon'      - Fixes breadcrumb
		* 'fixed-page-footer' - Fixes footer
		* 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
	-->
	{{-- {{dd(Auth::guard('admin')->check())}} --}}
	
	<body class="@if(!\Auth::user()->hasRole(['super-admin','sub-admin'])) animated fadeInDown @endif">

		<!-- HEADER -->
		<header id="header">
			
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo"> <img src="{{ URL::asset('assets/admin/img/logo_admin.png') }}" alt="SmartAdmin"> </span>

				
				<!-- END LOGO PLACEHOLDER -->
				@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
					<!-- Note: The activity badge color changes when clicked and resets the number to 0
					Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
					{{-- <span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span> --}}

					<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
					{{-- <div class="ajax-dropdown">

						<!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
						<div class="btn-group btn-group-justified" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" name="activity" id="ajax/notify/mail.html">
								Msgs (14) </label>
							<label class="btn btn-default">
								<input type="radio" name="activity" id="ajax/notify/notifications.html">
								notify (3) </label>
							<label class="btn btn-default">
								<input type="radio" name="activity" id="ajax/notify/tasks.html">
								Tasks (4) </label>
						</div>

						<!-- notification content -->
						<div class="ajax-notifications custom-scroll">

							<div class="alert alert-transparent">
								<h4>Click a button to show messages here</h4>
								This blank page message helps protect your privacy, or you can show the first message here automatically.
							</div>

							<i class="fa fa-lock fa-4x fa-border"></i>

						</div>
						<!-- end notification content -->

						<!-- footer: refresh area -->
						<span> Last updated on: 12/12/2013 9:43AM
							<button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
								<i class="fa fa-refresh"></i>
							</button> 
						</span>
						<!-- end footer -->

					</div> --}}
					<!-- END AJAX-DROPDOWN -->
				@endif
			</div>

			
			
			@if(Auth::user()->hasRole(['super-admin','sub-admin']))
				<!-- projects dropdown -->
				
				<!-- end projects dropdown -->

				<!-- pulled right: nav area -->
				<div class="pull-right">
					
					<!-- collapse menu button -->
					<div id="hide-menu" class="btn-header pull-right">
						<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
					</div>
					<!-- end collapse menu -->
					
					<!-- #MOBILE -->
					<!-- Top menu profile link : this shows only when top menu is active -->
					<ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
						<li class="">
							<a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> 
								<img src="{{ URL::asset('assets/admin/img/avatars/sunny.png') }}" alt="John Doe" class="online" />  
							</a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
								</li>
							</ul>
						</li>
					</ul>

					<!-- logout button -->
					<div id="logout" class="btn-header transparent pull-right">
						<span> <a href="{{ route('admin.logout') }}" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
					</div>
					<!-- logout form -->
					<form id="admin-logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
					<!-- end logout form -->
					<!-- end logout button -->

					<!-- search mobile button (this is hidden till mobile view port) -->
					{{-- <div id="search-mobile" class="btn-header transparent pull-right">
						<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
					</div> --}}
					<!-- end search mobile button -->

					<!-- input: search field -->
					{{-- <form action="search.html" class="header-search pull-right">
						<input id="search-fld"  type="text" name="param" placeholder="Find reports and more" data-autocomplete='[
						"ActionScript",
						"AppleScript",
						"Asp",
						"BASIC",
						"C",
						"C++",
						"Clojure",
						"COBOL",
						"ColdFusion",
						"Erlang",
						"Fortran",
						"Groovy",
						"Haskell",
						"Java",
						"JavaScript",
						"Lisp",
						"Perl",
						"PHP",
						"Python",
						"Ruby",
						"Scala",
						"Scheme"]'>
						<button type="submit">
							<i class="fa fa-search"></i>
						</button>
						<a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
					</form> --}}
					<!-- end input: search field -->

					<!-- fullscreen button -->
					<div id="fullscreen" class="btn-header transparent pull-right">
						<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
					</div>
					<!-- end fullscreen button -->
					
					<!-- #Voice Command: Start Speech -->
					{{-- <div id="speech-btn" class="btn-header transparent pull-right hidden-sm hidden-xs">
						<div> 
							<a href="javascript:void(0)" title="Voice Command" data-action="voiceCommand"><i class="fa fa-microphone"></i></a> 
							<div class="popover bottom"><div class="arrow"></div>
								<div class="popover-content">
									<h4 class="vc-title">Voice command activated <br><small>Please speak clearly into the mic</small></h4>
									<h4 class="vc-title-error text-center">
										<i class="fa fa-microphone-slash"></i> Voice command failed
										<br><small class="txt-color-red">Must <strong>"Allow"</strong> Microphone</small>
										<br><small class="txt-color-red">Must have <strong>Internet Connection</strong></small>
									</h4>
									<a href="javascript:void(0);" class="btn btn-success" onclick="commands.help()">See Commands</a> 
									<a href="javascript:void(0);" class="btn bg-color-purple txt-color-white" onclick="$('#speech-btn .popover').fadeOut(50);">Close Popup</a> 
								</div>
							</div>
						</div>
					</div> --}}
					<!-- end voice command -->

					<!-- multiple lang dropdown : find all flags in the flags page -->
					{{-- <ul class="header-dropdown-list hidden-xs">
						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-us" alt="United States"> <span> English (US) </span> <i class="fa fa-angle-down"></i> </a>
							<ul class="dropdown-menu pull-right">
								<li class="active">
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-us" alt="United States"> English (US)</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-fr" alt="France"> Français</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-es" alt="Spanish"> Español</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-de" alt="German"> Deutsch</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-jp" alt="Japan"> 日本語</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-cn" alt="China"> 中文</a>
								</li>	
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-it" alt="Italy"> Italiano</a>
								</li>	
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-pt" alt="Portugal"> Portugal</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-ru" alt="Russia"> Русский язык</a>
								</li>
								<li>
									<a href="javascript:void(0);"><img src="{{ URL::asset('assets/admin/img/blank.gif') }}" class="flag flag-kr" alt="Korea"> 한국어</a>
								</li>						
								
							</ul>
						</li>
					</ul> --}}
					<!-- end multiple lang -->

				</div>
				<!-- end pulled right: nav area -->
			@endif
		</header>
		<!-- END HEADER -->
		@if(Auth::user()->hasRole(['super-admin','sub-admin']))
			<!-- Left panel : Navigation area -->
			<!-- Note: This width of the aside area can be adjusted through LESS variables -->
			<aside id="left-panel">

				<!-- User info -->
				<div class="login-info">
					<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
						
						{{-- <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut"> --}}
						<a href="javascript:void(0);" >
							<img src="{{ URL::asset('assets/admin/img/avatars/sunny.png') }}" alt="me" class="online" /> 
							<span>
								john.doe 
							</span>
							<i class="fa fa-angle-down"></i>
						</a> 
						
					</span>
				</div>
				<!-- end user info -->

				<!-- NAVIGATION : This navigation is also responsive-->
				<nav>
					<!-- 
					NOTE: Notice the gaps after each icon usage <i></i>..
					Please note that these links work a bit different than
					traditional href="" links. See documentation for details.
					-->

					<ul>
						<li>
							<a href="{{route('admin.home')}}" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
							
						</li>

						{{-- Masters --}}
						<li @if(\Request::is('admin/policydocumentmaster/*')) class="active open" @endif>
							<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Masters</span></a>
							<ul>
								<li @if( \Request::is('admin/cars/car-makes') || \Request::is('admin/cars/car-makes/*') ) class="active open" @endif>
									<a href="{{url('/admin/policydocumentmaster')}}">Document Master</a>
									<ul>
										<li @if( ( \Request::is('admin/policydocumentmaster') || \Request::is('admin/policydocumentmaster/*') ) & !\Request::is('admin/policydocumentmaster/create') ) class="active" @endif>
											<a href="{{url('/admin/policydocumentmaster')}}"><i class="fa fa-fw fa-folder-open"></i> Document </a>
										</li>
										<li @if( \Request::is('admin/policydocumentmaster/create') ) class="active open" @endif>
											<a href="{{url('/admin/policydocumentmaster/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Document </a>
										</li>
									</ul>
								</li>
								<li @if( \Request::is('admin/addoncategory') || \Request::is('admin/addoncategory/*') ) class="active open" @endif>
									<a href="{{url('/admin/addoncategory')}}">Category Master</a>
									<ul>
										<li @if( ( \Request::is('admin/addoncategory') || \Request::is('admin/addoncategory/*') ) & !\Request::is('admin/addoncategory/create') ) class="active" @endif>
											<a href="{{url('/admin/addoncategory')}}"><i class="fa fa-fw fa-folder-open"></i> Category </a>
										</li>
										<li @if( \Request::is('admin/addoncategory/create') ) class="active open" @endif>
											<a href="{{url('/admin/addoncategory/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Category </a>
										</li>
									</ul>
								</li>
							</ul>
						</li>

						{{-- cars --}}
						<li @if(\Request::is('admin/cars/*')) class="active open" @endif>
							<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Cars</span></a>
							<ul>
								<li @if( \Request::is('admin/cars/car-makes') || \Request::is('admin/cars/car-makes/*') ) class="active open" @endif>
									<a href="{{url('/admin/cars/car-makes')}}">Makes</a>
									<ul>
										<li @if( ( \Request::is('admin/cars/car-makes') || \Request::is('admin/cars/car-makes/*') ) & !\Request::is('admin/cars/car-makes/create') ) class="active" @endif>
											<a href="{{url('/admin/cars/car-makes')}}"><i class="fa fa-fw fa-folder-open"></i>Car Makes </a>
										</li>
										<li @if( \Request::is('admin/cars/car-makes/create') ) class="active open" @endif>
											<a href="{{url('/admin/cars/car-makes/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Car Make </a>
										</li>
									</ul>
								</li>
								@endif
								{{--  --}}
								@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
								<li @if( \Request::is('admin/cars/car-models') || \Request::is('admin/cars/car-models/*') ) class="active open" @endif>
									<a href="{{url('/admin/cars/car-models')}}">Models</a>
									<ul>
										<li @if( ( \Request::is('admin/cars/car-models') || \Request::is('admin/cars/car-models/*') ) & !\Request::is('admin/cars/car-models/create') ) class="active" @endif>
											<a href="{{url('/admin/cars/car-models')}}"><i class="fa fa-fw fa-folder-open"></i>Models </a>
										</li>
										<li @if( \Request::is('admin/cars/car-models/create') ) class="active open" @endif>
											<a href="{{url('/admin/cars/car-models/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Car Model </a>
										</li>
									</ul>
								</li>
								@endif
								{{--  --}}
								@if(\Auth::user()->hasRole(['super-admin']))
								<li @if( \Request::is('admin/cars/car-model-variants') || \Request::is('admin/cars/car-models/*') ) class="active open" @endif>
									<a href="{{url('/admin/cars/car-model-variants')}}">Model Variants</a>
									<ul>
										<li @if( ( \Request::is('admin/cars/car-model-variants') || \Request::is('admin/cars/car-model-variants/*') ) & !\Request::is('admin/cars/car-model-variants/create') ) class="active" @endif>
											<a href="{{url('/admin/cars/car-model-variants')}}"><i class="fa fa-fw fa-folder-open"></i>Model Variants </a>
										</li>
										<li @if( \Request::is('admin/cars/car-model-variants/create') ) class="active" @endif>
											<a href="{{url('/admin/cars/car-model-variants/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Model Variant </a>
										</li>
									</ul>
								</li>
								@endif
								{{--  --}}
								@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
								<li @if( \Request::is('admin/cars/car-model-body') || \Request::is('admin/cars/car-model-body/*') ) class="active open" @endif>
									<a href="{{url('/admin/cars/car-model-body')}}">Model Bodies</a>
									<ul>
										<li @if( ( \Request::is('admin/cars/car-model-body') || \Request::is('admin/cars/car-model-body/*') ) & !\Request::is('admin/cars/car-model-body/create') ) class="active" @endif>
											<a href="{{url('/admin/cars/car-model-body')}}"><i class="fa fa-fw fa-folder-open"></i>Car Model Bodies </a>
										</li>
										<li @if( \Request::is('admin/cars/car-model-body/create') ) class="active open" @endif>
											<a href="{{url('/admin/cars/car-model-body/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Car Modal Body </a>
										</li>
									</ul>
								</li>
								@endif
								{{--  --}}
								@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
								<li @if( \Request::is('admin/cars/car-model-fuel') || \Request::is('admin/cars/car-model-fuel/*') ) class="active open" @endif>
									<a href="{{url('admin/cars/car-model-fuel')}}">Modal fuels</a>
									<ul>
										<li @if( ( \Request::is('admin/cars/car-model-fuel') || \Request::is('admin/cars/car-model-fuel/*') ) & !\Request::is('admin/cars/car-model-fuel/create') ) class="active" @endif>
											<a href="{{url('admin/cars/car-model-fuel')}}"><i class="fa fa-fw fa-folder-open"></i>Car Model fuels </a>
										</li>
										<li @if( \Request::is('admin/cars/car-model-fuel/create') ) class="active open" @endif>
											<a href="{{url('admin/cars/car-model-fuel/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Car Model fuel </a>
										</li>
									</ul>
								</li>
								
							</ul>
						</li>

						{{-- Home Page --}}
						<li @if( \Request::is('admin/homepages/*') || \Request::is('admin/homepages') ) class="active open" @endif>
							<a href="{{url('/admin/homepages')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Home Page</span></a>

							<ul>
								<li @if( \Request::is('admin/homepages') || \Request::is('admin/homepages/*') && !\Request::is('admin/homepages/create') ) class="active open" @endif>
									<a href="{{url('/admin/homepages')}}">Banners</a>
								</li>
								<li @if( \Request::is('admin/homepages/create')) class="active open" @endif>
									<a href="{{url('/admin/homepages/create')}}">Add Banner</a>
								</li>
							</ul>
						</li>

						{{-- partners --}}
						<li @if( \Request::is('admin/partners/*') ) class="active open" @endif>
							<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Partners</span></a>
							<ul>
								<li @if( \Request::is('admin/partners') || \Request::is('admin/partners/*') ) class="active open" @endif>
									<a href="{{url('/admin/partners')}}">Partners</a>
									<ul>
										<li @if( ( \Request::is('admin/partners') || \Request::is('admin/partners/*') ) & !\Request::is('admin/partners/create') ) class="active" @endif>
											<a href="{{url('/admin/partners')}}"><i class="fa fa-fw fa-folder-open"></i>Partners  </a>
										</li>
										<li @if( \Request::is('admin/partners/create') ) class="active open" @endif>
											<a href="{{url('/admin/partners/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Partners </a>
										</li>
									</ul>
								</li>
								<li @if( \Request::is('admin/partners/premium-plans') || \Request::is('admin/partners/premium-plans/*') ) class="active open" @endif>
									<a href="{{url('/admin/partners/premium-plans')}}">Premium Plans</a>
									<ul>
										<li @if( ( \Request::is('admin/partners/premium-plans') || \Request::is('admin/partners/premium-plans/*') ) & !\Request::is('admin/partners/premium-plans/create') ) class="active" @endif>
											<a href="{{url('/admin/partners/premium-plans')}}"><i class="fa fa-fw fa-folder-open"></i>Premium Plan </a>
										</li>
										<li @if( \Request::is('admin/partners/premium-plans/create') ) class="active open" @endif>
											<a href="{{url('/admin/partners/premium-plans/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Premium Plan </a>
										</li>
									</ul>
								</li>
								{{--  --}}
								<li @if( \Request::is('admin/partners/third-party-covers') || \Request::is('admin/partners/third-party-covers/*') ) class="active open" @endif>
									<a href="{{url('/admin/partners/third-party-covers')}}">Third Party Covers</a>
									<ul>
										<li @if( ( \Request::is('admin/partners/third-party-covers') || \Request::is('admin/partners/third-party-covers/*') ) & !\Request::is('admin/partners/third-party-covers/create') ) class="active" @endif>
											<a href="{{url('/admin/partners/third-party-covers')}}"><i class="fa fa-fw fa-folder-open"></i>Premium Plan </a>
										</li>
										<li @if( \Request::is('admin/partners/third-party-covers/create') ) class="active open" @endif>
											<a href="{{url('/admin/partners/third-party-covers/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Premium Plan </a>
										</li>
									</ul>
								</li>
								<li @if( \Request::is('admin/partners/benefit-masters') || \Request::is('admin/partners/benefit-masters/*') ) class="active open" @endif>
									<a href="{{url('/admin/partners/benefit-masters')}}">Benefit Master</a>
									<ul>
										<li @if( ( \Request::is('admin/partners/benefit-masters') || \Request::is('admin/partners/benefit-masters/*') ) & !\Request::is('admin/partners/benefit-masters/create') ) class="active" @endif>
											<a href="{{url('/admin/partners/benefit-masters')}}"><i class="fa fa-fw fa-folder-open"></i>Benefit Master </a>
										</li>
										<li @if( \Request::is('admin/partners/benefit-masters/create') ) class="active open" @endif>
											<a href="{{url('/admin/partners/benefit-masters/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Benefit Master </a>
										</li>
										<li @if( ( \Request::is('admin/planbenefits') || \Request::is('admin/planbenefits/*') ) & !\Request::is('admin/planbenefits/create') ) class="active" @endif>
											<a href="{{url('/admin/planbenefits')}}"><i class="fa fa-fw fa-folder-open"></i>Plan Benefit </a>
										</li>
										<li @if( \Request::is('admin/planbenefits/create') ) class="active open" @endif>
											<a href="{{url('/admin/planbenefits/create')}}"><i class="fa fa-fw fa-folder-open"></i> New Plan Benefit</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>

						{{-- Excess --}}
						<li @if( \Request::is('admin/excess/*') || \Request::is('admin/excess') ) class="active open" @endif>
							<a href="{{url('/admin/excess')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Excess</span></a>
							<ul>
								<li @if( \Request::is('admin/excess') || \Request::is('admin/excess/*') && !\Request::is('admin/excess/create') ) class="active open" @endif>
									<a href="{{url('/admin/excess')}}">Excess</a>
								</li>
								<li @if( \Request::is('admin/excess/create')) class="active open" @endif>
									<a href="{{url('/admin/excess/create')}}">Add Excess</a>
								</li>
							</ul>
						</li>

						{{-- Cross Selling Products --}}
						<li @if( \Request::is('admin/cross-selling-products/*') || \Request::is('admin/cross-selling-products') ) class="active open" @endif>
							<a href="{{url('/admin/cross-selling-products')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Cross Selling Products</span></a>
							<ul>
								<li @if( \Request::is('admin/cross-selling-products') || \Request::is('admin/cross-selling-products/*') && !\Request::is('admin/cross-selling-products/create') ) class="active open" @endif>
									<a href="{{url('/admin/cross-selling-products')}}">Cross Selling Products</a>
								</li>
								<li @if( \Request::is('admin/cross-selling-products/create')) class="active open" @endif>
									<a href="{{url('/admin/cross-selling-products/create')}}">Add Cross Selling Products</a>
								</li>
							</ul>
						</li>

						{{-- users --}}
						<li @if( \Request::is('admin/users/*') || \Request::is('admin/users') ) class="active open" @endif>
							<a href="{{url('/admin/users')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Users</span></a>
							<ul>
								<li @if( \Request::is('admin/users') || \Request::is('admin/users/*') && !\Request::is('admin/users/create') ) class="active open" @endif>
									<a href="{{url('/admin/users')}}">Users</a>
								</li>
								@if(\Auth::user()->can(['super-admin','user-create']))
								<li @if( \Request::is('admin/users/create')) class="active open" @endif>
									<a href="{{url('/admin/users/create')}}">Add user</a>
								</li>
								@endif
							</ul>
						</li>

						{{-- agents --}}
						<li @if( \Request::is('admin/agents/*') || \Request::is('admin/agents') ) class="active open" @endif>
							<a href="{{url('/admin/agents')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Agents</span></a>
							
							<ul>
								<li @if( \Request::is('admin/agents') || \Request::is('admin/agents/*') && !\Request::is('admin/users/create') ) class="active open" @endif>
									<a href="{{url('/admin/agents')}}">Agents</a>
								</li>
								@if(\Auth::user()->can(['super-admin','agent-create']))
								<li @if( \Request::is('admin/agents/create')) class="active open" @endif>
									<a href="{{url('/admin/agents/create')}}">Add Agent</a>
								</li>
								@endif
							</ul>
						</li>

						{{-- Leads --}}
						<li @if( \Request::is('admin/leads/*') || \Request::is('admin/leads') ) class="active open" @endif>
							<a href="{{url('/admin/leads')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Leads</span></a>
							@if(\Auth::user()->can(['super-admin','lead-create']))
							<ul>
								<li @if( \Request::is('admin/leads') || \Request::is('admin/leads/*') && !\Request::is('admin/leads/view') ) class="active open" @endif>
									<a href="{{url('/admin/leads')}}">Leads</a>
								</li>
							</ul>
							@endif
						</li>

						{{-- orders --}}
						<li @if( \Request::is('admin/orders/*') || \Request::is('admin/orders') ) class="active open" @endif>
							<a href="{{url('/admin/orders')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Orders</span></a>
							<ul>
								<li @if( \Request::is('admin/orders') || \Request::is('admin/orders/*') && !\Request::is('admin/orders/view') ) class="active open" @endif>
									<a href="{{url('/admin/orders')}}">Orders</a>
								</li>
							</ul>
						</li>

						{{-- Import --}}
						<li @if( \Request::is('admin/import/car-value*') || \Request::is('admin/import/car-value') ) class="active open" @endif>
							<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Import Car Values</span></a>
							<ul>
								<li @if( \Request::is('admin/import/car-value') || \Request::is('admin/import/car-value*') && !\Request::is('admin/users/create') ) class="active open" @endif>
									<a href="{{url('/admin/import/car-value')}}">Import</a>
								</li>
								<li @if( \Request::is('admin/import/car-value/show') || \Request::is('admin/import/car-value/show*') && !\Request::is('admin/users/create') ) class="active open" @endif>
									<a href="{{url('/admin/import/car-value/show')}}">Car Values</a>
								</li>
							</ul>
						</li>

						{{-- Report --}}
						<li @if( \Request::is('admin/reports/commission*') || \Request::is('admin/reports/commission') ) class="active open" @endif>
							<a href="{{url('/admin/reports/commission')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Report</span></a>
							<ul>
								<li @if( \Request::is('admin/reports/commission') || \Request::is('admin/reports/commission*') && !\Request::is('admin/reports/commission') ) class="active open" @endif>
									<a href="{{url('/admin/reports/commission')}}">Partner Commission</a>
								</li>
								<li @if( \Request::is('admin/reports/commission/agent') || \Request::is('admin/reports/commission/agent*') && !\Request::is('admin/reports/commission/agent') ) class="active open" @endif>
									<a href="{{url('/admin/reports/commission/agent')}}">Agent Commission</a>
								</li>
							</ul>
						</li>

						{{-- CMS --}}
						<li @if( \Request::is('admin/pages/*') ) class="active open" @endif>
							<a href="{{route('admin.pages.index')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">CMS</span></a>
							<ul>
								<li @if( \Request::is('admin/pages') ) class="active open" @endif>
									<a href="{{route('admin.pages.index')}}">Pages</a>
								</li>
								<li @if( \Request::is('admin/pages/create') ) class="active open" @endif>
									<a href="{{route('admin.pages.create')}}">Add Page</a>
								</li>
							</ul>
						</li>

						{{-- Roles --}}
						<li @if( \Request::is('admin/roles/*') || \Request::is('admin/roles') ) class="active open" @endif >
							<a href="{{url('/admin/roles')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Roles</span></a>
							<ul>
								<li @if( \Request::is('admin/roles') || \Request::is('admin/roles/*') ) class="active open" @endif>
									<a href="{{url('/admin/roles')}}">Roles</a>
								</li>
								{{-- <li @if( \Request::is('admin/roles/create') ) class="active open" @endif>
									<a href="">Add Role</a>
								</li> --}}
							</ul>
						</li>

						@if(Auth::user()->hasRole('super-admin'))
							<li @if( \Request::route()->getName() == 'admin.admin-users.index' || \Request::route()->getName() == 'admin.admin-users.create' ) class="active open" @endif >
								<a href="{{route('admin.admin-users.index')}}"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Admin Users</span></a>
								<ul>
									<li @if( \Request::route()->getName() == 'admin.admin-users.index' ) class="active open" @endif>
										<a href="{{route('admin.admin-users.index')}}">Users</a>
									</li>

									<li @if( \Request::route()->getName() == 'admin.admin-users.create' ) class="active open" @endif>
										<a href="{{route('admin.admin-users.create')}}">Add User</a>
									</li>
									
								</ul>
							</li>
						@endif

						{{-- <li class="top-menu-invisible">
							<a href="#"><i class="fa fa-lg fa-fw fa-cube txt-color-blue"></i> <span class="menu-item-parent">SmartAdmin Intel</span></a>
							<ul>
								<li class="">
									<a href="layouts.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">App Layouts</span></a>
								</li>
								<li class="">
									<a href="skins.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-picture-o"></i> <span class="menu-item-parent">Prebuilt Skins</span></a>
								</li>
								<li>
									<a href="applayout.html"><i class="fa fa-cube"></i> App Settings</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="inbox.html"><i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">Outlook</span> <span class="badge pull-right inbox-badge margin-right-13">14</span></a>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Graphs</span></a>
							<ul>
								<li>
									<a href="flot.html">Flot Chart</a>
								</li>
								<li>
									<a href="morris.html">Morris Charts</a>
								</li>
								<li>
									<a href="sparkline-charts.html">Sparklines</a>
								</li>
								<li>
									<a href="easypie-charts.html">EasyPieCharts</a>
								</li>
								<li>
									<a href="dygraphs.html">Dygraphs</a>
								</li>
								<li>
									<a href="chartjs.html">Chart.js</a>
								</li>
								<li>
									<a href="hchartable.html">HighchartTable <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Tables</span></a>
							<ul>
								<li>
									<a href="table.html">Normal Tables</a>
								</li>
								<li>
									<a href="datatables.html">Data Tables <span class="badge inbox-badge bg-color-greenLight hidden-mobile">responsive</span></a>
								</li>
								<li>
									<a href="jqgrid.html">Jquery Grid</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Forms</span></a>
							<ul>
								<li>
									<a href="form-elements.html">Smart Form Elements</a>
								</li>
								<li>
									<a href="form-templates.html">Smart Form Layouts</a>
								</li>
								<li>
									<a href="validation.html">Smart Form Validation</a>
								</li>
								<li>
									<a href="bootstrap-forms.html">Bootstrap Form Elements</a>
								</li>
								<li>
									<a href="bootstrap-validator.html">Bootstrap Form Validation</a>
								</li>
								<li>
									<a href="plugins.html">Form Plugins</a>
								</li>
								<li>
									<a href="wizard.html">Wizards</a>
								</li>
								<li>
									<a href="other-editors.html">Bootstrap Editors</a>
								</li>
								<li>
									<a href="dropzone.html">Dropzone</a>
								</li>
								<li>
									<a href="image-editor.html">Image Cropping</a>
								</li>
								<li>
									<a href="ckeditor.html">CK Editor</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">UI Elements</span></a>
							<ul>
								<li>
									<a href="general-elements.html">General Elements</a>
								</li>
								<li>
									<a href="buttons.html">Buttons</a>
								</li>
								<li>
									<a href="#">Icons</a>
									<ul>
										<li>
											<a href="fa.html"><i class="fa fa-plane"></i> Font Awesome</a>
										</li>
										<li>
											<a href="glyph.html"><i class="glyphicon glyphicon-plane"></i> Glyph Icons</a>
										</li>
										<li>
											<a href="flags.html"><i class="fa fa-flag"></i> Flags</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="grid.html">Grid</a>
								</li>
								<li>
									<a href="treeview.html">Tree View</a>
								</li>
								<li>
									<a href="nestable-list.html">Nestable Lists</a>
								</li>
								<li>
									<a href="jqui.html">JQuery UI</a>
								</li>
								<li>
									<a href="typography.html">Typography</a>
								</li>
								<li>
									<a href="#">Six Level Menu</a>
									<ul>
										<li>
											<a href="#"><i class="fa fa-fw fa-folder-open"></i> Item #2</a>
											<ul>
												<li>
													<a href="#"><i class="fa fa-fw fa-folder-open"></i> Sub #2.1 </a>
													<ul>
														<li>
															<a href="#"><i class="fa fa-fw fa-file-text"></i> Item #2.1.1</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-fw fa-plus"></i> Expand</a>
															<ul>
																<li>
																	<a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
																</li>
															</ul>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>
											<a href="#"><i class="fa fa-fw fa-folder-open"></i> Item #3</a>
			
											<ul>
												<li>
													<a href="#"><i class="fa fa-fw fa-folder-open"></i> 3ed Level </a>
													<ul>
														<li>
															<a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
														</li>
														<li>
															<a href="#"><i class="fa fa-fw fa-file-text"></i> File</a>
														</li>
													</ul>
												</li>
											</ul>	
										</li>
										<li>
											<a href="#" class="inactive"><i class="fa fa-fw fa-folder-open"></i> Item #4 (disabled)</a>
										</li>	
										
									</ul>
								</li>
							</ul>
						</li>	
						<li>
							<a href="widgets.html"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Widgets</span></a>
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-cloud"><em>3</em></i> <span class="menu-item-parent">Cool Features!</span></a>
							<ul>
								<li>
									<a href="calendar.html"><i class="fa fa-lg fa-fw fa-calendar"></i> <span class="menu-item-parent">Calendar</span></a>
								</li>
								<li>
									<a href="gmap-xml.html"><i class="fa fa-lg fa-fw fa-map-marker"></i> <span class="menu-item-parent">GMap Skins</span><span class="badge bg-color-greenLight pull-right inbox-badge">9</span></a>
								</li>
							</ul>
						</li>	
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-puzzle-piece"></i> <span class="menu-item-parent">App Views</span></a>
							<ul>
								<li>
									<a href="projects.html"><i class="fa fa-file-text-o"></i> Projects</a>
								</li>	
								<li>
									<a href="blog.html"><i class="fa fa-paragraph"></i> Blog</a>
								</li>
								<li>
									<a href="gallery.html"><i class="fa fa-picture-o"></i> Gallery</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-comments"></i> Forum Layout</a>
									<ul>
										<li><a href="forum.html">General View</a></li>
										<li><a href="forum-topic.html">Topic View</a></li>
										<li><a href="forum-post.html">Post View</a></li>
									</ul>
								</li>
								<li>
									<a href="profile.html"><i class="fa fa-group"></i> Profile</a>
								</li>
								<li>
									<a href="timeline.html"><i class="fa fa-clock-o"></i> Timeline</a>
								</li>
								<li>
									<a href="search.html"><i class="fa fa-search"></i>  Search Page</a>
								</li>
							</ul>		
						</li>
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent">E-Commerce</span></a>
							<ul>
								<li><a href="orders.html">Orders</a></li>
								<li><a href="products-view.html">Products View</a></li>
								<li><a href="products-detail.html">Products Detail</a></li>
							</ul>
						</li>	
						<li>
							<a href="#"><i class="fa fa-lg fa-fw fa-windows"></i> <span class="menu-item-parent">Miscellaneous</span></a>
							<ul>
								<li>
									<a href="../Landing_Page/" target="_blank">Landing Page <i class="fa fa-external-link"></i></a>
								</li>
								<li>
									<a href="pricing-table.html">Pricing Tables</a>
								</li>
								<li>
									<a href="invoice.html">Invoice</a>
								</li>
								<li>
									<a href="login.html" target="_top">Login</a>
								</li>
								<li>
									<a href="register.html" target="_top">Register</a>
								</li>
								<li>
									<a href="forgotpassword.html" target="_top">Forgot Password</a>
								</li>
								<li>
									<a href="lock.html" target="_top">Locked Screen</a>
								</li>
								<li>
									<a href="error404.html">Error 404</a>
								</li>
								<li>
									<a href="error500.html">Error 500</a>
								</li>
								<li>
									<a href="blank_.html">Blank Page</a>
								</li>
							</ul>
						</li>
						<li class="chat-users top-menu-invisible">
							<a href="#"><i class="fa fa-lg fa-fw fa-comment-o"><em class="bg-color-pink flash animated">!</em></i> <span class="menu-item-parent">Smart Chat API <sup>beta</sup></span></a>
							<ul>
								<li>
									<!-- DISPLAY USERS -->
									<div class="display-users">

										<input class="form-control chat-user-filter" placeholder="Filter" type="text">
										
										<a href="#" class="usr" 
											data-chat-id="cha1" 
											data-chat-fname="Sadi" 
											data-chat-lname="Orlaf" 
											data-chat-status="busy" 
											data-chat-alertmsg="Sadi Orlaf is in a meeting. Please do not disturb!" 
											data-chat-alertshow="true" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/5.png') }}' alt='Sadi Orlaf'>
													<div class='usr-card-content'>
														<h3>Sadi Orlaf</h3>
														<p>Marketing Executive</p>
													</div>
												</div>
											"> 
											<i></i>Sadi Orlaf
										</a>
									
										<a href="#" class="usr" 
											data-chat-id="cha2" 
											data-chat-fname="Jessica" 
											data-chat-lname="Dolof" 
											data-chat-status="online" 
											data-chat-alertmsg="" 
											data-chat-alertshow="false" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/1.png') }}' alt='Jessica Dolof'>
													<div class='usr-card-content'>
														<h3>Jessica Dolof</h3>
														<p>Sales Administrator</p>
													</div>
												</div>
											"> 
											<i></i>Jessica Dolof
										</a>
									
										<a href="#" class="usr" 
											data-chat-id="cha3" 
											data-chat-fname="Zekarburg" 
											data-chat-lname="Almandalie" 
											data-chat-status="online" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/3.png') }}' alt='Zekarburg Almandalie'>
													<div class='usr-card-content'>
														<h3>Zekarburg Almandalie</h3>
														<p>Sales Admin</p>
													</div>
												</div>
											"> 
											<i></i>Zekarburg Almandalie
										</a>
									
										<a href="#" class="usr" 
											data-chat-id="cha4" 
											data-chat-fname="Barley" 
											data-chat-lname="Krazurkth" 
											data-chat-status="away" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/4.png') }}' alt='Barley Krazurkth'>
													<div class='usr-card-content'>
														<h3>Barley Krazurkth</h3>
														<p>Sales Director</p>
													</div>
												</div>
											"> 
											<i></i>Barley Krazurkth
										</a>
									
										<a href="#" class="usr offline" 
											data-chat-id="cha5" 
											data-chat-fname="Farhana" 
											data-chat-lname="Amrin" 
											data-chat-status="incognito" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/female.png') }}' alt='Farhana Amrin'>
													<div class='usr-card-content'>
														<h3>Farhana Amrin</h3>
														<p>Support Admin <small><i class='fa fa-music'></i> Playing Beethoven Classics</small></p>
													</div>
												</div>
											"> 
											<i></i>Farhana Amrin (offline)
										</a>
									
										<a href="#" class="usr offline" 
											data-chat-id="cha6" 
											data-chat-fname="Lezley" 
											data-chat-lname="Jacob" 
											data-chat-status="incognito" 
											data-rel="popover-hover" 
											data-placement="right" 
											data-html="true" 
											data-content="
												<div class='usr-card'>
													<img src='{{ URL::asset('assets/admin/img/avatars/male.png') }}' alt='Lezley Jacob'>
													<div class='usr-card-content'>
														<h3>Lezley Jacob</h3>
														<p>Sales Director</p>
													</div>
												</div>
											"> 
											<i></i>Lezley Jacob (offline)
										</a>
										
										<a href="ajax/chat.html" class="btn btn-xs btn-default btn-block sa-chat-learnmore-btn">About the API</a>

									</div>
									<!-- END DISPLAY USERS -->
								</li>
							</ul>	
						</li> --}}
					</ul>
				</nav>
				

				<span class="minifyme" data-action="minifyMenu"> 
					<i class="fa fa-arrow-circle-left hit"></i> 
				</span>

			</aside>
			<!-- END NAVIGATION -->
		@endif

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			
			@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
				<!-- RIBBON -->
				<div id="ribbon">

					{{-- <span class="ribbon-button-alignment"> 
						<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
							<i class="fa fa-refresh"></i>
						</span> 
					</span> --}}

					<!-- breadcrumb -->
					<ol class="breadcrumb">
						@if(isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs))
							@foreach ($breadcrumbs as $breadcrumb)
								<li>{{$breadcrumb}}</li>
							@endforeach
						@else
							<li>Home</li><li>Dashboard</li>
						@endif
					</ol>
					<!-- end breadcrumb -->

					<!-- You can also add more buttons to the
					ribbon for further usability

					Example below:

					<span class="ribbon-button-alignment pull-right">
					<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
					<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
					<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
					</span> -->

				</div>
				<!-- END RIBBON -->
			@endif

			<!-- MAIN CONTENT -->
			<div id="content" class="@if(!(\Auth::user()->hasRole(['super-admin','sub-admin'])))container @endif">

				@yield('content')

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		@if(\Auth::user()->hasRole(['super-admin','sub-admin']))
			<!-- PAGE FOOTER -->
			<div class="page-footer">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<span class="txt-color-white">{!! env('COPYRIGHT') !!}</span>
					</div>
					{{-- 
					<div class="col-xs-6 col-sm-6 text-right hidden-xs">
						<div class="txt-color-white inline-block">
							<i class="txt-color-blueLight hidden-mobile">Last account activity <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>
							<div class="btn-group dropup">
								<button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
									<i class="fa fa-link"></i> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right text-left">
									<li>
										<div class="padding-5">
											<p class="txt-color-darken font-sm no-margin">Download Progress</p>
											<div class="progress progress-micro no-margin">
												<div class="progress-bar progress-bar-success" style="width: 50%;"></div>
											</div>
										</div>
									</li>
									<li class="divider"></li>
									<li>
										<div class="padding-5">
											<p class="txt-color-darken font-sm no-margin">Server Load</p>
											<div class="progress progress-micro no-margin">
												<div class="progress-bar progress-bar-success" style="width: 20%;"></div>
											</div>
										</div>
									</li>
									<li class="divider"></li>
									<li>
										<div class="padding-5">
											<p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
											<div class="progress progress-micro no-margin">
												<div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
											</div>
										</div>
									</li>
									<li class="divider"></li>
									<li>
										<div class="padding-5">
											<button class="btn btn-block btn-default">refresh</button>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>--}}
				</div> 
			</div>
			<!-- END PAGE FOOTER -->
		

			<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
			Note: These tiles are completely responsive,
			you can add as many as you like
			-->
			<div id="shortcut">
				<ul>
					<li>
						<a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
					</li>
					<li>
						<a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
					</li>
					<li>
						<a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
					</li>
					<li>
						<a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
					</li>
					<li>
						<a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
					</li>
					<li>
						<a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
					</li>
				</ul>
			</div>
			<!-- END SHORTCUT AREA -->
		@endif
		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="{{ URL::asset('assets/admin/js/plugin/pace/pace.min.js') }}"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ URL::asset("assets/admin/js/libs/jquery-2.1.1.min.js") }}"><\/script>');
			}
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ URL::asset("assets/admin/js/libs/jquery-ui-1.10.3.min.js") }}"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{ URL::asset('assets/admin/js/app.config.js') }}"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{ URL::asset('assets/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="{{ URL::asset('assets/admin/js/bootstrap/bootstrap.min.js') }}"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="{{ URL::asset('assets/admin/js/notification/SmartNotification.min.js') }}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{ URL::asset('assets/admin/js/smartwidgets/jarvis.widget.min.js') }}"></script>

		<!-- EASY PIE CHARTS -->
		<script src="{{ URL::asset('assets/admin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>

		<!-- SPARKLINES -->
		<script src="{{ URL::asset('assets/admin/js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>

		<!-- JQUERY VALIDATE -->
		<script src="{{ URL::asset('assets/admin/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{ URL::asset('assets/admin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="{{ URL::asset('assets/admin/js/plugin/select2/select2.min.js') }}"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="{{ URL::asset('assets/admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>

		<!-- browser msie issue fix -->
		<script src="{{ URL::asset('assets/admin/js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>

		<!-- FastClick: For mobile devices -->
		<script src="{{ URL::asset('assets/admin/js/plugin/fastclick/fastclick.min.js') }}"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		{{-- <script src="{{ URL::asset('assets/admin/js/demo.min.js') }}"></script> --}}

		<!-- MAIN APP JS FILE -->
		<script src="{{ URL::asset('assets/admin/js/app.min.js') }}"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="{{ URL::asset('assets/admin/js/speech/voicecommand.min.js') }}"></script>

		<!-- SmartChat UI : plugin -->
		<script src="{{ URL::asset('assets/admin/js/smart-chat-ui/smart.chat.ui.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/smart-chat-ui/smart.chat.manager.min.js') }}"></script>
		
		<!-- PAGE RELATED PLUGIN(S) -->
		
		<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		<script src="{{ URL::asset('assets/admin/js/plugin/flot/jquery.flot.cust.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/plugin/flot/jquery.flot.resize.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/plugin/flot/jquery.flot.time.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/plugin/flot/jquery.flot.tooltip.min.js') }}"></script>
		
		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="{{ URL::asset('assets/admin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
		
		<!-- Full Calendar -->
		<script src="{{ URL::asset('assets/admin/js/plugin/moment/moment.min.js') }}"></script>
		<script src="{{ URL::asset('assets/admin/js/plugin/fullcalendar/jquery.fullcalendar.min.js') }}"></script>
		
		{{-- bootstrapValidator --}}
		<script src="{{ URL::asset('assets/admin/js/bootstrap/bootstrapValidator.min.js') }}"></script>
		
		<script type="text/javascript" src="{{asset('assets/js/scripts.js')}}?v={{env('APP_VERSION',1)}}"></script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		{{-- <script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script> --}}
		{{--  --}}
		@yield('script')
		{{--  --}}
		@yield('tracking_script')
	</body>

</html>