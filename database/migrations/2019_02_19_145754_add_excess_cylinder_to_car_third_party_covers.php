<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcessCylinderToCarThirdPartyCovers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_third_party_covers', function (Blueprint $table) {
            $table->boolean('above_cylinder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_third_party_covers', function (Blueprint $table) {
            $table->boolean('above_cylinder');
        });
    }
}
