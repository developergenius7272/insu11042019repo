@php 
    $counter=0;
    $rowCounter=0;
    $passedMake=[];
    $passedBody=[];
    
@endphp
@foreach( $variantMakeArray as $make )
    @foreach( $variantBodyTypeArray as $bodyType )
        @if( isset($variantDataArray[$make][$bodyType]))
            @foreach($variantDataArray[$make][$bodyType] as $car )
                @if(!in_array($make,$passedMake))
                    @php $rowCounter=0 @endphp
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <b>Make :: {{$passedMake[] = $make}}</b>
                            </label>
                        </div>
                    </div>
                    
                @endif
                <div class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox" id="car_variants_{{$car->id}}" name="car_variants[]" checked="checked" value="{{$car->id}}"> <span>{{$car->name}} ({{$bodyType}})</span>
                        </label>
                    </div>
                </div>
                @php
                    ++$rowCounter; 
                    if($rowCounter%3 ==0 ){
                        $rowCounter=0;
                        echo "<div class='clearfix $counter'></div>";
                    }
                @endphp
                            

                
            @endforeach
        @endif
    @endforeach
@endforeach