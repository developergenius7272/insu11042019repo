<?php

namespace App\Http\Controllers;
// 
use App\Models\EmailTemplate;
use App\Models\EmailTemplateBase;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminEmailTemplateController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Email Templates'; 
        $this->data['pageTitle'] = 'Email Templates';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Email Templates';
        //$this->data['carMakes'] = CarMake::with('carmodels')->get();
        $this->data['emailTeplates'] = EmailTemplate::all();
        return view('admin.email_teplates.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new email teplate';
        return view('admin.email_teplates.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Update Email Template';
        $this->data['data'] = EmailTemplate::find($id);
        $this->data['bases'] = EmailTemplateBase::all();

        if( $this->data['data'] ) {
            return view('admin.email_teplates.create_edit',$this->data);
        } else {
            return redirect()->route('admin.email-templates.index')->with('error','No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $adminID = Auth::id();
        
        $emailTemplate = EmailTemplate::find($id);
        if( $emailTemplate ) {
            $emailTemplate->update($request->all());
            return redirect()->route('admin.email-templates.index')->with('success', 'Email Template updated successfully');
        }else{
            return redirect()->route('admin.email-templates.index')->with('error', 'Email Template not updated');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function preview(Request $request)
    {
        try {
            $error_message = '';

            $template_code      = $request->template_code;
            $subject            = $request->default_subject;
            $from_name          = env('APP_NAME','Insure Online');
            $from_email         = $request->default_from_email;
            $title              = $request->title;
            $main_html          = $request->main_html_current;
            $button_title       = $request->button_title;
            $button_link        = $request->button_link;
            // 
            $to_name            = "Insure Online";
            // 
            $preview_email        = $request->preview_email;
            if ($preview_email == '') {
                $preview_email = env("MAIL_ADMIN_EMAIL");
            }
            $to_email           = $preview_email;

            // create and send
            $from   = new \SendGrid\Email($from_name, $from_email);
            $to     = new \SendGrid\Email($to_name, $to_email);

            $content    = new \SendGrid\Content("text/html", " ");
            $mail       = new \SendGrid\Mail($from, $subject, $to, $content);

            // special case for no clicking tracking in Send Grid
            $main_html = str_replace('class="noclicktracking"', 'clicktracking=off', $main_html);

            $mail->addSection("{{body_section}}",$main_html);

            $mail->personalization[0]->addSubstitution("{{subject}}",$subject);
            $mail->personalization[0]->addSubstitution("{{title}}",$title);
            $mail->personalization[0]->addSubstitution("{{button_title}}",$button_title);
            $mail->personalization[0]->addSubstitution("{{button_link}}",$button_link);
            $mail->personalization[0]->addSubstitution("{{body}}","{{body_section}}");

            $mail->setTemplateId($template_code);
            $apiKey = env('SENDGRID_APIKEY', false);

            $sg = new \SendGrid($apiKey);
            try {
                $mail->addCustomArg("environment", env("APP_ENV"));
                try {
                    $response = $sg->client->mail()->send()->post($mail);
                    if ($response->statusCode() == 400) {
                        $error_message = 'Preview Error: Send Grid issue01: '.json_encode( json_decode($response->body()), JSON_PRETTY_PRINT);
                    }
                } catch (\Exception $eerrormail) {
                    $error_message = 'Preview Error: Send Grid issue02: '.$eerrormail->getMessage();
                }

            } catch (\Exception $efinal) {
                $error_message = 'Preview Error: Send Grid issue03: '.$efinal->getMessage();
            }

        } catch (\Exception $espot) {
            $error_message = "Preview Send Error04: " . $espot->getMessage();
        }

        if ($error_message == "") {
            $error_message = "Preview Successfully Sent!";
        }

        return response()->json(['error_message' => $error_message]);
        
    }
}
