<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonOrder extends Model
{
  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'parent_order_id', 'total_premium', 'vat','vat_amount','policy_fees'
    ];

    //relationship with Order Model
    
    public function order()
    {
        return $this->belongsTo('App\Models\Order','parent_order_id','id');
    }
}
