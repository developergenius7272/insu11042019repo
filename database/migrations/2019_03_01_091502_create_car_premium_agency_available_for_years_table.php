<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPremiumAgencyAvailableForYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_premium_agency_available_for_years', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_premiums_id')->nullable();
            $table->tinyInteger('year_number')->default(0)->nullable();
            $table->double('rate')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('car_premium_agency_available_for_years');
    }
}
