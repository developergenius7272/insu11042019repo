<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyCarDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_car_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->integer('car_make_id')->nullable();
            $table->integer('car_model_id')->nullable();
            $table->integer('car_model_variant_id')->nullable();
            $table->year('vehicle_year')->nullable();
            $table->integer('vehicle_value')->nullable();
            $table->date('vehicle_registration_date')->nullable();
            $table->string('city_of_registration',50)->nullable();
            $table->tinyInteger('is_car_new')->nullable();
            $table->tinyInteger('is_vehicle_modified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_car_details');
    }
}
