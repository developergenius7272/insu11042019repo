<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarModelVariant extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_model_id', 'name', 'make_year', 'car_model_body_type_id', 'cylinders', 'car_model_fuel_id', 'car_model_engine_size_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    
    ];

    public function carModel()
    {
        return $this->belongsTo('App\Models\CarModel')->where('status', '=', 1);    
    }

    public function carModelBodyType()
    {
        return $this->belongsTo('App\Models\CarModelBodyType')->where('status', '=', 1);    
    }
    // 
    public function CarPremiumVariant()
    {
        return $this->hasMany('App\Models\CarPremiumVariant');
    }
    //
    public function scopeActive($query)
    {
        return $query->where('car_model_variants.status',1);    
    }
}
