@extends('layouts.admin.master')
{{-- Cars table view --}}
@section('content')
<div class="row">
   <div>
       @if ($errors->any())
           <div class="alert alert-danger">
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </div>
       @endif
   </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>

                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarModelBodyType" method="POST" @if($formType == 'edit') action="{{action('AdminCarModelBodyController@update', $data->id)}}" @else action="{{url('/admin/cars/car-model-body')}}" @endif>
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <legend>{{$formTitle ?? ' '}}</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ $formType == 'edit' ? 'Update' : 'Enter'}} Car Body Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {{-- ui-autocomplete-loading --}}
                                            <input class="form-control " placeholder="e.g. Sedan" name="name" id="carModelBodyTypeName" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        <?php $statusArry = array( 0  => 'Inactive' , 1 => 'Active' ); ?>
                                                        @foreach($statusArry as $key => $value)
                                                            @if($key == $data->status) 
                                                                <option class="form-control"  selected  value="{{ $key }}" > {{ $value }}</option>
                                                            @else
                                                                <option class="form-control" value="{{ $key }}" > {{ $value }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label">Car Modal Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="carModalDescription" placeholder="Car Modal Description" rows="4"></textarea>
                                    </div>
                                </div> --}}
                            
                            </fieldset>
                            {{-- Keywords  --}}
                            <fieldset>
                                <legend>Keywords</legend>
                                {{--  --}}
                                <div id="keywordAddMoreSection">
                                    {{-- Existing --}}
                                    @if(isset($data->keyword))
                                    @php $keywords = $data->keyword @endphp
                                        @foreach($keywords as $keyword)
                                        <div class="form-group" >
                                            <label class="control-label col-md-2">Name</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        {{-- ui-autocomplete-loading --}}
                                                    <input class="form-control keywordText" placeholder="Keyword" name="keyword[]" id="keyword" type="text" value="{{$keyword->keyword}}">
        
                                                    </div>
                                                </div>
                                                <span class="help-block hide">Keyword already exist</span>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                    <div id="keywordAddBlock">
                                        <div class="form-group" >
                                            <label class="control-label col-md-2">Name</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        {{-- ui-autocomplete-loading --}}
                                                    <input class="form-control keywordText" placeholder="Keyword" name="keyword[]" id="keyword" type="text" value="">
        
                                                    </div>
                                                </div>
                                                <span class="help-block hide">Keyword already exist</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Add More</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <button type="button" id="addMoreKeyword"><i class="fa fa-plus-square"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/cars/car-model-body')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
		
    $(document).ready(function() {

        pageSetUp();

        $("#formCarModelBodyType").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $("#addMoreKeyword").click(function() {
            var newHtml = $("#keywordAddBlock");
            newHtml.find('.form-group').removeClass('has-warning')
            newHtml.find('.help-block').addClass('hide');
            $("#keywordAddMoreSection").append(newHtml.html());
        });
         $("#keywordAddMoreSection").on('blur', '.keywordText', function() {
            var keyword = $.trim($(this).val());
            // $(this).parents('.form-group').addClass('has-error');
            if( keyword == '' ) {

            } else {
                // check if keyworkd already exist
                var bodyId = '{{$data->id ?? 0}}'
                $(this).parents('.form-group').removeClass('has-warning')
                        .find('.help-block').addClass('hide');
                $.ajax({
                type:'POST',
                url:'{{route('admin.carModelBodyKeywordExist')}}',
                context: this,
                data:{bodyId:bodyId, name:keyword },

                success:function(data){
                    if( data.action == '0' ) {
                        $(this).parents('.form-group')
                        .addClass('has-warning')
                        .find('.help-block').removeClass('hide');
                    }
                }
            });
            }
        });
    })

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var carModelBodyTypeName  = $.trim($("#carModelBodyTypeName").val());        

        if (carModelBodyTypeName == ''){
            $("#carModelBodyTypeName").parent().find(".my-error").html('Please enter Car Body Name').show();
            $('#carModelBodyTypeName').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        return is_valid;
    }
    
    function hideErrorDiv() {

        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
