<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// 
use App\Models\CarPremium;
// 
class CreateCarPremiumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('car_premiums');
        Schema::create('car_premiums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_provider_id')->nullable();
            $table->integer('car_model_body_type_id')->default(0)->nullable();
            $table->integer('car_model_min_value')->default(0)->nullable();
            $table->integer('car_model_max_value')->default(0)->nullable();
            $table->unsignedTinyInteger('min_age')->default(0)->nullable();
            $table->unsignedTinyInteger('max_age')->default(0)->nullable();
            $table->tinyInteger('car_model_cylinder')->default(0)->nullable();
            $table->double('min_premium_agency')->default(0)->nullable();
            $table->double('min_premium_agency_rate')->default(0)->nullable();
            $table->double('min_premium_nonagency')->default(0)->nullable();
            $table->double('min_premium_nonagency_rate')->default(0)->nullable();
            $table->tinyInteger('available_for_months')->after('min_premium_nonagency_rate')->default('0')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->timestamps();
        });
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 1;  //THE ORIENTAL INSURANCE COMPANY LTD
        $CarPremium->car_model_body_type_id = 1;
        $CarPremium->min_age = 25;
        $CarPremium->max_age = 100;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 3.2;
        $CarPremium->min_premium_nonagency = 1300;
        $CarPremium->min_premium_nonagency_rate = 3;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 1;  //THE ORIENTAL INSURANCE COMPANY LTD
        $CarPremium->car_model_body_type_id = 2;
        $CarPremium->min_age = 25;
        $CarPremium->max_age = 100;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 3.2;
        $CarPremium->min_premium_nonagency = 1300;
        $CarPremium->min_premium_nonagency_rate = 3;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 1;  //THE ORIENTAL INSURANCE COMPANY LTD
        $CarPremium->car_model_body_type_id = 3;
        $CarPremium->min_age = 25;
        $CarPremium->max_age = 100;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 3.2;
        $CarPremium->min_premium_nonagency = 1300;
        $CarPremium->min_premium_nonagency_rate = 3;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 1;  //THE ORIENTAL INSURANCE COMPANY LTD
        $CarPremium->car_model_body_type_id = 4;
        $CarPremium->min_age = 25;
        $CarPremium->max_age = 100;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 3.2;
        $CarPremium->min_premium_nonagency = 1700;
        $CarPremium->min_premium_nonagency_rate = 2.7;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 1;  //THE ORIENTAL INSURANCE COMPANY LTD
        $CarPremium->car_model_body_type_id = 5;
        $CarPremium->min_age = 25;
        $CarPremium->max_age = 100;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 3.2;
        $CarPremium->min_premium_nonagency = 1600;
        $CarPremium->min_premium_nonagency_rate = 2.7;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 2;  //Union Insurance
        $CarPremium->car_model_body_type_id = 1;
        $CarPremium->car_model_max_value = 7000;
        $CarPremium->min_premium_agency = 1500;
        $CarPremium->min_premium_agency_rate = 4;
        $CarPremium->min_premium_nonagency = 1300;
        $CarPremium->min_premium_nonagency_rate = 3.05;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 2;  //Union Insurance
        $CarPremium->car_model_body_type_id = 3;
        $CarPremium->car_model_min_value = 0;
        $CarPremium->car_model_max_value = 7000;
        $CarPremium->min_premium_agency = 2000;
        $CarPremium->min_premium_agency_rate = 4;
        $CarPremium->min_premium_nonagency = 2000;
        $CarPremium->min_premium_nonagency_rate = 3.05;
        $CarPremium->save();
        // 
        $CarPremium = new CarPremium();
        $CarPremium->insurance_provider_id = 2;  //Union Insurance
        $CarPremium->car_model_body_type_id = 7;
        $CarPremium->car_model_min_value = 0;
        $CarPremium->car_model_max_value = 7000;
        $CarPremium->min_premium_agency = 2500;
        $CarPremium->min_premium_agency_rate = 4.5;
        $CarPremium->min_premium_nonagency = 2500;
        $CarPremium->min_premium_nonagency_rate = 3.7;
        $CarPremium->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_premiums');
    }
}
