<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplateBase;

class EmailTemplateBaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $templates = [
	            [
	                'template_id' => '67433146-ba4a-4e5c-ae04-9404706a3240',
	                'name' => 'TemplateA -HeadlineBodyCTA',
	                'status' => 1
	            ],
	             [
	                'template_id' => 'c218906e-68aa-40b4-8875-344dd77ba8be',
	                'name' => 'TemplateB-HeadlineBody',
	                'status' => 1
	            ],
	             [
	                'template_id' => '18db4778-6b94-4774-93fe-a6ae17cbac32',
	                'name' => 'TemplateC-Blank',
	                'status' => 1
	            ],
			];
		foreach($templates as $template) {
			EmailTemplateBase::create($template);
		}
    }
}
