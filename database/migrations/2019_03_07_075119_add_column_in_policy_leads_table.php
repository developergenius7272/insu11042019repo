<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPolicyLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('policy_leads', function (Blueprint $table) {
            $table->string('mobile')->nullable();
            $table->string('email');
            $table->string('name');
            $table->string('mobile_otp')->nullable();
            $table->string('email_otp')->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('policy_leads', function (Blueprint $table) {
            $table->dropColumn('mobile');
            $table->dropColumn('email');
            $table->dropColumn('mobile_otp')->nullable();
            $table->dropColumn('email_otp')->nullable();
            
        });
    }
}
