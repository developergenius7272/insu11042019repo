@extends('layouts.admin.master')
{{-- Car fuel add/edit table view --}}
@section('content')
<!-- <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script> -->

<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Pages</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formEmailTemplate" method="POST" @if($formType == 'edit') action="{{action('AdminPageController@update', $data->id)}}" @else action="{{route('admin.pages.store')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <legend>{{$formTitle ?? ' '}}</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Title</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Title" name="title" id="title" type="text" value="{{ $errors->any() ? old('title') : $data->title ?? '' }}">
                                            

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Slug</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Slug" name="slug" id="slug" type="text" value="{{ $errors->any() ? old('slug') : $data->slug ?? '' }}">
                                            

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-2">Heading</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Heading" name="heading" id="heading" type="text" value="{{ $errors->any() ? old('code') : $data->heading ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Sub Heading</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Sub Heading" name="sub_heading" id="sub_heading" type="text" value="{{ $errors->any() ? old('sub_heading') : $data->sub_heading ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2">Body (html)</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <textarea cols="60" id="main_html" name="body" class="form-control" required="required">{{isset($data['body']) ? $data->body : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--  -->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Page Banner</label>
                                    <div class="col-md-4">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_banner == 1) 
                                            <img id="imgCarMake" src='{{ asset('uploads/pages').'/'.$data->id.'_main' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="{{$data->id}}" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                <!--  -->

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.pages.index')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="main_html_current" id="main_html_current">

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')

 
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('assets/admin/js/plugin/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        CKEDITOR.replace( 'main_html', { height: '380px', startupFocus : true} );
        pageSetUp();

        $('#title').on('change', function(){
            var currentSlug = $.trim($("#slug").val());
            var name = $.trim($(this).val());
            if (name !== '' && currentSlug === ''){
                suggest_slug(name);
            }
        });

        $('#slug').on('change', function(){
            var slug = $.trim($(this).val());
            if( slug === '' ) {
                slug = $.trim($('#title').val());
            }
            if ( slug !== '' ){
                suggest_slug( slug );
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgCarMake").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        })
    })

    function suggest_slug(slug){
        if(slug !== ''){
            var id = '{{ $data->id ?? '' }}';
            $.ajax({
                type: "POST",
                url: '{{route("admin.pages.suggestSlug")}}',
                dataType: "json",
                data: {slug:slug, id:id},
                success: function(res) {
                    if(res.action == 1){
                        $('#slug').val(res.slug);
                    }
                },
                error: function() {}
            });
        }
    }
</script>
@endsection
