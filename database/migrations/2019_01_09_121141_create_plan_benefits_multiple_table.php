<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanBenefitsMultipleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_benefits_multiple', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_benefits_id')->nullable();
            $table->tinyInteger('percentage_or_amount')->default(0)->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->string('feature_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('plan_benefits_multiple');
    }
}
