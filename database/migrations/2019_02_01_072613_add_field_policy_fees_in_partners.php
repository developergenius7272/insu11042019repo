<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPolicyFeesInPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_providers', function(Blueprint $table) {
            $table->double('base_commission_percentage')->after('third_party_available')->default(0)->nullable();
            $table->double('base_commission_amount')->after('base_commission_percentage')->default(0)->nullable();
            $table->double('bonus_percentage')->after('base_commission_amount')->default(0)->nullable();
            $table->double('bonus_amount')->after('bonus_percentage')->default(0)->nullable();
            $table->double('policy_fees_percentage')->after('bonus_amount')->default(0)->nullable();
            $table->double('policy_fees_amount')->after('policy_fees_percentage')->default(0)->nullable();
            $table->double('upto_max_amount')->after('policy_fees_amount')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
