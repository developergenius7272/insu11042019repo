<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarValue extends Model
{
    protected $fillable = [
        'car_make_id', 'car_model_id', 'car_model_variant_id', 'values', 'min_value', 'max_value', 'insurance_year', 'manufacture_year', 'effective_date',
    ];

    protected $dates = ['effective_date'];
}
