<?php

namespace App\Http\Controllers;

use App\Models\AddonCategory;
use App\Models\CrossSellingProducts;
use App\Models\InsuranceProvider;
use App\User;
use App\Models\UserAgentAddonCommission;
use Auth;
use Illuminate\Http\Request;
use Image;

class AdminCrossSellingProductsController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Cross Selling Products';
        $this->data['pageTitle']     = 'Cross Selling Products';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = "Cross Selling Products";
        $products                   = CrossSellingProducts::all();
        //
        $this->data['products'] = $products;
        return view('admin.cross_selling_products.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType']  = 'add';
        $this->data['formTitle'] = 'Add Cross Selling Product';
        $partners                = InsuranceProvider::active()->orderBy('name', 'asc')->get();
        $addoncategories         = AddonCategory::active()->orderBy('name', 'asc')->get();
        //
        $this->data['partners']        = $partners;
        $this->data['addoncategories'] = $addoncategories;
        //
        return view('admin.cross_selling_products.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name                = $request->name;
        $insuranceProviderId = $request->insuranceProviderId;
        // $addon_commission_type   = $request->addon_commission_type;
        // $addon_commission_value   = $request->addon_commission_value;
        $addon_category_id   = $request->addon_category_id;
        $provider_name       = $request->provider_name;
        $premium             = $request->premium;
        $benefits            = $request->benefits;
        $sum_insured         = $request->sum_insured;
        $status              = $request->status;
        //
        $vat      = $request->vat;
        $fee_type = $request->fee_type;
        $fee      = $request->fee;

        //Conditions
        // 1. General Conditions

        request()->validate([
            'name'                => 'required',
            'insuranceProviderId' => 'required|numeric',
            'addon_category_id'   => 'required|numeric',
            //"provider_name"    => "required",
            "premium"             => "required|numeric|min:1",

        ]);

        if (!empty($name) && ($premium)) {
            $product                                 = new CrossSellingProducts();
            $product->name                           = $name;
            $product->insurance_provider_id          = $insuranceProviderId;
            // $product->addon_commission_type          = $addon_commission_type;
            // $product->addon_commission_value         = $addon_commission_value;
            $product->addon_category_id              = $addon_category_id;
            $product->provider_name                  = $provider_name;
            $product->premium                        = $premium;
            $product->benefits                       = $benefits;
            $product->sum_insured                    = $sum_insured;
            $product->status                         = $status;
            $product->created_by                     = $adminID;
            $product->modified_by                    = $adminID;
            //
            $product->vat                            = $vat;
            $product->fee_type                       = $fee_type;
            $product->fee                            = $fee;
            $product->save();

            // upload image
            $imageName = '';
            try {
                if ($request->hasFile('image')) {

                    // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();

                    $imageName = $product->id . '.' . $request->image->getClientOriginalExtension();
                    $imageName = $product->id . '_main';
                    $request->image->move(public_path('uploads/admin/cross_selling_product'), $imageName);
                    // resize for logo
                    $imageLogo = $product->id . '_logo';
                    $img       = Image::make(public_path('uploads/admin/cross_selling_product/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/admin/cross_selling_product/' . $imageLogo));

                    $product->has_logo = 1;
                    $product->save();

                }
            } catch (\Exception $e) {
                // do task when error
                // echo $e->getMessage();
            }

            //$partners  = InsuranceProvider::all()->toArray();
            $partner = InsuranceProvider::where('id',$insuranceProviderId)->first();

            // set all agetnts base commision for this partner
            if( $partner->agent_addon_base_commission_percentage > 0 || $partner->agent_addon_base_commission_amount > 0 ) {
                $agents = User::withRole('agent')->get();
                if($agents->count()) {
                    foreach($agents as $agent) {
                        $addonCommision                        = new UserAgentAddonCommission();
                        $addonCommision->user_id               = $agent->id;
                        //$addonCommision->insurance_provider_id = $partner->id;
                        $addonCommision->cross_selling_product_id = $product->id;
                        $addonCommision->percentage_or_amount  = $partner->agent_addon_base_commission_percentage > 0 ? 1 : 2;
                        $addonCommision->amount                = $partner->agent_addon_base_commission_percentage > 0 ? $partner->agent_addon_base_commission_percentage : $partner->agent_addon_base_commission_amount ;
                        $addonCommision->save();
                    }
                }
            }

            return redirect()->route('admin.cross-selling-products.index')->with('success', 'Cross Selling Product added successfully');

        } else {
            return redirect()->route('admin.cross-selling-products.index')->with('error', 'Cross Selling Product not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = CrossSellingProducts::find($id);
        if ($product) {
            $this->data['formType']  = 'edit';
            $this->data['formTitle'] = 'Update Cross Selling Product';
            //
            $partners        = InsuranceProvider::active()->orderBy('name', 'asc')->get();
            $addoncategories = AddonCategory::active()->orderBy('name', 'asc')->get();
            //
            $this->data['partners']        = $partners;
            $this->data['addoncategories'] = $addoncategories;
            $this->data['data']            = $product;
            //
            return view('admin.cross_selling_products.create_edit', $this->data);
        } else {
            return redirect('/admin/cross-selling-products')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name                = $request->name;
        $insuranceProviderId = $request->insuranceProviderId;
        // $addon_commission_type   = $request->addon_commission_type;
        // $addon_commission_value   = $request->addon_commission_value;
        $addon_category_id   = $request->addon_category_id;
        $provider_name       = $request->provider_name;
        $premium             = $request->premium;
        $benefits            = $request->benefits;
        $sum_insured         = $request->sum_insured;
        $status              = $request->status;
        //
        $vat      = $request->vat;
        $fee_type = $request->fee_type;
        $fee      = $request->fee;

        //Conditions
        // 1. General Conditions
        request()->validate([
            'name'                => 'required',
            'insuranceProviderId' => 'required|numeric',
            'addon_category_id'   => 'required|numeric',
            //"provider_name"    => "required",
            "premium"             => "required|numeric|min:1",

        ], [
            'name.required' => "Product Name is required.",
        ]);

        if (!empty($name) || !empty($premium)) {
            $product = CrossSellingProducts::find($id);
            if ($product) {

                $product->name                  = $name;
                $product->insurance_provider_id = $insuranceProviderId;
                // $product->addon_commission_type          = $addon_commission_type;
                // $product->addon_commission_value         = $addon_commission_value;
                $product->addon_category_id     = $addon_category_id;
                $product->provider_name         = $provider_name;
                $product->premium               = $premium;
                $product->benefits              = $benefits;
                $product->sum_insured           = $sum_insured;
                $product->status                = $status;
                $product->modified_by           = $adminID;
                //
                $product->vat      = $vat;
                $product->fee_type = $fee_type;
                $product->fee      = $fee;

                if ($request->has_logo_image == '0') {
                    //$product->has_logo = $request->has_logo_image;
                    $product->has_logo = 0;
                }

                $imageName = '';
                try {
                    if ($request->hasFile('image')) {
                        // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $product->id . '_main';
                        $request->image->move(public_path('uploads/admin/cross_selling_product'), $imageName);
                        // resize for logo
                        $imageLogo = $product->id . '_logo';
                        $img       = Image::make(public_path('uploads/admin/cross_selling_product/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/admin/cross_selling_product/' . $imageLogo));

                        $product->has_logo = 1;
                    }

                } catch (\Exception $e) {
                    // do task when error
                    // echo $e->getMessage();
                }
                $product->save();

                return redirect('/admin/cross-selling-products')->with('success', 'Cross Selling Product updated successfully');
            } else {
                return redirect('/admin/cross-selling-products')->with('error', 'Cross Selling Product not updated');
            }
        } else {
            return redirect('/admin/cross-selling-products')->with('error', 'Cross Selling Product not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getinsurancedetail(Request $request) {
        
        $response = array();

        echo $insuranceProviderId = $request->insuranceProviderId;die;
        //$homepageid = $request->homepageid;
        //$valDisplay = $request->valDisplay;
        /*
        $countHasOneValue = Homepage::where('display_at_home','=','1')->count();
        if ($valDisplay == 0)
        {
            if ($countHasOneValue == 1){
                //echo "no";
                $response['status'] = 1;
                echo json_encode($response) ;
            }
            else{
                // echo "yes";
                Homepage::where('id','<>',$homepageid)->update(['display_at_home' => 0]);
                Homepage::where('id',$homepageid)->update(['display_at_home' => $valDisplay]);
            }
        }
        else{
            Homepage::where('id','<>',$homepageid)->update(['display_at_home' => 0]);
            Homepage::where('id',$homepageid)->update(['display_at_home' => $valDisplay]);
        }
        */
        /*
        $response = array();
        $response['status'] = 1;
        echo json_encode($response) ;
        */
    }

}
