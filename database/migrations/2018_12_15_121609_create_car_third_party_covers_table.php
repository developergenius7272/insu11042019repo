<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarThirdPartyCoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_third_party_covers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_provider_id')->nullable();
            $table->integer('car_model_body_type_id')->default(0)->nullable();
            // $table->integer('car_model_min_value')->default(0)->nullable();
            // $table->integer('car_model_max_value')->default(0)->nullable();
            $table->unsignedTinyInteger('min_age')->default(0)->nullable();
            $table->unsignedTinyInteger('max_age')->default(0)->nullable();
            $table->tinyInteger('car_model_cylinder')->default(0)->nullable();
            $table->tinyInteger('driving_license_min_age')->default(0)->nullable();
            $table->tinyInteger('driving_license_max_age')->default(0)->nullable();
            // $table->double('min_premium_agency')->default(0)->nullable();
            // $table->double('min_premium_agency_rate')->default(0)->nullable();
            // $table->double('min_premium_nonagency')->default(0)->nullable();
            // $table->double('min_premium_nonagency_rate')->default(0)->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_third_party_covers');
    }
}
