<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_payment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->unsignedDecimal('transaction_amount')->nullable();
            $table->unsignedDecimal ('tax')->nullable();
            $table->integer ('payment_type')->nullable();
            $table->dateTime ('transaction_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_payment_details');
    }
}
