<?php

namespace App\Http\Controllers;
use App\Models\Homepage;

use Illuminate\Http\Request;
use Image;
use Auth;

class HomepageController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:super-admin|sub-admin');
        $this->data['breadcrumbs'][] = 'Homepages'; 
        $this->data['pageTitle'] = 'Home Pages';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Home Pages';
        $this->data['homepages'] = Homepage::all();
        //dd($this->data['homepages']);
        return view('admin.homepages.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Add a new Homepage';
        return view('admin.homepages.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $heading = $request->heading;
        $tag_line = $request->tag_line;
        $button_text = $request->button_text;
        $button_url = $request->button_url;   

        request()->validate([
            'name' => 'required|min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            //'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);


        if(!empty( $name ))
        {
            $body = Homepage::where('name',$name)->first();
            if( !$body ) {
                $body = new Homepage();
                $body->name = $name;
                $body->heading = $heading;
                $body->tag_line = $tag_line;
                $body->button_text = $button_text;
                $body->button_url = $button_url;
                //$body->display_at_home = $display_at_home;
                $body->created_by = $adminID;
                $body->modified_by = $adminID;
                $body->save();

                // upload image
                $imageName = '';
                try{
                    if ($request->hasFile('image')) { 
                        
                        // $imageName = $car->id.'.'.request()->image->getClientOriginalExtension();
                        
                        $imageName = $body->id.'.'.$request->image->getClientOriginalExtension();
                        $imageName = $body->id.'_main';
                        $request->image->move(public_path('uploads/homepage_banners'), $imageName);
                        // resize for logo
                        $imageLogo = $body->id.'_logo';
                        $img = Image::make(public_path('uploads/homepage_banners/' . $imageName));
                        $img->fit(75)->save(public_path('uploads/homepage_banners/' . $imageLogo));

                        $body->has_logo = 1;
                        $body->save();

                    }
                }
                catch(\Exception $e){
                    // do task when error
                    // echo $e->getMessage();
                }
                
                return redirect('/admin/homepages')->with('success', 'Banner added successfully');
            } else {
                return redirect('/admin/homepages')->with('error', 'Banner not added');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit homepage';
        $this->data['data'] = Homepage::find($id);
        
        if( $this->data['data'] ) {
            // 
            return view('admin.homepages.create_edit',$this->data);
        } else {
            return redirect('/admin/homepages')->with('error', 'No data found');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $heading = $request->heading;
        $tag_line = $request->tag_line;
        $button_text = $request->button_text;
        $button_url = $request->button_url;  

        request()->validate([
            'name' => "required|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.min' => "Name $name must be at least 2 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);


        if(!empty($name ))
        {
            $body = Homepage::where('name',$name)->where('id','<>',$id)->first();
            if( !$body ) {
                $body = Homepage::find($id);

                if( $body ) {
                    $body->name = $name;
                    $body->heading = $heading;
                    $body->tag_line = $tag_line;
                    $body->button_text = $button_text;
                    $body->button_url = $button_url;
                    $body->modified_by = $adminID;
                    // $body->has_logo = $request->has_logo_image;
                    // $body->save();
                    if ($request->has_logo_image == '0' ){
                        //$body->has_logo = $request->has_logo_image;
                        $body->has_logo = 0;
                    }


                    $imageName = '';
                    try{
                        if ($request->hasFile('image')) { 
                            // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                            $imageName = $body->id.'_main';
                            $request->image->move(public_path('uploads/homepage_banners'), $imageName);
                            // resize for logo
                            $imageLogo = $body->id.'_logo';
                            $img = Image::make(public_path('uploads/homepage_banners/' . $imageName));
                            $img->fit(75)->save(public_path('uploads/homepage_banners/' . $imageLogo));

                            $body->has_logo = 1;
                        }

                    }
                    catch(\Exception $e){
                        // do task when error
                        // echo $e->getMessage();
                    }

                    $body->save();

                    return redirect('/admin/homepages')->with('success', 'Banner updated successfully');
                } else {
                    return redirect('/admin/homepages')->with('error', 'Banner not updated');
                }
            } else {
                return redirect('/admin/homepages')->with('error', 'Banner not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function editHomePage(Request $request) {
        
        $response = array();

        $homepageid = $request->homepageid;
        $valDisplay = $request->valDisplay;

        $countHasOneValue = Homepage::where('display_at_home','=','1')->count();
        if ($valDisplay == 0)
        {
            if ($countHasOneValue == 1){
                //echo "no";
                $response['status'] = 1;
                echo json_encode($response) ;
            }
            else{
                // echo "yes";
                Homepage::where('id','<>',$homepageid)->update(['display_at_home' => 0]);
                Homepage::where('id',$homepageid)->update(['display_at_home' => $valDisplay]);
            }
        }
        else{
            Homepage::where('id','<>',$homepageid)->update(['display_at_home' => 0]);
            Homepage::where('id',$homepageid)->update(['display_at_home' => $valDisplay]);
        }

        /*
        $response = array();
        $response['status'] = 1;
        echo json_encode($response) ;
        */
    }
    
}
