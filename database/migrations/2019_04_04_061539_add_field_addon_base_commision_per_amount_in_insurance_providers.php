<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAddonBaseCommisionPerAmountInInsuranceProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_providers', function (Blueprint $table) {
            $table->double('addon_base_commission_percentage')->after('base_commission_amount')->default(0)->nullable();
            $table->double('addon_base_commission_amount')->after('addon_base_commission_percentage')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_providers', function (Blueprint $table) {
            //
        });
    }
}
