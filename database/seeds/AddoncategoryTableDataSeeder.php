<?php

use Illuminate\Database\Seeder;
use App\Models\AddonCategory;

class AddoncategoryTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addoncategories = [
            [
                'name' => 'Accidental',
                'status' => 1,
            ],
            [
                'name' => 'Zero Depreciation Cover',
                'status' => 1,
            ],            
            [
                'name' => 'Roadside Assistance',
                'status' => 1,
            ],            
            [
                'name' => 'Invoice Cover',
                'status' => 1,
            ],


        ];
        foreach ($addoncategories as $addoncategory) {
            AddonCategory::create($addoncategory);
        }
    }
}
