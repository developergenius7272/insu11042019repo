<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPremiumVariantLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_premium_variant_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_premium_variant_id');
            $table->integer('car_model_variant_id');
            $table->integer('car_premium_id');
            $table->integer('deleted_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_premium_variant_logs');
    }
}
