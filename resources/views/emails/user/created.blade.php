@extends('layouts.master_email')
@section('content')
<h2 style="font-size:32px; color:#25aae1; margin:0 0 25px; padding:0; font-weight:bold;">Welcome to insureonline.</h2>
<h4 style="font-size:16px; color:#494949; margin:0 0 15px; padding:0; font-weight:bold;">Hello {{ $user->name }},</h4>
<p style="font-size:15px; color:#494949; line-height:24px;">Welcome to insureonline.
<br />
<br />Please create your new password. You can login into InsureOnline using your registered email and password.
<br />
<a href="{{$url}}" style="background: #25aae1 none repeat scroll 0 0; border-radius: 5px; color: #fff; display: block; font-size: 17px; padding: 13px; text-align: center; text-decoration: none;">Create Password</a>
<br />
<br /></p>
<br />Thanks,
<br /> <strong>{{ config('app.name') }}</strong> 
@endsection('content')

{{-- 
@component('mail::message')
Hello {{ $user->name }},

Welcome to insureonline.
Please create your new password. You can login into InsureOnline using your registered email and password.
@component('mail::button', ['url' => $url, 'color' => 'green'])
Create Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

--}}