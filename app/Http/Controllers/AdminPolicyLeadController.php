<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarModelBodyType;
use App\Models\CarModelFuel;
use App\Models\CarModelVariant;
use App\Models\InsuranceProvider;
use App\Models\PolicyLead;
// 
use Illuminate\Http\Request;
use Image;
use Auth;

class AdminPolicyLeadController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Partners'; 
        $this->data['pageTitle'] = 'Partners';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Leads';
        //$leads = PolicyLead::all();
        $leads = PolicyLead::where('order_id', null)->get();
        $this->data['leads'] = $leads;
        return view('admin.policy_leads.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Update partner';
        $this->data['tableHeading'] = 'Leads';
        // 
        $leads = PolicyLead::find($id);
        // 
        if( $leads ) {
            $this->data['data'] = $leads;
            return view('admin.policy_leads.show', $this->data);
        }else{
            return redirect('/admin/leads')->with('error', 'No data found');
        }
    }
}
