<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Auth;
use Log;
use Mail;

class AdminPanelUserController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['role:super-admin']);
        $this->data['breadcrumbs'][] = 'Admin Users'; 
        $this->data['pageTitle']     = 'Admin Users';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Admin Users';
        // 
        $superAdmin                 = User::withRole('super-admin')->where('id','!=',Auth::id())->get();
        $subAdmin                   = User::withRole('sub-admin')->where('id','!=',Auth::id())->get();
        // 
        $this->data['users']        = $superAdmin->merge($subAdmin);
        // 
        return view('admin.admin-users.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType']  = 'add';
        $this->data['formTitle'] = 'Add a new admin user';
        $this->data['roles']     = Role::all();
        return view('admin.admin-users.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();
        // 
        $name     = $request->name;
        $email    = $request->email;
        $status   = $request->status;
        $dob      = $request->dob;
        $roleName = $request->role;
        $password = $request->password;
        
        if(empty($roleName)) {
            $roleName = 'sub-admin';
        }

        // if( !empty($dob) ) {
        //     // date format should be in YMD format
        //     $dob = sqlDateDMY($dob,'-');
        // }

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        $request->merge(["password" => bcrypt($password)]);
        // 
        request()->validate([
            'name' => 'required|min:2|max:100',
            'email' => 'required|unique:users|min:2|max:100',
            //'dob'      => 'date_format:Y-m-d',
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique' => "The name $email has already been taken.",
                //'dob.date_format' => "Date should be in YYYY-mm-dd format.",
            ]
        );
        if( $user = User::create($request->all()) ) {
            
            $user->status      = $status;
            $user->dob         = $dob;
            $user->created_by  = $adminID;
            $user->modified_by = $adminID;
            
            // 
            $role = Role::where('name', $roleName)->first();
            $user->attachRole($role);
            // upload image
            $imageName = '';
            try{
                if ($request->hasFile('image')) { 
                    
                    $imageName = $user->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $user->id.'_main';
                    $request->image->move(public_path('uploads/users'), $imageName);
                    // resize for logo
                    $imageLogo = $user->id.'_logo';
                    $img = Image::make(public_path('uploads/users/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/users/' . $imageLogo));

                    $user->has_logo = 1;
                }
            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }
            // 
            $user->save();
            // 
            return redirect()->route('admin.admin-users.index')->with('success', 'User added successfully');
        } else {
            return redirect()->route('admin.admin-users.index')->with('success', 'User not added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if( $id != Auth::id() ) {
            $user = User::find($id);
            if( $user ) {
                $this->data['formType']  = 'edit';
                $this->data['formTitle'] = 'Edit user';
                $this->data['data']      = $user;
                $this->data['roles']     = Role::all();
                return view('admin.admin-users.create_edit',$this->data);
            } else {
                return redirect()->route('admin.admin-users.index')->with('error', 'No data found');
            }
        }
        return redirect()->route('admin.admin-users.index')->with('error', 'No data found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name                 = $request->name;
        $email                = $request->email;
        $mobile               = $request->mobile;
        $gender               = $request->gender;
        $address              = $request->address;
        $maritalstatus        = $request->maritalstatus;
        $dob                  = $request->dob;
        $passport_number      = $request->passport_number;
        $passport_expiry_date = $request->passport_expiry_date;
        $license_number       = $request->license_number;
        $license_expiry_date  = $request->license_expiry_date;
        $uae_id               = $request->uae_id;
        $uae_id_expiry_date   = $request->uae_id_expiry_date;
        $status               = $request->status;
        $roleName             = $request->role;
        
        if(empty($roleName)) {
            $roleName = 'sub-admin';
        }

        if( !empty($dob) ) {
            $dob = Carbon::createFromFormat('d/m/Y', $dob);
        }

        request()->validate([
            "name"  => "required|min:2|max:100",
            "email" => "required|unique:users,email,$id"
        ], 
            [
                'name.required' => "The name field is required.",
                'name.unique'   => "The name $name has already been taken.",
                'name.min'      => "Name $name must be at least 2 characters.",
                'name.max'      => "Name $name should not be greater than 100 characters.",
            ],
            [
                'email.required' => "The email field is required.",
                'email.unique'   => "The name $email has already been taken.",
            ]
        );
        
        $body = User::find($id);
        // 
        if( $body ) {

            $body->name                 = $name;
            $body->email                = $email;
            $body->mobile               = $mobile;
            $body->gender               = $gender;
            $body->address              = $address;
            $body->maritalstatus        = $maritalstatus;
            $body->dob                  = $dob;
            $body->passport_number      = $passport_number;
            $body->passport_expiry_date = $passport_expiry_date;
            $body->license_number       = $license_number;
            $body->license_expiry_date  = $license_expiry_date;
            $body->uae_id               = $uae_id;
            $body->uae_id_expiry_date   = $uae_id_expiry_date;
            $body->status               = $status;
            $body->modified_by          = $adminID;

            if ($request->has_logo_image == '0' ){
                $body->has_logo = 0;
            }
            // 
            $imageName = '';
            try{
                if ($request->hasFile('image')) { 
                    // $imageName = $car->id.'.'.$request->image->getClientOriginalExtension();
                    $imageName = $body->id.'_main';
                    $request->image->move(public_path('uploads/users'), $imageName);
                    // resize for logo
                    $imageLogo = $body->id.'_logo';
                    $img = Image::make(public_path('uploads/users/' . $imageName));
                    $img->fit(75)->save(public_path('uploads/users/' . $imageLogo));

                    $body->has_logo = 1;
                }

            }
            catch(\Exception $e){
                // do task when error
                // echo $e->getMessage();
            }
            // 
            $roles = Role::whereIn('name', ['sub-admin','super-admin'])->get();
            $body->detachRoles($roles);
            // 
            // 
            $role = Role::where('name', $roleName)->first();
            $body->attachRole($role);

            $body->save();

            return redirect()->route('admin.admin-users.index')->with('success', 'User updated successfully');
        } else {
            return redirect()->route('admin.admin-users.index')->with('error', 'User not updated!');
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
