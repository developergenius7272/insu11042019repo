@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            {{--  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>  --}}

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus hidden>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="regpassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" onKeyUp="checkPasswordStrength();" required>
                                <span style="font-size: 0.7em">(Password should have minimum 8 characters. Password should include Lower Case, Upper Case, Numbers, Special Characters)</span>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-group row">
                            <div style="margin-left:35%" id="password-strength-status"></div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>

function checkPasswordStrength() {
	var number = /([0-9])/;
	var alphabets = /([A-Z])/;
	var lowercase = /([a-z])/;
	var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
	
	if($('#regpassword').val().length<8) {
		$('#password-strength-status').removeClass();
		$('#password-strength-status').addClass('weak-password');
		$('#password-strength-status').html('<p class="text-danger">Weak (should be atleast 8 characters.)</p>');
	} else {  	
	    if($('#regpassword').val().match(number) && $('#regpassword').val().match(alphabets) && $('#regpassword').val().match(special_characters) && $('#regpassword').val().match(lowercase)) {            
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('strong-password');
			$('#password-strength-status').html('Strong');
        } else {
			$('#password-strength-status').removeClass();
			$('#password-strength-status').addClass('medium-regpassword');
			$('#password-strength-status').html('<p class="text-danger">Password should include Lower Case, Upper Case, Numbers, Special Characters (@, $, #)');
        } 
	}
}
</script>

