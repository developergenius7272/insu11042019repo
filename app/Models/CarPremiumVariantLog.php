<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarPremiumVariantLog extends Model
{
    //
    protected $table = 'car_premium_variant_logs';
    protected $fillable = ['car_premium_variant_id', 'car_model_variant_id', 'car_premium_id','deleted_by'];
    public $timestamps = true;
}
