<?php

namespace App\Http\Controllers;

use App\Models\AddonCategory;
use Illuminate\Http\Request;

class AdminAddonCategory extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('role:super-admin|sub-admin');
        $this->data['breadcrumbs'][] = 'Addon'; 
        $this->data['breadcrumbs'][] = 'Category';
        $this->data['pageTitle'] = 'Addon Category';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Addon Category';
        $this->data['addoncategories'] = AddonCategory::all();

        return view('admin.addon_category.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new Addon Category';
        return view('admin.addon_category.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $name = $request->name;
        $status = $request->status;

        //Conditions
        // 1. General Conditions
        
        request()->validate([
            'name' => 'required',
        ]);
        

        if(!empty( $name ))
        {
            $product = new AddonCategory();
            $product->name = $name;
            $product->status = $status;
            $product->save();


            return redirect()->route('admin.addoncategory.index')->with('success', 'Addon Category added successfully');

        }else{
            return redirect()->route('admin.addoncategory.index')->with('error', 'Addon Category not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $addoncategory = AddonCategory::find($id);
        if( $addoncategory ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update Addon category';
            $this->data['data'] = $addoncategory;
            // 
            return view('admin.addon_category.create_edit', $this->data);
        } else {
            return redirect()->route('admin.addoncategory.index')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;
        
        //Conditions
        // 1. General Conditions
        request()->validate([
            'name' => 'required',
        ], [
            'name.required' => "Policy Document Master Name is required.",
        ]);
        
        
        if(!empty( $name ))
        {
            $policydocument = AddonCategory::find($id);
            if( $policydocument ) {

                $policydocument->name = $name;
                $policydocument->status = $status;
                $policydocument->save();

                return redirect()->route('admin.addoncategory.index')->with('success', 'Addon Category updated successfully');
            }else{
                return redirect()->route('admin.addoncategory.index')->with('error', 'Addon Category not updated');
            }
        }
        else{
            return redirect()->route('admin.addoncategory.index')->with('error', 'Addon Category not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
