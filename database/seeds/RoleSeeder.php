<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role =  Role::create([
            'name' => 'agent',
            'display_name' => 'agent',
            'description' => 'This will be one permission, that can not be assigned or modified.'
        ]);

        $permission = Permission::where('name', 'agent')->first();
        $role->attachPermission($permission);
    }
}
