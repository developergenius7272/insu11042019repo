<?php

namespace App\Http\Controllers;

use App\Models\PolicyDocumentMaster;
use Illuminate\Http\Request;

use Auth;

class AdminPolicyDocumentMasterController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('role:super-admin|sub-admin');
        $this->data['breadcrumbs'][] = 'Policy Documents'; 
        $this->data['breadcrumbs'][] = 'Policy Documents';
        $this->data['pageTitle'] = 'Policy Documents';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Policy Document Master';
        $this->data['policydocuments'] = PolicyDocumentMaster::all();

        return view('admin.policy_document_master.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new Policy Document Master';
        return view('admin.policy_document_master.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;

        //Conditions
        // 1. General Conditions
        
        request()->validate([
            'name' => 'required',
        ]);
        

        if(!empty( $name ))
        {
            $product = new PolicyDocumentMaster();
            $product->name = $name;
            $product->status = $status;
            $product->save();


            return redirect()->route('admin.policydocumentmaster.index')->with('success', 'Policy Document Master added successfully');

        }else{
            return redirect()->route('admin.policydocumentmaster.index')->with('error', 'Policy Document Master not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $policydocument = PolicyDocumentMaster::find($id);
        if( $policydocument ) {
            $this->data['formType'] = 'edit';
            $this->data['formTitle'] = 'Update Policy Document Master';
            $this->data['data'] = $policydocument;
            // 
            return view('admin.policy_document_master.create_edit', $this->data);
        } else {
            return redirect()->route('admin.policydocumentmaster.index')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;
        
        //Conditions
        // 1. General Conditions
        request()->validate([
            'name' => 'required',
        ], [
            'name.required' => "Policy Document Master Name is required.",
        ]);
        
        
        if(!empty( $name ))
        {
            $policydocument = PolicyDocumentMaster::find($id);
            if( $policydocument ) {

                $policydocument->name = $name;
                $policydocument->status = $status;
                $policydocument->save();

                return redirect()->route('admin.policydocumentmaster.index')->with('success', 'Policy Document Master updated successfully');
            }else{
                return redirect()->route('admin.policydocumentmaster.index')->with('error', 'Policy Document Master not updated');
            }
        }
        else{
            return redirect()->route('admin.policydocumentmaster.index')->with('error', 'Policy Document Master not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
