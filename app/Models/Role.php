<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use App\models\Permission;
use App\User;

use Auth;


class Role extends EntrustRole
{
     /**
     * Display a listing of the permissions belongs to role.
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Display a listing of the users having role.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Display a listing of the roles based on permissions associated with current user
     *
     * @return \Illuminate\Http\Response
     */
    public static function getRoles()
    {
        if ( Auth::user()->can(['admin', 'super-admin']) ){
            $roles = SELF::whereHas('permissions', function ($query) {
                $query->whereNotIn('name',['super-admin']);
            })->get();
        } else {
            $roles = SELF::whereHas('permissions', function ($query) {
                $query->whereNotIn('name', ['super-admin', 'admin']);
            })->get();
        }
        return $roles;
    }
}
