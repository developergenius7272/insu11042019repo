<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_make_id', 'name', 'description', 'order',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Get the post that owns the comment.
     */
    public function carmake()
    {
        //return $this->belongsTo('App\Models\CarMake');
        return $this->belongsTo('App\Models\CarMake', 'car_make_id', 'id');
    }

    public function carModelVariant()
    {
        return $this->hasMany('App\Models\CarModelVariant');
    }

    /**
     * Scope a query to only include active makes.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('car_models.status',1);    
    }
}
