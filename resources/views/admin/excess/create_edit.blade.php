@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    {{-- <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i> 
                Table 
            <span>> 
                Normal Tables
            </span>
        </h1>
    </div> --}}
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                    <!-- widget options:
                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                    data-widget-colorbutton="false"
                    data-widget-editbutton="false"
                    data-widget-togglebutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-collapsed="true"
                    data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Excess</h2>

                    </header>

                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    
                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
    
                            <form class="form-horizontal" id="formExcess" method="POST" @if($formType == 'edit') action="{{action('AdminExcessController@update', $data->id)}}" @else action="{{url('/admin/excess')}}" @endif>
                                 @csrf
                                 @if($formType == 'edit')
                                    <input name="_method" type="hidden" value="PATCH">
                                @endif
                                {{--  --}}
                                <fieldset>
                                    <legend>Partner</legend>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="insuranceProviderId">Insurance provider / Partner</label>
                                        <div class="col-md-10">
                                            <select class="form-control" id="insuranceProviderId" name="insuranceProviderId">
                                                <option value="">Select Partner</option>
                                                @if(isset($partners) && count($partners))
                                                    @foreach($partners as $partner)
                                                        <option value="{{$partner->id}}" 
                                                            @if(old('insuranceProviderId') == $partner->id) 
                                                                {{ 'selected' }} 
                                                            @elseif(isset($data) && $data->insurance_provider_id == $partner->id)
                                                                {{ 'selected' }} 
                                                            @endif
                                                            >{{strtoupper($partner->name)}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                        </div>
                                    </div>
                                </fieldset>
                                {{-- Car related --}}
                                <fieldset>
                                    <legend>Cars</legend>
                                    {{-- car body --}}

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Select Car Model Body Type</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                @if($formType == 'edit')
                                                    <select class="form-control" id="carBodyTypeId" name="carBodyTypeId">
                                                        <option value="">Select Car Type</option>
                                                        @if(isset($carBodies) && count($carBodies))
                                                            @foreach($carBodies as $body)
                                                                <option value="{{$body->id}}" 
                                                                    @if(old('carBodyTypeId') == $body->id) 
                                                                        {{ 'selected' }} 
                                                                    @elseif(isset($data) && $data->car_model_body_type_id == $body->id)
                                                                        {{ 'selected' }} 
                                                                    @endif

                                                                >{{strtoupper($body->name)}}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                                @else
                                                    <select class="form-control" id="carBodyTypeId" name="carBodyTypeId[]" multiple="multiple">
                                                        <!--option value="">Select Car Type</option-->
                                                        {{-- @if(isset($carBodies) && count($carBodies))
                                                            @foreach($carBodies as $body)
                                                                <option value="{{$body->id}}" @if(isset($data) && $data->car_model_body_type_id == $body->id) selected @endif>{{strtoupper($body->name)}}</option>
                                                            @endforeach
                                                        @endif --}}

                                                        @if(isset($carBodies) && count($carBodies))

                                                            @foreach($carBodies as $body)
                                                                @if($errors->any())
                                                                    @php ($is_record_found = 'no')

                                                                    @if(old('carBodyTypeId'))
                                                                        @foreach(old('carBodyTypeId') as $val)
                                                                            @if($val == $body->id) 
                                                                                @php ($is_record_found = 'yes')
                                                                                @break
                                                                            @endif
                                                                        @endforeach
                                                                    @endif

                                                                    <option value="{{$body->id}}" 
                                                                        @if ($is_record_found == 'yes')
                                                                            {{ 'selected' }} 
                                                                        @endif
                                                                        >{{strtoupper($body->name)}}
                                                                    </option>

                                                                @else
                                                                    <option value="{{$body->id}}" 
                                                                        @if(isset($data) && $data->car_model_body_type_id == $body->id)     {{ 'selected' }} 
                                                                        @endif
                                                                    >{{strtoupper($body->name)}}
                                                                </option>
                                                                @endif
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- car_model_min_value --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Car Model Min Value</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Car Model Min Value" name="car_model_min_value" id="car_model_min_value" type="number" min="1" value="{{ $errors->any() ? old('car_model_min_value') : $data->car_model_min_value ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- car_model_max_value --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Car Model Max Value</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Car Model Max Value" name="car_model_max_value" id="car_model_max_value" type="number" min="1" value="{{ $errors->any() ? old('car_model_max_value') : $data->car_model_max_value ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-2">Excess Amount</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-4">

                                                    <input class="form-control" placeholder="Excess Amount" name="excess_value" id="excess_value" type="number" min="1" value="{{ $errors->any() ? old('excess_value') : $data->excess_value ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                                </div>
                                                <div class="col-sm-1" style="width: 4%;">
                                                    <span>OR</span>
                                                </div>
                                                <div class="col-sm-4">

                                                    <input class="form-control" placeholder="Excess Description" name="excess_description" id="excess_description" type="text"value="{{ $errors->any() ? old('excess_description') : $data->excess_description ?? '' }}">
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                {{--  --}}
                                <fieldset>
                                    <legend>No. of Seats</legend>
                                    {{-- min_age --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Minimum Seat</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Minimum Seat" name="min_seat" id="min_seat" type="number" min="1" value="{{ $errors->any() ? old('min_seat') : $data->min_seat ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- max age --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Maximum Seat</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Maximum Seat" name="max_seat" id="max_seat" type="number" min="1" value="{{ $errors->any() ? old('max_seat') : $data->max_seat ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                {{--  --}}
                                <fieldset>
                                    <legend>Age Range (optional)</legend>
                                    {{-- min_age --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Minimum Age</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Minimum Age" name="min_age" id="min_age" type="number" min="1" value="{{ $errors->any() ? old('min_age') : $data->min_age ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- max age --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Maximum Age</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    
                                                    <input class="form-control" placeholder="Maximum Age" name="max_age" id="max_age" type="number" min="1" value="{{ $errors->any() ? old('max_age') : $data->max_age ?? '' }}">

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>


                                    {{--  --}}

                                <fieldset>
                                    <legend>Sub types / Status / Effective Date</legend>

                                    <div class="form-group">
                                        <label class="control-label col-md-2">Effective Date</label>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <input type="text" dbDate="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}" name="effective_date" value="{{ $errors->any() ? old('effective_date') : (isset($data->effective_date) ? $data->effective_date->format('d/m/Y H:i') : '') }}" placeholder="Effective Date" id="datetimepicker" class="form-control" autocomplete="off" readonly>

                                                    <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--  --}}
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Status</label>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    @if($formType == 'edit') 

                                                        <select name="status" class="form-control" >
                                                            @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                            @foreach($statusArry as $key => $value)
                                                                <option class="form-control" value="{{$key}}" 
                                                                    @if(old('status') == $key) 
                                                                        {{ 'selected' }} 
                                                                    @elseif($key == $data->status)
                                                                        {{ 'selected' }} 
                                                                    @endif
                                                                    >{{ $value }}
                                                                </option>
                                                            @endforeach

                                                        </select>
                                                    @else
                                                        <select name="status" class="form-control" >
                                                            <option  class="form-control" value="0"
                                                                @if(old('status') == 0) 
                                                                    {{ 'selected' }} 
                                                                @endif
                                                            >Inactive</option>

                                                            <option  class="form-control" value="1"
                                                                @if(old('status') == 1) 
                                                                    {{ 'selected' }} 
                                                                @endif
                                                            >Active</option>
                                                        </select>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                {{--  --}}
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{url('/admin/excess')}}" class="btn btn-default">Cancel</a>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
    
                            </form>
    
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            
        </div>

    </section>
</div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        // 
        $('#datetimepicker').datetimepicker({
            format:'d/m/Y H:i',
            //format:'d/m/Y',
            //timepicker:false,
            scrollMonth : false,
            scrollInput : false
        });

        $("#formExcess").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == true){
                //afterward
            }
            else{
                $(window).scrollTop(0);
                return false;
            }

        }));


    });
</script>

<script type="text/javascript">
    function textToDate(t){
           var ary= t.split(" ");
                var aryDate=ary[0].split("/");
                var aryTime=ary[1].split(":");
                var convertedDate = new Date(aryDate[2], 0, aryDate[0], 0, 0, 0, 0);
                //new Date(year, month, day, hours, minutes, seconds, milliseconds)
                convertedDate.setMonth(aryDate[1]>0?aryDate[1]-1:aryDate[1] ); 

                return convertedDate;
    }

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var insuranceProviderId  = $.trim($("#insuranceProviderId").val());
        var car_model_min_value  = $.trim($("#car_model_min_value").val());
        var car_model_max_value  = $.trim($("#car_model_max_value").val());
        var excess_value  = $.trim($("#excess_value").val());
        var excess_description  = $.trim($("#excess_description").val());
        var min_seat  = $.trim($("#min_seat").val());
        var max_seat  = $.trim($("#max_seat").val());        
        var min_age  = $.trim($("#min_age").val());
        var max_age  = $.trim($("#max_age").val());
        //var datetimepicker  = $.trim($("#datetimepicker").val());
        var t= $.trim($("#datetimepicker").val());  


        if (insuranceProviderId == ''){

            $("#insuranceProviderId").parent().find(".my-error").html('Please select Insurance Provider').show();
            $('#insuranceProviderId').closest('.form-group').addClass('has-error');
            is_valid = false;

        }

        @if($formType == 'add')
            var carBodyTypeId = $('#carBodyTypeId option:selected').index();
            if (carBodyTypeId < 0){
                
                $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
                $('#carBodyTypeId').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        @else
            var carBodyTypeId = $('#carBodyTypeId').prop('selectedIndex');

            if (carBodyTypeId == 0){
                
                $("#carBodyTypeId").parent().find(".my-error").html('Please select Car Model Body Type').show();
                $('#carBodyTypeId').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        @endif

        if (car_model_min_value.length > 0){
            
            if(!$.isNumeric( car_model_min_value ) || car_model_min_value < 1){
                $("#car_model_min_value").parent().find(".my-error").html('Please fill car model min value in numeric').show();
                $('#car_model_min_value').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (car_model_max_value.length > 0){

            if(!$.isNumeric( car_model_max_value ) || car_model_max_value < 1){
                $("#car_model_max_value").parent().find(".my-error").html('Please fill car model max value in numeric').show();
                $('#car_model_max_value').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if((!$.isNumeric(excess_value) || excess_value <= 0) && (excess_description == '') )
        {
                $("#excess_value").parent().find(".my-error").html('Please enter Excess Value or Excess Description').show();
                $('#excess_value').closest('.form-group').addClass('has-error');
                is_valid = false;
        }
        if (min_seat.length > 0){

            if(!$.isNumeric( min_seat ) || min_seat < 1){
                $("#min_seat").parent().find(".my-error").html('Please fill min age in numeric').show();
                $('#min_seat').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (max_seat.length > 0){

            if(!$.isNumeric( max_seat ) || max_seat < 1){
                $("#max_seat").parent().find(".my-error").html('Please fill max seat in numeric').show();
                $('#max_seat').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        if (min_age.length > 0){

            if(!$.isNumeric( min_age ) || min_age < 1){
                $("#min_age").parent().find(".my-error").html('Please fill min age in numeric').show();
                $('#min_age').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

        if (max_age.length > 0){

            if(!$.isNumeric( max_age ) || max_age < 1){
                $("#max_age").parent().find(".my-error").html('Please fill max age in numeric').show();
                $('#max_age').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }
        //effective date checking
        if($.trim(t).length>0)
        {
            @if($formType == 'add')
 
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
                if(currentDateTime.getTime() > effectiveDate.getTime())
                { 
                    $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                    is_valid = false;
                }

            @else

                var dbDateText = $("#datetimepicker").attr('dbdate');
                var dbDate  = textToDate(dbDateText)
                var effectiveDate  = textToDate(t)
                var currentDateTime = new Date(); 
                currentDateTime.setHours(0); currentDateTime.setMinutes(0); currentDateTime.setSeconds(0); currentDateTime.setMilliseconds(0); 
                 
 
                if(dbDate.getTime() != effectiveDate.getTime()){
                    if(currentDateTime.getTime() > effectiveDate.getTime())
                    {
                        $("#datetimepicker").parent().find(".my-error").html('Effective date not less than current date').show();
                        is_valid = false;
                    }
                }


            @endif
            
        }
        else
        {
            //alert("Please Enter valid Effective Date");
            $("#datetimepicker").parent().find(".my-error").html('Please enter effective date').show();
            $('#datetimepicker').closest('.form-group').addClass('has-error');
            is_valid = false;
        }




        return is_valid;

    }

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>

@endsection
