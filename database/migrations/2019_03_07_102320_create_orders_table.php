<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_providers_id');
            $table->integer('users_id');
            $table->json('benefit_options');
            $table->decimal('total', 8, 2);
            $table->decimal('premium_value', 8, 2);
            $table->decimal('vat_premium', 8, 2);
            $table->decimal('policy_fees', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
