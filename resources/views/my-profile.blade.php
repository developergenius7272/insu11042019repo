{{-- @extends('layouts.master') --}}
@extends('dashboard')
@section('account-panel')

          <div class="col-md-9"> 
            <!-- Account Main Panel Start -->
            <div class="account-main-panel">
              <div class="account-main-heading">
                <h2><i class="fa fa-list-ul"></i> My Profile</h2>
              </div>
              <div class="account-main-box">
                <div class="my-policy-table-part"> 
                  <!-- Policy Table Start -->
                  <div class="my-policy-table-heading">
                    <h3>My Profile</h3>
                  </div>



                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="HomeController-horizontal" id="formUser" method="POST" @if($data->hasRole('user')) u action="{{route('user-update-profile')}}" @else a action="{{route('agent-update-profile')}}" @endif enctype="multipart/form-data">
                        {{-- <form class="form-horizontal" id="formUser" method="POST" @if($data->hasRole('user')) action="{{action('AdminUserController@update', $data->id)}}" @else action="{{url('/admin/users')}}" @endif enctype="multipart/form-data"> --}}
                             @csrf
                             {{-- <input name="_method" type="hidden" value="PATCH"> --}}
                            {{--  --}}
                            <fieldset>
                                <legend>User</legend>
                                {{-- user name --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                            <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- email --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Email" name="email" id="email" type="text" value="{{ $errors->any() ? old('email') : $data->email ?? '' }}" readonly="">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Mobile --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Mobile</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Mobile" name="mobile" id="mobile" type="number" min="1" value="{{ $errors->any() ? old('mobile') : $data->mobile ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Address --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Address</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">


                                                <textarea class="form-control" placeholder="Textarea" name="address" id="address" rows="3">{{ $errors->any() ? old('address') : $data->address ?? '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Gender --}}
                                <!--legend>Gender</legend-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label lbl">Gender</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender'))
                                                        @if(old('gender') == 1)
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 1)
                                                            {{ 'checked' }}
                                                    @endif
                                                value="1">Male
                                            </label>
                                            <label>
                                                <input type="radio" name="gender"
                                                    @if(old('gender'))
                                                        @if(old('gender') == 2) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 2)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="2">Female
                                            </label>
                                            <label>
                                                <input type="radio" name="gender" 
                                                    @if(old('gender') )
                                                        @if(old('gender') == 3) 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->gender) && $data->gender == 3)
                                                            {{ 'checked' }} 
                                                    @endif
                                                value="3">Other  
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                {{-- Marital Status --}}
                                <!--legend>Marital Status</legend-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label lbl">Marital Status</label>
                                    <div class="col-md-10">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Married") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Married")
                                                        {{ 'checked' }} 
                                                    @endif

                                                value="Married">Married
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Unmarried") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Unmarried")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Unmarried">Unmarried
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus"
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Divorced") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Divorced")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Divorced">Divorced
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Widow") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Widow")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Widow">Widow
                                            </label>
                                            <label>
                                                <input type="radio" name="maritalstatus" 
                                                    @if(old('maritalstatus'))
                                                        @if(old('maritalstatus') == "Single") 
                                                            {{ 'checked' }} 
                                                        @endif
                                                    @elseif(isset($data->maritalstatus) && $data->maritalstatus == "Single")
                                                        {{ 'checked' }} 
                                                    @endif
                                                value="Single">Single
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Date Of Birth</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input type="text" name="dob" value="{{ $errors->any() ? old('dob') : (isset($data->dob) ? $data->dob->format('d/m/Y') : '') }}" placeholder="Date of birth" class="form-control datetimepicker" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> Passport Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Passport Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="Passport Number" name="passport_number" id="passport_number" type="text" value="{{ $errors->any() ? old('passport_number') : $data->passport_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Expiry Date</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="passport_expiry_date" value="{{ $errors->any() ? old('passport_expiry_date') : $data->passport_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> License Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">License Number</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="License Number" name="license_number" id="license_number" type="text" value="{{ $errors->any() ? old('license_number') : $data->license_number ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Expiry Date</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input type="text" name="license_expiry_date" value="{{ $errors->any() ? old('license_expiry_date') : $data->license_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> UAE ID Detail</legend>
                                {{-- min_age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">UAE ID</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <input class="form-control" placeholder="UAE ID" name="uae_id" id="uae_id" type="text" value="{{ $errors->any() ? old('uae_id') : $data->uae_id ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- max age --}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl">Expiry Date</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="text" name="uae_id_expiry_date" value="{{ $errors->any() ? old('uae_id_expiry_date') : $data->uae_id_expiry_date ?? '' }}" placeholder="Expiry Date" class="form-control datetimepicker" class="form-control" autocomplete="off" readonly>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            {{--  --}}
                            <fieldset>
                                <legend> Other Details</legend>
                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl ">Image</label>

                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <input type="file" class="btn btn-default" id="image" name="image">
                                                {{-- <p class="help-block">
                                                    some help text here.
                                                </p> --}}

                                                {{-- <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button> --}}

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 lbl"></label>
                                
                                    <div class="col-md-10">
                                        @if($data->has_logo == 1) 
                                            <img id="imgUsers" src='{{ asset('uploads/users').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">

                                        <a href="@if($data->hasRole('user')) {{route('user-dashboard')}} @else {{route('agent-dashboard')}} @endif" class="btn btn-default">Cancel</a>

                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ 'Update'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->




                </div>
              </div>
            </div>
            <!-- Account Main Panel End --> 
          </div>
@endsection

@section('css')

<style type="text/css">
    .lbl{
      float: left;
    }
</style>
@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->


<script type="text/javascript">
    $(document).ready(function() {
        
        // 
        $('.datetimepicker').datetimepicker({
            format:'Y-m-d',
            format:'d-m-Y',
            format:'d/m/Y',
            timepicker:false,
            scrollMonth : false,
            scrollInput : false,
            closeOnDateSelect : true,
        });
      });


</script>
<script type="text/javascript">

  $( function() {

    $("#formUser").on('submit',(function(e) {

        var is_valid =  validForm();
        
        if(is_valid == false){
            $(window).scrollTop(0);
            return false;
        }

    }));

    function validForm(){
        
        var is_valid = true;

        var name  = $.trim($("#name").val());
        if (name == ''){

            $("#name").parent().find(".my-error").html('Please enter name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;

        }  
        return is_valid;
    }

  });

</script>
@endsection
