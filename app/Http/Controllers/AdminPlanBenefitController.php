<?php

namespace App\Http\Controllers;
//
use App\Models\BenefitMaster;
use App\Models\InsuranceProvider;
use App\Models\PlanBenefit;
use App\Models\PlanBenefitMultiple;
use Illuminate\Support\Facades\DB;
use App\Models\PlanBenefitsPartner;


use Carbon\Carbon;
use Log;

use Illuminate\Http\Request;
use Auth;

class AdminPlanBenefitController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Benefit Master'; 
        $this->data['breadcrumbs'][] = 'Benefit'; 
        $this->data['pageTitle'] = 'Benefit Master';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Plan Benefit';
        //$this->data['benefitMasters'] = PlanBenefit::all();

        $planBenefits = PlanBenefitsPartner::select('plan_benefits_partners.id','plan_benefits_partners.insurance_providers_id','plan_benefits_partners.status')
                ->addSelect('insurance_providers.name as partner_name')
                ->addSelect('plan_benefits_partners.year')
                ->addSelect('plan_benefits_partners.effective_date')
                ->addSelect('plan_benefits_partners.plan_type')
                ->addSelect('plan_benefits_partners.sub_type')

                ->leftJoin('insurance_providers','insurance_providers.id', 'plan_benefits_partners.insurance_providers_id')
                //->distinct('plan_benefits.insurance_providers_id')
                ->groupBy('plan_benefits_partners.insurance_providers_id','plan_benefits_partners.year','plan_benefits_partners.effective_date','plan_benefits_partners.plan_type','plan_benefits_partners.sub_type')
                ->get();
                //->get()->toArray();
        $this->data['planBenefits'] = $planBenefits;

        return view('admin.plan_benefit.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new Benefit Plan';
        $copyof = $request->copyof;
        $current_year = Carbon::now()->year;
        $this->data['current_year'] = $current_year;
        // 
        $this->data['partners'] = InsuranceProvider::active()->orderBy('name', 'asc')->get();
        // 
        $this->data['benefitMasters'] = BenefitMaster::active()->orderBy('name', 'asc')->get();
        // 
        if(isset($copyof)){
            return $this->editCopyOf($copyof);
        }else{
            return view('admin.plan_benefit.create', $this->data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();
        
        $benefitMasters = BenefitMaster::active()->get();

        $insurance_providers_id = $request->insurance_providers_id;
        $year = $request->year;
        $effective_date = $request->effective_date;
        $plan_type = $request->plan_type;
        $status = $request->status;
        $sub_type = $request->sub_type;
        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }


        //Conditions
        // 1. General Conditions
        request()->validate([
            'insurance_providers_id' => 'required',
            'year' => 'required',
            'effective_date' => 'required',            
            'plan_type' => 'required'

        ], [
            'insurance_providers_id.required' => "Insurance Provider is required",
            'year.required' => "Please Select Year.",
            'effective_date.required' => "Please fill Effective Date.",
            'plan_type.required' => "Please Select Plan."
        ]);

        // 2. Effective date same as Year
        //$effective_year = Carbon::createFromFormat('Y-m-d h:m:s', $effective_date)->year; //wrong output
        $effective_year = Carbon::parse($effective_date)->year;

        if($year != $effective_year){
              $request->validate($request, [
                'effective_year' => 'required',
            ],[
                // 'effective_year.required' => "Year and Effective year must be of same year",
            ]);
        }

        // 3. Effective date should always be greater than the last effective date chosen for "partner, year and Comprehensive plan / TPL plan" combination.
        $planBenefitsExist = PlanBenefitsPartner::where('insurance_providers_id',$insurance_providers_id)
                                ->where('year',$year)
                                ->where('plan_type',$plan_type)
                                ->where('sub_type',$sub_type)
                                ->first();
        if( $planBenefitsExist ) {

            $getEffectiveDate = $planBenefitsExist->effective_date;

            if($getEffectiveDate > $effective_date){
                $this->validate($request, [
                    'effective_date_greater' => 'required',
                ],[
                    'effective_date_greater.required' => "Effective date should be greater than previous plan",
                ]);
            }
        }

        //4. check if benefits has selected or not
        $benefit_idArr = array();
        foreach($benefitMasters as $benefitMaster){

            //4-a.Condition to check (part-a)-> if benefits has selected or not
            $benefit_idArr[] = $request->input('benefit_id_'.$benefitMaster->id);

            //5.Condition to check -> 
            $benefit_id_last = end($benefit_idArr);

            if(!empty($benefit_id_last)){

                $included = $request->input('included_'.$benefitMaster->id);

                //if Not Included
                if($included == 1){

                    //5.Condition to check -> if perAmount blank and amount fill
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($single_or_multiple !=0 ){
                        $co = count($amount);
                        for ($k=0; $k < $co; $k++) {

                            if(!empty($amount[$k])){
                                //echo "no empty";die;
                                
                                //$perAmount = $request->input('perAmount_'.$benefitMaster->id);
                                
                                if(empty($perAmount[0]) || $perAmount[0] == 0){

                                    $this->validate($request, [
                                        'perAmount_not_blank' => 'required',
                                    ],[
                                        'perAmount_not_blank.required' => "Please select Percentage or Amount on ".$benefitMaster->name,
                                    ]);

                                }
                                
                            }
                            //5.Condition to check -> if perAmount fill and amount blank
                            if(empty($amount[$k])){

                                //$perAmount = $request->input('perAmount_'.$benefitMaster->id);
                                
                                //if(!empty($perAmount)){
                                if($perAmount[0] > 0 || $perAmount[0] == 0){

                                    $this->validate($request, [
                                        'amount_not_blank' => 'required',
                                    ],[
                                        'amount_not_blank.required' => "Please fill amount if Percentage or Amount selected on ".$benefitMaster->name,
                                    ]);

                                }
                            }

                        }
                    }


                }
            }

        }
        
        $benefit_idArr = array_filter($benefit_idArr);

        if(empty($benefit_idArr)){
            $this->validate($request, [
                'benefit_id_not_blank' => 'required',
            ],[
                'benefit_id_not_blank.required' => "Please select atleast 1 benefit",
            ]);
        }

        //Insertion
        if(!empty( $insurance_providers_id ))
        {
            $provider_details = new PlanBenefitsPartner();
            foreach($benefitMasters as $benefitMaster){

                $benefit_id = $request->input('benefit_id_'.$benefitMaster->id);
                //$benefit_masters_id = $request->input('benefit_masters_id_'.$benefitMaster->id);
                $included = $request->input('included_'.$benefitMaster->id);
                $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                $amount = $request->input('amount_'.$benefitMaster->id);
                $feature_description = $request->input('feature_description_'.$benefitMaster->id);
                $description = $request->input('description_'.$benefitMaster->id);
                $required = $request->input('required_'.$benefitMaster->id);
                
                $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                if ($benefit_id == $benefitMaster->id){
                    
                    $body = new PlanBenefit();
                    
                    $provider_details->insurance_providers_id = $insurance_providers_id;
                    $provider_details->year = $year;
                    $provider_details->effective_date = $effective_date;
                    $provider_details->plan_type = $plan_type;
                    $provider_details->sub_type = $sub_type;
                    $provider_details->status = $status;
                    $provider_details->created_by = $adminID;
                    $provider_details->modified_by = $adminID;

                    $body->benefit_masters_id = $benefitMaster->id;
                    $body->included = $included;
                    $body->single_or_multiple = $single_or_multiple;
                    $body->description = $description;

                    //if Not include
                    if($included == 1){
                        //if single
                        if($single_or_multiple == 1){

                            $body->percentage_or_amount = $perAmount[0];
                            $body->amount = $amount[0];
                            $body->required = $required[0];

                            $provider_details->save();
                            $body->plan_benefits_partners_id = $provider_details->id;
                            $body->save();
                         
                            $pb_id = $body->id;
                        }
                        //if multiple
                        elseif($single_or_multiple == 2){
                            $provider_details->save();
                            $body->plan_benefits_partners_id = $provider_details->id;
                            $body->save();
                            $pb_id = $body->id;

                            //if Not include
                            if($included == 1){
                                
                                $co = count($amount);
                                for ($k=0; $k < $co; $k++) {
                                    
                                    $pbMultiple = new PlanBenefitMultiple();
                                    $pbMultiple->plan_benefits_id = $pb_id;
                                    $pbMultiple->percentage_or_amount = $perAmount[0];
                                    $pbMultiple->amount = $amount[$k];
                                    $pbMultiple->feature_description = $feature_description[$k];
                                    $pbMultiple->save();

                                }
                            }
                        }
                        //if N/A
                        else{
                            $provider_details->save();
                            $body->plan_benefits_partners_id = $provider_details->id;
                            $body->save();
                            
                        }
                    }
                    elseif($included == 2){
                        $provider_details->save();
                        $body->plan_benefits_partners_id = $provider_details->id;
                        $body->save();
                       
                    }


                }

            }
                return redirect('/admin/planbenefits')->with('success', 'Plan Benefit added successfully');
        }
        else{
            return redirect('/admin/planbenefits')->with('error', 'Plan Benefit not added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit Plan Benefit';
        //$this->data['data'] = BenefitMaster::find($id);

        $current_year = date('Y');
        $this->data['current_year'] = $current_year;
        
        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;

        //$this->data['benefitMasters'] = BenefitMaster::all();
        $this->data['benefitMasters'] = BenefitMaster::orderBy('name')->get();

        //$planBenefitById = PlanBenefit::find($id);

        $planBenefits = PlanBenefitsPartner::select('plan_benefits_partners.insurance_providers_id','plan_benefits_partners.id','insurance_providers.name as partner_name','plan_benefits_partners.year','plan_benefits_partners.effective_date','plan_benefits_partners.plan_type','plan_benefits_partners.sub_type','plan_benefits_partners.status')

                ->leftJoin('insurance_providers','insurance_providers.id', 'plan_benefits_partners.insurance_providers_id')
                ->distinct('plan_benefits_partners.insurance_providers_id')
                ->where('plan_benefits_partners.id',$id)
                ->first();

        $this->data['data'] = $planBenefits;

        if( $this->data['data'] ) {

            $planBenefitsAll = PlanBenefitsPartner::select('plan_benefits_partners.*')
            ->addSelect('plan_benefits.*', 
                            DB::raw("case plan_benefits.included
                                    when 2 then 'yes'
                                    when 1 then 'no'
                                    else 'no' end as 'is_include',

                                    CASE plan_benefits.single_or_multiple
                                    when 2 then 
                                    (select group_concat(IFNULL(plan_benefits_multiple.percentage_or_amount, 0), '|#|', IFNULL(plan_benefits_multiple.amount, 0), '|#|',IFNULL(plan_benefits_multiple.feature_description, '')) as tect from plan_benefits_multiple 
                                        where plan_benefits_multiple.plan_benefits_id =  plan_benefits.id)
                                    else '' end as multiple_amount_description")
                            )
                    ->addSelect('insurance_providers.name as partner_name')
                    ->addSelect('benefit_masters.name as benefit_master_name')
                    ->leftJoin('insurance_providers','insurance_providers.id',
                     'plan_benefits_partners.insurance_providers_id')
                    ->leftJoin('plan_benefits','plan_benefits.plan_benefits_partners_id','plan_benefits_partners.id' )
                    ->leftJoin('benefit_masters', 'plan_benefits.benefit_masters_id', 'benefit_masters.id')
                    ->where('insurance_providers.id',$planBenefits->insurance_providers_id)
                    ->where('year',$planBenefits->year)
                    ->where('plan_type',$planBenefits->plan_type)
                    ->where('effective_date',$planBenefits->effective_date)
                    ->where('sub_type',$planBenefits->sub_type)
                    ->get();

            $this->data['planBenefitsAll'] = $planBenefitsAll;

            return view('admin.plan_benefit.edit',$this->data);
        } else {
            return redirect('/admin/planbenefits')->with('error', 'No data found');
        }
    }

    public function editCopyOf($id)
    {
        $this->data['formType'] = 'copyof';
        $this->data['formTitle'] = 'Edit Plan Benefit';
        //$this->data['data'] = BenefitMaster::find($id);

        $current_year = date('Y');
        $this->data['current_year'] = $current_year;
        
        $partners = InsuranceProvider::all();
        $this->data['partners'] = $partners;

        //$this->data['benefitMasters'] = BenefitMaster::all();
        $this->data['benefitMasters'] = BenefitMaster::where('status',1)->orderBy('name')->get();

        //$planBenefitById = PlanBenefit::find($id);

        $planBenefits = PlanBenefitsPartner::select('plan_benefits_partners.insurance_providers_id','plan_benefits_partners.id','insurance_providers.name as partner_name','plan_benefits_partners.year','plan_benefits_partners.effective_date','plan_benefits_partners.plan_type','plan_benefits_partners.sub_type','plan_benefits_partners.status')

                ->leftJoin('insurance_providers','insurance_providers.id', 'plan_benefits_partners.insurance_providers_id')
                ->distinct('plan_benefits_partners.insurance_providers_id')
                ->where('plan_benefits_partners.id',$id)
                ->first();

        $this->data['data'] = $planBenefits;

        if( $this->data['data'] ) {

            $planBenefitsAll = PlanBenefitsPartner::select('plan_benefits_partners.*')
            ->addSelect('plan_benefits.*', 
                            DB::raw("case plan_benefits.included
                                    when 2 then 'yes'
                                    when 1 then 'no'
                                    else 'no' end as 'is_include',

                                    CASE plan_benefits.single_or_multiple
                                    when 2 then 
                                    (select group_concat(IFNULL(plan_benefits_multiple.percentage_or_amount, 0), '|#|', IFNULL(plan_benefits_multiple.amount, 0), '|#|',IFNULL(plan_benefits_multiple.feature_description, '')) as tect from plan_benefits_multiple 
                                        where plan_benefits_multiple.plan_benefits_id =  plan_benefits.id)
                                    else '' end as multiple_amount_description")
                            )
                    ->addSelect('insurance_providers.name as partner_name')
                    ->addSelect('benefit_masters.name as benefit_master_name')
                    ->leftJoin('insurance_providers','insurance_providers.id',
                     'plan_benefits_partners.insurance_providers_id')
                    ->leftJoin('plan_benefits','plan_benefits.plan_benefits_partners_id','plan_benefits_partners.id' )
                    ->leftJoin('benefit_masters', 'plan_benefits.benefit_masters_id', 'benefit_masters.id')
                    ->where('insurance_providers.id',$planBenefits->insurance_providers_id)
                    ->where('year',$planBenefits->year)
                    ->where('plan_type',$planBenefits->plan_type)
                    ->where('effective_date',$planBenefits->effective_date)
                    ->where('sub_type',$planBenefits->sub_type)
                    ->get();

            $this->data['planBenefitsAll'] = $planBenefitsAll;

            return view('admin.plan_benefit.edit',$this->data);
        } else {
            return redirect('/admin/planbenefits')->with('error', 'No data found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "ppkkkk23";die;
        $benefitMasters = BenefitMaster::all();

        $pb_id_request = $request->pb_id;

        $insurance_providers_id = $request->insurance_providers_id;
        $year = $request->year;
        $effective_date = $request->effective_date;
        $plan_type = $request->plan_type;
        $status = $request->status;
        $sub_type = $request->sub_type;

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        //Conditions
        // 1. General Conditions
        request()->validate([
            'insurance_providers_id' => 'required',
            'year' => 'required',
            'effective_date' => 'required',            
            'plan_type' => 'required',

        ], [
            'insurance_providers_id.required' => "Insurance Provider is required",
            'year.required' => "Please Select Year.",
            'effective_date.required' => "Please fill Effective Date.",
            'plan_type.required' => "Please Select Plan.",
        ]);

        // 2. Effective date same as Year
        //$effective_year = Carbon::createFromFormat('Y-m-d h:m:s', $effective_date)->year; //wrong output
        $effective_year = Carbon::parse($effective_date)->year;

        if($year != $effective_year){
              $this->validate($request, [
                'effective_year' => 'required',
            ],[
                'effective_year.required' => "Year and Effective year must be of same year",
            ]);
        }

        //4. check if benefits has selected or not
        $benefit_idArr = array();
        foreach($benefitMasters as $benefitMaster){

            //4-a.Condition to check (part-a)-> if benefits has selected or not
            $benefit_idArr[] = $request->input('benefit_id_'.$benefitMaster->id);

            //5.Condition to check -> 
            $benefit_id_last = end($benefit_idArr);

            if(!empty($benefit_id_last)){

                $included = $request->input('included_'.$benefitMaster->id);

                //if Not Included
                if($included == 1){

                    //5.Condition to check -> if perAmount blank and amount fill
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($single_or_multiple !=0 ){

                        $co = count($amount);
                        for ($k=0; $k < $co; $k++) {

                            if(!empty($amount[$k])){
                                
                                if(empty($perAmount[$k]) || $perAmount[$k] == 0){

                                    $this->validate($request, [
                                        'perAmount_not_blank' => 'required',
                                    ],[
                                        'perAmount_not_blank.required' => "Please select Percentage or Amount on ".$benefitMaster->name,
                                    ]);

                                }
                                
                            }
                            //5.Condition to check -> if perAmount fill and amount blank
                            if(empty($amount[$k])){

                                if($perAmount[$k] > 0 || $perAmount[$k] == 0){

                                    $this->validate($request, [
                                        'amount_not_blank' => 'required',
                                    ],[
                                        'amount_not_blank.required' => "Please fill amount if Percentage or Amount selected on ".$benefitMaster->name,
                                    ]);

                                }
                            }

                        }                        
                    }

                }
            }

        }  

        //4-b.Condition to check (part-4-a)-> if benefits has selected or not
        $benefit_idArr = array_filter($benefit_idArr);
        if(empty($benefit_idArr)){
            $this->validate($request, [
                'benefit_id_not_blank' => 'required',
            ],[
                'benefit_id_not_blank.required' => "Please select atleast 1 benefit",
            ]);
        }

        //updation
        if(!empty( $insurance_providers_id ))
        {
            $PlanBenefit = PlanBenefitsPartner::where('id',$pb_id_request)->get();

            if( $PlanBenefit ) {

                foreach($benefitMasters as $benefitMaster){

                    $benefit_id = $request->input('benefit_id_'.$benefitMaster->id);
                    $included = $request->input('included_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $feature_description = $request->input('feature_description_'.$benefitMaster->id);
                    $description = $request->input('description_'.$benefitMaster->id);
                    
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($benefit_id == $benefitMaster->id){

                        $benefitOfPlanExist = PlanBenefit::where('insurance_providers_id',$insurance_providers_id)
                                ->where('year',$year)
                                ->where('plan_type',$plan_type)
                                ->where('effective_date',$effective_date)
                                ->where('benefit_masters_id',$benefit_id)
                                ->first();

                        if($benefitOfPlanExist){

                            $planBenefitUpdate = PlanBenefit::find($benefitOfPlanExist->id);

                            if($planBenefitUpdate){
                                
                                //1) all time update on the parent record
                                // $planBenefitUpdate->year = $year;
                                // $planBenefitUpdate->effective_date = $effective_date;
                                // $planBenefitUpdate->plan_type = $plan_type;
                                // $planBenefitUpdate->sub_type = $sub_type;
                                $planBenefitUpdate->status = $status;
                                $planBenefitUpdate->benefit_masters_id = $benefitMaster->id;
                                $planBenefitUpdate->included = $included;
                                $planBenefitUpdate->single_or_multiple = $single_or_multiple;
                                $planBenefitUpdate->description = $description;
                                // $planBenefitUpdate->percentage_or_amount = $perAmount[0];
                                // $planBenefitUpdate->amount = $amount[0];

                                //if Not include
                                if($included == 1){
                                    //if single
                                    if($single_or_multiple == 1){

                                        $planBenefitUpdate->percentage_or_amount = $perAmount[0];
                                        $planBenefitUpdate->amount = $amount[0];

                                        $planBenefitUpdate->save();
                                        //$pb_id = $body->id;

                                        //2)delete all record of multiple table of current record
                                        $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                    }
                                    //if multiple
                                    elseif($single_or_multiple == 2){

                                        $planBenefitUpdate->percentage_or_amount = 0;
                                        $planBenefitUpdate->amount = 0;
                                        $planBenefitUpdate->save();
                                        //$pb_id = $body->id;

                                        //2)delete all record of multiple table of current record
                                        $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();

                                        //if Not include
                                        if($included == 1){
                                            
                                            $co = count($perAmount);
                                            for ($k=0; $k < $co; $k++) {
                                                
                                                $pbMultiple = new PlanBenefitMultiple();
                                                //$pbMultiple->plan_benefits_id = $pb_id;
                                                $pbMultiple->plan_benefits_id = $benefitOfPlanExist->id;
                                                $pbMultiple->percentage_or_amount = $perAmount[$k];
                                                $pbMultiple->amount = $amount[$k];
                                                $pbMultiple->feature_description = $feature_description[$k];
                                                $pbMultiple->save();

                                            }
                                        }
                                    }
                                }
                                //if included
                                elseif($included == 2){

                                    $planBenefitUpdate->single_or_multiple = 0;
                                    $planBenefitUpdate->percentage_or_amount = 0;
                                    $planBenefitUpdate->amount = 0;
                                    $planBenefitUpdate->description = $description;
                                
                                    $planBenefitUpdate->save();

                                    //2)delete all record of multiple table of current record
                                    $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                }
                                //if N/A
                                //else{
                                //delete case afterward
                                elseif($included == 0){

                                    //2)delete all record of multiple table of current record
                                    $res=PlanBenefit::where('id',$benefitOfPlanExist->id)->delete();
                                    $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                }


                            }


                        }
                        else{
                            //new row as it is like create 
                            $planBenefitInsert = new PlanBenefit();

                            $planBenefitInsert->insurance_providers_id = $insurance_providers_id;
                            $planBenefitInsert->year = $year;
                            $planBenefitInsert->effective_date = $effective_date;
                            $planBenefitInsert->plan_type = $plan_type;
                            $planBenefitInsert->sub_type = $sub_type;
                            $planBenefitInsert->status = $status;
                            $planBenefitInsert->benefit_masters_id = $benefitMaster->id;
                            $planBenefitInsert->included = $included;
                            $planBenefitInsert->single_or_multiple = $single_or_multiple;
                            $planBenefitInsert->description = $description;

                            //if Not include
                            if($included == 1){
                                //if single
                                if($single_or_multiple == 1){

                                    $planBenefitInsert->percentage_or_amount = $perAmount[0];
                                    $planBenefitInsert->amount = $amount[0];

                                    $planBenefitInsert->save();
                                    $pb_id = $planBenefitInsert->id;
                                }
                                //if multiple
                                elseif($single_or_multiple == 2){

                                    $planBenefitInsert->save();
                                    $pb_id = $planBenefitInsert->id;

                                    //if Not include
                                    if($included == 1){
                                        
                                        $co = count($perAmount);
                                        for ($k=0; $k < $co; $k++) {
                                            
                                            $pbMultiple = new PlanBenefitMultiple();
                                            $pbMultiple->plan_benefits_id = $pb_id;
                                            $pbMultiple->percentage_or_amount = $perAmount[$k];
                                            $pbMultiple->amount = $amount[$k];
                                            $pbMultiple->feature_description = $feature_description[$k];
                                            $pbMultiple->save();

                                        }
                                    }
                                }
                                //if N/A
                                else{
                                    $planBenefitInsert->save();
                                }
                            }
                            elseif($included == 2){
                                $planBenefitInsert->save();
                            }
                        }

                    }

                }

                return redirect('/admin/planbenefits')->with('success', 'Plan Benefits updated successfully');
            }

        }
        else {
            return redirect('/admin/planbenefits')->with('error', 'Plan Benefits not updated');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addFormPlanBenefit(Request $request) {
        //echo "pp22";die;
        $response = array();
        $check_error = 0;

        //$benefitMasters = BenefitMaster::all();
        $benefitMasters = BenefitMaster::where('status',1)->get();

        $insurance_providers_id = $request->insurance_providers_id;
        $year = $request->year;
        $effective_date = $request->effective_date;
        $plan_type = $request->plan_type;
        $status = $request->status;
        $sub_type = $request->sub_type;
        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        if(empty($insurance_providers_id)){
            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please Select Insurance Provider" );
        }
        else if(empty($year)){
            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please Select Year" );
        }
        else if(empty($effective_date)){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please fill effective date" );
        }
    
        // 2. Effective date same as Year
        //$effective_year = Carbon::createFromFormat('Y-m-d h:m:s', $effective_date)->year; //wrong output
        $effective_year = Carbon::parse($effective_date)->year;

        if($year != $effective_year){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Year and Effective year must be of same year" );
            
        }

        // 3. Effective date should always be greater than the last effective date chosen for "partner, year and Comprehensive plan / TPL plan" combination.
        $planBenefitsExist = PlanBenefitsPartner::where('insurance_providers_id',$insurance_providers_id)
                                ->where('year',$year)
                                ->where('plan_type',$plan_type)
                                ->where('sub_type',$sub_type)
                                ->first();
        if( $planBenefitsExist ) {

            $getEffectiveDate = $planBenefitsExist->effective_date;
            
            if($getEffectiveDate > $effective_date || $getEffectiveDate == $effective_date ){

                $check_error = 1;
                $response['status'] =  0;
                $response['msgs'][] = array("msg" => "Effective date should be greater than previous plan" );
                
            }
        }

        //5.Effective date should be greater than current date
        $currentDate = Carbon::now();
        if($currentDate > $effective_date){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Effective date should be greater than current date" );
        }

        //4. check if benefits has selected or not
        $benefit_idArr = array();
        foreach($benefitMasters as $benefitMaster){

            //4-a.Condition to check (part-a)-> if benefits has selected or not
            $benefit_idArr[] = $request->input('benefit_id_'.$benefitMaster->id);

            //5.Condition to check -> 
            $benefit_id_last = end($benefit_idArr);

            if(!empty($benefit_id_last)){

                $included = $request->input('included_'.$benefitMaster->id);
 
                //if Not Included
                if($included == 1){

                    //5.Condition to check -> if perAmount blank and amount fill
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($single_or_multiple !=0 ){
                        $co = count($amount);
                        
                        for ($k=0; $k < $co; $k++) {

                            if(!empty($amount[$k])){
                                //echo "no empty";die;
                                
                                //$perAmount = $request->input('perAmount_'.$benefitMaster->id);
                                
                                if(empty($perAmount[0]) || $perAmount[0] == 0){

                                    $check_error = 1;
                                    $response['status'] =  0;
                                    $response['msgs'][] = array("msg" => "Please select Percentage or Amount on ". $benefitMaster->name);

                                }
                                
                            }
                            //5.Condition to check -> if perAmount fill and amount blank
                            if(empty($amount[$k])){

                                //$perAmount = $request->input('perAmount_'.$benefitMaster->id);
                                
                                //if(!empty($perAmount)){
                                if($perAmount[0] > 0 || $perAmount[0] == 0){
                                    // echo "pp";
                                    // echo $perAmount[$k];die;
                                    $check_error = 1;
                                    $response['status'] =  0;
                                    $response['msgs'][] = array("msg" => "Please fill amount if Percentage or Amount selected on ". $benefitMaster->name );

                                }
                            }

                        }                        
                    }



                }
            }

        }

        $benefit_idArr = array_filter($benefit_idArr);
        
        if(empty($benefit_idArr)){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please select atleast 1 benefit" );
            
        }
//echo "<pre>";print_r($response);die;
        /*
        //Insertion
        if($check_error == 0){
            if(!empty( $insurance_providers_id ))
            {
                foreach($benefitMasters as $benefitMaster){

                    $benefit_id = $request->input('benefit_id_'.$benefitMaster->id);
                    //$benefit_masters_id = $request->input('benefit_masters_id_'.$benefitMaster->id);
                    $included = $request->input('included_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $feature_description = $request->input('feature_description_'.$benefitMaster->id);
                    $description = $request->input('description_'.$benefitMaster->id);
                    
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($benefit_id == $benefitMaster->id){
                        
                        $body = new PlanBenefit();

                        $body->insurance_providers_id = $insurance_providers_id;
                        $body->year = $year;
                        $body->effective_date = $effective_date;
                        $body->plan_type = $plan_type;
                        $body->sub_type = $sub_type;
                        $body->status = $status;
                        $body->benefit_masters_id = $benefitMaster->id;
                        $body->included = $included;
                        $body->single_or_multiple = $single_or_multiple;
                        $body->description = $description;

                        //if Not include
                        if($included == 1){
                            //if single
                            if($single_or_multiple == 1){

                                $body->percentage_or_amount = $perAmount[0];
                                $body->amount = $amount[0];
                                //$body->description = $description;
                                //$body->single_or_multiple = $single_or_multiple;
                                $body->save();
                                $pb_id = $body->id;
                            }
                            //if multiple
                            elseif($single_or_multiple == 2){

                                //$body->description = $description;
                                //$body->single_or_multiple = $single_or_multiple;
                                $body->save();
                                $pb_id = $body->id;

                                //if Not include
                                if($included == 1){
                                    
                                    $co = count($perAmount);
                                    for ($k=0; $k < $co; $k++) {
                                        
                                        $pbMultiple = new PlanBenefitMultiple();
                                        $pbMultiple->plan_benefits_id = $pb_id;
                                        $pbMultiple->percentage_or_amount = $perAmount[$k];
                                        $pbMultiple->amount = $amount[$k];
                                        $pbMultiple->feature_description = $feature_description[$k];
                                        $pbMultiple->save();

                                    }
                                }
                            }
                            //if N/A
                            else{
                                $body->save();
                            }
                        }
                        elseif($included == 2){
                            $body->save();
                        }


                    }

                }
                    
                $response['status'] =  0;
            }
            else{
                //$response['status'] =  1;
                $response['status'] =  1;
                $response['msgs'][] = array("msg" => "Please Select Insurance Provider" );
            }
        }
        */

        if($check_error == 0){
            $response['status'] =  1;
        }

        echo json_encode( $response );
        exit();      
    }


    public function editFormPlanBenefit(Request $request) {

        //echo "pp22";die;
        $response = array();
        $check_error = 0;

        $benefitMasters = BenefitMaster::all();

        $pb_id_request = $request->pb_id;

        $insurance_providers_id = $request->insurance_providers_id;
        $year = $request->year;
        $effective_date = $request->effective_date;
        $plan_type = $request->plan_type;
        $status = $request->status;
        $sub_type = $request->sub_type;

        if( !empty($effective_date) ) {
            $effective_date = Carbon::createFromFormat('d/m/Y H:i', $effective_date);
        }

        if(empty($insurance_providers_id)){
            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please Select Insurance Provider" );
        }
        else if(empty($year)){
            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please Select Year" );
        }
        else if(empty($effective_date)){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please fill effective date" );
        }
    
        // 2. Effective date same as Year
        //$effective_year = Carbon::createFromFormat('Y-m-d h:m:s', $effective_date)->year; //wrong output
        $effective_year = Carbon::parse($effective_date)->year;

        if($year != $effective_year){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Year and Effective year must be of same year" );
            
        }

        //5.Effective date should be greater than current date
        $plan = PlanBenefitsPartner::find($pb_id_request);
        $old_effective_date = $plan->effective_date;

        $currentDate = Carbon::now();
        if($old_effective_date != $effective_date){

            $currentDate = Carbon::now();
            if($currentDate > $effective_date){

                $check_error = 1;
                $response['status'] =  0;
                $response['msgs'][] = array("msg" => "Effective date should be greater than current date" );
            }

        }

        //4. check if benefits has selected or not
        $benefit_idArr = array();
        foreach($benefitMasters as $benefitMaster){

            //4-a.Condition to check (part-a)-> if benefits has selected or not
            $benefit_idArr[] = $request->input('benefit_id_'.$benefitMaster->id);

            //5.Condition to check -> 
            $benefit_id_last = end($benefit_idArr);

            if(!empty($benefit_id_last)){

                $included = $request->input('included_'.$benefitMaster->id);

                //if Not Included
                if($included == 1){

                    //5.Condition to check -> if perAmount blank and amount fill
                    $amount = $request->input('amount_'.$benefitMaster->id);
                    $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                    $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                    if ($single_or_multiple !=0 ){
                        $co = count($amount);
                        for ($k=0; $k < $co; $k++) {

                            if(!empty($amount[$k])){
                                
                                if(empty($perAmount[0]) || $perAmount[0] == 0){

                                    $check_error = 1;
                                    $response['status'] =  0;
                                    $response['msgs'][] = array("msg" => "Please select Percentage or Amount on ". $benefitMaster->name );

                                }
                                
                            }
                            //5.Condition to check -> if perAmount fill and amount blank
                            if(empty($amount[$k])){

                                if($perAmount[0] > 0 || $perAmount[0] == 0){

                                    $check_error = 1;
                                    $response['status'] =  0;
                                    $response['msgs'][] = array("msg" => "Please fill amount if Percentage or Amount selected on ". $benefitMaster->name );

                                }
                            }

                        }                        
                    }

                }
            }

        }

        $benefit_idArr = array_filter($benefit_idArr);
        
        if(empty($benefit_idArr)){

            $check_error = 1;
            $response['status'] =  0;
            $response['msgs'][] = array("msg" => "Please select atleast 1 benefit" );
            
        }


        //updation
        //updation
        //updation
        //updation
        
        if($check_error == 0){

            if(!empty( $insurance_providers_id ))
            {
                //$PlanBenefit = PlanBenefit::where('id',$pb_id_request)->get();
                $PlanBenefit = PlanBenefitsPartner::find($pb_id_request);

                if( $PlanBenefit ) {

                    ////
                    PlanBenefitsPartner::where(['insurance_providers_id'=>$PlanBenefit->insurance_providers_id,'year'=>$PlanBenefit->year,'plan_type'=>$PlanBenefit->plan_type,'sub_type'=>$PlanBenefit->sub_type,'effective_date'=>$PlanBenefit->effective_date]);
                    ////                  

                    foreach($benefitMasters as $benefitMaster){

                        $benefit_id = $request->input('benefit_id_'.$benefitMaster->id);
                        $included = $request->input('included_'.$benefitMaster->id);
                        $perAmount = $request->input('perAmount_'.$benefitMaster->id);
                        $amount = $request->input('amount_'.$benefitMaster->id);
                        $feature_description = $request->input('feature_description_'.$benefitMaster->id);
                        $description = $request->input('description_'.$benefitMaster->id);
                        $required = $request->input('required_'.$benefitMaster->id);
                        
                        $single_or_multiple = $request->input('singleMul_'.$benefitMaster->id);

                        if ($benefit_id == $benefitMaster->id){

                            $benefitOfPlanExist1 = PlanBenefitsPartner::where('insurance_providers_id',$insurance_providers_id)
                                    ->where('year',$year)
                                    ->where('plan_type',$plan_type)
                                    ->where('effective_date',$effective_date)
                                    ->where('sub_type',$sub_type)
                                    ->first();
                            $benefitOfPlanExist = PlanBenefit::where('plan_benefits_partners_id',$benefitOfPlanExist1->id)
                                    ->where('benefit_masters_id',$benefit_id)->first();
                            //dd($benefitOfPlanExist);

                            if($benefitOfPlanExist){
                                
                                
                                $planBenefitUpdate = PlanBenefit::find($benefitOfPlanExist->id);
                               
                                if($planBenefitUpdate){
                                    $benefitOfPlanExist1->status = $status;
                                    $planBenefitUpdate->benefit_masters_id = $benefitMaster->id;
                                    $planBenefitUpdate->included = $included;
                                    $planBenefitUpdate->single_or_multiple = $single_or_multiple;
                                    $planBenefitUpdate->description = $description;
                                    // $planBenefitUpdate->percentage_or_amount = $perAmount[0];
                                    // $planBenefitUpdate->amount = $amount[0];

                                    //if Not include
                                    if($included == 1){
                                        //if single
                                        if($single_or_multiple == 1){

                                            $planBenefitUpdate->percentage_or_amount = $perAmount[0];
                                            $planBenefitUpdate->amount = $amount[0];
                                            $planBenefitUpdate->required = $required[0];
                                            $planBenefitUpdate->plan_benefits_partners_id = $benefitOfPlanExist1->id;

                                            $planBenefitUpdate->save();
                                            //$pb_id = $body->id;

                                            $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                        }
                                        //if multiple
                                        elseif($single_or_multiple == 2){

                                            $planBenefitUpdate->percentage_or_amount = 0;
                                            $planBenefitUpdate->amount = 0;
                                            $planBenefitUpdate->plan_benefits_partners_id = $benefitOfPlanExist1->id;
                                            $planBenefitUpdate->required = 0;
                                            $benefitOfPlanExist1->save();
                                            $planBenefitUpdate->save();
                                            //$pb_id = $body->id;

                                            $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();

                                            //if Not include
                                            if($included == 1){
                                                
                                                $co = count($amount);
                                                for ($k=0; $k < $co; $k++) {
                                                    
                                                    $pbMultiple = new PlanBenefitMultiple();
                                                    //$pbMultiple->plan_benefits_id = $pb_id;
                                                    $pbMultiple->plan_benefits_id = $benefitOfPlanExist->id;
                                                    $pbMultiple->percentage_or_amount = $perAmount[0];
                                                    $pbMultiple->amount = $amount[$k];
                                                    $pbMultiple->feature_description = $feature_description[$k];
                                                    $pbMultiple->save();

                                                }
                                            }
                                        }
                                        //if N/A
                                        else{
                                            $planBenefitUpdate->percentage_or_amount = 0;
                                            $planBenefitUpdate->amount = 0;
                                            $planBenefitUpdate->required = 0;
                                            $planBenefitUpdate->plan_benefits_partners_id = $benefitOfPlanExist1->id;
                                            $planBenefitUpdate->save();

                                            $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                            
                                        }

                                    }
                                    //if included
                                    elseif($included == 2){
                                        //dd($planBenefitUpdate->id);
                                        $planBenefitUpdate->single_or_multiple = 0;
                                        $planBenefitUpdate->percentage_or_amount = 0;
                                        $planBenefitUpdate->amount = 0;
                                        $planBenefitUpdate->description = $description;
                                        $planBenefitUpdate->required = 0;
                                        $planBenefitUpdate->plan_benefits_partners_id = $benefitOfPlanExist1->id;  
                                        //dd($planBenefitUpdate->id);
                                        $benefitOfPlanExist1->save();
                                        $planBenefitUpdate->save();
                                        //2)delete all record of multiple table of current record
                                        $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                    }
                                    //if N/A
                                    //else{
                                    //delete case afterward
                                    elseif($included == 0){

                                        //2)delete all record of multiple table of current record
                                        $res=PlanBenefit::where('id',$benefitOfPlanExist->id)->delete();
                                        $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                                    }


                                }


                            }
                            else{
                                //new row as it is like create 
                                $planBenefitInsert = new PlanBenefit();
                                
                                $benefitOfPlanExist1->status = $status;
                                $planBenefitInsert->benefit_masters_id = $benefitMaster->id;
                                $planBenefitInsert->included = $included;
                                $planBenefitInsert->single_or_multiple = $single_or_multiple;
                                $planBenefitInsert->description = $description;
                                $planBenefitInsert->plan_benefits_partners_id =$benefitOfPlanExist1->id;

                                //if Not include
                                if($included == 1){
                                    //if single
                                    if($single_or_multiple == 1){

                                        $planBenefitInsert->percentage_or_amount = $perAmount[0];
                                        $planBenefitInsert->amount = $amount[0];
                                        $planBenefitInsert->required = $required[0];

                                        $planBenefitInsert->save();
                                        $pb_id = $planBenefitInsert->id;
                                    }
                                    //if multiple
                                    elseif($single_or_multiple == 2){

                                        $planBenefitInsert->save();
                                        $pb_id = $planBenefitInsert->id;

                                        //if Not include
                                        if($included == 1){
                                            
                                            $co = count($amount);
                                            for ($k=0; $k < $co; $k++) {
                                                
                                                $pbMultiple = new PlanBenefitMultiple();
                                                $pbMultiple->plan_benefits_id = $pb_id;
                                                $pbMultiple->percentage_or_amount = $perAmount[0];
                                                $pbMultiple->amount = $amount[$k];
                                                $pbMultiple->feature_description = $feature_description[$k];
                                                $pbMultiple->save();

                                            }
                                        }
                                    }
                                    //if N/A
                                    else{
                                        $benefitOfPlanExist1->save();
                                        $planBenefitInsert->save();
                                    }
                                }
                                elseif($included == 2){
                                    $benefitOfPlanExist1->save();
                                    $planBenefitInsert->save();
                                }
                            }

                        }

                    }
                    /////

                    //delete remaining record of status 99
                   // $PlanBenefit99 = PlanBenefit::where('status',99)->get();
                    //dd($PlanBenefit99);
                    // $res=PlanBenefit::where('id',$benefitOfPlanExist->id)->delete();
                    // $res=PlanBenefitMultiple::where('plan_benefits_id',$benefitOfPlanExist->id)->delete();
                    // foreach ($PlanBenefit99 as $value) {
                    //     //echo $value->id;
                    //     PlanBenefitMultiple::where('plan_benefits_id',$value->id)->delete();
                    // }
                    //die;
                    //PlanBenefit::where('status',99)->delete();
                    /////
                    $response['status'] =  1;
                }

            }
            else{
                $response['status'] =  0;
                $response['msgs'][] = array("msg" => "Please Select Insurance Provider" );
            }

        }
        

        // if($check_error == 0){
        //     $response['status'] =  1;
        // }

        echo json_encode( $response );
        exit();      
    }


}
