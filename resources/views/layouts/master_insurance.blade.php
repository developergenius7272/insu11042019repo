<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.master_head')
  @yield('css')
</head>
<body>
<!-- Header Start -->
<header style="background:#213367;">
  <nav class="navbar navbar-expand-lg navbar-dark"> <a class="navbar-brand" href="/"><img src="{{ URL::asset('images/logo.png') }}" alt="Insure Online" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBar" aria-controls="navBar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navBar">
      <ul class="navbar-nav ml-md-auto">
        <li class="nav-item"> <a class="nav-link" href="tel:+ 971 4433 3455"><i class="fa fa-phone"></i> + 971 4433 3455</a> </li>
        @role(['agent','user'])
          <li class="nav-item"> <a class="nav-link" href="{{route('user-dashboard')}}">My Account</a></li>
        @else
          <li class="nav-item"> <a class="nav-link" id="loginreg" >Login / Register</a> </li>
        @endrole
      </ul>
      <ul class="navbar-nav navbar-social">
        <li class="nav-item"> <a class="nav-link" target="blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a> </li>
        <li class="nav-item"> <a class="nav-link" target="blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a> </li>
        <li class="nav-item"> <a class="nav-link" target="blank" href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a> </li>
      </ul>
    </div>
  </nav>
</header>
<!-- Header End --> 

<!-- Register Modal Start -->
<div class="modal fade register" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content sign-modal">
      <div class="modal-body">
        <div class="sign-inner-part">
          <div class="modal-logo"><img src="images/logo.png" alt="Insure Online" /></div>
          <div class="sign-form-part">
                <span class="invalid-feedback" role="alert">
                       <strong></strong>
                 </span>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span> </div>
                <input  type="text" id="regname" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder = "Name" required autofocus>                  
              </div>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></span> </div>
                <input  type="email" id ="regemail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder = "Email" required>
              </div>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span> </div>
                <input id="regpassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" onKeyUp="checkPasswordStrength();" required>
                    <span style="font-size: 0.7em;">(Password should have minimum 8 characters. Password should include Lower Case, Upper Case, Numbers, Special Characters)</span>

                    {{--  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif  --}}
                    
              </div>
              <div id="password-strength-status"></div>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span> </div>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password" required>
              </div>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></span> </div>
                <input type="tel"  id="regmobile" name = "mobile" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{ old('mobile') }}" placeholder="Mobile No." required>
                 {{--  @if ($errors->has('mobile'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('mobile') }}</strong>
                       </span>
                  @endif  --}}
              </div>
              <div class="btns-part">
                <button type="submit" class="btn btn-secondary btn-block" id="registernow">Register Now</button>
              </div>
            
            <div class="already-text-block">Already have an account? <a href="#" onclick="showLoginForm()">Sign In</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Register Modal End --> 

<!-- Login Modal Start -->
<div class="modal fade login" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content sign-modal">
      <div class="modal-body">
        <div class="sign-inner-part">
          <div class="modal-logo"><img src="images/logo.png" alt="Insure Online" /></div>
          <div class="sign-form-part">
              <div class="input-group">
              <span class="invalid-feedback" role="alert">
                              <strong>These credentials do not match our records.</strong>
                        </span>
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa     fa-user" aria-hidden="true"></i></span> </div>
                
                    <input  type="email" id ="signInEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required   placeholder ="Email" autofocus>    
              </div>
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span> </div>
                  <input id="signInPassword" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
              </div>
              <div class="btns-part">
                <button type="submit" id ="signin" class="btn btn-secondary btn-block">Sign In</button>
              </div>
            <div class="already-text-block">No Account? <a href="#" onclick = "showRegisterForm()">Register here</a></div>
            <div class="already-text-block"><a id="forgetpassword" href="#" data-backdrop="static" onclick="showForgetpassword()">Forgot Password?</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Login Modal End --> 

<!-- forget password Modal Start -->
<div class="modal fade" id="forgetpasswordModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content sign-modal">
      <div class="modal-body">
        <div class="sign-inner-part">
          <div class="modal-logo"><img src="images/logo.png" alt="Insure Online" /></div>
          <div class="sign-form-part">
              <div class="input-group">
                <div class="input-group-prepend"> <span class="input-group-text"><i class="fa     fa-user" aria-hidden="true"></i></span> </div>
                    <input  id="forgetemail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required   placeholder ="Email" autofocus>
                    
                        <span class="invalid-feedback" role="alert">
                             
                        </span>
                   
              </div>
              <div class="btns-part">
                <button type="submit" id="forgetpass" class="btn btn-secondary btn-block">Send Password Reset Link</button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- forget password Modal End --> 

<!-- Main Container Start -->
@yield('content')
<!-- Main Container End --> 

<!-- Company Logo Slider Start -->
<section class="company-logo-section">
  <div class="owl-carousel company-slider">
  
    @if($partners)
    
      @foreach($partners as $partner)
        @if($partner->has_logo == 1) 
          <div class="company-logo"> 
            <a href="JavaScript:void(0);" >
              <img src='{{ asset('uploads/admin/insurance_providers').'/'.$partner->id.'_main' }}' alt="{{ $partner->name}}" width=110 height=50> 
            </a> 
          </div>
        @endif
      @endforeach

    @endif
  </div>
</section>
<!-- Company Logo Slider End --> 

@include('layouts.master_footer')
@yield('script')
 
<script>
function showRegisterForm(){
   $('#loginModal').modal('hide');
   $('#registerModal').modal('show');
}
function showLoginForm(){
  
     $('#registerModal').modal('hide');
   $('#loginModal').modal('show');     
}
function showForgetpassword(){
  
  $('#loginModal').modal('hide');
  $('#forgetpasswordModal').modal('show');
          $("#forgetpasswordModal").modal({
            backdrop: 'static'
            });
  
}
$(document).ready(function() {
    $('body').on('click', '#loginreg', function(event){
    event.preventDefault();
     showLoginForm();
     $('#loginModal').modal('show');
     //$('.modal-backdrop').remove();
  });

   $('body').on('click', '#forgetpass', function(event){
    event.preventDefault();
    var email = $('#forgetemail').val();
    $.ajax({
            url: "{{ route('password.email') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: { email:email, _method: "POST"},
            success: function(response) {
               if ( response.status == 'success' ){
                 $("#forgetpasswordModal .invalid-feedback").css("display", "block");
                   $("#forgetpasswordModal .invalid-feedback").html("Password reset link sent successfully to your email id.");
                  
               }else{
                 $("#forgetpasswordModal .invalid-feedback").css("display", "block");
                 $("#forgetpasswordModal .invalid-feedback").html("This email id is not registered with us.");
               }
            },
            dataType: 'json'
        });
    
  });

  $('body').on('click', '#signin', function(event){
    event.preventDefault();
    var email = $('#signInEmail').val();
    var password = $('#signInPassword').val();
    $.ajax({
            url: "{{ route('login') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: { email:email, password:password, _method: "POST"},
            success: function(response) {
              if ( response.status == 'success' ){
                response.url
                window.location.replace(response.url);
                 
                  
               }else{
                $("#loginModal .invalid-feedback").css("display", "block");
                $("#loginModal .invalid-feedback").text(response.msg)
               }
               
            },
           dataType: 'json'
        }); 
  });

  $('body').on('click', '#registernow', function(event){
    event.preventDefault();
    var email = $('#regemail').val();
    var password = $('#regpassword').val();
    var name = $('#regname').val();
    var mobile = $('#regmobile').val();
    $.ajax({
            url: "{{ route('register') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: { email:email, password:password, name:name, mobile:mobile, _method: "POST"},
            success: function(response) {
              console.log(response);
              if ( response.status == 'success' ){
                var redirect= "{{ url('/please-verify-email') }}";
                window.location.replace(redirect);
               }else{
                $("#registerModal .invalid-feedback").css("display", "block");
                $("#registerModal .invalid-feedback").text(response.msg)
               }   
            },
             error: function(res) {
               var errors = res.responseJSON; //this will get the errors response data.
                console.log(errors);
                errorsHtml = '<div><ul>';
               for(var k in errors.errors){
                  if(errors.errors.hasOwnProperty(k)){
                     errors.errors[k].forEach(function(val){
                      errorsHtml += '<li>' + val + '</li>';
                      });
                  }
                }
                errorsHtml += '</ul></di>';
                $("#registerModal .invalid-feedback").css("display", "block");  
                $( '#registerModal .invalid-feedback' ).html( errorsHtml );
              },
           dataType: 'json'
        }); 
  });

});

function checkPasswordStrength() {
  var number = /([0-9])/;
  var alphabets = /([A-Z])/;
  var lowercase = /([a-z])/;
  var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
  
  if($('#regpassword').val().length<8) {
    $('#password-strength-status').removeClass();
    $('#password-strength-status').addClass('weak-password');
    $('#password-strength-status').html('<p class="text-danger">Weak (should be atleast 8 characters.)</p>');
  } else {    
      if($('#regpassword').val().match(number) && $('#regpassword').val().match(alphabets) && $('#regpassword').val().match(special_characters) && $('#regpassword').val().match(lowercase)) {            
      $('#password-strength-status').removeClass();
      $('#password-strength-status').addClass('strong-password');
      $('#password-strength-status').html('Strong');
        } else {
      $('#password-strength-status').removeClass();
      $('#password-strength-status').addClass('medium-regpassword');
      $('#password-strength-status').html('<p class="text-danger">Password should include Lower Case, Upper Case, Numbers, Special Characters (@, $, #)');
        } 
  }
}
</script>
</body>
</html>
