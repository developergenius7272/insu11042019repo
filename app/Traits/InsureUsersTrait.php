<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Models\AgentUser;
use App\User;

trait InsureUsersTrait
{
    public function assignDefaultAgent($user)
    {
    	$super_agent = User::active()->orderBy('id','asc')->withRole('super-agent')->first();

        $agentUser = new AgentUser();
        $agentUser->agent_id = $super_agent->id;
        $agentUser->user_id = $user->id;
        $agentUser->is_current_agent = 1;
        $agentUser->save();
    }

   
}
