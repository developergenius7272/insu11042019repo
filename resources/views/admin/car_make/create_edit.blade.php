@extends('layouts.admin.master')
{{-- Car fuel add/edit table view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Cars</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formCarMake" method="POST" @if($formType == 'edit') action="{{action('AdminCarMakeController@update', $data->id)}}" @else action="{{url('/admin/cars/car-makes')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <legend>{{$formTitle ?? ' '}}</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ $formType == 'edit' ? 'Update' : 'Enter'}} Car Make Name</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {{-- ui-autocomplete-loading --}}
                                                <input class="form-control" placeholder="{{ $formType == 'edit' ? 'Update' : 'Enter'}} Car Make Name : BMW" name="name" id="name" type="text" value="{{ $errors->any() ? old('name') : $data->name ?? '' }}">

                                                <span class="my-error" style='color:#b94a48;display:none' >*</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-4">
                                        <input type="file" class="btn-default" id="image" name="image">
                                        {{-- <p class="help-block">
                                            some help text here.
                                        </p> --}}

                                        <button class="btn btn-warning btnDeleteInsertedImage" style="display:none;margin-top:10px;" type="button">Delete Image</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                
                                    <div class="col-md-10">
                                        @if($formType == 'edit' && $data->has_logo == 1) 
                                            <img id="imgCarMake" src='{{ asset('uploads/admin/car_makes').'/'.$data->id.'_logo' }}' width=50 height=50>

                                            <button class="btn btn-warning btnDeleteImage" value="<?php echo $data->id; ?>" type="button">Delete Image</button>
                                        @endif
                                    </div>
                                    <input type="hidden" name="has_logo_image" id="has_logo_image" value="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label">Car Modal Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="carModalDescription" placeholder="Car Modal Description" rows="4"></textarea>
                                    </div>
                                </div> --}}
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{url('/admin/cars/car-makes')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')

{{-- <script src="{{ URL::asset('assets/admin/js/bootstrap/bootstrapValidator.min.js') }}"></script> --}}
{{-- <script src="js/plugin/bootstrapvalidator/bootstrapValidator.min.js"></script> --}}
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        
        // 
        /*
        $('#formCarMake').bootstrapValidator({
            // feedbackIcons : {
            //     valid : 'glyphicon glyphicon-ok',
            //     invalid : 'glyphicon glyphicon-remove',
            //     validating : 'glyphicon glyphicon-refresh'
            // },
            trigger: 'blur',
            fields : {

                name : {
                    //group : '.col-md-4',
                    validators : {
                        notEmpty : {
                            message : 'The Name is required'
                        }
                    }
                }
            }
        });
        */
        // 
        $("#formCarMake").on('submit',(function(e) {

            var is_valid =  validForm();
            
            if(is_valid == false){
                $(window).scrollTop(0);
                return false;
            }

        }));

        $("#image").change(function(){
             //$(this).attr('src','/new/image/src.jpg');   
             $(".btnDeleteInsertedImage").show();
             $(".btnDeleteImage").hide();

            $("#imgCarMake").hide();
            $("#has_logo_image").val(0);
        });

        $('.btnDeleteInsertedImage').click(function(e){

            e.preventDefault();
            var img = $("#image").val();

            if(img){
                var msg = 'Do you really want to delete Image ?';
                
                if (confirm(msg)) {

                    //$("#imgCarMake").hide();
                    $("#image").val('');
                    $(".btnDeleteInsertedImage").hide();

                } else {

                    return false;
                }
            }
        });

        $('.btnDeleteImage').click(function(e){

            e.preventDefault();

            var msg = 'Do you really want to delete Image ?';
            
            if (confirm(msg)) {

                $("#imgCarMake").hide();
                $("#has_logo_image").val(0);
                $(".btnDeleteImage").hide();
                //$(".btnDeleteInsertedImage").hide();

            } else {

                return false;
            }

        });

        
    })

</script>

<script type="text/javascript">

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var name  = $.trim($("#name").val());
        if (name == ''){

            $("#name").parent().find(".my-error").html('Please enter Car Make Name').show();
            $('#name').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        return is_valid;
    }
    
    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }
</script>
@endsection
