<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\PlanBenefit;

class PlanBenefitsPartner extends Model
{
    protected $fillable = [
        'insurance_providers_id', 'year', 'effective_date', 'plan_type','sub_type',
    ];
    protected $dates = ['effective_date'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopePlanType($query)
    {
        return $query->where('plan_type', 1);
    }

    public function scopeComprehensive($query)
    {
        return $query->where('plan_type', 0);
    }
    
    public function scopeIncluded($query)
    {
        return $query->where('included', 1);
    }
    public function scopeEffectiveDate($query)
    {
        $current_date = Carbon::now()->toDateTimeString();
        // return $query->where('effective_date','<=',$current_date);
        return $query->where('effective_date', $query->orderBy('effective_date', 'DESC')->pluck('effective_date')->first());
    }
    public function planBenefits()
    {
      return $this->hasMany(PlanBenefit::class, 'plan_benefits_partners_id', 'id');
    }

    // public function amount($premiumValue, $percentage_or_amount, $amount, $no_of_seats, $benefit_id)
    // {
    //    return $benefit_id;
    //     if ($benefit_id == 6) {
    //         if ($percentage_or_amount == 1 ) { 
    //             $amountcalcultaed = ($premiumValue * $amount) / 100;
    //             return  (($no_of_seats - 1) * $amountcalcultaed);
    //         } else {
    //             return (($no_of_seats - 1) * $amount);
    //         }
    //     } else {
    //         if ($percentage_or_amount == 1 ) { 
    //             $amountcalcultaed = ($premiumValue * $amount) / 100;
    //             return  $amountcalcultaed;
    //         } else {
    //             return $amount;
    //         }
    //     }
    // }
}
