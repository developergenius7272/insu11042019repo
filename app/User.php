<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\EmailNotification;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;
use App\Mail\WelcomeToInsureOnline;
use Illuminate\Support\Facades\Log;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['dob'];

    protected $fillable = ['name','mobile','password','email','address','gender','maritalstatus','passport_number','passport_expiry_date','license_number','license_expiry_date','uae_id','uae_id_expiry_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function verifyEmailNotification()
    {
        $this->verify(new EmailNotification);
    }

    // public function scopeActive($query)
    // {
    //     return $query->where('car_makes.status',1);    
    // }

    public function scopeActive($query)
    {
        return $query->where('status', 1);    
    }
    
    public function scopeRoleUser($query)
    {
        return $query->where('users.user_role',0);
    }

    public function scopeRoleAgent($query)
    {
        return $query->where('users.user_role',1);
    }

    public static function sendWelcomeEmail($user)
    {
        // Generate a new reset password token
        $token = app('auth.password.broker')->createToken($user);
        $url = route('password.reset', ['token' => $token,'email'=> $user->email]);
        Log::debug($user);
        // Send email
        Mail::to($user->email)->send(new UserCreated($url, $user));
    }
    
    public static function welcomeNotification($user) {
        Mail::to($user->email)->send(new WelcomeToInsureOnline($user));
    }

    public function order() {
        return $this->hasMany('App\Models\Order', 'users_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->email));
    }
    
    public function orderagent() {
        return $this->hasMany('App\Models\Order', 'created_by', 'id');
    }

    public function clients() {
        return $this->hasMany('App\Models\AgentUser', 'agent_id', 'id')->active();
    }

    public function userAgentCommission() {
        return $this->hasMany('App\Models\UserAgentCommission', 'user_id', 'id');
    }

    public function userAgentAddonCommission() {
        return $this->hasMany('App\Models\UserAgentAddonCommission', 'user_id', 'id');
    }
    
    public function upload() {
        return $this->hasMany('App\Models\Upload', 'user_id', 'id');
    }
}
