<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrossSellingProducts extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'provider_name', 'price', 'benefits', 'status','vat','fee'
    ];

    public function addonCategory() {
        return $this->belongsTo('App\Models\AddonCategory', 'addon_category_id', 'id');
    }

    public function insuranceProvider() {
        return $this->belongsTo('App\Models\InsuranceProvider', 'insurance_provider_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);    
    }

    public function userAgentAddonCommission() {
        return $this->hasMany('App\Models\UserAgentAddonCommission', 'cross_selling_product_id', 'id');
    }

}
