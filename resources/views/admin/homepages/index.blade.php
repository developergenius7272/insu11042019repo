@extends('layouts.admin.master')
{{-- prontend users table view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>
                         
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>{{$tableHeading ?? 'Cars'}}</h2>

                </header>
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{-- <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p> --}}
                        
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Banner Name</th>
                                    <th>Heading</th>
                                    <th>Tag Line</th>
                                    <th>Button Text</th>
                                    <th>Button URL</th>
                                    <th>Display At Home</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($homepages)
                                @foreach($homepages as $homepage)
                                <tr>
                                    <td>
                                        @if(!empty($homepage->has_logo))
                                            <img src='{{ asset('uploads/homepage_banners').'/'.$homepage->id.'_logo' }}' width=50 height=50>
                                        @endif
                                    </td>
                                    <td>{{ $homepage->name }} </td>
                                    <td>{{ $homepage->heading }}</td>
                                    <td>{{ $homepage->tag_line }}</td>
                                    <td>{{ $homepage->button_text }}</td>
                                    <td>{{ $homepage->button_url }}</td>

                                    <td>
                                        <select name="display_at_home" class="display_at_home" valuex="{{$homepage->id}}" >
                                            <?php                                   
                                                $display_at_homeArry = array( 0  => 'Not Display' , 1 => 'Display' );
                                                foreach( $display_at_homeArry as $key => $value ) {
                                                
                                                if( $key == $homepage->display_at_home ) {

                                            ?>
                                            <option class="form-control"  selected  value="<?php echo $key  ?>" > <?php  echo $value ?> </option>
                                            <?php
                                                }
                                                else {
                                            ?>
                                            <option class="form-control"  value="<?php echo $key  ?>" > <?php echo $value ?> </option>
                                            <?php
                                                }
                                                }
                                            ?>
                                        </select>   
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" style="float:left;margin-right:10px;" href="{{action('HomepageController@edit', $homepage->id)}}">Edit</a>

                                        <form action="{{action('HomepageController@destroy', $homepage->id)}}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
		
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
    $(document).ready(function() {
        
        pageSetUp();
        
        /* // DOM Position key index //
    
        l - Length changing (dropdown)
        f - Filtering input (search)
        t - The Table! (datatable)
        i - Information (records)
        p - Pagination (paging)
        r - pRocessing 
        < and > - div elements
        <"#id" and > - div with an id
        <"class" and > - div with a class
        <"#id.class" and > - div with an id and class
        
        Also see: http://legacy.datatables.net/usage/features
        */	

        /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });
    
    })

</script>

<script type="text/javascript">

$('.display_at_home').change(function(){

    var homepageid = $(this).attr("valuex");
    var valDisplay = $(this).val();

    $.ajax(
    {
        //url:'/admin/cars/make/models',
        type:'POST',
        //url:'/admin/changehomepage',
        url:'{{route('admin.editHomePage')}}',
        data:{homepageid:homepageid, valDisplay:valDisplay},

        success: function(response){

            response = $.parseJSON(response);
            var status = response.status;
            //alert(status);

            if( status == 1 ) {

                // $("#imgCarMake").hide();
                // $("#has_logo_image").val(0);
                alert('Not allowed!!!');
                //$(".display_at_home").val(0).change();
            }
        }
    });
});
</script>
@endsection
