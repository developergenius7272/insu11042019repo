<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_provider_id')->nullable();
            $table->integer('car_model_body_type_id')->default(0)->nullable();
            $table->integer('car_model_min_value')->default(0)->nullable();
            $table->integer('car_model_max_value')->default(0)->nullable();
            $table->unsignedTinyInteger('min_seat')->default(0)->nullable();
            $table->unsignedTinyInteger('max_seat')->default(0)->nullable();
            $table->unsignedTinyInteger('min_age')->default(0)->nullable();
            $table->unsignedTinyInteger('max_age')->default(0)->nullable();
            $table->string('excess_value')->nullable();
            $table->datetime('effective_date')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('excesses');
    }
}
