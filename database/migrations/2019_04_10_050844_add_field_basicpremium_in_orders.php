<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBasicpremiumInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->double('basic_premium')->after('benefit_options')->default(0)->nullable();
            $table->double('app_commission')->after('base_commission_percentage')->default(0)->nullable();
            $table->integer('car_premiums_id')->after('users_id')->default(0)->nullable();
            $table->double('agent_commission')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
