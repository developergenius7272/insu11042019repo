<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyRenewalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_renewal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->tinyInteger('is_thirdparty_only')->nullable();
            $table->tinyInteger('is_agency_repair')->nullable();
            $table->tinyInteger('is_policy_expired')->nullable();
            $table->date('registeration_date')->nullable();
            $table->tinyInteger('is_claim')->nullable();
            $table->tinyInteger('no_claim_cert')->nullable();
            $table->tinyInteger('no_of_claim_years')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_renewal_details');
    }
}
