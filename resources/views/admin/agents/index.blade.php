@extends('layouts.admin.master')
{{-- prontend users table view --}}
@section('content')
<div class="row">
    <!-- error/message -->
    <div >
        @if (Session::has('error'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('error') !!}</p>
            </div>
        @endif
        @if (Session::has('success'))
             <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <p>{!! Session::get('success') !!}</p>
             </div>
         @endif
    </div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>{{$tableHeading ?? 'Agents'}}</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        {{-- <p>Adds borders to any table row within <code>&lt;table&gt;</code> by adding the <code>.table-bordered</code> with the base class</p> --}}
                        
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Reset password</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($users)
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                        @if(!empty($user->has_logo))
                                            <img src='{{ asset('uploads/admin/agents').'/'.$user->id.'_logo' }}' onerror="if (this.src != 'error.jpg') this.src = '{{ asset('images').'/user_default.png' }}';" width=50 height=50>
                                        @else
                                            <img src='{{ asset('images').'/user_default.png' }}' width=50 height=50>
                                        @endif
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-primary password-sent" onclick="return onclickFunction('{{ $user->email }}')">Send Reset Email

                                        </td>
                                    <td>{{ isset($user->created_at) ? $user->created_at->format('d/m/Y H:i') : '' }}</td>
                                    <td>
                                        {{ $user->status == 0 ? 'Inactive' : 'Active' }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" style="float:left;margin-right:10px;" href="{{route('admin.edit.agent', ['id' => $user->id])}}">Edit</a>

                                        {{-- <form action="{{action('AdminUserController@destroy', $user->id)}}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form> --}}

                                        @if( $user->clients->count() > 0 )
                                            
                                            <a class="btn btn-primary" href="{{route('admin.agents.showAgentsUser',[$user->id])}}">View Users</a>
                                        
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
        
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
    $(document).ready(function() {
        
        pageSetUp();
        
        /* // DOM Position key index //
    
        l - Length changing (dropdown)
        f - Filtering input (search)
        t - The Table! (datatable)
        i - Information (records)
        p - Pagination (paging)
        r - pRocessing 
        < and > - div elements
        <"#id" and > - div with an id
        <"class" and > - div with a class
        <"#id.class" and > - div with an id and class
        
        Also see: http://legacy.datatables.net/usage/features
        */  

        /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "order": [[ 4, 'desc' ]],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

        /* END BASIC */
        
        // /* COLUMN FILTER  */
        // var otable = $('#datatable_fixed_column').DataTable({
        //     //"bFilter": false,
        //     //"bInfo": false,
        //     //"bLengthChange": false
        //     //"bAutoWidth": false,
        //     //"bPaginate": false,
        //     //"bStateSave": true // saves sort state using localStorage
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_fixed_column) {
        //             responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_fixed_column.respond();
        //     }        
        
        // });
        
        // // custom toolbar
        // $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
                
        // // Apply the filter
        // $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
            
        //     otable
        //         .column( $(this).parent().index()+':visible' )
        //         .search( this.value )
        //         .draw();
                
        // } );
        // /* END COLUMN FILTER */   
    
        // /* COLUMN SHOW - HIDE */
        // $('#datatable_col_reorder').dataTable({
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "autoWidth" : true,
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_col_reorder) {
        //             responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_col_reorder.respond();
        //     }            
        // });
        
        // /* END COLUMN SHOW - HIDE */

        // /* TABLETOOLS */
        // $('#datatable_tabletools').dataTable({
            
        //     // Tabletools options: 
        //     //   https://datatables.net/extensions/tabletools/button_options
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
        //             "t"+
        //             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "oLanguage": {
        //         "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //     },       
        //     "oTableTools": {
        //             "aButtons": [
        //             "copy",
        //             "csv",
        //             "xls",
        //             {
        //                 "sExtends": "pdf",
        //                 "sTitle": "SmartAdmin_PDF",
        //                 "sPdfMessage": "SmartAdmin PDF Export",
        //                 "sPdfSize": "letter"
        //             },
        //             {
        //                 "sExtends": "print",
        //                 "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
        //             }
        //             ],
        //         "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
        //     },
        //     "autoWidth" : true,
        //     "preDrawCallback" : function() {
        //         // Initialize the responsive datatables helper once.
        //         if (!responsiveHelper_datatable_tabletools) {
        //             responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
        //         }
        //     },
        //     "rowCallback" : function(nRow) {
        //         responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
        //     },
        //     "drawCallback" : function(oSettings) {
        //         responsiveHelper_datatable_tabletools.respond();
        //     }
        // });
        
        // /* END TABLETOOLS */
    
    })

</script>

<script type="text/javascript">
/*
function onclickFunction(val){

    $.ajax(
    {
        type:'POST',
     
        url:'{route('admin.passwordSent')}}',
        data:{user_id:val},

        success: function(response){

            response = $.parseJSON(response);
            var status = response.status;

            if( status == 1 ) {

                alert('Password link has been sent');

            }
            else{
                alert('error');                
            }
        }
    });

}
*/

function onclickFunction(val){

    $.ajax({
      url: "{{ route('password.email') }}",
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'POST',
      data: { email:val, _method: "POST"},
      success: function(response) {
         if ( response.status == 'success' ){
           $(".invalid-feedback").css("display", "block");
             $(".invalid-feedback").html("Password reset link sent successfully to your email id.");
            
         }else{
           $(".invalid-feedback").css("display", "block");
           $(".invalid-feedback").html("This email id is not registered with us.");
         }
      },
      dataType: 'json'
    });
}

</script>
@endsection
