@extends('layouts.admin.master_login')

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-md-push-4 col-lg-4 col-lg-push-4">
        <div class="well no-padding">
            <form  id="login-form" class="smart-form client-form">
                @csrf
                <header>
                    Sign In
                </header>

                <fieldset>
                    
                    <section>
                    <span class="invalid-feedback" style="display:none;" role="alert">
                              <strong>These credentials do not match our records.</strong>
                        </span>
                        <label class="label">E-mail</label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <input type="email" id="adminEmail" name="email" value="{{ old('email') }}">
                            <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                    </section>

                    <section>
                        <label class="label">Password</label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                            <input type="password" id="adminPassword" name="password">
                            <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                        {{-- <div class="note">
                            <a href="forgotpassword.html">Forgot password?</a>
                        </div> --}}
                    </section>

                    {{-- <section>
                        <label class="checkbox">
                            <input type="checkbox" name="remember" checked="">
                            <i></i>Stay signed in</label>
                    </section>
                </fieldset> --}}
                <footer>
                    <button type="submit" id ="adminSignIn" class="btn btn-primary">
                        Sign in
                    </button>
                </footer>
            </form>

        </div>
        
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
    $('body').on('click', '#adminSignIn', function(event){
        event.preventDefault();
        var email = $('#adminEmail').val();
        var password = $('#adminPassword').val();
        $.ajax({
                url: "{{ route('admin.login.submit') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: { email:email, password:password, _method: "POST"},
                success: function(response) {
                if ( response.status == 'success' ){
                    response.url
                   
                    window.location.replace(response.url);
                    
                    
                }else{
                    $(".invalid-feedback").css("display", "block");
                    $(".invalid-feedback").text(response.msg)
                }
                
                },
            dataType: 'json'
            }); 
    });
});


</script>

@endsection





@section('content__old')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.login.submit') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <span>admin@app.com</span>
                                <span>admin-password</span>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
