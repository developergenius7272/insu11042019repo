<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\InsuranceProvider;
use App\Models\CarModelBodyType;

class Excess extends Model
{
    protected $appends = 'excesValue';

    protected $fillable = [
        'insurance_provider_id', 'car_model_body_type_id', 'car_model_min_value', 'car_model_max_value', 'min_seat', 'max_seat', 'min_age', 'max_age', 'excess_value', 'effective_date', 'status',
    ];

    protected $dates = ['effective_date'];

    public function InsuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id');
    }

    public function carModelBodyType()
    {
        return $this->belongsTo(CarModelBodyType::class, 'car_model_body_type_id', 'id')->where('status', '=', 1);    
    }

    // public function excesValue()
    // {
    //     $this->excessValue = $this->excess_value ? $this->excess_value : $this->excess_description;
    //     return $this->excessValue;
    // }

    public function getExcesValueAttribute()
    {
        return $this->excess_value ? $this->excess_value : $this->excess_description; 
    }
}
