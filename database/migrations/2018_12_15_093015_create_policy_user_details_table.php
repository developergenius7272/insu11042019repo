<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('name',255)->nullable();
            $table->string('mobile',50)->nullable();
            $table->integer('nationality')->nullable();
            $table->integer('license_country')->nullable();
            $table->year('intl_driving_exp_year')->nullable();
            $table->string('uae_driving_years')->nullable();
            $table->date('dob')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_user_details');
    }
}
