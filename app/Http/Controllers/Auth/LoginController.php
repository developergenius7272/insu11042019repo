<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/start';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return redirect()->route('start')->with('error', 'Please login to verify your email');
    }

    protected function redirectTo()
    {
        if ( auth()->user()->can(['agent']) ) {
            return 'agent/mypolicy';
        } elseif (auth()->user()->can(['super-admin'])) {
            return 'admin/home';
        } else {
            return 'user/mypolicy';
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if ( auth()->user()->status ) {
                if ( auth()->user()->can(['agent']) ) {
                    if(auth()->user()->can('agent')){
                    $url = route('agent-mypolicy');
                    }else{
                        Auth::logout();
                        return response()->json(['status' => 'failed','msg'=>'Your account is deactivated. Please contact InsureOnline administrator.']);
                    }
                } elseif ($request->is('admin/*') && auth()->user()->hasRole(['super-admin','sub-admin'])) {
                    $url = route('admin.home');
                } else {
                    if(auth()->user()->can('user')){
                    $url = route('user-mypolicy');
                    }else{
                        Auth::logout();
                        return response()->json(['status' => 'failed','msg'=>'Your account is deactivated. Please contact InsureOnline administrator.']);
                    }
                }
                return response()->json(['status' => 'success', 'url' => $url]);
            } else {
                Auth::logout();
                return response()->json(['status' => 'failed','msg'=>'Your account is deactivated. Please contact InsureOnline administrator.']);
            }
        } else {
            //return response()->json(['status' => 'failed','msg'=>'These credentials do not match our records.']);
            return response()->json(['status' => 'failed','msg'=>'Invalid email or password.']);
        }
    }
}
