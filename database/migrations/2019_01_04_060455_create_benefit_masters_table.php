<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\BenefitMaster;

class CreateBenefitMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('benefit_masters');
        Schema::create('benefit_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('for_each_passenger')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });

        //
        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Loss and Damage of Vehicle';
        $BenefitMaster->description = 'Loss and Damage of Vehicle';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Third Party Bodily Injury';
        $BenefitMaster->description = 'Third Party Bodily Injury';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Third Party Property Damage';
        $BenefitMaster->description = 'Third Party Property Damage';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Ambulance Cover';
        $BenefitMaster->description = 'Ambulance Cover';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Personal Accident Benefit - Driver';
        $BenefitMaster->description = 'Personal Accident Benefit - Driver';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Personal Accident Benefit – Passengers';
        $BenefitMaster->description = 'Personal Accident Benefit – Passengers';
        $BenefitMaster->for_each_passenger = 1;
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Emergency Medical Expenses';
        $BenefitMaster->description = 'Emergency Medical Expenses';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Personal Injury';
        $BenefitMaster->description = 'Personal Injury';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Oman Cover';
        $BenefitMaster->description = 'Oman Cover';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Natural Disaster, Strike, Riots';
        $BenefitMaster->description = 'Natural Disaster, Strike, Riots';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Personal Belongings';
        $BenefitMaster->description = 'Personal Belongings';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Windscreen Damage';
        $BenefitMaster->description = 'Windscreen Damage';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Replacement of Locks';
        $BenefitMaster->description = 'Replacement of Locks';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();
        
        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Valet Parking Theft';
        $BenefitMaster->description = 'Valet Parking Theft';
        $BenefitMaster->status = 1;
        $BenefitMaster->save(); 

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Road Side Assistance';
        $BenefitMaster->description = 'Road Side Assistance';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();      

        $BenefitMaster = new BenefitMaster();
        $BenefitMaster->name = 'Courtesy Car Cash Benefit/Rent a Car';
        $BenefitMaster->description = 'Courtesy Car Cash Benefit/Rent a Car';
        $BenefitMaster->status = 1;
        $BenefitMaster->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_masters');
    }
}
