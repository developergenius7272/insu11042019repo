<?php

namespace App\Http\Controllers;
// 
use App\Models\CarModelBodyType;
use App\Models\CarModelBodyTypeKeyword;
// 
use Illuminate\Http\Request;
use Auth;

class AdminCarModelBodyController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Car'; 
        $this->data['breadcrumbs'][] = 'model';
        $this->data['breadcrumbs'][] = 'Body Types';
        $this->data['pageTitle'] = 'Car Body Types';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['tableHeading'] = 'Car model Bodies';
        $this->data['carBodies'] = CarModelBodyType::all();
        return view('admin.car_model_bodies.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new car model body';
        return view('admin.car_model_bodies.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;
        // 
        $keywords = $request->keyword;

        request()->validate([
            'name' => 'required|unique:car_model_body_types|min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 3 characters.",
            'name.max' => "Name $name should not be greater than 100 characters."
        ]);

        if(!empty($name ))
        {
            $body = CarModelBodyType::where('name',$name)->first();
            if( !$body ) {
                $body = new CarModelBodyType();
                $body->name = $name;
                $body->status = $status;
                $body->created_by = $adminID;
                $body->modified_by = $adminID;
                $body->save();

                // 
                array_push($keywords,$name);
                // 
                if( count($keywords) ) {
                    foreach($keywords as $keyword) {
                        if( !empty($keyword) ) {

                            $exist = CarModelBodyTypeKeyword::where('keyword',$keyword)
                                ->where('car_model_body_type_id','<>',$body->id)
                                ->first();
                            // 
                            if( !$exist ) {
                                CarModelBodyTypeKeyword::updateOrCreate(
                                    ['car_model_body_type_id' => $body->id, 'keyword' => $keyword],
                                    ['status' => 1]
                                );
                            }
                        }
                    }
                }
                //
                return redirect('/admin/cars/car-model-body')->with('success', 'Car Model Body Type added successfully');
            } else {
                return redirect('/admin/cars/car-model-body')->with('error', 'Car Model Body Type not added');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit car model body';
        $this->data['data'] = CarModelBodyType::find($id);
        // dd($this->data['data']->keyword);
        if( $this->data['data'] ) {
            return view('admin.car_model_bodies.create_edit',$this->data);
        }
        else{
            return redirect('/admin/cars/car-model-body')->with('error', 'No data found');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();

        $name = $request->name;
        $status = $request->status;
        // 
        $keywords = $request->keyword;

        request()->validate([
            'name' => "required|unique:car_model_body_types,name,$id|min:2|max:100"

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $name has already been taken.",
            'name.min' => "Name $name must be at least 3 characters.",
            'name.max' => "Name $name should not be greater than 100 characters.",
        ]);

        if(!empty($name ))
        {
            $bodyExist = CarModelBodyType::where('name',$name)->where('id','<>',$id)->first();
            if( !$bodyExist ) {
                $body = CarModelBodyType::find($id);
                if( $body ) {
                    $body->name = $name;
                    $body->status = $status;
                    $body->modified_by = $adminID;
                    $body->save();
                    // 
                    CarModelBodyTypeKeyword::where('car_model_body_type_id',$body->id)->update(['status' => 99]);
                    // 
                    array_push($keywords,$name);
                    // 
                    if( count($keywords) ) {
                        foreach($keywords as $keyword) {
                            if( !empty($keyword) ) {

                                $exist = CarModelBodyTypeKeyword::where('keyword',$keyword)
                                    ->where('car_model_body_type_id','<>',$body->id)
                                    ->first();
                                // 
                                if( !$exist ) {
                                    CarModelBodyTypeKeyword::updateOrCreate(
                                        ['car_model_body_type_id' => $body->id, 'keyword' => $keyword],
                                        ['status' => 1]
                                    );
                                }
                            }
                        }
                    }
                    // 
                    CarModelBodyTypeKeyword::where('car_model_body_type_id',$body->id)->where('status', 99)->delete();
                    // 
                    return redirect('/admin/cars/car-model-body')->with('success', 'Car Model Body Type updated successfully');
                } else {
                    return redirect('/admin/cars/car-model-body')->with('error', 'Car Model Body Type has not updated');
                }
            } else {
                return redirect('/admin/cars/car-model-body')->with('error', 'Car Model Body Type has not updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $body = CarModelBodyType::find($id);
        if( $body ) {
            // find child data if any
            $carModelsVariantCount = \App\Models\CarModelVariant::where('car_model_body_type_id', $body->id)->count();
            if( $carModelsVariantCount ) {
                return redirect('/admin/cars/car-model-body')->with('success','Information has not been deleted');
            } else {
                $body->delete();
                return redirect('/admin/cars/car-model-body')->with('success','Information has been deleted');
            }
        } else {
            return redirect('/admin/cars/car-model-body')->with('success','No information found');
        }
    }

    /**
     * Check if keyword already associate with other car model body type
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function keywordExist( Request $request ) {
        $keyword = $request->name;
        $bodyId = $request->bodyId;
        $keywordExist = CarModelBodyTypeKeyword::where('keyword',$keyword);
        if( $bodyId > 0 ) {
            $keywordExist = $keywordExist->where('car_model_body_type_id','<>',$bodyId);
        }
        $keywordExist = $keywordExist->first();
        if( $keywordExist ) {
            return response()->json(['action' => 0,'error' => 'Already Exist']);
        } else {
            return response()->json(['action' => 1,'error' => '']);
        }
    }
}
