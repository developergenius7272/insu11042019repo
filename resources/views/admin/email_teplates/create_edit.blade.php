@extends('layouts.admin.master')
{{-- Car fuel add/edit table view --}}
@section('content')
<!-- <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script> -->

<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Email Template</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formEmailTemplate" method="POST" @if($formType == 'edit') action="{{action('AdminEmailTemplateController@update', $data->id)}}" @else action="{{route('admin.email-templates.store')}}" @endif enctype="multipart/form-data">
                             @csrf
                             @if($formType == 'edit')
                                <input id="form_method" name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                                <legend>{{$formTitle ?? ' '}}</legend>
                                {{--  --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ $formType == 'edit' ? '' : 'Enter'}} Code</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                            @if( $formType == 'edit' )
                                                <input class="form-control" readonly="" disabled="" type="text" value="{{$data->code}}">
                                                
                                            @else
                                                <input class="form-control" placeholder="Code" name="code" id="code" type="text" value="{{ $errors->any() ? old('code') : '' }}">
                                            @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-2">Description</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Description" name="description" id="description" type="text" value="{{ $errors->any() ? old('code') : $data->description ?? '' }}">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Subject</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Subject" name="default_subject" id="default_subject" type="text" value="{{ $errors->any() ? old('default_subject') : $data->default_subject ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">From Email</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="From Email" name="default_from_email" id="default_from_email" type="text" value="{{ $errors->any() ? old('default_from_email') : $data->default_from_email ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-2">Title (Heading)</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Title (Heading)" name="title" id="title" type="text" value="{{ $errors->any() ? old('title') : $data->title ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Body (html)</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <textarea cols="60" id="main_html" name="main_html" class="form-control" required="required">{{isset($data['main_html']) ? $data['main_html']: ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Button Title</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Button Title" name="button_title" id="button_title" type="text" value="{{ $errors->any() ? old('button_title') : $data->button_title ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Button Link</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="form-control" placeholder="Button Link" name="button_link" id="button_link" type="text" value="{{ $errors->any() ? old('button_link') : $data->button_link ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Send Grid Template</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <select name="template_code" class="form-control">
                                                    @foreach($bases as $base)
                                                        <option value="{{$base->template_id}}" @if(isset( $data->template_code) && $data->template_code == $base->template_id ) selected @endif >{{$base->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                @if($formType == 'edit') 
                                                    <select name="status" class="form-control" >
                                                        @php ($statusArry = array( 0  => 'Inactive' , 1 => 'Active' ))

                                                        @foreach($statusArry as $key => $value)
                                                            <option class="form-control" value="{{$key}}" 
                                                                @if(old('status') == $key) 
                                                                    {{ 'selected' }} 
                                                                @elseif($key == $data->status)
                                                                    {{ 'selected' }} 
                                                                @endif
                                                                >{{ $value }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select name="status" class="form-control" >
                                                        <option  class="form-control" value="0"
                                                            @if(old('status') == 0) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Inactive</option>

                                                        <option  class="form-control" value="1"
                                                            @if(old('status') == 1) 
                                                                {{ 'selected' }} 
                                                            @endif
                                                        >Active</option>
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--  --}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Send Preview Email to:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="preview_email" class="form-control"> 
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary" id="send_email" type="button">Send</button>
                                    </div>
                                </div>
                            
                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.email-templates.index')}}" class="btn btn-default">Cancel</a>
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="main_html_current" id="main_html_current">

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

@endsection

@section('script')

 
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{URL::asset('assets/admin/js/plugin/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        CKEDITOR.replace( 'main_html', { height: '380px', startupFocus : true} );
        pageSetUp();

        $("#send_email").click(function(event) {
            var editor_current = CKEDITOR.instances.main_html.getData();
            var current_input = $('#main_html_current').val(editor_current);
            $('#formEmailTemplate').append(current_input);
            
            $("#form_method").val('POST');
            // 
            $.ajax({
                url: '{{route("admin.email-templates.preview")}}',
                type: "POST",
                dataType: 'json',
                data: $("#formEmailTemplate").serialize(),
                success: function (result) {
                    // $("#formEmailTemplate").text(result.error_message);

                    $('#main_html_current').val('');
                    $("#form_method").val('PATCH');
                },
                error: function (xhr, resp, text) {
                    $("#form_method").val('PATCH');
                    $('#main_html_current').val('');
                }
            });
            // $.ajax({
            //     url: '{{route("admin.email-templates.preview")}}',
            //     type: 'POST',
            //     dataType: 'json',
            //     data: $("#formEmailTemplate").serialize(),
            // })
            // .done(function() {
            //     console.log("success");
            // })
            // .fail(function() {
            //     console.log("error");
            // })
            // .always(function() {
            //     console.log("complete");
            // });
            
        });
    })

</script>
@endsection
