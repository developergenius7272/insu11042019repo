<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionFieldsInAdditionalcoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additionalcovers', function (Blueprint $table) {
            
            $table->tinyInteger('site_commission_percentage_or_amount')->default(0)->nullable()->comment='0 = N/A, 1 = Percentage, 2 = Amount';
            $table->double('site_commission_amount')->default(0)->nullable();
            $table->double('site_commission_value')->default(0)->nullable();
            // 
            $table->integer('agent_id')->default(0)->nullable();
            $table->tinyInteger('agent_commission_percentage_or_amount')->default(0)->nullable()->comment='0 = N/A, 1 = Percentage, 2 = Amount';
            $table->double('agent_commission_amount')->default(0)->nullable();
            $table->double('agent_commission_value')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additionalcovers', function (Blueprint $table) {
            //
        });
    }
}
