<?php

namespace App\Http\Controllers;
// 
use App\Models\CarMake;
use App\Models\CarModel;
// 
use Illuminate\Http\Request;
use Auth;

class AdminCarModelController extends Controller
{
    private $data =	[];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['breadcrumbs'][] = 'Car'; 
        $this->data['breadcrumbs'][] = 'Models';
        $this->data['pageTitle'] = 'Car Models';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    /*
        $this->data['tableHeading'] = 'Car Models';
        $carModels = CarModel::select('car_models.*')
                    ->addSelect('car_makes.name as car_make_name')
                    ->join('car_makes','car_makes.id', 'car_models.car_make_id')
                    ->get();
        $this->data['carModels'] = $carModels;
    */
        //dd($carModels);
        //$carModels = CarModel::with('carmake')->get()->toArray();
        $carModels = CarModel::with('carmake')->get();
        //dd($carModels);
        $this->data['carModels'] = $carModels;

        return view('admin.car_model.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['formType'] = 'add';
        $this->data['formTitle'] = 'Create new car model';
        // 
        $this->data['carMakes'] = CarMake::active()->get();
        return view('admin.car_model.create_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $adminID = Auth::id();

        $carMakeId = $request->carMakeId;
        $carMakeName = $request->carMakeNewName;
        $carModalName = $request->name;
        $carModalDescription = $request->carModalDescription;
        $status = $request->status;

        request()->validate([
            'name' => "required|unique:car_models,name,NULL,id,car_make_id,$carMakeId|min:2|max:100",
            //'name' => 'min:2|max:100'

        ], [
            'name.required' => "The name field is required.",
            'name.unique' => "The name $carModalName has already been taken.",
            'name.min' => "Name $carModalName must be at least 2 characters.",
            'name.max' => "Name $carModalName should not be greater than 100 characters.",
        ]);

        if( empty($carModalName) ) {
            return redirect('/admin/cars/car-models')->with('error', 'Empty data');
        }
        // 
        if( $carMakeId == 'new' ) {
            if( empty($carMakeName) ) {
                return redirect('/admin/cars/car-models')->with('error', 'Empty data');
            }
            $carMakeExist = CarMake::where('name',$carMakeName)->first();
            if( !$carMakeExist ) {
                $carMake = new CarMake();
                $carMake->name = $carMakeName;
                $carMake->status = $status;
                $carMake->save();
                // 
                $carMake->order = $carMake->id;
                $carMakeId = $carMake->id;
            } else {
                $carMakeId = $carMakeExist->id;
            }
        }
        // 
        if( $carMakeId > 0 ) {
            $carMake = CarMake::where('id',$carMakeId)->first();
            if( $carMake ) {
                // 
                $carModel = CarModel::where('car_make_id',$carMakeId)->where('name',$carModalName)->first();
                if( !$carModel ) {
                    $car = new CarModel();
                    $car->car_make_id = $carMakeId;
                    $car->name = $carModalName;
                    $car->description = $carModalDescription;
                    $car->status = $status;
                    $car->created_by = $adminID;
                    $car->modified_by = $adminID;
                    $car->save();
                    return redirect('/admin/cars/car-models')->with('success', 'Car Model added successfully');
                } else {
                    return redirect('/admin/cars/car-models')->with('success', 'Car Model has not added');
                }
            }
        } else {
            return redirect('/admin/cars/car-models')->with('success', 'Car Model has not added');
        }
        // // 
        // if(!empty( $carModalName ))
        // {
        //     $body = CarModel::where('name',$name)->first();
        //     if( !$body ) {
        //         $body = new CarModel();
        //         $body->name = $name;
        //         $body->save();
        //         return redirect('/admin/cars/car-models')->with('success', 'Information has been added');
        //     } else {
        //         return redirect('/admin/cars/car-models')->with('error', 'Information has not been added');
        //     }
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['formType'] = 'edit';
        $this->data['formTitle'] = 'Edit car model';
    /*
        $data = CarModel::select('car_models.*')
                ->addSelect('car_makes.name as car_make_name')
                ->where('car_models.id',$id)
                ->leftjoin('car_makes','car_makes.id','car_models.car_make_id')
                ->first();
        $this->data['data'] = $data;
    */
        
        $data = CarModel::with('carmake')->where('id', $id)->first();
        $this->data['data'] = $data;

        if( $this->data['data'] ) {
            return view('admin.car_model.create_edit',$this->data);
        }
        else{
            return redirect('/admin/cars/car-models')->with('error', 'No data found');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adminID = Auth::id();
        $name = $request->name;
        $status = $request->status;
        $body = CarModel::find($id);
        if( $body ) {
        
            $carMakeId = $body->car_make_id;
        
            request()->validate([
                'name' => "required|unique:car_models,name,$id,id,car_make_id,$carMakeId|min:2|max:100",
            ], [
                'name.required' => "The name field is required.",
                'name.unique' => "The name $name has already been taken.",
                'name.min' => "Name $name must be at least 2 characters.",
                'name.max' => "Name $name should not be greater than 100 characters.",
            ]);
            // 
            $body->name = $name;
            $body->status = $status;
            $body->modified_by = $adminID;
            $body->save();
            return redirect('/admin/cars/car-models')->with('success', 'Car Model updated successfully');
            
        } else {
            return redirect('/admin/cars/car-models')->with('error', 'Car Model not updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carModel = CarModel::find($id);
        if( $carModel ) {
            // find child data if any
            $carModelsVariantCount = \App\Models\CarModelVariant::where('car_model_id', $carModel->id)->count();
            if( $carModelsVariantCount ) {
                return redirect('/admin/cars/car-models')->with('success','Information has not been deleted');
            } else {
                $carModel->delete();
                return redirect('/admin/cars/car-models')->with('success','Information has been deleted');
            }
        } else {
            return redirect('/admin/cars/car-models')->with('success','No information found');
        }
    }
}
