@extends('layouts.admin.master')
{{-- Cars premium view --}}
@section('content')
<div class="row">
    <div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('warning'))
        <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
</div>
    <!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-colorbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Policy Details</h2>

                </header>

                
                
                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" id="formRole" method="POST" @if($formType == 'edit') action="{{action('Auth\RoleController@update',$role->id)}}" @else action="{{url('/admin/roles/')}}"  @endif>
                             @csrf
                             @if($formType == 'edit')
                                <input name="_method" type="hidden" value="PATCH">
                            @endif
                            <fieldset>
                            {{--name--}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Role Name</label>
                                    <div class="col-md-10">   
                                        <input class="form-control"  id ="display_name" name="display_name" type="text" value="{{ $errors->any() ? old('display_name') : (isset($role->name) ? $role->name : '') }}" required>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                {{--Description--}}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description</label>
                                    <div class="col-md-10">   
                                        <input class="form-control"  id ="description" name="description" type="text" value="{{ $errors->any() ? old('description') : (isset($role->description) ? $role->description : '') }}" required>
                                        <span class="my-error" style='color:#b94a48;display:none' >*</span>
                                    </div>
                                </div>
                                 {{-- Permission --}}
                                <div class="form-group">
                                    <label class="control-label col-md-2">Permissions</label>
                                        <div class="col-md-10" id="permission">
                                         @foreach( $permissionGroups as $permission )
                                            <div class="col-md-4">
                                                <div class="checkbox">
                                                    <label>
                                                    @if(isset($role))
                                                     @if( in_array($permission->name, $role->permissions->pluck('name')->toArray()) ) 
                                                            <input name="permissions[]" class="form-check-input {{ $errors->has('permission') ? ' is-invalid' : '' }}" type="checkbox" value={{ $permission->name }} id="{{ $permission->name }}" checked><span>{{ $permission->display_name }}</span>
                                                        @else
                                                       <input name="permissions[]" class="form-check-input {{ $errors->has('permission') ? ' is-invalid' : '' }}" type="checkbox" value={{ $permission->name }} id="{{ $permission->name }}"> <span>{{ $permission->display_name }}</span>
                                                       @endif
                                                       @else
                                                         <input name="permissions[]" class="form-check-input {{ $errors->has('permission') ? ' is-invalid' : '' }}" type="checkbox" value={{ $permission->name }} id="{{ $permission->name }}"> <span>{{ $permission->display_name }}</span>
                                                       @endif
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                            
                                        </div>
                                </div>
                                {{--  --}}
                            </fieldset>
                            {{--  --}}
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('admin.roles.index')}}" class="btn btn-default">Cancel</a>
                                        
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                {{ $formType == 'edit' ? 'Update' : 'Submit'}}
                                            </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        
    </div>

</section>

{{-- <div id="agency_available_for_year_copy">
    <div class="form-group" id="min_premium_agency">
        <label class="control-label col-md-2">Year</label>
        <div class="col-md-10">
            <div class="row">
                <div class="col-sm-12">
                    <input class="form-control" placeholder="Available for years" name="available_for_year" type="text" min="1" value="">
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/css/jquery.datetimepicker.min.css') }}"/>
@endsection

@section('script')
<script src="{{ URL::asset('assets/admin/js/jquery.datetimepicker.js') }}"></script> 
<!-- PAGE RELATED PLUGIN(S) -->

<script type="text/javascript">
        
    $(document).ready(function() {
        
        pageSetUp();
        //

        $("#formRole").on('submit',(function(e) {

            //var action = 0;
            // hideErrorDiv();

            var is_valid =  validForm();
            
            if(is_valid == true){
                //afterward
            }
            else{
                $(window).scrollTop(0);
                return false;
            }
        }));

        //
    });

    function getVariantByBodyType(carBodyTypeId,successCallback) {
        
        $.ajax({
            type:'POST',
            url:'{{route('admin.getVariantByBodyType')}}',
            data:{carBodyTypeId:carBodyTypeId},
            dataType: "html",
            success:function(data){
                $("#model_and_variants").empty().html(data);
                // 
                if (typeof successCallback === "function") {
                    successCallback()
                }
            }
        });
    }

    function validForm(){
        hideErrorDiv();

        var is_valid = true;

        var roleName = $.trim($("#display_name").val());
        var RoleDescription  =$.trim($("#description").val());

        if (roleName == ''){

            $("#display_name").parent().find(".my-error").html('Please enter Role name').show();
            $('#display_name').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

        if (RoleDescription == ''){

            $("#description").parent().find(".my-error").html('Please enter Role Description').show();
            $('#description').closest('.form-group').addClass('has-error');
            is_valid = false;
        }

       
        if (min_premium_nonagency.length > 0){

            if(!$.isNumeric( min_premium_nonagency ) || min_premium_nonagency < 1){
                $("#min_premium_nonagency").parent().find(".my-error").html('Please fill min premium nonagency in numeric').show();
                $('#min_premium_nonagency').closest('.form-group').addClass('has-error');
                is_valid = false;
            }
        }

    }

       

    

    

    function hideErrorDiv() {
        $(".my-error").hide();
        $(".form-group").removeClass('has-error');
    }


/*
    $(".formCarPremium").on('submit',(function(e) {

        var action = 0;

        var formType = "{{--$formType--}}";

        if (formType == 'add'){
            $.ajax(
            {
                type:'POST',
                //url:'/admin/retrievedatacarpremium',
                url:'{{--route('admin.getDataCarPremium')--}}',
                //data:{insuranceProviderId:insuranceProviderId,carBodyTypeIdVal:carBodyTypeIdVal},
                  data: new FormData(this),
                    processData: false,
                    contentType: false,
                  async : false,

                success: function(response){

                    response = $.parseJSON(response);
                    var status = response.status;
                    var msg = '';

                    if( status == 0) {

                        var name_insurance_provider = response.name_insurance_provider;

                        var name_car_model_body_type = response.implodeBodyType;

                        msg = "Insurance Provider : "+ name_insurance_provider + " and Car Model Body type : "+ name_car_model_body_type + " already exist, do you still want to update ?";
                        
                        if (confirm(msg)) {
                            
                            action = 1;
                            //return true;

                        } else {
                            
                            return false;
                        }

                    }

                    if( status == 1 ) {
                      
                      action = 1;

                    }

                }
            });

            if (action == 0){

                return false;
            }
        }
  }));
  */

</script>
@endsection
